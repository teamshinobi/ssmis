# SSMIS#

This repository is made for SSMIS a management information system for SAIDSOTG PNP

### Contributors ###

* JC Frane
* Mark Anthony Consuegra
* Ralph Fabros

### Resources folders for file uploading are ignored please download it and place it inside public folder during cloning ###

* https://www.dropbox.com/s/r2gcj4lxsn9ifk2/resources.zip?dl=0
