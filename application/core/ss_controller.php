<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ss_controller extends CI_Controller {
 
	function __construct(){
		parent::__construct();
        
	}
	//tells whether the user has logged in or not
	public function is_logged_in(){
	   return $this->ion_auth->logged_in();
	}
	
	//echo response in json format.
	public function echo_response($data, $code, $status_message){
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		//$CI->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin")
		$this->output->set_status_header($code, $status_message);
		
		echo json_encode($data);
	}
    public function save_audit(){
        $this->load->model('audit_trailmod');
        $audit = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $data['name'] = $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name');
        $data['description'] = $data['name'] . ' ' . $audit['description'];
        $data['operation'] = $audit['operation'];
        $data['rank'] = $this->session->userdata('rank');
        $data['username'] = $this->session->userdata('username');
        $this->audit_trailmod->add($data);
    }

    public function get_user_pnp_id(){
        $pnp_id = $this->session->userdata('username');
        
        $response['username'] = $pnp_id;
        $response['is_admin'] = $this->ion_auth->is_admin();
        
        $this->echo_response($response,201,'Not OK!');     
    }
    
  public function get_user(){
        $pnp_id = $this->session->userdata('username');
        
        $response['username'] = $pnp_id;
        $response['is_admin'] = $this->ion_auth->is_admin();
        
        return $response;
    }
    
    public function get_current_user(){
        
        $rank = $this->session->userdata('rank');
        $name = $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name');
        preg_match_all('/\b\w/',  $rank, $matches);
        
        
        return implode('',$matches[0]) . ' ' . $name;
    }
	
 
    public function ss_clear_temps(){
        echo 'Clear';
        $this->load->model('generic_model');
        
        $this->generic_model->clear_temps();
    }
    
    
    public function clear_resources(){
        unset($extra_js);
        unset($extra_views);
        unset($extra_css);
    }
    
    //get system current date
    public function ss_date($echoable = TRUE){
        $response['date'] = date("Y-m-d");
        
        if($echoable){
            $this->echo_response($response, 200, 'OK!');
        } else {
            return $response['date'];
        }
       
    }
    public function ss_year(){
        $response['date'] = date("Y");
        $this->echo_response($response, 200, 'OK!');
    }
    public function ss_month(){
        $response['date'] = DateTime::createFromFormat('m', date("m"))->format('F');
        $this->echo_response($response, 200, 'OK!');
    }
    
    
	//log-out
	public function logout(){
        //save to audit trail
        $this->load->model('audit_trailmod');
        $data['operation'] = 'log-out';
        $data['name'] = $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name');
        $data['description'] = $data['name'] . ' has logged-out from the system';
        $data['rank'] = $this->session->userdata('rank');
        $data['username'] = $this->session->userdata('username');
        $this->audit_trailmod->add($data);
		$this->ion_auth->logout();
		//redirect to log-in page
		redirect(base_url());
	}
    
}