<?php

    class Searchmod extends Ss_model { 
        
        
        public function crawl_images($q_str){
            $splits = explode(' ', $q_str);
            
            $config['table'] = 'violators_tbl';
            $config['cols'] = array('violator_id','first_name','last_name', 'middle_name',
                                   'alias','file_path');
            $config['like'] = $splits;
            $config['order_by'] = 'violator_id';
            $config['order_arrange'] = 'DESC';
            
            $violators = $this->special_get($config);
            
            if($violators){
            
            } else {
                $this->set_query_msg('No results found!');
            }
            
            
            return $violators;
            
        }
        
        //search cases for related info about the query_string
        public function crawl_case($q_str){
            $splits = explode(' ', $q_str);
            
            $config['table'] = 'case_report_tbl';
            $config['like'] = $splits;
            $config['order_by'] = 'case_id';
            $config['order_arrange'] = 'DESC';
            $config['conditions'] = array('on_archive' => 0);
            $cases = $this->special_get($config);
            
            
            if($cases){
              foreach($cases as $case){
                    $case->date_of_arrest = $this->ss_get_case_date($case->blotter_entry_nr);
                    $case->violators = $this->ss_get_case_violators($case->blotter_entry_nr);
                    $case->count_violators = count($case->violators);
                    $case->investigator = $this->get_case_investigator($case->blotter_entry_nr);
              }
            }
          
            return $cases;
        }
        
        function get_case_investigator($blotter){
            
            $config['table'] = 'arrest_details_tbl';
            $config['cols'] = 'investigator_pnp_id';
            $config['conditions'] = array(
                'blotter_entry_nr' => $blotter,
            );
                
            $inv = $this->special_get($config)[0];
            $info = $this->ss_get_police_info($inv->investigator_pnp_id);
            foreach($info as $key => $value){
                $inv->$key = $value;
            }
            return $inv;
        
        }
        //search violators for related info about the query_string
        public function crawl_violators($q_str){
            
            $splits = explode(' ', $q_str);
            
            $config['table'] = 'violators_tbl';
            $config['cols'] = array('violator_id','first_name','last_name', 'middle_name',
                                   'alias','nationality','occupation','file_path');
            $config['like'] = $splits;
            $config['order_by'] = 'violator_id';
            $config['order_arrange'] = 'DESC';
            $violators = $this->special_get($config);
            
            if($violators){
               //query more here. 
            } else {
                $this->set_query_msg('No results found!');
            }   
            
           return $violators;
            
        }
        
        public function crawl_pnp($q_str){
            //converts special characters to its string value
            $q_str =  urldecode($q_str);
            
            $splits = explode(' ', $q_str);
            
            $config['table'] = 'pnp_officer_tbl';
            $config['like'] = $splits;
            
            $pnp = $this->special_get($config);
            
            if($pnp){
            
            } else {
                $this->set_query_msg('No results found!');
            }
            
            return $pnp;
        }
    }