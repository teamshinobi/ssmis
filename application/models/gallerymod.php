<?php
    class Gallerymod extends Ss_model {

        public function violators(){
            $config['table'] = 'violators_tbl';
            
            $config['cols'] = array('file_path','violator_id');
            
            $config['order_by'] = 'violator_id';
            $config['order_arrange'] = 'DESC';
            
            $results = $this->special_get($config);
            
            return $results;
        }


    }