<?php

	class Ramod extends Ss_model {
	
		//gets the list of all active and non-active pnp officers
		//NOTE that we only get the required information to be displayed in the table.
		public function get_all_ra(){
		
			return $this->normal_get('charges_tbl');
		}
		public function get_charge($id){
			$params = array(
			
				'charge_id' => $id,
			
			);
			$config = array(
			
				'table' => 'violators_tbl',
				'params' => $params,
				'limit' => 1,
				'offset' => 0
			);
			
			$data = $this->param_get($config);

			return $data;
		}
		//saving to charge table
		public function insert_ra($data){
			$this->db->trans_begin();

			$this->db->insert('charges_tbl', $data);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
			
				return true;
			}
			
		}
		//updating the ra in viewing part
		public function update_ra($data){
			$this->db->trans_begin();

			$config = array(
				'table' => 'charges_tbl',
				'column' => 'charge_id',
				'column_value' => $data['charge_id'],
				'data' => $data,
			);

			$this->normal_update($config);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
			
				return true;
			}
		}
		public function charges_id() {
	        $config = array(
	            'strategy' => 'DB_INCREMENT',
	            'prefix' => 'CHARGE', 
	        );

	        //get the last row in database.
	        $query_config = array(
	            'table' => 'charges_tbl',
	            'column' => 'charge_id',
	            'order' => 'desc',
	            'limit' => 1,
	            'start' => 0,
	            'fields' => array('charge_id'),
	        );

	        //returns array of objects. In this case 1 object only
	        $data = $this->limited_order_get($query_config);
	        //this will be the basis of our DB_INCREMENT strategy		
	        $last_id = "";

	        if(empty($data)){
	            $last_id = false;
	        }else{
	            $last_id = $data[0]->charge_id;
	        }

	        $this->load->library('Incrementor', $config);			

	        return $this->incrementor->increment($last_id);

	    }
	    public function remove_charge($id){
			
			$this->db->trans_begin();
		
			$params = array(
				
				'charge_id' => $id,
			
			);
			$config = array(
				
				'table' => 'charges_tbl',
				'params' => $params,
			
			
			);
			
			$this->normal_delete($config);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correcommit transaction then upload image
				$this->db->trans_commit();	
				
				return true;
			}
		}
				
		/*==========================================
			
						Reporting
			
		==========================================*/
		
		public function report_all(){
		
			$this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			
			$arrayName = array('Charges ID', 'Article', 'Section', 'Fine', 'Description');
			$arrayCol = array(35,25,25,25,75);
			
			$this->pdfgenerator->DefOrientation = 'P';
			$this->pdfgenerator->AliasNbPages();
			$this->pdfgenerator->AddPage();
			$this->pdfgenerator->createTableHeader($arrayName,$arrayCol);
			
			
			$this->pdfgenerator->Ln();
			
			$data = $this->get_all_ra();
			$this->pdfgenerator->SetFont('Arial','',10);
			
			foreach($data as $ra){
				for($i=0; $i <= 4; $i++){
					switch($i){
						case 0:
							$this->pdfgenerator->Cell($arrayCol[0],10 * $this->pdfgenerator->WordWrap($ra->description,95),
								$ra->charge_id ,1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell($arrayCol[1],10 * $this->pdfgenerator->WordWrap($ra->description,95),
								$ra->article,1,0,'C');
							break;
						case 2:
							$this->pdfgenerator->Cell($arrayCol[2],10 * $this->pdfgenerator->WordWrap($ra->description,95),
								$ra->section ,1 ,0, 'C');
							break;
						case 3:
							$this->pdfgenerator->Cell($arrayCol[3],10 * $this->pdfgenerator->WordWrap($ra->description,95),
								$ra->fine ,1 ,0, 'C');
							break;
						case 4:
							$this->pdfgenerator->MultiCell($arrayCol[4],10,$ra->description,1,2);
							break;
					}
				}	
				
			} 
			
			$this->pdfgenerator->Output();
		}
		
	}

