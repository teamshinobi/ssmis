<?php
require_once('application/libraries/mem_image.php');

const TEMPIMGLOC = 'tempimg.png';
	class Viewcasereportmod extends Ss_model {
	
		/*-----------------------------------------------------
		
							Case Report
							
		-------------------------------------------------------*/
        public function is_case_closed($case_id){
            $data = $this->ss_get_case($case_id);
            
            return $data->progress;
        }
        
        public function check_case_completeness($case_id){
            
            $mix = new stdClass();
            $mix->emps = array();
            
            
            $case = $this->ss_get_case($case_id);
            
            if($case->spot_report_id == 'NONE'){
                $mix->emps[] = 'There is no Spot Report attached.';            
            }
            
            if($case->pre_op_id == 'NONE'){
                $mix->emps[] = 'There is no Pre-operation Report attached.';
            }
            
            if($case->coordination_id == 'NONE'){
                $mix->emps[] = 'There is no Coordination Form attached.';
            }
                
            if($case->main_affidavit_id == 'NONE'){
                $mix->emps[] = 'There is no Main Affidavit attached.';
            }
            
            //check directory
        
            $CI =& get_instance(); 
            $CI->load->model('casereportmod');
           
            $files = $CI->casereportmod->scan_permanent_dir($case_id);
            
            
            if(count($files['laboratory']) < 3){
                $mix->emps[] = 'There are no laboratory documents attached.';
            }
            
            if(count($files['medical']) < 3){
                $mix->emps[] = 'There are no medical documents attached.';
            }
            
            
            if($case->nature == 'With Warrant of Arrest'){
                if(count($files['warrant']) < 3){
                    $mix->emps[] = 'There are no warrant arrest documents attached.';
                }
            }
            
            if($case->nature == 'Search Warrant'){
                if(count($files['search']) < 3){
                    $mix->emps[] = 'There are no search warrant documents attached.';
                }
            }
            
           
            if(count($mix->emps) > 0){
            
                $mix->has_empty = true;
            
            } else {
            
                $mix->has_empty = false;
            
            }
            
            return $mix;
            
        }
       
        public function update_cc_judge($case_id, $cc, $judge){
            $to_return = false;
            $this->db->trans_begin();
            
            //update cc
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'cc',
                'param_2' =>    "'{$cc}'",
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            if($this->db->trans_status() === FALSE){
              
        
                $this->db->trans_rollback();

                $to_return = false;
            }
            
            $config['set_params'] = array(
                'param_1' => 'cc_judge',
                'param_2' =>    "'{$judge}'",
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            
            if($this->db->trans_status() === FALSE){
              
        
                $this->db->trans_rollback();

                $to_return = false;
                
            } else {
                $this->db->trans_commit();
                
                $to_return = true;
            }
            
            return $to_return;
        }
        
        public function update_evidence($case_id,$status){
            $to_return = false;
            $this->db->trans_begin();
            
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'status_of_evidences',
                'param_2' =>    "'{$status}'",
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            if($this->db->trans_status() === FALSE){
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
              
                $this->db->trans_rollback();

                $to_return = false;
            }else{	
                $this->db->trans_commit();
                $to_return = true;

            }	
           
            return $to_return;
        }
        
        
        public function get_case_access($user,$case_id){
            $grant_access = false;
            if($user['is_admin'] == true){
                $grant_access = true;
            } else {
                //get blotter
                $config['table'] = 'case_report_tbl';
                $config['cols'] = array('blotter_entry_nr');
                $config['conditions'] = array(
                
                    'case_id' => $case_id,
                
                );
                
                
                $blotter = $this->special_get($config)[0];
                
                
                
                $config['table'] = 'arrest_details_tbl';
                $config['cols'] = array('investigator_pnp_id','booker_pnp_id');
                $config['conditions'] = array(
                
                    'blotter_entry_nr' => $blotter->blotter_entry_nr,
                
                );
                
                $results = $this->special_get($config)[0];
                
                if($results->investigator_pnp_id == $user['username'] || $results->booker_pnp_id == $user['username']){
                
                    $grant_access = true;
                }
            }
            
           
            
            return $grant_access;
            
        }
        
        public function get_case_reports(){
            $config = array(
                'limit' => null,
                'offset' => null,
                'table' => 'case_report_tbl',
                'order_by' => 'case_id',
                'order_arrange' => 'DESC',
                'conditions' => array('on_archive'=>0),

            );
            
            $cases = $this->special_get($config);

            if($cases){
                foreach($cases as $case){
                    $v_config['table'] = 'arrest_booking_form_tbl';
                    $v_config['cols'] = array('violator_id');
                    $v_config['distinct'] = TRUE;
                    $v_config['conditions'] = array(
                        'blotter_entry_nr' => $case->blotter_entry_nr,
                    );

                    $violators = $this->special_get($v_config);

                    if($violators != false){
                      $case->count_violators = count($violators);
                    }

                    //get violators info
                    foreach($violators as $violator){
                        $info = $this->get_case_violators($violator->violator_id);
                        foreach($info as $key => $value){
                            $violator->$key = $value;
                        }
                    }

                    $case->violators = $violators;
                    
                    //get investigator and date
                    $case->investigator = $this->get_case_investigator($case->blotter_entry_nr);
                }
           }
           

            return $cases;
        }
		
        function get_case_investigator($blotter){
            
            $config['table'] = 'arrest_details_tbl';
            $config['cols'] = 'investigator_pnp_id';
            $config['conditions'] = array(
                'blotter_entry_nr' => $blotter,
            );
                
            $inv = $this->special_get($config)[0];
            $info = $this->ss_get_police_info($inv->investigator_pnp_id);
            foreach($info as $key => $value){
                $inv->$key = $value;
            }
            return $inv;
        
        }
        function get_case_violators($id){
            $config['table'] = 'violators_tbl';
            $config['cols'] = array('first_name','last_name', 'alias');
            $config['conditions'] =array( 'violator_id' => $id);
            
            
            return $this->special_get($config)[0];
        }
        
		public function get_casereport($caseId){
			$params = array(
				'case_id' => $caseId,
			);
			$config = array(
				'table' => 'case_report_tbl',
				'limit' => null,
				'offset' => null,
				'conditions' => $params,
			);
			
			$data = $this->special_get($config);
			
			return $data;
		}
		
		public function get_casereport_details($caseId){
			$params = array(
				'case_id' => $caseId,
			);
			$config = array(
				'table' => 'case_report_tbl',
				'limit' => null,
				'offset' => null,
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			
			$arrestDetails = $this->get_arrest_details_by_id($data[0]->blotter_entry_nr, $caseId);
			$data[0]->arrestDetails = $arrestDetails[0];
	
			return $data[0];
		}
		
		/*-----------------------------------------------------
		
							Arrest Details
							
		-------------------------------------------------------*/
		
		public function get_arrest_details(){
			$config = array(
				'limit' => null,
				'offset' => null,
				'cols' => 'date_of_arrest',
				'table' => 'arrest_details_tbl',
			);
			$data = $this->special_get($config);

			return $data;
		}
		
		public function get_arrest_details_by_id($id, $caseId){
			$params = array(
				'blotter_entry_nr' => $id,
			);
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'arrest_details_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			$investigator = $this->get_pnp_name($data[0]->investigator_pnp_id);
			$booker = $this->get_pnp_name($data[0]->booker_pnp_id);
			
			$data[0]->investigatorName = $investigator[0]->rankAcronym . ' ' . $investigator[0]->first_name . ' ' . $investigator[0]->last_name;
			$data[0]->bookerName = $booker[0]->rankAcronym . ' ' . $booker[0]->first_name . ' ' . $booker[0]->last_name;
			
			$arresting = $this->get_arresting_officers($data[0]->blotter_entry_nr);
			
			$inc = 0;
			$offset = 3;
			$officers_arr1 = [];
			$officers_arr2 = [];
			foreach($arresting as $officers){
				if($inc <= 3){
					$officers_arr1[] = $arresting[$inc]->arrestingName;
					
				}else{
					$officers_arr2[] = $arresting[$inc]->arrestingName;
				}

				$inc++;
			}
			$arrestingNames1 = implode(', ', $officers_arr1);
			$data[0]->arrestingOfficers1 = $arrestingNames1;
			
			$arrestingNames2 = implode(', ', $officers_arr2);
			$data[0]->arrestingOfficers2 = $arrestingNames2;

			$barangayDetails = $this->get_barangay_details($data[0]->barangay_id);
			$data[0]->caseLocation = $data[0]->street_name . ' Brgy. ' . $barangayDetails[0]->barangay_name . ', ' . $data[0]->city_name;
			
			$suspectDetails = $this->get_suspects($data[0]->blotter_entry_nr, $caseId);
			$data[0]->suspectDetails = $suspectDetails;
			
			return $data;
		}
		
		public function get_arrest_details_date($id){
			$params = array(
				'blotter_entry_nr' => $id,
			);
			$col = array(
				'date_of_arrest',
			);
			$config = array(
				'cols' => $col,
				'table' => 'arrest_details_tbl',
				'limit' => null,
				'offset' => null,
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			return $data;
		}
		
		public function get_barangay_details($id){
			$params = array(
				'barangay_id' => $id,
			);
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'barangay_tbl',
				'cols' => 'barangay_name',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			return $data;
		}
		
		/*-----------------------------------------------------
		
								Police
							
		-------------------------------------------------------*/
		
		public function get_pnp_name($id){
			$params = array(
				'pnp_id' => $id,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'pnp_officer_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			foreach($data as $object){
				preg_match_all('/\b\w/', $data[0]->rank, $matches);
				$object->rankAcronym = implode('',$matches[0]);
			}
			
			return $data;
		}
		
		public function get_arresting_officers($blot){
			$params = array(
				'blotter_entry_nr' => $blot,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'arresting_officers_list_tbl',
				'cols' => 'arresting_officer_id',
				'order_by' => 'arresting_officer_id',
				'order_arrange' => 'asc',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			$inc = 0;
			foreach($data as $arresting){
				$arrestingDetails = $this->get_pnp_name($data[$inc]->arresting_officer_id);
				$arresting->arrestingName = $arrestingDetails[0]->rankAcronym . ' ' . $arrestingDetails[0]->first_name . ' ' . $arrestingDetails[0]->last_name;
				$inc++;
			}
			return $data;
		}
		
		/*-----------------------------------------------------
		
								Suspects
							
		-------------------------------------------------------*/
		
		public function get_suspects($id, $caseId){
			$params = array(
				'blotter_entry_nr' => $id,
			);
		
			$config = array(
				'limit' => null,
				'offset' => null,
				'cols' => 'violator_id',
				'table' => 'arrest_booking_form_tbl',
				'conditions' => $params,
				'group_by' => 'violator_id',
			);
			$data = $this->special_get($config);
			
			$increment = 0;
			
			foreach($data as $object){
				$violatorDetails = $object;
				$suspectDetails = $this->get_suspect_details($data[$increment]->violator_id);
				$violatorDetails->suspectFN = $suspectDetails[0]->first_name; 
				$violatorDetails->suspectMN = $suspectDetails[0]->middle_name; 
				$violatorDetails->suspectLN = $suspectDetails[0]->last_name; 
				$violatorDetails->suspectAlias = $suspectDetails[0]->alias;
				$violatorDetails->violatorPic = $suspectDetails[0]->file_path;
				$violatorDetails->violatorBirth = $suspectDetails[0]->birth_date;
				$violatorDetails->violatorNation = $suspectDetails[0]->nationality;
				$violatorDetails->violatorMarks = $suspectDetails[0]->identifying_marks;
				$violatorDetails->violatorHeight = $suspectDetails[0]->height;
				$violatorDetails->violatorWeight = $suspectDetails[0]->weight;
				$violatorDetails->violatorEduc = $suspectDetails[0]->educational_attainment;
				$violatorDetails->violatorStrt = $suspectDetails[0]->street_name;
				$violatorDetails->violatorBrg = $suspectDetails[0]->barangay_name;
				$violatorDetails->violatorCity = $suspectDetails[0]->city_name;
				$violatorDetails->violatorEye = $suspectDetails[0]->eyes_color;
				$violatorDetails->violatorHair = $suspectDetails[0]->hair_color;
				$violatorDetails->violatorMother = $suspectDetails[0]->mother_name;
				$violatorDetails->violatorFather = $suspectDetails[0]->father_name;
				$violatorDetails->violatorConPer = $suspectDetails[0]->contact_person;
				$violatorDetails->violatorRelation = $suspectDetails[0]->relationship;
				$violatorDetails->violatorContact = $suspectDetails[0]->contact_info;

				//--------------------Violator Age----------------------------
				$this->load->library('DateTimeHelper');
				$age = $this->datetimehelper->get_age($suspectDetails[0]->birth_date);
				$violatorDetails->violatorAge = $age->y;
				
				//--------------------Violator Age Status---------------------
				if($violatorDetails->violatorAge <= 17){
					$violatorDetails->violatorAgeStatus = '(Minor)';
				}else{
					$violatorDetails->violatorAgeStatus = '';
				}
				//--------------------Violator Gender-------------------------
				if($suspectDetails[0]->gender == 1){
					$violatorDetails->violatorGender = 'Male';
				}else{
					$violatorDetails->violatorGender = 'Female';
				}
				//--------------------Violator Charges------------------------
				$violatorDetails->charges = $this->get_charges($id, $violatorDetails->violator_id);
				
				//--------------------Violator Item Seized------------------------
				
				$violatorItem = $this->get_item_seized($violatorDetails->violator_id, $caseId);
				if($violatorItem != false){
					if($violatorItem[0]->violatorSiezedItems === null){
						$violatorDetails->itemSeized = false;
					}else{
						$violatorDetails->itemSeized = $violatorItem;
					}
				}else{
					$violatorDetails->itemSeized = false;
				}
				//--------------------Violator Narcotic Seized------------------------
				
				$violatorNarc = $this->get_narcotic_seized($violatorDetails->violator_id, $caseId);
				if($violatorNarc != false){
					if($violatorNarc[0]->violatorSiezedNarcotic === null){
						$violatorDetails->narcoticSeized = false;
					}else{
						$violatorDetails->narcoticSeized = $violatorNarc;
					}
				}else{
					$violatorDetails->narcoticSeized = false;
				}
				
				$increment++;
			}
			
			return $data;
		}
		
		public function get_suspect_details($id){
			$params = array(
				'violator_id' => $id,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'violators_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			return $data;
		}
		
		public function get_finger_prints($id){
			$params = array(
				'violator_id' => $id,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'fingerprint_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			return $data;
		}
		public function get_left_fingerprints($id){
			$params = array(
				'violator_id' => $id,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'fingerprint_left_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			return $data;
		}
		/*-----------------------------------------------------
		
							Item Seized
							
		-------------------------------------------------------*/
		public function get_item_seized($violatorId, $caseId){
			$params = array(
				'case_id' => $caseId,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'item_seized_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
            if($data != false){
                foreach($data as $itemSeized){
                    $violatorSiezedItems = $this->get_violator_from_item($violatorId, $itemSeized->item_seized_id);
                    if($violatorSiezedItems !== false){
                        $itemSeized->violatorSiezedItems = $violatorSiezedItems;
                    }else{
                        $itemSeized->violatorSiezedItems = null;
                    }
                }
            } else {
                return false;
            
            }
			
			
			return $data;
		}
		
		public function get_violator_from_item($violatorId, $id){
			$params = array(
				'item_seized_id' => $id,
				'violator_id' => $violatorId,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'violator_item_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			return $data;
		}
	
		/*-----------------------------------------------------
		
							Narcotic Seized
							
		-------------------------------------------------------*/
		
		public function get_narcotic_seized($violatorId, $caseId){
			$params = array(
				'case_id' => $caseId,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'narcotics_item_seized_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			if($data != false){
				foreach($data as $narcoticSeized){
					$narcotic = $this->get_violator_from_narcotic($violatorId, $narcoticSeized->narcotics_seized_id);
					if($narcotic !== false){
						$narcoticSeized->violatorSiezedNarcotic = $narcotic;
						$narcoticDetails = $this->get_narcotic($narcoticSeized->narcotics_id);
						$narcoticSeized->narcoticName = $narcoticDetails[0]->name;
						$narcoticSeized->narcoticDesc = $narcoticDetails[0]->description;
						$narcoticSeized->narcoticUnit = $narcoticDetails[0]->unit;
					}else{
						$narcoticSeized->violatorSiezedNarcotic = null;
					}
				}
			}
			return $data;
			
		}
		public function get_violator_from_narcotic($violatorId, $id){
			$params = array(
				'narcotics_seized_id' => $id,
				'violator_id' => $violatorId,
			);
			
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'violator_narcotics_item_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
			return $data;
		}
		
		public function get_narcotic($id){
			$config['table'] = 'narcotics_tbl';
            $config['conditions'] = array('narcotics_id' => $id);
                
			$data = $this->special_get($config)[0];
			
			return $data;
		}
		
		/*-----------------------------------------------------
		
							All Property Seized
							
		-------------------------------------------------------*/
		
		public function get_narcotic_from_caseid($id){
			$params = array(
				'case_id' => $id,
			);

			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'narcotics_item_seized_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
            
            if($data){
                foreach($data as $nar){
                    
                    $narc = $this->get_narcotic($nar->narcotics_id);
                    
                    $nar->description = $narc->name;
                }
            }
			
			return $data;
		}
		
		public function get_item_from_caseid($id){
			
            $params = array(
				'case_id' => $id,
			);

			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'item_seized_tbl',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			
            if($data == false){
                $data = array();
            }
			return $data;
		}
		
		public function get_property_seized($id){
			
			$narcotics = $this->get_narcotic_from_caseid($id);
			
			$items = $this->get_item_from_caseid($id);
			
            if(!$narcotics){
                $narcotics = array();
            }
            
            if(!$items){
                $items = array();
            }
            
            
            $result = array_merge($narcotics,$items);
            
			return $result;
		}
		
		/*-----------------------------------------------------
		
							Charges
							
		-------------------------------------------------------*/
		
		public function get_charges($blotter, $violatorId){
			$params = array(
				'blotter_entry_nr' => $blotter,
				'violator_id' => $violatorId,
			);
			$config = array(
				'limit' => null,
				'offset' => null,
				'table' => 'arrest_booking_form_tbl',
				'cols' => 'charge_id',
				'conditions' => $params,
			);
			$data = $this->special_get($config);
			foreach($data as $object){
				$chargeName = $this->get_charges_details($object->charge_id);
				$object->chargeName = $chargeName[0]->article . ' ' . $chargeName[0]->section;
			}
			return $data;
		}
		
		public function get_charges_details($id){
			$params = array(
				'charge_id' => $id,
			);
			$cols = array(
				'article',
				'section',
			);
			$config = array(
				'limit' => null,
				'offset' => null,
				'cols' => $cols,
				'table' => 'charges_tbl',
				'conditions' => $params,
			);
			
			$data = $this->special_get($config);
			
			return $data;
		}
		//=============================================================================================
        //                                      Consolidating        
        //=============================================================================================
        
        public function get_coordination_from_case($case_id){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('attached_to' => $case_id);
            
            return $this->special_get($config)[0];
        }
        
        public function get_preop_from_case($case_id){
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array('attached_to' => $case_id);
            
            return $this->special_get($config)[0];
        }
        
        public function get_spot_report_from_case($case_id){
            $config['table'] = 'spot_report_tbl';
            $config['conditions'] = array('attached_to' => $case_id,);
            
            return $this->special_get($config)[0];
        }
        
        public function get_main_affidavit_from_case($case_id){
            $config['table'] = 'affidavit_tbl';
            $config['conditions'] = array('attached_to' => $case_id,
                                         'type' => 'main');
            
            return $this->special_get($config)[0];
        }
        
        public function get_supplemental_affidavit_from_case($case_id){
            $config['table'] = 'affidavit_tbl';
            $config['conditions'] = array('attached_to' => $case_id,
                                         'type' => 'supplemental');
            
            return $this->special_get($config)[0];
        }
		/*-----------------------------------------------------
		
							Reporting
							
		-------------------------------------------------------*/
		
		public function full_case_report($case_id){
            $this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			$this->pdfgenerator->DefOrientation = 'P';
			$this->pdfgenerator->AliasNbPages();
            
            //------------------------------------------Pre-Operation Form----------------------------
            
            $CI =& get_instance(); 
            $CI->load->model('formsmod');
            
            
            $pre_op = $this->get_preop_from_case($case_id);
            if($pre_op){
                $pre_op_pdf = $CI->formsmod->preoperation_pdf($pre_op->pre_op_id);
            }
			
            //------------------------------------------Coordination Form----------------------------
            
            $coordination = $this->get_coordination_from_case($case_id);
            if($coordination){
                $coordination_pdf = $CI->formsmod->coordination_pdf($coordination->coordination_id);    
            }
            
            //------------------------------------------Spot Report Form----------------------------
            
            $spot = $this->get_spot_report_from_case($case_id);
            if($spot){
                $main_pdf = $CI->formsmod->spot_report_pdf($spot->spot_report_id);    
            }			
            
            //------------------------------------------Affidavits Form----------------------------
                
                //=====================Main Affidavit
            
                $main = $this->get_main_affidavit_from_case($case_id);
                if($main){
                    $main_pdf = $CI->formsmod->affidavit_pdf($main->affidavit_id);
                }                
            
                //=====================Supplemental Affidavit
            
                $supplemental = $this->get_supplemental_affidavit_from_case($case_id);
                if($supplemental){
                    $supplemental_pdf = $CI->formsmod->affidavit_pdf($supplemental->affidavit_id);
                }
                            
            //------------------------------------------Medical Form-------------------------------------
            
            $medical_form = $CI->formsmod->medical_pdf($case_id);

            //------------------------------------------Laboratory Form-------------------------------------
            
            $laboratory_form = $CI->formsmod->laboratory_pdf($case_id);
            
            //------------------------------------------Search Form-------------------------------------
            
            $search_form = $CI->formsmod->search_pdf($case_id);
            
            //------------------------------------------Warrant Form-------------------------------------
            
            $warrant_form = $CI->formsmod->warrant_pdf($case_id);
            
            //------------------------------------------Other Form-------------------------------------
            
            $other_form = $CI->formsmod->other_pdf($case_id);
                        
            //------------------------------------------Inventory Seized Form----------------------------
            
            $this->inventory_seized_pdf($case_id);
            
            //------------------------------------------Arrest Booking-----------------------------------
            
            $this->arrest_booking_pdf($case_id);
            
			$this->pdfgenerator->Output($case_id . '.pdf', 'I');
        }
        
        public function inventory_seized_pdf($case_id){
                       
			//-----------------------------------------------------------------------------------------------
            //                                             Labels
			//-----------------------------------------------------------------------------------------------
                        
			$propertySeized = $this->get_property_seized($case_id);            
                        
			$CI =& get_instance(); 
            $CI->load->model('editcasereportmod');
            
			$data = $CI->editcasereportmod->get_full_details($case_id);

			$violatorArr = [];

			foreach ($data->violators as $vls) {
				$violatorArr[] = $vls->first_name . ' ' . substr($vls->middle_name, 0, 1) . '. ' . $vls->last_name;
			}

			$violatorString = implode(', ' , $violatorArr);

            $this->load->library('DateTimeHelper');
			$this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			$this->pdfgenerator->DefOrientation = 'P';
			$this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
            
			$this->pdfgenerator->AddPage();
			
            $this->pdfgenerator->SetFont('Arial','BU',14);
            $this->pdfgenerator->Cell(200,5,'INVENTORY OF PROPERTY SEIZED',0,1,'C');  
            
            $this->pdfgenerator->Ln(10);

            $this->pdfgenerator->SetFont('Arial','B',12);			
			$this->pdfgenerator->Cell(140,10,'DATE :',0, 0, 'R');

			$this->pdfgenerator->SetFont('Arial','U',12);		
			$this->pdfgenerator->Cell(100,10,$this->datetimehelper->parseto_standard_date($data->arrest_details->date_of_arrest), 0, 1, 'L');
			
			$text = '<p><span style="line-height: 22.1000003814697px; text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is to acknowledge the receipt of the property/items(dangerous drugs) herein described below from ';
			
			$text .= '<b><u>' . $violatorString . '</u></b>' . ' during the ';

			$text .= '<b><u>' . $data->case_details->nature . ' Operation </u></b>' . ' at ';

			$text .= '<b><u>' . $data->arrest_details->street_name . ' St. Brgy. ' . $data->arrest_details->barangay . ' ' . $data->arrest_details->city_name . '</u></b>' . ' on ';

			$text .= '<b><u>' . $this->datetimehelper->parseto_standard_date($data->arrest_details->date_of_arrest) . '</u></b>' . ' at about ';

			$text .= '<b><u>' . $this->datetimehelper->parseto_system_time($data->arrest_details->time_of_arrest) . '.</u></b></span></p>';

			$this->pdfgenerator->SetLeftMargin(25);
			$this->pdfgenerator->SetRightMargin(20);
			$this->pdfgenerator->SetFont('Arial','',12);
			$this->pdfgenerator->MultiAlignCell(0,0,$this->pdfgenerator->WriteHTML($text),0,1,'J');

			$this->pdfgenerator->Ln(20);

			$arrayName = array('Item Number', 'Quantity', 'Remarks', 'Description');
			$arrayCol = array(25,25,40,75);
			
			$this->pdfgenerator->createTableHeader($arrayName,$arrayCol, 10);
			
			$this->pdfgenerator->Ln();
			$this->pdfgenerator->SetFont('Arial','',10);
            
            $cnt = 1;
            
            if($propertySeized){
                foreach($propertySeized as $ps){
                                       
                    for($i=0; $i <= 4; $i++){
                        switch($i){
                            case 0:
                                $this->pdfgenerator->Cell($arrayCol[0],10,$cnt,1,0,'C');
                                break;
                            case 1:
                                $this->pdfgenerator->Cell($arrayCol[1],10,$ps->quantity,1,0,'C');
                                break;
                            case 2:
                                $this->pdfgenerator->Cell($arrayCol[2],10,$ps->markings,1,0,'C');
                                break;
                            case 3:						
                                $this->pdfgenerator->MultiCell($arrayCol[3],10,$ps->description,1 ,'C');
                                break;
                        }
                    }
                    $cnt++;
                } 
            }
            
            $this->pdfgenerator->Ln(20);

            
            $this->pdfgenerator->MultiAlignCell(30,5,'Seizing Officer :',0, 0, 'L');
            $x = $this->pdfgenerator->GetX();
            $y = $this->pdfgenerator->GetY();
            
            if($data->seizing_officers){
            	foreach ($data->seizing_officers as $so) {
            		$this->pdfgenerator->SetX($x);
            		$this->pdfgenerator->SetFont('Arial','BU',12);
					$this->pdfgenerator->MultiAlignCell(50,5,$so->rank . ' ' . $so->first_name,0, 1, 'C');		

					$this->pdfgenerator->SetX($x);
					$this->pdfgenerator->SetFont('Arial','I',10);
					$this->pdfgenerator->MultiAlignCell(50,5,'Rank/Name/Signature',0, 1, 'C');

					$this->pdfgenerator->Ln(15);
				}
            }
            $this->pdfgenerator->MultiAlignCell(30,5,'Witness/es :',0, 0, 'L');
			if($data->witnesses){
				foreach ($data->witnesses as $w) {
            		$this->pdfgenerator->SetX($x);
            		$this->pdfgenerator->SetFont('Arial','BU',12);
					$this->pdfgenerator->MultiAlignCell(50,5,$w->name,0, 0, 'C');		
					$this->pdfgenerator->Ln();
					$this->pdfgenerator->SetX($x);
					$this->pdfgenerator->SetFont('Arial','I',10);
					$this->pdfgenerator->MultiAlignCell(50,5,'Name/Signature',0, 0, 'C');

					$this->pdfgenerator->Ln(15);
				}
			}

			$this->pdfgenerator->SetXY($x+50,$y);            			

            $this->pdfgenerator->MultiAlignCell(30,5,'Arresting Officer :',0, 0, 'L');

            $x1 = $this->pdfgenerator->GetX();

			if($data->arrest_details->arresting_officers){
				foreach ($data->arrest_details->arresting_officers as $ao) {
					$this->pdfgenerator->SetX($x1);
					$this->pdfgenerator->SetFont('Arial','BU',12);
					$this->pdfgenerator->MultiAlignCell(50,5,$ao->rank . ' ' . $ao->first_name,0, 1, 'C');		
					$this->pdfgenerator->SetX($x1);
					$this->pdfgenerator->SetFont('Arial','I',10);
					$this->pdfgenerator->MultiAlignCell(50,5,'Rank/Name/Signature',0, 1, 'C');

					$this->pdfgenerator->Ln(15);
				}
			}
	
			
			

        }
        
		public function arrest_booking_pdf($id){
            
			$CI =& get_instance(); 
            $CI->load->model('editcasereportmod');
            
			$data = $CI->editcasereportmod->get_full_details($id);
       
			$this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			$this->pdfgenerator->DefOrientation = 'P';
			$this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
                        
			//---------------------------------------------------------------------------------------------
			//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<-Arrest and Booking Form->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			//---------------------------------------------------------------------------------------------
            
			foreach($data->violators as $violators){
                $this->pdfgenerator->Header = 'Arrest Booking';
				$this->pdfgenerator->AddPage();

				$this->pdfgenerator->Ln(6);
                                
				$this->pdfgenerator->Image( base_url() . $violators->file_path . '/' .  $violators->violator_id . '_front.jpg',20,10,30,30);				
			
				$this->pdfgenerator->SetRightMargin(15);
            	$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->SetFont('Arial','',10);
				$this->pdfgenerator->MultiAlignCell(42,5,'BLOTTER ENRTRY NR :',0, 0, 'L');
				$this->pdfgenerator->MultiAlignCell(42,5,$data->case_details->blotter_entry_nr,'B',0,'C');
				$this->pdfgenerator->MultiAlignCell(42,5,'',0,0,'C');

				$this->pdfgenerator->MultiAlignCell(15,5,'DATE :',0,0,'L');
				$this->pdfgenerator->MultiAlignCell(36,5,$this->datetimehelper->parseto_standard_date($data->arrest_details->date_of_arrest),'B',0,'C');

				$this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(19);

				//		violator name
				$this->pdfgenerator->MultiAlignCell(59,5,$violators->last_name,'B',0,'C');
				$this->pdfgenerator->MultiAlignCell(59,5,$violators->first_name,'B',0,'C');
				$this->pdfgenerator->MultiAlignCell(58,5,$violators->middle_name,'B',0,'C');

				$this->pdfgenerator->Ln();
				$this->pdfgenerator->SetX(19);
				$this->pdfgenerator->SetFont('Arial','',8);
				$this->pdfgenerator->MultiAlignCell(59,5,'(Last Name)',0,0,'C');
				$this->pdfgenerator->MultiAlignCell(59,5,'(First Name)',0,0,'C');
				$this->pdfgenerator->MultiAlignCell(58,5,'(Middle Name)',0,0,'C');

				$this->pdfgenerator->Ln();
				$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->SetFont('Arial','',10);
				$this->pdfgenerator->MultiAlignCell(22,5,'ADDRESS :',0,0,'L');
				$this->pdfgenerator->MultiAlignCell(155,5,$violators->street_name . ' St. Brgy. ' . $violators->barangay_name . ' ' . $violators->city_name . ' City','B',0,'L');

				$this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->MultiAlignCell(17,5,'TEL NO :',0,0,'L');
				$this->pdfgenerator->MultiAlignCell(40,5,$violators->tel_no,'B',0,'C');                            
				$this->pdfgenerator->MultiAlignCell(12,5,'POB :',0,0,'L');
				$this->pdfgenerator->MultiAlignCell(45,5,$violators->place_of_birth,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(12,5,'DOB :',0,0,'L');
				$this->pdfgenerator->MultiAlignCell(51,5,$this->datetimehelper->parseto_standard_date($violators->birth_date),'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                            				
                if($violators->marital_status == 'Single'){
					$single = true;
                    $married = false;
                    $widow = false;
                    $seperated = false;
				}elseif($violators->marital_status == 'Married'){
					$single = false;
                    $married = true;
                    $widow = false;
                    $seperated = false;
				}elseif($violators->marital_status == 'Widow/er'){
					$single = false;
                    $married = false;
                    $widow = true;
                    $seperated = false;
				}elseif($violators->marital_status == 'Seperated'){
					$single = false;
                    $married = false;
                    $widow = false;
                    $seperated = true;
				}
                
                if($violators->gender == 0){
					$male = false;
                    $female = true;
				}elseif($violators->gender == 1){
                    $male = true;
					$female = false;
				}
				
                $this->pdfgenerator->MultiAlignCell(35,5,'MARITAL STATUS :',0,0,'L');
                $mx = $this->pdfgenerator->GetX();
                $this->pdfgenerator->MultiAlignCell(5,5,$this->pdfgenerator->Rect($this->pdfgenerator->GetX()+1.5,$this->pdfgenerator->GetY()+1, 2.5, 2.5, $single),0,0,'C');
                $this->pdfgenerator->MultiAlignCell(21,5,'SINGLE',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(5,5,$this->pdfgenerator->Rect($this->pdfgenerator->GetX()+1.5,$this->pdfgenerator->GetY()+1, 2.5, 2.5, $widow),0,0,'C');
                $this->pdfgenerator->MultiAlignCell(50,5,'WIDOW/ER',0,0,'L');
                
                $this->pdfgenerator->MultiAlignCell(20,5,'GENDER :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(5,5,$this->pdfgenerator->Rect($this->pdfgenerator->GetX()+1.5,$this->pdfgenerator->GetY()+1, 2.5, 2.5, $male),0,0,'C');
                $this->pdfgenerator->MultiAlignCell(21,5,'MALE',0,1,'L');
                
                $this->pdfgenerator->SetX($mx);
                $this->pdfgenerator->MultiAlignCell(5,5,$this->pdfgenerator->Rect($this->pdfgenerator->GetX()+1.5,$this->pdfgenerator->GetY()+1, 2.5, 2.5, $married),0,0,'C');
                $this->pdfgenerator->MultiAlignCell(21,5,'MARRIED',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(5,5,$this->pdfgenerator->Rect($this->pdfgenerator->GetX()+1.5,$this->pdfgenerator->GetY()+1, 2.5, 2.5, $seperated),0,0,'C');
                $this->pdfgenerator->MultiAlignCell(50,5,'SEPERATED',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(20,5,'',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(5,5,$this->pdfgenerator->Rect($this->pdfgenerator->GetX()+1.5,$this->pdfgenerator->GetY()+1, 2.5, 2.5, $female),0,0,'C');
                $this->pdfgenerator->MultiAlignCell(21,5,'FEMALE',0,0,'L');
				
                $age = $this->datetimehelper->get_age($violators->birth_date);
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(12,5,'AGE :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(15,5,$age->y,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(19,5,'WEIGHT :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(19,5,$violators->weight,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(18,5,'HEIGHT :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(19,5,$violators->height,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(14,5,'EYES :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(19,5,$violators->eyes_color,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(13,5,'HAIR :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(29,5,$violators->hair_color,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(28,5,'COMPLEXION :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(31,5,$violators->complexion,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(28,5,'OCCUPATION :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(31,5,$violators->occupation,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(28,5,'NATIONALITY :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(31,5,$violators->nationality,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(70,5,'HIGHEST EDUCATIONAL ATTAINMENT :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(107,5,$violators->educational_attainment,'B',0,'L');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(36,5,'NAME OF SCHOOL :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(141,5,$violators->name_of_school,'B',0,'L');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(44,5,'LOCATION OF SCHOOL :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(133,5,$violators->location_of_school,'B',0,'L');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(75,5,'IDENTIFYING MARKS/CHARACTERISTICS :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(102,5,$violators->identifying_marks,'B',0,'L');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(33,5,"DRIVER'S LIC NR :",0,0,'L');
                $this->pdfgenerator->MultiAlignCell(35,5,$violators->dr_license,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(23,5,'ISSUED AT :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(40,5,$violators->dr_issued_at,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(10,5,'ON :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(36,5,$violators->dr_issued_on,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(28,5,'RES CERT NR :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(40,5,$violators->res_cert,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(46,5,'DATE AND PLACE ISSUE :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(63,5,$violators->res_cert_issued_on . ' / ' . $violators->res_cert_issued_at,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(35,5,'OTHER ID CARDS :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(65,5,$violators->other_ids,'B',0,'C');
                $this->pdfgenerator->MultiAlignCell(14,5,'ID NO :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(63,5,$violators->id_numbers,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(36,5,'NAME OF FATHER :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(110,5,$violators->father_name,'B',0,'L');
                $this->pdfgenerator->MultiAlignCell(12,5,'AGE :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(19,5,$violators->father_age,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(22,5,'ADDRESS :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(155,5,$violators->father_address,'B',0,'L');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(36,5,'NAME OF MOTHER :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(110,5,$violators->mother_name,'B',0,'L');
                $this->pdfgenerator->MultiAlignCell(12,5,'AGE :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(19,5,$violators->mother_age,'B',0,'C');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(22,5,'ADDRESS :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(155,5,$violators->mother_address,'B',0,'L');
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->SetFont('Arial','',8);
                $this->pdfgenerator->MultiAlignCell(177,5,'NAME & ADDRESS OF PERSON TO BE CONTACTED IN CASE OF EMERGENCY:',0,0,'C');
                
                $this->pdfgenerator->Ln(5);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->SetFont('Arial','',10);
                $this->pdfgenerator->MultiAlignCell(15,5,'NAME :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(162,5,$violators->contact_person,'B',0,'L');
                
                
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(30,5,'RELATIONSHIP :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(87,5,$violators->relationship,'B',0,'L');
                $this->pdfgenerator->MultiAlignCell(14,5,'TEL # :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(46,5,$violators->contact_info,'B',0,'C');
                
                
                $this->pdfgenerator->Ln(5);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->SetFont('Arial','',10);
                $this->pdfgenerator->MultiAlignCell(20,5,'LAWYER :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(162,5,$data->arrest_details->lawyer ,'B',0,'L');

                
				$charges = '';
				
                $articles = new stdClass();
                //create arrays for article
                foreach($violators->charges as $charge){
                    if(!property_exists($articles,$charge->article)){
                        $article = $charge->article;
                        $articles->$article = array();
                    } 
                }
                
               //put each section in array
                foreach($violators->charges as $charge){
                    if(property_exists($articles, $charge->article)){

                        $name =  $charge->article;
                        $arr = $articles->$name;
                        array_push($articles->$name,$charge->section);

                    }

                } 
                
                
                $charges = '';
                foreach($articles as $key => $value){
                    $charges .= $key . ' ' ;
                    foreach($articles->$key as $section){
                        $charges .= $section . ', ';
                    }
                    
                    $charges .= '; ';
                }
                
                $chargeLines = $this->pdfgenerator->WordWrap($charges, 90);
								
                $this->pdfgenerator->Ln(6);
				$this->pdfgenerator->SetX(18);
                $this->pdfgenerator->MultiAlignCell(37,5,'OFFENSE CHARGE :',0,0,'L');
				$y2 = $this->pdfgenerator->GetY();				
                $this->pdfgenerator->MultiAlignCell(90,5,$charges,'B',1,'L');
			 	$this->pdfgenerator->MultiAlignCell(37,5,'',0,0,'L');
				$this->pdfgenerator->SetFont('Arial','',7);
                
				$this->pdfgenerator->MultiAlignCell(90,5,'(NATURE OF OFFENSE)',0,0,'C');
				$x2 = $this->pdfgenerator->GetX();
				
				$this->pdfgenerator->SetXY($x2,$y2);
				$this->pdfgenerator->MultiAlignCell(6,5,'',0,0,'C');
                $this->pdfgenerator->MultiAlignCell(47,5,'','B',1,'C');				
				$this->pdfgenerator->MultiAlignCell(135,5,$data->case_details->is_number,0,0,'C');
				$this->pdfgenerator->MultiAlignCell(47,5,'(CRIM / IS NO.)',0,0,'C');
				$y3 = $this->pdfgenerator->GetX();
                
				$this->pdfgenerator->Ln(6+$chargeLines);							
				$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->SetFont('Arial','',10);
				$this->pdfgenerator->MultiAlignCell(38,5,'WHERE ARRESTED :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(139,5,$data->arrest_details->street_name . ' St. Brgy. ' . $data->arrest_details->barangay . ' ' . $data->arrest_details->city_name,'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(34,5,'DATE ARRESTED :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(143,5,$this->datetimehelper->parseto_standard_date($data->arrest_details->date_of_arrest),'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(62,5,'NAME OF ARRESTING OFFICER/S :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(115,5,'','B',1,'C');
									
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(27,5,'UNIT: SECTOR',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(150,5,'Station Anti-Illegal Drugs-Special Operation Task Group','B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(75,5,'MEDICAL EXAMINATION CONDUCTED AT :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(102,5,$data->arrest_details->med_at,'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(16,5,'BY: DR.',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(90,5,$data->arrest_details->dr_name,'B',0,'L');
				$this->pdfgenerator->MultiAlignCell(10,5,'ON :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(61,5,$data->arrest_details->med_on,'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(47,5,'FINGERPRINT TAKEN BY :',0,0,'L');
                if($data->arrest_details->fingerman){
                    $this->pdfgenerator->MultiAlignCell(130,5,$data->arrest_details->fingerman->rank . ' ' . $data->arrest_details->fingerman->first_name . ' ' . $data->arrest_details->fingerman->last_name,'B',0,'L');
                }else {
                    $this->pdfgenerator->MultiAlignCell(130,5,'','B',0,'L');
                }
               
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(35,5,'PHOTO TAKEN BY :',0,0,'L');
    
                if($data->arrest_details->photographer){
                    $this->pdfgenerator->MultiAlignCell(142,5,$data->arrest_details->photographer->rank . ' ' . $data->arrest_details->photographer->first_name . ' ' . $data->arrest_details->photographer->last_name,'B',0,'L');
                } else {
                    $this->pdfgenerator->MultiAlignCell(142,5,'','B',0,'L');        
                }
               
				$arresting_array = [];
                foreach($data->arrest_details->arresting_officers as $ao){
                    $arresting_array[] = $ao->rank . ' ' . $ao->first_name . ' ' . $ao->last_name;
                }
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->MultiAlignCell(42,5,'ARRESTING OFFICER :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(135,5,implode(', ', $arresting_array),'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->SetFont('Arial','',7);
				$this->pdfgenerator->MultiAlignCell(42,5,'',0,0,'L');
				$this->pdfgenerator->MultiAlignCell(135,5,'RANK/NAME/SIGNATURE',0,0,'C');				
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);	
				$this->pdfgenerator->SetFont('Arial','',10);
				$this->pdfgenerator->MultiAlignCell(42,5,'DUTY INVESTIGATOR :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(135,5,$data->arrest_details->investigator->rank . ' ' . $data->arrest_details->investigator->first_name . ' ' . $data->arrest_details->investigator->last_name,'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);		
				$this->pdfgenerator->MultiAlignCell(65,5,'BOOK BY(RANK/NAME/SIGNATURE) :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(112,5,$data->arrest_details->booker->rank . ' ' . $data->arrest_details->booker->first_name . ' ' . $data->arrest_details->booker->last_name,'B',0,'L');
				
				$this->pdfgenerator->Ln(6);							
				$this->pdfgenerator->SetX(18);		
				$this->pdfgenerator->MultiAlignCell(67,5,'SIGNATURE OF PERSON ARRESTED :',0,0,'L');
                $this->pdfgenerator->MultiAlignCell(110,5,'','B',0,'C');
				
				$this->pdfgenerator->Ln(5);							
				$this->pdfgenerator->SetX(18);		
				$this->pdfgenerator->MultiAlignCell(67,5,'',0,0,'L');
				$this->pdfgenerator->SetFont('Arial','',7);
                $this->pdfgenerator->MultiAlignCell(110,4,'(INDICATE IF PERSON REFUSE TO SIGN)',0,0,'C');
				
				$Rfingers = $this->get_finger_prints($violators->violator_id);
                $lFingers = $this->get_left_fingerprints($violators->violator_id);
                				
				$j=0;
				$thumbR = '';
				$indexR = '';
				$middleR = '';
				$ringR = '';
				$littleR = '';
				$thumbL = '';
				$indexL = '';
				$middleL = '';
				$ringL = '';
				$littleL = '';
				
				if($Rfingers){
					foreach($Rfingers as $rightFinger){
						
						//----------------------------right hand------------------------
				        if($rightFinger->thumb_rimg){
                            $thumbR = $rightFinger->thumb_rimg;
                        }else{
							$thumbR = '';
						}

                        if($rightFinger->index_rimg){
                            $indexR = $rightFinger->index_rimg;
                        }else{
							$indexR = '';
						}
						
                        if($rightFinger->middle_rimg){
                           $middleR = $rightFinger->middle_rimg;
                        }else{
							$middleR = '';
						}
                        if($rightFinger->ring_rimg){
                           $ringR = $rightFinger->ring_rimg;
                        }else{
							$ringR = '';
						}
				      
                        if($rightFinger->little_rimg){
                            $littleR = $rightFinger->little_rimg;
                        }else{
							$littleR = '';
						}						
					}
				}
				if($lFingers){
					foreach($lFingers as $leftFinger){
						if($leftFinger->thumb_limg){
							$thumbL = $leftFinger->thumb_limg;
						}else{
							$thumbL = '';
						}

						if($leftFinger->index_limg){
						   $indexL = $leftFinger->index_limg;
						}else{
							$indexL = '';
						}

						if($leftFinger->middle_limg){
							$middleL = $leftFinger->middle_limg;
						}else{
							$middleL = '';
						}

						if($leftFinger->ring_limg){
							$ringL = $leftFinger->ring_limg;
						}else{
							$ringL = '';
						}

						if($leftFinger->little_limg){
							$littleL = $leftFinger->little_limg;
						}else{
							$littleL = '';
						}
					}                  
				}
				
				
				$this->pdfgenerator->Ln(5);							
				$this->pdfgenerator->SetX(18);		
				$this->pdfgenerator->SetFont('Arial','',6);
				$this->pdfgenerator->MultiAlignCell(177,4,'RIGHT HAND','T',0,'L');
				
				$this->pdfgenerator->Ln();							
				$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($thumbR, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'TRB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($indexR, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'LTB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($middleR, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'LTB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($ringR, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'LTB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($littleR, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'TLB',0,'C');
				
				$this->pdfgenerator->Ln();							
				$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->MultiAlignCell(35.4,4,'THUMB','L',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,4,'INDEX',0,0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,4,'MIDDLE',0,0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,4,'RING',0,0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,4,'LITTLE','R',0,'C');
				
				$this->pdfgenerator->Ln();							
				$this->pdfgenerator->SetX(18);
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($thumbL, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'TRB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($indexL, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'LTB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($middleL, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'LTB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($ringL, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'LTB',0,'C');				
				$this->pdfgenerator->MultiAlignCell(35.4,25,$this->pdfgenerator->MemImage($littleL, $this->pdfgenerator->GetX(),$this->pdfgenerator->GetY(), 30,25),'TLB',0,'C');
				
				$this->pdfgenerator->Ln();							
				$this->pdfgenerator->SetX(18);						
				$this->pdfgenerator->MultiAlignCell(177,4,'LEFT HAND','B',0,'L');
				
			}
            

		}
		
	}


