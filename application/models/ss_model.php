<?php
	class Ss_model extends CI_Model{
	
		private $db_errs = [];
        
        private $q_msg;
	
		//gets data in the table without any WHERE clause.
		public function normal_get($table){	
			
			$query = $this->db->get($table);
			
			return $this->return_data($query);		
		}
		
		//gets data with multiple param
		public function param_get($config){
				
			$query = $this->db->get_where($config['table'], $config['params'],
				$config['limit'], $config['offset']);
			
			return $this->return_data($query);		
		}
			
		
		/*=============================================
		*	limited_order_get($config); 	
		*
		*	Gets all data with ordering and limit/offset
		* 
		*	Accepts associative array and must be in 
		*	following format:
		*
		* 	$config['table'], $config['column'], $config['fields']
		*   $config['order'], $config['limit'], 
		*	$config['offset']
		*
		* 	where the value of order must be 'asc' or 'desc' only.
		*==============================================*/
		
		public function limited_order_get($config){
			$this->db->order_by($config['column'], $config['order']);
			
			//parse selection fields. If not set or empty, select all.
			if(isset($config['fields'])){
				$fields = "";
				$last = end($config['fields']);
				foreach($config['fields'] as $value){
					if($value === $last){
						$fields .= $value . ' ';
					}else{
						$fields .= $value . ', ';
					}
					$this->db->select($fields);
				}//end foreach
			}
			
			$query = $this->db->get($config['table'], $config['limit'], $config['start']);
			
			//echo  $this->db->last_query();
			
			return $this->return_data($query);
		}
		
		//insert data in table
		public function normal_insert($table,$data){
			$this->db->insert($table,$data);
		}
		
		//deletes data with multiple condition
		/*
		*	$config['table'] => table name where the deletion would take place
		*	$config['params'] => needed condition to satisfy the delete
		*
		*/
		public function normal_delete($config){
			$this->db->delete($config['table'], $config['params']); 
		}
		
		//updates a data in table with specified condition. 1 condition only
		/*
		*	accepts $config variable where the indices are following:
		*	
		*	$config['table'] ->  The name of the table.
		*	$config['column'] -> The name of the column where you base the condition
		*	$config['column_value'] -> The condition 
		*	$config['data'] -> an associative array containing namevalue pairs for the new data.
		*/
		public function normal_update($config){
			$this->db->where($config['column'], $config['column_value']);
			$this->db->update($config['table'], $config['data']);
		}
		
		//gets data in the table with specified fields. This will minimize the query time
		public function fields_get($table,$fields = []){
			//specify the fields.
			$this->db->select($fields);
			
			$query = $this->db->get($table);
			
			return $this->return_data($query);		
		}
		
		/**
		*	This method accepts multiple parameters
		*	$table - table to retrieved data from
		*   $fields - the columns to get in the specified table name
		*	$conditions - parameters to be appended after where clause
		*	$logic - logical operator. the default is OR. However if conditions where not set $logic will not be used
		*	$params - arguments to be binded in ?.
		*
		*	NOTE: There could be codeigniter implementation for this. Nag-mamatalino lang. haha!
		*/
		public function bindable_get($table, $fields = [], $conditions = [], 
			$logic = 'OR', $params = []){
			
			//NOTE: Do not remove spaces on query strings.
			
			$sql = "SELECT";
			
			//specify columns to get when $fields is not empty
			if(!empty($fields)){
				
				//gets the last in order to identify if we are going to stop inserting ,
				$last = end($fields);
				
				foreach($fields as $value){
					if($last !== $value){
						$sql .= " {$value}, ";
					}else{
						$sql .= " {$value} ";
					}		
				}
			}else{
				$sql .= " *";
			}
			
			$sql .= " FROM {$table}";
			
			//specify where conditions if $conditions is not empty
			if(isset($conditions) || !empty($conditions)){
				//get the last item in array to know  if we gonna insert $logic or not
				$last = end($conditions);
			
				$sql .= " WHERE";
				foreach($conditions as $value){
					$sql .= " {$value} = ?";
					//appends logical operator if conditions length is greater than 1
					if(count($conditions) > 1 && $last != $value){
						$sql .= " {$logic}";
					}
				}
			}
			
			$query = $this->db->query($sql, $params);

			
			return $this->return_data($query);	
		}
		
		//returns result set on normal queries.
		public function return_data($query){
			if($query->num_rows > 0){
				foreach($query->result() as $row){
					$data[] = $row;
				}
				return $data;
			}
			return false;
		}
		
		/*=================================================================
		*
		*	Error logging for database operations
		*
		*==================================================================*/
		public function log_db_error($code , $message){
			$this->db_errs['db_err_code'] = $code;
			$this->db_errs['db_err_message'] = $message;
		}
		
		public function get_log_db_errors(){
			return $this->db_errs;
		}	
        
        public function get_rank_acronym($rank){
            preg_match_all('/\b\w/',  $rank, $matches);
            return implode('',$matches[0]);
        }

		//retrieve single or multiple records in database with or without condition
        /*=================================================
        *
        *   $config['table'] => the name of the table
        *   $config['cols'] => If this was not set then we retrieve all columns
        *   $config['limit'] => limit, if bot limit and offset are not set we retrieve all rows.     
        *   $config['offset'] => offset
		*	$config['order_arrange'] => ASC or DESC
        *   $config['conditions'] => associative array of columns and values in which where clause is to be applied.
        * 	$config['distinct'] => TRUE OR FALSE, defaults to TRUE;
		*	$config['like'] => associative arrays that contains LIKE params;
        
        *   If you want to return all row then specify limit as null
        *   and offset as null.
        **=============================================*/
		
        public function special_get($config){
            
            //set like 
            if(isset($config['like'])){
                $splits = $config['like'];
                if( count($splits) === 1){
                    $this->db->like('keywords', $splits[0]);
                } else {
                    $incr = 1;
                    foreach($splits as $split){
                        if($incr === 1){
                            $this->db->like('keywords', $split);
                        } else {
                            $this->db->or_like('keywords', $split);
                        }
                        
                        $incr++;
                    }   
                }
            }
           
		      
            //set distinct base on pass parameters. Default to TRUE
			$config['distinct'] = isset($config['distinct']) ? $config['distinct'] : TRUE;
			if($config['distinct'] === TRUE){
				$this->db->distinct();
			}
            //check if offset and limit is not set
			if(!isset($config['limit'])){
				$config['limit'] = null;
			}
			if(!isset($config['offset'])){
				$config['offset'] = null;
			}
			//check conds
            if(!isset($config['conditions'])){
                if(isset($config['cols'])){
                    $this->db->select($config['cols']);
                }
                //check order
                if(isset($config['order_arrange']) && isset($config['order_by'])){
                    $this->db->order_by($config['order_by'],$config['order_arrange']);
                }
                $query = $this->db->get($config['table'], $config['limit'], $config['offset']);
                 
            }else{
				
                if(isset($config['cols'])){
                    $this->db->select($config['cols']);
                }
				if(isset($config['order_arrange']) && isset($config['order_by'])){
					$this->db->order_by($config['order_by'],$config['order_arrange']);
				}
                //check order
                if(isset($config['order_arrange']) && isset($config['order_by'])){
                    $this->db->order_by($config['order_by'],$config['order_arrange']);
                }
				
                $query = $this->db->get_where($config['table'], $config['conditions'], $config['limit'], $config['offset']);
        			
			}
		             
            return $this->return_data($query);
        }
		
		function special_date_search($table,$start_date,$end_date){
			$this->db->select('*');
			$this->db->from('arrest_details_tbl');
			
			$this->db->where('date_of_arrest >=', $start_date);
			$this->db->where('date_of_arrest <=', $end_date);
			
			$query = $this->db->get();
			
			$res = $this->return_data($query);
			
			var_dump($res);
		}
		/*===========================================================================
		*	Insert or updates 1 record at a time
		*
		*	$config['type'] = "UPDATE or INSERT", if not set or none of the two is supplied 
		*		INSERT operation will do. If UPDATE is supplied then update operation will be done. 
		*	$config['table'] = Table name of course.	
		*   $config['data'] = The data to be inserted
		*	$config['conditions'] = associative array passing all the coditions
        *   $config['use_set'] = TRUE OR FALSE, if TRUE use the set method of CI.
        *   $config['set_params'] = associative array for set() parameters
		*==========================================================================*/
		public function special_save($config){
        
			if(!isset($config['type'])){
				$config['type'] = "INSERT";
			}
			if(strtolower($config['type']) === "insert"){
				$this->db->insert($config['table'],$config['data']);
			}else{
                $this->db->where($config['conditions']);
                if(!isset($config['use_set'])){
				    $this->db->update($config['table'], $config['data']);
                }else{
                    $params = $config['set_params'];
                    $this->db->set($params['param_1'], $params['param_2'], $params['param_3']);
                    $this->db->update($config['table']);
                }
				
			}
			return $this->check();
		}
        
        public function special_delete($config){
            $this->db->where($config['conditions']);
            $this->db->delete($config['table']);
            return $this->check();
        }
        //deletes all date in a particular table
        public function delete_all($table){
            $this->db->empty_table($table); 
        }
		
        public function ihash_mo_baby($str){
            return md5($str);
        }
		//check if an operation is failed
		private function check(){
			return $this->db->trans_status();
		}
        
     
        
        /*===========================================================
        *   
        *            REUSABLE METHODS SHARED IN EACH MODEL
        *
        **===========================================================*/
        
        public function set_query_msg($msg){
            $this->q_msg = $msg;
        
        }
        
        public function get_query_msg(){
            return $this->q_msg;
        }
        
        //gets the current user
        public function ss_user(){
            $pnp_id = $this->session->userdata('username');
        
            $response['username'] = $pnp_id;
            $response['is_admin'] = $this->ion_auth->is_admin();
            
            $info = $this->ss_get_police_info($response['username']);
            return $info;
        }
        
        //gets all columns in case_report_tbl
        public function ss_get_case($case_id){
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            $case = $this->special_get($config)[0];
            
            return $case;
        }
       
        public function ss_get_case_violators($blotter){
            $config['table'] = 'arrest_booking_form_tbl';
            $config['cols'] = array('violator_id');
            $config['conditions'] = array('blotter_entry_nr' => $blotter);
            
            $violators = $this->special_get($config);
            
            foreach($violators as $violator){
    
                $info = $this->ss_get_violator_info($violator->violator_id);
               
                foreach($info as $key => $value){
                     $violator->$key = $value;
                }
        
            }        
            
            return $violators;
            
       }
        
        public function ss_get_violator_info($id){
            $config['table'] = 'violators_tbl';
            $config['cols'] = array('first_name','last_name','alias');
            $config['conditions'] = array('violator_id' => $id);
            
            return $this->special_get($config)[0];
        
        }
        
        public function ss_get_police_info($id){
            $config['table'] = 'pnp_officer_tbl';
            $config['cols'] = array('middle_name','first_name','last_name','rank');
            $config['conditions'] = array('pnp_id' => $id);
            
            return $this->special_get($config)[0];
        }
        
        public function ss_get_case_date($blotter_entry){
            $config['table'] = 'arrest_details_tbl';
            $config['cols'] = array('date_of_arrest');
            $config['conditions'] = array('blotter_entry_nr' => $blotter_entry);
            
            return $this->special_get($config)[0]->date_of_arrest;
        }
        
        public function get_barangay_info($id){
            $config['table'] = 'barangay_tbl';
            $config['conditions'] = array('barangay_id' => $id);
            
            return $this->special_get($config)[0];
        }
        
         public function ss_audit($operation, $description){
            
            $data = array();
            $data['username'] = $this->session->userdata('username');;
            $pnp = $this->ss_get_police_info($data['username']);
            $full = $pnp->first_name . ' ' . $pnp->last_name;
            $data['name'] = $pnp->first_name . ' ' . $pnp->last_name;
            $data['rank'] = $pnp->rank;
            $data['operation'] =  $operation;
            $data['description'] = $full . ' ' . $description;
            
             //call another model
            $CI =& get_instance(); 
            $CI->load->model('audit_trailmod');
           
            $CI->audit_trailmod->add($data);
            
        }
	}