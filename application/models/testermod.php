<?php
	class Testermod extends Ss_model {
	
	
        public function delete(){
           
            $this->delete_all('case_report_tbl');
            $this->delete_all('arrest_details_tbl');
            $this->delete_all('item_seized_tbl');
            $this->delete_all('narcotics_item_seized_tbl');
            $this->delete_all('pre_operation_tbl'); 
          
        }
        public function delete_violators(){
          $this->delete_all('violators_tbl');  
        }
        
        public function delete_attachments(){
            $this->delete_all('coordination_form_tbl'); 
            $this->delete_all('spot_report_tbl');  
            $this->delete_all('pre_operation_tbl');  
        
        }
        public function test_time(){
            $this->load->library('DateTimeHelper');
            
            echo $this->datetimehelper->parseto_db_time('11:30 PM');
            echo $this->datetimehelper->parseto_system_time('23:45:00');
        }
        
        public function get_fingerprints(){
            $config['table'] = 'fingerprint_tbl';
            
             return $this->special_get($config);
        
        }
    }