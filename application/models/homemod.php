<?php
    class Homemod extends Ss_model {
        
        protected $months_cases = array();
        
        
        function count_table($what){
            switch($what){
                case "violators":
                    $table = 'violators_tbl';
                    break;
                case "cases":
                    $table = 'case_report_tbl';
                    break;
                case "pnps":
                    $table = 'pnp_officer_tbl';
                    break;
                default:
                    echo 'Eh putang ina mo magsusupply ka lang ng parameter mali pa?';
            }
            
            return $this->db->count_all($table);
        }
        /*================================================================
        *                  ANNUAL CASE REPORTS
        *===============================================================*/
       
        function get_annual_report(){
            for($i = 1; $i <= 12; $i++){
                $days = cal_days_in_month(CAL_GREGORIAN,$i,$this->get_year());
                $start_date = $this->get_year() . '-' . str_pad($i,2,'0',STR_PAD_LEFT) . '-' . '1';
                $end_date = $this->get_year() . '-' . str_pad($i,2,'0',STR_PAD_LEFT) . '-' . $days;
                $this->get_annual_number_of_cases($start_date, $end_date);
            }
            return $this->months_cases;
        }
        
        function get_annual_number_of_cases($start_date, $end_date){
            //echo $start_date .  " "  . $end_date . "\n";
            list($day, $month_num, $yr) = explode("-",$start_date);
            $config['table'] = 'arrest_details_tbl';
			$config['cols'] = array('blotter_entry_nr');
			$config['conditions'] = array(
				'date_of_arrest >=' => $start_date,
				'date_of_arrest <=' => $end_date,
			);
		
			$blotters = $this->special_get($config);
            if($blotters){
                $dt = DateTime::createFromFormat('!m', $month_num);
                $month = $dt->format('F');
                $count = count($blotters);
                $this->months_cases[$month] = $count;  
            }else{
                $dt = DateTime::createFromFormat('!m', $month_num);
                $month = $dt->format('F');
                $count = 0;
                $this->months_cases[$month] = $count;
            }  
        }
        
        
        function get_monthly_report(){
            $this->load->library('DateTimeHelper');
            $year = $this->datetimehelper->get_current_year();
            $month = $this->datetimehelper->get_current_month();
            $days = $this->datetimehelper->get_num_days($month, $year);
            
            $result = new stdClass ();
            
            for($i = 1; $i <= $days; $i++){
                $date = $this->get_year() . '-' . str_pad($month,2,'0',STR_PAD_LEFT) . '-' . $i;
                $day = 'Day ' . $i;
                $result->$day = $this->get_monthly_number_of_cases($date);
            }
            
           return $result;
            
        }
        function get_monthly_number_of_cases($date){
            $config['table'] = 'arrest_details_tbl';
			$config['cols'] = array('blotter_entry_nr');
			$config['conditions'] = array(
				'date_of_arrest' => $date,
			);
               
            $data = $this->special_get($config);
            
            if($data){
                $count = count($data);
            } else {
                $count = 0;
            }
             
            return $count;
        }
        
        
            
        private function get_year(){
            return date("Y");
        }

    }