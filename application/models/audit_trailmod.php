<?php
	class Audit_trailmod extends Ss_model {
        
        
        function add($data){
            $row = array(
                'pnp_id' => $data['username'],
                'name' => $data['name'],
                'rank' => $data['rank'],
                'operation' => $data['operation'],
                'description' => $data['description'],
                'date' => $this->getDate(),
                'time' => $this->getTime(),
                   
            );
            $config['table'] = 'audit_tbl';
            $config['data'] = $row;
        
            $result = $this->special_save($config);
           
            return $result;
        }
        
        
        function retrieve(){
            $config['table'] = 'audit_tbl';
            $config['order_arrange'] = 'DESC';
            $config['order_by'] = 'date';
            $results =$this->special_get($config);
            $this->load->library('DateTimeHelper');
            if($results){
              foreach($results as $result){
                $result->time = $this->datetimehelper->parseto_system_time($result->time);
              }
            }
          
            
          
            return $results;
        
        }
        
        function getTime(){
            date_default_timezone_set('Asia/Manila');
            $dt = new DateTime();
            return $dt->format("H:i:s");
        }
        
        function getDate(){
            return date("Y-m-d");
        }
        
}