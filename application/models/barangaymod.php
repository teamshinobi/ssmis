<?php
	class Barangaymod extends Ss_model {
		
		//$array = columns that you want to get in the database
		public function get_all_barangay(){
			return $this->normal_get('barangay_tbl');
		}
		public function get_barangay($id){
			$params = array(
			
				'barangay_id' => $id,
			
			);
			$config = array(
			
				'table' => 'barangay_tbl',
				'params' => $params,
				'limit' => 1,
				'offset' => 0
			);
			
			$data = $this->param_get($config);

			return $data;
		
		}
		public function remove_barangay($id){
			
			$this->db->trans_begin();
		
			$params = array(
				
				'barangay_id' => $id,
			
			);
			$config = array(
				
				'table' => 'barangay_tbl',
				'params' => $params,
			
			
			);
			
			$this->normal_delete($config);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correcommit transaction then upload image
				$this->db->trans_commit();	
				
				return true;
			}
		}
				
		
	    /*public function insert($data){
	        $to_return = new stdClass();
	        $this->db->trans_begin();
	        
	        $config['table'] = 'barangay_tbl';
	       // $data['image_path'] = 'public\resources\photos\vehicles';
	        $config['data'] = $data;
	        
	        $this->special_save($config);
	   
	        if($this->db->trans_status() === FALSE){
	            $array['error_message'] = $this->db->_error_message();
	            $array['error_number'] = $this->db->_error_number();

	            $this->db->trans_rollback();
	            
	            $to_return->success = FALSE;
	            $to_return->db_err = $array;
	        }else{  
	            $to_return->success = TRUE;
	            $to_return->next_id = $this->next_barangay_id($data['barangay_id']);
	          //  $this->upload_photo($data['vehicle_id']);
	            $this->db->trans_commit(); 
	        }
	            
	        return $to_return;
	    }*/
	    public function insert_barangay($data){
			$this->db->trans_begin();

			$this->db->insert('barangay_tbl', $data);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
			
				return true;
			}
			
		}


		public function update_barangay($data){
			$this->db->trans_begin();

			$config = array(
				'table' => 'barangay_tbl',
				'column' => 'barangay_id',
				'column_value' => $data['barangay_id'],
				'data' => $data,
			);

			$this->normal_update($config);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
			
				return true;
			}
		}
		
		public function barangay_id() {
        $config = array(
            'strategy' => 'DB_INCREMENT',
            'prefix' => 'BARANGAY-2014', 
        );

        //get the last row in database.
        $query_config = array(
            'table' => 'barangay_tbl',
            'column' => 'barangay_id',
            'order' => 'desc',
            'limit' => 1,
            'start' => 0,
            'fields' => array('barangay_id'),
        );

        //returns array of objects. In this case 1 object only
        $data = $this->limited_order_get($query_config);
        //this will be the basis of our DB_INCREMENT strategy		
        $last_id = "";

        if(empty($data)){
            $last_id = false;
        }else{
            $last_id = $data[0]->barangay_id;
        }

        $this->load->library('Incrementor', $config);			

        return $this->incrementor->increment($last_id);

    }

		/*==========================================
			
						Reporting
			
		==========================================*/
		
		public function report_all(){
		
			$this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			
			$arrayName = array('Barangay ID', ' Name', 'Incidents', 'Category', 'Chairman');
			$arrayCol = array(55,25,25,25,55);
			
			$this->pdfgenerator->DefOrientation = 'P';
			$this->pdfgenerator->AliasNbPages();
			$this->pdfgenerator->AddPage();
			$this->pdfgenerator->createTableHeader($arrayName,$arrayCol);
			
			
			$this->pdfgenerator->Ln();
			
			$data = $this->get_all_barangay();
			$this->pdfgenerator->SetFont('Arial','',10);
			
			foreach($data as $barangay){
				for($i=0; $i <= 4; $i++){
					switch($i){
						case 0:
							$this->pdfgenerator->Cell($arrayCol[0],10 * $this->pdfgenerator->WordWrap($barangay->barangay_id,95),
								$barangay->barangay_id,1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell($arrayCol[1],10 * $this->pdfgenerator->WordWrap($barangay->barangay_name,95),
								$barangay->barangay_name,1,0,'C');
							break;
						case 2:
							$this->pdfgenerator->Cell($arrayCol[2],10 * $this->pdfgenerator->WordWrap($barangay->barangay_incidents,95),
								$barangay->barangay_incidents ,1 ,0, 'C');
							break;
						case 3:
							$this->pdfgenerator->Cell($arrayCol[3],10 * $this->pdfgenerator->WordWrap($barangay->barangay_category,95),
								$barangay->barangay_category ,1 ,0, 'C');
							break;
						case 4:
							$this->pdfgenerator->MultiCell($arrayCol[4],10,$barangay->barangay_chairman,1 ,'C', 0);
							break;
							
					}
				}	
				
			} 
			
			$this->pdfgenerator->Output();
		}
	}