<?php
	class Settingsmod extends Ss_model {
        
        public function get_templates(){
            $config['table'] = 'templates_tbl';
            $config['conditions'] = array('id' => '1');
            
            $result = $this->special_get($config)[0];
            
            return $result;
        }
        
        
        public function update_templates($data){
            $to_return = true;
			$this->db->trans_begin();
            
            
            $config['table'] = 'templates_tbl';
            $config['conditions'] = array('id' => '1');
            $config['data'] = $data;
            $config['type'] = 'UPDATE';
            
            $res = $this->special_save($config);
            if($this->db->trans_status() === FALSE){
				$array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
				
				$this->db->trans_rollback();
                
                $to_return = $array;
			}else{	
                
                $this->db->trans_commit();
				
			}		
			return $to_return;
        }
        
        public function start_backup(){
        
            // Load the DB utility class
            $this->load->dbutil();
            
            $prefs = array(
                'tables'      => array(''),  // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'zip',             // gzip, zip, txt
                'filename'    => 'ssmis.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );

            

            // Backup your entire database and assign it to a variable
            $backup =& $this->dbutil->backup($prefs); 

            // Load the file helper and write the file to your server
            $this->load->helper('file');
            $dir =  realpath(APPPATH . '../public/backup');
            write_file($dir . '/ssmis.sql', $backup); 

            // Load the download helper and send the file to your desktop
            $this->load->helper('download');
            force_download('ssmis.sql', $backup);
        }
        
        public function get_users(){
            
            $this->load->library("DateTimeHelper");
            
            $config['table'] = 'users';
            $results = $this->special_get($config);
            
            foreach($results as $res){
                $res->pnp_info = $this->ss_get_police_info($res->username);
                $res->created_on = $this->datetimehelper->parse_timestamp($res->created_on);
            }
            
            return $results;
        }
        public function set_active($id){
            $data = array(
                'active' => 1,
            );
            $res = $this->ion_auth->update($id, $data);
            
            return $res;
        }
        
        public function set_inactive($id){
            $data = array(
                'active' => 0,
            );
            $res = $this->ion_auth->update($id, $data);
            
            return $res;
        }
         
        public function reset_password($id){
            $mix = array();
            
            $random = $this->generateRandomString();
            
            $mix['new_pass'] = $random;
            
            $data = array(
                'password' => $random,
            );
            $res = $this->ion_auth->update($id, $data);
            
            $mix['result'] = $res;
            
            return $mix;
        }
        
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
        
        
        /*=============================================================================
        *
        *                            CONTROL
        *
        *============================================================================*/
        
        public function get_controls(){
            $config['table'] = 'control_tbl';
            $config['conditions'] = array('id' => '1');
            
            $result = $this->special_get($config)[0];
            
            return $result;
        }
        
        public function update_control($control, $value){
              
            $config['table'] = 'control_tbl';
            $config['conditions'] = array('id' => 1);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => $control,
                'param_2' => $value,
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';

            $result = $this->special_save($config);

            return $result;
       
        }  
        
        /*=============================================================================
        *
        *                                STATION
        *
        *============================================================================*/
        
        public function update_station($data){
            
            $config['table'] = 'station_settings_tbl';
            $config['conditions'] = array('id' => 1);
            $config['type'] = 'UPDATE';
            $config['data'] = $data;

            $result = $this->special_save($config);

            return $result;
        }
        
        public function get_station_information(){
            $config['table'] = 'station_settings_tbl';
            $config['conditions'] = array('id' => '1');
            
            $result = $this->special_get($config)[0];
            
            return $result;
        
        }
        
}