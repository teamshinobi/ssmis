<?php
	class Narcoticsmod extends Ss_model {
	
		public function get_all_narcotics(){
			return $this->normal_get('narcotics_tbl');
			
		}
		public function get_narcotics($id){
			$params = array(
			
				'narcotics_id' => $id,
			
			);
			$config = array(
			
				'table' => 'narcotics_tbl',
				'params' => $params,
				'limit' => 1,
				'offset' => 0
			);
			
			$data = $this->param_get($config);

			return $data;
		
		}
		public function get_id(){
        $config = array(
            'strategy' => 'DB_INCREMENT',
            'prefix' => 'NARCOTICS', 
        );

        //get the last row in database.
        $query_config = array(
            'table' => 'narcotics_tbl',
            'column' => 'narcotics_id',
            'order' => 'desc',
            'limit' => 1,
            'start' => 0,
            'fields' => array('narcotics_id')
        );

        //returns array of objects. In this case 1 object only
        $data = $this->limited_order_get($query_config);
        //this will be the basis of our DB_INCREMENT strategy		
        $id = "";

        if(empty($data)){
            $id = false;
        }else{
            $id = $data[0]->narcotics_id;
        }

		//$this->load->library('Incrementor', $config);			

        return $id ;//$this->incrementor->increment($last_id);
    
		}
		public function insert_narcotics($data){
			$this->db->trans_begin();

			$this->db->insert('narcotics_tbl', $data);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
			
				return true;
			}
			
		}
		public function insert($data){
        $to_return = new stdClass();
        $this->db->trans_begin();
        
        $config['table'] = 'narcotics_tbl';
       // $data['image_path'] = 'public\resources\photos\vehicles';
        $config['data'] = $data;
        
        $this->special_save($config);
   
        if($this->db->trans_status() === FALSE){
            $array['error_message'] = $this->db->_error_message();
            $array['error_number'] = $this->db->_error_number();

            $this->db->trans_rollback();
            
            $to_return->success = FALSE;
            $to_return->db_err = $array;
        }else{  
            $to_return->success = TRUE;
            $to_return->next_id = $this->next_narcotics_id($data['narcotics_id']);
          //  $this->upload_photo($data['vehicle_id']);
            $this->db->trans_commit(); 
        }
            
        return $to_return;
    }
		
		public function narcotics_id() {
        $config = array(
            'strategy' => 'DB_INCREMENT',
            'prefix' => 'NARCOTICS', 
        );

        //get the last row in database.
        $query_config = array(
            'table' => 'narcotics_tbl',
            'column' => 'narcotics_id',
            'order' => 'desc',
            'limit' => 1,
            'start' => 0,
            'fields' => array('narcotics_id')
        );

        //returns array of objects. In this case 1 object only
        $data = $this->limited_order_get($query_config);
        //this will be the basis of our DB_INCREMENT strategy		
        $last_id = "";

        if(empty($data)){
            $last_id = false;
        }else{
            $last_id = $data[0]->narcotics_id;
        }

		$this->load->library('Incrementor', $config);			

        return $this->incrementor->increment($last_id);
    }
    public function update($data){
 		$this->db->trans_begin();

			$config = array(
				'table' => 'narcotics_tbl',
				'column' => 'narcotics_id',
				'column_value' => $data['narcotics_id'],
				'data' => $data,
			);

			$this->normal_update($config);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
			
				return true;
			}
	}
    	public function remove_narcotic($id){
			
			$this->db->trans_begin();
		
			$params = array(
				
				'narcotics_id' => $id,
			
			);
			$config = array(
				
				'table' => 'narcotics_tbl',
				'params' => $params,
			
			
			);
			
			$this->normal_delete($config);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correcommit transaction then upload image
				$this->db->trans_commit();	
				
				return true;
			}
		}
				
		
		/*==========================================
			
						Reporting
			
		==========================================*/
		
		public function report_all(){
		
			$this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			
			$arrayName = array('Narcotics ID', 'Name', 'Unit', 'Description');
			$arrayCol = array(35,35,25,100);
			
			$this->pdfgenerator->DefOrientation = 'P';
			$this->pdfgenerator->AliasNbPages();
			$this->pdfgenerator->AddPage();
			$this->pdfgenerator->createTableHeader($arrayName, $arrayCol);
			
			
			$this->pdfgenerator->Ln();
			
			$data = $this->get_all_narcotics();
			$this->pdfgenerator->SetFont('Arial','',10);
			
			foreach($data as $narcotics){
				for($i=0; $i <= 4; $i++){
					switch($i){
						case 0:
							$this->pdfgenerator->Cell($arrayCol[0],10 * $this->pdfgenerator->WordWrap($narcotics->description,95),
								$narcotics->narcotics_id ,1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell($arrayCol[1],10 * $this->pdfgenerator->WordWrap($narcotics->description,95),
								$narcotics->name,1,0,'C');
							break;
						case 2:
							$this->pdfgenerator->Cell($arrayCol[2],10 * $this->pdfgenerator->WordWrap($narcotics->description,95),
								$narcotics->unit ,1 ,0, 'C');
							break;
						case 3:
							$this->pdfgenerator->MultiCell($arrayCol[3],10,$narcotics->description,1,2);
							break;
					}
				}	
				
			} 
			
			$this->pdfgenerator->Output();
		}
		
	}
