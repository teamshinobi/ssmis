<?php
	class Casereportmod extends Ss_model {
	   
		
        public function delete_distribution($id){
            $config['table'] = 'distribution_tbl';
            $config['conditions'] = array(
                'id' => $id,
            );
            
            $result = $this->special_delete($config);
            
            return $result;
        }
        
        public function edit_distribution($data){
            $config['table'] = 'distribution_tbl';
          
            $row = array(
                
                'copies' => $data['copies'],
                'distributed_to' => $data['distributed_to'],
                'case_id' => $data['case_id'],
                'when' => $data['when'],
            );
            
            
            $config['data'] = $row;
            $config['type'] = 'UPDATE';
            $config['conditions'] = array(
                'case_id' => $data['case_id'],
            );
            
            $result = $this->special_save($config);
            
            return $result;
        }
        
        public function get_distributions($case_id){
            $config['table'] = 'distribution_tbl';
            $config['conditions'] = array(
                'case_id' => $case_id,
            );
            
            $results = $this->special_get($config);
            
            return $results;
        }
        public function distribute($data){
            $config['table'] = 'distribution_tbl';
            $config['data'] = $data;
            
            $result = $this->special_save($config);
            
            return $result;
        }
        
        /*==========================================================
		*
		*				ARRESTDETAILS DATABASE OPERATIONS
		*
		*===========================================================*/
        public function check_blotter($blotter){
            $config['table'] = 'arrest_details_tbl';
            $config['cols'] = 'blotter_entry_nr';
            $config['conditions'] = array(
                'blotter_entry_nr' => $blotter,
            );
            
            $res = $this->special_get($config);
            
            if($res){
                return true;
            } else {
                return false;
            }
        }
        
		//gets the list of all active pnps only
		public function get_active_pnps(){
			$params = array(
			
				'status' => 'Active',
			
			);
			
			$config = array(
			
				'table' => 'pnp_officer_tbl',
				'params' => $params,
				'limit' => null,
				'offset' => null
			);
			
			$data = $this->param_get($config);
			
			return $data;
		}
        
        public function get_active_investigators(){
            $this->db->select('*');
			$this->db->from('group_members_tbl');
			$this->db->join('pnp_officer_tbl', 'group_members_tbl.pnp_id = pnp_officer_tbl.pnp_id');
			$this->db->where('group_id', 'GROUP-0001');
			$this->db->where('pnp_officer_tbl.status', 'active');
			
            $query = $this->db->get();
				
			return $this->return_data($query);
        }
        
		public function get_case_id(){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'SJ-CASE',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'case_report_tbl',
				'column' => 'case_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('case_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->case_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
			
		}
        
        //move the default picture to temp
        public function create_document_folders($id){
            $this->load->library('FileManager');
            $this->filemanager->create_default_scene($id);
            $this->filemanager->create_document_folders($id);	
        }
        
        public function remove_temp_doc_dir($case_id, $folder){
            
            $this->load->library('FileManager');
            
            $this->filemanager->remove_docs_in_temp($case_id,$folder);
        }
        
        public function scan_permanent_dir($case_id){
            $lab_path = 'public\\resources\\photos\\case\\' . $case_id . '\\laboratory';
            $medical_path = 'public\\resources\\photos\\case\\' . $case_id . '\\medical';
            $search_path = 'public\\resources\\photos\\case\\' . $case_id . '\\search';
            $warrant_path = 'public\\resources\\photos\\case\\' . $case_id . '\\warrant';
            $others_path = 'public\\resources\\photos\\case\\' . $case_id . '\\others';
            if(!file_exists($lab_path)){
                mkdir($lab_path);
            }
            
            if(!file_exists($medical_path)){
                mkdir($medical_path);
            }
            if(!file_exists($search_path)){
                mkdir($search_path);
            }
            if(!file_exists($warrant_path)){
                mkdir($warrant_path);
            }
                
            if(!file_exists($others_path)){
                mkdir($others_path);
            }
            
            $lab_files = scandir($lab_path);
            $medical_files = scandir($medical_path);
            $search_files = scandir($search_path);
            $warrant_files = scandir($warrant_path);
            $others_files = scandir($others_path);
            
            $files['laboratory'] = $lab_files;
            $files['medical'] = $medical_files;
            $files['search'] = $search_files;
            $files['warrant'] = $warrant_files;
            $files['others'] = $others_files;
    
            
            return $files;
        }
        public function scan_temp_dir($case_id){
            //lab files
            $lab_path = 'public\\resources\\temps\\case\\' . $case_id . '\\laboratory';
            $medical_path = 'public\\resources\\temps\\case\\' . $case_id . '\\medical';
            $search_path = 'public\\resources\\temps\\case\\' . $case_id . '\\search';
            $warrant_path = 'public\\resources\\temps\\case\\' . $case_id . '\\warrant';
            $others_path = 'public\\resources\\temps\\case\\' . $case_id . '\\others';
            
            if(!file_exists($lab_path)){
                mkdir($lab_path);
            }
            if(!file_exists($medical_path)){
                mkdir($medical_path);
            }
            if(!file_exists($search_path)){
                mkdir($search_path);
            }
            if(!file_exists($warrant_path)){
                mkdir($warrant_path);
            }
            
            if(!file_exists($others_path)){
                mkdir($others_path);
            }
            
            $lab_files = scandir($lab_path);
            $medical_files = scandir($medical_path);
            $search_files = scandir($search_path);
            $warrant_files = scandir($warrant_path);
            $others_files = scandir($others_path);
            
            $files['laboratory'] = $lab_files;
            $files['medical'] = $medical_files;
            $files['search'] = $search_files;
            $files['warrant'] = $warrant_files;
            $files['others'] = $others_files;
            
            return $files;
        }
        
        public function upload_scene($id){
            $this->load->library('FileManager');
            $this->filemanager->upload_scene($id);	
        }
        
        public function upload_laboratory($case_id){
            $this->load->library('FileManager');
            return $this->filemanager->upload_documents($case_id,'laboratory','laboratory');	
        }
        
        public function upload_medical($case_id){
            $this->load->library('FileManager');
            return $this->filemanager->upload_documents($case_id,'medical','medical');	
        }
        
        public function upload_search($case_id){
            $this->load->library('FileManager');
            return $this->filemanager->upload_documents($case_id,'search','search');	
        }
        
        public function upload_warrant($case_id){
            $this->load->library('FileManager');
            return $this->filemanager->upload_documents($case_id,'warrant','warrant');	
        }
        
        public function upload_others($case_id){
            $this->load->library('FileManager');
            return $this->filemanager->upload_documents($case_id,'others','others');	
        }
        
        
		/*==========================================================
		*
		*				VIOLATORDETAILS DATABASE OPERATIONS
		*
		*===========================================================*/
		public function upload_temp($data){
			/*$this->load->library('FileManager');
			$tags = array('front','rear','right','left');
			$behavior = array(
				'use_default' => true,
				'make_dir' => true,
				'sub_folder' => $data['violator_id'],
			);
			$this->filemanager->upload_to_temp($data['violator_id'],'violator', $tags, $behavior);*/
            
            $this->load->library('FileHelper');	
            $uploadNames = array('front','rear','right','left');
            $this->filehelper->useTemp();
            $this->filehelper->useThumb();
            $this->filehelper->uploadFiles($data['violator_id'], 'violator/' . $data['violator_id'], $uploadNames);
        }
        
		public function update_existing_violator_image($data){
			$this->load->library('FileManager');
			$tags = array('front','rear','right','left');
			$behavior = array(
				'use_default' => false,
				//I set make_dir to true, basta para maievaluate putang ina. hahaha!
				'make_dir' => true,
				'sub_folder' => $data['violator_id'],
			);
			$this->filemanager->upload_to_temp($data['violator_id'],'violator', $tags, $behavior);	
		}
		public function get_first_violator_id(){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'SJ-VIOL',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'violators_tbl',
				'column' => 'violator_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('violator_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->violator_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
			
		}
		public function get_violator_next_id($prev_id){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'SJ-VIOL',
				'includes_year' => true
			);
			$this->load->library('Incrementor', $config);
			
			return $this->incrementor->increment($prev_id);
						
		}
		public function get_all_violators(){
			$params = array(
			
				'remarks' => 'On-Jail',
			
			);
			
			$config = array(
			
				'table' => 'violators_tbl',
				'params' => $params,
				'limit' => null,
				'offset' => null
			);
			
			$data = $this->param_get($config);
			
			return $data;
		}
		
		public function get_violator($id){
			$params = array(
			
				'violator_id' => $id,
			
			);
			
			$config = array(
			
				'table' => 'violators_tbl',
				'params' => $params,
				'limit' => 1,
				'offset' => 0
			);
			
            
			$data = $this->param_get($config);
			
            foreach($data as $v){
                $info = $this->get_violator_ids($v->violator_id);
                if($info){
                    foreach($info as $key => $value){
                         $v->$key = $value;
                    }
                }
               
            }
			
            
            
            return $data;
		}
        
        public function get_violator_ids($id){
            $config['table'] = 'ids_tbl';
            $config['conditions'] = array('violator_id' => $id);
            
            return $this->special_get($config)[0];     
        }
		
		public function copy_permanent_to_temp($id){
			$this->load->library('FileManager');
			
			$this->filemanager->copy_permanent_to_temp('violator',$id);	
			
			return true;
			
		}
        
        public function remove_temp($id){
         
            $this->load->library('FileManager');
			
            //theres no way to know kung naremove ba ung photos or what. Tingin ko meron, pero wag muna. Thesis eh.
            $dir = realpath(APPPATH . '../public/resources/temps/violator/' . $id);
            
           
			$this->filemanager->rrmdir($dir);	
            
        
            return true;
        }
		/*==========================================================
		*
		*			INVENTORY SECTION DATABASE OPERATIONS
		*
		*===========================================================*/
		public function inventoryid(){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'INVENTORY',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'inventory_tbl',
				'column' => 'inventory_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('inventory_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->inventory_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
		}
		
		public function itemfirstid(){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'ITEM',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'item_seized_tbl',
				'column' => 'item_seized_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('item_seized_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->item_seized_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
		}
		
		public function narcoticsitemfirstid(){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'NITEM',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'narcotics_item_seized_tbl',
				'column' => 'narcotics_seized_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('narcotics_seized_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->narcotics_seized_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
		}
	
		public function upload_temp_item($data){
			$this->load->library('FileManager');
			$tags = array('item');
			$behavior = array(
				'use_default' => true,
				'make_dir' => true,
				'sub_folder' => $data['case_id'],
				'default_pic' => 'default_img.jpg',
			);
			$this->filemanager->upload_to_temp($data['item_id'],'inventory', $tags, $behavior);	
		}
		
		public function item_next_id($prev_id){
			//supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'ITEM',
				'includes_year' => true
			);
			$this->load->library('Incrementor', $config);
			
			return $this->incrementor->increment($prev_id);				
		}
		public function nitem_next_id($prev_id){
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'NITEM',
				'includes_year' => true
			);
			$this->load->library('Incrementor', $config);
			
			return $this->incrementor->increment($prev_id);		
		}
		/*==========================================================
		*
		*		CASE REPORT SUBMISSIONS DATABASE OPERATIONS
		*
		*===========================================================*/
		public function submit($data){
			//super palpak na code, pwede namang isang if na lang. LETSE! HAHAHA! Ayusin to!
			/*$to_return;
			
			$this->db->trans_begin();
			
			$this->insert_arrest_details($data);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$to_return = $this->return_error_message();
				
			} else {
	
				$this->insert_arresting_officers($data);
				
				if($this->db->trans_status() === false){
					
					$to_return = $this->return_error_message();
				
				} else { 
					$folders = $this->insert_violators($data);			
					if($this->db->trans_status() === false){
						$to_return = $this->return_error_message();
					
					}else{
						$this->insert_arrest_booking_form($data);
						if($this->db->trans_status() === false){
							$to_return = $this->return_error_message();
						} else { 
							$this->insert_inventory(json_decode($data['inventory']));
							if($this->db->trans_status() === false){
								$to_return = $this->return_error_message();
							}else{
								$this->insert_items(json_decode($data['items']));			
								if($this->db->trans_status() === false){
									$to_return = $this->return_error_message();
								}else{
									$this->insert_violator_items(json_decode($data['items']));					
									$img_success = $this->move_violators_images($folders);
									$img_success = $this->move_items_images(json_decode($data['inventory']));
									$this->db->trans_commit();
									$to_return = true;
								}		
							}										
						}	
					}	
				}
			} */
			
            $to_return = true;
			//ultra code refactor kumapara dun sa una na nasa taas. Mehehehe.
            $has_duplicate = $this->check_blotter($data['blotter_entry_nr']);
            if($has_duplicate){
                $array['error_message'] = 'Blotter entry has duplicate.';
                $array['error_number'] = '15';
            
                $to_return = $array;
            } else {
                $this->db->trans_begin();
			
                $this->insert_arrest_details($data);
                $this->update_barangay($data['barangay_id']);
                $this->insert_arresting_officers($data);
                $folders = $this->insert_violators($data);	
                $this->insert_arrest_booking_form($data);	
                $this->insert_case_report($data);
                $this->insert_seizing_officers($data['case_id'],json_decode($data['seizing_officers']));
                $this->insert_items(json_decode($data['items']));
                $this->insert_nitems(json_decode($data['nitems']));	
                $this->insert_violator_items(json_decode($data['items']));	
                $this->insert_violator_nitems(json_decode($data['nitems']));
                $this->insert_witnesses(json_decode($data['witnesses']), $data['case_id']);
                $this->create_case_keywords($data);
                $this->create_violators_keywords($data);

                if($this->db->trans_status() === FALSE){
                    $array['error_message'] = $this->db->_error_message();
                    $array['error_number'] = $this->db->_error_number();

                    $this->db->trans_rollback();

                    $to_return = $array;
                }else{	
                    $this->db->trans_commit();
                    try{
                        $img_success = $this->move_violators_images($folders);
                        $img_success = $this->move_items_images($data['case_id']);
                        $img_success = $this->move_scene_images($data['case_id']);
                    }catch(Exception $e){
                        //KAHIT MAG-ERROR ka pa impakto ka!
                        $to_return = true;
                    }	
                }	
            }
			
			return $to_return;
		}
		
        private function move_scene_images($case_id){
            $this->load->library('FileManager');
			
            $bool =  $this->filemanager->move_scene_permanent($case_id);	
			return $bool;
        }
		private function insert_case_report($data){
			$case['case_id'] = $data['case_id'];
			$case['is_number'] = $data['is_number'];
			$case['blotter_entry_nr'] = $data['blotter_entry_nr'];
			$case['nature'] = $data['nature'];
			$case['progress'] = $data['progress'];
			$case['doc_path'] = 'public/resources/photos/case/' . $data['case_id'] . '/';
			$case['operation_name'] = $data['operation'];
			$case['summary'] = $data['summary'];	
            
            //spot report
            if($data['spot_report_id'] != "NONE"){
                $case['spot_report_id'] = $data['spot_report_id'];
                $this->update_spotreport($data['spot_report_id'], $data['case_id']);
            } else {
                $case['spot_report_id'] = $data['spot_report_id'];
            }
            
            //preoperation rreport
            if($data['pre_op_id'] != "NONE"){
                $case['pre_op_id'] = $data['pre_op_id'];
                $this->update_preoperation($data['pre_op_id'], $data['case_id']);
            } else {
                $case['pre_op_id'] = $data['pre_op_id'];
            }
            
            //coordination report
            if($data['coordination_id'] != "NONE"){
                $case['coordination_id'] = $data['coordination_id'];
                $this->update_coordination($data['coordination_id'], $data['case_id']);
            } else {
                $case['coordination_id'] = $data['coordination_id'];
            }
              
            //main affidavit
           
            if($data['main_id'] != "NONE"){
                $case['main_affidavit_id'] = $data['main_id'];
                $this->update_affidavit($data['main_id'], $data['case_id']);
            } else {
                $case['main_affidavit_id'] = $data['main_id'];
            }
            
            if($data['supp_id'] != "NONE"){
                $case['supp_affidavit_id'] = $data['supp_id'];
                $this->update_affidavit($data['supp_id'], $data['case_id']);
            } else {
                $case['supp_affidavit_id'] = $data['supp_id'];
            }
            
			$config['table'] = "case_report_tbl";
			$config['data'] = $case;
            
			$this->special_save($config);
		}
        
        private function insert_witnesses($witnesses, $case_id){
            $config['table'] = 'witness_tbl';
            foreach($witnesses as $obj){
                $row = array(
                    'case_id' => $case_id,
                    'name' => $obj->name,
                    'address' => $obj->address,
                    'contact' => $obj->contact,
                );  
                $config['data'] = $row;
                $this->special_save($config);
            }
        }
        
        private function insert_seizing_officers($case_id, $aoSeizingOfficers){
            foreach($aoSeizingOfficers as $obj){
                $row = array(
                    'case_id' => $case_id,
                    'seizing_officer_id' => $obj->pnp_id,
                );  
                $config['table'] = 'seizing_officers_tbl';
                $config['data'] = $row;
                $this->special_save($config);
            }
        }
		//this will also move narcotics_images I dont know why. HAHAHA!
		private function move_items_images($case_id){
			//NOTE we only have 1 folder to move the INVENTORY folder in which the dir name is the inventory id.
			$this->load->library('FileManager');
			$behavior['tags'] = array('item');
			return $this->filemanager->move_temp_to_permanent('inventory',array($case_id), $behavior);	
		}
		private function insert_violator_nitems($items){
			foreach($items as $item){
				$violators = $item->violators;
				foreach($violators as $violator){
					
					$row = array(
						'narcotics_seized_id' => $item->item_id,
						'violator_id' =>  $violator->violator_id,
					);
				
					$this->normal_insert('violator_narcotics_item_tbl',$row);
				}
			}
		}
		private function insert_violator_items($items){
			foreach($items as $item){
				$violators = $item->violators;
				foreach($violators as $violator){
					
					$row = array(
						'item_seized_id' => $item->item_id,
						'violator_id' =>  $violator->violator_id,
					);
				
					$this->normal_insert('violator_item_tbl',$row);
				}
			}
		}
		private function insert_nitems($items){
			foreach($items as $item){
				$narcotics = $item->narcotics;
			
				$row['narcotics_seized_id'] = $item->item_id;
				$row['case_id'] = $item->case_id;
				$row['narcotics_id'] = $narcotics->narcotics_id;
				$row['quantity'] = $item->quantity;
				$row['markings'] = $item->markings;
                //note this could trigger an error
				$row['is_in_chain_custody'] = $item->is_chain_custody;
				$row['file_path'] = $item->file_path;
				
				$this->normal_insert('narcotics_item_seized_tbl',$row);
			}
		}
		private function insert_items($items){
			
			foreach($items as $item){
				
				$row['item_seized_id'] = $item->item_id;
				$row['case_id'] = $item->case_id;
				$row['quantity'] = $item->quantity;
				$row['description'] = $item->description;
				$row['markings'] = $item->markings;
				$row['is_chain_custody'] = $item->is_chain_custody;
				$row['file_path'] = $item->file_path;
				
				$this->normal_insert('item_seized_tbl',$row);
			
			}
		}
		
		private function move_violators_images($folders){
			$this->load->library('FileManager');
			$behavior['tags'] = array('front','rear','right','left');
			return $this->filemanager->move_temp_to_permanent('violator',$folders, $behavior);	
		}
        
		private function insert_arrest_booking_form($data){
			$violators = json_decode($data['violators']);
			
			foreach($violators as $violator){
				
				$charges = $violator->charges;
				
				foreach($charges as $charge){
					$row = array(
						'blotter_entry_nr' => $data['blotter_entry_nr'],
						'violator_id' => $violator->violator_id,
						'charge_id' => $charge,
					);
					
					$this->normal_insert('arrest_booking_form_tbl',$row);
				}	
			}
		}
		//returns folder names to be used when moving temporary folders to permanent
		private function insert_violators($data){
			$violators = json_decode($data['violators']);
			$this->load->library('DateTimeHelper');	
			$folders = array();
			
			foreach($violators as $violator){
				$parsed_date =$this->datetimehelper->parseto_db_date($violator->birth_date);
				$violator->birth_date = $parsed_date;
                $this->insert_ids($violator);
                
				if($violator->is_existing){
					$this->unset_unnecessary_props($violator);
					
					$config = array(
						'table' => 'violators_tbl',
						'column' => 'violator_id',
						'column_value' => $violator->violator_id,
						'data' => $violator,	
					);
					
					$this->normal_update($config);
				}else{
					//unset unncessary properties
					$this->unset_unnecessary_props($violator);
					$this->normal_insert('violators_tbl',$violator);       
				}
				$folders[] = $violator->violator_id;
			}
			return $folders;
		}
        
        private function insert_ids($violator){
          
            $row = array(
                'violator_id' => $violator->violator_id,
                'dr_license' => $violator->dr_license,
                'dr_issued_at' => $violator->dr_issued_at, 
                'dr_issued_on' =>  $violator->dr_issued_on,
                'res_cert' => $violator->res_cert,
                'res_cert_issued_at' => $violator->res_cert_issued_at,
                'res_cert_issued_on' => $violator->res_cert_issued_on,
                'other_ids' => $violator->other_ids,
                'id_numbers' => $violator->id_numbers,
            );
            
            if($violator->is_existing){
                
                //double check for God's sake, we can remove it kapag naupdate na lahat ng violator.
                
                $c_config['table'] = 'ids_tbl';
                $c_config['conditions'] = array('violator_id' => $violator->violator_id);
                
                $res = $this->special_get($c_config);
                
                if($res){
                    $config = array(
                        'table' => 'ids_tbl',
                        'column' => 'violator_id',
                        'column_value' => $violator->violator_id,
                        'data' => $row,	
                    );
                    $this->normal_update($config);
                } else {
                    //insert
                    $config['table'] = 'ids_tbl';
                    $config['data'] = $row;
                    $this->special_save($config);  
                }
                
            } else {
                $config['table'] = 'ids_tbl';
                $config['data'] = $row;
                $this->special_save($config);  
            }
        
        }
		
		private function unset_unnecessary_props($violator){
			unset($violator->existing);
			unset($violator->front_temp_file_path);
			unset($violator->rear_temp_file_path);
			unset($violator->right_temp_file_path);
			unset($violator->left_temp_file_path);
			unset($violator->is_existing);
            unset($violator->dr_license);
            unset($violator->dr_issued_at);
            unset($violator->dr_issued_on);
            unset($violator->res_cert);
            unset($violator->res_cert_issued_at);
            unset($violator->res_cert_issued_on);
            unset($violator->other_ids);
            unset($violator->id_numbers);
		}
		
		private function insert_arresting_officers($data){
			$ids = explode(',', $data['arresting_officers_id']);
			
			foreach($ids as $value){
				$arr['blotter_entry_nr'] = $data['blotter_entry_nr'];
				$arr['arresting_officer_id'] = $value;
				$this->normal_insert('arresting_officers_list_tbl',$arr);
			}
		}
		
		private function insert_arrest_details($data){
			//fix the time and time of arrest
			$this->load->library('DateTimeHelper');
			
			$parsed_date =$this->datetimehelper->parseto_db_date($data['date_of_arrest']);
			$parsed_time =$this->datetimehelper->parseto_db_time($data['time_of_arrest']);
			
			//di ko na nga kailangan gawin to ambobo ko talaga. HAHAHA!
			$arrest['date_of_arrest'] = $parsed_date;
			$arrest['time_of_arrest'] = $parsed_time;
			$arrest['blotter_entry_nr'] = $data['blotter_entry_nr'];
			$arrest['street_name'] = $data['street_name'];
			$arrest['barangay_id'] = $data['barangay_id'];
			$arrest['city_name'] = $data['city_name'];
			$arrest['lawyer'] = $data['lawyer'];
            $arrest['med_at'] = $data['med_at'];
            $arrest['dr_name'] = $data['dr'];
            $arrest['med_on'] = $data['med_on'];
			$arrest['investigator_pnp_id'] = $data['investigator_id'];
            $arrest['finger_taken_by'] = $data['finger_taken_by'];
            $arrest['photo_taken_by'] = $data['photo_taken_by'];
			$arrest['booker_pnp_id'] = $this->session->userdata('username');
            
			
			$this->normal_insert('arrest_details_tbl',$arrest);
		}
        
        //update attached_to property
        private function update_spotreport($sp_id, $case_id){
           
            $config['table'] = 'spot_report_tbl';
            $config['conditions'] = array('spot_report_id' => $sp_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
        
        //update attached_to property
        private function update_preoperation($preop_id, $case_id){
           
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array('pre_op_id' => $preop_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
    
        }
        
        //update attached_to property
        private function update_coordination($coord_id, $case_id){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $coord_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
        
        private function update_affidavit($main_id, $case_id){
          
            $config['table'] = 'affidavit_tbl';
            $config['conditions'] = array('affidavit_id' => $main_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
        
        //increment case count for barangay
        private function update_barangay($barangay_id){
            $config['table'] = 'barangay_tbl';
            $config['conditions'] = array('barangay_id' => $barangay_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'barangay_incidents',
                'param_2' => ' barangay_incidents + 1',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
           
        }
		
		private function return_error_message(){
			//$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
	
			$array['error_message'] = $this->db->_error_message();
			$array['error_number'] = $this->db->_error_number();
			
			return $array;
		}
        
        /*====================================================================
        *                     CASE KEY WORDS 
        *====================================================================*/
        
        public function create_case_keywords($data){
            $keywords = array();
            
            //clear old keywords
            $this->clear_keywords($data['case_id']);
            
            //case_id as keyword
            $keywords[] = $data['case_id'];
            //blotter entry as keyword
            $keywords[] = $data['blotter_entry_nr'];
            //date as keyword
            $keywords[] = $data['date_of_arrest'];
            //street as keyword
            $keywords[] = $data['street_name'];
            //cityname as keyword
			$keywords[] = $data['city_name'];
            //barangay as keyword
            $keywords[] = $this->get_barangay_info($data['barangay_id'])->barangay_name;
            //nature as keywords
            $keywords[] = $data['nature'];
            //progress as keywords
            $keywords[] = $data['progress'];
            //violators name and ids as keyword
            $violators = json_decode($data['violators']);
            
            foreach($violators as $v){
                $keywords[] = $v->violator_id;
                $keywords[] = $v->first_name;
                $keywords[] = $v->last_name;
                $keywords[] = $v->middle_name;
                $keywords[] = $v->alias;
            }
            
            $key_str = $this->create_keywords_str($keywords);
            $this->update_keywords($key_str, 'case_report_tbl', array('case_id' => $data['case_id']));
                
        }
        
        //$decode tells whether to decode or not the data
        public function create_violators_keywords($data, $decode = TRUE){
            $keywords = array();
            
            $violators; 
            
            if($decode === TRUE){
                $violators = json_decode($data['violators']);
            } else {
                $violators = $data;
            }
          
            
            foreach($violators as $violator){
                $keywords[] = $violator->first_name;
                $keywords[] = $violator->middle_name;
                $keywords[] = $violator->last_name;
                $keywords[] = $violator->alias;
                $keywords[] = $violator->violator_id;
                
                $key_str = $this->create_keywords_str($keywords);
                $this->update_keywords($key_str, 'violators_tbl', array('violator_id' => $violator->violator_id));
                
                $keywords = array();
            }
            
        
        }
        
        private function create_keywords_str($keywords){
            $str = '';
            
            
            foreach($keywords as $key){
                $str .= $key . ' ';
            }
            
            
            return $str;
        }
        
        private function update_keywords($str,$table, $conditions){
            $config['table'] = $table;
            $config['conditions'] = $conditions;
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'keywords',
                'param_2' =>  '"'.$str.'"',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $this->special_save($config);
        
        }
        
        private function clear_keywords($id){
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'keywords',
                'param_2' => '""',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $this->special_save($config);
        }
	}