<?php

class Archivesmod extends Ss_model {
    
    private $mix = array();
    
    
    public function get_all_unarchived(){
        $config['table'] = 'case_report_tbl';
        $config['cols'] = array('case_id','blotter_entry_nr','progress','is_number');
        
        $config['conditions'] = array('on_archive' => 0);
        
        $cases = $this->special_get($config);
    
        foreach($cases as $case){
            
            $case->doa = $this->ss_get_case_date($case->blotter_entry_nr);
            $case->v_count = count($this->ss_get_case_violators($case->blotter_entry_nr));
            
        }
        
        return $cases; 
    }
    
    public function get_archives(){
        $config['table'] = 'archives_tbl';
        
        return $this->special_get($config);
    }
    
    /*============================================================================
    *
    *                           ARCHIVING PROCESS
    *
    *===========================================================================*/
    
    //automatic archiving
    public function do_automatic_archiving(){
        return $this->get_archivable_cases();
    }
    
    
    public function get_archivable_cases(){
        
       
        $failed = false;
    
        $this->load->library('DateTimeHelper');
        
        $config['table'] = 'case_report_tbl';
        $config['cols'] = array('case_id','blotter_entry_nr','progress','is_number','date_closed');
        $config['conditions'] = array(
            'progress' => 'Closed',
            'on_archive' => 0,
        );
        
        $cases = $this->special_get($config);
        $cases_arch = array();
        if($cases){
            foreach($cases as $case){
                $case->doa = $this->ss_get_case_date($case->blotter_entry_nr);
                $case->blotter =  $case->blotter_entry_nr;

                $date1 = new DateTime($case->date_closed);
                $date2 = new DateTime($this->datetimehelper->get_current_date());
                $interval = $date1->diff($date2);
                
                if($interval->y >= 5){
                    //$case_arch['case_id'] = $case->case_id;
                    //$case_arch['blotter'] = $case->blotter_entry_nr;
                    $cases_arch[] = $case;
                }
           
            }
        
            if(count($cases_arch) > 0){
                $this->archive($cases_arch);

                if($this->db->trans_status() === FALSE){
                    $array['error_message'] = $this->db->_error_message();
                    $array['error_number'] = $this->db->_error_number();
                    
                    $failed = true;
                   
                    $this->db->trans_rollback();

                }else{	
                    $failed = false;
                }  

            }
        }
        
        $this->update_archive_date_settings();
         
        return $failed;
    }
    
    public function get_archive_settings(){
        $config['table'] = 'archives_settings_tbl';
        
        return $this->special_get($config)[0];
    }
    
    public function update_archive_date_settings(){
        
        $this->load->library('DateTimeHelper');
        
        $newDate= $this->datetimehelper->get_current_date();
            
        $config['table'] = 'archives_settings_tbl';
        $config['conditions'] = array('id' => 1);
        $config['use_set'] = TRUE;
        $config['set_params'] = array(
            'param_1' => 'last_archiving',
            'param_2' => "'{$newDate}'",
            'param_3' => FALSE,
        );     
        $config['type'] = 'UPDATE';
        
        $result = $this->special_save($config);

        return $result;
    }
    
    public function update_archive_freq_settings($value){
        $config['table'] = 'archives_settings_tbl';
        $config['conditions'] = array('id' => 1);
        $config['use_set'] = TRUE;
        $config['set_params'] = array(
            'param_1' => 'archive_frequency',
            'param_2' => "'{$value}'",
            'param_3' => FALSE,
        );     
        $config['type'] = 'UPDATE';
        
        $result = $this->special_save($config);

        return $result;
    
    }
    
    public function archive($data){
        
        $this->db->trans_begin();
        
        foreach($data as $obj){
            $result = $this->set_flag($obj->case_id);
            $this->insert_archive($obj);
        }
        
        
        if($this->db->trans_status() === FALSE){
            $array['error_message'] = $this->db->_error_message();
            $array['error_number'] = $this->db->_error_number();

            $this->db->trans_rollback();

            $mix = $array;
            
        }else{	
            $mix = true;
            $this->db->trans_commit();     
        }	
        
       
        return $mix;
    }
    
    public function insert_archive($obj){
        $config['table'] = 'archives_tbl';
        
        $this->load->library('DateTimeHelper');
       
        $row = array(
            'case_id' => $obj->case_id,
            'blotter' => $obj->blotter,
            'date' => $this->datetimehelper->get_current_date(),
        );
        
        $config['data'] = $row;
        
        $this->special_save($config);
        
    }

    public function restore_archive($archive_id){
        $case_id = $this->get_case_from_archive($archive_id);
        $up_result = $this->set_flag($case_id, 0);
        $del_result = $this->delete_archive($archive_id);
        
        return $del_result;
    }
    
    public function delete_archive($archive_id){
        $config['table'] = 'archives_tbl';
        $config['conditions'] = array('id' => $archive_id);
        
        return $this->special_delete($config);
    }
    
    public function set_flag($case_id, $value = 1){
        
        $config['table'] = 'case_report_tbl';
        $config['conditions'] = array('case_id' => $case_id);
        $config['use_set'] = TRUE;
        $config['set_params'] = array(
            'param_1' => 'on_archive',
            'param_2' => $value,
            'param_3' => FALSE,
        );     
        $config['type'] = 'UPDATE';
        
        $result = $this->special_save($config);

        return $result;
    }
    
    public function download_archive($archive_id){
        $case_id = $this->get_case_from_archive($archive_id);
        
        //transfer to archive
        $this->load->library('ArchiveManager');
        
        $this->archivemanager->transfer_to_archive($case_id);
        
        //zipping and download
        $this->load->library('zip');
        $dest_path = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/archives/'; 
        $this->zip->read_dir($dest_path,FALSE);
    
        $this->zip->download($case_id . '.zip');

    }
    
    public function get_case_from_archive($archive_id){
        $config['table'] = 'archives_tbl';
        $config['conditions'] = array('id' => $archive_id);
        $config['cols'] = array('case_id');
        
        
        return $this->special_get($config)[0]->case_id;
    }
    
    
    //EL FINDER
    
    function elfinder_init(){
        $this->load->helper('path');
        $path = realpath(APPPATH . '../public/resources/photos/');
        $opts = array(
        // 'debug' => true, 
        'roots' => array(
          array( 
            'driver' => 'LocalFileSystem', 
            'path'   => set_realpath($path), 
            'URL'    => site_url(base_url())
            // more elFinder options here
          ) 
        )
        );
        $this->load->library('FileManager2', $opts);
    }
    
    
}