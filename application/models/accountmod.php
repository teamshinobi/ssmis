<?php

	class Accountmod extends Ss_model {
        
        
        public function update($data){
            $mix['result'] = false;
            
			$this->db->trans_begin();
            
            unset($data['prev_id']);
            
            $config = array(
				'table' => 'pnp_officer_tbl',
				'column' => 'pnp_id',
				'column_value' => $data['pnp_id'],
				'data' => $data,	
			);
           
            $this->normal_update($config);
            
			if($this->db->trans_status() === false){	$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				$array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                
              
				$this->db->trans_rollback();	
				
				$mix['result'] = false;
				$mix['errors'] = $array;
                                                    
			} else {
                
                //all is correct, commit transaction then upload image
                $this->db->trans_commit();
                $mix['result'] = true;
			}
            
            return $mix;	
        }
        
        public function change_pass($data){
            $id = $this->get_acc_id($data['pnp_id']);
    
          	$new = array(
				'password' => $data['password'],
            );
            
            $result = $this->ion_auth->update($id, $new);   
            
            return $result;
        }
        
        public function get_acc_id($pnp_id){
            $config['table'] = 'users';
            $config['cols'] = 'id';
            $config['conditions'] = array('username' => $pnp_id);
            
            $acc_id = $this->special_get($config)[0];
            
            return $acc_id->id;
        
        }
    }