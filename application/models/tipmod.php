<?php
	
    class Tipmod extends Ss_model {
        
        function add($data){
            
            $to_return = false;
            
            $config['table'] = 'tips_tbl';
            
            $row = array(
                'date' => $data['date'],
                'story' => $data['story'],
                'do_show' => 0,
            );
            
            $config['data'] = $row;
            
            $res = $this->special_save($config);
            $id = $this->db->insert_id();
           
            if($res == 1){
                $to_return = true;
                $this->save_targets(json_decode($data['targets']),$id);    
            } 
            
            return $to_return;
        }
        
        function save_targets($targets,$id){
            $config['table'] = 'tips_targets_tbl';
            
            foreach($targets as $target){
                 $row = array(
                    'tip_id' => $id,
                    'full_name' => $target->full_name,
                    'alias' => $target->alias,
                    'nationality' => $target->nationality,
                 );
                $config['data'] = $row;
                $res = $this->special_save($config);
            }             
        }
        
        
        function retrieve(){
            $config['table'] = 'tips_tbl';
            
            $res = $this->special_get($config);
             
            
            $config['table'] = 'tips_targets_tbl';
            
            foreach($res as $obj){
                
                $config['conditions'] = array( 
                    'tip_id' => $obj->id,     
                );
                    
                $obj->targets = $this->special_get($config); 
            }    
                 
            
            return $res;
        }  
        
        public function show($id){
            
            $config['table'] = 'tips_tbl';
            $config['conditions'] = array(
                'id' => $id,
            );
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'do_show',
                'param_2' => 1,
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);

            return $result;
        
        }
        
        public function unshow($id){
            
            $config['table'] = 'tips_tbl';
            $config['conditions'] = array(
                'id' => $id,
            );
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'do_show',
                'param_2' => 0,
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);

            return $result;
        
        }
        
        public function shown_targets(){
            $config['table'] = 'tips_tbl';
            $config['conditions'] = array(
                'do_show' => 1,
            );
            
            $results = $this->special_get($config);
            $targets = array();
           
            $config['table'] = 'tips_targets_tbl';
            if($results){
                foreach($results as $obj){

                    $config['conditions'] = array( 
                        'tip_id' => $obj->id,     
                    );

                    $res = $this->special_get($config); 

                    foreach($res as $tar){
                        $targets[] = $tar;
                    }   
                }  
            
            }
             
        
            
           return $targets;
        }
    }