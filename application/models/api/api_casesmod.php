<?php

class Api_casesmod extends Ss_model {
    
    
    public function retrieve(){
        $CI =& get_instance(); 
        $CI->load->model('viewcasereportmod');
        $results = $CI->viewcasereportmod->get_case_reports();
        
        return $results; 
    }
    
    public function get($id){
       
        $CI =& get_instance(); 
        $CI->load->model('editcasereportmod');
        $results = $CI->editcasereportmod->get_full_details($id);
        
        return $results; 
    }
    
   
}