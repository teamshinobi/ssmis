<?php

class Api_violatormod extends Ss_model {
    
    
    public function retrieve(){
    
        $config['table'] = 'violators_tbl';
        $config['cols'] = array('violator_id', 'first_name', 'last_name',
                               'middle_name', 'alias', 'file_path');
        $config['order_by'] = 'violator_id';
        $config['order_arrange'] = 'DESC';
        
        $results = $this->special_get($config);
        
        return $results;
    }
    
    //gets all info about a particular violator
    public function get($id){
        $CI =& get_instance(); 
        $CI->load->model('violatormod');
        $results = $CI->violatormod->get($id);
        $results[0]->cases = $CI->violatormod->get_cases($id);
        return $results[0];
    }
    
}