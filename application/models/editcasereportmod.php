<?php
	class Editcasereportmod extends Ss_model {
        
        private $case;
        
        public function get_full_details($case_id) {
             
            $this->case = new stdClass();
            
            $this->case->case_details = $this->get_case_details($case_id);
            
            $this->case->arrest_details = $this->get_arrest_details($this->case->case_details->blotter_entry_nr);
            
            $this->case->violators = $this->get_case_violators($this->case->case_details->blotter_entry_nr);
            
            $this->case->items = $this->get_case_items($case_id);
            
            $this->case->nitems = $this->get_case_nitems($case_id);
            
            $this->case->seizing_officers = $this->get_case_seizing_officers($case_id); 
            
            $this->case->witnesses = $this->get_case_witnesses($case_id);
            
            return $this->case;
        }
        
        
        private function get_case_details($case_id){
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            
            return $this->special_get($config)[0];
        }
        
        
        private function get_arrest_details($blotter){
            $config['table'] = 'arrest_details_tbl';
            $config['conditions'] = array('blotter_entry_nr' => $blotter);
            
            $blotter = $this->special_get($config)[0];
            
            //set time format
            $this->load->library('DateTimeHelper');
            $blotter->time_of_arrest = $this->datetimehelper->parseto_system_time($blotter->time_of_arrest);
            
            //get investigator information
            $blotter->investigator = $this->get_police_info($blotter->investigator_pnp_id);
            $blotter->investigator->pnp_id = $blotter->investigator_pnp_id;
            unset($blotter->investigator_pnp_id);
            
            //get fingerman information
            $blotter->fingerman = $this->get_police_info($blotter->finger_taken_by);
            if($blotter->fingerman){
                $blotter->fingerman->pnp_id = $blotter->finger_taken_by;
            } 
            unset($blotter->finger_taken_by);
            
            $blotter->photographer = $this->get_police_info($blotter->photo_taken_by);
            if($blotter->photographer){
                $blotter->photographer->pnp_id = $blotter->photo_taken_by;
            } 
            unset($blotter->photo_taken_by);
            
            //get booking officer info
            $blotter->booker = $this->get_police_info($blotter->booker_pnp_id);
            $blotter->booker->pnp_id = $blotter->booker_pnp_id;
            unset($blotter->booker_pnp_id);
            
            //get arresting officers
            $blotter->arresting_officers = $this->get_case_arrestingofficers($blotter->blotter_entry_nr);
            
            $blotter->barangay = $this->get_barangay_name($blotter->barangay_id)->barangay_name;
          
            return $blotter;
        }
        
        private function get_police_info($pnp_id){
            $config['table'] = 'pnp_officer_tbl';
            $config['cols'] = array('rank','first_name','last_name');
            $config['conditions'] = array('pnp_id' => $pnp_id);
            
            $pnp = $this->special_get($config)[0];
            
            if($pnp){
                $pnp->rank = $this->get_rank_acronym($pnp->rank);
            }
           
            
            return $pnp;
        }
        
        private function get_barangay_name($barangay_id){
            $config['table'] = 'barangay_tbl';
            $config['cols'] = array('barangay_name');
            $config['conditions'] = array('barangay_id' => $barangay_id);
            
            $brgy = $this->special_get($config)[0];
            
            
            return $brgy;
        }
        
        
        private function get_case_arrestingofficers($blotter){
            $config['table'] = 'arresting_officers_list_tbl';
            $config['cols'] = array('arresting_officer_id');
            $config['conditions'] = array('blotter_entry_nr' => $blotter);
            
            $pnps = $this->special_get($config);
            $config['table'] = 'pnp_officer_tbl';
            $config['cols'] = array('rank','first_name','last_name');
            
            foreach($pnps as $pnp){
                $config['conditions'] = array('pnp_id' => $pnp->arresting_officer_id);
                $info = $this->special_get($config)[0];
                 
                foreach($info as $key => $value){
                    $pnp->$key = $value;
                }
                $pnp->rank = $this->get_rank_acronym($pnp->rank);
                $pnp->pnp_id = $pnp->arresting_officer_id;
                
                unset($pnp->arresting_officer_id);
                  
            }
    
            return $pnps;
        }
        
        private function get_case_witnesses($case_id){
            $config['table'] = 'witness_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            $witnesses = $this->special_get($config);
            
            return $witnesses;
        }
        
        private function get_case_violators($blotter){
            $config['table'] = 'arrest_booking_form_tbl';
            $config['cols'] = array('violator_id');
            $config['conditions'] = array('blotter_entry_nr' => $blotter);
            
            $violators = $this->special_get($config);
            
            foreach($violators as $violator){
                $v_config['table'] = 'violators_tbl';
                $v_config['conditions'] = array('violator_id' => $violator->violator_id);
                
                $info = $this->special_get($v_config)[0];
                //loop info to append as property
                foreach($info as $key => $value){
                    $violator->$key = $value;
                }
                
                //get credentials
                $cred = $this->get_violator_ids($violator->violator_id);
                if($cred){
                    foreach($cred as $key => $value){
                        $violator->$key = $value;
                    }
                }
                
                //get cases, at this point violators info is complete
                $violator->charges = $this->get_violator_charges($violator->violator_id, $blotter);
                
                
            }
            
            return $violators;
        
        }
        
        public function get_violator_ids($id){
            $config['table'] = 'ids_tbl';
            $config['conditions'] = array('violator_id' => $id);
            
            return $this->special_get($config)[0];     
        }
		
        private function get_violator_charges($violator_id,$blotter){
           
            $v_config['table'] = 'arrest_booking_form_tbl';
            $v_config['conditions'] = array(
                'violator_id' => $violator_id, 
                'blotter_entry_nr' => $blotter
            );

            $charges = $this->special_get($v_config);
          
            //get charges info
            $c_config['table'] = 'charges_tbl';
            foreach($charges as $charge){
                $c_config['conditions'] = array('charge_id' => $charge->charge_id);
                $info = $this->special_get($c_config)[0];
                
                //loop info to append as property
                foreach($info as $key => $value){
                    $charge->$key = $value;
                }
                
                unset($charge->blotter_entry_nr);
                unset($charge->violator_id);
            } 
            
            return $charges;
        }
        
       
        private function get_case_items($case_id){ 
            
            $v_config['table'] = 'item_seized_tbl';
            $v_config['conditions'] = array(
                'case_id' => $case_id,      
            );
            
            $items = $this->special_get($v_config);
            //get violators for each_item
            
            if($items){
                foreach($items as $item){
                    $v_config['table'] = 'violator_item_tbl';
                    $v_config['cols'] = array('violator_id');
                    $v_config['conditions'] = array(
                        'item_seized_id' => $item->item_seized_id,      
                    );    

                    $item->violators = $this->special_get($v_config); 

                    //gets violators full name
                    if($item->violators){
                        foreach($item->violators as $violator){
                            $v_config['table'] = 'violators_tbl';
                            $v_config['cols'] = array('violator_id','first_name','last_name');
                            $v_config['conditions'] = array(
                                'violator_id' => $violator->violator_id,      
                            );  

                            $info = $this->special_get($v_config)[0];
                            foreach($info as $key => $value){
                                 $violator->$key = $value;
                            }
                        } 
                    }


                }
            }
              
            return $items;

        }
        
        
        private function get_case_nitems($case_id){ 
            $v_config['table'] = 'narcotics_item_seized_tbl';
            $v_config['conditions'] = array(
                'case_id' => $case_id,      
            );
            
            $items = $this->special_get($v_config);
            //get violators for each_item
            $v_config['table'] = 'violator_narcotics_item_tbl';
            
            if($items){
                foreach($items as $item){
                    $v_config['cols'] = array('violator_id');
                    $v_config['conditions'] = array(
                        'narcotics_seized_id' => $item->narcotics_seized_id,      
                    );    

                    $item->violators = $this->special_get($v_config); 

                    if($item->violators != false){
                        foreach($item->violators as $violator){
                            $config['table'] = 'violators_tbl';
                            $config['cols'] = array('violator_id','first_name','last_name');
                            $config['conditions'] = array(
                                'violator_id' => $violator->violator_id,      
                            );  

                            $info = $this->special_get($config)[0];
                            foreach($info as $key => $value){
                                 $violator->$key = $value;
                            }
                        } 
                    }
                    
                    //gets narcotics info
                    $n_config['table'] = 'narcotics_tbl';
                    $n_config['cols'] = array('name');
                    $n_config['conditions'] = array('narcotics_id' => $item->narcotics_id);

                    $item->narcotics = $this->special_get($n_config)[0];
                    $item->narcotics->narcotics_id = $item->narcotics_id;
                    //gamit kasi sa front end ung index na narcotics_name potaena.
                    $item->narcotics->narcotics_name = $item->narcotics->name;
                    unset($item->narcotics->name);
                    unset($item->narcotics_id);

                }
            }
            
            
            return $items;
        }
         
        function get_case_seizing_officers($case_id){
            $config['table'] = 'seizing_officers_tbl';
            $config['cols'] = array('seizing_officer_id');
            $config['conditions'] = array('case_id' => $case_id);
            
            $pnps = $this->special_get($config);
            $config['table'] = 'pnp_officer_tbl';
            $config['cols'] = array('rank', 'first_name', 'last_name');
            foreach($pnps as $pnp){
                $config['conditions'] = array('pnp_id' => $pnp->seizing_officer_id);
               
                $info = $this->special_get($config)[0];
                 
                foreach($info as $key => $value){
                    $pnp->$key = $value;
                }
                $pnp->rank = $this->get_rank_acronym($pnp->rank);
                $pnp->pnp_id = $pnp->seizing_officer_id;
                
                unset($pnp->seizing_officer_id);
                  
            }
            
            return $pnps;
        }
        
        
        /*==============================================================
        *              IMAGES UPLOADING AND UPDATING
        *==============================================================*/
        
        function transfer_violator_photos_to_temp($v_ids){
            
            $to_return = false;
            
            $this->load->library('FileManager');
            
            if($v_ids){
                foreach($v_ids as $id){
              
                    $to_return = $this->filemanager->copy_permanent_to_temp('violator',$id);	
                }
            } 
            
			return $to_return;
        }
        
        function transfer_items_photos_to_temp($case_id){   
             
            $this->load->library('FileManager');
          
            $to_return = $this->filemanager->copy_permanent_to_temp('inventory',$case_id);	
            
            return $to_return;
        }
        
        function transfer_scene_photos_to_temp($case_id){
            $this->load->library('FileManager');
          
            $to_return = $this->filemanager->copy_permanent_to_temp('case',$case_id);	
            
            return $to_return;
        }
         
        //update the closing date of case report
        function update_closing_date($case_id){
            
            $this->load->library('DateTimeHelper');
            $current_date = $this->datetimehelper->get_current_date();
            
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'date_closed',
                'param_2' => "'{$current_date}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
        
        
        /*==============================================================
        *           HANDLING SUBMISSION. MADUGO TO HUTAENA.
        *==============================================================*/
        
        
        public function submit($data){
            $to_return = true;
            $this->db->trans_begin();
        
            $this->update_arrest_details($data);
            $this->update_barangays($data['barangay_id'], $data['prev_brgy_id']);
            $this->update_arresting_officers($data);;
            $this->update_witnesses($data['case_id'],json_decode($data['witnesses']));
            $folders = $this->update_violators($data);
            $this->update_arrestbooking_form($data['blotter_entry_nr'], json_decode($data['violators']));
            $this->update_items(json_decode($data['items']),json_decode($data['items_rem']));
            $this->update_violator_items(json_decode($data['items']));
            $this->update_nitems(json_decode($data['nitems']),json_decode($data['nitems_rem']));
            $this->update_violator_nitems(json_decode($data['nitems']));
            $this->update_seizing_officers($data['case_id'],json_decode($data['seizing_officers']));
            $this->update_case_report($data);
            
            //call another model
            $CI =& get_instance(); 
            $CI->load->model('casereportmod');
           
            $CI->casereportmod->create_case_keywords($data);
            $CI->casereportmod->create_violators_keywords($data);
          
            if ($this->db->trans_status() === FALSE) {
              
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
				
				$this->db->trans_rollback();
                
                $to_return = $array;
            } else {
                
                $this->move_violators_images($folders);
                $this->move_items_images($data['case_id']);
                $this->move_scene_images($data['case_id']);  
            }
            
            $this->db->trans_commit();
            
            //yeah
            if($data['progress'] == 'Closed'){
                $this->update_closing_date($data['case_id']);
            }
            
            return $to_return;
        }
        
        private function move_scene_images($case_id){
            $this->load->library('FileManager');
			$behavior = 'PUKE!';
            return $this->filemanager->move_temp_to_permanent('case',array($case_id), $behavior);	
        }
        
        private function update_case_report($data){
            $case['case_id'] = $data['case_id'];
			$case['is_number'] = $data['is_number'];
			$case['blotter_entry_nr'] = $data['blotter_entry_nr'];
			$case['nature'] = $data['nature'];
			$case['progress'] = $data['progress'];
			$case['doc_path'] = 'public/resources/photos/case/' . $data['case_id'] . '/';
			$case['operation_name'] = $data['operation'];
			$case['summary'] = $data['summary'];
            
            //spot report
            if($data['spot_report_id'] != "NONE"){
                $case['spot_report_id'] = $data['spot_report_id'];
                //para malaman kung may naremove bang attachment at i-set sa NONE ung attachment na un
                if($data['spot_report_id_e'] != 'NONE'){
                    $this->update_spotreport($data['spot_report_id_e'], 'NONE');
                }
              
                $this->update_spotreport($data['spot_report_id'], $data['case_id']);
            } else {
                //para malaman kung may naremove bang attachment at i-set sa NONE ung attachment na un
                if($data['spot_report_id_e'] != 'NONE'){
                    $this->update_spotreport($data['spot_report_id_e'], 'NONE');
                }
              
                $case['spot_report_id'] = $data['spot_report_id'];
            }
            
            
            
            //preoperation report
            if($data['pre_op_id'] != "NONE"){
                $case['pre_op_id'] = $data['pre_op_id'];
                if($data['pre_op_id_e'] != 'NONE'){
                    $this->update_preoperation($data['pre_op_id_e'], 'NONE');
                }
                $this->update_preoperation($data['pre_op_id'], $data['case_id']);
            } else {
                if($data['pre_op_id_e'] != 'NONE'){
                    $this->update_preoperation($data['pre_op_id_e'], 'NONE');
                }
                $case['pre_op_id'] = $data['pre_op_id'];
            }
            
            //coordination report
            if($data['coordination_id'] != "NONE"){
                $case['coordination_id'] = $data['coordination_id'];
                if($data['coordination_id_e'] != 'NONE'){
                    $this->update_coordination($data['coordination_id_e'], 'NONE');
                }
                $this->update_coordination($data['coordination_id'], $data['case_id']);
            } else {
                if($data['coordination_id_e'] != 'NONE'){
                    $this->update_coordination($data['coordination_id_e'], 'NONE');
                }
                $case['coordination_id'] = $data['coordination_id'];
            }
            //main affidavit report
            if($data['main_id'] != "NONE"){
                $case['main_affidavit_id'] = $data['main_id'];
                if($data['main_id_e'] != 'NONE'){
                   $this->update_affidavit($data['main_id_e'], 'NONE');
                }
                $this->update_affidavit($data['main_id'], $data['case_id']);
            } else {
                if($data['main_id_e'] != 'NONE'){
                    $this->update_affidavit($data['main_id_e'], 'NONE');
                }
                $case['main_affidavit_id'] = $data['main_id'];
            }
            
            //supp affidavit
            if($data['supp_id'] != "NONE"){
                $case['supp_affidavit_id'] = $data['supp_id'];
                if($data['supp_id_e'] != 'NONE'){
                   $this->update_affidavit($data['supp_id_e'], 'NONE');
                }
                $this->update_affidavit($data['supp_id'], $data['case_id']);
            } else {
                if($data['supp_id_e'] != 'NONE'){
                    $this->update_affidavit($data['supp_id_e'], 'NONE');
                }
                $case['supp_affidavit_id'] = $data['supp_id'];
            }
            
            $config['table'] = 'case_report_tbl';
            $config['data'] = $case;
            $config['type'] = 'UPDATE';
            $config['conditions'] = array('case_id' =>  $case['case_id']);
            
			$this->special_save($config); 
        }
        
       
        private function update_affidavit($id, $case_id){
            $config['table'] = 'affidavit_tbl';
            $config['conditions'] = array('affidavit_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
        //update attached_to property
        private function update_spotreport($sp_id, $case_id){
           
            $config['table'] = 'spot_report_tbl';
            $config['conditions'] = array('spot_report_id' => $sp_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
          //update attached_to property
        private function update_preoperation($preop_id, $case_id){
           
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array('pre_op_id' => $preop_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
    
        }
        
        //update attached_to property
        private function update_coordination($coord_id, $case_id){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $coord_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'{$case_id}'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
        }
        
        private function update_witnesses($case_id,$witnesses){
            //delete
            $del_config['table'] = 'witness_tbl';
            $del_config['conditions'] = array(
                'case_id' => $case_id,
            );
            $this->special_delete($del_config);
            
            //reinsert
            $config['table'] = 'witness_tbl';
           
            foreach($witnesses as $obj){
                $row = array(
                    'case_id' => $case_id,
                    'name' => $obj->name,
                    'address' => $obj->address,
                    'contact' => $obj->contact,
                ); 
                
                $config['data'] = $row;
                $this->special_save($config);
            }
        }
        
        
        private function update_seizing_officers($case_id, $aoSeizingOfficers){
            //delete
            $del_config['table'] = 'seizing_officers_tbl';
            $del_config['conditions'] = array(
                'case_id' => $case_id,
            );
            $this->special_delete($del_config);
             
            //retinsert
            foreach($aoSeizingOfficers as $obj){
                $row = array(
                    'case_id' => $case_id,
                    'seizing_officer_id' => $obj->pnp_id,
                );  
                $config['table'] = 'seizing_officers_tbl';
                $config['data'] = $row;
                $this->special_save($config);
            }   
        
        }
        
        
       	//this will also move narcotics_images I dont know why. HAHAHA!
		private function move_items_images($case_id){
			//NOTE we only have 1 folder to move the INVENTORY folder in which the dir name is the inventory id.
			$this->load->library('FileManager');
			$behavior['tags'] = array('item');
			return $this->filemanager->move_temp_to_permanent('inventory',array($case_id), $behavior);	
		}
        
        private function update_violator_nitems($items){
            
            //delete old records first
            $del_config['table'] = 'violator_narcotics_item_tbl';
            foreach($items as $item){
				 $del_config['conditions'] = array(
                    'narcotics_seized_id' => $item->item_id,
                );
                $this->special_delete($del_config);
			}
            
            //reinsert
			foreach($items as $item){
				$violators = $item->violators;
				foreach($violators as $violator){
					
					$row = array(
						'narcotics_seized_id' => $item->item_id,
						'violator_id' =>  $violator->violator_id,
					);
				
					$this->normal_insert('violator_narcotics_item_tbl',$row);
				}
			}
        }
        private function update_nitems($items,$nitems_rem){
           
            //delete
            $del_config['table'] = 'narcotics_item_seized_tbl';
            foreach($nitems_rem as $item){
				 $del_config['conditions'] = array(
                    'narcotics_seized_id' => $item,
                );
                
                $this->special_delete($del_config);
			}
            
            $config['table'] = 'narcotics_item_seized_tbl';
            $config['type'] = 'UPDATE';
            
			foreach($items as $item){
		
				$narcotics = $item->narcotics;
			 
				$row['narcotics_seized_id'] = $item->item_id;
				$row['case_id'] = $item->case_id;
				$row['narcotics_id'] = $narcotics->narcotics_id;
				$row['quantity'] = $item->quantity;
				$row['markings'] = $item->markings;
				$row['is_in_chain_custody'] = $item->is_chain_custody;
				$row['file_path'] = $item->file_path;     
                
                if($item->is_existing){
                    $config['data'] = $row;
                    $config['conditions'] = array('narcotics_seized_id' => $item->item_id);  
                    $this->special_save($config);
                } else {
                    $this->normal_insert('narcotics_item_seized_tbl',$row);
                }
   			
			}
     
        }
        private function update_violator_items($items){
          
            //delete recors to avoid duplicates
            $del_config['table'] = 'violator_item_tbl';
            foreach($items as $item){
				 $del_config['conditions'] = array(
                    'item_seized_id' => $item->item_id,
                );
                $this->special_delete($del_config);
               
			}
            
            //reinsert
			foreach($items as $item){
				$violators = $item->violators;
				foreach($violators as $violator){
					
					$row = array(
						'item_seized_id' => $item->item_id,
						'violator_id' =>  $violator->violator_id,
					);
				
					$this->normal_insert('violator_item_tbl',$row);
				}
			}
		}
        
        private function update_items($items,$item_rem){	 
        
            //delete, $item_rem is array of strings of removed item
            $del_config['table'] = 'item_seized_tbl';
            foreach($item_rem as $item){
				 $del_config['conditions'] = array(
                    'item_seized_id' => $item,
                );
                $this->special_delete($del_config);
               
			}
            
            $config['table'] = 'item_seized_tbl';
            $config['type'] = 'UPDATE';
            
			foreach($items as $item){
				
				$row['item_seized_id'] = $item->item_id;
				$row['case_id'] = $item->case_id;
				$row['quantity'] = $item->quantity;
				$row['description'] = $item->description;
				$row['markings'] = $item->markings;
				$row['is_chain_custody'] = $item->is_chain_custody;
				$row['file_path'] = $item->file_path;
                
                
                if($item->is_existing){
                    $config['data'] = $row;
                    $config['conditions'] = array('item_seized_id' => $item->item_id);  
                    $this->special_save($config);
                } else {
                    $this->normal_insert('item_seized_tbl',$row);
                }
   			
			}
		}
        
        private function move_violators_images($folders){
			$this->load->library('FileManager');
			$behavior['tags'] = array('front','rear','right','left');
			return $this->filemanager->move_temp_to_permanent('violator',$folders, $behavior);	
		}
        
        
        private function update_arrestbooking_form($blotter ,$violators){
           
            //delete the previous records
            $del_config['table'] = 'arrest_booking_form_tbl';
            $del_config['conditions'] = array(
                'blotter_entry_nr' => $blotter,

            );
            $this->special_delete($del_config);
		
             
            //reinsert baby!
            foreach($violators as $violator){
				
				$charges = $violator->charges;
				
				foreach($charges as $charge){
					$row = array(
						'blotter_entry_nr' => $blotter,
						'violator_id' => $violator->violator_id,
						'charge_id' => $charge,
					);
					
					$this->normal_insert('arrest_booking_form_tbl',$row);
				}	
			}
		}
		//returns folder names to be used when moving temporary folders to permanent
		private function update_violators($data){
			$violators = json_decode($data['violators']);
            
			$this->load->library('DateTimeHelper');	
			$folders = array();
			
			foreach($violators as $violator){
				$parsed_date =$this->datetimehelper->parseto_db_date($violator->birth_date);
				$violator->birth_date = $parsed_date;
				$this->insert_ids($violator);
                
                if($violator->is_existing){
					$this->unset_unnecessary_props($violator);
					
					$config = array(
						'table' => 'violators_tbl',
						'column' => 'violator_id',
						'column_value' => $violator->violator_id,
						'data' => $violator,	
					);
					
					$this->normal_update($config);
                    
				} else {
					//unset unncessary properties
					$this->unset_unnecessary_props($violator);
					$this->normal_insert('violators_tbl',$violator);
				}
				
                $folders[] = $violator->violator_id;
			}
			return $folders;
		}
        
         private function insert_ids($violator){
          
            $row = array(
                'violator_id' => $violator->violator_id,
                'dr_license' => $violator->dr_license,
                'dr_issued_at' => $violator->dr_issued_at, 
                'dr_issued_on' =>  $violator->dr_issued_on,
                'res_cert' => $violator->res_cert,
                'res_cert_issued_at' => $violator->res_cert_issued_at,
                'res_cert_issued_on' => $violator->res_cert_issued_on,
                'other_ids' => $violator->other_ids,
                'id_numbers' => $violator->id_numbers,
            );
            
            if($violator->is_existing){
                
                //double check for God's sake, we can remove it kapag naupdate na lahat ng violator.
                
                $c_config['table'] = 'ids_tbl';
                $c_config['conditions'] = array('violator_id' => $violator->violator_id);
                
                $res = $this->special_get($c_config);
                
                if($res){
                    $config = array(
                        'table' => 'ids_tbl',
                        'column' => 'violator_id',
                        'column_value' => $violator->violator_id,
                        'data' => $row,	
                    );
                    $this->normal_update($config);
                } else {
                    //insert
                    $config['table'] = 'ids_tbl';
                    $config['data'] = $row;
                    $this->special_save($config);  
                }
                
            } else {
                $config['table'] = 'ids_tbl';
                $config['data'] = $row;
                $this->special_save($config);  
            }
        
        }
        private function unset_unnecessary_props($violator){
			unset($violator->existing);
			unset($violator->front_temp_file_path);
			unset($violator->rear_temp_file_path);
			unset($violator->right_temp_file_path);
			unset($violator->left_temp_file_path);
			unset($violator->is_existing);
            unset($violator->dr_license);
            unset($violator->dr_issued_at);
            unset($violator->dr_issued_on);
            unset($violator->res_cert);
            unset($violator->res_cert_issued_at);
            unset($violator->res_cert_issued_on);
            unset($violator->other_ids);
            unset($violator->id_numbers);
            unset($violator->id);
		}
        
        public function update_arrest_details($data){
           
            $this->load->library('DateTimeHelper');
			
			$parsed_date =$this->datetimehelper->parseto_db_date($data['date_of_arrest']);
			$parsed_time =$this->datetimehelper->parseto_db_time($data['time_of_arrest']);
			
			//di ko na nga kailangan gawin to ambobo ko talaga. HAHAHA!
            
			$arrest['date_of_arrest'] = $parsed_date;
			$arrest['time_of_arrest'] = $parsed_time;
			$arrest['blotter_entry_nr'] = $data['blotter_entry_nr'];
			$arrest['street_name'] = $data['street_name'];
			$arrest['barangay_id'] = $data['barangay_id'];
			$arrest['city_name'] = $data['city_name'];
			$arrest['city_name'] = $data['city_name'];
			$arrest['lawyer'] = $data['lawyer'];
            $arrest['med_at'] = $data['med_at'];
            $arrest['dr_name'] = $data['dr'];
            $arrest['med_on'] = $data['med_on'];
			$arrest['investigator_pnp_id'] = $data['investigator_id'];
            $arrest['finger_taken_by'] = $data['finger_taken_by'];
            $arrest['photo_taken_by'] = $data['photo_taken_by'];
			//$arrest['booker_pnp_id'] = $this->session->userdata('username');
         
			$config['table'] = 'arrest_details_tbl';
            $config['data'] = $arrest;
            $config['type'] = 'UPDATE';
          
            $config['conditions'] = array('blotter_entry_nr' => $data['prev_blotter']);
            
			$this->special_save($config);
        }
        
        
        private function update_barangays($new_brgy, $old_brgy){
            if($new_brgy != $old_brgy){
                //plus one for new barangay
                $config['table'] = 'barangay_tbl';
                $config['conditions'] = array('barangay_id' => $new_brgy);
                $config['use_set'] = TRUE;
                $config['set_params'] = array(
                    'param_1' => 'barangay_incidents',
                    'param_2' => ' barangay_incidents + 1',
                    'param_3' => FALSE,
                );     
                $config['type'] = 'UPDATE';
                $this->special_save($config);
                
                
                //negative 1 for old barangay
                $config['conditions'] = array('barangay_id' => $old_brgy);
                $config['set_params'] = array(
                    'param_1' => 'barangay_incidents',
                    'param_2' => ' barangay_incidents - 1',
                    'param_3' => FALSE,
                );  
                
                $this->special_save($config);
            }
        }
        
        //There would be no checking if there are changes that happend. Just fucking update that baby!
        private function update_arresting_officers($data){
            
            //delete first the previous arresting officers.
            $del_config['table'] = 'arresting_officers_list_tbl';
            $del_config['conditions'] = array('blotter_entry_nr' => $data['blotter_entry_nr']);
            $this->special_delete($del_config);
            
            //reinsert
            $ids = explode(',', $data['arresting_officers_id']);
			
			foreach($ids as $value){
				$arr['blotter_entry_nr'] = $data['blotter_entry_nr'];
				$arr['arresting_officer_id'] = $value;
				$this->normal_insert('arresting_officers_list_tbl',$arr);
			}
        }   
    }