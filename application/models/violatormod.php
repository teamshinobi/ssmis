<?php
	class Violatormod extends Ss_model {
		
		function get_all(){
			$config['table'] = 'violators_tbl';
			$config['order_by'] = 'violator_id';
			$config['order_arrange'] = 'DESC';
            
            $results =  $this->special_get($config);
            
            foreach($results as $result){
                $result->last_case = $this->get_cases($result->violator_id)[0];  
            } 
            
			return $results;	
		}
		
		function get($id){
			$config['table'] = 'violators_tbl';
			$config['limit'] = 1;
			$config['offset'] = 0;
			$config['conditions'] = array(
				'violator_id' => $id,
			);
			
            $violator = $this->special_get($config);
            
            foreach($violator as $v){
                $info = $this->get_ids($v->violator_id);
                if($info){
                    foreach($info as $key => $value){
                         $v->$key = $value;
                    }
                }
               
            }
            
			return $violator;
		}

        public function get_ids($id){
            $config['table'] = 'ids_tbl';
            $config['conditions'] = array('violator_id' => $id);
            
            return $this->special_get($config)[0];     
        }
		function fingerprints($id){
			$config['table'] = 'fingerprint_tbl';
			$config['limit'] = 1;
			$config['offset'] = 0;
			$config['conditions'] = array(
				'violator_id' => $id,
			);
			
			
			return $this->special_get($config);
		}
        function fingerprints_left($id){
			$config['table'] = 'fingerprint_left_tbl';
			$config['limit'] = 1;
			$config['offset'] = 0;
			$config['conditions'] = array(
				'violator_id' => $id,
			);
			
			
			return $this->special_get($config);
		}
        
		function get_cases($id){
            
            $to_return = false;
            
			$config['table'] = 'arrest_booking_form_tbl';
			$config['cols'] = array('blotter_entry_nr');
			$config['distinct'] = TRUE;
			$config['conditions'] = array(
				'violator_id' => $id,	
			);
			$blotters = $this->special_get($config);
			
            if($blotters){
                //get all blotters and dates and push it to array
                $blottersAO = [];
                foreach($blotters as $value){
                    //retrieve date
                    $config['table'] = 'arrest_details_tbl';
                    $config['cols'] = array('date_of_arrest','investigator_pnp_id');
                    $config['conditions'] = array(
                        'blotter_entry_nr' => $value->blotter_entry_nr,
                    );

                    $date = $this->special_get($config);
                    $blotter['blotter_entry_nr'] = $value->blotter_entry_nr;
                    $blotter['date_of_arrest'] = $date[0]->date_of_arrest;

                    $blottersAO[] = $blotter;
                }


                //create cases objects
                $casesAO = [];
                $config['table'] = 'case_report_tbl';
                unset($config['cols']);
                foreach($blottersAO as $obj){
                    $config['conditions'] = array(
                        'blotter_entry_nr' => $obj['blotter_entry_nr'],
                    );
                    $res = $this->special_get($config);
                    if($res != FALSE){
                        
                        $res[0]->date_of_arrest = $obj['date_of_arrest'];
                        
                         //involved violators
                        $v_config['table'] = 'arrest_booking_form_tbl';
                        $v_config['cols'] = array('violator_id');
                        $v_config['conditions'] = array('blotter_entry_nr' => $res[0]->blotter_entry_nr);

                        $violators = $this->special_get($v_config);
                    

                        $res[0]->count_violators = count($violators);
                        
                        foreach($violators as $v){
                            
                            $info = $this->get_case_violator($v->violator_id);
                            foreach($info as $key => $value){
                                $v->$key = $value;
                            }
                        
                        }
                        
                        $res[0]->violators = $violators;
                 
                        $casesAO[] = $res[0];
                        
                        //get cases
                        $res[0]->investigator = $this->get_case_investigator($res[0]->blotter_entry_nr);
          
                    }
                    
                }
                $to_return = $casesAO;
              
            }
			
			return $to_return;			
		}
        function get_case_investigator($blotter){
            
            $config['table'] = 'arrest_details_tbl';
            $config['cols'] = 'investigator_pnp_id';
            $config['conditions'] = array(
                'blotter_entry_nr' => $blotter,
            );
                
            $inv = $this->special_get($config)[0];
            $info = $this->ss_get_police_info($inv->investigator_pnp_id);
            foreach($info as $key => $value){
                $inv->$key = $value;
            }
            return $inv;
        
        }
       function get_case_violator($id){
            $config['table'] = 'violators_tbl';
            $config['cols'] = array('first_name','last_name', 'alias');
            $config['conditions'] =array( 'violator_id' => $id);
            
            
            return $this->special_get($config)[0];
        }
		
		function update_photo($data){
			$this->load->library('FileManager');
			$config['module'] = 'violator';
			$config['tag'] = $data['tag'];
			$config['id'] = $data['violator_id'];
			$config['multilevel'] = TRUE;
			return $this->filemanager->overwrite_image($config);		
		}
		
		function update($data){
			
			$config['table'] = 'violators_tbl';
			$config['type'] = 'UPDATE';
			//NOTE: Ayaw gumana kapag di ko ginaganito poke ng ina!
			$arr = array(
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'middle_name' => $data['middle_name'],
				'alias' => $data['alias'],
				'birth_date' => $data['birthdate'],
				'gender' => $data['gender'],
                'place_of_birth' => $data['place_of_birth'],
                'marital_status' => $data['marital_status'],
                'occupation' => $data['occupation'],
                'tel_no' => $data['tel_no'],
				'street_name' => $data['street_name'],
				'barangay_name' => $data['barangay_name'],
				'city_name' => $data['city_name'],
				'nationality' => $data['nationality'],
				'weight' => $data['weight'],
				'height' => $data['height'],
				'educational_attainment' => $data['educational_attainment'],
                'name_of_school' => $data['name_of_school'],
                'location_of_school' => $data['location_of_school'],
				'eyes_color' => $data['eyes_color'],
				'hair_color' => $data['hair_color'],
				'identifying_marks' => $data['identifying_marks'],
                'complexion' => $data['complexion'],
				'mother_name' => $data['mother_name'],
                'mother_address' => $data['mother_address'],
                'mother_age' => $data['mother_age'],
				'father_name' => $data['father_name'],
                'father_address' => $data['father_address'],
                'father_age' => $data['father_age'],
				'contact_person' => $data['contact_person'],
				'relationship' => $data['relationship'],
				'contact_info' => $data['contact_info']
			);
            
			$config['data'] = $arr;
			
            $config['conditions'] = array(
				'violator_id' => $data['violator_id'],
			);
            
            $this->update_keywords($arr, $data['violator_id']);
			
			$result = $this->special_save($config);
            
            $this->insert_ids($data);
			
			return $result;
		}
        
        private function insert_ids($data){
              
            $row = array(
                'violator_id' => $data['violator_id'],
                'dr_license' => $data['dr_license'],
                'dr_issued_at' => $data['dr_issued_at'], 
                'dr_issued_on' => $data['dr_issued_on'],
                'res_cert' => $data['res_cert'],
                'res_cert_issued_at' => $data['res_cert_issued_at'],
                'res_cert_issued_on' => $data['res_cert_issued_on'],
                'other_ids' => $data['other_ids'],
                'id_numbers' => $data['id_numbers'],
            );
            
            $c_config['table'] = 'ids_tbl';
            $c_config['conditions'] = array('violator_id' => $data['violator_id']);

            $res = $this->special_get($c_config);

            if($res){
                $config = array(
                    'table' => 'ids_tbl',
                    'column' => 'violator_id',
                    'column_value' => $data['violator_id'],
                    'data' => $row,	
                );
                $this->normal_update($config);
            } else {
                //insert
                $config['table'] = 'ids_tbl';
                $config['data'] = $row;
                $this->special_save($config);  
            }
          
        }
        
        
        
        function update_keywords($violator,$id){
                
            $object = (object) $violator;
            $object->violator_id = $id;
             //updates keyword
            $array = array();
            $array[] = $object;
               
            $CI =& get_instance(); 
            $CI->load->model('casereportmod');
            
            $CI->casereportmod->create_violators_keywords($array,false);
        }
        
		function search_cases($range){
			list($start, $end) = explode("-", $range);
			
			//get all violators id in the arrest_booking_form_tbl
			$config['table'] = 'arrest_booking_form_tbl';
			$config['cols'] = array('violator_id');
			$config['distinct'] = TRUE;
			$violators = $this->special_get($config);
			unset($config);
			
			//gets info of each violator
			foreach($violators as $violator){
				$config['table'] = 'violators_tbl';
				$config['cols'] = array('first_name','last_name','middle_name','file_path','remarks');
				$config['conditions'] = array(
					'violator_id' => $violator->violator_id,
				);
				$info = $this->special_get($config);
				$violator->info = $info[0];
				
				//get blotters
				$config['table'] = 'arrest_booking_form_tbl';
				$config['cols'] = array('blotter_entry_nr');
				$config['conditions'] = array(
					'violator_id' => $violator->violator_id,
				);
				$config['distinct'] = TRUE;
				$blotters = $this->special_get($config);
				unset($config); //unset array for distinct prop to be removed
				//determine if the count of blotters is right with the supplied conditions
				if(count($blotters) >= $start && count($blotters) <= $end){	
					//get dates of each blotter
					foreach($blotters as $blotter){
						$config['table'] = 'arrest_details_tbl';
						$config['cols'] = array('date_of_arrest');
						$config['conditions'] = array(
							'blotter_entry_nr' => $blotter->blotter_entry_nr,
						);
						$b_info = $this->special_get($config);
						//loop thru info properties and add it to $blotter
						foreach($b_info[0] as $key => $value){
							$blotter->$key = $value;
						}
						
						//gets the case_reports associated with this blotter and put it on blotter for easy reading.
						$config['table'] = 'case_report_tbl';
						$config['cols'] = array('case_id');
						$config['conditions'] = array(
							'blotter_entry_nr' => $blotter->blotter_entry_nr,
						);
						$case = $this->special_get($config);
						
						//loop through case[0] properties
						foreach($case[0] as $key => $value){
							$blotter->$key = $value;
						}
					}
					$violator->blotters = $blotters;
				}
			}
	
			//remove all violators with no blotter
			$aoViolators = array();
			foreach($violators as $violator){
				if(property_exists($violator,'blotters')){
					$aoViolators[] = $violator;
				}
			}
			return $aoViolators;
		}
		function search_date($start, $end){
			//get all blotters associated with the start and end date.
			$config['table'] = 'arrest_details_tbl';
			$config['cols'] = array('blotter_entry_nr','date_of_arrest');
			$config['conditions'] = array(
				'date_of_arrest >=' => $start,
				'date_of_arrest <=' => $end,
			);
		
			$blotters = $this->special_get($config);
			if(!$blotters){
				return false;
			}
			
			foreach($blotters as $blotter){
				//get all violators sa isang bloter
				$config['table'] = 'arrest_booking_form_tbl';
				$config['cols'] = array('violator_id');
				$config['distinct'] = TRUE;
				$config['conditions'] = array(
					'blotter_entry_nr' => $blotter->blotter_entry_nr,
				);
				$violators = $this->special_get($config);
				//para di ma-crash kapag walang violators ang isang blotter which is unlikely naman!
				if($violators){
					foreach($violators as $violator){
						$config_v['table'] = 'violators_tbl';
						$config_v['cols'] = array('violator_id','first_name','last_name','middle_name','file_path','remarks');
						$config_v['conditions'] = array(
							'violator_id' => $violator->violator_id,
						);
						$violator->details = $this->special_get($config_v);
					}
				}
				$blotter->violators = $violators;
				
				//get cases sa isang blotter
				$config1['table'] = 'case_report_tbl';
				$config1['cols'] = array('case_id');
				$config1['conditions'] = array(
					'blotter_entry_nr' => $blotter->blotter_entry_nr,
				);
				$case = $this->special_get($config1);
				$blotter->case_report = $case[0];
			}
			
			return $blotters;
		}
		
		function test(){
			$config['table'] = 'fingerprint_tbl';
			$config['conditions'] = array('violator_id' => 'SJ-VIOL-2015-0001');
			$data = $this->special_get($config);
		
			header("Content-type: image/jpg");
			echo $data[0]->thumb_img;
		}
       
}
		