<?php
	class Generic_model extends Ss_model {
        
        
        //clears photos in temps when unloading casereport and edit casereport module
        public function clear_temps(){
            $this->load->library('FileManager');

            //theres no way to know kung naremove ba ung photos or what. Tingin ko meron, pero wag muna. Thesis eh.
            $dir = realpath(APPPATH . '../public/resources/temps/violator/');
            $this->filemanager->rmdir_subdirectories($dir);	
            $dir = realpath(APPPATH . '../public/resources/temps/case/');
            $this->filemanager->rmdir_subdirectories($dir);	
            $dir = realpath(APPPATH . '../public/resources/temps/inventory/');
            $this->filemanager->rmdir_subdirectories($dir);	
            
            return true;
        }
    }