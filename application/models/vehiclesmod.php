<?php
class Vehiclesmod extends Ss_model {


    public function upload_photo($id){
        $this->load->library('FileManager');
        $config['module'] = 'vehicles';
        $config['id'] = $id;
        $config['multilevel'] = FALSE;
        return $this->filemanager->flexible_image_upload($config);
    }
    
    public function next_vehicles_id($id){
        $config = array(
            'strategy' => 'NORMAL_INCREMENT',
            'prefix' => 'SJ-VEHICLE',   
        );
        $this->load->library('Incrementor', $config);

        return $this->incrementor->increment($id);

    }
    public function vehicles_id() {
        $config = array(
            'strategy' => 'DB_INCREMENT',
            'prefix' => 'SJ-VEHICLE', 
        );

        //get the last row in database.
        $query_config = array(
            'table' => 'vehicles_tbl',
            'column' => 'vehicle_id',
            'order' => 'desc',
            'limit' => 1,
            'start' => 0,
            'fields' => array('vehicle_id')
        );

        //returns array of objects. In this case 1 object only
        $data = $this->limited_order_get($query_config);
        //this will be the basis of our DB_INCREMENT strategy		
        $last_id = "";

        if(empty($data)){
            $last_id = false;
        }else{
            $last_id = $data[0]->vehicle_id;
        }

        $this->load->library('Incrementor', $config);			

        return $this->incrementor->increment($last_id);
    }
    
    public function getall(){
        $config['table'] = 'vehicles_tbl';
        
        return $this->special_get($config);
    
    }
    public function update($data){
        $to_return = false;
        $this->db->trans_begin();
        $id = $data['vehicle_id'];
        $config['table'] = 'vehicles_tbl';
        unset($data['file']);
        $row = array(
            'vehicle_id' => $data['vehicle_id'],
            'make'=> $data['make'],
            'type'=> $data['type'],
            'color' => $data['color'],
            'plate_no'=> $data['plate_no'],
            'image_path' => 'public\resources\photos\vehicles',

            );
        $config['data'] = $row;
        $config['type'] = 'UPDATE';
        $config['conditions'] = array(
                'vehicle_id' => $id
                );

        $this->special_save($config);

        if($this->db->trans_status() === FALSE){
            $array['error_message'] = $this->db->_error_message();
            $array['error_number'] = $this->db->_error_message();

            $this->db->trans_rollback();
            
         
        }else{
            $to_return = TRUE;
            //$to_return->next_id = $this->next_vehicles_id($data['vehicle_id']);
           if(isset($_FILES["file"])){
            $this->upload_photo($data['vehicle_id']);
            }

            $this->db->trans_commit(); 
        }

        return $to_return;
    }
    
    public function insert($data){
        $to_return = new stdClass();
        $this->db->trans_begin();

        
        $config['table'] = 'vehicles_tbl';
        unset($data['file']);
        $data['image_path'] = 'public\resources\photos\vehicles';


        $config['data'] = $data;
        
        $this->special_save($config);
   
        if($this->db->trans_status() === FALSE){
            $array['error_message'] = $this->db->_error_message();
            $array['error_number'] = $this->db->_error_number();

            $this->db->trans_rollback();
            
            $to_return->success = FALSE;
            $to_return->db_err = $array;
        }else{	
            $to_return->success = TRUE;
            $to_return->next_id = $this->next_vehicles_id($data['vehicle_id']);
            $this->upload_photo($data['vehicle_id']);
            $this->db->trans_commit(); 
        }
            
        return $to_return;
    }



    public function get_vehicle($id){
            $config['table'] = 'vehicles_tbl';
            //$config['cols'] = array('');
            $config['conditions'] = array(
                'vehicle_id' => $id,
                );

            
            $data = $this->special_get($config)[0];

            return $data;
    }
   public function remove_vehicle($id){

            $this->db->trans_begin();
        
            $params = array(
                
                'vehicle_id' => $id,
            
            );
            $config = array(
                
                'table' => 'vehicles_tbl',
                'params' => $params,
            
            
            );
            
            $this->normal_delete($config);
            
            //if there are query errors or some sort of errors return error message
            if($this->db->trans_status() === false){
                $this->log_db_error($this->db->_error_number(), $this->db->_error_message());
                
                $this->db->trans_rollback();    
                
                return $this->get_log_db_errors();
                
            } else {
                //all is correcommit transaction then upload image
                $this->db->trans_commit();  
                
                return true;
            }
   }


}