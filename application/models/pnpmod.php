<?php
	class Pnpmod extends Ss_model {
	
		/*==========================================================
		*
		*				POLICE DATABASE OPERATIONS
		*
		*===========================================================*/
		
		//gets the list of all active and non-active pnp officers
		//NOTE: We only get the required information to be displayed in the table.
		public function get_all_pnp(){
			$array= array('pnp_id','first_name','last_name','middle_name','file_path',
				'rank','status');
			$rest = $this->fields_get('pnp_officer_tbl',$array);
		
			return $rest;
		}
		//gets information about the pnp given his pnp id.
		public function get_pnp($id){
			$params = array(
			
				'pnp_id' => $id,
			
			);
			$config = array(
			
				'table' => 'pnp_officer_tbl',
				'params' => $params,
				'limit' => 1,
				'offset' => 0
			);
			
			$data = $this->param_get($config);
			
			return $data;
			
		}
		//adds police to db
		public function insert_new_pnp($data){
           
            $to_return = true;
            
			$this->db->trans_begin();
			
			//add the file_paths
			$data['file_path'] = 'public/resources/photos/pnp/' . $data['pnp_id'] . '';
			$this->load->library('DateTimeHelper');
			//converts date input so that db can format it
			$parsed_date =$this->datetimehelper->parseto_db_date($data['member_since']);
			$data['member_since'] = $parsed_date;
            //wala lang to
            $data['has_account'] = 0;
			
            $row = array(
                'pnp_id' => $data['pnp_id'],
                'rank' => $data['pnp_id'],
                'first_name' => $data['first_name'],
                'middle_name' => $data['middle_name'],
                'last_name' => $data['last_name'],
                'rank' => $data['rank'],
                'gender' => $data['gender'],
                'member_since' => $data['member_since'],
                'address' => $data['address'],
                'contact_number' => $data['contact_number'],
                'email' => $data['email'],
                'file_path' => $data['file_path'],
                'has_account' => $data['has_account'],
                'status' => 'Active',
                'keywords' => '',
            );
                
         
			$this->normal_insert('pnp_officer_tbl',$row);
            $this->create_keywords($data);
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				$array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                
               
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
                //upload the image
                $this->load->library('FileHelper');   
                $img_succ = $this->filehelper->uploadFile($data['pnp_id'], 'pnp', 'userfile');	
            
                if($img_succ){
                    $to_return = $img_succ;
                    //all is correct, commit transaction then upload image
                    $this->db->trans_commit();
                    
                } else {
                   
                    $this->db->trans_rollback();
                }
			}
            
            return $to_return;	
		}
		//update pnp
		// NOTE: Hindi na natin need i-specify ang file path ng image kasi sa add na ginawa yon.
		public function update_pnp($data){
            $prev_id = $data['prev_id'];	
			
            $this->db->trans_begin();
			
			//set-up configuration for an update. 
			//See Ss_Model.php for parameter definitions.

			//unset prev_id to avoid errors
			unset($data['prev_id']);
			
            //reset file_path always
            $data['file_path'] = "public/resources/photos/pnp/" . $data['pnp_id']; 
            
			//parse to db_time
			$this->load->library('DateTimeHelper');
			//converts date input so that db can format it
			$parsed_date = $this->datetimehelper->parseto_db_date($data['member_since']);
			$data['member_since'] = $parsed_date;
			
			$config = array(
				'table' => 'pnp_officer_tbl',
				'column' => 'pnp_id',
				'column_value' => $prev_id,
				'data' => $data,	
			);
		
			
			$this->normal_update($config);
            $this->create_keywords($data);
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
			
			} else {
                
                //if not equals, then there was a change in ID, change file path too.
                if($prev_id !== $data['pnp_id']){
                    $this->load->library('FileManager');
                    $this->filemanager->rename_pnp_images($prev_id, $data['pnp_id']);
                }
				$this->db->trans_commit();
			}
            
        
            //ganito ko kagaling, return true kahit anong mangyari.
            return true;
			
		}
        
        public function create_keywords($data){
            $keywords = array();
            
            $keywords[] = $data['first_name'];
            $keywords[] = $data['last_name'];
            $keywords[] = $data['middle_name'];
            $keywords[] = $data['rank'];
            $keywords[] = $data['email'];
            $keywords[] = $data['status'];
            $keywords[] = $data['pnp_id'];
          
        
            $key_str = $this->create_keywords_str($keywords);
            
            $this->update_keywords($key_str, 'pnp_officer_tbl', array('pnp_id' => $data['pnp_id']));
            
        }
        
        private function create_keywords_str($keywords){
             $str = '';
            
            foreach($keywords as $key){
                $str .= $key . ' ';
            } 
            
            return $str;
        }
        
        private function update_keywords($str, $table, $conditions){
            $config['table'] = $table;
            $config['conditions'] = $conditions;
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'keywords',
                'param_2' =>  '"'.$str.'"',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $this->special_save($config);
        
        }
        //get cases in which a particular police acts as investigator
        public function get_as_investigator($id){
            $config['table'] = 'arrest_details_tbl';
            $config['cols'] = array('blotter_entry_nr');
            $config['order_arrange'] = 'DESC';
            $config['order_by'] = 'date_of_arrest';
            $config['limit'] = 5;
            $config['offset'] = 0;
            $config['conditions'] = array(
            
                'investigator_pnp_id' => $id,
            
            );
                  
            $results = $this->special_get($config);
            
            unset($config);
            //get case id
            $config['table'] = 'case_report_tbl';
            $config['cols'] = 'case_id';
            
            if($results){
                foreach($results as $res){
                    $config['conditions'] = array( 'blotter_entry_nr' => $res->blotter_entry_nr);

                    $case = $this->special_get($config)[0];
                    foreach($case as $key => $value){
                            $res->$key = $value;
                    }
                } 
            }
               
            
            return $results;
       
        }
        //get cases in which a particular police acts as arresting officer
        public function get_as_arresting($id){
            $config['table'] = 'arresting_officers_list_tbl';
            $config['cols'] = array('blotter_entry_nr');
            $config['order_arrange'] = 'DESC';
            $config['order_by'] = 'blotter_entry_nr';
            $config['limit'] = 5;
            $config['offset'] = 0;
            $config['conditions'] = array('arresting_officer_id' => $id);
            $results = $this->special_get($config);
            
            unset($config);
            //get case id
            $config['table'] = 'case_report_tbl';
            $config['cols'] = 'case_id';
            
            if($results){
                foreach($results as $res){
                    $config['conditions'] = array( 'blotter_entry_nr' => $res->blotter_entry_nr);

                    $case = $this->special_get($config)[0];
                    foreach($case as $key => $value){
                            $res->$key = $value;
                    }
                } 
            }
            
            return $results;
        }
		
		/*==========================================================
		*
		*				GROUPS DATABASE OPERATIONS
		*
		*===========================================================*/
		//adds group to db
		public function insert_new_group($data){
		
			$this->db->trans_begin();
			
			//the default value at first creation is 0
			$data['members'] = 0;
			
			$this->normal_insert('group_tbl',$data);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correcommit transaction then upload image
				$this->db->trans_commit();	
				
				return true;
			}
		}	
		
		//NOTE: We only get the required information to be displayed in the table.
		public function get_all_groups(){
			$config['table'] = 'group_tbl';
            $groups = $this->special_get($config);
            
            //get members
            $config['table'] = 'group_members_tbl';
            $config['cols'] = array('group_id');
            $config['distinct'] = FALSE;
            foreach($groups as $group){
                $config['conditions'] = array('group_id' => $group->group_id);
                $res = $this->special_get($config);
                $group->members = count($res);
            }
            
            return $groups;
		}
		//gets group_id and group_name
        //imp stands for important
        public function get_all_groups_imp(){
            $config['table'] = 'group_tbl';
            $config['cols'] = array('group_id', 'group_name');
            
            return $this->special_get($config);
        }
		public function get_group($id){
			$params = array(
			
				'group_id' => $id,
			
			);
			$config = array(
			
				'table' => 'group_tbl',
				'params' => $params,
				'limit' => 1,
				'offset' => 0
			);
			
			$data = $this->param_get($config);
			
		
			return $data;
		}
		
		public function update_group($data){
	
			$this->db->trans_begin();
			
			//set-up configuration for an update. 
			//See Ss_Model.php for parameter definitions.
			$config = array(
				'table' => 'group_tbl',
				'column' => 'group_id',
				'column_value' => $data['group_id'],
				'data' => $data,	
			);
		
			$this->normal_update($config);
			
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
			
			} else {
				//all is correct, commit transaction then upload image
				$this->db->trans_commit();
				
							
				return true;
			}
		}
		
		public function add_member($group_id, $batch_id){
			$this->db->trans_begin();
			
			$data['group_id'] = $group_id;
			$data['pnp_id'] = $batch_id;
			
			$this->normal_insert('group_members_tbl',$data);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correcommit transaction then upload image
				$this->db->trans_commit();	
				
				return true;
			}
			
		}
		
		//retrieve all members of a particular group
		public function retrieve_members($group_id){
		
			$this->db->select('*');
			$this->db->from('group_members_tbl');
			$this->db->join('pnp_officer_tbl', 'group_members_tbl.pnp_id = pnp_officer_tbl.pnp_id');
			$this->db->where('group_id', $group_id);
			
			$query = $this->db->get();
				
			return $this->return_data($query);
		}
		
		//retrieve policemen not member of a particular group
		public function retrieve_not_members($group_id){

			$query = $this->db->query("SELECT * FROM pnp_officer_tbl WHERE pnp_id NOT IN (SELECT pnp_id FROM group_members_tbl WHERE group_id = ?)", array($group_id));		
			
			$data = $query->result();
			
			return $data;
		}
		
		//retrieve the affiliations of a particular pnp
		public function get_affiliations($pnp_id){
			
			$this->db->select('*');
			$this->db->from('group_members_tbl');
			$this->db->join('group_tbl', 'group_members_tbl.group_id = group_tbl.group_id');
			$this->db->where('pnp_id', $pnp_id);
			
			$query = $this->db->get();
				
			$data =  $this->return_data($query);
			
			return $data;
			
		}
		
		//remove affiliation
		public function remove_affiliation($group_id, $batch_id){
			
			$this->db->trans_begin();
		
			$params = array(
				
				'group_id' => $group_id,
				'pnp_id' => $batch_id,
			
			);
			$config = array(
				
				'table' => 'group_members_tbl',
				'params' => $params,
			
			
			);
			
			$this->normal_delete($config);
			
			//if there are query errors or some sort of errors return error message
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				//all is correcommit transaction then upload image
				$this->db->trans_commit();	
				
				return true;
			}
		}
		
        //gets access type in user_account_tbl
        //deprecated
        function get_user($id){
            $config['table'] = 'user_account_tbl';
            $config['cols'] = array('access_type');
            $config['conditions'] = array('pnp_id' => $id);
            
            return $this->special_get($config);
            
        }
        
        function update_photo($data){        
            $this->load->library('FileHelper');
            return  $this->filehelper->uploadFile($data['pnp_id'], 'pnp');
        }
        
        function check_privileges($username){
            
            $config['table'] = 'users';
            $config['conditions'] = array('username' => $username);
            
            $account = $this->special_get($config)[0];
            
            if($account){
                $config['table'] = 'users_groups';
                $config['cols'] = 'group_id';
                $config['conditions'] = array('user_id' => $account->id);
                $fkey = $this->special_get($config)[0];

                //get group info
                $config['table'] = 'groups';
                $config['cols'] = 'name';
                $config['conditions'] = array('id' => $fkey->group_id);


                $account->group = $this->special_get($config)[0];
            } 
        
              
            return $account; 
        }
        
        function create_account($pnp_id){
            $username = $pnp_id;
            $password = $this->generateRandomString();

            $pnp = $this->get_pnp($pnp_id)[0];
            $email = $pnp->email;
            
            $additional_data = array(
                'first_name' => $pnp->first_name,
                'last_name' => $pnp->last_name,  
            );
            
            $group = array(); // Sets user to admin. No need for array('1', '2') as user is always set to member by default

            $result = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
            
            $mix['result'] = $result;
            $mix['password'] = $password;
            
            return $mix;
            /*$random_pass = $this->generateRandomString();
            //supply strategy
			$config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'account',
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'user_account_tbl',
				'column' => 'account_num',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('account_num')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->account_num;
			}
            
            $this->load->library('Incrementor', $config);			

			$next_id = $this->incrementor->increment($last_id);
            
            //actual creation of account
            $config['table'] = 'user_account_tbl';
            $account = array(
                'account_num' => $next_id,
                'pnp_id' => $pnp_id,
                'date_created' => date("Y-m-d"),
                'password' => $this->ihash_mo_baby($random_pass),
                'access_type' => 'read/write',
            );
        
            $config['data'] = $account;
            echo $random_pass;
            $this->special_save($config);
            $this->send_to_mail($random_pass);*/
            
        }
        function send_to_mail($random_pass){
            $to = "j.cfrane@gmail.com";
            $subject = "SSMIS Password";
            $txt = $random_pass;
            $headers = "From: j.cfrane@gmail.com" . "\r\n";

            $ret = mail($to,$subject,$txt,$headers);    
            echo $ret;
        }
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
		/*==================================================================
		*
		*					Report Generator Methods
		*
		*===================================================================*/
		public function report_all(){
		
			$this->load->library('PdfGenerator'); // Load library
			$this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
			
			$this->pdfgenerator->DefOrientation = 'L';
			$this->pdfgenerator->AliasNbPages();
			$this->pdfgenerator->AddPage();
			
			for($i = 0; $i <= 2; $i++){
				$this->pdfgenerator->Ln();
			}
			
			$headers = array('Batch ID', 'Rank', 'Full Name', 'Remarks', 'Picture');
			$arrayCol = array(55,55,55,55,55);
			
			$this->pdfgenerator->createTableHeader($headers, $arrayCol);
			
			
			$this->pdfgenerator->Ln();
			
			$data = $this->get_all_pnp();
		
			foreach($data as $police){
				for($i=0; $i <= 4; $i++){			
					switch($i){
						case 0:
							$this->pdfgenerator->Cell(55,20, $police->pnp_id ,1);
							break;
						case 1:
							$this->pdfgenerator->Cell(55,20, $police->rank ,1);
							break;
						case 2:
							$full_name = $police->last_name . ', ' . $police->first_name . ' ' . substr($police->middle_name,0,1) . '.';
							$this->pdfgenerator->Cell(55,20, $full_name  ,1);
							break;
						case 3:
							$this->pdfgenerator->Cell(55,20, $police->status ,1);
							break;
						case 4:
						 
							$this->pdfgenerator->Cell(55,20, 
								$this->pdfgenerator->Image($police->file_path,$this->pdfgenerator->getX() + 20,$this->pdfgenerator->getY(),20)
								,1,'C');					
							break;
					}
				}
				$this->pdfgenerator->Ln();			
			}
		
			$this->pdfgenerator->Output();
		}
	}
	