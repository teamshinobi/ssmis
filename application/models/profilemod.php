<?php
	class Profilemod extends Ss_model {
	
		public function insert_message($data){
			$this->db->trans_begin();

			$this->normal_insert('messages_tbl',$data);

			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
				
			} else {
				$this->db->trans_commit();	
				
				return true;
			}
		}
		
		public function get_inbox($id){
			$params = array(
				'inbox_status' => 'recieved',
				'recipient' => $id,
			);
			$config = array(
				'table' => 'messages_tbl',
				'order_arrange' => 'desc',
				'order_by' => 'msg_date',
				'conditions' => $params,
			);
			
			$data = $this->special_get($config);

			return $data;
		}
		public function get_drafts($id){
			$params = array(
				'draft_status' => 'saved',
				'sender' => $id,
				
			);
			$config = array(
				'table' => 'messages_tbl',
				'limit' => null,
				'offset' => null,
				'order_arrange' => 'desc',
				'order_by' => 'msg_date',
				
				'conditions' => $params,
			);
			
			$data = $this->special_get($config);

			return $data;
		}
		public function get_sents($id){
			$params = array(
				'sent_item_status' => 'sent',
				'sender' => $id,
			);
			
			$config = array(
				'table' => 'messages_tbl',
				'limit' => null,
				'offset' => null,
				'order_arrange' => 'desc',
				'order_by' => 'msg_date',
				'conditions' => $params,
			);
			
			$data = $this->special_get($config);

			return $data;
		}
		
		public function get_message_by_id($id){
			$params = array(
				'msg_id' => $id,
			);
			$config = array(
				'table' => 'messages_tbl',
				'params' => $params,
				'limit' => null,
				'offset' => null
			);
			
			$data = $this->param_get($config);

			return $data;
		}
		
		public function update_message($data){
			$this->db->trans_begin();
			$data = array($_POST['column'] => $_POST['status']);
			
			$config = array(
				'table' => 'messages_tbl',
				'column' => 'msg_id',
				'column_value' => $_POST['id'],
				'data' => $data,	
			);
			$this->normal_update($config);
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
			
			} else {

				$this->db->trans_commit();
				return true;
			}
		}
		
		public function update_draft($data){
			$this->db->trans_begin();
			
			$config = array(
				'table' => 'messages_tbl',
				'column' => 'msg_id',
				'column_value' => $_POST['id'],
				'data' => $data,	
			);
			$this->normal_update($config);
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				
				$this->db->trans_rollback();	
				
				return $this->get_log_db_errors();
			
			} else {

				$this->db->trans_commit();
				return true;
			}
		}
		
		public function delete_message($data){
			$this->db->trans_begin();
			$params = array(
				'msg_id' => $_POST['id'],
			);
			$config = array(
				'table' => 'messages_tbl',
				'params' => $params,
			);
			$this->normal_delete($config);
			if($this->db->trans_status() === false){
				$this->log_db_error($this->db->_error_number(), $this->db->_error_message());
				$this->db->trans_rollback();	
				return $this->get_log_db_errors();
			} else {
				$this->db->trans_commit();
				return true;
			}
			
		}
		public function get_pnp_name($id){
			$params = array(
				'pnp_id' => $id,
			);
			$col = array(
				'last_name',
				'first_name'
			);

			$config = array(
				'cols' => $col,
				'table' => 'pnp_officer_tbl',
				'limit' => null,
				'offset' => null,
				'conditions' => $params,
			);
			
			$data = $this->special_get($config);
			
			return $data;
			
		}
	}
	
