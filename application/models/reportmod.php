<?php
class Reportmod extends Ss_model {
    
    private $plants = 0;
    private $seedlings = 0;
    private $seeds = 0;
    private $stalks = 0;
    private $marijuana = 0;
    private $cocaine = 0;
    private $shabu = 0;
    private $syrup = 0;
    private $dried = 0;
    private $tbags = 0;
    private $others = 0;
    private $bb = 0;
    private $search = 0;
    private $warrant = 0;
    private $chanced = 0;
    private $incidental = 0;
    private $arrested = 0;

    public function statistics_dangerous_drugs($bMonth, $eMonth, $year, $use_current = false){
        
       
        $drugs_array = array('Plants', 'Seedlings', 'Seeds', 'Stalks',
                            'Marijuana', 'Cocaine', 'Shabu', 'Syrup',
                             'Dried Leaves', 'TBags',
                            );
        //===================END DECLARATIONS===========================//
       
        $this->load->library('DateTimeHelper');
        
        if($use_current){
           
            $bMonth = $this->datetimehelper->get_current_month();
            $year = $this->datetimehelper->get_current_year();
        
            $bMonthDays =  $this->datetimehelper->get_num_days($bMonth, $year);
            
            $bdate = $year . '-' . str_pad($bMonth, 2,'0',STR_PAD_LEFT) . '-' . '1';
            $edate = $year . '-' . str_pad($bMonth, 2,'0',STR_PAD_LEFT) . '-' . $bMonthDays;
        } else {
             //gets the integer value of begin month
            $bMonthInt = $this->datetimehelper->get_month_int_value($bMonth);
            $bMonthDays =  $this->datetimehelper->get_num_days($bMonthInt, $year);

            //create the begin date
            $bdate = $year . '-' . str_pad($bMonthInt, 2,'0',STR_PAD_LEFT) . '-' . '1';

            //gets the integer value of end month
            $eMonthInt = $this->datetimehelper->get_month_int_value($eMonth);
            $eMonthDays =  $this->datetimehelper->get_num_days($eMonthInt, $year);

            //create the end date
            $edate = $year . '-' . str_pad($eMonthInt, 2,'0',STR_PAD_LEFT) . '-' . $eMonthDays;
        
        
        }
       
        
        //===================END PARSING DATE===========================//
        
        $config['table'] = 'arrest_details_tbl';
        $config['cols'] = array('blotter_entry_nr');
        $config['conditions'] = array(
            'date_of_arrest >=' => $bdate,
            'date_of_arrest <=' => $edate,
        );
        $results = $this->special_get($config);
        $statistics = new stdClass();
        
        //loop each item to get case id
        
        if($results){
            
            $statistics->no_case = count($results);
            
            foreach($results as $res){
                $res->case_details = $this->get_case_details($res->blotter_entry_nr);
                $res->drugs_occurances = $this->get_drugs_occurances($res->case_details->case_id);
                $res->arrested_violators = $this->get_violators($res->blotter_entry_nr);
                
                foreach($res->drugs_occurances as $drug){
                    if(in_array($drug->name,$drugs_array)){
                       $this->increment_occurances($drug->name);
                    } else {
                        $this->others++;
                    }
                }
                    
            }
            $statistics->plants = $this->plants;
            $statistics->marijuana = $this->marijuana;
            $statistics->seedlings = $this->seedlings;
            $statistics->seeds = $this->seeds;
            $statistics->stalks = $this->stalks;
            $statistics->cocaine = $this->cocaine;
            $statistics->shabu = $this->shabu;
            $statistics->syrup = $this->syrup;
            $statistics->dried = $this->dried;
            $statistics->tbags = $this->tbags;
            $statistics->others = $this->others;
            
            $statistics->bb = $this->bb;
            $statistics->search = $this->search;
            $statistics->warrant = $this->warrant;
            $statistics->chanced = $this->chanced;
            $statistics->incidental = $this->incidental;

            $statistics->arrested = $this->arrested;
        }
        
        
        return $statistics;
    }
    
    private function increment_occurances($name){
      
        switch($name){
            case 'Plants':
                $this->plants++;
                break;
            case 'Seedlings':
                $this->seedlings++;
                break;
            case 'Seeds':
                $this->seeds++;
                break;
            case 'Stalks':
                $this->stalks++;
                break;
            case 'Marijuana':
                $this->marijuana++;
                break; 
            case 'Cocaine':
                $this->cocaine++;
                break;
            case 'Shabu':
                $this->shabu++;
                break;
            case 'Syrup':
                $this->syrup++;
                break;
            case 'Dried Leaves':
                $this->dried++;
                break;
            case 'TBags':
                $this->tbags++;
                break;
            default:
                $this->others++;
        }
    }
    
    private function get_drugs_occurances($case_id){
        $config['table'] = 'narcotics_item_seized_tbl';
        $config['cols'] = array('narcotics_id');
        $config['conditions'] = array(
            'case_id' => $case_id,
        );
        
        $drugs = $this->special_get($config);
        
        foreach($drugs as $drug){
            $info = $this->get_narcotics_info($drug->narcotics_id);
            foreach($info as $key => $value){
                $drug->$key = $value;
            }
        }
       
        return $drugs;
    }
    
    private function get_narcotics_info($id){
        $config['table'] = 'narcotics_tbl';
        $config['cols'] = array('name');
        $config['conditions'] = array(
            'narcotics_id' => $id,
        );
        
        return $this->special_get($config)[0];
        
    }

    private function get_case_details($blotter){
        $config['table'] = 'case_report_tbl';
        $config['cols'] = array('case_id','nature');
        $config['conditions'] = array(
            'blotter_entry_nr' => $blotter,
        );
       
        $case = $this->special_get($config)[0];
        switch($case->nature){
            case 'Buy Bust':
                $this->bb++;
                break;
            case 'With Warrant of Arrest':
                $this->warrant++;
                break;
            case 'Search Warrant':
                $this->search++;
                break;
            case 'On Chanced':
                $this->chanced++;
                break;
            case 'Incidental Arrest':
                $this->incidental++;
                break;
        }
        return $case;
    }
    private function get_violators($blotter){
        $config['table'] = 'arrest_booking_form_tbl';
        $config['cols'] = array('violator_id');
        $config['conditions'] = array(
            'blotter_entry_nr' => $blotter,
        );

        $violators = $this->special_get($config)[0];

        foreach($violators as $viol) {
            $this->arrested++;
        }
        
        return $violators;
    }
    
    //====================END DRUG STATISTICS===========================//
    
    public function yearly_acc_report($year, $use_year = FALSE){
        $this->load->library('DateTimeHelper');
        
        if($use_year){
            $year = $this->datetimehelper->get_current_year();
        }
        
        //12 is fixed for december
        $days = $this->datetimehelper->get_num_days(12, $year);
        
        $start_date = $year . '-' . str_pad(01, 2,'0',STR_PAD_LEFT) . '-' . '1';
        $end_date =  $year . '-' . str_pad(12, 2,'0',STR_PAD_LEFT) . '-' . $days;
        
        $config['table'] = 'arrest_details_tbl';
        $config['cols'] = array('blotter_entry_nr','date_of_arrest','time_of_arrest',
                               'street_name', 'barangay_id','city_name');
        $config['conditions'] = array(
            'date_of_arrest >=' => $start_date,
            'date_of_arrest <=' => $end_date,
        );
        $results = $this->special_get($config);
        
        if($results){
            foreach($results as $res){

                //get barangay name
                $res->barangay = $this->get_barangay_name($res->barangay_id);
                unset($res->barangay_id);

                //get case info;
                $casereport = $this->get_case_id($res->blotter_entry_nr);
                foreach($casereport as $key => $value ){
                    $res->$key = $value;
                } 

                $res->violators = $this->get_case_violators($res->blotter_entry_nr);
                $res->items = $this->get_case_items($res->case_id);
                $res->nitems = $this->get_case_nitems($res->case_id);

                //gets cumulative violations
                $res->violations = $this->get_cum_violations($res->violators);
            }
        }
       
           
        return $results;
    }

    public function month_acc_report($month, $year, $use_month = FALSE, $use_year = FALSE){
        $this->load->library('DateTimeHelper');
        
        $month = $this->datetimehelper->get_month_int_value($month);
        
        if($use_month){
            $month = $this->datetimehelper->get_current_month();
        } 
        if($use_year){
            $year = $this->datetimehelper->get_current_year();
        }
      
        $days = $this->datetimehelper->get_num_days($month, $year);
        $start_date = $year . '-' . str_pad($month, 2,'0',STR_PAD_LEFT) . '-' . '1';
        $end_date =  $year . '-' . str_pad($month, 2,'0',STR_PAD_LEFT) . '-' . $days;
        
        $config['table'] = 'arrest_details_tbl';
        $config['cols'] = array('blotter_entry_nr','date_of_arrest','time_of_arrest',
                               'street_name', 'barangay_id','city_name');
        $config['conditions'] = array(
            'date_of_arrest >=' => $start_date,
            'date_of_arrest <=' => $end_date,
        );
        
        $results = $this->special_get($config);
        if($results){
            foreach($results as $res){

                //get barangay name
                $res->barangay = $this->get_barangay_name($res->barangay_id);
                unset($res->barangay_id);

                //get case info;
                $casereport = $this->get_case_id($res->blotter_entry_nr);
                foreach($casereport as $key => $value ){
                    $res->$key = $value;
                } 

                $res->violators = $this->get_case_violators($res->blotter_entry_nr);
                $res->items = $this->get_case_items($res->case_id);
                $res->nitems = $this->get_case_nitems($res->case_id);

                //gets cumulative violations
                $res->violations = $this->get_cum_violations($res->violators);
            }
        }
      
        
        
        
        return $results;
    }
    
    private function get_cum_violations($violators){
        $cum_charges = array();
      
        foreach($violators as $violator){
  
            
            foreach($violator->charges as $charge){
                $bool = in_array($charge, $cum_charges); // true
                if(!$bool){
                    $cum_charges[] = $charge;
                }
            } 
        
        }  
        
        return $cum_charges;
    }
    
    private function get_case_violators($blotter){
        $config['table'] = 'arrest_booking_form_tbl';
        $config['cols'] = array('violator_id');
        $config['conditions'] = array('blotter_entry_nr' => $blotter);

        $violators = $this->special_get($config);

        foreach($violators as $violator){
            $v_config['table'] = 'violators_tbl';
            $v_config['cols'] = array('first_name','last_name','middle_name','alias');
            $v_config['conditions'] = array('violator_id' => $violator->violator_id);

            $info = $this->special_get($v_config)[0];
            //loop info to append as property
            foreach($info as $key => $value){
                $violator->$key = $value;
            }

            //get cases, at this point violators info is complete
            $violator->charges = $this->get_violator_charges($violator->violator_id, $blotter);

        }

            return $violators;
        
    }
    
     private function get_violator_charges($violator_id,$blotter){
           
            $v_config['table'] = 'arrest_booking_form_tbl';
            $v_config['conditions'] = array(
                'violator_id' => $violator_id, 
                'blotter_entry_nr' => $blotter
            );

            $charges = $this->special_get($v_config);
          
            //get charges info
            $c_config['table'] = 'charges_tbl';
            foreach($charges as $charge){
                $c_config['conditions'] = array('charge_id' => $charge->charge_id);
                $info = $this->special_get($c_config)[0];
                
                //loop info to append as property
                foreach($info as $key => $value){
                    $charge->$key = $value;
                }
                
                unset($charge->blotter_entry_nr);
                unset($charge->violator_id);
            } 
            
            return $charges;
    }
     private function get_case_items($case_id){ 
            
            $v_config['table'] = 'item_seized_tbl';
            $v_config['conditions'] = array(
                'case_id' => $case_id,      
            );
            
            $items = $this->special_get($v_config);
            //get violators for each_item

            if($items){
                foreach($items as $item){
                    $v_config['table'] = 'violator_item_tbl';
                    $v_config['cols'] = array('violator_id');
                    $v_config['conditions'] = array(
                        'item_seized_id' => $item->item_seized_id,      
                    );    

                    $item->violators = $this->special_get($v_config); 

                    //gets violators full name
                    if($item->violators){
                        foreach($item->violators as $violator){
                            $v_config['table'] = 'violators_tbl';
                            $v_config['cols'] = array('violator_id','first_name','last_name');
                            $v_config['conditions'] = array(
                                'violator_id' => $violator->violator_id,      
                            );  

                            $info = $this->special_get($v_config)[0];
                            foreach($info as $key => $value){
                                 $violator->$key = $value;
                            }
                        } 
                    }
                }   
            }
                 
            return $items;
        }
        
        
        private function get_case_nitems($case_id){ 
            $v_config['table'] = 'narcotics_item_seized_tbl';
            $v_config['conditions'] = array(
                'case_id' => $case_id,      
            );
            
            $items = $this->special_get($v_config);
            //get violators for each_item
            $v_config['table'] = 'violator_narcotics_item_tbl';
           
            if($items){
                foreach($items as $item){
                    $v_config['cols'] = array('violator_id');
                    $v_config['conditions'] = array(
                        'narcotics_seized_id' => $item->narcotics_seized_id,      
                    );    

                    $item->violators = $this->special_get($v_config); 

                    if($item->violators != false){
                        foreach($item->violators as $violator){
                            $config['table'] = 'violators_tbl';
                            $config['cols'] = array('violator_id','first_name','last_name');
                            $config['conditions'] = array(
                                'violator_id' => $violator->violator_id,      
                            );  

                            $info = $this->special_get($config)[0];
                            foreach($info as $key => $value){
                                 $violator->$key = $value;
                            }
                        } 
                    }



                    //gets narcotics info
                    $n_config['table'] = 'narcotics_tbl';
                    $n_config['cols'] = array('name');
                    $n_config['conditions'] = array('narcotics_id' => $item->narcotics_id);

                    $item->narcotics = $this->special_get($n_config)[0];
                    $item->narcotics->narcotics_id = $item->narcotics_id;
                    //gamit kasi sa front end ung index na narcotics_name potaena.
                    $item->narcotics->narcotics_name = $item->narcotics->name;
                    unset($item->narcotics->name);
                    unset($item->narcotics_id);

                } 
                
            }
            return $items;

        }
    //case id daw pero andaming nireturn. Talino tang ina!
    public function get_case_id($blotter){
        $config['table'] = 'case_report_tbl';
        $config['cols'] = array('case_id','is_number','operation_name','nature','progress','cc','cc_judge','status_of_evidences');
        $config['conditions'] = array(
            'blotter_entry_nr' => $blotter,
        );
        
        $case = $this->special_get($config)[0];
       
        return $case;
    }
    
    public function get_barangay_name($barangay_id){
        $config['table'] = 'barangay_tbl';
        $config['cols'] = array('barangay_name');
        
        
        $config['conditions'] = array(
            'barangay_id' => $barangay_id,
        );
            
        $brgy = $this->special_get($config)[0];
        
        return $brgy->barangay_name;
    }
    
    //gets admin
    private function get_chief(){
        $CI =& get_instance(); 
        $CI->load->model('settingsmod');
           
        $admin = $CI->settingsmod->get_station_information()->chief;      
        
        return $admin;
    }
    
    
    //==============================================================================================>
    //                                      Monthly Accomplishment
    //==============================================================================================>

    public function monthly_accomplishment_pdf($month,$year){
        $data = $this->month_acc_report($month, $year);
      
        if($data){    
        foreach($data as $acc){
            
            //-----------------------------------------------------------------------------------------------
            //                                             Labels
            //-----------------------------------------------------------------------------------------------

            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
            $this->pdfgenerator->DefOrientation = 'L';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;

            $this->pdfgenerator->AddPage();			
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->Cell(0,6,'MONTHLY ACCOMPLISHMENT REPORT',0,1,'C');

            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->Cell(0,6,'Period Covered: ' . ucfirst($month) . ' ' . $year,0,1,'C');

            $this->pdfgenerator->ln(10);

            $widthArray = [35,25,25,25,28,75,28,30,24,30,25,25,25];

            $this->pdfgenerator->SetFont('Arial','B',11);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],7,'NAME OF ACCUSED',1,0,'C');	
            $this->pdfgenerator->MultiAlignCell($widthArray[1],7,'DATE/TIME OF ARREST',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[2],7,'PLACE OF ARREST',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[3],7,'OFFENSE CHARGED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[4],7,'OPERATION CONDUCTED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[5],7,'DRUG EVIDENCE SEIZED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6],7,'STATUS OF EVIDENCE',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7],7,'I.S#/ PROSECUTOR',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[8],7,'CC# JUDGE',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[9],7,'STATUS OF CASE',1,0,'C');

            $this->pdfgenerator->SetXY(148,95);
            $this->pdfgenerator->Cell($widthArray[10],7,'Shabu',1,0,'C');

            $this->pdfgenerator->SetXY(173,95);
            $this->pdfgenerator->Cell($widthArray[10],7,'Marijuana',1,0,'C');

            $this->pdfgenerator->SetXY(198,95);
            $this->pdfgenerator->Cell($widthArray[10],7,'Others',1,0,'C');

            $this->pdfgenerator->Ln();

            $y = $this->pdfgenerator->GetY();
            $x = $this->pdfgenerator->GetX();

            $this->pdfgenerator->SetX($x);
		
            for($j = 0; $j<=0; $j++){               
				for($i=0; $i <= 9; $i++){
                    $this->pdfgenerator->Cell($widthArray[$i],60,'',1,0,'C');
				}					
			}
            $this->pdfgenerator->SetX(148);
            for($j = 0; $j<=0; $j++){               
				for($i=10; $i <= 12; $i++){
                    $this->pdfgenerator->Cell($widthArray[$i],60,'',1,0,'C');
				}					
			}
            
            //-----------------------------------------------------------------------------------------------
            //                                             Datas
            //-----------------------------------------------------------------------------------------------
            $this->pdfgenerator->SetFont('Arial','B',10);
            $violatorArr = [];
            $x = 10;
            $this->pdfgenerator->SetX($x);
            $i = 1;
            foreach($acc->violators as $v){
                $violatorArr[] = $i . '.  ' . $v->first_name . ' ' . strtoupper(substr($v->middle_name, 0,1)) . '. ' . $v->last_name;
                $i++;
            }
            $violators = implode("\n", $violatorArr);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],8,$violators,0,0);
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[1],8,$this->datetimehelper->parseto_standard_date($acc->date_of_arrest) . "\n" . $this->datetimehelper->parseto_system_time($acc->time_of_arrest),0,0);
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[2],8,$acc->street_name . ' Brgy. ' . $acc->barangay . ' ' . $acc->city_name, 0,0);
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $violationArr = [];
            
            foreach($acc->violations as $vs){
                $violationArr[] = substr($vs->section, -1);                
            }
            $violations = implode(', ', $violationArr);
            $this->pdfgenerator->MultiAlignCell($widthArray[3],8,'Violation of' . "\n". 'RA 9165' . "\n" . 'Sec. ' . $violations,0,0, 'C');
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[4],8,$acc->operation_name,0,0,'C');
            
            $this->pdfgenerator->SetX($x + 103);                        
                    
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[6],8,$acc->status_of_evidences,0,0,'C'); //--------------------------Status Evidence
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[7],8,$acc->is_number,0,0,'C');
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $judge = $acc->cc_judge;
            $cc = $acc->cc;
            $cj = "";
            if(!empty($judge) && !empty($cc)){
                $cj = $judge . '/' . $cc;
            } else if(!empty($cc)){
                $cj = $cc;
            } else {
                $cj = $judge;
            }
            
            $this->pdfgenerator->MultiAlignCell($widthArray[8],8,$cj,0,0,'C'); //--------------------------Judge
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[6],8,$acc->progress,0,0,'C');
            
            $this->pdfgenerator->SetX($x);
            
            $shabuX = 148;
            $marijuana = 173;
            $othersX = 198;
            
            if($acc->nitems){
                
                foreach($acc->nitems as $n){
                    if($n->narcotics->narcotics_name == 'Shabu'){
                        $this->pdfgenerator->SetX($shabuX);
                        $this->pdfgenerator->Cell($widthArray[10],7,chr(149) . ' ' . $n->quantity,0,1,'C');
                    }elseif($n->narcotics->narcotics_name == 'Dried Leaves'){
                        $this->pdfgenerator->SetX($marijuana);
                        $this->pdfgenerator->Cell($widthArray[10],7,chr(149) . ' ' . $n->quantity,0,1,'C');
                    }else{
                        $this->pdfgenerator->SetX($othersX);
                        $this->pdfgenerator->Cell($widthArray[10],7,chr(149) . ' ' . $n->quantity,0,1,'C');
                    }
                }        
            }
            
        }
        $user = $this->ss_user();
        $total_pages = $this->pdfgenerator->PageNo();
        $rank = $this->get_rank_acronym($user->rank);
        $this->pdfgenerator->ModFooter($rank . ' ' . $user->first_name . ' ' . $user->last_name,$total_pages, $this->get_chief());
            
        $total_pages = $this->pdfgenerator->PageNo(); 
		
		
        $this->pdfgenerator->Output('Monthly_Accomplishment_' . $year . '.pdf', 'I');
            
        }
    }
    
    //==============================================================================================>
    //                                      Yearly Accomplishment
    //==============================================================================================>

    public function yearly_accomplishment_pdf($year){
        $data = $this->yearly_acc_report($year);
        if($data){
        $user = $this->ss_user();
        foreach($data as $acc){
            
            //-----------------------------------------------------------------------------------------------
            //                                             Labels
            //-----------------------------------------------------------------------------------------------

            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
            $this->pdfgenerator->DefOrientation = 'L';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
            

            $this->pdfgenerator->AddPage();			
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->Cell(0,6,'YEARLY ACCOMPLISHMENT REPORT',0,1,'C');

            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->Cell(0,6,'Period Covered: '. $year,0,1,'C');

            $this->pdfgenerator->ln(10);

            $widthArray = [35,25,25,25,28,75,28,30,24,30,25,25,25];

            $this->pdfgenerator->SetFont('Arial','B',11);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],7,'NAME OF ACCUSED',1,0,'C');	
            $this->pdfgenerator->MultiAlignCell($widthArray[1],7,'DATE/TIME OF ARREST',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[2],7,'PLACE OF ARREST',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[3],7,'OFFENSE CHARGED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[4],7,'OPERATION CONDUCTED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[5],7,'DRUG EVIDENCE SEIZED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6],7,'STATUS OF EVIDENCE',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7],7,'I.S#/ PROSECUTOR',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[8],7,'CC# JUDGE',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[9],7,'STATUS OF CASE',1,0,'C');

            $this->pdfgenerator->SetXY(148,95);
            $this->pdfgenerator->Cell($widthArray[10],7,'Shabu',1,0,'C');

            $this->pdfgenerator->SetXY(173,95);
            $this->pdfgenerator->Cell($widthArray[10],7,'Marijuana',1,0,'C');

            $this->pdfgenerator->SetXY(198,95);
            $this->pdfgenerator->Cell($widthArray[10],7,'Others',1,0,'C');

            $this->pdfgenerator->Ln();

            $y = $this->pdfgenerator->GetY();
            $x = $this->pdfgenerator->GetX();

            $this->pdfgenerator->SetX($x);
		
            for($j = 0; $j<=0; $j++){               
				for($i=0; $i <= 9; $i++){
                    $this->pdfgenerator->Cell($widthArray[$i],60,'',1,0,'C');
				}					
			}
            $this->pdfgenerator->SetX(148);
            for($j = 0; $j<=0; $j++){               
				for($i=10; $i <= 12; $i++){
                    $this->pdfgenerator->Cell($widthArray[$i],60,'',1,0,'C');
				}					
			}
            
            //-----------------------------------------------------------------------------------------------
            //                                             Datas
            //-----------------------------------------------------------------------------------------------
            $this->pdfgenerator->SetFont('Arial','B',10);
            $violatorArr = [];
            $x = 10;
            $this->pdfgenerator->SetX($x);
            $i = 1;
            foreach($acc->violators as $v){
                $violatorArr[] = $i . '.  ' . $v->first_name . ' ' . strtoupper(substr($v->middle_name, 0,1)) . '. ' . $v->last_name;
                $i++;
            }
            $violators = implode("\n", $violatorArr);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],8,$violators,0,0);
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[1],8,$this->datetimehelper->parseto_standard_date($acc->date_of_arrest) . "\n" . $this->datetimehelper->parseto_system_time($acc->time_of_arrest),0,0);
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[2],8,$acc->street_name . ' Brgy. ' . $acc->barangay . ' ' . $acc->city_name, 0,0);
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $violationArr = [];
            
            foreach($acc->violations as $vs){
                $violationArr[] = substr($vs->section, -1);                
            }
            $violations = implode(', ', $violationArr);
            $this->pdfgenerator->MultiAlignCell($widthArray[3],8,'Violation of' . "\n". 'RA 9165' . "\n" . 'Sec. ' . $violations,0,0, 'C');
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[4],8,$acc->operation_name,0,0,'C');
            
            $this->pdfgenerator->SetX($x + 103);                        
                    
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[6],8,$acc->status_of_evidences,0,0,'C'); //--------------------------Status Evidence
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[7],8,$acc->is_number,0,0,'C');
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $judge = $acc->cc_judge;
            $cc = $acc->cc;
            $cj = "";
            if(!empty($judge) && !empty($cc)){
                $cj = $judge . '/' . $cc;
            } else if(!empty($cc)){
                $cj = $cc;
            } else {
                $cj = $judge;
            }
            $this->pdfgenerator->MultiAlignCell($widthArray[8],8,$cj,0,0,'C'); //--------------------------Judge
            
            $x = $this->pdfgenerator->GetX();
            $this->pdfgenerator->SetX($x);
            $this->pdfgenerator->MultiAlignCell($widthArray[6],8,$acc->progress,0,0,'C');
            
            $this->pdfgenerator->SetX($x);
            
            $shabuX = 148;
            $marijuana = 173;
            $othersX = 198;
            
            if($acc->nitems){
                
                foreach($acc->nitems as $n){
                    if($n->narcotics->narcotics_name == 'Shabu'){
                        $this->pdfgenerator->SetX($shabuX);
                        $this->pdfgenerator->Cell($widthArray[10],7,chr(149) . ' ' . $n->quantity,0,1,'C');
                    }elseif($n->narcotics->narcotics_name == 'Marijuana'){
                        $this->pdfgenerator->SetX($marijuana);
                        $this->pdfgenerator->Cell($widthArray[10],7,chr(149) . ' ' . $n->quantity,0,1,'C');
                    }else{
                        $this->pdfgenerator->SetX($othersX);
                        $this->pdfgenerator->Cell($widthArray[10],7,chr(149) . ' ' . $n->quantity,0,1,'C');
                    }
                }        
            }
            
        }
        $total_pages = $this->pdfgenerator->PageNo(); 
        $rank = $this->get_rank_acronym($user->rank);

        $this->pdfgenerator->ModFooter($rank . ' ' . $user->first_name . ' ' . $user->last_name,$total_pages, $this->get_chief());
            
        $this->pdfgenerator->Output('Monthly_Accomplishment_' . $year . '.pdf', 'I');
        } //end if
    }
    
    //==============================================================================================>
    //                                      Statistics
    //==============================================================================================>
    public function has_props($obj){
        // test wrapped into a function
        foreach( $obj as $x ) 
            return true;
        return false;

    }
    public function statistic_pdf($bMonth, $eMonth, $year){
        $data = $this->statistics_dangerous_drugs($bMonth, $eMonth, $year);
        $month = "";
        if($bMonth == $eMonth){
            $month = $bMonth;
        }else{
            $month = strtoupper($bMonth) . " - " . strtoupper($eMonth);
        }
        $user = $this->ss_user();
        
        $rank = $this->get_rank_acronym($user->rank);
          
        if($this->has_props($data)){
                   
            //-----------------------------------------------------------------------------------------------
            //                                             Labels
            //-----------------------------------------------------------------------------------------------

            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folderonth
            $this->pdfgenerator->DefOrientation = 'L';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
            
            $this->pdfgenerator->AddPage();         
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->Cell(0,6,'STATISTICS ON DANGEROUS / PROHIBITED DRUGS',0,1,'C');

            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->Cell(0,6,'Period Covered: '. $month . ' ' .$year,0,1,'C');

            $this->pdfgenerator->ln(10);

            $widthArray = [15,110,25,85,65,25,20,15,16.25];
            $height = 15;

            $this->pdfgenerator->SetX(15);

            $this->pdfgenerator->SetFont('Arial','B',11);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],$height,'',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[1],$height,'OPERATION CONDUCTED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[2],10,'NUMBER OF' . "\n" . 'PERSON ARRESTED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[3],$height,'MARIJUANA CONFISCATED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[4],$height,'OTHER DRUG/S CONFISCATED',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[5],10,'CASE FILED IN COURT',1,1,'C');
                        
            $this->pdfgenerator->SetY(103);
            $this->pdfgenerator->SetX(15);

            $this->pdfgenerator->SetFont('Arial','',8);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],7.5,'Unit/ Office',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6],5,'Number of Cases Reported',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-7,7.5,'Buy Bust',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-3,7.5,'Warrant of Arrest',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,7.5,'Search Warrant',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,$height,'Chanced',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,$height,'Incidental',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,7.5,'Total Operation',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[2],10,'',0,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-2,$height,'Plants',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7]+3,$height,'Seedlings',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-1,$height,'Seeds',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-1,$height,'Stalks',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-2,7.5,'Dried Leaves',1,0,'C');              
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-2,$height,'TBags',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height,'Others',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height,'Cocaine',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height,'Shabu',1,0,'C');
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height,'Syrup',1,1,'C');

            //-----------------------------------------------------------------------------------------------
            //                                             Datas
            //-----------------------------------------------------------------------------------------------

            $this->pdfgenerator->SetX(15);
            $this->pdfgenerator->SetFillColor(88,193,63);
            $this->pdfgenerator->SetFont('Arial','B',10);
            $this->pdfgenerator->MultiAlignCell($widthArray[0],10,'SAID SOTG',1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[6],$height+5,$data->no_case,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-7,$height+5,$data->bb,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-3,$height+5,$data->warrant,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,$height+5,$data->search,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,$height+5,$data->chanced,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,$height+5,$data->incidental,1,0,'C', true);

            $total = $data->bb + $data->warrant + $data->search + $data->chanced + $data->incidental;
            $this->pdfgenerator->MultiAlignCell($widthArray[6]-5,$height+5,$total,1,0,'C', true);

            $this->pdfgenerator->MultiAlignCell($widthArray[2],$height+5,$data->arrested,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-2,$height+5,$data->plants,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[7]+3,$height+5,$data->seedlings,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-1,$height+5,$data->seeds,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-1,$height+5,$data->stalks,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-2,$height+5,$data->dried,1,0,'C', true);            
            $this->pdfgenerator->MultiAlignCell($widthArray[7]-2,$height+5,$data->tbags,1,0,'C', true);

            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height+5,$data->others,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height+5,$data->cocaine,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height+5,$data->shabu,1,0,'C', true);
            $this->pdfgenerator->MultiAlignCell($widthArray[8],$height+5,$data->syrup,1,0,'C', true);

            $this->pdfgenerator->MultiAlignCell($widthArray[5],$height+5,$data->no_case,1,1,'C', true);

            $this->pdfgenerator->StatFooter($rank . ' ' . $user->first_name . ' ' . $user->last_name, $this->get_chief());
          
            $this->pdfgenerator->Output('Dangerous Drugs Statistics' . $year . '.pdf', 'I');
        } else {
            echo 'No data available.';
        }
    }
    
}