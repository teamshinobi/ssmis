<?php

	class Formsmod extends Ss_model {
         
        public function mark_as_undenied($id,$table,$condition){
            
            $config['table'] = $table;
            $config['conditions'] = $condition;
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'denied',
                'param_2' => '0',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            return $result;
        
        }
        
        public function mark_as_denied($id,$table,$condition){
            
            $config['table'] = $table;
            $config['conditions'] = $condition;
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'denied',
                'param_2' => '1',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            return $result;
        
        }
        
        public function get_template($type){
            $col = '';
            
            if($type == 'spot'){
                $col = 'spot';
            } else if ($type == 'main') {
                $col = 'main';
            } else if ($type == 'supp'){
                $col = 'supp';
            }
            
            $config['table'] = 'templates_tbl';
            $config['cols'] = array($col);
            $results = $this->special_get($config)[0];
            
            return $results;
        }
        
        public function unattach($table,$case_id,$condition,$column){
            $to_return = true;
            
            $this->db->trans_begin();
           
            $config['table'] = $table;
            $config['conditions'] = $condition;
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'attached_to',
                'param_2' => "'NONE'",
                'param_3' => FALSE,
            ); 
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            if($this->db->trans_status() === FALSE){
               
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;
                var_dump($array);
                $this->db->trans_rollback();
			}
            
            //unattach case
            $config['table'] = 'case_report_tbl';
            $config['conditions'] = array('case_id' => $case_id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => $column,
                'param_2' => "'NONE'",
                'param_3' => FALSE,
            ); 
            
            $result = $this->special_save($config);
            
            if($this->db->trans_status() === FALSE){
               
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;
                $this->db->trans_rollback();
			}else{	
				$this->db->trans_commit();		
			}		
            
			return $to_return;
     
        }
        
        
        /*===============================================================================
        +                           PRE-OPERATION
        *===============================================================================*/
        
        public function delete_preoperation($id){
            $del_config['table'] = 'pre_operation_tbl';
            $del_config['conditions'] = array('pre_op_id' => $id);
            
            return $this->special_delete($del_config);
        }
        
        public function get_preop_creator($id){
            $config['table'] = 'pre_operation_tbl';
            $config['cols'] = array('created_by');
            $config['conditions'] = array('pre_op_id' => $id);
            
            $creator = $this->special_get($config)[0];
            
            return $creator;
        }
        
        public function get_preoperation($id){
            
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array(
                'pre_op_id' => $id,
            );
            
            $data = $this->special_get($config);
            
            $this->load->library('DateTimeHelper');
            $pre_op = $data[0];
            $pre_op->time = $this->datetimehelper->parseto_system_time($pre_op->time);
            $pre_op->duration_from_time = $this->datetimehelper->parseto_system_time($pre_op->duration_from_time);
            $pre_op->duration_to_time = $this->datetimehelper->parseto_system_time($pre_op->duration_to_time);
            
            $pre_op->targets = $this->get_preoperation_targets($id);
            
            
            return $pre_op;
        
        }
        
        
        public function get_preoperation_targets($id){
            $config['table'] = 'target_person_tbl';
            $config['conditions'] = array(
                'pre_op_id' => $id,
            );
            
            return $this->special_get($config);
        }
        
        
        public function get_all_preoperations(){
            $config['table'] = 'pre_operation_tbl';
            $config['order_by'] = 'pre_op_id';
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
            
            $this->load->library('DateTimeHelper');
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['cols'] = array('first_name','last_name','rank');
            
            if($data){
                foreach($data as $obj){
                    $obj->time = $this->datetimehelper->parseto_system_time($obj->time);
                   
                    $p_config['conditions'] = array('pnp_id' => $obj->created_by);
                    
                    $obj->created_by_info = $this->special_get($p_config)[0];     
                    $obj->created_by_info->rank = $this->get_rank_acronym($obj->created_by_info->rank);
                }
            }
         
            return $data;  
        }
        
        public function get_all_preoperations_un(){
            $config['table'] = 'pre_operation_tbl';
            $config['order_by'] = 'pre_op_id';
            $config['order_arrange'] = 'desc';
            $config['conditions'] = array(
                'attached_to' => 'NONE',
            );
            $data = $this->special_get($config);
            
            $this->load->library('DateTimeHelper');
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['cols'] = array('first_name','last_name','rank');
            
            if($data){
                foreach($data as $obj){
                    $obj->time = $this->datetimehelper->parseto_system_time($obj->time);
                   
                    $p_config['conditions'] = array('pnp_id' => $obj->created_by);
                    
                    $obj->created_by_info = $this->special_get($p_config)[0];     
                    $obj->created_by_info->rank = $this->get_rank_acronym($obj->created_by_info->rank);
                }
            }
         
            return $data;  
        }
        
        
        public function get_preoperation_id(){
            $config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'PCD',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'pre_operation_tbl',
				'column' => 'pre_op_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('pre_op_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->pre_op_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
        }
        
        public function get_target_first_id(){
            $config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'TP',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'target_person_tbl',
				'column' => 'target_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('target_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->target_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
        
        }
        public function get_target_next_id($prev_id){
        	$config = array(
				'strategy' => 'NORMAL_INCREMENT',
				'prefix' => 'TP',
				'includes_year' => true
			);
			$this->load->library('Incrementor', $config);
			
			return $this->incrementor->increment($prev_id);
        }
         
        public function approve_preoperation($id,$cn){
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array('pre_op_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'has_approved',
                'param_2' => '1',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array('pre_op_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'control_no',
                'param_2' => "'{$cn}'",
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            return $result;
        }
        
        public function unapprove_preoperation($id){
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array('pre_op_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'has_approved',
                'param_2' => '0',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
              
           
            
            return $result;
        }
        
        //PRE-OPERATION TRANSACTION
        public function save_preoperation($data){
           
            $to_return = true;
            
            $this->db->trans_begin();
            $data['created_by'] = $this->session->userdata('username');
            $this->save_preoperation_details($data);
            $this->save_targets($data['pre_op_id'],json_decode($data['targets']));
            
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();
                
                $description = 'has created a Pre-operation Report with ID of ' . 
                    $data['pre_op_id'];
                $operation = 'create pre-operation report';
                
                $this->ss_audit($operation, $description);
				
			}		
            
        
			return $to_return;
           

        }
        
        public function save_preoperation_details($data){
            $this->load->library('DateTimeHelper');
            $data['time'] = $this->datetimehelper->parseto_db_time($data['time']);
            $data['duration_from_time'] = $this->datetimehelper->parseto_db_time($data['duration_from_time']);
            $data['duration_to_time'] = $this->datetimehelper->parseto_db_time($data['duration_to_time']);
            $data['date'] = $this->datetimehelper->parseto_db_date($data['date']);
            
            $row = array(
                'pre_op_id' => $data['pre_op_id'], 
                'control_no' => $data['control_no'],
                'date' => $data['date'],
                'time' => $data['time'],
                'summary' => $data['summary'],
                'course_of_action' => $data['course_of_action'],
                'duration_to_time' => $data['duration_to_time'],
                'duration_from_time' => $data['duration_from_time'],
                'duration_to_date' => $data['duration_to_date'],
                'duration_from_date' => $data['duration_from_date'],
                'target_location' => $data['target_location'],
                'created_by' => $data['created_by'],
                'attached_to' => 'NONE',
            );
            $config['table'] = 'pre_operation_tbl';
            $config['data'] = $row;
            
            $this->special_save($config);   
        }
        
        public function save_targets($pre_op_id,$targets){
            
            foreach($targets as $target){
                $row = array(
                    'target_id' => $target->target_id,
                    'full_name' => $target->full_name,
                    'pre_op_id' => $pre_op_id,
                    'alias' => $target->alias,
                    'nationality' => $target->nationality,
                );
                $config['table'] = 'target_person_tbl';
                $config['data'] = $row;
            
                $this->special_save($config);
            }         
        }
        
        public function update_preoperation($data){
            $to_return = true;
            
            $this->db->trans_begin();
            $this->update_preoperation_details($data);
            $this->update_preoperation_targets($data['pre_op_id'], json_decode($data['targets']), $data['has_targets_changed']);
            
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();		
			}		
            
			return $to_return;
        }
        
        public function update_preoperation_details($data){
            $this->load->library('DateTimeHelper');
            $data['time'] = $this->datetimehelper->parseto_db_time($data['time']);
            $data['duration_from_time'] = $this->datetimehelper->parseto_db_time($data['duration_from_time']);
            $data['duration_to_time'] = $this->datetimehelper->parseto_db_time($data['duration_to_time']);
            $data['date'] = $this->datetimehelper->parseto_db_date($data['date']);
            
            $row = array(
                'pre_op_id' => $data['pre_op_id'], 
                'control_no' => $data['control_no'],
                'date' => $data['date'],
                'time' => $data['time'],
                'summary' => $data['summary'],
                'course_of_action' => $data['course_of_action'],
                'duration_to_time' => $data['duration_to_time'],
                'duration_from_time' => $data['duration_from_time'],
                'duration_to_date' => $data['duration_to_date'],
                'duration_from_date' => $data['duration_from_date'],
                'target_location' => $data['target_location'],
                'attached_to' => 'NONE',
            );
            $config['table'] = 'pre_operation_tbl';
            $config['data'] = $row;
            $config['type'] = 'UPDATE';
            $config['conditions'] = array(
				'pre_op_id' => $data['pre_op_id'],
			);
            
            $this->special_save($config);
        }
        
        public function update_preoperation_targets($id, $targets, $has_changed){
            if($has_changed == true){
                $del_config['table'] = 'target_person_tbl';
                $del_config['conditions'] = array('pre_op_id' => $id);
                $this->special_delete($del_config);
                $this->save_targets($id, $targets);
            }
            
        }
        
        /*===============================================================================
        +                         SPOT REPORT
        *===============================================================================*/
        
        public function get_spot_creator($id){
            $config['table'] = 'spot_report_tbl';
            $config['cols'] = array('created_by');
            $config['conditions'] = array('spot_report_id' => $id);
            
            $creator = $this->special_get($config)[0];
            
            return $creator;
        }
        
        
        public function delete_spotreport($id){
            $del_config['table'] = 'spot_report_tbl';
            $del_config['conditions'] = array('spot_report_id' => $id);
            
            return $this->special_delete($del_config);
        }
        public function update_spotreport($data){
           
            $to_return = true;
            $this->db->trans_begin();
           
            $this->update_spotreport_details($data);
            
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();		
			}		
          
			return $to_return;
        
        }
        
        public function update_spotreport_details($data){
          
            $row = array(
                'spot_report_id' => $data['spot_report_id'],
                'content' => $data['content'],
                'created_by' =>  $this->session->userdata('username'),
                'delivered_by' => $data['delivered_by'],
                'attached_to' => 'NONE',
            );
            $config['table'] = 'spot_report_tbl';
            $config['data'] = $row;
            $config['type'] = 'UPDATE';
            $config['conditions'] = array(
				'spot_report_id' => $data['spot_report_id'],
			);
            
            $ech = $this->special_save($config);
        
        }
        
        
        public function get_spot_report($id){
            $config['table'] = 'spot_report_tbl';
            $config['conditions'] = array(
                'spot_report_id' => $id,
            );
            
            $data = $this->special_get($config)[0];
            
            $config['table'] = 'pnp_officer_tbl';
            $config['cols'] = array('first_name', 'last_name', 'middle_name', 'rank');
            $config['conditions'] = array(
                'pnp_id' => $data->delivered_by,
            );
            
            $data->delivered_by_info = $this->special_get($config)[0];    
            preg_match_all('/\b\w/',  $data->delivered_by_info->rank, $matches);
            $data->delivered_by_info->rank_full = $data->delivered_by_info->rank;
            $data->delivered_by_info->rank = implode('',$matches[0]);
            
            
            $config['table'] = 'pnp_officer_tbl';
            $config['cols'] = array('first_name', 'last_name','middle_name','rank');
            $config['conditions'] = array(
                'pnp_id' => $data->created_by,
            );   
            $data->created_by_info = $this->special_get($config)[0];    
            preg_match_all('/\b\w/',  $data->created_by_info->rank, $matches);
            $data->created_by_info->rank_full = $data->created_by_info->rank;
            $data->created_by_info->rank = implode('',$matches[0]);
            
            return $data;  
            
        }
        
         public function get_all_spot_report_un(){
            $config['table'] = 'spot_report_tbl';
            $config['order_by'] = 'spot_report_id';
            $config['order_arrange'] = 'desc';
            $config['conditions'] = array(
                'attached_to' => 'NONE',
            );
            $data = $this->special_get($config);
            
            if($data){
                foreach($data as $spot){
                    $pnp_config['table'] = 'pnp_officer_tbl';
                    $pnp_config['cols'] = array('first_name', 'last_name', 'rank');
                    $pnp_config['conditions'] = array(
                        'pnp_id' => $spot->delivered_by,
                    );

                    $spot->delivered_by_info = $this->special_get($pnp_config)[0];
                    $spot->delivered_by_info->batch_id = $spot->delivered_by;

                    preg_match_all('/\b\w/', $spot->delivered_by_info->rank, $matches);
                    $spot->delivered_by_info->rank = implode('',$matches[0]);

                    unset($spot->delivered_by);

                    //get officer that created the spot
                    $pnp_config['conditions'] = array(
                        'pnp_id' =>  $spot->created_by,
                    );
                    $spot->created_by_info = $this->special_get($pnp_config)[0];
                    $spot->created_by_info->batch_id = $spot->created_by;

                    preg_match_all('/\b\w/', $spot->created_by_info->rank, $matches);
                    $spot->created_by_info->rank = implode('',$matches[0]);

                    unset($spot->created_by);


                }
            }
            
            return $data;  
        }
        public function get_all_spot_reports(){
            $config['table'] = 'spot_report_tbl';
            $config['order_by'] = 'spot_report_id';
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
           
            foreach($data as $spot){
                $pnp_config['table'] = 'pnp_officer_tbl';
                $pnp_config['cols'] = array('first_name', 'last_name', 'rank');
                $pnp_config['conditions'] = array(
                    'pnp_id' => $spot->delivered_by,
                );
                
                $spot->delivered_by_info = $this->special_get($pnp_config)[0];
                $spot->delivered_by_info->batch_id = $spot->delivered_by;
                
                preg_match_all('/\b\w/', $spot->delivered_by_info->rank, $matches);
				$spot->delivered_by_info->rank = implode('',$matches[0]);

                unset($spot->delivered_by);
                
                //get officer that created the spot
                $pnp_config['conditions'] = array(
                    'pnp_id' =>  $spot->created_by,
                );
                $spot->created_by_info = $this->special_get($pnp_config)[0];
                $spot->created_by_info->batch_id = $spot->created_by;
                
                preg_match_all('/\b\w/', $spot->created_by_info->rank, $matches);
				$spot->created_by_info->rank = implode('',$matches[0]);
              
                unset($spot->created_by);
                
                
            }
    
            return $data;  
        
        }
        public function get_spotreport_id(){
            $config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'SPOT',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'spot_report_tbl',
				'column' => 'spot_report_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('spot_report_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->spot_report_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
        }
        
        public function save_spot_report($data){
            $to_return = true;
            
            $this->db->trans_begin();
            $this->save_spot_report_details($data);
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();
				$description = 'has created a Spot Report with ID of ' . 
                    $data['spot_report_id'];
                $operation = 'create coordination dorm';
                $this->ss_audit($operation, $description);
			}
            
            return $to_return;
        }
        
        public function save_spot_report_details($data){
            $this->load->library('DateTimeHelper');
            $data['created_by'] = $this->session->userdata('username');
            $data['date'] = $this->datetimehelper->get_current_date();
            $data['attached_to'] = 'NONE';
            
            $config['table'] = 'spot_report_tbl';
            $config['data'] = $data;
            
            $this->special_save($config);
        
        }
        /*===============================================================================
        +                           COORDINATION FORM
        *===============================================================================*/
        public function get_coord_creator($id){
            $config['table'] = 'coordination_form_tbl';
            $config['cols'] = array('created_by');
            $config['conditions'] = array('coordination_id' => $id);
            
            $creator = $this->special_get($config)[0];
            
            return $creator;
        }
   
        public function get_coordination($id){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $id);
            
            $c =  $this->special_get($config)[0];
            
            $this->load->library('DateTimeHelper');
            $c->duration_from_time = $this->datetimehelper->parseto_system_time($c->duration_from_time);
            $c->duration_to_time = $this->datetimehelper->parseto_system_time($c->duration_to_time);
            
            //gets team leader info 
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['conditions'] = array('pnp_id' => $c->team_leader);
            $p_config['cols'] = array('first_name','last_name', 'rank');
            
            $info = $this->special_get($p_config)[0];
            $info->rank = $this->get_rank_acronym($info->rank);
            $c->team_leader_info = $info;
        
            //gets members
            $c->members = $this->get_coordination_members($id);
            //gets vehicles
            $c->vehicles = $this->get_coordination_vehicles($id);
            return $c;
            
        } 
        public function get_coordination_vehicles($id){
            //load vehicles id
            $p_config['table'] = 'coordination_vehicles_tbl';
            $p_config['conditions'] = array('coordination_id' => $id);
            $p_config['cols'] = array('vehicle_id');
            
            $vehicles = $this->special_get($p_config);
           
            //get vehicles info
            $p_config['table'] = 'vehicles_tbl';
            $p_config['cols'] = array('type','make', 'color', 'plate_no');
            
            foreach($vehicles as $obj){
                $p_config['conditions'] = array('vehicle_id' => $obj->vehicle_id);
                $info = $this->special_get($p_config)[0];
                //assing $info properties to 
                foreach($info as $key => $value){
				    $obj->$key = $value;
				}
            }
            
            return $vehicles;
        
        }
        public function get_coordination_members($id){
            //load memmbers
            $p_config['table'] = 'coordination_member_tbl';
            $p_config['conditions'] = array('coordination_id' => $id);
            $p_config['cols'] = array('pnp_id');
           
            $members = $this->special_get($p_config);
            //get members_info
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['cols'] = array('first_name','last_name', 'rank');
            foreach($members as $obj){
                $p_config['conditions'] = array('pnp_id' => $obj->pnp_id);
                $info = $this->special_get($p_config)[0];
                //assing $info properties to 
                foreach($info as $key => $value){
				    $obj->$key = $value;
				}
                
                //get rank acronym
                $obj->rank = $this->get_rank_acronym($obj->rank);
            }
            
            
            return $members;
        
        }
        
        public function get_all_coordinations_un(){
            $config['table'] = 'coordination_form_tbl';
            $config['order_by'] = 'coordination_id';
            $config['conditions'] = array(
                'attached_to' => 'NONE',
            );
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
            
            $this->load->library('DateTimeHelper');
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['cols'] = array('first_name','last_name','rank');
            
            
            if($data){
                foreach($data as $c){
                    //set time
                    $c->duration_from_time = $this->datetimehelper->parseto_system_time($c->duration_from_time);
                    $c->duration_to_time = $this->datetimehelper->parseto_system_time($c->duration_to_time);

                    //get team leader info
                    $p_config['conditions'] = array('pnp_id' => $c->team_leader);

                    $c->team_leader_info = $this->special_get($p_config)[0];     
                    $c->team_leader_info->rank = $this->get_rank_acronym($c->team_leader_info->rank);

                    //get creator info
                    $p_config['conditions'] = array('pnp_id' => $c->created_by);
                    $c->created_by_info = $this->special_get($p_config)[0];     
                    $c->created_by_info->rank = $this->get_rank_acronym($c->created_by_info->rank);
                }
            
            }
                   
            return $data;  
        }
        
        public function get_all_coordinations(){
            $config['table'] = 'coordination_form_tbl';
            $config['order_by'] = 'coordination_id';
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
            
            $this->load->library('DateTimeHelper');
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['cols'] = array('first_name','last_name','rank');
            
            foreach($data as $c){
                //set time
                $c->duration_from_time = $this->datetimehelper->parseto_system_time($c->duration_from_time);
                $c->duration_to_time = $this->datetimehelper->parseto_system_time($c->duration_to_time);
                
                //get team leader info
                
                $p_config['conditions'] = array('pnp_id' => $c->team_leader);
                
                $c->team_leader_info = $this->special_get($p_config)[0];     
                $c->team_leader_info->rank = $this->get_rank_acronym($c->team_leader_info->rank);
            
                //get creator info
                $p_config['conditions'] = array('pnp_id' => $c->created_by);
                $c->created_by_info = $this->special_get($p_config)[0];     
                $c->created_by_info->rank = $this->get_rank_acronym($c->created_by_info->rank);
            }
            
                 
            return $data;  
        }
        public function get_coordination_id(){
            $config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'COORDINATION',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'coordination_form_tbl',
				'column' => 'coordination_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('coordination_id')
			);
			
			//returns array of objects. In this case 1 object only
			$data = $this->limited_order_get($query_config);
			//this will be the basis of our DB_INCREMENT strategy		
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->coordination_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
        }
        
        public function approve_coordination($id,$cn){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'has_approved',
                'param_2' => '1',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'control_no',
                'param_2' => "'{$cn}'",
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            return $result;
        }
        
        
        public function unapprove_coordination($id){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'has_approved',
                'param_2' => '0',
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array('coordination_id' => $id);
            $config['use_set'] = TRUE;
            $config['set_params'] = array(
                'param_1' => 'control_no',
                'param_2' => "''",
                'param_3' => FALSE,
            );     
            $config['type'] = 'UPDATE';
            $result = $this->special_save($config);
            
            return $result;
        }
        public function save_coordination_form($data){
            $to_return = true;
            
            $this->db->trans_begin();
            $this->save_coordination_form_details($data);
            $this->save_coordination_members($data['coordination_id'],json_decode($data['members']));
            $this->save_coordination_vehicles($data['coordination_id'],json_decode($data['vehicles']));
            
            if($this->db->trans_status() === FALSE){  
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $this->db->trans_rollback();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();
                $description = 'has created a Coordination Form with ID of ' . 
                    $data['coordination_id'];
                $operation = 'create coordination dorm';
                $this->ss_audit($operation, $description);
				
			}
            
            return $to_return;
        }
        
        public function save_coordination_form_details($data){
            $this->load->library('DateTimeHelper');
            $data['duration_from_time'] = $this->datetimehelper->parseto_db_time($data['duration_from_time']);
            $data['duration_to_time'] = $this->datetimehelper->parseto_db_time($data['duration_to_time']);
            $data['date'] = $this->datetimehelper->parseto_db_date($data['date']);
            $data['duration_from_date'] = $this->datetimehelper->parseto_db_date($data['duration_from_date']);
            $data['duration_to_date'] = $this->datetimehelper->parseto_db_date($data['duration_to_date']);
        
            $row = array(
                'coordination_id' => $data['coordination_id'],
                'control_no' => $data['control_no'],
                'date' =>  $data['date'],
                'unit_id' => $data['unit'],
                'type_of_operation' => $data['type_of_operation'],
                'duration_to_time' => $data['duration_to_time'],
                'duration_from_time' => $data['duration_from_time'],
                'duration_from_date' => $data['duration_from_date'],
                'duration_to_date' => $data['duration_to_date'],
                'area_of_operation' => $data['area_of_operation'],
                'team_leader' => $data['team_leader'],
                'created_by' => $this->session->userdata('username'),
                'received_by' => $data['received_by'],
            );
            
            $config['table'] = 'coordination_form_tbl';
            $config['data'] = $row;
         
            $this->special_save($config);
           
        }
        
        public function save_coordination_members($c_id, $members){
            $config['table'] = 'coordination_member_tbl';
            foreach($members as $member){
                $row = array(
                    'coordination_id' => $c_id,
                    'pnp_id' => $member,
                );
               
                $config['data'] = $row;
                
                $this->special_save($config);
            }
        }
        
        public function save_coordination_vehicles($c_id, $vehicles){
            $config['table'] = 'coordination_vehicles_tbl';
            foreach($vehicles as $vehicle){
                $row = array(
                    'coordination_id' => $c_id,
                    'vehicle_id' => $vehicle,
                );
               
                $config['data'] = $row;
                
                $this->special_save($config);
            }
        }
        
        
        public function update_coordination($data){         
            $to_return = true;
            $this->db->trans_begin();
            $this->update_coordination_details($data);
            
            if($data['has_members_changed']){
                $this->update_coordination_members($data['coordination_id'],json_decode($data['members']));
            }
            
            if($data['has_vehicles_changed']){
                $this->update_coordination_vehicles($data['coordination_id'],json_decode($data['vehicles']));
            }
            
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();		
			}		
          
			return $to_return;
        }
        
        public function delete_coordination($id){
            $del_config['table'] = 'coordination_form_tbl';
            $del_config['conditions'] = array('coordination_id' => $id);
            
            return $this->special_delete($del_config);
        }
        
        private function update_coordination_details($data){
            $row = array(
                'coordination_id' => $data['coordination_id'],
                'control_no' => $data['control_no'],
                'date' =>  $data['date'],
                'unit_id' => $data['unit'],
                'type_of_operation' => $data['type_of_operation'],
                'duration_to_time' => $data['duration_to_time'],
                'duration_from_time' => $data['duration_from_time'],
                'duration_from_date' => $data['duration_from_date'],
                'duration_to_date' => $data['duration_to_date'],
                'area_of_operation' => $data['area_of_operation'],
                'team_leader' => $data['team_leader'],
                'created_by' => $this->session->userdata('username'),
                'received_by' => $data['received_by'],
            );
            $config['table'] = 'coordination_form_tbl';
            $config['data'] = $row;
            $config['type'] = 'UPDATE';
            $config['conditions'] = array(
				'coordination_id' => $data['coordination_id'],
			);
            
            $this->special_save($config);
        }
        
        private function update_coordination_members($c_id, $members){     
            $del_config['table'] = 'coordination_member_tbl';
            $del_config['conditions'] = array('coordination_id' => $c_id);
            $this->special_delete($del_config);
            $this->save_coordination_members($c_id, $members);
   
        }
        
        private function update_coordination_vehicles($c_id, $vehicles){
            $del_config['table'] = 'coordination_vehicles_tbl';
            $del_config['conditions'] = array('coordination_id' => $c_id);
            $this->special_delete($del_config);
            $this->save_coordination_vehicles($c_id, $vehicles);
        }
        
        //----------------------------------> Mark <----------------------------------------//
          public function pdf_get_preoperation($id){
            $config['table'] = 'pre_operation_tbl';
            $config['conditions'] = array(
                'pre_op_id' => $id,
            );
            
            $data = $this->special_get($config);
            $this->load->library('DateTimeHelper');
            


            $data[0]->duration_from_time = $this->datetimehelper->parseto_system_time($data[0]->duration_from_time);
            $data[0]->duration_to_time = $this->datetimehelper->parseto_system_time($data[0]->duration_to_time);

            //set date
            $data[0]->duration_from_date = $this->datetimehelper->parseto_standard_date($data[0]->duration_from_date);
            $data[0]->duration_to_date = $this->datetimehelper->parseto_standard_date($data[0]->duration_to_date);
            $data[0]->date = $this->datetimehelper->parseto_standard_date($data[0]->date);

            $target_person = $this->get_targets($id);
            
			$target_arr = [];
			foreach($target_person as $t){
                $target_arr[] = $t->full_name . ' a.k.a. ' . $t->alias;					
			}
			$targetPersons = implode(', ', $target_arr);
			$data[0]->target_persons = $targetPersons;
            
            $data[0]->nationality = $target_person;
            $preoperation = $data[0];
            
            return $preoperation;
        }
        
        public function get_targets($id){
            $config['table'] = 'target_person_tbl';
            $config['cols'] = array(
                'full_name',
                'alias',
                'nationality'
            );
            
            $config['conditions'] = array(
                'pre_op_id' => $id,
            );
            
            $data = $this->special_get($config);
            
           
            
            return $data;
        }
        
        public function pdf_get_coordination($id){
            $config['table'] = 'coordination_form_tbl';
            $config['conditions'] = array(
                'coordination_id' => $id,
            );
            
            $data = $this->special_get($config);
            $this->load->library('DateTimeHelper');
            $p_config['table'] = 'pnp_officer_tbl';
            $p_config['cols'] = array('first_name','last_name','rank');
            
            $c_config['table'] = 'pnp_officer_tbl';
            $c_config['cols'] = array('first_name','middle_name','last_name','rank');
            
            
            $u_config['table'] = 'group_tbl';
            $u_config['cols'] = array('group_name');
           
            foreach($data as $c){
                //set time
                $c->duration_from_time = $this->datetimehelper->parseto_system_time($c->duration_from_time);
                $c->duration_to_time = $this->datetimehelper->parseto_system_time($c->duration_to_time);
                
                //set date
                $c->duration_from_date = $this->datetimehelper->parseto_standard_date($c->duration_from_date);
                $c->duration_to_date = $this->datetimehelper->parseto_standard_date($c->duration_to_date);
                $c->date = $this->datetimehelper->parseto_standard_date($c->date);
                
                //get team leader info
                
                $p_config['conditions'] = array('pnp_id' => $c->team_leader);
                
                $c->team_leader_info = $this->special_get($p_config)[0];     
                $c->team_leader_info->rank = $this->get_rank_acronym($c->team_leader_info->rank);
                
                //get creator of coordination
                
                $c_config['conditions'] = array('pnp_id' => $c->created_by);
                
                $c->created_by_info = $this->special_get($c_config)[0];     
                $c->created_by_info->rank = $this->get_rank_acronym($c->created_by_info->rank);
                
                $members = $this->get_members($id);    
                $c->members = $members;
            
                $vehicles = $this->get_vehicle($id);
                $c->vehicles = $vehicles;
                
                $u_config['conditions'] = array('group_id' => $c->unit_id);
                
                $unit_name = $this->special_get($u_config)[0];
                $c->unit_name = $unit_name->group_name;
            }
           
            
                
            $coordination = $data[0];
            
            return $coordination;
        }
        
        public function get_members($id){
            $config['table'] = 'coordination_member_tbl';
            $config['distinct'] = false;
            $config['conditions'] = array(
                'coordination_id' => $id,
            );
            
            $data = $this->special_get($config);
            if($data){
                foreach($data as $m){
                    $p_config['table'] = 'pnp_officer_tbl';
                    $p_config['cols'] = array('first_name','last_name','middle_name','rank');
                    $p_config['conditions'] = array('pnp_id' => $m->pnp_id);

                    $members_info = $this->special_get($p_config)[0];     
                    $m->members_name = $this->get_rank_acronym($members_info->rank) . ' ' . $members_info->first_name . ' ' .
                        substr($members_info->middle_name, 0,1) . '. ' . $members_info->last_name;
                }
             
            }      
            return $data;
        }
        
        public function get_vehicle($id){
            $config['table'] = 'coordination_vehicles_tbl';
            $config['distinct'] = false;
            $config['conditions'] = array(
                'coordination_id' => $id,
            );
            
            $data = $this->special_get($config);
            
            foreach($data as $v){
                $v_config['table'] = 'vehicles_tbl';
                $v_config['cols'] = array('make','type','color','plate_no');
                $v_config['conditions'] = array('vehicle_id' => $v->vehicle_id);

                $vehicle_info = $this->special_get($v_config)[0];     
                $v->vehicle_used = $vehicle_info;
            }
            
            $vehicles = $data;
            
            return $vehicles;
           
        }
        /*===============================================================================
        +                                   Affidavit
        *===============================================================================*/
        
        
        public function update_affidavit($data){
            
            
            $to_return = true;
            $this->db->trans_begin();
            
            $row = array(
                'affidavit_id' => $data['affidavit_id'],
                'content' => $data['content'],
            );
            $config['table'] = 'affidavit_tbl';
            $config['data'] = $row;
            $config['type'] = 'UPDATE';
            $config['conditions'] = array(
				'affidavit_id' => $data['affidavit_id'],
			);
            
            $this->special_save($config);
            
            $this->update_narrators(json_decode($data['narrators']),$data['affidavit_id']);
          
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();		
			}	
            	
          
			return $to_return;
        }
        
        private function update_narrators($narrators,$affidavit_id){
            
           
             //delete first the previous arresting officers.
            $del_config['table'] = 'affidavit_narrator_tbl';
            $del_config['conditions'] = array('affidavit_id' => $affidavit_id);
            $this->special_delete($del_config);
            
           
			foreach($narrators as $value){
				$arr['affidavit_id'] = $affidavit_id;
				$arr['pnp_id'] = $value;
				$this->normal_insert('affidavit_narrator_tbl',$arr);
			}
        }
        
        
        
        public function delete_affidavit($id){
            $del_config['table'] = 'affidavit_tbl';
            $del_config['conditions'] = array('affidavit_id' => $id);
            
            return $this->special_delete($del_config);
        }
        
        public function get_affidavit_id(){
            $config = array(
				'strategy' => 'DB_INCREMENT',
				'prefix' => 'AFFIDAVIT',
				'includes_year' => true
			);
			
			//get the last row in database.
			$query_config = array(
				'table' => 'affidavit_tbl',
				'column' => 'affidavit_id',
				'order' => 'desc',
				'limit' => 1,
				'start' => 0,
				'fields' => array('affidavit_id')
			);
			
			$data = $this->limited_order_get($query_config);
			
			$last_id = "";
			
			if(empty($data)){
				$last_id = false;
			}else{
				$last_id = $data[0]->affidavit_id;
			}
			
			$this->load->library('Incrementor', $config);			

			return $this->incrementor->increment($last_id);
        }
        
        public function get_affidavit($id){
            $config['table'] = 'affidavit_tbl';
            $config['conditions'] = array(
                'affidavit_id' => $id,
            );
            
            $data = $this->special_get($config);
            
            $affidavits = $data[0];
            $affidavits->narrators = $this->get_narrators($affidavits->affidavit_id);

            return $affidavits;  
            
        }
        
        public function get_all_supp_affidavits_un(){
            $config['table'] = 'affidavit_tbl';
            $config['order_by'] = 'affidavit_id';
            $config['conditions'] = array(
                'type' => 'supplemental',
                'attached_to' => 'NONE',
            );
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
            
            $inc = 0;
            if($data){
                foreach($data as $affidavits){
                    $narrators = $this->get_narrators($data[$inc]->affidavit_id);
                    $affidavits->narrators = $narrators;
                    $inc++;
                }
            }
            
            return $data;  
        }
        
        public function get_all_main_affidavits_un(){
            $config['table'] = 'affidavit_tbl';
            $config['order_by'] = 'affidavit_id';
            $config['conditions'] = array(
                'type' => 'main',
                'attached_to' => 'NONE',
            );
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
            
            $inc = 0;
            
            if($data){
                foreach($data as $affidavits){
                    $narrators = $this->get_narrators($data[$inc]->affidavit_id);
                    $affidavits->narrators = $narrators;
                    $inc++;
                }
            }
   
            return $data;  
        }
        
        public function get_all_affidavits($type){
            $config['table'] = 'affidavit_tbl';
            $config['order_by'] = 'affidavit_id';
            $config['conditions'] = array(
                'type' => $type,
            );
            $config['order_arrange'] = 'desc';
            $data = $this->special_get($config);
            
            $inc = 0;
            foreach($data as $affidavits){
                $narrators = $this->get_narrators($data[$inc]->affidavit_id);
                $affidavits->narrators = $narrators;
                $inc++;
            }
            
            return $data;  
        }
        
        public function get_narrators($id){
            $config['table'] = 'affidavit_narrator_tbl';
            $config['order_by'] = 'pnp_id';
            $config['order_arrange'] = 'desc';
            $config['conditions'] = array(
                'affidavit_id' => $id,
            );
            $data = $this->special_get($config);
            
            if($data){
                foreach($data as $narrators){
                    $pnp_config['table'] = 'pnp_officer_tbl';
                    $pnp_config['cols'] = array('first_name', 'last_name', 'rank', 'pnp_id');
                    $pnp_config['conditions'] = array(
                        'pnp_id' => $narrators->pnp_id,
                    );

                    $narrators_info = $this->special_get($pnp_config)[0];
                    $narrators_info->batch_id = $narrators->pnp_id;

                    preg_match_all('/\b\w/', $narrators_info->rank, $matches);
                    $narrators_info->rank = implode('',$matches[0]);
                    
                    $narrators->narratorName = $narrators_info->rank . ' ' . $narrators_info->first_name . ' ' . $narrators_info->last_name;
                    $narrators->pnp_id;

                }
            }
            
            return $data; 
        }
        
        public function save_affidavit_form($data){
            $to_return = true;
            
            $this->db->trans_begin();
            $this->save_affidavit_details($data);
            $this->save_affidavit_narrators($data['affidavit_id'],json_decode($data['narrators']));
            
            if($this->db->trans_status() === FALSE){  
                $array['error_message'] = $this->db->_error_message();
                $array['error_number'] = $this->db->_error_number();
                $this->db->trans_rollback();
                $to_return = $array;     
			}else{	
				$this->db->trans_commit();
				
			}
            
            return $to_return;
        }
        
        public function save_affidavit_details($data){
            
            $this->load->library('DateTimeHelper');
            
            $data['date_created'] = $this->datetimehelper->get_current_date();
            $data['attached_to'] = 'NONE';
            
            $row = array(
                'affidavit_id' => $data['affidavit_id'],
                'content' => $data['content'],
                'date_created' => $data['date_created'],
                'type' => $data['type'],
                'attached_to' => $data['attached_to'],
                
            );
            $config['table'] = 'affidavit_tbl';
            $config['data'] = $row;
            
            $this->special_save($config);
        
        }
        
        public function save_affidavit_narrators($aff, $police){
            
            $config['table'] = 'affidavit_narrator_tbl';
            foreach($police as $police){
                $row = array(
                    'affidavit_id' => $aff,
                    'pnp_id' => $police,
                );
               
                $config['data'] = $row;
                
                $this->special_save($config);
            }
        
        }
        
        public function affidavit_pdf($id){
            
            $data = $this->get_affidavit($id);
            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
            $this->pdfgenerator->DefOrientation = 'P';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
            $this->pdfgenerator->AddPage();
            
            if($data->type == "main"){
                $this->pdfgenerator->SetFont('Arial','B',14);
                $this->pdfgenerator->Ln(10);
                $this->pdfgenerator->Cell(200,5,'PINAGSAMANG SALAYSAY NG PAG ARESTO',0,1,'C');  
                
            }else{
                $this->pdfgenerator->SetFont('Arial','B',14);
                $this->pdfgenerator->Ln(10);
                $this->pdfgenerator->Cell(200,5,'KARAGDAGAN SALAYSAY',0,1,'C');  
                
            }
            $this->pdfgenerator->Ln(15);
            
            $this->pdfgenerator->SetFont('Arial','',10);
            
            $this->pdfgenerator->SetLeftMargin(20);
            $this->pdfgenerator->SetRightMargin(20);
            $this->pdfgenerator->SetXY(20, 70);
            $this->pdfgenerator->MultiCell(0,0,$this->pdfgenerator->WriteHTML($data->content), 0, 'FJ', false);
            
            
            $x = 23;
            $y = 285;
            $x1 = 23;
            $y1 = 305;
            $i = 0;
            foreach($data->narrators as $obj){
                if($i <= 2 ){
                    $this->pdfgenerator->SetXY($x,$y);
                    $this->pdfgenerator->Write(0,$obj->narratorName);
                    $this->pdfgenerator->Line($x-1,$y + 2,$x+40, $y +2);
                    $this->pdfgenerator->SetXY($x+6,$y+5);
                    $this->pdfgenerator->Write(0,'Nagsasalaysay');
                    $i++;
                    $x += 65;
                }else{
                    
                    $this->pdfgenerator->SetXY($x1,$y1);
                    $this->pdfgenerator->Write(0,$obj->narratorName);
                    $this->pdfgenerator->Line($x1-1, $y1 +2,$x1 + 40, $y1 +2);
                    $this->pdfgenerator->SetXY($x1+6,$y1+5);
                    $this->pdfgenerator->Write(0,'Nagsasalaysay');
                    $i++;
                    $x1 += 65;
                    
                }
            }
            
            $this->pdfgenerator->SetXY(145,331);
            $this->pdfgenerator->Write(0,'Administering Officer');
            $this->pdfgenerator->Line(135,328,195,328);
            
            $this->pdfgenerator->SetLeftMargin(10);
            $this->pdfgenerator->SetRightMargin(10);

        }
        
        /*===============================================================================
            
                                        Coordination Form
            
        ================================================================================*/
        
        public function coordination_pdf($id){
            
            $data = $this->pdf_get_coordination($id);
            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
            $this->pdfgenerator->DefOrientation = 'P';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
            $this->pdfgenerator->AddPage();
            
            $this->pdfgenerator->SetFont('Arial','BU',14);
            $this->pdfgenerator->Cell(200,5,'COORDINATION FORM',0,1,'C');  
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(20,70);
            $this->pdfgenerator->Write(0,'Control No.');
            $this->pdfgenerator->Line(44,72,105,72);
            
            $this->pdfgenerator->SetXY(140,70);
            $this->pdfgenerator->Write(0,'Date:');
            $this->pdfgenerator->Line(153,72,190,72);
            
            $this->pdfgenerator->SetXY(30,80);
            $this->pdfgenerator->Write(0,'  I.       Unit/Office :');                    
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(30,92);
            $this->pdfgenerator->Write(0,' II.       Type of Operation : (Check appropriate box)');
            
            $this->pdfgenerator->SetXY(25,98);
            $this->pdfgenerator->Cell(40, 8, 'Surveillance', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, '', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, 'Buy-Bust', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, '', 1,1, 'C');
            
            $this->pdfgenerator->SetX(25);
            $this->pdfgenerator->Cell(40, 8, 'Casing', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, '', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, 'Search Warrant', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, '', 1,1, 'C');
            
            $this->pdfgenerator->SetX(25);
            $this->pdfgenerator->Cell(40, 8, 'Test-Buy', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, '', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, 'Other (Specify)', 1,0, 'C');
            $this->pdfgenerator->Cell(40, 8, '', 1,1, 'C');
            
            $this->pdfgenerator->SetXY(30,130);
            $this->pdfgenerator->Write(0,'III.        Duration : FROM');
            $this->pdfgenerator->Line(80,132,100,132);
            
            $this->pdfgenerator->SetXY(70,140);
            $this->pdfgenerator->Write(0,'TO');
            $this->pdfgenerator->Line(80,142,100,142);
            
            $this->pdfgenerator->SetFont('Arial','I',10);
            
            $this->pdfgenerator->SetXY(83,135);
            $this->pdfgenerator->Write(0,'(Time)');
            
            $this->pdfgenerator->SetXY(83,145);
            $this->pdfgenerator->Write(0,'(Time)');
            
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(30,150);
            $this->pdfgenerator->Write(0,'IV.        Area of Operation :');
                        
            $this->pdfgenerator->SetXY(30,160);
            $this->pdfgenerator->Write(0,' V.        Team Leader :');
            
            $this->pdfgenerator->SetXY(30,215);
            $this->pdfgenerator->Write(0,'VI.        Vehicle Used :');
            
            $this->pdfgenerator->SetY(165);
            for($j=0; $j <= 5; $j++){
                $this->pdfgenerator->SetX(25);
				for($i=0; $i <= 2; $i++){
					switch($i){
						case 0:
                            $this->pdfgenerator->Cell(80,7,'',1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell(80,7,'',1,1,'C');
							break;						
					}
				}	
			}
            
            $this->pdfgenerator->SetXY(30,290);
            $this->pdfgenerator->Write(0,'VII.       Coordination Made by:');
            $this->pdfgenerator->Line(89,292,175,292);
            
            $this->pdfgenerator->SetXY(30,300);
            $this->pdfgenerator->Write(0,'VIII.      Coordination Received by:');
            $this->pdfgenerator->Line(96,302,175,302);
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(130,270);
            $this->pdfgenerator->Write(0,'JESON AMODO VIGILIA');
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(130,275);
            $this->pdfgenerator->Write(0,'Police Inspector');
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(130,280);
            $this->pdfgenerator->Write(0,'Chief, SAID-SOTG');
            
            /*===============================================================================
            
                                                Datas
            
            ================================================================================*/
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            
            $this->pdfgenerator->SetXY(155,70);
            $this->pdfgenerator->Write(0,$data->date);
            
             $this->pdfgenerator->SetXY(45,70);
            $this->pdfgenerator->Write(0,$data->control_no);
            
            $this->pdfgenerator->SetFont('Arial','BU',12);
            $this->pdfgenerator->SetXY(70,77.5);
            $this->pdfgenerator->MultiCell(120, 5,$data->unit_name, 0, 'J');
            
            if($data->type_of_operation == 'Surveillance'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(83, 100.5, 83.5 + 2, 101 + 2 );
                $this->pdfgenerator->Line(83, 101 + 2, 83.5 + 2, 100.5 );
            }elseif($data->type_of_operation == 'Buy-Bust'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(165, 100.5, 165.5 + 2, 101 + 2 );
                $this->pdfgenerator->Line(165, 101 + 2, 165.5 + 2, 100.5 );
            }elseif($data->type_of_operation == 'Casing'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(83, 109.5, 83.5 + 2, 110 + 2 );
                $this->pdfgenerator->Line(83, 110 + 2, 83.5 + 2, 109.5 );
            }elseif($data->type_of_operation == 'Search Warrant'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(165, 108.5, 165.5 + 2, 109 + 2 );
                $this->pdfgenerator->Line(165, 109 + 2, 165.5 + 2, 108.5 );
            }elseif($data->type_of_operation == 'Test-Buy'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(83, 117.5, 83.5 + 2, 118 + 2 );
                $this->pdfgenerator->Line(83, 118 + 2, 83.5 + 2, 117.5 );
            }
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetLineWidth(0);
            
            $this->pdfgenerator->SetXY(80,130);
            $this->pdfgenerator->Write(0,$data->duration_from_time);
            
            $this->pdfgenerator->SetXY(100,130);
            $this->pdfgenerator->Write(0,$data->duration_from_date);
            
            $this->pdfgenerator->SetXY(80,140);
            $this->pdfgenerator->Write(0,$data->duration_to_time);
            
            $this->pdfgenerator->SetXY(100,140);
            $this->pdfgenerator->Write(0,$data->duration_to_date);
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(83,150);
            $this->pdfgenerator->Write(0,$data->area_of_operation);
            
            $this->pdfgenerator->SetXY(73,160);
            $this->pdfgenerator->Write(0,$data->team_leader_info->rank . ' ' . $data->team_leader_info->first_name . ' ' .
                                       $data->team_leader_info->last_name);
            
            $this->pdfgenerator->SetFont('Arial','',10);
            
            $y = 168;
            $y1 = 168;
            $i = 1;
            if($data->members){
                foreach($data->members as $m){
                    if($i <= 6){
                        $this->pdfgenerator->SetXY(45,$y);
                        $this->pdfgenerator->Write(0,$m->members_name);                
                        $y += 7;
                    }else{                    
                        $this->pdfgenerator->SetXY(115, $y1);
                        $this->pdfgenerator->Write(0,$m->members_name);                
                        $y1 += 7;
                    }
                    $i++;
                }       
            }
                 
            $this->pdfgenerator->SetXY(25,220);
            $arrayName = array('TYPE', 'MAKE', 'COLOR', 'PLATE NO.');
			$arrayCol = array(50,35,25,50);
            $this->pdfgenerator->createTableHeader($arrayName,$arrayCol, 12);
            $this->pdfgenerator->Ln();
            
            foreach($data->vehicles as $v){
                $this->pdfgenerator->SetX(25);
                $this->pdfgenerator->SetFont('Arial','',10);
				for($i=0; $i <= 4; $i++){
					switch($i){
						case 0:
							$this->pdfgenerator->Cell($arrayCol[0],7, $v->vehicle_used->type, 1, 0, 'C');
							break;
						case 1:
							$this->pdfgenerator->Cell($arrayCol[1],7, $v->vehicle_used->make, 1, 0, 'C');
							break;
						case 2:
							$this->pdfgenerator->Cell($arrayCol[2],7, $v->vehicle_used->color, 1, 0, 'C');
							break;
						case 3:
							$this->pdfgenerator->Cell($arrayCol[3],7, $v->vehicle_used->plate_no, 1, 1, 'C');
							break;
					}
				}					
			}
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(90,290);
            $this->pdfgenerator->Write(0,$data->created_by_info->rank . ' ' . $data->created_by_info->first_name . ' ' .
                                       substr($data->created_by_info->middle_name,0,1) . '. ' . 
                                       $data->created_by_info->last_name);
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(97,300);
            $this->pdfgenerator->Write(0,$data->received_by);
                        
            $tableHeader = array('PRINTED NAME', 'UNIT/OFFICE', 'REMARKS');
			$arrayCols = array(60,50,50);
            
            
            $this->pdfgenerator->SetXY(25,310);
            $this->pdfgenerator->createTableHeader($tableHeader,$arrayCols, 12);
            $this->pdfgenerator->Ln();
            for($k=1; $k <= 2; $k++){
                $this->pdfgenerator->SetX(25);
				for($o=0; $o <= 3; $o++){
					switch($o){
						case 0:
                            $this->pdfgenerator->Cell($arrayCols[0],7,'',1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell($arrayCols[1],7,'',1,0,'C');
							break;
                        case 2:
							$this->pdfgenerator->Cell($arrayCols[2],7,'',1,1,'C');
							break;
					}
				}	
			}
            
        }
        
        /*===============================================================================
            
                                    Pre-Operation Form
            
        ================================================================================*/
        
        public function preoperation_pdf($id){
            
            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
            $this->pdfgenerator->DefOrientation = 'P';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 0;
            $this->pdfgenerator->AddPage();
            
            $this->pdfgenerator->SetFont('Arial','BU',14);
            $this->pdfgenerator->Cell(200,5,'PRE-OPERATION REPORT',0,1,'C');  
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(20,70);
            $this->pdfgenerator->Write(0,'Control No.:');
            $this->pdfgenerator->Line(44,72,105,72);
            
            $this->pdfgenerator->SetXY(140,70);
            $this->pdfgenerator->Write(0,'Date:');
            $this->pdfgenerator->Line(153,72,190,72);
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(30,80);
            $this->pdfgenerator->Write(0,'I.      SUMMARY OF INFORMATION :');
                        
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(30,125);
            $this->pdfgenerator->Write(0,'II.     COURSE OF ACTION :');
            
            $this->pdfgenerator->SetY(130);
            for($j=0; $j <= 2; $j++){
                $this->pdfgenerator->SetX(45);
				for($i=0; $i <= 4; $i++){
					switch($i){
						case 0:
                            $this->pdfgenerator->Cell(10,7,'',1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell(50,7,'',1,0,'C');
							break;			
                        case 2:
                            $this->pdfgenerator->Cell(10,7,'',1,0,'C');
							break;
						case 3:
							$this->pdfgenerator->Cell(50,7,'',1,1,'C');
							break;
					}
				}	
			}
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(57,134);
            $this->pdfgenerator->Write(0,'Surveillance');
            
            $this->pdfgenerator->SetXY(57,141);
            $this->pdfgenerator->Write(0,'Casing');
            
            $this->pdfgenerator->SetXY(57,148);
            $this->pdfgenerator->Write(0,'Test');
            
            $this->pdfgenerator->SetXY(117,134);
            $this->pdfgenerator->Write(0,'Buy-Bust');
            
            $this->pdfgenerator->SetXY(117,141);
            $this->pdfgenerator->Write(0,'Search Warrant');
            
            $this->pdfgenerator->SetXY(117,148);
            $this->pdfgenerator->Write(0,'Other (Specify)');
            
            $this->pdfgenerator->SetFont('Arial','I',10);
            $this->pdfgenerator->SetXY(85,165);
            $this->pdfgenerator->Write(0,'(Time)');
            $this->pdfgenerator->Line(82,162.5,103,162.5);
            
            $this->pdfgenerator->SetXY(85,175);
            $this->pdfgenerator->Write(0,'(Time)');
            $this->pdfgenerator->Line(82,172.5,103,172.5);
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(30,160);
            $this->pdfgenerator->Write(0,'III.    DURATION :   FROM');
            
            $this->pdfgenerator->SetXY(73,170);
            $this->pdfgenerator->Write(0,'TO');
            
            $this->pdfgenerator->SetXY(30,182);
            $this->pdfgenerator->Write(0,'IV.     TARGET LOCATION :');
            
            $this->pdfgenerator->SetXY(30,195);
            $this->pdfgenerator->Write(0,'V.      TARGET PERSON :');
                        
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(31, 220);
            
            for($i=0; $i <= 4; $i++){
                switch($i){
                    case 0:
                        $this->pdfgenerator->Cell(10,7,'',1,0,'C');
                        break;
                    case 1:
                        $this->pdfgenerator->Cell(40,7,' Filipino',1,0,'L');
                        break;
                    case 2:
                        $this->pdfgenerator->Cell(10,7,'',1,0,'C');
                        break;	
                    case 3:
                       
                        $this->pdfgenerator->Cell(100,7,' Foreign (Specify)',1,1,'L');   
                        $this->pdfgenerator->Line(128,226,189,226);
                        break;	
                }
            }
            
            $this->pdfgenerator->SetXY(130,260);            
            $this->pdfgenerator->Write(0,'JESON AMODO VIGILLA');
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(130,265);
            $this->pdfgenerator->Write(0,'Police Inspector');
            $this->pdfgenerator->SetXY(130,270);
            $this->pdfgenerator->Write(0,'Chief, SAID-SOTG');
            
            $this->pdfgenerator->Ln();
            
            $arrayName = array('PRINTED NAME', 'UNIT/OFFICE', 'REMARKS');
			$arrayCol = array(50,50,50);
			
			$this->pdfgenerator->SetXY(35,290);
			$this->pdfgenerator->createTableHeader($arrayName,$arrayCol, 12);
			$this->pdfgenerator->Ln();
			for($j = 0; $j<=2; $j++){
                $this->pdfgenerator->SetX(35);
				for($i=0; $i <= 3; $i++){
					switch($i){
						case 0:
							$this->pdfgenerator->Cell($arrayCol[0],7,'',1,0,'C');
							break;
						case 1:
							$this->pdfgenerator->Cell($arrayCol[1],7,'',1,0,'C');
							break;
						case 2:
							$this->pdfgenerator->Cell($arrayCol[2],7,'',1 ,1, 'C');
							break;
						
					}
				}	
				
			} 
            $this->pdfgenerator->SetXY(90,335);
            $this->pdfgenerator->Write(0,'------CONFIDENTIAL-----');
            
            //=================================Datas=========================================>
                          
            $this->pdfgenerator->SetFont('Arial','B',12);
            
            $data = $this->pdf_get_preoperation($id);
            
            $this->pdfgenerator->SetXY(155,70);
            $this->pdfgenerator->Write(0,$data->date);
            
            $this->pdfgenerator->SetXY(45,70);
            $this->pdfgenerator->Write(0,$data->control_no);
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(40,87);
            
            $this->pdfgenerator->MultiCell(150,5,$data->summary, 0, 'FJ', false);
            
            if($data->course_of_action == 'Surveillance'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(48, 132.5, 48.5 + 2, 133 + 2 );
                $this->pdfgenerator->Line(48, 133 + 2, 48.5 + 2, 132.5 );
            }elseif($data->course_of_action == 'Buy-Bust'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(109, 132.5, 109.5 + 2, 133 + 2 );
                $this->pdfgenerator->Line(109, 133 + 2, 109.5 + 2, 132.5 );                
            }elseif($data->course_of_action == 'Casing'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(48, 139.5, 48.5 + 2, 140 + 2 );
                $this->pdfgenerator->Line(48, 140 + 2, 48.5 + 2, 139.5 );
            }elseif($data->course_of_action == 'Search Warrant'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(109, 139.5, 109.5 + 2, 140 + 2 );
                $this->pdfgenerator->Line(109, 140 + 2, 109.5 + 2, 139.5 );
            }elseif($data->course_of_action == 'Test'){
                $this->pdfgenerator->SetLineWidth(1.5);
                $this->pdfgenerator->Line(48, 146.5, 48.5 + 2, 147 + 2 );
                $this->pdfgenerator->Line(48, 147 + 2, 48.5 + 2, 146.5 );
            }
            
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->SetXY(83,157.5);
            $this->pdfgenerator->WriteHTML($data->duration_from_time . '<b>  ' . $data->duration_from_date . '</b>');
            
            $this->pdfgenerator->SetXY(83,167.5);
            $this->pdfgenerator->WriteHTML($data->duration_to_time . '<b>  ' . $data->duration_to_date . '</b>');
            
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->SetXY(85,182);
            $this->pdfgenerator->Write(0,$data->target_location);
            
            $this->pdfgenerator->SetFont('Arial','UB',12);
            $this->pdfgenerator->SetXY(81,192.5);
            
            $this->pdfgenerator->MultiCell(110,5,$data->target_persons, 0, 'FJ', false);
            
            $this->pdfgenerator->SetFont('Arial','',12);
            foreach($data->nationality as $n){
                if($n->nationality == 'Filipino'){
                    $this->pdfgenerator->SetLineWidth(1.5);
                    $this->pdfgenerator->Line(34, 222.5, 34.5 + 2, 223 + 2 );
                    $this->pdfgenerator->Line(34, 223 + 2, 34.5 + 2, 222.5 );
                }else{
                    $this->pdfgenerator->SetLineWidth(1.5);
                    $this->pdfgenerator->Line(85, 222.5, 85.5 + 2, 223 + 2 );
                    $this->pdfgenerator->Line(85, 223 + 2, 85.5 + 2, 222.5 );
                    $this->pdfgenerator->SetXY(130,224);
                    $this->pdfgenerator->Write(0, $n->nationality);
                }    
            }
            
            $this->pdfgenerator->SetLineWidth(0);
            $this->pdfgenerator->SetLeftMargin(10);
            $this->pdfgenerator->SetRightMargin(0);
        }
        
        /*===============================================================================
            
                                        SPOT REPORT
            
        ================================================================================*/
        
        
        public function spot_report_pdf($id){
            $this->load->library('PdfGenerator'); // Load library
            $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
            $this->pdfgenerator->DefOrientation = 'P';
            $this->pdfgenerator->AliasNbPages();
            $this->pdfgenerator->Header = 'Forms';
            $this->pdfgenerator->flagg = 1;
            
            
            $this->pdfgenerator->AddPage();
            
            //-----------------------------------DATA------------------------------------------>
            $data = $this->get_spot_report($id);     
            $this->pdfgenerator->SetLeftMargin(20);
            $this->pdfgenerator->SetFont('Arial','',12);
            
            $this->pdfgenerator->Ln(10);
            $this->pdfgenerator->SetY(55);
            $this->pdfgenerator->WriteHTML($data->content);
            
            $this->pdfgenerator->SetX(130);
            $this->pdfgenerator->SetFont('Arial','B',12);
            $this->pdfgenerator->Cell(100,5,strtoupper($data->delivered_by_info->first_name . ' ' . $data->delivered_by_info->middle_name . ' ' . $data->delivered_by_info->last_name),0,1,'L');
            $this->pdfgenerator->SetX(130);
            $this->pdfgenerator->SetFont('Arial','',12);
            $this->pdfgenerator->Cell(100,5,$data->delivered_by_info->rank_full,0,1,'L'); 
            
            $this->pdfgenerator->SetX(130);
            $this->pdfgenerator->Cell(100,5,'SAID-SOTG',0,1,'L'); 
            
            $this->pdfgenerator->SetLeftMargin(10);
        }
        
        /*===============================================================================
            
                                       Medical
            
        ================================================================================*/
        
        public function medical_pdf($cas_id){
            
            $file_path = 'public/resources/photos/case/' . $cas_id . '/medical/';
            
            $med_scans = array();

            if (is_dir($file_path)) {
                if ($dh = opendir($file_path)) {
                    
                    while (($file = readdir($dh)) !== false) {
                        if (!is_dir($file_path.$file)) {
                             if (strpos($file_path.$file,'thumb') == false) {
                                $med_scans[] = $file;
                            }    
                            
                        }
                    }

                    closedir($dh);

                }
            }
            
            foreach($med_scans as $ms){
                $this->load->library('PdfGenerator'); // Load library
                $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
                $this->pdfgenerator->DefOrientation = 'P';
                $this->pdfgenerator->AliasNbPages();
                $this->pdfgenerator->Header = 'None';
                $this->pdfgenerator->flagg = 1;

                $this->pdfgenerator->AddPage();
                $this->pdfgenerator->Image($file_path . $ms, 0 , 0 , $this->pdfgenerator->w,$this->pdfgenerator->h);

            }
        }
        
        /*===============================================================================
            
                                       Laboratory
            
        ================================================================================*/
        
        public function laboratory_pdf($cas_id){
            
            $file_path = 'public/resources/photos/case/' . $cas_id . '/laboratory/';
            
            $lab_scans = array();

            if (is_dir($file_path)) {
                if ($dh = opendir($file_path)) {
                    
                    while (($file = readdir($dh)) !== false) {
                        if (!is_dir($file_path.$file)) {
                            if (strpos($file_path.$file,'thumb') == false) {
                                $lab_scans[] = $file;
                            }    
                           
                        }
                    }

                    closedir($dh);

                }
            }
            
            foreach($lab_scans as $ls){

                $this->load->library('PdfGenerator'); // Load library
                $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
                $this->pdfgenerator->DefOrientation = 'P';
                $this->pdfgenerator->AliasNbPages();
                $this->pdfgenerator->Header = 'None';
                $this->pdfgenerator->flagg = 1;

                $this->pdfgenerator->AddPage();
                $this->pdfgenerator->Image($file_path . $ls, 0 , 0 , $this->pdfgenerator->w,$this->pdfgenerator->h);

            }
                        
        }
        
        /*===============================================================================
            
                                       Search
            
        ================================================================================*/
        
        public function search_pdf($cas_id){
            
            $file_path = 'public/resources/photos/case/' . $cas_id . '/search/';
            
            $search_scans = array();

            if (is_dir($file_path)) {
                if ($dh = opendir($file_path)) {
                    
                    while (($file = readdir($dh)) !== false) {
                        if (!is_dir($file_path.$file)) {
                                
                            if (strpos($file_path.$file,'thumb') == false) {
                                $search_scans[] = $file;
                            }                    
                            
                        }
                    }

                    closedir($dh);

                }
            }
            
            foreach($search_scans as $ss){

                $this->load->library('PdfGenerator'); // Load library
                $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
                $this->pdfgenerator->DefOrientation = 'P';
                $this->pdfgenerator->AliasNbPages();
                $this->pdfgenerator->Header = 'None';
                $this->pdfgenerator->flagg = 1;

                $this->pdfgenerator->AddPage();
                $this->pdfgenerator->Image($file_path . $ss, 0 , 0 , $this->pdfgenerator->w,$this->pdfgenerator->h);

            }
                        
        }
        
        /*===============================================================================
            
                                       Warrant
            
        ================================================================================*/
        
        public function warrant_pdf($cas_id){
            
            $file_path = 'public/resources/photos/case/' . $cas_id . '/warrant/';
            
            $warrant_scans = array();

            if (is_dir($file_path)) {
                if ($dh = opendir($file_path)) {
                    
                    while (($file = readdir($dh)) !== false) {
                        if (!is_dir($file_path.$file)) {
                            
                            if (strpos($file_path.$file,'thumb') == false) {
                                $warrant_scans[] = $file;
                            }
                            
                        }
                    }

                    closedir($dh);

                }
            }
            
            foreach($warrant_scans as $ws){

                $this->load->library('PdfGenerator'); // Load library
                $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
                $this->pdfgenerator->DefOrientation = 'P';
                $this->pdfgenerator->AliasNbPages();
                $this->pdfgenerator->Header = 'None';
                $this->pdfgenerator->flagg = 1;

                $this->pdfgenerator->AddPage();
                $this->pdfgenerator->Image($file_path . $ws, 0 , 0 , $this->pdfgenerator->w,$this->pdfgenerator->h);

            }
                        
        }
        
        /*===============================================================================
            
                                       Others
            
        ================================================================================*/
        
        public function other_pdf($cas_id){
            
            $file_path = 'public/resources/photos/case/' . $cas_id . '/others/';
            
            $other_scans = array();

            if (is_dir($file_path)) {
                if ($dh = opendir($file_path)) {
                    
                    while (($file = readdir($dh)) !== false) {
                        if (!is_dir($file_path.$file)) {
                            
                            
                            if (strpos($file_path.$file,'thumb') == false) {
                               $other_scans[] = $file;
                            }   
                            
                            
                        }
                    }

                    closedir($dh);

                }
            }
            
            foreach($other_scans as $os){

                $this->load->library('PdfGenerator'); // Load library
                $this->pdfgenerator->fontpath = 'public/fonts/'; // Specify font folder
                $this->pdfgenerator->DefOrientation = 'P';
                $this->pdfgenerator->AliasNbPages();
                $this->pdfgenerator->Header = 'None';
                $this->pdfgenerator->flagg = 1;

                $this->pdfgenerator->AddPage();
                $this->pdfgenerator->Image($file_path . $os, 0 , 0 , $this->pdfgenerator->w,$this->pdfgenerator->h);

            }
                        
        }
    }
