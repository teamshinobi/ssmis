<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Settings extends Ss_controller {
    
    
    public function index(){
        $data['title'] = "Settings";
		$data['page_content'] = "settings_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'Settings';
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
			"loading_modal.php",
           
		);
		
		
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/jquery.contextMenu.css",	     
            "public/included_css/summernote.css"
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
            "public/included_js/summernote.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/jquery.dataTables.min.js",
            "public/included_js/jquery.contextMenu.js",  
            "public/system_js/modules/main.js",	
            "public/system_js/modules/settings_module.js",	
            "public/system_js/settings.js",	
           
            
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
    }
    
    public function get_templates(){
     
        $this->load->model('settingsmod');
        
        $res = $this->settingsmod->get_templates();
        
        if($res){
            $response['has_data'] = true;
            $response['data'] = $res;
            $this->echo_response($response,200,'OK!');  
        } else {
            $response['has_data'] = false;
            $response['data'] = 'Error in retrieving templates';
            $this->echo_response($response,500,'OK!');  
        }
        
        
    }
    public function save_templates(){
        $data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
        
        $this->load->model('settingsmod');
        
        $res = $this->settingsmod->update_templates($data);
      
        if($res == '1'){
        	$response['success'] = true;
			$response['message'] = 'Success saving template settings!';
		
            $this->echo_response($response,200,'OK!');
            
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
            $this->echo_response($response,201,'NOT OK !');
        
        }
    }
    
    public function backup(){
        $this->load->model('settingsmod');
        $this->settingsmod->start_backup();
    }
    
    public function get_users(){
        $this->load->model('settingsmod');
        $results = $this->settingsmod->get_users();
        
        if($results){
        	$response['has_date'] = true;
			$response['data'] = $results;
		
            $this->echo_response($response,200,'OK!');
            
        } else {
        	$response['success'] = false;
			$response['message'] = 'Error saving template settings!';

            $this->echo_response($response,200,'OK!');
        
        }
    }
    
    public function set_active($id){
        $this->load->model('settingsmod');
        $result = $this->settingsmod->set_active($id);
        
        if($result){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
    }
    
    public function set_inactive($id){
        $this->load->model('settingsmod');
        $result = $this->settingsmod->set_inactive($id);
        
        if($result){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
    }
    
    public function reset_password($id){
        $this->load->model('settingsmod');
        $mix = $this->settingsmod->reset_password($id);
        
        if($mix['result']){
            $response['success'] = true;
            $response['mix'] = $mix;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
    }
    
    
    /*=============================================================================
    *
    *                                CONTROL
    *
    *============================================================================*/
    
    public function get_controls(){
        $this->load->model('settingsmod');
        
        $res = $this->settingsmod->get_controls();
        
        if($res){
            $response['has_data'] = true;
            $response['data'] = $res;
            $this->echo_response($response,200,'OK!');  
        } else {
            $response['has_data'] = false;
            $response['data'] = 'Error in retrieving controls';
            $this->echo_response($response,500,'OK!');  
        }
    }
    
    public function update_control($control, $value){
        $this->load->model('settingsmod');
        $res = $this->settingsmod->update_control($control,$value);
        
        echo 'Result: ' . $res;
    }
    
    /*=============================================================================
    *
    *                                STATION
    *
    *============================================================================*/
    
    public function save_station_settings(){
        $data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
        $this->load->model('settingsmod');
        $res = $this->settingsmod->update_station($data);
      
        if($res){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }  
    }
    
    public function station_information(){
        $this->load->model('settingsmod');
        $res = $this->settingsmod->get_station_information();
        
          if($res){
            $response['has_data'] = true;
            $response['settings'] = $res;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
            $this->echo_response($response,201,'Not OK!');
        }  
    }
}