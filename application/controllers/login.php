<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LogIn extends Ss_controller {
	
	function __construct(){
		//So in case we have to do something in the constructor we dont override the parent constructor.
		parent::__construct();
		header('Access-Control-Allow-Origin: *'); 
	}
	
	function index(){
        $ip = $this->get_ip();
       
        $data = array();
        
		if($this->is_logged_in()){
			redirect(base_url() . 'home');
            
			$data['title'] = "Login";
            $data['page_content'] = "login_view.php";
            $extra_css = array(
         
                "public/system_css/login.css",
             );

            $data['extra_css'] = $extra_css;

            $extra_js = array(
                "public/system_js/modules/main.js",
                "public/system_js/login.js",
                "public/system_js/modules/login_module.js"
            );

            $data['extra_js'] = $extra_js;
     
		}else{
            if($this->ion_auth->get_attempts_num($ip) >= 3){
                $data['page_content'] = "lock.php";
                 $data['title'] = "Lock";
                $extra_js = array(
                    "public/system_js/modules/main.js",
                    "public/system_js/lock.js",
                );
                
                $data['extra_js'] = $extra_js;    
            }else {
                $data['title'] = "Login";
                $data['page_content'] = "login_view.php";
                $extra_css = array(
                    
                    "public/system_css/login.css",
                 );

                $data['extra_css'] = $extra_css;

                $extra_js = array(
                    "public/system_js/modules/main.js",
                    "public/system_js/login.js",
                    "public/system_js/modules/login_module.js"
                );

                $data['extra_js'] = $extra_js;
            
            }  
		}	
       $this->load->view('includes/template.php',$data);
	}
    function test(){
        $username = 'pnp-2015-0002';
		$password = '12345678';
		$email = 'jay@gmail.com';
		$additional_data = array(
								'first_name' => 'JC',
								'last_name' => 'Frane',
								);
		$group = array(); // Sets user to admin. No need for array('1', '2') as user is always set to member by default

		$this->ion_auth->register($username, $password, $email, $additional_data, $group);
    }
    
    function clear_attempts(){
      
        $ip = $this->get_ip();
       
        $this->ion_auth->clear_login_attempts($ip);
        
        if($this->ion_auth->is_max_login_attempts_exceeded($ip)){
            $this->load->model('usermod');
            //force clear. Beastmode tang ina!
            $this->usermod->force_clear($ip);
        }
        
        
        echo 'Attempts ' . $this->ion_auth->get_attempts_num($ip) . "\n";
    }
    function get_ip(){
        
        $ip = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR');
        
        
        return $ip;
        
    }
	function loguser(){
		$data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		
        $identity = $data['batch_id'];
		$password = $data['password'];
		$remember =  FALSE; // remember the user
        
        $this->ion_auth->login($identity, $password, $remember);
       
        if($this->ion_auth->is_max_login_attempts_exceeded($identity)){
            $this->session->set_flashdata('message', 'You have too many login attempts');
            $response['success'] = false;
            $response['identity'] = $identity;
            $response['attempts'] = $this->ion_auth->get_attempts_num($identity);
            $this->echo_response($response,200,'OK');	
        }   else {
            
            if ($this->ion_auth->logged_in()){
                $this->load->model('pnpmod');
                //set sessions cookies.

                $results = $this->pnpmod->get_pnp($data['batch_id']);
              
                $session = array(
                    "username" => $data['batch_id'],
                    "first_name" => $results[0]->first_name,
                    "middle_name" => $results[0]->middle_name,
                    "last_name" => $results[0]->last_name,
                    "rank" => $results[0]->rank,
                    "is_logged_in" => true
                );

                $this->session->set_userdata($session);
                //redirect to home page.
                $response['success'] = true;
                $response['session'] = $session;
                $this->echo_response($response,200,'OK');	
            } else {
                //redirect to log-in page with error message
                $response['success'] = false;
                $this->echo_response($response,201,'Not OK!');	
            }
        
        }
       
		
        /*$this->load->model('usermod');
		
		$validated = $this->usermod->validate_credentials($data);
		
		//when $result returns true redirect to home page.
		if($validated){
			$this->load->model('pnpmod');
			//set sessions cookies.
			
			$results = $this->pnpmod->get_pnp($data['batch_id']);
			$account = $this->pnpmod->get_user($data['batch_id']);
			$session = array(
				"username" => $data['batch_id'],
				"first_name" => $results[0]->first_name,
				"middle_name" => $results[0]->middle_name,
				"last_name" => $results[0]->last_name,
                "rank" => $results[0]->rank,
                "access_type" => $account[0]->access_type,
				"is_logged_in" => true
			);
			
			$this->session->set_userdata($session);
			//redirect to home page.
			$response['success'] = true;
            $response['session'] = $session;
			$this->echo_response($response,200,'OK');	
		}else{
			//redirect to log-in page with error message
			$response['success'] = false;
			$this->echo_response($response,201,'Not OK!');	
		}*/
	}
	
}