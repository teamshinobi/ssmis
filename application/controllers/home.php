<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Ss_controller {
	
	function __construct(){
		parent::__construct();
	}

	public function index(){
		if($this->is_logged_in()){
            $this->load->model('homemod');
			$data['title'] = "Home";
			$data['page_content'] = "home_view.php";
			$data['logout'] = "/./ssmis/home/logout";
            $data['violators'] = $this->homemod->count_table('violators');
            $data['cases'] = $this->homemod->count_table('cases');
            $data['pnps'] = $this->homemod->count_table('pnps');
            
            
            $data['active_nav'] = '';
			//you can add as many as you want
			$views = array(
                "includes/sidebar.php",
                "loading_modal.php"
            );
            
			$data['extra_view'] = $views;
			
			//extra stylesheets for the page. 
			$stylesheets = array(
                "public/included_css/jquery.dataTables.min.css",
                "public/included_css/jquery.mCustomScrollbar.css",
				"public/system_css/home.css"
			);
			$data['extra_css'] = $stylesheets;
			
			//extra js
			$js = array(
                "public/system_js/sidebar.js",
                "public/included_js/jquery.canvasjs.min.js", 
                "public/included_js/jquery.dataTables.min.js",
                "public/included_js/jquery.mCustomScrollbar.min.js",
                "public/included_js/bootbox.min.js",
                 "public/included_js/morris.min.js",
                "public/included_js/raphael-min.js",
                "public/system_js/modules/main.js",
                "public/system_js/modules/home_module.js",
                "public/system_js/home.js",
            
            );
			$data['extra_js'] = $js;
			
			$this->load->view('includes/template.php',$data);
		}else{
			redirect(base_url());
		}	
	}
	
	public function render_content(){
		if($this->is_logged_in()){
			$this->render_viewcasereports_content();		
		}else{
			redirect(base_url());
		}		
	}
    
    public function monthly_case_report(){
        $this->load->model('homemod');
        $monthly_cases = $this->homemod->get_monthly_report();
        
          
        if($monthly_cases){
            $response['error'] = false;
			$response['message'] = 'Success retrieving report!';
			$response['monthly_cases'] = $monthly_cases;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['error'] = false;
			$response['message'] = 'No reports has been retrieved!';
			
			$this->echo_response($response,201,'NOT OK!');
        }
    }
    public function annual_case_report(){
        $this->load->model('homemod');
        $annual_cases = $this->homemod->get_annual_report();
        
        if($annual_cases){
            $response['error'] = false;
			$response['message'] = 'Success retrieving report!';
			$response['annual_cases'] = $annual_cases;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['error'] = false;
			$response['message'] = 'No reports has been retrieved!';
			
			$this->echo_response($response,201,'NOT OK!');
        }
    }
    
    public function yearly_acc_report(){
        $this->load->model('reportmod');
        $results = $this->reportmod->yearly_acc_report(null,true);
        
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
			$this->echo_response($response,201,'NOT OK!');
        }
    }
    
    public function montly_acc_report(){
        $this->load->model('reportmod');
        $results = $this->reportmod->month_acc_report(null,null,true,true);
        
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
			$this->echo_response($response,201,'NOT OK!');
        }
    }
    
    public function statistics_dangerous_drugs(){
        $this->load->model('reportmod');
        
        $results = $this->reportmod->statistics_dangerous_drugs(null, null, null, true);
        
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
			$this->echo_response($response,201,'NOT OK!');
        }
    }

}