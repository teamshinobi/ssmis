<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Archives extends Ss_controller {
    
    
    public function index(){
        if($this->is_logged_in()){
			$this->render_content();
		}else{
			redirect(base_url());
		}	
    }
    function file_manager(){
        $data['title'] = "File Manager";
		$data['page_content'] = "archives_view/file_manager_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'Archives';
        
        $views = array(
			"includes/sidebar.php",
        
			"loading_modal.php",
		);
        
        $data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
          "public/included_css/elfinder.min.css",
            "public/included_css/file-manager-theme.css",
		);
        
  
		$data['extra_css'] = $stylesheets;
        
        //extra js
		$js = array(
            "public/included_js/jquery.browser.min.js",
            "public/included_js/elfinder.min.js",
			"public/system_js/sidebar.js",
            "public/system_js/modules/main.js",
            "public/system_js/file_manager.js",   
		);
        
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
    
    }
    
    
    function render_content(){
        $data['title'] = "Archives";
		$data['page_content'] = "archives_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'Archives';
        
        $views = array(
			"includes/sidebar.php",
            "archives_view/show_casereports_modal.php",
			"loading_modal.php",
		);
        
        $data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/jquery.contextMenu.css",	
		);
        
  
		$data['extra_css'] = $stylesheets;
        
        //extra js
		$js = array(
			"public/system_js/sidebar.js",
          
            "public/included_js/jquery.dataTables.min.js",
            "public/included_js/jquery.contextMenu.js",
            "public/included_js/bootbox.min.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/archives_module.js",
            "public/system_js/archives.js",
           
		);
        
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
    
    }
    
    function casereports(){
        $this->load->model('archivesmod');
        $result = $this->archivesmod->get_all_unarchived();
        
        
         if($result){
            $response['success'] = true;
            $response['has_data'] = true;
            $response['message'] = 'Successfully retrieved case reports';
            $response['data'] = $result;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
            $this->echo_response($response,201,'NOT OK !');
        }    
    }
    
    function retrieve(){
        $this->load->model('archivesmod');
        $result = $this->archivesmod->get_archives();
        if($result){
            $response['success'] = true;
            $response['has_data'] = true;
            $response['message'] = 'Successfully retrieved archives';
            $response['data'] = $result;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
            $this->echo_response($response,201,'NOT OK !');
        }
    }
    
    function archive(){
        $data = $this->input->post(NULL, TRUE); 
        
        $this->load->model('archivesmod');
        $mix = $this->archivesmod->archive(json_decode($data['cases']));
    
        if($mix === TRUE){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'NOT OK !');
        }
    }
    
    function restore_from_web($id){
        $this->load->model('archivesmod');
        $mix = $this->archivesmod->restore_archive($id);
        
        if($mix == 1){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'NOT OK !');
        }
    }
    
    function download($archive_id){
        $this->load->model('archivesmod');
        $this->archivesmod->download_archive($archive_id);
    }
    
    function get_archive_settings(){
        $this->load->model('archivesmod');
        $result = $this->archivesmod->get_archive_settings();
        
        if($result){
            $response['has_data'] = true;
            $response['settings'] = $result;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
            $this->echo_response($response,201,'NOT OK !');
        }
        
    }
    
    function do_archiving(){
        $this->load->model('archivesmod');
        $result = $this->archivesmod->do_automatic_archiving();   
  
        if($result === false){
            $response['success'] = true;   
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,200,'OK!');
        }
    }
    
    
    function update_archive_freq($value){
        $this->load->model('archivesmod');
        $mix = $this->archivesmod->update_archive_freq_settings($value);
        
        if($mix == 1){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'NOT OK !');
        }
    }
    function init_file_manager(){
        $this->load->model('archivesmod');
        $this->archivesmod->elfinder_init();    
    }
    
}