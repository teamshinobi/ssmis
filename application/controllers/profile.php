<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Ss_controller {
	
	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
		}else{
			echo 'Unidentify access!';
		}	
	}
	
	public function render_content(){
		$id = $this->session->userdata('username');
		
		$data['title'] = "Profile";
		$data['page_content'] = "pnp_views/profile_view.php";
        $data['active_nav'] = "";
        $data['logout'] = "/./ssmis/home/logout";
		$data['id'] = $id;
        
		
		$this->load->model('pnpmod');
		$profile = $this->pnpmod->get_pnp($id);
		$data['profile'] = $profile;
		
		$views = array(
			"includes/sidebar.php",
			
		);
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			"http://fonts.googleapis.com/css?family=Play",
			"http://fonts.googleapis.com/css?family=Raleway",
			"public/system_css/fullcalendar.css",
			"public/system_css/profile.css",
		);
		$data['extra_css'] = $stylesheets;
		
		$js = array(
			"public/system_js/modules/main.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/fullcalendar.min.js",
			"public/included_js/calendar.js",
			"public/included_js/filestyle.js",
			"public/system_js/modules/profile_module.js",
			"public/system_js/pnp_profile.js",
			"http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js",
			"public/included_js/scriptcam.js",
			
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
}