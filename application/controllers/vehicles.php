<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicles extends Ss_controller {
    
    
    public function index(){
        $data['title'] = "Vehicles";
        $data['active_nav'] = 'Miscellaneous';
		$data['page_content'] = "vehicles_view.php";
        $data['logout'] = "/./ssmis/logout";
        
        $views = array(	
			"includes/sidebar.php",
            "vehicles_view/add_vehicle_modal.php",
            "vehicles_view/view_vehicle_modal.php",
            "loading_modal.php",
		);
        
        $stylesheets = array(
        	
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/jquery.contextMenu.css",
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/included_js/jquery.contextMenu.js",
            "public/included_js/bootbox.min.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/vehicles_module.js",
            "public/system_js/vehicles.js",
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        $this->load->view('includes/template.php', $data);	
    }
    public function retrieve(){
        $this->load->model('vehiclesmod');
        $data = $this->vehiclesmod->getall();
        
        if($data){
            $response['success'] = true;
            $response['message'] = 'Successfully saved!';
			$response['data'] = $data;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
    }
    
    public function add(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('vehiclesmod');
        $result = $this->vehiclesmod->insert($data);
        
        if($result->success === true){
            $response['success'] = true;
            $response['message'] = 'Successfully saved!';
			$response['id'] = $result->next_id;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
			$response['db_err'] = $result->db_err;
			$this->echo_response($response,201,'Not OK!');
        }
        //$this->vehiclesmod->upload_photo($data);
    }

    public function id(){
        $this->load->model('vehiclesmod');
   
        $id = $this->vehiclesmod->vehicles_id();
       
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}
    
    }
    public function view($id){
        $this->load->model('vehiclesmod');
        $result = $this->vehiclesmod->get_vehicle($id);


        
        if($result){
            $response['error'] = false;
            $response['message'] = 'vehicle successfully retrieve!';
            $response['data'] = $result;
            $this->echo_response($response,200,'OK!');
        }else{
            $response['error'] = true;
            $response['message'] = 'Vehicle not retrieve';
            $response['db_err'] = $result;
        
            $this->echo_response($response,200,'OK!');
        }

    }
    public function update_vehicle(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('vehiclesmod');
        $result = $this->vehiclesmod->update($data);

        
          if($result){
            $response['success'] = true;
            $response['message'] = 'vehicle successfully update!';
            $response['data'] = $result;
            $this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false;
            $response['message'] = 'Vehicle not update';
            
            $this->echo_response($response,200,'OK!');
        }
    }
    public function remove_vehicle($id){
        $this->load->model('vehiclesmod');
        $this->vehiclesmod->remove_vehicle($id);
    }
    
    
}
    