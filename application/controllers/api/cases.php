<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Cases API

class Cases extends Ss_controller {
    
    public function retrieve(){
        $this->load->model('api/api_casesmod');
        $results = $this->api_casesmod->retrieve();
        
        if ($results) {
            $response['has_data'] = true;
        	$response['data'] = $results;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
        	$response['message'] = 'No results returned...!';
			$this->echo_response($response,200,'OK!');
        }
    }
    
    
    public function get($case_id){
        $this->load->model('api/api_casesmod');
        $results = $this->api_casesmod->get($case_id);
        
        if ($results) {
            $response['has_data'] = true;
        	$response['data'] = $results;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
        	$response['message'] = 'No results returned...!';
			$this->echo_response($response,200,'OK!');
        }
    }

}