<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User extends Ss_controller {



    public function login(){
        
        $data = $this->input->post(NULL,TRUE);
        
        $batchId = $data['batch_id'];
		$password = $data['password'];
		$remember =  FALSE; // remember the user
        
        $this->ion_auth->login($batchId, $password, $remember);
        
        if ($this->ion_auth->logged_in()){
            $this->load->model('pnpmod');
            
            //set sessions cookies
            $results = $this->pnpmod->get_pnp($data['batch_id']);
            
            $profile = array(
                "username" => $data['batch_id'],
                "first_name" => $results[0]->first_name,
                "middle_name" => $results[0]->middle_name,
                "last_name" => $results[0]->last_name,
                "rank" => $results[0]->rank,
                "is_logged_in" => true,
                'is_admin' =>  $this->ion_auth->is_admin(),
            );

            $response['success'] = true;
            $response['profile'] = $profile;
            $this->echo_response($response,200,'OK!');	
        
        } else {
            $response['success'] = false;
            $response['message'] = 'Cannot Log-in';
            $this->echo_response($response,201,'Not OK!');	
        }
        
    }
    
}