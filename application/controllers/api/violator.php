<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Violator API

class Violator extends Ss_controller {
    
    public function retrieve(){
        
        $this->load->model('api/api_violatormod');
        
        $results = $this->api_violatormod->retrieve();
        
        if ($results) {
            $response['has_data'] = true;
        	$response['data'] = $results;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
        	$response['message'] = 'No results returned...!';
			$this->echo_response($response,200,'OK!');
        }
    }
    
    public function get($id){
        $this->load->model('api/api_violatormod');
        $results = $this->api_violatormod->get($id);
        
        if ($results) {
            $response['has_data'] = true;
        	$response['data'] = $results;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
        	$response['message'] = 'No results returned...!';
			$this->echo_response($response,200,'OK!');
        }
    }
}