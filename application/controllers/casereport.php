<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casereport extends Ss_controller {
	
	function __construct(){
		parent::__construct();
	}
	
	public function index(){
		if($this->is_logged_in()){
			$this->render_newcasereport_content();
			//echo 'logged-in';
		}else{
			redirect(base_url());
		}	
		
	}
	
	public function viewcasereports(){
		if($this->is_logged_in()){
			$this->render_viewcasereports_content();		
		}else{
			redirect(base_url());
		}		
	}
	
	private function render_newcasereport_content(){
		$data['title'] = "New Case Report";
		$data['page_content'] = "new_casereport_view.php";
		$data['logout'] = "casereport/logout";
        $data['active_nav'] = 'Case Report';
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
			"includes/prompt_modal.php",	
			"casereport_views/add_investigator_modal.php",
            "casereport_views/add_fingerman_modal.php",
            "casereport_views/add_photographer_modal.php",
			"casereport_views/add_arrestingofficers_modal.php",	
			"casereport_views/add_violator_form.php",	
			"casereport_views/view_violator_modal.php",		
			"casereport_views/existing_violators_modal.php",
			"casereport_views/add_item_form.php",
			"casereport_views/show_item_modal.php",
            "casereport_views/show_barangays_modal.php",
			"casereport_views/add_seizing_officer_modal.php",
			"casereport_views/add_narcotics_item_form.php",
			"casereport_views/show_narcotics_item_modal.php",
            "casereport_views/show_spotreports_modal.php",
            "casereport_views/show_preoperations_modal.php",
            "casereport_views/show_coordinations_modal.php",
            "casereport_views/show_mainaffidavits_modal.php",
            "casereport_views/show_suppaffidavits_modal.php",
            "casereport_views/add_witness_modal.php",
            "casereport_views/view_witness_modal.php",
			"loading_modal.php",
		);
		
		
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			
			"public/included_css/bootstrap-timepicker.css",
			"public/included_css/datepicker.css",
			"public/included_css/jquery.dataTables.min.css",
			"public/included_css/jquery.toolbars.css",
			"public/system_css/casereport.css",	
			"public/included_css/jquery.contextMenu.css",				
			"public/included_css/jquery.cleditor.css",
			"public/included_css/magnific-popup.css",
            "public/included_css/summernote.css",
			"public/included_css/jtoast.css",
            "public/system_css/forms.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/included_js/filestyle.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
			"public/included_js/jquery.contextMenu.js",
			"public/included_js/jquery.cleditor.min.js",
			"public/included_js/jquery.toolbar.js",
			"public/included_js/jquery.magnific-popup.js",
			"public/included_js/jtoast.js",
            "public/included_js/summernote.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/casereport_module.js",
            "public/system_js/modules/casereport_violators_module.js",
            "public/system_js/modules/casereport_inventory_module.js",
            "public/system_js/modules/attachment_module.js",
            "public/system_js/modules/witness_module.js",
			"public/system_js/casereport.js",
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	private function render_viewcasereports_content(){
		$data['title'] = "Case Reports";
		$data['page_content'] = "view_casereports_view.php";
		
		//you can add as many as you want
		$views = array("includes/sidebar.php");
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array( 
			
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array("public/system_js/sidebar.js");
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	
	/*==================================================================
	*
	*							AJAX CALLS
	*
	*===================================================================*/
	//handles case report form submission
	public function submit(){
		$data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
        
		$this->load->model('casereportmod');
		
		$result = $this->casereportmod->submit($data);
		
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Success saving case report!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error saving transaction.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}
	}
	
	/*=======================================================
	*
	* Upload temp photos for violators. This temp photos will be
	* moved on their permanent folders on submit
	*
	* $tag = left/right/front/rear
	*=========================================================*/
    public function delete_distribution(){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $this->load->model('casereportmod');
        $res = $this->casereportmod->delete_distribution($data['id']);
        
        if($res){
           $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,200,'Not OK!');
        }
        
    }
    public function update_distribution(){
        $this->load->model('casereportmod');
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $res = $this->casereportmod->edit_distribution($data);
        
        if($res == 1){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,200,'Not OK!');
        }
    }
    
    public function distributions($case_id){
        $this->load->model('casereportmod');
        $results = $this->casereportmod->get_distributions($case_id);
        
        if($results){
            $response['has_data'] = true;
			$response['message'] = 'Success!';
            $response['data'] = $results;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['has_data'] = false;
			$response['message'] = 'No distribution seen';
			$this->echo_response($response,201,'Not OK!');
        }
        
    }
    
    public function distribute(){
        $this->load->model('casereportmod');
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $data['when'] = $this->ss_date(false);
        $result = $this->casereportmod->distribute($data);
        
        if($result){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,200,'Not OK!');
        }
    }
    
    public function check_blotter($blotter){
        $this->load->model('casereportmod');
        $result = $this->casereportmod->check_blotter($blotter);
        
        if($result){
			$response['has_duplicate'] = true;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['has_duplicate'] = false;
		
			$this->echo_response($response,200,'Not OK!');
		}		
    }
    
	public function caseid(){
		$this->load->model('casereportmod');
		
		$id = $this->casereportmod->get_case_id();
		
		if($id){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['id'] = $id;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error retrieving pnps.';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'Not OK!');
		}		
	}
    
    //create default scene img.
    public function documentfolders(){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
		$this->load->model('casereportmod');
        $this->casereportmod->create_document_folders($data['case_id']);
    }
    
    public function uploadscene(){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $this->load->model('casereportmod');
        $this->casereportmod->upload_scene($data['case_id']);
    }
    
    //removes subfiles in a dir in temp folders.
    public function remove_temp_doc_dir($case_id, $folder){
        $this->load->model('casereportmod');
        $this->casereportmod->remove_temp_doc_dir($case_id, $folder);
        
        //always true. Allright!
        $response['success'] = true;
        $this->echo_response($response,200,'OK!');
    }
    //scans directory
    public function scan_temp_dir($case_id){
 
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
       
        $this->load->model('casereportmod');
        $files = $this->casereportmod->scan_temp_dir($case_id);
        
      if($files){
            $response['error'] = false;
			$response['message'] = 'Success!';
            $response['files'] = $files;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['error'] = true;
			$response['message'] = 'Unable to scan directory';
			$this->echo_response($response,201,'Not OK!');
        }
    }
    
    //scans directory
    public function scan_permanent_dir($case_id){
        $this->load->model('casereportmod');
        $files = $this->casereportmod->scan_permanent_dir($case_id);
       
        if($files){
            $response['error'] = false;
			$response['message'] = 'Success!';
            $response['files'] = $files;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['error'] = true;
			$response['message'] = 'Unable to scan directory';
			$this->echo_response($response,201,'Not OK!');
        }
    }
    public function upload_documents($type){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $this->load->model('casereportmod');
        
        if($type === 'laboratory'){
            $result = $this->casereportmod->upload_laboratory($data['case_id']);
        } else if ($type === 'medical'){
            $result = $this->casereportmod->upload_medical($data['case_id']);
        } else if ($type === 'search'){
            $result = $this->casereportmod->upload_search($data['case_id']);
        } else if ($type === 'warrant'){
            $result = $this->casereportmod->upload_warrant($data['case_id']);
        } else if ($type === 'others'){
            $result = $this->casereportmod->upload_others($data['case_id']);
        }
        
        
        if($result == 1){
            $response['error'] = false;
			$response['message'] = 'Success!';
			$this->echo_response($response,200,'OK!');
        } else {
            $response['error'] = true;
			$response['message'] = 'Failed to upload!';
			$this->echo_response($response,201,'Not OK!');
        }
    }
    
	public function uploadtemp(){
		
		$data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
		$this->load->model('casereportmod');
        
		$this->casereportmod->upload_temp($data);
		
	}
    
    public function removetemp(){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $this->load->model('casereportmod');
		$this->casereportmod->remove_temp($data['id']);  
    }
	
	//return the list of all active pnp only
	public function activepnps(){
		$this->load->model('casereportmod');
		
		$result = $this->casereportmod->get_active_pnps();
		
		if($result){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error retrieving pnps.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}		
	}
    
    public function activeinvestigators(){
        $this->load->model('casereportmod');
		$result = $this->casereportmod->get_active_investigators();
		if($result){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error retrieving pnps.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}
    }
	
	//loads the first violator_id on page load
	public function firstviolatorid(){
		$this->load->model('casereportmod');
		
		$id = $this->casereportmod->get_first_violator_id();
		
		if($id){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['id'] = $id;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error retrieving pnps.';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'Not OK!');
		}		
	}
	
	//gets violator next id base on the $prev_id
	public function nextviolatorid($prev_id){
		$this->load->model('casereportmod');
		
		$id = $this->casereportmod->get_violator_next_id($prev_id);	
		
		if($id){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['id'] = $id;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error retrieving pnps.';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'Not OK!');
		}	
	}
	
	//gets all violators in the database
	public function violators(){
		$this->load->model('casereportmod');
		
		$results = $this->casereportmod->get_all_violators();	
		
		if($results){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
			$this->echo_response($response,200,'Not OK!');
		}	
	}
	
	public function existingviolator($id){
		$this->load->model('casereportmod');
		
		$result = $this->casereportmod->get_violator($id);	
				
		if($result){
			$response['error'] = false;
			$response['message'] = 'Success!';
			
			//make a copy of images from permanent folder to temp folder.
			$bool = $this->casereportmod->copy_permanent_to_temp($id);	
			
			$response['data'] = $result;
			
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error retrieving violators.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}	
	}
	
	public function update_existing_violator_image(){
		$data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
		$this->load->model('casereportmod');
		$this->casereportmod->update_existing_violator_image($data);	
		
		$response['message'] = "Violator Image updated";
		$this->echo_response($response,200,'Ok!');	
	}
	
	/*=================================================================================
	*
	*								INVENTORY SECTION
	*	
	*===============================================================================*/
	public function inventoryid(){
		$this->load->model('casereportmod');
		
		$id = $this->casereportmod->inventoryid();
		
		if($id){
			$response['id'] = $id;
			$response['success'] = true;
			$response['message'] = "Success!";
			$this->echo_response($response,200,'Ok!');	
		} else {
			$response['message'] = "Failed!";
			$response['success'] = false;
			$this->echo_response($response,200,'Not Ok!');	
		}
	}
	
	public function itemfirstid(){
		$this->load->model('casereportmod');
		
		$id = $this->casereportmod->itemfirstid();
		
		if($id){
			$response['id'] = $id;
			$response['success'] = true;
			$response['message'] = "Success!";
			$this->echo_response($response,200,'Ok!');	
		} else {
			$response['message'] = "Failed!";
			$response['success'] = false;
			$this->echo_response($response,200,'Not Ok!');	
		}
	}
	
	public function narcoticsitemfirstid(){
		$this->load->model('casereportmod');
		
		$id = $this->casereportmod->narcoticsitemfirstid();
		
		if($id){
			$response['id'] = $id;
			$response['success'] = true;
			$response['message'] = "Success!";
			$this->echo_response($response,200,'Ok!');	
		} else {
			$response['message'] = "Failed!";
			$response['success'] = false;
			$this->echo_response($response,200,'Not Ok!');	
		}
	}
	
	public function uploadtempitem(){
		//var_dump($violator_id);	
		$data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
		$what = $data['what'];
		$this->load->model('casereportmod');
		$this->casereportmod->upload_temp_item($data);
		if($what === 'item'){
			$id = $this->casereportmod->item_next_id($data['item_id']);
		}else{
			$id = $this->casereportmod->nitem_next_id($data['item_id']);
		}
		
		
		if($id){
			$response['id'] = $id;
			$response['success'] = true;
			$response['message'] = "Success!";
			$this->echo_response($response,200,'Ok!');	
		}else{
			$response['message'] = "Failed!";
			$response['success'] = false;
			$this->echo_response($response,200,'Not Ok!');	
		}
	}
    
    /*=================================================================================
	*
	*								ATTACHMENT SECTION
	*	
	*===============================================================================*/
    
    
    public function get_current_time(){
    
    }
}