<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends Ss_controller {



    
    function index(){
        $data['title'] = "Error";
        $data['active_nav'] = 'Home';
		$data['page_content'] = "error_404.php";
        $data['logout'] = '/./ssmis/error/logout';
        
        $views = array(	
			"includes/sidebar.php",
          
		);
        
        $stylesheets = array(
            "http://fonts.googleapis.com/css?family=Play",
            "http://fonts.googleapis.com/css?family=Raleway",	
      
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
           
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        
        $this->load->view('includes/template.php',$data);
    
    }


}