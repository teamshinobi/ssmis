<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pnp extends Ss_controller {
	
	public function __construct(){
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
	}
	
	public function index(){
		
		if($this->is_logged_in()){
			$this->render_maintenance_content();
		}else{
			echo 'Unidentify access!';
		}	
	}
	//view a specific police
	public function view($id){
		$this->db->cache_delete_all();
		if($this->is_logged_in()){
			$this->render_view_police_content($id);
		}else{
			echo 'Unidentify access!';
		}	
	}
	//view a specific group
	public function viewgroup($id){
		$this->db->cache_delete_all();
		if($this->is_logged_in()){
			$this->render_view_group_content($id);
		}else{
			echo 'Unidentify access!';
		}	
	}
	//group controller method
	public function groups(){
		$this->db->cache_delete_all();
		if($this->is_logged_in()){
			$this->render_groups_content();
		}else{
			echo 'Unidentify access!';
		}	
	}
	
	
	public function render_view_group_content($id){
		$data['title'] = "PNP";
		$data['page_content'] = "pnp_views/view_group.php";
        $data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'View PNP';
		
		//gets the information about the groups
		$this->load->model('pnpmod');
		$group = $this->pnpmod->get_group($id);
		$data['group'] = $group;
	
			
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
			"includes/prompt_modal.php",
			"pnp_views/edit_group_modal.php",
			"pnp_views/add_member_modal.php",
             "loading_modal.php",
    
		);
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			//"public/system_css/default.css", 
			//"public/system_css/sidebar.css",
			"public/system_css/groups.css",
			"public/included_css/jquery.dataTables.min.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			//"public/system_js/sidebar.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/pnp_module.js",
			"public/system_js/group.js",
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	public function render_groups_content(){
		$data['title'] = "PNP";
		$data['page_content'] = "pnp_views/groups_view.php";
        $data['logout'] = "logout";
        $data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'View PNP';
		
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
			"includes/prompt_modal.php",
			"pnp_views/add_group_modal.php",
            "loading_modal.php",
		);
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
	
			//"public/system_css/default.css", 
			//"public/system_css/sidebar.css",
			"public/system_css/groups.css",
			"public/included_css/jquery.dataTables.min.css",
			"public/included_css/TableTools.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			//"public/system_js/sidebar.js",
			"public/included_js/filestyle.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/ZeroClipboard.js",
			"public/included_js/jquery.tableTools.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/pnp_module.js",
			"public/system_js/group.js",
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	public function render_view_police_content($id){
	
		$data['title'] = "Profile";
		$data['page_content'] = "pnp_views/view_pnp.php";
		$data['id'] = $id;
        $data['logout'] = "/./ssmis/home/logout";
            $data['active_nav'] = 'PNP';
		
		//gets the information about the pnp
		$this->load->model('pnpmod');
		$police = $this->pnpmod->get_pnp($id);
		$data['police'] = $police[0];
		
		//you can add as many as you want
		$views = array(
		
			"includes/sidebar.php",
			"includes/prompt_modal.php",
			//"pnp_views/affiliations_modal.php",
            "pnp_views/update_pnp_modal.php",
            "pnp_views/upload_modal.php",
		
		);
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			
			"public/included_css/jquery.dataTables.min.css",
            "public/included_css/datepicker.css",
			"public/included_css/TableTools.css",
            "public/system_css/pnp.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/filestyle.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/pnp_module.js",
			"public/system_js/view_pnp.js",	
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	
	}
	public function render_maintenance_content(){
		$data['title'] = "PNP";
		$data['page_content'] = "pnp_view.php";
        $data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'PNP';
		
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
            "loading_modal.php",
			"pnp_views/add_modal.php",
		);
        
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			"public/included_css/jquery.dataTables.min.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/included_js/jquery.dataTables.min.js",
			"public/system_js/modules/main.js",
            "public/system_js/services.js",
			"public/system_js/modules/pnp_module.js",
			"public/system_js/pnp.js",	
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	
	
	/*==================================================================
	*
	*					Report Generator Methods
	*
	*===================================================================*/
	//generate a list of all pnp officers regardless of their units and remarks
	public function report($type){
		$this->load->model('pnpmod');
		if($type === 'all'){
			$this->pnpmod->report_all();
		}	
	}
	/*==================================================================
	*
	*							AJAX CALLS
	*
	*===================================================================*/
    public function get($id){
        $this->load->model('pnpmod');
        $police = $this->pnpmod->get_pnp($id);
        
        if($police){
			$response['error'] = false;
			$response['message'] = "Success!";
			$response['data'] = $police[0];
			
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
		
    }
	/*
	* Accepts 1 parameter only to determine what to save.
	* if p retrieve police
	* if g retrieve group
	*
	*/
	public function retrieve($type){
		$this->db->cache_delete_all();
		$this->load->model('pnpmod');
	
		if($type==='p'){
			$data = $this->pnpmod->get_all_pnp();
		}else{
			$data = $this->pnpmod->get_all_groups();
		}
		if($data){
			$response['error'] = false;
			$response['message'] = "Success!";
			$response['data'] = $data;
			
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
            $response['sEcho'] = 1;
            $response['iTotalRecords'] = '0';
            $response['iTotalDisplayRecords'] = '0';
            $response['aaData'] = []; 
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
	
	//gets important field in group
    public function retrieveg(){
        $this->load->model('pnpmod');
        
        $data = $this->pnpmod->get_all_groups_imp();
        if($data){
			$response['error'] = false;
			$response['message'] = "Success!";
			$response['data'] = $data;
			
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
    }
	/*
	* Accepts 1 parameter only to determine what to save.
	* if p save police
	* if g save group
	*
	*/
	public function add($type){
		$data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		
		$this->load->model('pnpmod');
		if($type==='p'){
			$result = $this->pnpmod->insert_new_pnp($data);
		}else{
			$result = $this->pnpmod->insert_new_group($data);
		}
		
		//if the insertion is successful, $result will be true else will be a message error
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Successfully saved!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Police not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	
	/*
	* Accepts 1 parameter only to determine what to update.
	* if p update police
	* if g update group
	*
	*/
	public function update($type){
		
		$data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		//var_dump($data);
		//var_dump($_FILES);
		$this->load->model('pnpmod');

		if($type === 'p'){
			$result = $this->pnpmod->update_pnp($data);
		}else{
			$result = $this->pnpmod->update_group($data);
		}
		
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Police successfully updated!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Police not updated!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}	
	}
	
	//joins a police to a particular group
	public function join_group($group_id, $batch_id){
	
		$this->load->model('pnpmod');
		
		$result = $this->pnpmod->add_member($group_id,$batch_id);
		
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Member added!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Member not added!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}
	}
	
	//retrieve all members of a particular group giving group id
	public function retrieve_group_members($group_id){
		$this->load->model('pnpmod');
		
		$result = $this->pnpmod->retrieve_members($group_id);
		
		if($result){
			$response['error'] = false;
			$response['message'] = 'Successful!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Unable to retrieve members!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}
		
	}
	//retrieve all police that is not still a member of a particular group
	public function retrieve_not_group_members($group_id){
		$this->load->model('pnpmod');
		
		$result = $this->pnpmod->retrieve_not_members($group_id);
		
		if($result){
			$response['error'] = false;
			$response['message'] = 'Successful!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Unable to retrieve members!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}	
	}
	
	//retrieve all groups that the a particular police has joined
	public function retrieve_affiliations($pnp_id){
		$this->load->model('pnpmod');
		
		$result = $this->pnpmod->get_affiliations($pnp_id);
		
		if($result){
			$response['error'] = false;
			$response['message'] = 'Successful!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Unable to retrieve members!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}	
	}
    
	//remove affiliation of a particular police giving his id and the group id
	public function remove_affiliation($group_id, $batch_id){
		$this->load->model('pnpmod');
		
		$result = $this->pnpmod->remove_affiliation($group_id, $batch_id);
		
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Member removed from the group.!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Unable to remove the member from the group. If you think this is a bug, please report.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}	
	}
    
    public function upload(){
        $data = $this->input->post(NULL,TRUE);
		$this->load->model('pnpmod');
		$result = $this->pnpmod->update_photo($data);	
		
		if($result){
			$response['message'] = "Successfully updated.";
			$response['success'] = true;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['message'] = "Photo not updated for some reasons.";
			$response['success'] = false;
			$this->echo_response($response,200,'NOT OK!');
		}	
    }
    
    //gets police privileges base on ID
    public function privileges(){
        $data = $this->input->post(NULL,TRUE);
     	$this->load->model('pnpmod');
		$result = $this->pnpmod->check_privileges($data['pnp_id']);	
        if($result){
			$response['message'] = "Successfully retrieved.";
			$response['has_account'] = true;
            $response['account_details'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['message'] = "No account yet.";
			$response['has_account'] = false;
			$this->echo_response($response,200,'OK!');
		}	
    }
    
    public function account(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('pnpmod');
		$result = $this->pnpmod->create_account($data['pnp_id']);
        
        if($result['result']) {
            $response['success'] = true;
            $response['password'] = $result['password'];
            $this->echo_response($response,200,'OK!');
        } else {   
            $response['success'] = false;
            $this->echo_response($response,201, 'Not OK!');
        }
    }
    
    public function as_investigator($id){
        $this->load->model('pnpmod');
        $result = $this->pnpmod->get_as_investigator($id);
        
        if($result) {
			$response['has_data'] = true;
            $response['result'] = $result;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['message'] = "This police hasn't been assigned as investigator.";
			$response['has_data'] = false;
			$this->echo_response($response,201,'OK!');
        }
    }
    
    public function as_arresting($id){
        $this->load->model('pnpmod');
        $result = $this->pnpmod->get_as_arresting($id);
         
        if($result) {
			$response['has_data'] = true;
            $response['result'] = $result;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['message'] = "This police hasn't been assigned as an arresting officer.";
			$response['has_data'] = false;
			$this->echo_response($response,201,'OK!');
        }
    }
    
}



