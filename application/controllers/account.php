<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends Ss_controller {
    
    public function index(){
        $data['title'] = "Account";
		$data['page_content'] = "pnp_views/account_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = '';
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
			"loading_modal.php",
		);
		
		
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			"http://fonts.googleapis.com/css?family=Play",
			"http://fonts.googleapis.com/css?family=Raleway",
			"public/included_css/bootstrap-timepicker.css",
			"public/included_css/datepicker.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/included_js/filestyle.js",
			"public/included_js/bootbox.min.js",
             "public/system_js/modules/main.js",
            "public/system_js/modules/account_module.js",
            "public/system_js/account.js",
			
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
    }
    public function update(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('accountmod');
        
        $mix = $this->accountmod->update($data);
        
        if($mix['result']){
            $response['success'] = true;
            $response['message'] = 'Successfully updated information';
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Information not updated!';
            $response['errors'] = $mix['errors'];
            $this->echo_response($response,200,'Not OK!');
        }
    }
    public function change_pass(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('accountmod');
        
        $pnp_id = $this->session->userdata('username');
      
        $data['pnp_id'] = $pnp_id;
        
        $result = $this->accountmod->change_pass($data);
        
        if ($result) {
            $response['success'] = true;
            $response['message'] = 'Successfully updated the password!';
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Unable to change password.'; 
            $this->echo_response($response,201,'Not OK!');
        }
    }
}