<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Editcasereport extends Ss_controller {
    	
	function __construct(){
		parent::__construct();
	}
    
    
    function index(){
    
    }
    
    //clear all images/files in temps folder
    public function clear_temps(){
       $this->ss_clear_temps();
    }
    
    function edit($case_id){
        if (!$this->get_access_non_json($case_id)) {
            redirect(base_url());
        } else {
            $data['title'] = "Edit Case Report";
            $data['page_content'] = "edit_casereport_view.php";
            $data['logout'] = "casereport/logout";
            $data['active_nav'] = 'Case Report';
            //you can add as many as you want
            $views = array(
                "includes/sidebar.php",
                "includes/prompt_modal.php",
                "casereport_views/show_barangays_modal.php",
                "casereport_views/add_investigator_modal.php",
                "casereport_views/add_fingerman_modal.php",
                "casereport_views/add_photographer_modal.php",
                "casereport_views/add_arrestingofficers_modal.php",	
                "casereport_views/add_violator_form.php",	
                "casereport_views/view_violator_modal.php",		
                "casereport_views/existing_violators_modal.php",
                "casereport_views/add_item_form.php",
                "casereport_views/show_item_modal.php",
                "casereport_views/add_seizing_officer_modal.php",
                "casereport_views/add_narcotics_item_form.php",
                "casereport_views/show_narcotics_item_modal.php",
                "casereport_views/show_spotreports_modal.php",
                "casereport_views/show_preoperations_modal.php",
                "casereport_views/show_coordinations_modal.php",
                "casereport_views/show_mainaffidavits_modal.php",
                "casereport_views/show_suppaffidavits_modal.php",
                "casereport_views/add_witness_modal.php",
                "casereport_views/view_witness_modal.php",
                "loading_modal.php",
            );


            $data['extra_view'] = $views;

            //extra stylesheets for the page. 
            $stylesheets = array(
                
                "public/included_css/bootstrap-timepicker.css",
                "public/included_css/datepicker.css",
                "public/included_css/jquery.dataTables.min.css",
                "public/included_css/jquery.toolbars.css",
                "public/system_css/casereport.css",	
                "public/included_css/jquery.contextMenu.css",				
                "public/included_css/jquery.cleditor.css",
                "public/included_css/magnific-popup.css",
                "public/included_css/summernote.css",
                "public/included_css/jtoast.css",
                "public/system_css/forms.css",
            );
            $data['extra_css'] = $stylesheets;

            //extra js
            $js = array(
                "public/system_js/sidebar.js",
                "public/included_js/bootstrap-datepicker.js",
                "public/included_js/bootstrap-timepicker.js",
                "public/included_js/filestyle.js",
                "public/included_js/jquery.dataTables.min.js",
                "public/included_js/bootbox.min.js",
                "public/included_js/jquery.contextMenu.js",
                "public/included_js/jquery.cleditor.min.js",
                "public/included_js/jquery.toolbar.js",
                "public/included_js/jquery.magnific-popup.js",
                "public/included_js/jtoast.js",
                "public/included_js/summernote.js",
                "public/system_js/modules/main.js",
                "public/system_js/modules/casereport_module.js",
                "public/system_js/modules/casereport_violators_module.js",
                "public/system_js/modules/casereport_inventory_module.js",
                "public/system_js/modules/attachment_module.js",
                "public/system_js/modules/edit_casereport_module.js",
                "public/system_js/modules/witness_module.js",
                "public/system_js/edit_casereport.js",
            );
            $data['extra_js'] = $js;

            $this->load->view('includes/template.php',$data);
        }
        
    }
    
    //give access to user if he she is the investigator or booker of chief of the particular case
    public function get_access_non_json($case_id){
        $user = $this->get_user();
        $this->load->model('viewcasereportmod');
        $access = $this->viewcasereportmod->get_case_access($user,$case_id);
        
        return $access;
        
    }
    //gets all full information about case report
    function full($id){
        $this->load->model('editcasereportmod');
		
		$result = $this->editcasereportmod->get_full_details($id);	
				
		if($result){
			$response['error'] = false;
			$response['message'] = 'Success!';
			$response['case'] = $result;
			
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error getting full details.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,201,'Not OK!');
		}	
    }
    
    function violator_photos(){
        $data = $this->input->post(NULL, TRUE);
        
        $this->load->model('editcasereportmod');

        $bool = $this->editcasereportmod->transfer_violator_photos_to_temp(json_decode($data['ids']));
        
        if($bool == 1){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
    }
    
    function item_photos(){
        $data = $this->input->post(NULL, TRUE);
        
        $this->load->model('editcasereportmod');

        $bool = $this->editcasereportmod->transfer_items_photos_to_temp($data['case_id']);
        
        if($bool == 1){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,500,'Not OK!');
        }
    }
    
    
    function scene_photos(){
        $data = $this->input->post(NULL, TRUE);
        
        $this->load->model('editcasereportmod');
     

        $bool = $this->editcasereportmod->transfer_scene_photos_to_temp($data['case_id']);
        
        if($bool == 1){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,500,'Not OK!');
        }
    }
    function submit(){
        $data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
        
        $this->load->model('editcasereportmod');
		
		$result = $this->editcasereportmod->submit($data);
        
      	if($result === true){
			$response['error'] = false;
			$response['message'] = 'Success saving case report!';
			$response['data'] = $result;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = true;
			$response['message'] = 'Error saving transaction.';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'Not OK!');
		}
    
    }
    
}