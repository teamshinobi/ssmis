<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends Ss_controller {
    
    public function index(){
        
        $k = $this->input->get(NULL, TRUE);
        
        $data['title'] = "Search";
		$data['page_content'] = "search_results_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'Search';
        $data['k'] = $k['k'];
       
        $views = array(
			"includes/sidebar.php",
			"loading_modal.php",
		);
		
		
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			
            "public/included_css/bootcards-desktop.css",
            "public/included_css/jquery.mCustomScrollbar.css",
            "public/system_css/search.css",
             "public/system_css/gallery.css"
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
            "public/included_js/dentist.js",
            "public/included_js/jquery.mCustomScrollbar.min.js",
            "public/included_js/simplePagination.js",
            "public/included_js/sscardify.js",
            "public/included_js/masonry.min.js",
            "public/included_js/imagesloaded.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/paginator_module.js",
            "public/system_js/modules/search_module.js",
            
            "public/system_js/search.js",
		);
		$data['extra_js'] = $js;
        
        $this->load->view('includes/template.php',$data);
    }
    
    //search for cases keywords
    public function q_case($q = ""){
    
        $real_str = str_replace('+',' ',$q);      
        
        
        $this->load->model('searchmod');
        
        
        $results = $this->searchmod->crawl_case($real_str);
        
        if($results){
			$response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = false;
            $response['has_data'] = false;
			$response['message'] = 'No results returned';
			
		
			$this->echo_response($response,201,'Not OK!');
        }
   }
    
    public function q_violators($q = ""){
        $real_str = str_replace('+',' ',$q);      
        
        $this->load->model('searchmod');
        
        $results = $this->searchmod->crawl_violators($real_str);
        
        if($results){
			$response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = false;
            $response['has_data'] = false;
			$response['message'] = $this->searchmod->get_query_msg();
			
			$this->echo_response($response,201,'Not OK!');
        }
    }
    
    public function q_pnp($q = ""){
        $real_str = str_replace('+',' ',$q);
        
        $this->load->model('searchmod');
        
        $results = $this->searchmod->crawl_pnp($real_str);
        
        if($results){
			$response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = false;
            $response['has_data'] = false;
			$response['message'] = $this->searchmod->get_query_msg();
			
			$this->echo_response($response,201,'Not OK!');
        }
        
    }
    
    public function q_images($q = "") {
        $real_str = str_replace('+',' ',$q);
        
        $this->load->model('searchmod');
        
        $results = $this->searchmod->crawl_images($real_str);
        
        if($results){
			$response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
		} else {
			$response['error'] = false;
            $response['has_data'] = false;
			$response['message'] = $this->searchmod->get_query_msg();
			
			$this->echo_response($response,201,'Not OK!');
        }
    }

}