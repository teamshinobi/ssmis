<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends Ss_controller {
    
    
    function index(){
        $data['title'] = "Gallery";
		$data['page_content'] = "gallery_view.php";
		$data['logout'] = "casereport/logout";
        $data['active_nav'] = 'Violators';
        
        $views = array(
			"includes/sidebar.php",
			"loading_modal.php",
		);
        
        $data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
            "public/included_css/magnific-popup.css",
            "public/system_css/gallery.css",
		);
        
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
            "public/included_js/masonry.min.js",
            "public/included_js/imagesloaded.js",
            "public/included_js/jquery.magnific-popup.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/gallery_module.js",
            "public/system_js/gallery.js",
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
    }
    
    public function violators(){
        $this->load->model('gallerymod');
        $results = $this->gallerymod->violators();
        if($results){
			$response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
		} else {
            $response['has_data'] = false;
			$response['message'] = 'No data retrieved!';
			$this->echo_response($response,200,'Not OK!');
		}	
    }
    
}