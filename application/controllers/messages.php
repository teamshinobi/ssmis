<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends Ss_controller {
		
	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
		}else{
			echo 'Unidentify access!';
		}	
	}
	
	public function render_content(){
		$data['title'] = "Messages";
		$data['page_content'] = "pnp_views/messages_view.php";
        $data['active_nav'] = "";
        $data['logout'] = "/./ssmis/logout";
        
		$views = array(
			"includes/sidebar.php",
		);
		$data['extra_view'] = $views;
		
		$stylesheets = array(
			"http://fonts.googleapis.com/css?family=Play",
			"http://fonts.googleapis.com/css?family=Raleway",
			"public/system_css/fullcalendar.css",
			"public/system_css/profile.css",
			
		);
		$data['extra_css'] = $stylesheets;
		
		$js = array(
			"public/system_js/messages.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/filestyle.js",
			"public/system_js/modules/main.js",
			"public/included_js/bootbox.min.js",
			"public/system_js/modules/profile_module.js",
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}

	public function add(){
		date_default_timezone_set('Singapore');
		$currDate = new DateTime();
		if(isset($_POST['Sent'])){
			$data = array(
				'sender' => $this->session->userdata('username'),
				'recipient' => $this->input->post('recipient'),
				'subject' => $this->input->post('subject'),
				'content' => $this->input->post('content'),
				'msg_date' => $currDate->format('Y-m-d'),
				'msg_time' => $currDate->format('H:i:s'),
				'inbox_status' => 'recieved',
				'sent_item_status' => 'sent',
				'msg_status' => 'unread',
				);
			$this->load->model('profilemod');
			$result = $this->profilemod->insert_message($data);
			$name = $_POST['Sent'];
			if($result === true){
				$response['name'] = $name;
				$response['error'] = false;
				$response['message'] = 'Successfully saved!';
				$this->echo_response($response,200,'OK!');
			}else{
				$response['error'] = true;
				$response['message'] = 'Message not saved!';
				$response['db_err'] = $result;
			
				$this->echo_response($response,200,'OK!');
			}
		}else if(isset($_POST['Draft'])){
			$data = array(
				'sender' => $this->session->userdata('username'),
				'recipient' => $this->input->post('recipient'),
				'subject' => $this->input->post('subject'),
				'content' => $this->input->post('content'),
				'msg_date' => $currDate->format('Y-m-d'),
				'msg_time' => $currDate->format('H:i:s'),
				'draft_status' => 'saved',
				);
			$this->load->model('profilemod');
			$result = $this->profilemod->insert_message($data);
			
			if($result === true){
				$response['error'] = false;
				$response['message'] = 'Successfully saved!';
				$this->echo_response($response,200,'OK!');
			}else{
				$response['error'] = true;
				$response['message'] = 'Police not saved!';
				$response['db_err'] = $result;
			
				$this->echo_response($response,200,'OK!');
			}
		}
		
	}
	
	public function inbox(){
		$id = $this->session->userdata('username');
		
		$this->load->model('profilemod');
		$data = $this->profilemod->get_inbox($id);

		$increment = 0;
		
		foreach ($data as $object) {
			$policeNames = $object;
			$sender = $this->profilemod->get_pnp_name($data[$increment]->sender);
			$recipient = $this->profilemod->get_pnp_name($data[$increment]->recipient);

			$policeNames->senderName = $sender[0]->last_name . ', ' . $sender[0]->first_name;
			$policeNames->recipientName = $recipient[0]->last_name . ', ' . $recipient[0]->first_name;
			
			$increment++;
		}

		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
			$this->echo_response($response,201,'OK, but with errors.');
		}

	}
	
	public function drafts(){
		$id = $this->session->userdata('username');
		
		$this->load->model('profilemod');
		$data = $this->profilemod->get_drafts($id);
		
		$result = array();
		$increment = 0;
		
		foreach ($data as $object) {
			$policeNames = $object;
			$sender = $this->profilemod->get_pnp_name($data[$increment]->sender);
			$recipient = $this->profilemod->get_pnp_name($data[$increment]->recipient);

			$policeNames->senderName = $sender[0]->last_name . ', ' . $sender[0]->first_name;
			$policeNames->recipientName = $recipient[0]->last_name . ', ' . $recipient[0]->first_name;
			$increment++;
		}

		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');

		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
		
	}
	
	public function sents(){
		$id = $this->session->userdata('username');
		
		$this->load->model('profilemod');
		$data = $this->profilemod->get_sents($id);
		
		$result = array();
		$increment = 0;
		
		foreach ($data as $object) {
			$policeNames = $object;
			$sender = $this->profilemod->get_pnp_name($data[$increment]->sender);
			$recipient = $this->profilemod->get_pnp_name($data[$increment]->recipient);

			$policeNames->senderName = $sender[0]->last_name . ', ' . $sender[0]->first_name;
			$policeNames->recipientName = $recipient[0]->last_name . ', ' . $recipient[0]->first_name;
			$increment++;
		}

		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
		
		
	}

	public function view($id){
		$this->load->model('profilemod');
		$data = $this->profilemod->get_message_by_id($id);
		$sender = $data[0]->sender;
		$recipient = $data[0]->recipient;
		if($data){
			$this->load->model('pnpmod');
			$data1 = $this->pnpmod->get_pnp($sender);
			$data2 = $this->pnpmod->get_pnp($recipient);
			$senderName = $data1[0]->last_name . ', ' . $data1[0]->first_name;
			$recipientName = $data2[0]->last_name . ', ' . $data2[0]->first_name;
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
		
		if($data){
			$response['data'] = $data;
			$response['senderName'] = $senderName;
			$response['recipientName'] = $recipientName;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
	
	public function update(){
		$data = $this->input->post(NULL, TRUE);
		
		$this->load->model('profilemod');
		$result = $this->profilemod->update_message($data);
		
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Narcotic/s successfully saved!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Narcotic/s not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
		
	public function updateDraft(){
		$data = $this->input->post(NULL, TRUE);
		
		$this->load->model('profilemod');
		
		date_default_timezone_set('Singapore');
		$currDate = new DateTime();
		
		if(isset($_POST['Sent'])){
			
			$result1 = $this->profilemod->delete_message($data);
			
			$data = array(
				'sender' => $this->session->userdata('username'),
				'recipient' => $this->input->post('recipient'),
				'subject' => $this->input->post('subject'),
				'content' => $this->input->post('content'),
				'msg_date' => $currDate->format('Y-m-d'),
				'msg_time' => $currDate->format('H:i:s'),
				'inbox_status' => 'recieved',
				'sent_item_status' => 'sent',
				'msg_status' => 'unread',
			);
			
			$result2 = $this->profilemod->insert_message($data);
			$name = $_POST['Sent'];
			if($result2 === true){
				$response['name'] = $name;
				$response['error'] = false;
				$response['message'] = 'Successfully saved!';
				$this->echo_response($response,200,'OK!');
			}else{
				$response['error'] = true;
				$response['message'] = 'Police not saved!';
				$response['db_err'] = $result;
			
				$this->echo_response($response,200,'OK!');
			}
		}else{
			$data = array(
				'sender' => $this->session->userdata('username'),
				'recipient' => $this->input->post('recipient'),
				'subject' => $this->input->post('subject'),
				'content' => $this->input->post('content'),
				'msg_date' => $currDate->format('Y-m-d'),
				'msg_time' => $currDate->format('H:i:s'),
				'draft_status' => 'saved',
			);
			
			$result = $this->profilemod->update_draft($data);
			
			if($result === true){
				$response['error'] = false;
				$response['message'] = 'Narcotic/s successfully saved!';
				$this->echo_response($response,200,'OK!');
			}else{
				$response['error'] = true;
				$response['message'] = 'Narcotic/s not saved!';
				$response['db_err'] = $result;
			
				$this->echo_response($response,200,'OK!');
			}
		}
	}
	
	public function deleteMessage(){
		$data = $this->input->post(NULL, TRUE);
		
		$this->load->model('profilemod');
		$data1 = $this->profilemod->get_message_by_id($_POST['id']);
		
		
		if($_POST['column'] === 'inbox_status'){
			if($data1[0]->sent_item_status === $_POST['status']){
				$result = $this->profilemod->delete_message($data);
			}else{
				$result = $this->profilemod->update_message($data);
			}
		}else if($_POST['column'] === 'sent_item_status'){
			if($data1[0]->inbox_status == $_POST['status']){
				$result = $this->profilemod->delete_message($data);
			}else{
				$result = $this->profilemod->update_message($data);
			}
		}else if($_POST['column'] === 'draft_status'){
			$result = $this->profilemod->delete_message($data);
		}
		
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Messages successfully deleted!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Messages not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
		
	}
}