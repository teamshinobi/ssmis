<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Tip extends Ss_controller {
        
        function index(){
            $data['title'] = "Tips";
            $data['page_content'] = "tip_view.php";
            $data['logout'] = "/./ssimsi/home/logout";
            $data['active_nav'] = 'Tips';

            $views = array(
                "includes/sidebar.php",
                "tip_views/add_new_tip.php",
                "tip_views/import_target.php",
                "loading_modal.php",
            );
            
            $data['extra_view'] = $views;
		
            //extra stylesheets for the page. 
            $stylesheets = array(
                "http://fonts.googleapis.com/css?family=Play",
                "http://fonts.googleapis.com/css?family=Raleway",
                "public/included_css/bootstrap-timepicker.css",
                "public/included_css/jquery.contextMenu.css",
                "public/included_css/datepicker.css",
                "public/included_css/jquery.dataTables.min.css",
            );
            $data['extra_css'] = $stylesheets;

            //extra js
            $js = array(
                "public/system_js/sidebar.js",
                "public/included_js/jquery.dataTables.min.js",
                "public/included_js/jquery.contextMenu.js",
                "public/included_js/bootbox.min.js",
                "public/included_js/bootstrap-datepicker.js",
			    "public/included_js/bootstrap-timepicker.js",
                "public/system_js/modules/main.js",
                "public/system_js/modules/tip_module.js",
                "public/system_js/tip.js",
                );
            $data['extra_js'] = $js;

            $this->load->view('includes/template.php',$data);
            
        }
        
        public function add(){
            $data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
            
            $this->load->model('tipmod');
            
            $ret = $this->tipmod->add($data);
            
            if ($ret) {
                $response['success'] = true;
                $this->echo_response($response,200,'OK!');
            } else {
                $response['success'] = false;  
                $this->echo_response($response,201,'Not OK!');
            }
         
        }
        
        public function retrieve(){
            $this->load->model('tipmod');   
            $ret = $this->tipmod->retrieve(); 
            
             if($ret) {
                $response['has_data'] = true;
                $response['data'] = $ret;
                $this->echo_response($response,200,'OK!');
            } else {
                $response['has_data'] = false;  
                $this->echo_response($response,201,'Not OK!');
            }
        }
    
        public function show($id){
            $this->load->model('tipmod');   
            $ret = $this->tipmod->show($id); 
            
            if ($ret) {
                $response['success'] = true;
                $this->echo_response($response,200,'OK!');
            } else {
                $response['success'] = false;  
                $this->echo_response($response,201,'Not OK!');
            }
           
        }
        public function unshow($id){
            $this->load->model('tipmod');   
            $ret = $this->tipmod->unshow($id); 
            
            if ($ret) {
                $response['success'] = true;
                $this->echo_response($response,200,'OK!');
            } else {
                $response['success'] = false;  
                $this->echo_response($response,201,'Not OK!');
            }
        }
        
        public function get_shown_targets(){
            $this->load->model('tipmod');   
            $ret = $this->tipmod->shown_targets(); 
        
            if($ret) {
                $response['has_data'] = true;
                $response['data'] = $ret;
                $this->echo_response($response,200,'OK!');
            } else {
                $response['sEcho'] = 1;
                $response['iTotalRecords'] = '0';
                $response['iTotalDisplayRecords'] = '0';
                $response['aaData'] = [];
                $this->echo_response($response,201,'Not OK!');
            }
        
        }
    }