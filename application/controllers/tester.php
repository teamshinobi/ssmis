<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tester extends Ss_controller {


    public function index(){
        echo 'Pota!';
    }
    
    public function delete(){
        $this->load->model('testermod');
        $this->testermod->delete();
        echo 'Deleted!';
    }
    public function delete_violators(){
        $this->load->model('testermod');
        $this->testermod->delete_violators();
        echo 'Deleted';
    }
    public function delete_attachments(){
        $this->load->model('testermod');
        $this->testermod->delete_attachments();
        echo 'Deleted';
    }
    
    public function testtime(){
        $this->load->model('testermod');
        $this->testermod->test_time();
    }
    
    public function testblob(){
        $this->load->model('testermod');
        
        $obj = $this->testermod->get_fingerprints()[0];
        
        echo '<img src="data:image/jpeg;base64,'.base64_encode( $obj->thumb_rimg ).'"/>';
    }   
}