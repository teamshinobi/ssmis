<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit_trail extends Ss_controller {
    
    public function index(){
        $data['title'] = "Audit Trail";
		$data['page_content'] = "audit_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'Audit Tral';
    
       
        $views = array(
			"includes/sidebar.php",
			"loading_modal.php",
		);
		
		
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
			"http://fonts.googleapis.com/css?family=Play",
			"http://fonts.googleapis.com/css?family=Raleway",
            "public/included_css/jquery.dataTables.min.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/audit_module.js",
            "public/system_js/audit.js",

		);
		$data['extra_js'] = $js;
        
        
        $this->load->view('includes/template.php',$data);
    }
    
    public function add(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('audit_trailmod');
        $result = $this->audit_trailmod->add($data);

        if($result == 1){
            $response['success'] = true;
            $response['message'] = 'Audit trail saved';
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Audit trail not saved';
            $this->echo_response($response,201,'NOT OK !');
        }
    }
    
    public function retrieve(){
        $this->load->model('audit_trailmod');
        $result = $this->audit_trailmod->retrieve();
        
        if($result){
            $response['success'] = true;
            $response['has_data'] = true;
            $response['message'] = 'Successfully retrieved audit trails';
            $response['data'] = $result;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
            $this->echo_response($response,201,'NOT OK !');
        }
        
    }
}