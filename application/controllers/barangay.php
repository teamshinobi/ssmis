<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barangay extends Ss_controller {

	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
		}else{
			redirect(base_url());
		}
	}

	public function render_content(){
		$data['title'] = "Barangay";
        $data['active_nav'] = 'Miscellaneous';
		$data['page_content'] = "barangay_view.php";
        $data['logout'] = 'barangay/logout';
	
		$views = array(	
			"includes/sidebar.php",
			"barangay_view/add_modal.php",
			"barangay_view/view_modal.php",
		);
		$data['extra_view'] = $views;

			$stylesheets = array(
				
			"public/included_css/jquery.dataTables.min.css",
			"public/included_css/jquery.contextMenu.css",
		
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/jquery.contextMenu.js",
			"public/system_js/modules/main.js",
			"public/included_js/bootbox.min.js",
			"public/system_js/modules/barangay_module.js",
			"public/system_js/barangay.js",
		
		);
		$data['extra_js'] = $js;

		$this->load->view('includes/template.php', $data); 

	}
	/*=======================================================
	*
	*  Accepts two possible parameters
	*
	*	a = retrieve all columns in barangay_tbl
	*	in = retrieve id and name only in barangay_tbl
	*
	*========================================================*/
	
	public function retrieve(){
		$this->load->model('barangaymod');
		
		$data = $this->barangaymod->get_all_barangay();
		if($data){
			$response['data'] = $data;
			
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
	public function get($id){
		$this->load->model('barangaymod');
		$result = $this->barangaymod->get_barangay($id);

		if($result){
			$response['error'] = false;
			$response['message'] = 'This law is successfully saved!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'This law is not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'NOT OK!');
		}
	}
	public function update(){
		$data = $this->input->post(NULL, TRUE);// returns all POST items with XSS filter

		$this->load->model('barangaymod');
		$result = $this->barangaymod->update_barangay($data);

		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Barangay information succesfully updated!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'This law is not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'NOT OK!');
		}
	}
	public function add(){
		$this->load->model('barangaymod');
		$id = $this->barangaymod->barangay_id();
		
		$data = array(
				'barangay_id' => $id,
				'barangay_name' => $this->input->post('barangay_name'),
				'city' => $this->input->post('city'),
				'barangay_incidents' => $this->input->post('barangay_incidents'),
				'barangay_category' => $this->input->post('barangay_category'),
				'barangay_chairman' => $this->input->post('barangay_chairman'),
			
		);
		$this->load->model('barangaymod');
		$result = $this->barangaymod->insert_barangay($data);
		//if the insertion is successful, $result will be true else will be a message error
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Barangay/s successfully saved!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Barangay/s not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	public function report($type){
		$this->load->model('barangaymod');
		if($type === 'all'){
			$this->barangaymod->report_all();
		}
	}
	 public function id(){
        $this->load->model('barangaymod');
   
        $id = $this->barangaymod->barangay_id();
       
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	public function view($id){

		$this->load->model('barangaymod');
		$result = $this->barangaymod->get_barangay($id);
		
		if($result){
			$response['error'] = false;
			$response['message'] = 'barangay successfully retrieve0!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Barangay not retrieve';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	public function remove_barangay($id){
		$this->load->model('barangaymod');
		$result = $this->barangaymod->remove_barangay($id);

		if($result){
			$response['error'] = false;
			$response['message'] = 'barangay successfully deleted!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Barangay not deleted';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}

	}
	

}