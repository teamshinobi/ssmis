<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ra extends Ss_controller {

	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
		}else{
			redirect(base_url());
		}
	}

	public function render_content(){
		$data['title'] = "RA9165";
		$data['page_content'] = "ra_view.php";
		$data['active_nav'] = 'Miscellaneous';
		$data['logout'] = "/./ssmis/logout";

	
		$views = array(	
			"includes/sidebar.php",
			"ra_views/add_modal.php",
			"ra_views/view_modal.php",
		);
		$data['extra_view'] = $views;
		
		$stylesheets = array(
				
			"public/included_css/jquery.contextMenu.css",
			"public/included_css/jquery.dataTables.min.css",
			
			
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
			"public/included_js/jquery.contextMenu.js",
			"public/system_js/modules/ra_module.js",
			"public/system_js/ra.js",
			"public/system_js/modules/main.js",

		
		);

	    $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        $this->load->view('includes/template.php', $data);	 

	}

	        
	public function retrieve(){
		$this->load->model('ramod');
		$data = $this->ramod->get_all_ra();

		if($data){
			$response['data'] = $data;
			
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}	
	}

	public function get($id){
		$this->load->model('ramod');
		$result = $this->ramod->get_charge($id);

		if($result){
			$response['error'] = false;
			$response['message'] = 'Successfully retrieved!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'This law is not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'NOT OK!');
		}
	}
	public function add(){

		$this->load->model('ramod');
		$id = $this->ramod->charges_id();

	 	$data = array(
			'charge_id' => $id,
			'article' => $this->input->post('article'),
			'section' => $this->input->post('section'),
			'description' => $this->input->post('description'),
			'fine' => $this->input->post('fine'),
			'imprisonment' => $this->input->post('imprisonment'),
			
		);	
		$this->load->model('ramod');
		$result = $this->ramod->insert_ra($data);


		//if the insertion is successful, $result will be true else will be a message error
		if($result == true){
			$response['error'] = false;
			$response['message'] = 'Barangay/s successfully saved!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Barangay/s not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	public function update(){
		$data = $this->input->post(NULL, TRUE);// returns all POST items with XSS filter

		$this->load->model('ramod');
		$result = $this->ramod->update_ra($data);

		if($result === true){
			$response['error'] = false;
			$response['message'] = 'This law is successfully saved!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'This law is not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'NOT OK!');
		}
	}
	public function id(){
        $this->load->model('ramod');
   
        $id = $this->ramod->charges_id();
       
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	public function remove_charges($id){
		$this->load->model('ramod');
		$result = $this->ramod->remove_charge($id);
      
		if($result === TRUE){
			$response['error'] = false;
			$response['message'] = 'Charge successfully deleted!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Charge not deleted';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}

	}

	public function report($type){
		$this->load->model('ramod');
		if($type === 'all'){
			$this->ramod->report_all();
		}
	}
}