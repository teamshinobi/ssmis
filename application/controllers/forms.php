<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forms extends Ss_controller {
    
    public function index(){
       
    }
    
    public function spotreport(){
        $data['title'] = "Spot Report";
        $data['active_nav'] = 'Forms';
		$data['page_content'] = "form_views/spot_report_view.php";
        $data['logout'] = '/./ssmis/home/logout';
        
        $views = array(	
			"includes/sidebar.php",
            "form_views/add_delivery_boy_modal.php",
            "form_views/view_spot_reports_modal.php",
            "form_views/view_specific_spot_report_modal.php",
            "loading_modal.php",
		);
        
        $stylesheets = array(	
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/datepicker.css",
            "public/included_css/bootstrap-timepicker.css",
            "public/included_css/summernote.css",
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
            "public/included_js/summernote.js",
            "public/system_js/modules/spot_report_module.js",
            "public/system_js/spot_report.js",
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        $this->load->view('includes/template.php', $data);	
    }
    public function preoperation(){  
        $data['title'] = "Pre-operation";
        $data['active_nav'] = 'Forms';
		$data['page_content'] = "form_views/preoperation_view.php";
        $data['logout'] = '/./ssmis/home/logout';
        
        $views = array(	
			"includes/sidebar.php",
            "form_views/add_target_modal.php",
            "form_views/import_target_modal.php",
            "form_views/view_preoperations_modal.php",
            "form_views/input_control_modal.php",
            "loading_modal.php",
		);
        $stylesheets = array(
          	
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/datepicker.css",
            "public/included_css/bootstrap-timepicker.css",
            "public/included_css/jquery.contextMenu.css",	
            "public/system_css/forms.css",	
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
             "public/included_js/jquery.contextMenu.js",
            "public/system_js/modules/preoperation_module.js",
            "public/system_js/forms.js",
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        
        $this->load->view('includes/template.php', $data);		
		
    }
    
    
    function coordination(){
   
        $data['title'] = "Coordination";
        $data['active_nav'] = 'Forms';
        $data['page_content'] = "form_views/coordination/coordination_view.php";

        $data['created_by'] = $this->get_current_user();

        $data['logout'] = '/./ssmis/home/logout';
        
        $views = array(	
            "includes/sidebar.php",
            "form_views/coordination/add_team_leader_modal.php",
            "form_views/coordination/add_members_modal.php",
            "form_views/coordination/show_coordinations_modal.php",
            "form_views/coordination/show_vehicles_modal.php",
            "form_views/input_control_modal.php",
            "loading_modal.php",
        );
        $stylesheets = array(
            	
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/datepicker.css",
            "public/included_css/bootstrap-timepicker.css",
            "public/included_css/jquery.contextMenu.css",	
            "public/system_css/forms.css",	
        );
        $js = array (
            "public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
            "public/included_js/bootstrap-timepicker.js",
            "public/included_js/jquery.contextMenu.js",
            "public/system_js/modules/coordination_module.js",
            "public/system_js/coordination.js"
        );

        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;

        $this->load->view('includes/template.php', $data);	
        
		
    }
    
   
    
    /*=======================================================
    *                       VIEW
    *
    *======================================================*/
    
    public function view($type, $id){
        if($type == "coordination"){
            $this->view_coordination($id);
        } 
    }
    public function view_coordination($id){
    
        $data['title'] = "Coordination";
        $data['active_nav'] = 'Forms';
		$data['page_content'] = "form_views/coordination/view_coordination.php";
        $data['id'] = $id;
       
        $data['logout'] = 'forms/logout';
        
        $views = array(	
			"includes/sidebar.php",
            "loading_modal.php",
		);
        $stylesheets = array(
            	
            "public/included_css/jquery.dataTables.min.css",
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
            
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        
        $this->load->view('includes/template.php', $data);	
    
    }
    
    
    /*=======================================================
    *                       EDIT
    *
    *======================================================*/
    
    public function edit($type, $id){
        if($type == 'spotreport'){
            $this->edit_spotreport($id);
        } else if ($type == "coordination") {
            $this->edit_coordination($id);
        } else if($type == 'preoperation' ){
            $this->edit_preoperation($id);
        } else if($type == 'main'){
           $this->edit_affidavit($type,$id);
        } else if($type == 'supp'){
           $this->edit_affidavit($type,$id);
        }
    }
    
    public function edit_affidavit($type,$id){
        $data['title'] = "Affidavits";
        $data['active_nav'] = 'Affidavits';
		$data['page_content'] = "form_views/affidavits/edit_affidavit.php";
        $data['id'] = $id;
        $data['type'] = $type;
        $data['logout'] = '/./ssmis/forms/logout';
        
        
        $views = array(	
			"includes/sidebar.php",
            "form_views/affidavits/add_narrator_view.php",
            "loading_modal.php",
		);
        
        $stylesheets = array(
            
            "public/included_css/summernote.css",
        );
        
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/summernote.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/edit_affidavit_module.js",
            "public/system_js/edit_affidavits.js",
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        
        $this->load->view('includes/template.php', $data);
    }
    
    
    public function edit_preoperation($id){
        $data['title'] = "Pre-operation";
        $data['active_nav'] = 'Forms';
		$data['page_content'] = "form_views/view_specific_preop_view.php";
        $data['id'] = $id;
        $data['logout'] = '/./ssmis/forms/logout';
        
        $views = array(	
			"includes/sidebar.php",
            "loading_modal.php",
            "form_views/add_target_modal.php",
            "form_views/import_target_modal.php",
            "loading_modal.php",
		);
        $stylesheets = array(
            
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/datepicker.css",
            "public/included_css/bootstrap-timepicker.css",
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
            "public/system_js/modules/view_preoperation_module.js",
            "public/system_js/forms.js",
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        
        $this->load->view('includes/template.php', $data);	
    }
    public function edit_coordination($id){
        $this->clear_resources();
        if(count($this->uri->segment_array()) == 4){
            $data['title'] = "Coordination";
            $data['active_nav'] = 'Forms';
            $data['page_content'] = "form_views/coordination/edit_coordination.php";
            $data['id'] = $id;
            $data['created_by'] = $this->get_current_user();

           $data['logout'] = '/./ssmis/forms/logout';
            
            $views = array(	
                "includes/sidebar.php",
                "form_views/coordination/add_team_leader_modal.php",
                "form_views/coordination/add_members_modal.php",
                "form_views/coordination/show_coordinations_modal.php",
                "form_views/coordination/show_vehicles_modal.php",
                "loading_modal.php",
            );
            $stylesheets = array(
                	
                "public/included_css/jquery.dataTables.min.css",
                "public/included_css/jquery.contextMenu.css",
                "public/included_css/jquery.contextMenu.css",
                "public/included_css/datepicker.css",
                "public/included_css/bootstrap-timepicker.css",
            );
            $js = array(
                "public/included_js/jquery.dataTables.min.js",
                "public/system_js/modules/main.js",
                "public/included_js/bootbox.min.js",
                "public/included_js/bootstrap-datepicker.js",
                "public/included_js/bootstrap-timepicker.js",
                "public/system_js/modules/coordination_module.js",
                "public/system_js/modules/view_coordination_module.js",
                "public/included_js/jquery.contextMenu.js",
                "public/system_js/coordination_view.js"
            );

            $data['extra_css'] = $stylesheets;
            $data['extra_js'] = $js;
            $data['extra_view'] = $views;

            $this->load->view('includes/template.php', $data);	
             
        }     
    
    }
    
    public function edit_spotreport($id){
       
        $data['title'] = "Spot Report";
        $data['active_nav'] = 'Forms';
        $data['page_content'] = "form_views/edit_spotreport_view.php";
        $data['id'] = $id;
        $data['logout'] = '/./ssmis/forms/logout';
        
        $views = array(	
			"includes/sidebar.php",
            "form_views/add_delivery_boy_modal.php",
            "loading_modal.php",
		);
        $stylesheets = array(
            
            "public/included_css/jquery.dataTables.min.css",
            "public/included_css/datepicker.css",
            "public/included_css/bootstrap-timepicker.css",
             "public/included_css/summernote.css",
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",
            "public/system_js/modules/main.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
            "public/included_js/summernote.js",
            "public/system_js/modules/spot_report_module.js",
             "public/system_js/modules/edit_spot_report_module.js",
            "public/system_js/spot_report.js",
        );
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        
        $this->load->view('includes/template.php', $data);	  
    }
    
    //gets the creator of a particular form/report
    public function get_created_by($type, $id){
        $this->load->model('formsmod');
        if($type == 'spot'){
            $creator = $this->formsmod->get_spot_creator($id);
        } else if ($type == 'coordination'){
            $creator = $this->formsmod->get_coord_creator($id);
        } else if ($type == 'preoperation'){
            $creator = $this->formsmod->get_preop_creator($id); 
        }
        
        if($creator){
            $response['success'] = true;
			$response['message'] = 'Successfully gets the creator!';
            $response['creator'] = $creator;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
			$response['message'] = 'Error getting the creator!';   
			$this->echo_response($response,500,'Error!');
        }
    
    }
    public function unattach($type){
        $data = $this->input->post(NULL,FALSE);
        $this->load->model('formsmod');
        if($type == 'coordination'){
            $table = 'coordination_form_tbl';
            $condition = array('coordination_id' => $data['id']);
            $column = 'coordination_id';
            
            $result =  $this->formsmod->unattach($table,$data['case_id'],$condition,'coordination_id');
        } else if ( $type == 'preoperation' ){
            $table = 'pre_operation_tbl';
            $condition = array('pre_op_id' => $data['id']);
            $column = 'pre_op_id';
    
            $result =  $this->formsmod->unattach($table,$data['case_id'],$condition,'pre_op_id');
        } else if ( $type == 'main' ) {
            $table = 'affidavit_tbl';
            $condition = array('affidavit_id' => $data['id']);
            $result =  $this->formsmod->unattach($table,$data['case_id'],$condition,'main_affidavit_id');
        } else if( $type == 'supp' ){
            $table = 'affidavit_tbl';
            $condition = array('affidavit_id' => $data['id']);
            $result =  $this->formsmod->unattach($table,$data['case_id'],$condition,'supp_affidavit_id');
        } else if ( $type == 'spot'){
            $table = 'spot_report_tbl';
            $condition = array('spot_report_id' => $data['id']);
            $result =  $this->formsmod->unattach($table,$data['case_id'],$condition,'spot_report_id');
        }
        
       if($result === TRUE) {
            $response['success'] = true;
			$response['message'] = 'Successfully unattached!';
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Errors in unattaching...';
            $response['db_err'] = $result;
            $this->echo_response($response,200,'OK!');
        }
        
    }
    
    
    //get all unattach forms
    public function getall_un($type){
        $this->load->model('formsmod');
        if($type == 'main'){
            $data = $this->formsmod->get_all_main_affidavits_un();
        } else if ($type == 'supplemental') {
            $data = $this->formsmod->get_all_supp_affidavits_un();
        }else if ($type == 'coordination') {
            $data = $this->formsmod->get_all_coordinations_un();
        } else if ($type == 'spot') {
            $data = $this->formsmod->get_all_spot_report_un();
        } else if ($type == 'preoperation') {
            $data = $this->formsmod->get_all_preoperations_un();
        }
     
        if($data) {
            $response['success'] = true;
			$response['message'] = 'Success';
            $response['data'] = $data;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
            $this->echo_response($response,201,'Not OK!');
        }
    }
    
    public function getall($type){
        $this->load->model('formsmod');
        if($type == 'preoperation'){
            $data = $this->formsmod->get_all_preoperations();
        } else if($type == 'coordination'){
            $data = $this->formsmod->get_all_coordinations();   
        } else if($type == 'spotreport'){
            $data = $this->formsmod->get_all_spot_reports();
        } else if($type == 'affidavits'){
            $data = $this->formsmod->get_all_affidavits();
        } else {
            echo 'Cannot retrieve any data!'; 
        }
        if($data) {
            $response['success'] = true;
			$response['message'] = 'Success';
            $response['data'] = $data;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
            $this->echo_response($response,201,'Not OK!');
        }
        
    }
    public function id($type){
        $this->load->model('formsmod');
        if($type === "preoperation"){
            $id = $this->formsmod->get_preoperation_id();
        }else if($type === "spotreport") {
            $id = $this->formsmod->get_spotreport_id();
        } else if($type === "coordination"){
            $id = $this->formsmod->get_coordination_id();
        } else if($type === "affidavit"){
            $id = $this->formsmod->get_affidavit_id();
        } else {
            $id = false;
        }
        
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}
    }
    //gets target id
    public function targetid(){
        $this->load->model('formsmod');
        $id = $this->formsmod->get_target_first_id();
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}    
    }
    
    public function targetnextid($prev_id){
        $this->load->model('formsmod');
        $id = $this->formsmod->get_target_next_id($prev_id);  
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}    
    }
    

    public function save($type){
        $data = $this->input->post(NULL,FALSE);
        $this->load->model('formsmod');
        if($type=="preoperation"){
            $result = $this->formsmod->save_preoperation($data);  
        }else if($type=="coordination"){  
            $result = $this->formsmod->save_coordination_form($data);
        }else if($type=="spotreport"){
            $result = $this->formsmod->save_spot_report($data);  
        }else if($type=="affidavit"){
            $result = $this->formsmod->save_affidavit_form($data);  
        } else{
            echo 'Page not found!';
        }
        
      
        if($result === TRUE) {
            $response['success'] = true;
			$response['message'] = 'Report successfully saved!';
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Report not saved!';
            $response['db_err'] = $result;
            $this->echo_response($response,200,'OK!');
        }
    }
    
    public function approve($id, $type="preoperation", $cn = ""){
        $this->load->model('formsmod');
        
        if($type == "preoperation"){
            $result = $this->formsmod->approve_preoperation($id,$cn);
            
        } else {
            $result = $this->formsmod->approve_coordination($id,$cn);     
        }
     
        if($result == 1){
            $response['success'] = true;
			$response['message'] = 'Successfully approved!';
			$this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false    ;
			$response['message'] = 'Update Failed! Contact IT Support.';
			$this->echo_response($response,200,'Not OK!');
        }
    }
    
    public function unapprove($id, $type="preoperation") {
        $this->load->model('formsmod');
        
        if($type == "preoperation"){
            $result = $this->formsmod->unapprove_preoperation($id);     
        } else {
            $result = $this->formsmod->unapprove_coordination($id);   
        }
     
        if($result == 1){
            $response['success'] = true;
			$response['message'] = 'Successfully unapproved!!';
			$this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false    ;
			$response['message'] = 'Update Failed! Contact IT Support.';
			$this->echo_response($response,200,'Not OK!');
        }

    }
    public function mark_as_undenied($id,$type){
        $this->load->model('formsmod');
        
        if($type == 'preoperation'){
            $table='pre_operation_tbl';
            $condition = array('pre_op_id' => $id);
            $result = $this->formsmod->mark_as_undenied($id, $table, $condition);
        } else if($type == 'coordination'){
            $table='coordination_form_tbl';
            $condition = array('coordination_id' => $id);
            $result = $this->formsmod->mark_as_undenied($id, $table, $condition);
        }
        
        
        if($result == 1){
            $response['success'] = true;
			$response['message'] = 'Successfully Denied!!';
			$this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false    ;
			$response['message'] = 'Update Failed! Contact IT Support.';
			$this->echo_response($response,200,'Not OK!');
        }
    }
    
    public function mark_as_denied($id,$type){
        $this->load->model('formsmod');
        
        if($type == 'preoperation'){
            $table='pre_operation_tbl';
            $condition = array('pre_op_id' => $id);
            $result = $this->formsmod->mark_as_denied($id, $table, $condition);
        } else if ($type == 'coordination') {
            $table='coordination_form_tbl';
            $condition = array('coordination_id' => $id);
            $result = $this->formsmod->mark_as_denied($id, $table, $condition);
        }
        
        
        if($result == 1){
            $response['success'] = true;
			$response['message'] = 'Successfully Denied!!';
			$this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false    ;
			$response['message'] = 'Update Failed! Contact IT Support.';
			$this->echo_response($response,200,'Not OK!');
        }
    }
    
    public function get($type, $id){

        $this->load->model('formsmod');
        if($type === 'preoperation'){
            $result = $this->formsmod->get_preoperation($id);
        } else if ($type === 'spotreport'){
            $result = $this->formsmod->get_spot_report($id);
        } else if ($type === 'coordination') {
            $result = $this->formsmod->get_coordination($id);
        } else if($type === 'affidavit'){
            $result = $this->formsmod->get_affidavit($id);
        }
        
         if($result){
            $response['success'] = true;
			$response['message'] = 'Successful.';
            $response['data'] = $result;
             
			$this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false;
			$response['message'] = 'Failed.';
			$this->echo_response($response,200,'Not OK!');
        }
        
    }
    
    public function update($type){
        $data = $this->input->post(NULL, FALSE);
        $this->load->model('formsmod');
      
        if($type == 'preoperation'){
            $result = $this->formsmod->update_preoperation($data);
        } else if ($type == 'spotreport'){
            $result = $this->formsmod->update_spotreport($data);
        } else if($type == 'coordination') {
            $result = $this->formsmod->update_coordination($data);    
        } else if($type == 'affidavit') {
            $result = $this->formsmod->update_affidavit($data);   
        }
        
        if($result === TRUE) {
                 $response['success'] = true;
			$response['message'] = 'Report successfull saved!';
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Report not saved!';
            $response['db_err'] = $result;
            $this->echo_response($response,200,'OK!');
        }
    }   
    
    public function delete($type, $id){
        $this->load->model('formsmod');
        if($type == 'preoperation'){
            $result = $this->formsmod->delete_preoperation($id);
        } else if($type == 'spotreport'){
            $result = $this->formsmod->delete_spotreport($id);
        } else if ($type == 'coordination'){
            $result = $this->formsmod->delete_coordination($id);
        } else if ($type == 'affidavit'){
            $result = $this->formsmod->delete_affidavit($id);
        }
         
        if($result == 1){
            $response['success'] = true;
			$response['message'] = 'Report deleted';
			$this->echo_response($response,200,'OK!');
        }else{
            $response['success'] = false    ;
			$response['message'] = 'Delete Failed!';
			$this->echo_response($response,201,'Not OK!');
        }
    }
    
    /*-------------------------------------------------------------------------
    
                                    AFFIDAVITS
    
    ---------------------------------------------------------------------------*/
    
    public function getall_affidavits($type){
        $this->load->model('formsmod');
        $data = $this->formsmod->get_all_affidavits($type);
        
        if($data) {
            $response['success'] = true;
			$response['message'] = 'Success';
            $response['data'] = $data;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $response['message'] = 'Cannot fetch data.';
            $this->echo_response($response,201,'Not OK!');
        }
        
    }
    
    public function affidavits(){
        $data['title'] = "Affidavits";
        $data['active_nav'] = 'Forms';
		$data['page_content'] = "form_views/affidavit_view.php";
        $data['logout'] = '/./ssmis/home/logout';
        
        $views = array(	
			"includes/sidebar.php",            
            "form_views/affidavits/add_narrator_view.php",
            "form_views/affidavits/show_affidavits_view.php",
            "loading_modal.php",
		);
        
        $stylesheets = array(
            	
            "public/included_css/jquery.dataTables.min.css",
             "public/included_css/jquery.contextMenu.css",	
            "public/included_css/datepicker.css",
            "public/included_css/bootstrap-timepicker.css",
            "public/included_css/summernote.css",
            "public/system_css/affidavit.css",
        );
        $js = array(
            "public/included_js/jquery.dataTables.min.js",  
            "public/included_js/jquery.contextMenu.js",
            "public/included_js/bootbox.min.js",
            "public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
            "public/included_js/summernote.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/affidavit_module.js",
            "public/system_js/affidavit.js",
        );
        
        $data['extra_css'] = $stylesheets;
        $data['extra_js'] = $js;
        $data['extra_view'] = $views;
        $this->load->view('includes/template.php', $data);	
    }
    
    /*-------------------------------------------------------------------------
    
                                    FORMS
    
    ---------------------------------------------------------------------------*/
    
    public function affidavit_pdf($id){
        $this->load->model('formsmod');
		$data = $this->formsmod->affidavit_pdf($id);
        
        $this->pdfgenerator->Output('' . $id . '.pdf', 'I');
        
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}

    }
    
    public function coordination_pdf($id){
        $this->load->model('formsmod');
		$data = $this->formsmod->coordination_pdf($id);
                
        $this->pdfgenerator->Output($id . '.pdf', 'I');
        
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
    }
    
    public function preoperation_pdf($id){
        $this->load->model('formsmod');
		$data = $this->formsmod->preoperation_pdf($id);
        
        $this->pdfgenerator->Output('pre_operation_' . $id . '.pdf','I');
        
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
    }
    
    public function spot_report_pdf($id){
        $this->load->model('formsmod');
		$data = $this->formsmod->spot_report_pdf($id);
        
        $this->pdfgenerator->Output('spot_report_' . $id . '.pdf','I');
        
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
    }
    public function get_template($type){
        $this->load->model('formsmod');
        $data = $this->formsmod->get_template($type);
		if($data){
            $response['has_data'] = true;
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['has_data'] = false;
			$response['message'] = "Failed!";
			$this->echo_response($response,201,'OK, but with errors.');
		} 
    }
    
}