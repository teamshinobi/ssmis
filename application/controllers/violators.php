<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Violators extends Ss_controller {
	
	function __construct(){
		parent::__construct();
	}	
	
	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
			//echo 'logged-in';
		}else{
			redirect(base_url());
		}	
	}
	
	public function view($id){
		if($this->is_logged_in()){
			$this->render_violator_profile($id);
			//echo 'logged-in';
		}else{
			redirect(base_url());
		}	
	}
	public function render_violator_profile($id){
		$this->load->model('violatormod');
		$violator = $this->violatormod->get($id);
		$fingerprints = $this->violatormod->fingerprints($id);
        $lFingerprints = $this->violatormod->fingerprints_left($id);
        
		$data['title'] = $violator[0]->first_name . ' ' . $violator[0]->last_name;
		$data['page_content'] = "violators_views/violator_profile.php";	
		$data['logout'] = "../logout";
		$data['violator'] = $violator[0];
		$data['fingerprints'] = $fingerprints[0];
        $data['lFingerprints'] = $lFingerprints[0];
        $data['active_nav'] = 'Violators';
		
		
		
		$views = array(
			"includes/sidebar.php",
			"loading_modal.php",
			"violators_views/edit_violator_modal.php",
			"violators_views/update_image_modal.php",
		);
		//extra stylesheets for the page. 
		$stylesheets = array(
			
			"public/included_css/jquery.dataTables.min.css",
			"public/included_css/bootstrap-timepicker.css",
            "public/included_css/jquery.mCustomScrollbar.css",
			"public/included_css/datepicker.css",
			"public/included_css/jcard.css",
            "public/included_css/bootcards-desktop.css",
			"public/system_css/violators.css",
		);
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
            "public/included_js/simplePagination.js",
            "public/included_js/jquery.mCustomScrollbar.min.js",
            "public/included_js/sscardify.js",
            "public/included_js/jcard.js",
			"public/included_js/filestyle.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/view_violator_module.js",
			"public/system_js/violators.js",
		);
		
		$data['extra_js'] = $js;
		$data['extra_view'] = $views;
		$data['extra_css'] = $stylesheets;
		
		$this->load->view('includes/template.php',$data);
	}
	
	public function render_content(){
		$data['title'] = "Violators";
		$data['page_content'] = "violators_view.php";
		$data['logout'] = "violators/logout";
         $data['active_nav'] = 'Violators';
		$views = array(
			"includes/sidebar.php",
			"loading_modal.php",
			"violators_views/advanced_search.php",
			"violators_views/reports.php",
		);
		//extra stylesheets for the page. 
		$stylesheets = array(
			
			"public/included_css/jquery.dataTables.min.css",
			"public/included_css/bootstrap-timepicker.css",
			"public/included_css/datepicker.css",
			"public/system_css/violators.css",
		);
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/violators_module.js",
			"public/system_js/violators.js",
		);
		
		$data['extra_js'] = $js;
		$data['extra_view'] = $views;
		$data['extra_css'] = $stylesheets;
		
		$this->load->view('includes/template.php',$data);
	}	
	/*===================================================================
	*							AJAX CALL
	*===================================================================*/
	
	function getall(){
		$this->load->model('violatormod');
		$data = $this->violatormod->get_all();
	
		if($data){
			$response['error'] = false;
			$response['message'] = 'Successful!';
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Failed';
			$response['db_err'] = $result;

			$this->echo_response($response,200,'NOT OK!');
		}
	}
	
	function get_by_page(){
		
	}
	
	function get($id){
		$this->load->model('violatormod');
		$violator = $this->violatormod->get($id);
		
		if($violator){
			$response['error'] = false;
			$response['message'] = 'Successful!';
			$response['violator'] = $violator[0];
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Failed';
			$response['db_err'] = $result;

			$this->echo_response($response,200,'NOT OK!');
		}
	}
	
	
	function cases($id){
		$this->load->model('violatormod');
		$data = $this->violatormod->get_cases($id);
		
		if($data){
			$response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Successful!';
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
        } else {
            
			$response['error'] = false;
            $response['has_data'] = false;
			$response['message'] = 'The violator seems no association with any cases.';
			$response['db_err'] = $data;

			$this->echo_response($response,200,'NOT OK!');
		}
	}
	
	function upload(){
		$data = $this->input->post(NULL,TRUE);
		$this->load->model('violatormod');
		$result = $this->violatormod->update_photo($data);	
		
		if($result){
			$response['message'] = "Successfully updated.";
			$response['success'] = true;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['message'] = "Photo not updated for some reasons.";
			$response['success'] = false;
			$this->echo_response($response,200,'NOT OK!');
		}	
	}	
	function update(){
		$data = $this->input->post(NULL,TRUE);
		$this->load->model('violatormod');
		$codeNum = $this->violatormod->update($data);
		
		if($codeNum){
			$response['message'] = "Successfully updated.";
			$response['success'] = true;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['message'] = "Photo not updated for some reasons.";
			$response['success'] = false;
			$this->echo_response($response,200,'NOT OK!');
		}	
	}
	
	function datesearch($start, $end){
		$this->load->model('violatormod');
		$data = $this->violatormod->search_date($start, $end);
		
		if($data){
			$response['success'] = true;
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['success'] = false;
			$response['message'] = 'No violators retrieved in the specified date range.';
			$this->echo_response($response,200,'Not OK!');
		}
	}
	
	function casessearch($range){
		$this->load->model('violatormod');
		$data = $this->violatormod->search_cases($range);
		
		if($data){
			$response['success'] = true;
			$response['data'] = $data;
			$response['size'] = count($data);
			$this->echo_response($response,200,'OK!');
		}else{
			$response['success'] = false;
			$response['message'] = 'No violators retrieved in the specified cases range.';
			$this->echo_response($response,200,'Not OK!');
		}
	}
	function test(){
		$this->load->model('violatormod');
		$this->violatormod->test();
	}
    function search($search_filter,$search){
        $this->load->model;('violatormod');
        $this->violatormod->search($search_filter, $search);
        
        
    }
        
}