<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Narcotics extends Ss_controller {
	
	function __construct(){
		//So in case we have to do something in the constructor we dont override the parent constructor.
		parent::__construct();
		//my constructor code here
	}
		
	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
		}else{
			echo 'Unidentify access!';
		}	
	}
	
	private function render_content(){
		$data['title'] = "Narcotics";
		$data['page_content'] = "narcotics_view.php";
		$data['active_nav'] = "Miscellaneous";
		$data['logout'] = "/./ssmis/logout";
		//you can add as many as you want
		$views = array(

			"includes/sidebar.php",
			"narcotics_view/add_modal.php",
			"narcotics_view/view_modal.php",
		
		);
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
				
			//"public/system_css/default.css",
			"public/system_css/narcotics.css",
			//"public/system_css/sidebar.css",
			"public/included_css/jquery.dataTables.min.css",
			"public/included_css/jquery.contextMenu.css",
		);
		$data['extra_css'] = $stylesheets;
		
		$js = array(
			"public/system_js/modules/main.js",
			//"public/system_js/sidebar.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/jquery.contextMenu.js",
			"public/included_js/bootbox.min.js",
			"public/system_js/modules/narcotics_module.js",
			"public/system_js/narcotics.js",

			
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	


	public function retrieve(){
		$this->load->model('narcoticsmod');
		$data = $this->narcoticsmod->get_all_narcotics();
		
		if($data){
			$response['data'] = $data;
			
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
	/*public function add(){
        $data = $this->input->post(NULL,TRUE);
        $this->load->model('narcoticsmod');
        $result = $this->narcoticsmod->insert($data);
        
        if($result->success === true){
            $response['success'] = true;
            $response['message'] = 'Successfully saved!';
			$response['id'] = $result->next_id;
			$this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
			$response['db_err'] = $result->db_err;
			$this->echo_response($response,201,'Not OK!');
        }
        //$this->vehiclesmod->upload_photo($data);
    }*/
    public function update(){
    	$data = $this->input->post(NULL,TRUE);
    	$this->load->model('narcoticsmod');

    	$result = $this->narcoticsmod->update($data);

    	if($result === true){
			$response['error'] = false;
			$response['message'] = 'This narcotic was updated!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'This This narcotic was not successfully updated!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'NOT OK!');
		}

    }
	public function add(){
		$this->load->model('narcoticsmod');
		$id = $this->narcoticsmod->narcotics_id();
		$data = array(
				'narcotics_id' => $id,
				'name' => $this->input->post('name'),
				'description' => $this->input->post('description'),
			
		);
		$this->load->model('narcoticsmod');
		$result = $this->narcoticsmod->insert_narcotics($data);
		//if the insertion is successful, $result will be true else will be a message error
		if($result === true){
			$response['error'] = false;
			$response['message'] = 'Narcotic/s successfully saved!';
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Narcotic/s not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	
	public function view($id){

		$this->load->model('narcoticsmod');
		$result = $this->narcoticsmod->get_narcotics($id);


		
		if($result){
			$response['error'] = false;
			$response['message'] = 'Narcotic/s successfully saved!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Narcotic/s not saved!';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	
	  public function id(){
        $this->load->model('narcoticsmod');
   
        $id = $this->narcoticsmod->narcotics_id();
       
        if($id){
			$response['error'] = false;
			$response['id'] = $id;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Cannot get ID';
			$response['db_err'] = $id;
		
			$this->echo_response($response,200,'OK!');
		}
	}
	
	public function remove_narcotics($id){
		$this->load->model('narcoticsmod');
		$result = $this->narcoticsmod->remove_narcotic($id);

		if($result === TRUE){
			$response['error'] = false;
			$response['message'] = 'Narcotic successfully deleted!';
			$response['data'] = $result;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = 'Narcotic not deleted';
			$response['db_err'] = $result;
		
			$this->echo_response($response,200,'OK!');
		}

	}
	
	public function report(){
		$this->load->model('narcoticsmod');
		$this->narcoticsmod->report_all();
	}
}
	