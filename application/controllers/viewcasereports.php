<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Viewcasereports extends Ss_controller {
		
	public function index(){
		if($this->is_logged_in()){
			$this->render_content();
		}else{
			echo 'Unidentify access!';
		}	
	}
	
	public function render_content(){
		$data['title'] = "Case Reports";
		$data['page_content'] = "casereports_view.php";
        $data['active_nav'] = 'Case Report';
        $data['logout'] = '/./ssmis/home/logout';
		$views = array(
			"includes/sidebar.php",
            "loading_modal.php",
		);
		$data['extra_view'] = $views;
		
		$stylesheets = array(
          
			"public/system_css/casereport.css",
            "public/included_css/jquery.mCustomScrollbar.css",
            "public/included_css/bootcards-desktop.css",
			"public/included_css/jcard.css",
		);
		$data['extra_css'] = $stylesheets;
		
		$js = array(
            "public/included_js/bootbox.min.js",
            "public/included_js/simplePagination.js",
            "public/included_js/jquery.mCustomScrollbar.min.js",
            "public/included_js/ssCardify.js",
			"public/system_js/modules/main.js",
			"public/system_js/modules/view_casereports_module.js",
			"public/system_js/viewcasereports.js",
			"public/included_js/jcard.js",
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
	}
	
	public function view($id){
		$this->db->cache_delete_all();
		if($this->is_logged_in()){
			$this->render_view_casereport_content($id);
		}else{
			echo 'Unidentify access!';
		}	
	}
	
	public function render_view_casereport_content($id){
      
        if($this->get_access_non_json($id) || $this->is_case_closed($id)){
            $data['title'] = "Case Reports";
            $data['page_content'] = "casereport_views/casereportdetails_view.php";
            $data['active_nav'] = 'Case Report';
            $data['logout'] = '/./ssmis/home/logout';

            $this->load->model('viewcasereportmod');
            $caseReport = $this->viewcasereportmod->get_casereport($id);
            $data['caseReport'] = $caseReport;

            $views = array(
                "includes/sidebar.php",	
                "casereport_views/distribute_modal.php",
                "casereport_views/view_distribution.php",
                "casereport_views/edit_status_evidences_modal.php",
                "casereport_views/edit_cc_judge_modal.php",
                "loading_modal.php",
            );
            $data['extra_view'] = $views;

            $stylesheets = array(
                
                "public/included_css/magnific-popup.css",
                "public/included_css/datepicker.css",
                "public/system_css/casereport.css",

            );
            $data['extra_css'] = $stylesheets;

            $js = array(
                "public/included_js/jquery.magnific-popup.js",
                "public/included_js/bootstrap-datepicker.js",
                "public/included_js/bootbox.min.js",
                "public/system_js/modules/main.js",
                "public/system_js/modules/view_casereport_details_module.js",
                "public/system_js/viewcasedetails.js",

            );
            $data['extra_js'] = $js;

            $this->load->view('includes/template.php',$data);
        } else {
            //echo $this->is_case_closed();
            redirect(base_url());
        }
        
        
		
	}
	
	public function details($id){
		$this->load->model('viewcasereportmod');
		$caseDetails = $this->viewcasereportmod->get_casereport_details($id);
		$data['caseReportDetails'] = $caseDetails;
		
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
	
	public function retrieve(){
		$this->load->model('viewcasereportmod');
		$data = $this->viewcasereportmod->get_case_reports();
		
        if($data){
            $increment = 0;
            foreach($data as $object){
                $case = $object;
                $arrestDate = $this->viewcasereportmod->get_arrest_details_date($data[$increment]->blotter_entry_nr);
                $case->arrestDate = $arrestDate[0]->date_of_arrest;
                $increment++;
            }
        }
		
      
		if($data){
            $response['success'] = true;
            $response['has_data'] = true;
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['success'] = false;
            $response['has_data'] = false;
			$response['message'] = "No cases found!";
			$this->echo_response($response,201,'No returned data.');
		}
	}
	
	public function report($id){
		$this->load->model('viewcasereportmod');
		$data = $this->viewcasereportmod->full_case_report($id);
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
    
    //checks if the case report is complete before printing
    public function is_case_complete($id){
        $this->load->model('viewcasereportmod');
        $mix = $this->viewcasereportmod->check_case_completeness($id);
        
        if($mix->has_empty){
            $response['printable'] = false;
            $response['emps'] = $mix->emps;
            
            $this->echo_response($response,200,'OK!');        
        } else {
            $response['printable'] = true;
            
            $this->echo_response($response,200,'OK!');          
        }
        
    }
	
    //give access to user if he she is the investigator or booker of chief of the particular case
    public function get_access_non_json($case_id){
        $user = $this->get_user();
        $this->load->model('viewcasereportmod');
        $access = $this->viewcasereportmod->get_case_access($user,$case_id);
        
        return $access;
        
    }
    
    //give access to user if he she is the investigator or booker of chief of the particular case
    public function get_access($case_id){
        $user = $this->get_user();
        $this->load->model('viewcasereportmod');
        $access = $this->viewcasereportmod->get_case_access($user,$case_id);
        
        if($access){
            $response['proceed'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['proceed'] = false;
            $this->echo_response($response,201,'OK!');
        }
    }
    
    public function is_case_closed($case_id){
       
        $this->load->model('viewcasereportmod');
        $progress = $this->viewcasereportmod->is_case_closed($case_id);
        
        if($progress == 'Closed')
            return true;
        else 
            return false;
    }
    
    //update status of evidences
    public function update_status(){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $this->load->model('viewcasereportmod');
       
        $res = $this->viewcasereportmod->update_evidence($data['case_id'], $data['status']);
        
        if($res){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
        
    }
    
    //update cc and judge.
    public function update_cc_judge(){
        $data = $this->input->post(NULL, TRUE); // returns all POS items with XSS filter
        $this->load->model('viewcasereportmod');
       
        $res = $this->viewcasereportmod->update_cc_judge($data['case_id'], $data['cc'],
                                                        $data['judge']);
        
        if($res){
            $response['success'] = true;
            $this->echo_response($response,200,'OK!');
        } else {
            $response['success'] = false;
            $this->echo_response($response,201,'Not OK!');
        }
        
    }
    
	//---------------------tester only------------------------
	public function test($id){
		$this->load->model('viewcasereportmod');
		$data = $this->viewcasereportmod->get_property_seized($id);
		
		if($data){
			$response['data'] = $data;
			$this->echo_response($response,200,'OK!');
		}else{
			$response['error'] = true;
			$response['message'] = "Failed!";
		
			$this->echo_response($response,201,'OK, but with errors.');
		}
	}
}