<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Ss_controller {


    function index(){
       $data['title'] = "Reports";
		$data['page_content'] = "reports_view.php";
		$data['logout'] = "/./ssmis/home/logout";
        $data['active_nav'] = 'Reports';
		//you can add as many as you want
		$views = array(
			"includes/sidebar.php",
			"includes/prompt_modal.php",
            "reports_views/month_year_modal.php",
            "reports_views/year_modal.php",
            "reports_views/monthly_acc_report.php",
            "reports_views/case_id_modal.php",
            "reports_views/begin_end_month.php",
			"loading_modal.php",
		);
		
		
		$data['extra_view'] = $views;
		
		//extra stylesheets for the page. 
		$stylesheets = array(
		
			"public/included_css/bootstrap-timepicker.css",
			"public/included_css/datepicker.css",
			"public/included_css/jquery.dataTables.min.css",
            "public/system_css/themeforest.css",
            "public/system_css/home.css",
		);
		$data['extra_css'] = $stylesheets;
		
		//extra js
		$js = array(
			"public/system_js/sidebar.js",
			"public/included_js/bootstrap-datepicker.js",
			"public/included_js/bootstrap-timepicker.js",
			"public/included_js/filestyle.js",
			"public/included_js/jquery.dataTables.min.js",
			"public/included_js/bootbox.min.js",
            "public/system_js/modules/main.js",
            "public/system_js/modules/reports_module.js",
            "public/system_js/reports.js",
        
		);
		$data['extra_js'] = $js;
		
		$this->load->view('includes/template.php',$data);
    }
    
    
    
    public function yearly_acc_report($year){
        $this->load->model('reportmod');
        $results = $this->reportmod->yearly_acc_report($year);
        
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
			$this->echo_response($response,201,'NOT OK!');
        }
    }
    public function montly_acc_report($month, $year){        
        
        $this->load->model('reportmod');
        $results = $this->reportmod->month_acc_report($month,$year);
        
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
            $response['sEcho'] = 1;
			$response['iTotalRecords'] = '0';
			$response['iTotalDisplayRecords'] = '0';
		    $response['aaData'] = [];
			$this->echo_response($response,201,'NOT OK!');
        }
    }
    
    public function monthly_acc_pdf($month, $year){        
        
        $this->load->model('reportmod');
        $results = $this->reportmod->monthly_accomplishment_pdf($month,$year);
      
       
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
            echo 'No cases found in the particular date.';
        }
    }
    
    public function yearly_acc_pdf($year){        
        
        $this->load->model('reportmod');
        $results = $this->reportmod->yearly_accomplishment_pdf($year);
        
        if($results){
            $response['error'] = false;
            $response['has_data'] = true;
			$response['message'] = 'Success retrieving report!';
			$response['data'] = $results;
			
			$this->echo_response($response,200,'OK!');
        } else {
           echo 'No cases found in the particular date.';
        }
    }
    
     public function statistics_pdf($bMonth, $eMonth, $year){        
        
        $this->load->model('reportmod');
        $this->reportmod->statistic_pdf($bMonth, $eMonth, $year);
    
    }

}

	