<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

    //I-repair na lang to pag may time pa nyeta di ko magets code ko.
	class Filemanager {

		private $_CI;
		
		/**
		*
		* Load image_lib and upload libraries in constructor.
		*
		**/
		public function __construct(){
			$this->_CI =& get_instance();
			$this->_CI->load->library('image_lib');
			$this->_CI->load->library('upload');
		}
        
        
		//upload 1 image/file only at a destination folder
		//there is no moving of image to temp here. Just a normal upload
		public function img_normal_upload($id,$folder,$operation = 'add'){
		
            $to_return = true;
            
			$path = realpath(APPPATH . '../public/resources/photos/' . $folder );
			//var_dump($path);
		
			//checks if there is an image to be uploaded. If none, use default image.	
			if(!empty($_FILES) && $_FILES["userfile"]["name"] != "" && isset($_FILES["userfile"]["name"])){
			
				// Note we dont have to need $path be cause we do it already on $config
				$config = array (
					'image_library' => 'gd',
					'upload_path' =>  $_SERVER['DOCUMENT_ROOT'] . '\\ssmis\\public\\resources\\photos\\' . $folder . ' \\',
					'file_name' =>  $id . '.jpg',
					'allowed_types' => 'png|jpg|jpeg',
					'overwrite' => TRUE,
					'max_size' => '5000',	
				);
				$this->_CI->upload->initialize($config);
				//kapag failed
				if(!$this->_CI->upload->do_upload()){
					$to_return = false;
				}else{
					//now, the $path will become our resource path for copying and creating thumbnails.
                    $src_path = $path . '/' . $id . '.jpg';
                    $thumb_path = $path . '/' . $id . '_thumb.jpg';
                    $this->make_thumb($src_path, $thumb_path , 100);					

					$to_return = true;
				}
			} else {	
                // NOTE, putang ina di ko to maintindihan.  
                // Ang value ng APPPATH is application/ , this is not the root folder.
                // Pwede ko siguro palitan ng $_SERVER['DOCUMENT_ROOT'] or dirname with str_replace
                $resource_path = realpath(APPPATH . '../public/resources/default_picture.jpg');
                //we have to append '/' kasi hindi niya nilalagay sa realpath ito
                $new_path = $path . '/' . $id . '.jpg';
            
                $thumb_path = $path . '/' . $id . '_thumb.jpg';
                
                $suc = copy($resource_path, $new_path);
                
                if($suc){
                    $to_return = true;
                    $this->make_thumb($new_path, $thumb_path , 100);					
                   
                }
									
			}
            
            $this->_CI->image_lib->clear();
            return $to_return;
			
		}
		
        public function rename_pnp_images($prev_name, $new_name){
            /*$directory = realpath(APPPATH . '../public/resources/photos/pnp/');
            echo $directory;
            $prevName = $directory . $prev_name . ".jpg";
            $prevNameThumb = $directory . $prev_name . "_thumb.jpg";
            if ($handle = opendir($directory) {
                while (false !== ($fileName = readdir($handle))) {
                    if($prevName == $fileName){
                        $newName = $directory . $new_name . ".jpg";
                        $newNameThumb = $directory . $new_name . "_thumb.jpg";
                        rename($prevName, $newName);
                        rename($prevNameThumb, $newNameThumb);
                    }
                  
                }
                closedir($handle);
            }*/
            $directory = realpath(APPPATH . '../public/resources/photos/pnp/');
            if ($handle = opendir($directory))
            {
                
                /*Iterate the files*/
                while (false !== ($file = readdir($handle)))
                {
                      if ($file != "." && $file != "..")
                      {
                           if(!strstr($file,$prev_name))
                           {
                               continue; //Skip as it does not contain $prev_name
                           }
                        
                           $newf = str_replace($prev_name,$new_name,$file);

                           /*Rename the old file accordingly*/
                           rename($directory .'/' . $file, $directory . '/' . $newf);
                      }
                }

                /*Close the handle*/
                closedir($handle);
            }
        }
		/*==========================================================================================
		*
		*	Uploads a photo to the temp folder. This method is mostly used by adding violator.
		*	but no one can stop you from using this to other modules.
		*
		*	$folder = the name of the folder like "violators"
		*	$tags = array of tags. This can be empty. Note that tags must be the same as the file name in the forms.
		*
		*	This method makes a directory per object. For example. If the violator is the
		*	one to be saved, then this method create a folder using the violator_id as the name
		*
		*====================================================================================*/
		public function upload_to_temp($id, $folder, $tags, $behavior){
			
			$to_return = false;
			
			$path = realpath(APPPATH . '../public/resources/temps/' . $folder);
			
			if($behavior['make_dir'] === true){
				//makes a directory if file/directory not existing.
				if(!file_exists($path . '/' . $behavior['sub_folder'])){
					mkdir($path . '/' . $behavior['sub_folder'], 0700);		
				}
				//navigate to the currently created path
				$path = $path . '/' . $behavior['sub_folder'];
			}
			
			if(isset($tags)){
				//use these tags to check the files present on submission
				foreach($tags as $tag){
					if(array_key_exists($tag,$_FILES)){
						if(!empty($_FILES) && $_FILES[$tag]["name"] != "" && isset($_FILES[$tag]["name"])){
							$config = array (
								'image_library' => 'gd',
								'upload_path' => $path ,
								'file_name' =>  $id . '_' . $tag . '.jpg',
								'allowed_types' => 'png|jpg|jpeg',
								'overwrite' => TRUE,
								'max_size' => '5000',	
							);
							$this->_CI->upload->initialize($config);
							if(!$this->_CI->upload->do_upload($tag)){
								$to_return = false;
								echo $this->_CI->upload->display_errors();
							}else{	
								$thumb_folder_path = $path . '/thumbs' ;	
								if(!file_exists($thumb_folder_path)){
									mkdir($thumb_folder_path, 0700);	
								}							
								$this->make_thumb($path . '/' . $id . '_' . $tag . '.jpg', $thumb_folder_path . '/' . $id . '_' . $tag . '.jpg', 100);
								$to_return = true;
							}
							
						}else{
							//Hindi na dapat marating to!
							$to_return = false;
						}
					}else{
						//$use_default would be false when updating selected violator.
						if($behavior['use_default'] === true){
							//copy the default picture
							$default_pic = 'default_picture.jpg';
							if(isset($behavior['default_pic'])){
								$default_pic = $behavior['default_pic'];
							}
							$default_picture = realpath(APPPATH . '../public/resources/' . $default_pic);
							$to_return = copy($default_picture, $path . '/' . $id . '_' . $tag . '.jpg');
							if($to_return){
								$thumb_folder_path = $path . '/thumbs' ;
								if(!file_exists($thumb_folder_path)){
									mkdir($thumb_folder_path, 0700);	
								}
								$this->make_thumb($path . '/' . $id . '_' . $tag . '.jpg', $thumb_folder_path . '/' . $id . '_' . $tag . '.jpg', 100);					
							}
									
							//echo 'Using default with path of ' . $path . '/' . $id . '_' . $tag . '.jpg' ;
						} 
						
					}					
				}
			}

			return $to_return;
		}
		
		
		/**
		* 	Move temp folders to their permanent places
	    *
		*	$module_folder = example (violator, pnp, etc)		
		*   $folders = name of folders within module_folder
		**/
		public function move_temp_to_permanent($module_folder, $folders, $behavior){
			$bool = false;
			
			$module_folder_path = realpath(APPPATH . '../public/resources/temps/' . $module_folder);
	
			$module_folder_destination_path = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/photos/' . $module_folder . '/';
			
			foreach($folders as $folder){
				if(file_exists($module_folder_destination_path . $folder)){
					$this->rrmdir($module_folder_destination_path . $folder);
					$bool = rename($module_folder_path . '/' . $folder, $module_folder_destination_path . $folder);
				}else{
					$bool = rename($module_folder_path . '/' . $folder, $module_folder_destination_path . $folder);		
				}
			}
			return $bool;
		}
        
		function make_thumb($src, $dest, $desired_width) {
			/* read the source image */
			$source_image = imagecreatefromjpeg($src);
	
			$width = imagesx($source_image);
			$height = imagesy($source_image);
			
			/* find the "desired height" of this thumbnail, relative to the desired width  */
			$desired_height = floor($height * ($desired_width / $width));
			
			/* create a new, "virtual" image */
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			
			/* copy source image at a resized size */
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			
			/* create the physical thumbnail image to its destination */
			imagejpeg($virtual_image, $dest);
		}
        
		//recursive method
		//be careful in using this it might delete your project.
		function rrmdir($dir) { 
		  foreach(glob($dir . '/*') as $file) { 
			if(is_dir($file)) 
				$this->rrmdir($file); 
			else 
				unlink($file); 
		  } 
		  rmdir($dir); 
		}
        
        function rmdir_subdirectories($dir) { 
           if (is_dir($dir)) { 
             $objects = scandir($dir); 
             foreach ($objects as $object) { 
               if ($object != "." && $object != "..") { 
                 if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object); 
               } 
             } 
             reset($objects); 
               
            $pieces = explode("\\", $dir);
            $segment = end($pieces);
           
               if($segment != 'violator' && $segment != 'inventory' && $segment != 'case' && $segment != 'laboratory' && $segment != 'medical' 
                 && $segment != 'search' && $segment != 'warrant' && $segment != 'others'){
                    rmdir($dir);
               }   
             
           } 
         }
		
		public function copy_permanent_to_temp($module_folder, $folder_name){
			$source_path = realpath(APPPATH . '../public/resources/photos/' . $module_folder . '/' . $folder_name);
			$destination_path = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/' . $module_folder . '/' . $folder_name;
			//echo $source_path . ' ' . $destination_path;
			$bool = $this->xcopy($source_path, $destination_path, 0755);
            
            return $bool;
		}
    
		
		/**
		 * Copy a file, or recursively copy a folder and its contents
		 * @param       string   $source    Source path
		 * @param       string   $dest      Destination path
		 * @param       string   $permissions New folder creation permissions
		 * @return      bool     Returns true on success, false on failure
		 */

		function xcopy($source, $dest, $permissions = 0755)
		{
			// Check for symlinks
			if (is_link($source)) {
				return symlink(readlink($source), $dest);
			}

			// Simple copy for a file
			if (is_file($source)) {
				return copy($source, $dest);
			}

			// Make destination directory
			if (!is_dir($dest)) {
				mkdir($dest, $permissions);
			}

			// Loop through the folder
			$dir = dir($source);
			while (false !== $entry = $dir->read()) {
				// Skip pointers
				if ($entry == '.' || $entry == '..') {
					continue;
				}
				// Deep copy directories
				$this->xcopy("$source/$entry", "$dest/$entry", $permissions);
			}

			// Clean up
			$dir->close();
			return true;
		}
		
		//overwrite only 1 image. Used in updating photos one at a time. Tingin ko pwede din to sa uploading eh.
		public function overwrite_image($config){
			$to_return = true;
			
			//indicates whether to use subfolder within module folders;
			if($config['multilevel']){
				$path = realpath(APPPATH . '../public/resources/photos/' . $config['module'] . '/' . $config['id']);
			
			} else {
				$path = realpath(APPPATH . '../public/resources/photos/' . $config['module']);
			}
			
			
			if(!empty($_FILES) && $_FILES["file"]["name"] != "" && isset($_FILES["file"]["name"])){
				if(isset($config['tag'])){
					$file_path = $config['id'] . '_' . $config['tag'] . '.jpg';
					$thumb_source_path = $path . '/' . $config['id'] . '_' . $config['tag'] . '.jpg';
					$thumb_dest_path = $path . '/thumbs' . '/' . $config['id'] . '_' . $config['tag'] . '.jpg';
	
				} else {
					$file_path = $config['id'] . '.jpg';
					$thumb_source_path = $path . '/' . $config['id'] . '.jpg';
					$thumb_dest_path = $path .  '/' . $config['id'] . '_thumb.jpg';
				}
				$config_img = array (
					'image_library' => 'gd',
					'upload_path' => $path ,
					'file_name' =>  $file_path,
					'allowed_types' => 'png|jpg|jpeg',
					'overwrite' => TRUE,
					'max_size' => '5000',	
				);
				$this->_CI->upload->initialize($config_img);
				if(!$this->_CI->upload->do_upload("file")){
					$to_return = false;
					//$to_return = $this->_CI->upload->display_errors();
				}else{
                    list($width, $height, $type, $attr) = getimagesize($thumb_source_path);
					$this->make_thumb($thumb_source_path,$thumb_dest_path, 100);					
				}
			}else{
				$to_return = false;
			}
			return $to_return;
		}
        
        
        //move default picture to case temps
        public function create_default_scene($id){
            $src_file = realpath(APPPATH . '../public/resources/photos/default_img.jpg');
            
            $dir =  $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/'  . $id;
            
            //check if folder exists
            if(!file_exists($dir)){
                mkdir( $dir );
            } 
            
            $dest = $dir . '/' . $id . '.jpg'; 
            
            echo $src_file . "\n" . $dest;
            $bool = $this->xcopy($src_file, $dest, 0755);
            
            //make thumb
            if($bool){
                $thumb_src = $dest;
                $dest =  $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/'  . $id . '/'. $id .'_thumb.jpg';
                
                $this->make_thumb($thumb_src, $dest, 500);
            
            }

            return $bool;
        }
        
        public function upload_scene ($id) {  
            $path =  $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/' . $id . '/';
            
            if(!empty($_FILES) && $_FILES["case"]["name"] != "" && isset($_FILES["case"]["name"])){
                $config = array (
                    'image_library' => 'gd',
                    'upload_path' => $path ,
                    'file_name' =>  $id . '.jpg',
                    'allowed_types' => 'jpg|jpeg',
                    'overwrite' => TRUE,
                    'max_size' => '5000',	
                );
                $this->_CI->upload->initialize($config);
                if(!$this->_CI->upload->do_upload("case")){
                    $to_return = false;
                    echo $this->_CI->upload->display_errors();
                }else{	
                    list($width, $height, $type, $attr) = getimagesize($path . '/' . $id. '.jpg');
                    
                    $this->make_thumb($path . '/' . $id. '.jpg', $path . '/' . $id. '_thumb.jpg', ($width / 2));
                    //$to_return = true;
                }

            }
        }
        
        //error prone potaragis!
        public function move_scene_permanent($id){
            $src_file = realpath(APPPATH . '../public/resources/temps/case/' . $id);
            $dest_file = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/photos/case/' . $id;
            
            $bool = false;
            
            if(file_exists($dest_file)){
                echo 'Here';
                $this->rrmdir($dest_file);
                $bool = rename($src_file, $dest_file);
            } else {
                $bool = rename($src_file, $dest_file);
            }
            
            return $bool;
        }
        
        public function create_document_folders($id){
            $lab_exam = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/' . $id . '/laboratory';
            $medical_exam = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/' . $id . '/medical';
            $search = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/' . $id . '/search';
            $warrant = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/' . $id . '/warrant';
            $others = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/case/' . $id . '/others';
            
            if(!file_exists($lab_exam)){
                mkdir($lab_exam);
            }
            
            if(!file_exists($medical_exam)){
                mkdir($medical_exam);
            }
            
             if(!file_exists($search)){
                mkdir($search);
            }
            
            if(!file_exists($warrant)){
                mkdir($warrant);
            }
            
            if(!file_exists($others)){
                mkdir($others);
            }
        }
        
        public function remove_docs_in_temp($id,$folder){
            $path =  $_SERVER['DOCUMENT_ROOT'] . '\\ssmis\\public\\resources\\temps\\case\\' . $id . '\\'. $folder;
            $this->rmdir_subdirectories($path);
        }
        
        //documents
        public function upload_documents($id, $folder, $tag){

            $to_return = true;
            
            $path =  $_SERVER['DOCUMENT_ROOT'] . '\\ssmis\\public\\resources\\temps\\case\\' . $id . '\\'. $tag;
           
            if(!empty($_FILES) && isset($_FILES)){
                $this->rmdir_subdirectories($path);
                
                $incr = 1;
                
                foreach($_FILES as $file){
                    $name = $tag . '_' . $incr;

                    $config = array (
                        'image_library' => 'gd',
                        'upload_path' => $path ,
                        'file_name' =>  $tag . '_' . $incr . '.jpg',
                        'allowed_types' => 'png|jpg|jpeg',
                        'overwrite' => TRUE,
                        'max_size' => '5000',	
                    );
                    $this->_CI->upload->initialize($config);
                    if(!$this->_CI->upload->do_upload($name)){
                        $to_return = false;
                        echo $this->_CI->upload->display_errors();
                    }else{	
                        $src = $path . '\\' . $name .'.jpg';
                        $dest= $path . '\\' . $name .'_thumb.jpg';
                        list($width, $height, $type, $attr) = getimagesize($src);
                        $ret = $this->make_thumb($src,$dest, ($width / 2));

                    }
                    $incr++;
                }
                $incr = 0;
            }
           
            return $to_return;
            
        }
      public function flexible_image_upload($config){

			$to_return = true;
			
			//indicates whether to use subfolder within module folders;
			if($config['multilevel']){
				$path = realpath(APPPATH . '../public/resources/photos/' . $config['module'] . '/' . $config['id']);
			
			} else {
				$path = realpath(APPPATH . '../public/resources/photos/' . $config['module']);
			}
			
			
			if(!empty($_FILES) && $_FILES["file"]["name"] != "" && isset($_FILES["file"]["name"])){
				if(isset($config['tag'])){
					$file_path = $config['id'] . '_' . $config['tag'] . '.jpg';
					$thumb_source_path = $path . '/' . $config['id'] . '_' . $config['tag'] . '.jpg';
					$thumb_dest_path = $path . '/thumbs' . '/' . $config['id'] . '_' . $config['tag'] . '.jpg';
	
				} else {
					$file_path = $config['id'] . '.jpg';
					$thumb_source_path = $path . '/' . $config['id'] . '.jpg';
					$thumb_dest_path = $path .  '/' . $config['id'] . '_thumb.jpg';
				}
				$config_img = array (
					'image_library' => 'gd',
					'upload_path' => $path ,
					'file_name' =>  $file_path,
					'allowed_types' => 'png|jpg|jpeg',
					'overwrite' => TRUE,
					'max_size' => '2000',	
				);
				$this->_CI->upload->initialize($config_img);
				if(!$this->_CI->upload->do_upload("file")){
					$to_return = false;
					//$to_return = $this->_CI->upload->display_errors();
				}else{
					$this->make_thumb($thumb_source_path,$thumb_dest_path, 100);					
				}
			}else{
				$to_return = true;
				$resource_path = realpath(APPPATH . '../public/resources/default_img.jpg');
                //we have to append '/' kasi hindi niya nilalagay sa realpath ito
                $new_path = $path . '/' . $config['id'] . '.jpg';
            	
            	//echo $new_path;
                $thumb_path = $path . '/' . $config['id'] . '_thumb.jpg';
                
                $suc = copy($resource_path, $new_path);
                
                if($suc){
                    $to_return = true;
                    $this->make_thumb($new_path, $thumb_path , 100);					
                   
                }
			}
			return $to_return;
		
		}
	}
		