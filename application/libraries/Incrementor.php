<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

	class Incrementor{
		
		/* A string variable that will define
			how incrementing will work.
			
			VALUES:
			
			1. NORMAL_INCREMENT
				
				Exp. will increment id by 1
				based on the given parameter.
			
			2. DB_INCREMENT
				
				Exp. will increment id by 1
				based on the last row in db.
			
			3. HASH__NORMAL_INCREMENT
			
				Exp. will increment id by 1
				based on the given parameter
				then use hashing algos.
		 
		 
		 */
		private $strategy;
		//prefix of the id such as SJ-VIOL
		private $prefix;
		//if we want to include year
		private $includes_year;
		
		function __construct($config){
			if(isset($config['strategy'])){
				$this->strategy = $config['strategy'];
			}
			if(isset($config['prefix'])){
				$this->prefix = $config['prefix'];
			}
			if(isset($config['includes_year'])){
				$this->includes_year = $config['includes_year'];
			}
		}
		
		function increment($arg = null){
			if($this->strategy === 'DB_INCREMENT'){
				return $this->do_db_increment($arg);
			}else if($this->strategy === 'NORMAL_INCREMENT'){
				return $this->do_normal_increment($arg);
			}
		}
		
		//$arg = id of the object
		private function do_db_increment($arg){
			if($arg != false){
				return $this->perform_incrementing_algo($arg);
			} else {	
				return $this->generate_starter_id();
			}
		}
		
		private function do_normal_increment($arg){
			return $this->perform_incrementing_algo($arg);
		}
		
		private function perform_incrementing_algo($id){

			$pieces = explode("-", $id);
		
			$to_be_incremented = end($pieces);
            
			// give 00 leading characters
            $next_id = $this->prefix . '-' . (isset($this->includes_year) ? date("Y") . '-': '')
				. str_pad($to_be_incremented + 1, 4, '0', STR_PAD_LEFT);
		
			
			return $next_id;
		}
		
		//generate started id based on configuration.
		function generate_starter_id(){
			return $this->prefix . '-'
				. (isset($this->includes_year) ? date("Y"): '')
				. '-0001';
		}
	}	
	