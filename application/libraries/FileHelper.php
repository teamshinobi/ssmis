<?php 
    
/*============================================================
*
*   A new implementation of FileManager class that will be both
*   compatible on dev and prod environment. 
*
*   Author: JC Frane
*   Created at: December 5, 2015
*
*   Currently supports only images mime types.
*
*=============================================================*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class FileHelper {
    
    private $_CI;
    
    private $fileMaxSize = '5000';
    
    private $allowedTypes = 'pnp|jpg|jpeg';
    
    private $rootDirectory;
    
    private $tempRootDirectory;
    
    private $defaultImageFilePath ;
    
    //as of now this would be TRUE for all instances.
    private $createThumb = true;
    
    //tells whether the upload process should create thumb folder
    private $createThumbFolder = false;
    
    private $thumbFolderDirectory;
    
    private $configuration;
    
    private $errors = array();
    
    private $uploadToTemp = false;
    
    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->_CI->load->library('image_lib');
        $this->_CI->load->library('upload'); 
       
        $this->rootDirectory = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/photos/';
        $this->tempRootDirectory = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/temps/';
        $this->defaultImagePath = realpath(APPPATH . '../public/resources/default_picture.jpg');
        
        //upload_path and file_name configuration must be set depending on the given path.
        $this->configuration = array (
            'image_library' => 'gd',
            'allowed_types' => $this->allowedTypes,
            'overwrite' => 'TRUE',
            'max_size' => $this->fileMaxSize,
        );    
    }
    
    
    /*=================================================================
    *
    *   Uploads single file.   
    *
    *   @param $id id of the object that will be used as the file name.
    *   @param $folder the folder name where the file should be placed.
    *   @param $uploadName the name used in the file input of HTML.
    *
    *   @return array of errors if with error and TRUE if success.
    *
    *================================================================*/
    public function uploadFile($id, $folder, $uploadName = 'file')
    {
        $path = $this->rootDirectory . $folder . '/';
        $fileName =  $id . '.jpg';
        $thumbFileName =  $id . '_thumb.jpg';
        
        $this->configuration['upload_path'] = $path;
        $this->configuration['file_name'] =  $fileName;
        
        $this->_CI->upload->initialize($this->configuration);
        if(!empty($_FILES) && $_FILES[$uploadName]['name'] != "" && isset($_FILES[$uploadName]['name'])){
            
            //call CI Image Library upload function
            if(!$this->_CI->upload->do_upload($uploadName)){
                
                $this->errors['ci_upload_error'] = 'Cannot upload image.';
                return false;
                
            } else {
                //now, the $path will become our resource path for copying and creating thumbnails.
                $src_path = $path . $fileName;
                $thumb_path = $path . $thumbFileName;
                $this->createThumbnail($src_path, $thumb_path , 100);					
            }
            
        } else {
            $destinationPath = $path . $fileName;
            if(!$this->createDefaultFile($destinationPath)){
                return FALSE;  
            };
        }
        
        $this->_CI->image_lib->clear();
        return TRUE;
    }
    
    /*=================================================================
    *
    *   Uploads multiple files.   
    *   
    *   @param $id id of the object that will be used as the file name. 
    *   this would be appended by uploadNames values.
    *
    *   @param $folder root folder where will the photos get
    *   uploaded. This could be subdirectories within a directory like so:
    *   'temps/folder'
    *
    *   @param $uploadNames names of the file input given in html 
    *   this will be appended to $id.
    *
    *================================================================*/
    public function uploadFiles($id, $folder, $uploadNames = array())
    {
        
        //throw exception when uploadNames is empty.
        
        $path = $this->rootDirectory . $folder . '/';
        $this->configuration['upload_path'] = $path;
           
        $this->makeFolderIfNotExists($path);
      
        if($this->createThumbFolder) {
            $this->thumbFolderDirectory = $path . 'thumbs/';
            $this->makeFolderIfNotExists($this->thumbFolderDirectory);
            
        }
     
        if(empty($_FILES)) {
            //use defaults
            $fileName = $id . '_' . 'front' . '.jpg';
            $destinationPath = $path . $fileName;
            if(!$this->createDefaultFile($destinationPath)){
                return FALSE;
            };
            foreach($uploadNames as $name){   
               
            }
        } else {
            foreach($uploadNames as $name){   
                $fileName = $id . '_' . $name . '.jpg';
                if(array_key_exists($name,$_FILES)){
                    if(file_exists($_FILES[$name]['name']) || is_uploaded_file($_FILES[$name]['name'])) {

                    } else {

                    }
                } else {
                    $destinationPath = $path . $fileName;            
                    if($this->createDefaultFile($destinationPath)){
                        $src_path = $path . $fileName;
                        $thumb_path = $this->thumbFolderDirectory . $fileName . '_thumb.jpg';
                        $this->createThumbnail($src_path, $thumb_path , 100);	
                    };  
                    $this->errors['ci_upload_error'] = 'Cannot create default picture.';
                    return FALSE;
                }
            }
        }        
    }
        
    /*=================================================================
    *
    *   Moves file from a source folder to destination folder.   
    *
    *================================================================*/
    public function moveFile()
    {
    
    }
    
    public function moveFiles()
    {
       
    }
    
    /*=================================================================
    *
    *               Create folder if not existing  
    *
    *================================================================*/
    private function makeFolderIfNotExists($folderPath)
    {
        if(!file_exists($folderPath)){
            mkdir($folderPath, 0700);		
        }
    }   
           
    public function createDefaultFile($destinationPath)
    {
        var_dump($destinationPath);
        return copy($this->defaultImageFilePath, $destinationPath);
    }
    
    //no return type as of now.
    public function createThumbnail($srcPath, $thumbPath, $desiredWidth = 100)
    {
        /* read the source image */
        $source_image = imagecreatefromjpeg($srcPath);

        $width = imagesx($source_image);
        $height = imagesy($source_image);

        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = floor($height * ($desiredWidth / $width));

        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desiredWidth, $desired_height);

        /* copy source image at a resized size */
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desiredWidth, $desired_height, $width, $height);

        /* create the physical thumbnail image to its destination */
        imagejpeg($virtual_image, $thumbPath);
    }
          
    /*=================================================================
    *
    *                        Setter methods   
    *
    *================================================================*/   
    
    //tells FileHelper that preceeding uploads will be uploaded to temp directory.
    public function useTemp()
    {
        $this->uploadToTemp = true;
        $this->rootDirectory = $this->tempRootDirectory;
    }  
    public function useThumb()
    {
        $this->createThumbFolder = true;
    }  
           
    /*=================================================================
    *
    *                       Getter methods
    *
    *================================================================*/ 
     
    public function getErrors()
    {
        return $this->errors;
       
    }         
           
}