<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

	class Datetimehelper{
		
		
		//parse our string from datepicker to DATE format acceptable in mysql
		public function parseto_db_date($str){
			$date = new DateTime($str);
			
			return $date->format('Y-m-d');
		}
        
        public function parse_timestamp($int){
            return date('Y-m-d', $int);
        }
        
       
		public function parseto_standard_date($date){
			
			$date = new DateTime($date);
		
			return $date->format('F j, Y');
		}
		//parse our string from textfields to TIME format acceptable in mysql
		public function parseto_db_time($str){
			
			$time = new DateTime($str);

			return $time->format('H:i:s');
		}
		
		public function parseto_system_date($date){
			
			$date = new DateTime($date);
		
			return $date->format('m/d/Y');
		}
		
		public function parseto_system_time($str){
            $time = new DateTime($str);
            
            
            return $time->format('h:i A');
		}
        
        public function get_current_date(){
            return date('Y-m-d');
        }
        
        public function get_current_year(){
             return date("Y");
        }
        
        public function get_month_int_value($month){
            $date = date_parse($month);
            return $date['month'];
        }
        
        public function get_month_full_value($monthNum){
            $dateObj = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
        }
        
        
        //returns integer value of a month
        public function get_current_month(){
            $now = new DateTime('now');
            $month = $now->format('m');
            
            return $month;
        }
        
        public function get_age($date){
			$birth = new DateTime($date);
			$currDate = new DateTime();
			return  $birth->diff($currDate);
		}
	
        
        //get number of days of particular month in a particular year,
        public function get_num_days($month, $year){
            $days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
            
            return $days;
        }
	}
	
	