<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class ArchiveManager {
      
    public function transfer_to_archive($case_id){
        $files = array();
        
        $dest_path = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/archives/';
        
        $this->rrmdir($dest_path);
        
        $case_path =  $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/photos/case/' . $case_id . '/';
        $inv_path =  $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/photos/inventory/' . $case_id . '/';
        
        
        $this->recurse_copy($case_path, $dest_path . '/case');
        $this->recurse_copy($inv_path, $dest_path . '/inventory');
    }
 
    function rrmdir($dir) { 
        foreach(glob($dir . '/*') as $file) { 
            if(is_dir($file)) 
                $this->rrmdir($file); 
            else 
                unlink($file); 
        } 
        rmdir($dir); 
    }
    function recurse_delete($dir) { 
       if (is_dir($dir)) { 
            $objects = scandir($dir); 
            foreach ($objects as $object) { 
                if ($object != "." && $object != "..") { 
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); 
                        else unlink($dir."/".$object); 
                } 
            } 
            reset($objects); 
            
            $pieces = explode("//", $dir);
            $segment = end($pieces);
          
            if($segment != 'archives'){
                rmdir($dir);
            }   

        } 
    }
    function recurse_copy($src,$dst) { 
        $dest_path = $_SERVER['DOCUMENT_ROOT'] . '/ssmis/public/resources/archives/';
        if(!is_dir($dest_path)){
         
            mkdir($dest_path, 0755);
        }
        
        $dir = opendir($src); 
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } 
                else { 
                    copy($src . '/' . $file,$dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    }    
}