<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('html2pdf.php');



class Pdfgenerator extends PDF_MemImage
{


  // Extend FPDF using this class
  // More at fpdf.org -> Tutorials
  function __construct($orientation='L', $unit='mm', $size='Legal')
  {
    // Call parent constructor
    parent::__construct($orientation,$unit,$size);
  }
  
  //called automatically
  public function Header(){
      if($this->Header == 'Forms'){
          if($this->PageNo() !== 0){
                if($this->DefOrientation === 'L'){
                    $this->Ln(10);
                    $this->Image('public/resources/design_photos/pnp_logo.png',70,20,32,35);
                    $this->SetFont('Arial','',12);
                    $this->Cell(0,6,'Republic of the Philippines',0,1,'C');  
                    $this->Cell(0,6,'PHILIPPINE NATIONAL POLICE',0,1,'C');
                    $this->SetFont('Arial','B',12);              
                    $this->Cell(0,6,'EASTERN POLICE DISTRICT',0,1,'C');
                    $this->SetFont('Arial','BI',12);
                    $this->Cell(0,6,'SAN JUAN CITY POLICE STAION',0,1,'C');
                    $this->Cell(0,6,'STATIONG ANTI-ILLEGAL DRUGS SPECIAL OPERATIONTASK GROUP',0,1,'C');
                    $this->SetFont('Arial','',12);
                    $this->Cell(0,6,'Santolan Road, San Juan City',0,1,'C');
                    $this->Ln(10);
                }elseif($this->DefOrientation === 'P'){
                    $this->Ln(10);
                    $this->Image('public/resources/design_photos/pnp_logo.png',60,20,20,20);
                    $this->SetFont('Arial','',10);
                    $this->Cell(200,5,'Republic of the Philippines',0,1,'C');  
                    $this->Cell(200,5,'PHILIPPINE NATIONAL POLICE',0,1,'C');
                    $this->SetFont('Arial','B',10);              
                    $this->Cell(200,5,'EASTERN POLICE DISTRICT',0,1,'C');
                    $this->SetFont('Arial','BI',10);
                    $this->Cell(200,5,'SAN JUAN CITY POLICE STAION',0,1,'C');
                    $this->Cell(200,5,'STATIONG ANTI-ILLEGAL DRUGS SPECIAL OPERATIONTASK GROUP',0,1,'C');
                    $this->SetFont('Arial','',10);
                    $this->Cell(200,5,'Santolan Road, San Juan City',0,1,'C');
                    $this->Ln(5);                       
                }
            }	
	}elseif($this->Header == 'Arrest Booking'){
        if($this->PageNo() !== 0){
            $this->SetLeftMargin(15);
            $this->SetRightMargin(15);
            $this->Ln(); 
            $this->Image('public/resources/design_photos/pnp_logo.png',150,10,28,28);
            $this->SetFont('Arial','',10);
            $this->Cell(0,5,'PHILIPPINE NATIONAL POLICE',0,1,'C');            
            $this->Cell(0,5,'EASTERN POLICE DISTRICT',0,1,'C');
            $this->Cell(0,5,'SAN JUAN CITY POLICE STAION',0,1,'C');            
            $this->Cell(0,5,'PNP ARREST AND BOOKING SHEET',0,1,'C');
        }

	}elseif($this->Header == 'Accomplishment'){
          
            if ($this->PageNo() === 1) { 
                $this->Ln(10);
                $this->Image('public/resources/design_photos/pnp_logo.png',70,20,32,35);
                $this->SetFont('Arial','',12);
                $this->Cell(0,6,'Republic of the Philippines',0,1,'C');  
                $this->Cell(0,6,'PHILIPPINE NATIONAL POLICE',0,1,'C');
                $this->SetFont('Arial','B',12);              
                $this->Cell(0,6,'EASTERN POLICE DISTRICT',0,1,'C');
                $this->SetFont('Arial','BI',12);
                $this->Cell(0,6,'SAN JUAN CITY POLICE STAION',0,1,'C');
                $this->Cell(0,6,'STATIONG ANTI-ILLEGAL DRUGS SPECIAL OPERATIONTASK GROUP',0,1,'C');
                $this->SetFont('Arial','',12);
                $this->Cell(0,6,'Santolan Road, San Juan City',0,1,'C');
                $this->Ln(10);
            }
      }
	
	
  }
    
    public function Footer(){
        $lastPage = $this->AliasNbPages();
        if($this->flagg == 1) {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,'Page '. $this->PageNo() . '/{nb}',0,0,'C');
        }
    }
    public function StatFooter($str, $admin="No admin supplied."){
        if($str) {
            $this->SetFont('Arial','',12);
            $this->SetY(-50);
            $this->SetX(35);
            $this->Cell(180,7,'Prepared by:',0,0,'L');
            $this->Cell(0,7,'Noted by:',0,1,'L');
            $this->SetX(35);
            $this->SetFont('Arial','B',12);
            $this->Cell(180,7,$str,0,0,'L');
            $this->Cell(0,7,$admin,0,1,'L');

            $this->SetFont('Arial','',12);
            $this->SetX(35);
            $this->Cell(180,7,'',0,0,'L');
            
            $this->SetX(35);
            $this->Cell(180,7,' ',0,0,'L');
            $this->Cell(0,7,'Chief, SAID-SOTG',0,1,'L');
        }
    }
    
    public function ModFooter($str,$noPages, $admin="No admin supplied"){
        if ($this->PageNo() === $noPages) {
            $this->SetFont('Arial','',12);
            $this->SetY(-50);
            $this->SetX(35);
            $this->Cell(180,7,'Prepared by:',0,0,'L');
            $this->Cell(0,7,'Noted by:',0,1,'L');
            $this->SetX(35);
            $this->SetFont('Arial','B',12);
            $this->Cell(180,7,$str,0,0,'L');
            $this->Cell(0,7,$admin,0,1,'L');

            $this->SetFont('Arial','',12);
            $this->SetX(35);
            $this->Cell(180,7,' ',0,0,'L');
            $this->Cell(0,7,'Chief, SAID-SOTG',0,1,'L');
        }
    }
  
	/*=============================================
						Word Wrap
	==============================================*/
	function WordWrap($text, $maxwidth){
		$text = trim($text);
		if ($text==='')
			return 0;
		$space = $this->GetStringWidth(' ');
		$lines = explode("\n", $text);
		$text = '';
		$count = 0;

		foreach ($lines as $line)
		{
			$words = preg_split('/ +/', $line);
			$width = 0;

			foreach ($words as $word)
			{
				$wordwidth = $this->GetStringWidth($word);
				if ($wordwidth > $maxwidth)
				{
					// Word is too long, we cut it
					for($i=0; $i<strlen($word); $i++)
					{
						$wordwidth = $this->GetStringWidth(substr($word, $i, 1));
						if($width + $wordwidth <= $maxwidth)
						{
							$width += $wordwidth;
							$text .= substr($word, $i, 1);
						}
						else
						{
							$width = $wordwidth;
							$text = rtrim($text)."\n".substr($word, $i, 1);
							$count++;
						}
					}
				}
				elseif($width + $wordwidth <= $maxwidth)
				{
					$width += $wordwidth + $space;
					$text .= $word.' ';
				}
				else
				{
					$width = $wordwidth + $space;
					$text = rtrim($text)."\n".$word.' ';
					$count++;
				}
			}
			$text = rtrim($text)."\n";
			$count++;
		}
		$text = rtrim($text);
		return $count;
	}
	/*=============================================
						Header
	==============================================*/
	function createTableHeader($arrayName, $arrayCol, $size){
		foreach($arrayName as $index => $arrayName){
			$this->SetFont('Arial','B',$size);
			$this->Cell($arrayCol[$index],7,$arrayName,1,0,'C');
		}
	}
    
    function MultiAlignCell($w,$h,$text,$border=0,$ln=0,$align='L',$fill=false){
        // Store reset values for (x,y) positions
        $x = $this->GetX() + $w;
        $y = $this->GetY();

        // Make a call to FPDF's MultiCell
        $this->MultiCell($w,$h,$text,$border,$align,$fill);

        // Reset the line position to the right, like in Cell
        if( $ln==0 )
        {
            $this->SetXY($x,$y);
        }
    }
    
    function CheckPageBreak($h){
        #If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
        {
        $this->AddPage($this->CurOrientation);
        }
    }
    function NbLines($w,$txt){
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0){
            $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r",'',$txt);
            $nb=strlen($s);
        }
        if($nb>0 and $s[$nb-1]=="\n"){
            $nb--;
            $sep=-1;
            $i=0;
            $j=0;
            $l=0;
            $nl=1;
        }
        while($i<$nb){
            $c=$s[$i];
            if($c=="\n"){
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' '){
                $sep=$i;
                $l+=$cw[$c];
            }
            if($l>$wmax){
                if($sep==-1){
                    if($i==$j)
                    $i++;
                }
                else{
                    $i=$sep+1;
                }
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else{
                $i++;
            }
        }
        return $nl; 
    }
    
}

?>