
		<div id="content">
			<div id="content-header">
				<h1>Barangay Maintenance</h1>
			</div> <!-- #content-header -->	
			
			<div id="content-container">
			
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url() . "index.php/barangay"?>">Barangay</a></li>
							<li><a href="<?php echo base_url() . "index.php/barangay"?>" class="active">List of Barangay in San Juan</a>
							</li>							
						</ol>
					</div>
				</div>

				<!-- TOOLBAR -->
				<div class="row">
					<div class="col-lg-12 col-xs-12 col-xs-12">
						<div class="btn-toolbar" role="toolbar">
							<div class="btn-group">
								<button type="button" id = "addButton"class="btn btn-default"><i class="fa fa-plus"></i> Add</button>
							</div>
						</div>
					</div>
				</div>
				<!-- List Of RA Laws -->
				
				<div class="row">
					<div class="col-lg-12">
						<div class="portlet">
							<div class="portlet-header">
								<h2><i class="fa fa-group"></i><span class="break"></span>Barangays</h2>
							</div>
							<div class="portlet-content">
								<table id="barangay_table" class="table table-striped table-bordered table-hover table-highlight table-checkable">
									<thead>
										<tr>
											<th class="">Barangay ID</th>
											<th class="">Name of Barangay</th>
											<th class="">Name of City
											<th class="">No. of Incidents</th>
											<th class="">Current Chairman</th>
										</tr>	
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>





