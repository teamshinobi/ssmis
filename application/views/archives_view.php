	<div id="content">		
		
		<div id="content-header">
			<h1><i class="fa fa-archive"> </i> Archives</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Notes
                    </div>
                </div>
                <div class="panel-body">
                    <p>
                        All archived files are not shown to other users unless you restore it.
                        <br>You can download archives by right clicking a row then click download.
                    </p>
                </div>
            </div>
            
            <div class="row">
				<div class="col-lg-12">
					<div class="portlet">
						<div class="portlet-header">
							<h2><i class="fa fa-archive"></i><span class="break"></span>Archives</h2>
                            <ul class="portlet-tools pull-right">
                                <li>
                                    <button title="Archive"  id="addButton" type="button" class="btn btn-md btn-default">
                                        <span><i class="fa fa-plus"></i></span>
                                    </button>
                                </li>
                            </ul>
						</div>
						<div class="portlet-content">
							<div class="table-responsive">
								<table id="archives_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable">
									<thead>
										<tr>
											<th class="">ID</th>
											<th class="">Case ID</th>
											<th class="">Blotter</th>
                                            <th class="">Date Archived</th>
										</tr>	
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>         
			</div>
            <div class="row">
                <div class="col-lg-4">
                    <button id="fileManager" class="btn btn-default" type="button">File Manager</</button>
                </div>
            </div>
        </div>
    </div>   
<!-- End of -->
</div>