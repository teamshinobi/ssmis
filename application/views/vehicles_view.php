	<div id="content">		
		
		<div id="content-header">
			<h1>Maintenance</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">	
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() . "/vehicles"?>">Vehicles</a>
						</li>								
					</ol>
				</div>
			</div>
            <div class="row">
				<div class="col-lg-12 col-xs-12 col-xs-12">
                    <div class="btn-toolbar" role="toolbar">
                        <div class="btn-group">
                            <button id="addButton" type="button" class="toolbar-button btn btn-default"><i class="fa fa-plus"></i> Add </button>
                        </div>
                    </div>
				</div>
			</div>
            <div class="row">
				<div class="col-lg-12">
					<div class="portlet">
						<div class="portlet-header">
							<h2><i class="fa fa-car"></i><span class="break"></span>Station Vehicles</h2>
						</div>
						<div class="portlet-content">
							<div class="table-responsive">
								<table id="vehicles_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable">
									<thead>
										<tr>
											<th class="">ID</th>
											<th class="">Type</th>
											<th class="">Make</th>
                                            <th class="">Color</th>
											<th class="">Plate No.</th>
                                            <th class="">Image</th>
										</tr>	
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	   
<!-- End of -->
</div>