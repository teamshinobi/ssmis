	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="modal-title" id="myModalLabel">
						<h4><i class="fa fa-lg fa-search"></i> Select records you want to archive.</h4>
						
					</div>
				</div>
				<div class="modal-body">
				    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            
                            <table id="casereports_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable">
                                <thead>
                                    <tr>
        
                                        <th class="">Case ID</th>
                                        <th class="">Blotter</th>
                                        <th class="">DOA</th>
                                        <th class="">IS #</th>
                                        <th class="">Progress</th>
                                        <th class=""><i style="margin-left:7px;" class="fa fa-check"></i></th>
                                    </tr>	
                                </thead>
				            </table>
                                  
                        </div>
                    </div>
					
				<!-- END of MODAL BODY -->
				</div>
					
					<div class="modal-footer">
						<button type="button"class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="startArchiveButton" type="button"class="btn btn-default">Start Archiving</button>
					</div>
				</form>
			</div>
		</div>
	</div>	