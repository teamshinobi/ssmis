	<div id="content">		
		
		<div id="content-header">
			<h1>Violators</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">	
		<div class="row">
			<div class="portlet">
				<div class="portlet-header">
					<h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Violators</h2>
                </div>
                
				<div class="portlet-content">
					<div class="row">
						<div class="table-responsive col-lg-12 col-md-12 col-xs-12 col-sm-12">
							<table id="violators_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
								<thead>
									<tr>
										<th class="">Violator ID</th>
										<th class="">Full Name</th>
										<th class="">Alias</th>
										<th class="">Last Case</th>
										<th class="">Picture</th>						
									</tr>	
								</thead>				
							</table>		
						
						<p>Click a row in a table to view violator.</p>
						</div>
					
			 </div>
		</div>
	</div>	
<!-- End of -->
</div>