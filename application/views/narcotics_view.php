	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
			<h1>Narcotics</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() . "index.php/narcotics"?>">NARCOTICS</a>
						</li>
						<li><a href="<?php echo base_url() . "index.php/narcotics"?>" class="active">Maintenance</a>
						</li>							
					</ol>
				</div>
			</div>
			<!-- TOOLBAR -->
			<div class="row toolbar-container">
				<div class="col-lg-12 col-xs-12 col-xs-12">
					<div class="btn-toolbar" role="toolbar">
						<div class="btn-group">
							<button id="addButton" type="button" class="toolbar-button btn btn-default"><i class="fa fa-plus"></i> Add</button>
					   </div>
					</div>
				</div>
			</div>
			
			<div class="portlet">
				<div class="portlet-header">
					<h2><i class="fa fa-group"></i><span class="break"></span>Narcotic</h2>
				</div>
				<div class="portlet-content">
					<div class="table-responsive">
						<table id="narcotics_table" class="table table-striped table-bordered table-hover table-highlight table-checkable">
							<thead>
								<tr>
									<th>Narcotics ID</th>
									<th>Name</th>
									<th>Description</th>
								</tr>	
							</thead>
						</table>	
					</div>					
				</div>
			</div>
		</div>
	</div>
<!-- End of Wrapper for the sidebar -->
</div>