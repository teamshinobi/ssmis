<div class="modal fade" id="addModal"  tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><i class="fa fa-lg fa-car"></i> Record New Vehicle Information</h4>
                <p id="vehicles_id" class="text-muted">Vehicle ID</p>
            </div>
            <form id="add_vehicle_form">
            <div class="modal-body" id="add_vehicles">
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            MAKE    
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input name="make" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            TYPE    
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input name="type" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            COLOR    
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input name="color" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            PLATE NO    
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input name="plate_no" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            UPLOAD IMAGE   
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input id="file" name="file" type="file"/>
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6 col-md=6 col-lg-offset-4 col-md-offset-4">
                        <img id="filepreview" name="filepreview" class="img-img-responsive img-thumbnail" style="width:200px;height:200px;" src=<?php echo base_url() . 'public/resources/default_img.jpg ' ;?>/>
				    </div>
                </div>
            </div>			
            <div class="modal-footer">
                <button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>	
            </form>
        </div>
    </div>
</div>	