	<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="modal-title" id="myModalLabel">
						<h4><i class="fa fa-lg fa-car"></i>Information about the Vehicle</h4>
						<p id = "vehicle_view"> Barangay ID</p>
					</div>
				</div>
				<div class="modal-body" id = "view_vehicles">
				<form id="view-vehicle-form" method="POST">
								
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "label-pad">Make:</label>
								</div>
								<div class = "col-lg-4">
									<input  id ="make" name="make"  type = "text" class = "form-control" placeholder = "make" required>	
								</div>
								<div class = "col-lg-2">
									<label class = "label-pad">Type:</label>
								</div>
								<div class = "col-lg-4">
									<input name = "type" id ="type" type = "text" class = "form-control" placeholder = "TYPE" >
								</div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "label-pad">Color:</label>
								</div>
								<div class = "col-lg-4">
									<input name="color" id ="color" type = "text" class = "form-control" placeholder = "color" required>	
								</div>
								<div class = "col-lg-2">
									<label class = "label-pad">Plate No:</label>
								</div>
								<div class = "col-lg-4">
									<input name="plate_no" id = "plate_no" type = "text" class = "form-control" placeholder = "Plate No" required>	
								</div>
							</div>

							<div class = "row row-pad">
								<div class="col-lg-6 col-md=6 col-lg-offset-4 col-md-offset-4">
                    			    <img id="filepreviewView" class="img-img-responsive img-thumbnail" style="width:200px;height:200px;" src=<?php echo base_url() . 'public/resources/default_img.jpg ' ;?>/>
				   				 </div>
							</div>
							<div class = "row row-pad">
								<div class= "col-lg-4">
								</div>
								<div class="col-lg-6 col-md=6">
                       				 <input id="fileView" name="fileView" type="file"/>
				   				 </div>
							</div>
					

					
				<!-- END of MODAL BODY -->
				</div>
					
					<div class="modal-footer">
						<button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" id="saveButton" class="btn btn-primary saveButton">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>	