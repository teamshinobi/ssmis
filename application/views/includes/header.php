<html>
	<head>
		<title><?php echo $title ?></title>
		<link rel="shortcut icon" href="<?php echo base_url() . 'public/resources/design_photos/logo32.png' ?>">
		<!-- Loads default CSS and JS including extra CSS and JS -->
		<?php $this->load->view('includes/links.php'); ?>	
	</head>
	<!-- add background image depends on what page to be loaded -->
	<body>
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>