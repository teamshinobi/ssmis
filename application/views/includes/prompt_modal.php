	<div class="modal fade" id="prompt-modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="modal-title">Provide Title Here</h4>
				</div>
				<div class="modal-body" id="modal-body">
					<p>
						This is the body to be changed dynamically
					</p>
				</div>	
				<div class="modal-footer">
					<button type="button" id="saveButton" class="btn btn-default" data-dismiss="modal">Ok</button>
				</div>
			</div>
		</div>
	</div>