<meta name = "viewport" content = "width=device-width, initial-scale= 1.0" charset="utf-8">

<!-- BOOTSTRAP CSS -->
<link rel="stylesheet" type="css/text" href="<?php echo base_url()?>public/included_css/bootstrap.css" media="screen"/>
<!-- FONT-AWESOME minified -->
<link rel="stylesheet" href="<?php echo base_url()?>public/included_css/font-awesome/css/font-awesome.min.css"/>
<!-- JQUERY CSS minified -->
<link rel="stylesheet" type="css/text" href=" <?php echo base_url() ?>public/included_css/jquery-ui.min.css"/>
<!-- JQUERY -->
<script type="text/javascript" src=" <?php echo base_url() ?>public/included_js/jquery.js"></script> 
<!-- JQUERY UI minified -->
<script type="text/javascript" src=" <?php echo base_url() ?>public/included_js/jquery-ui.min.js"></script> 
<!-- JQUERY BOOTSTRAP minified-->
<script type="text/javascript" src=" <?php echo base_url() ?>public/included_js/bootstrap.js"></script> 
<!-- APP -->
<script type="text/javascript" src=" <?php echo base_url() ?>public/system_js/App.js"></script> 
<link rel="stylesheet" type="css/text" href=" <?php echo base_url() ?>public/system_css/App.css"/>

<!-- Loads Extra CSS -->

<?php
    
    
	if(isset($extra_css) || !empty($extra_css)){
		foreach($extra_css as $value){
			$href = $value;
			if(strcmp(substr($value,0,4),'http') == 0){
				$href = $value;
			}else{
				$href = base_url() . $value;
			}
			
			echo "<link rel=\"stylesheet\" href=\"" . $href ."\"" . "/>"; 
		}
	}
	
?>