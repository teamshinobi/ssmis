<div id="wrapper">

	<header id="header">

		<h1 id="site-logo" style="padding-bottom: 50px;">
			<a href="./index.html">
				<img style="width: 200px; height: 75px;" src="<?php echo base_url() . 'public/resources/design_photos/PNP.png'?>" alt="Site Logo" />
			</a>
		</h1>	

		<a href="javascript:;" data-toggle="collapse" data-target=".top-bar-collapse" id="top-bar-toggle" class="navbar-toggle collapsed">
			<i class="fa fa-cog"></i>
		</a>

		<a href="javascript:;" data-toggle="collapse" data-target=".sidebar-collapse" id="sidebar-toggle" class="navbar-toggle collapsed">
			<i class="fa fa-reorder"></i>
		</a>

	</header> <!-- header -->


	<nav id="top-bar" class="collapse top-bar-collapse">

		<ul class="nav navbar-nav pull-right">
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
					<i class="fa fa-user"></i>
						<?php echo $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name'); ?>
		        	<span class="caret"></span>
		    	</a>

		    	<ul class="dropdown-menu" role="menu">
			        <li>
			        	<a href="<?php echo base_url() . "profile/"?>">
			        		<i class="fa fa-user"></i> 
			        		&nbsp;&nbsp;My Profile
			        	</a>
			        </li>
			        <li>
			        	<a href="<?php echo base_url() . "messages"?>">
			        		<i class="fa fa-envelope-o"></i> 
			        		&nbsp;&nbsp;My Messages
			        	</a>
			        </li>
			            <?php
                    if($this->ion_auth->is_admin()){
                    ?>
                        <li>
                            <a href="<?php echo base_url() .'settings/'?>">
                                <i class="fa fa-cogs"></i> 
                                &nbsp;&nbsp;Settings
                            </a>
                        </li>
                    <?php } ?>
                    <li class="divider"></li>
                    <?php
                    if($this->ion_auth->is_admin()){
                    ?>
                        <li>
                            <a href="<?php echo base_url() . "audit_trail"?>">
                                <i class="fa fa-keyboard-o"></i> 
                                &nbsp;&nbsp;Audit Trail
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . "tip"?>">
                                <i class="fa fa-book"></i> 
                                &nbsp;&nbsp;Tip Notebook
                            </a>
                        </li>
                    <?php } ?>
			        <li class="divider"></li>
			        <li>
			        	<a href="<?php echo $logout; ?>">
			        		<i class="fa fa-sign-out"></i> 
			        		&nbsp;&nbsp;Logout
			        	</a>
			        </li>
                    
		    	</ul>
		    </li>
		</ul>
	</nav> <!-- /#top-bar -->
	
	
	<div id="sidebar-wrapper" class="collapse sidebar-collapse">
	
		<div id="search">
            
            <form action="/./ssmis/search/" method="GET">
                <input name="k" class="form-control" type="text" placeholder="Search..." >
                <button type="submit" id="search-btn" class="btn" autocomplete="off" ><i class="fa fa-search"></i></button>
            </form>		
		</div> <!-- #search -->
             
		<nav id="sidebar">		
			
			<ul id="main-nav" class="open-active">		
			
				<li class="<?php if($title === 'Home'){ echo 'active';}?>">				
					<a href="<?php echo base_url() . "home"?>">
						<i class="fa fa-dashboard"></i>
						Home
					</a>				
				</li>
                <li class="<?php if(isset($active_nav)){
                          if($active_nav === "Forms") { 
                              echo 'active ';
                          }}
                        ?>dropdown">
					<a href="javascript:;">
						<i class="fa fa-file-text"></i>
						  Forms
						<span class="caret"></span>
					</a>
                    
					<ul class="sub-nav">
                        <li class="">
							<a  href="<?php echo base_url() . "forms/spotreport"?>" class="<?php if($title === 'Spot Report'){ echo 'sub-active';}?>">
								<i class="fa fa-thumb-tack <?php if($title === 'Spot Report'){ echo 'sub-active';}?>"></i> 
								Spot Report
							</a>
						</li>
                        <li class="">
							<a  href="<?php echo base_url() . "forms/preoperation"?>" class="<?php if($title === 'Pre-operation'){ echo 'sub-active';}?>">
								<i class="fa fa-connectdevelop <?php if($title === 'Pre-operation'){ echo 'sub-active';}?>"></i> 
								Pre-Operation Report
							</a>
						</li>
                        <li class="">
							<a  href="<?php echo base_url() . "forms/coordination"?>" class="<?php if($title === 'Coordination'){ echo 'sub-active';}?>">
								<i class="fa fa-arrows <?php if($title === 'Coordination'){ echo 'sub-active';}?>"></i> 
				                Coordination Form
							</a>
						</li>
                         <li class="">
							<a  href="<?php echo base_url() . "forms/affidavits"?>" class="<?php if($title === 'Affidavits'){ echo 'sub-active';}?>">
								<i class="fa fa-leanpub <?php if($title === 'Affidavits'){ echo 'sub-active';}?>"></i> 
				                Affidavits
							</a>
						</li>
                    </ul>
                </li>
				
				
				<li class="<?php if($active_nav === "Case Report"){ echo 'active ';}?>dropdown">
					<a href="javascript:;">
						<i class="fa fa-file-text"></i>
						Case Report
						<span class="caret"></span>
					</a>				
					
					<ul class="sub-nav">
						<li class="<?php if($active_nav === "Case Report"){ echo 'active';}?>">
							<a  href="<?php echo base_url() . "casereport"?>" class="<?php if($title === 'New Case Report'){ echo 'sub-active';}?>">
								<i class="fa fa-folder"></i> 
								New Case Report
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() . "viewcasereports"?>" class="<?php if($title === 'Case Reports'){ echo 'sub-active';}?>">
								<i class="fa fa-money"></i> 
								View Case Reports
							</a>
						</li>
					</ul>						
					
				</li>	
				
				<li class="<?php if($active_nav === "Violators"){ echo 'active ';}?>dropdown">
					<a href="javascript:;">
						<i class="fa fa-group"></i>
						Violators
						<span class="caret"></span>
					</a>
					
					<ul class="sub-nav">
						<li>
							<a href="<?php echo base_url() . "violators"?>" >
								<i class="fa fa-location-arrow"></i>
								View Violators
							</a>
						</li>
						
						<li>
							<a href="<?php echo base_url() . "gallery"?>" class="<?php if($title === 'Gallery'){ echo 'sub-active';}?>">
								<i class="fa fa-image"></i>
								Gallery
							</a>
						</li>							
					</ul>	
									
				</li>
              
               
                
                 <?php
                    if($this->ion_auth->is_admin()){
                ?>
                <li class="<?php if($title === "PNP"){ echo 'active ';}?>dropdown">
                    <a  href="javascript:;">
                        <i class="fa fa-user"></i>
                        PNP
                        <span class="caret"></span>
                    </a>	

                    <ul class="sub-nav">
                        <li>
                            <a href="<?php echo base_url() . "pnp"?>">
                                <i class="fa fa-hand-o-up"></i>
                                Maintenance
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . "pnp/groups"?>">
                                <i class="fa fa-reorder"></i>
                                Groups
                            </a>
                        </li>	
                    </ul>
                </li>         

                <?php } ?>
                
                <?php
                    if($this->ion_auth->is_admin()){
                ?>
                <li class="<?php if($active_nav === "Miscellaneous"){ echo 'active ';}?> dropdown">
					<a href="javascript:;">
						<i class="fa fa-plus"></i>
						Miscellaneous
						<span class="caret"></span>
					</a>
					<ul class="sub-nav">
                        <li class="<?php if($title === 'Narcotics'){ echo 'sub-active';}?>">				
                            <a href="<?php echo base_url() . "narcotics"?>">
                                <i class="fa fa-leaf"></i>
                                Narcotics
                            </a>				
                        </li>

                        <li>				
                            <a href="<?php echo base_url() . "barangay"?>" class="<?php if($title === 'Barangay'){ echo 'sub-active';}?>">
                                <i class="fa fa-users <?php if($title === 'Barangay'){ echo 'sub-active';}?>" ></i>
                                Barangay
                            </a>				
                        </li>
                        <li>				
                            <a href="<?php echo base_url() . "ra"?>" class="<?php if($title === 'RA 9165'){ echo 'sub-active';}?>">
                                <i class="fa fa-legal <?php if($title === 'RA 9165'){ echo 'sub-active';}?>" ></i>
                                RA 9165
                            </a>				
                        </li>
                        <li>				
                            <a href="<?php echo base_url() . "vehicles"?>" class="<?php if($title === 'Vehicles'){ echo 'sub-active';}?>">
                                <i class="fa fa-car <?php if($title === 'Vehicles'){ echo 'sub-active';}?>"></i>
                                Vehicles
                            </a>				
                        </li>
                    </ul>
                </li>
                 <?php } ?>
				
				<li>				
					<a href="<?php echo base_url() . "reports"?>">
						<i class="fa fa-pencil"></i>
						Reports
					</a>				
				</li>
                <?php
                    if($this->ion_auth->is_admin()){
                ?>
                <li class="<?php if($title === 'Archives'){ echo 'active';}?>">				
					<a href="<?php echo base_url() . "archives"?>">
						<i class="fa fa-archive"></i>
						Archives
					</a>				
				</li>
                <?php } ?>
			</ul>
					
		</nav> <!-- #sidebar -->

	</div> <!-- /#sidebar-wrapper -->