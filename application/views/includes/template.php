<?php 
	//This file loads all required subviews and links
	
	$this->load->view('includes/header.php'); 

	
	//This can be sidebar etc.
	if(!empty($extra_view)){
		foreach($extra_view as $value){
			$this->load->view($value);
		}
	}


	$this->load->view($page_content);

	$this->load->view('includes/footer.php'); 

?>