
	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
			<h1><?php echo $group[0]->group_name ?></h1>
		</div> <!-- #content-header -->	
		
		<div id="content-container">
			<div class="row">
				<div class="col-lg-12 col-sm-12 col-xs-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() . "index.php/pnp"?>">PNP</a>
						</li>
						<li><a href="<?php echo base_url() . "index.php/pnp/groups"?>" class="active">Groups</a>
						</li>
						<li><a href="<?php echo base_url() . "index.php/pnp/viewgroup/" . $group[0]->group_id ?>" class="active"><?php echo $group[0]->group_name ?></a>
						</li>							
					</ol>
				</div>	
			</div>	
			<!-- TOOLBAR -->
			<div class="row toolbar-container">
				<div class="col-lg-12 col-xs-12 col-xs-12">
					<div class="btn-toolbar" role="toolbar">
						<div class="btn-group">
							<button id="editGroupButton" type="button" class="toolbar-button btn btn-default">Edit Group</button>
							<button id="newMemberButton" type="button" class="toolbar-button btn btn-default">New Member</button>
						</div>
					</div>
				</div>
			</div>	
			
			<!-- Group Members -->		
			<div class="portlet">
				<div class="portlet-header">
					<h2><i class="fa fa-group"></i><span class="break"></span>Members</h2>
				</div>
				<div class="portlet-content">
					<div class="table-responsive">
						<table id="members_table" class="table table-striped table-bordered table-hover table-highlight table-checkabl">
							<thead>
								<tr>
									<th class="">Batch ID</th>
									<th class="">Full Name</th>
									<th class="">Rank</th>
									<th class="">Picture</th>
								</tr>	
							</thead>
						</table>
					</div>					
				</div>
			</div>
							
		</div>
	</div>
    <!-- /#page-content-wrapper -->
	   
<!-- End of Wrapper for the sidebar -->
</div>