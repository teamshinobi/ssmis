	<div id="content">
		<div id="content-header">
				<h1>Account settings</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
              <div class="row">

                    <div class="col-md-3 col-sm-4">

                        <ul id="myTab" class="nav nav-pills nav-stacked">
                            <li class="active">
                                <a href="#basic-tab" data-toggle="tab">
                                    <i class="fa fa-user"></i> 
                                    &nbsp;&nbsp;Basic Info
                                </a>
                            </li>
                             <li>
                                <a href="#credentials-tab" data-toggle="tab">
                                    <i class="fa fa-lock"></i> 
                                    &nbsp;&nbsp;Username and password
                                </a>
                            </li>
                           
                        </ul>
                    </div> <!-- /.col -->

                    <div class="col-md-9 col-sm-8">

                        <div class="tab-content stacked-content">
                            
                            
                            <div class="tab-pane fade in active" id="basic-tab">

                                <h3 class="">Basic Info</h3>

                                <p>Change information about you.</p>

                                <br />

                                <form id="profile_info_form">
                                    <div class = "row">
                                        <div class = "col-lg-2">
                                           <label class = "form-label">First Name</label>
                                        </div>
                                        <div class = "col-lg-6">
                                           <input name="first_name" id="first_name" type = "text" class = "form-control" placeholder = "First Name" required>
                                        </div>
                                    </div>
                                     <div class = "row">
                                        <div class = "col-lg-2">
                                           <label class = "form-label">Middle Name:</label>
                                        </div>
                                        <div class = "col-lg-6">
                                           <input name="middle_name" id="middle_name" type = "text" class = "form-control" placeholder = "Middle Name" required>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class = "col-lg-2">
                                           <label class = "form-label">Last Name:</label>
                                        </div>
                                        <div class = "col-lg-6">
                                           <input name="last_name" id="last_name" type = "text" class = "form-control" placeholder = "Last Name" required>
                                        </div>
                                    </div>
                                     <div class = "row">
                                        <div class = "col-lg-2">
                                           <label class = "form-label">Gender: </label>
                                        </div>
                                        <div class = "col-lg-1">
                                                <div class="radio">
                                                    <label><input name="gender" type="radio" value="1" checked>Male</label>
                                                </div>
								        </div>
                                        <div  class="col-lg-1">
                                            <div class="radio">
                                                <label><input name="gender" type="radio" value="0">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class = "col-lg-2">
                                           <label class = "form-label">Address:</label>
                                        </div>
                                        <div class = "col-lg-6">
                                           <input name="address" id="address" type = "text" class = "form-control" placeholder = "Address" required>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class = "col-lg-2">
                                           <label class = "form-label">Contact #:</label>
                                        </div>
                                        <div class = "col-lg-6">
                                           <input name="contact_number" id="contact_number" type = "text" class = "form-control" placeholder = "Contact Numbers" required>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class = "col-lg-2">
                                           <button type="submit" class="btn btn-default">
                                                <i class="fa fa-save"></i>   
                                               Save
                                            </button>
                                        </div>                                   
                                    </div>
                                </form>
                            
                         
                            
                            
                            </div> <!-- END TAB -->
                            
                            <div class="tab-pane fade" id="credentials-tab">

                                <h3 class="">Username and password</h3>

                                <p id="message"> Change username and password. <br/> 
                                    NOTE: Your batch ID will be your username. 
                                    If you are going to change your username, ask the admin/chief
                                    of your station.
                                </p>

                                <br />
                                <div class="row form-group">
                                    <div class="col-md-2 col-lg-2">
                                        <label>New password</label>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <input type="password" class="form-control" id="changePass" />
                                    </div>
                                </div>
                                <div style="margin-left: 5px;" class="row form-group">
                                    <button id="changePassButton" type="button" class="btn btn-default"><i class="fa fa-save"></i> Save </button>
                                </div>
                            </div>
                        </div>
                    </div>
           </div>
        </div>
    </div>

<!-- End of Wrapper for the sidebar -->
</div>

