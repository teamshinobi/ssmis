	<div class="modal fade" id="affiliationsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4>Affiliations</h4>
				</div>
				<div class="modal-body">
					<div class="portlet">
						<div class="portlet-header">
							<h2><i class="fa fa-group"></i><span class="break"></span>Affiliations</h2>
						</div>	
						<div class="portlet-content">
							<div id="affiliationsList" class="list-group">
								
							</div>
						</div>
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" id="closeAffiliationModal" class="btn btn-default" data-dismiss="modal">Close</button>		
				</div>
			
			</div>
		</div>
	</div>