	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				    <h4>Complete the form below before submitting...</h4>					
				</div>
				<div class="modal-body">
					<div class = "form-body-wall">
						<form id="add-police-form" method="POST" enctype="multipart/form-data">
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "form-label">Batch ID:</label>
								</div>
								<div class = "col-lg-4">
									<input name="pnp_id" type = "text" class = "form-control" placeholder = "Batch ID" required>
								</div>
								<div class = "col-lg-2">
									<label class = "form-label">Rank:</label>
								</div>
								<div class = "col-lg-4">
									<select name="rank" class = "form-control">
										<option>Police Officer 1</option>
										<option>Police Officer 2</option>
										<option>Police Officer 3</option>
										<option>Senior Police Officer 1</option>
										<option>Senior Police Officer 2</option>
										<option>Senior Police Officer 3</option>
										<option>Senior Police Officer 4</option>
										<option>Police Inspector</option>
										<option>Police Senior Inspector</option>
										<option>Police Chief Inspector</option>
										<option>Police Superintendent</option>
										<option>Police Senior Superintendent</option>
										<option>Police Chief Superintendent</option>
										<option>Police Director</option>
										<option>Police Deputy Director General</option>
										<option>Police Director General</option>
									</select>
								</div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "form-label">First Name:</label>
								</div>
								<div class = "col-lg-4">
									<input name="first_name" type = "text" class = "form-control" placeholder = "First Name" required>
								</div>
								<div class = "col-lg-2">
									<label class = "form-label">Member Since:</label>
								</div>
								<div class = "col-lg-4">
									<input id="member_since" name="member_since" type = "text" class = "form-control" placeholder = "Date" required>
								</div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "form-label">Middle Name:</label>
								</div>
								<div class = "col-lg-4">
									<input name="middle_name" type = "text" class = "form-control" placeholder = "Middle Name" required>
								</div>
								<div class = "col-lg-2">
									<label class = "form-label">Address:</label>
								</div>
								<div class = "col-lg-4">
									<textarea name="address" class="form-control" placeholder = "Complete Address" required></textarea>
								</div>
							</div>
							<div class = "row row-pad ">
								<div class = "col-lg-2">
									<label  class = "form-label">Last Name:</label>
								</div>
								<div class = "col-lg-4">
									<input name="last_name" type = "text" class = "form-control" placeholder = "Last Name" required>
								</div>
								<div class = "col-lg-2">
									<label class = "form-label">Email Address:</label>
								</div>
								<div class = "col-lg-4 form-group has-feedback">
									<input name="email" type = "email" class="form-control" placeholder = "Email Address" >
                                </div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "form-label">Status:</label>
								</div>
								<div class = "col-lg-4">
									<select name="status" class = "form-control">
										<option>Active</option>
										<option>Retired</option>
									</select>
								</div>
								<div class = "col-lg-2">
									<label class = "form-label">Contact Number:</label>
								</div>
								<div class = "col-lg-4">
									<input name="contact_number" type = "text" class="form-control" placeholder = "Contact Number" required>
								</div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "form-label">Gender:</label>
								</div>
								<div class = "col-lg-1">
									<div class="radio">
										<label><input name="gender"  type="radio" name="gender" value="1" checked>Male</label>
									</div>
								</div>
								<div  class="col-lg-1">
									<div class="radio">
										<label><input name="gender" type="radio" name="gender" value="0">Female</label>
									</div>
                                </div>				
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "form-label">Upload Picture:</label>
								</div>
								<div class = "col-lg-4">
									<input name="userfile" id="pnp_image_file" type = "file">
								</div>
							</div>	
							<div class = "row row-pad">	
								<div class = "col-lg-6 col-lg-offset-4">
									<img id="preview" class="solo-upload" src="<?php echo base_url() . "public/resources/photos/default_picture.jpg"?>"/>
								</div>
								<div class = "col-lg-6 col-lg-offset-4">
									<label id="preview-msg" style="margin-left: 90px; margin-top: 10px" class="form-label">Preview</label>
								</div>
							</div>	
					
					<!-- END of MODAL BODY -->
					</div>
					
					<div class="modal-footer">
						<button type="button" id="saveButton" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				</form>
				<!-- END BODY WALL -->
				</div>
			</div>
		</div>
	</div>	