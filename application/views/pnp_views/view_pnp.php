	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
			<h1>Profile<button style="float:right;margin-right:20px;" id="showUpdateFormButton" type="button" class="btn btn-default">Update Police</button> </h1>
		</div> <!-- #content-header -->	
		
		<div id="content-container">
            <div class="row">
                <div class="col-md-3 col-lg-3 side-info">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <img id="profile-pic" class="img-responsive img-thumbnail" 
                                 src="<?php echo base_url() . $police->file_path . '.jpg'; ?>"/>
                            <button id="uploadButton" type="button" class="btn btn-sm btn-default"><i class="fa fa-camera"></i> Change</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <p class="p-highlight">
                                <?php
                                    echo $police->first_name . ' ' . $police->last_name;
                                ?>
                                <span title="View full information" class="view"><i class="fa fa-eye"></i></span>
                            </p>
                            <p class="p-normal">
                                    <?php 
                                        if($police->gender === "1"){
                                             echo '<i class="fa fa-lg fa-male"></i>';
                                        } else {
                                             echo '<i class="fa fa-lg fa-female"></i>';
                                        }

                                    ?>
                                    <span id="pnp_id" style="margin-left: 5px;" class=""><?php echo '   ' . $police->pnp_id; ?></span>
                               
                            </p>
                            <p class="p-normal">
                                <span class="info-header"><i class="fa fa-star"> </i>&nbsp Rank </span><br>
                                <span class="info"><?php echo $police->rank; ?></span>
                            </p>
                            <p class="p-normal">
                                <span class="info-header"><i class="fa fa-beer"> </i>&nbsp Status </span><br>
                                <span class="info"><?php echo $police->status; ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <p class="p-highlight">
                                <?php
                                    echo 'Address';
                                ?>
                            </p>
                            <p class="p-normal">
                                <span class="info"><i class="fa fa-home"> </i><?php echo ' ' . $police->address; ?></span>
                            </p>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <p class="p-highlight">
                                <?php
                                    echo 'Contact Information';
                                ?>
                            </p>
                             <p class="p-normal">
                                <span class="info-header"><i class="fa fa-phone"> </i>&nbsp Phone/Cellphone </span><br>
                                <span class="info"><?php echo $police->contact_number; ?></span>
                            </p>
                            <p class="p-normal">
                                <span class="info-header"><i class="fa fa-google-plus"> </i>&nbsp Email </span><br>
                                <span class="info"><?php echo $police->email ?></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul id="myTab" class="nav nav-tabs">
                                <li class="active"><a href="#cases" data-toggle="tab"> Cases Handled</a></li>
                                <li><a href="#affiliations" data-toggle="tab">Affiliations</a></li>
                                <li><a href="#privileges" data-toggle="tab">Privileges</a></li>
                            </ul>
                             <div style="padding:10px;" id="myTabContent" class="tab-content">
                                <!-- Arrest Details Tab -->
                                <div class="tab-pane fade in active" id="cases">
                                    <h4> <i class="fa fa-search"></i>  As Investigator</h4>
                                    
                                    <hr/>
                                    
                                    <ul id="investigatorCases">
                                    
                                    </ul>
                                    
                                    
                                    <h4 style="margin-top: 50px;"> <i class="fa fa-bullhorn"></i>  As Arresting Officer</h4>
                                    <hr/>
                                    
                                    
                                    <ul id="arrestingCases">
                                    
                                    </ul>
                                </div>
                                <!-- Arrest Details Tab -->
                                <div class="tab-pane fade in" id="affiliations">
                                    <p> You can join this member to affiliations/groups on <a href="../groups">Group Module</a></p>
                                    <div id="affiliationsList" class="list-group">
								
                                    </div>
                                </div>
                                <!-- Inventory Details Tab -->
                                <div class="tab-pane fade in" id="privileges">
                                    <p id="p-message"> Privileges</p>
                                </div>
                            </div>
                        </div>         
                    </div>
                </div>
            </div>
            
		</div>	
	</div>
		   
<!-- End of Wrapper for the sidebar -->
</div>