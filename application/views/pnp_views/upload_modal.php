<div class="modal fade" id="uploadFormModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Upload</h4>
			</div>
			<div class="modal-body" id="inventory_form_div">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<input name="userfile" class="filestyle" data-size="sm" id="userfile" type = "file">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="uploadPhotoButton" class="btn btn-default">Upload</button>
				<button type="button" id="closeUploadForm"class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>