<div class="modal fade" id="updatePNPModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Update</h4>
                <p id="pnp_id_u_text" class="text-muted"></p>
            </div>
            <form id="update_pnp_form" method="POST">
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>Batch ID: </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <input id="pnp_id_u" type="text" class="form-control" name="pnp_id"/>
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <label>E-mail Address: </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <input id="email_u" type="text" class="form-control" name="email"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>First Name: </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <input id="first_name_u" type="text" class="form-control" name="first_name"/>
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <label>Last Name: </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <input id="last_name_u" type="text" class="form-control" name="last_name"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>Middle Name: </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <input id="middle_name_u" type="text" class="form-control" name="middle_name"/>
                    </div>
                    <div class="col-md-2 col-lg-2">
                       <label>Gender</label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <label class="radio-inline"><input type="radio" name="gender" value="1">Male</label>  
                        <label class="radio-inline"><input type="radio" name="gender" value="0">Female</label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>Rank: </label>
                    </div>
                    <div class="col-md-4 col-lg-4" id="rank_div">
                        <select id="rank_u" name="rank" class = "form-control">
                            <option value="Police Officer 1">Police Officer 1</option>
                            <option value="Police Officer 2">Police Officer 2</option>
                            <option value="Police Officer 3">Police Officer 3</option>
                            <option value="Senior Police Officer 1">Senior Police Officer 1</option>
                            <option value="Senior Police Officer 2">Senior Police Officer 2</option>
                            <option value="enior Police Officer 3">Senior Police Officer 3</option>
                            <option value="Senior Police Officer 4">Senior Police Officer 4</option>
                            <option value="Police Inspector">Police Inspector</option>
                            <option value="Police Senior Inspector">Police Senior Inspector</option>
                            <option value="Police Chief">Police Chief Inspector</option>
                            <option value="Police Superintendent">Police Superintendent</option>
                            <option value="Police Senior Superintendent">Police Senior Superintendent</option>
                            <option value="Police Chief Superintendent">Police Chief Superintendent</option>
                            <option value="Police Director">Police Director</option>
                            <option value="Police Deputy Director General">Police Deputy Director General</option>
                            <option value="Police Director General">Police Director General</option>
                        </select>
                    </div>
                    <div class="col-md-2 col-lg-2">
                            <label>Member since: </label>
                     </div>
                    <div class="col-md-4 col-lg-4">
                        <div id="member_since" class="input-group input-append date" data-date-start-view="2">
                            <input name="member_since" class="form-control" type="text" placeholder="Member Since"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                   <div class="col-md-2 col-lg-2">
                        <label>Status </label>
                    </div> 
                    <div class="col-md-4 col-lg-4" id="rank_div">
                        <select id="status_u" name="status" class = "form-control">
                            <option value="Active">Active</option>
                            <option value="Retired">Retired</option>
                            <option value="Dismissed">Dismissed</option>
                         </select>
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <label>Contact Number </label>
                    </div> 
                    <div class="col-md-4 col-lg-4">
                         <input id="contact_number_u" name="contact_number" class="form-control" type="text" placeholder="Contact Number"/>
                    </div>
                </div>
                <div class="row form-group">
                     <div class="col-md-2 col-lg-2">
                        <label>Complete Address </label>
                    </div> 
                    <div class="col-md-4 col-lg-4">
                         <input id="address_u" name="address" class="form-control" type="text" placeholder="Address"/>
                    </div>
                </div>
            </div>    
            <div class="modal-footer">
                <button type="button" id="closeUpdatePNPButton" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>  
            </form>
        </div>
    </div>
</div>