	<div id="content">
		<div id="content-header">
			<h1>My Messages&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button id="composeButton" type="button" class="toolbar-button btn btn-default" title="New Message">
					<i class="fa fa-pencil-square-o"></i>&nbsp;Compose</button></h1>
			
		</div> <!-- #content-header -->	
		<div id="content-container">
			
			<div class="row">
				<div class="col-md-8">
					<div class="row msgTitle">
						<div class="col-lg-4">
							<a id="inbox"><h4><i class="fa fa-inbox"></i>&nbsp;Inbox</h4></a>
						</div>
						<div class="col-lg-4">
							<a id="sentItems"><h4><i class="fa fa-share"></i>&nbsp;Sent Items</h4></a>
						</div>
						<div class="col-lg-3 col-lg-offset-1">
							<a id="drafts"><h4><i class="fa fa-folder-open-o"></i>&nbsp;Drafts</h4></a>
						</div>
					</div>
					<ul class="messagesList"></ul>
				</div>
				<div id="viewContent" class="col-md-4" style="display: none;">
					<div class="row">
						<div class="col-md-12 message dark">
							<div class="header">
								<div class="row subjectHeader">
									<div class="col-md-10"><h2 id="message-subject"></h2></div>
									<div class="col-lg-1">
										<button style="display: none;" class="btn btn-default close" id="continueMsg" type="button" title="Continue Message"><i class="fa fa-share"></i></button>
									</div>
									<div class="col-lg-1">
										<button class="btn btn-default close" id="deleteMsg" type="button" title="Delete Message"><i class="fa fa-trash"></i></button>
									</div>
								</div>
								<div class="row contentRow">
									<div class="from"><i class="fa fa-user"></i>&nbsp;<b id="message-recipient"></b></div>
									<div class="date"><b id="message-date"></b>&nbsp;<i class="fa fa-calendar"></i></div>
									<div class="from" id="receiver"><i class="fa fa-user"></i>&nbsp;<b id="message-sender"></b></div>
									<div class="date"><b id="message-time"></b>&nbsp;<i class="fa fa-clock-o"></i></div>
								</div>
							</div>
							<div class="content">
								<blockquote id="message-content"></blockquote>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="modal fade" id="composeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="message-form" method="POST" enctype="multipart/form-data">
					<div class="modal-header newMsg">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h2>New Message</h2>
						<h5><?php echo date('M d, Y')?><h5>
					</div>
					<div class="modal-body">
						<div class="row row-pad">
							<div class="col-md-1">
								<label class = "form-label">To:</label>
							</div>
							<div class = "col-lg-11">
								<input id="recipient" name="recipient" type = "text" class = "form-control">
								</div>
						</div>
						<div class="row row-pad">
							<div class = "col-lg-12">
								<input id="subject" name="subject" type = "text" class = "form-control" placeholder = "Subject" >
							</div>
						</div>
						<div class="row row-pad">
							<div class = "col-lg-12">
								<textarea rows="5" name="content"  id= "msgcontent" class="form-control" placeholder = "Message"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success" name="Sent" value="Sent">Send</button>
						<button type="submit" class="btn btn-warning" name="Draft" value="Draft">Save to Drafts</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="continueModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="continue-message-form" method="POST" enctype="multipart/form-data">
					<div class="modal-header newMsg">
						<button type="button" class="close" id="closeModal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h2>New Message</h2>
						<h5><?php echo date('M d, Y')?><h5>
					</div>
					<div class="modal-body">
						<div class="row row-pad">
							<div class="col-md-1">
								<label class = "form-label">To:</label>
							</div>
							<div class = "col-lg-11">
								<input id="recipient_c" name="recipient" type = "text" class = "form-control">
								</div>
						</div>
						<div class="row row-pad">
							<div class = "col-lg-12">
								<input id="subject_c" name="subject" type = "text" class = "form-control" placeholder = "Subject" >
							</div>
						</div>
						<div class="row row-pad">
							<div class = "col-lg-12">
								<textarea rows="5" name="content"  id= "msgcontent_c" class="form-control" placeholder = "Message"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success" name="Sent" value="Sent">Send</button>
						<button type="submit" class="btn btn-warning" name="Save" value="Save">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>