	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
			<h1>Groups</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
		
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() . "index.php/pnp"?>">PNP</a>
						</li>
						<li><a href="<?php echo base_url() . "index.php/pnp/groups"?>" class="active">Groups</a>
						</li>							
					</ol>
				</div>
			</div>
			<!-- Groups table to be DataTable -->
			<div class="portlet">
					<div class="portlet-header">
						<h2><i class="fa fa-group"></i><span class="break"></span>Groups</h2>
                        <ul class="portlet-tools pull-right">
                            <li>
                                <button title="New Group"  id="addButton" type="button" class="btn btn-md btn-default">
                                    <span><i class="fa fa-plus"></i></span>
                                </button>

                            </li>
                         </ul>
                    </div>
					<div class="portlet-content">
						<table id="groups_table" class="table table-striped table-bordered table-hover table-highlight table-checkabl">
							<thead>
								<tr>
									<th class="">Group ID</th>
									<th class="">Group Name</th>
									<th class="">Group Task</th>
									<th class="">Members</th>
								</tr>	
							</thead>
						</table>	
					</div>					
			</div>
		</div>	
	</div>
    <!-- /#page-content-wrapper -->
	   
<!-- End of Wrapper for the sidebar -->
</div>