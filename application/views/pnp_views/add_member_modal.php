	<div class="modal fade" id="addMemberModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4>Click a member to add in the group.</h4>
				</div>
				<form id="edit-group-form" method="POST">
					<div class="modal-body">
						<div class="portlet">
							<div class="portlet-header">
								<h2><i class="fa fa-group"></i><span class="break"></span>Our Policemen</h2>
							</div>	
							<div class="portlet-content">
								<table id="police_table"  class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
									<thead>
										<tr>											
											<th class="">Batch ID</th>
											<th class="">Rank</th>
											<th class="">Full Name</th>
											<th class="">Picture</th>
										</tr>	
									</thead>
								</table>
							</div>
						</div>
					</div>	
					<div class="modal-footer">
						<button type="button" id="" class="btn btn-default" data-dismiss="modal">Exit</button>
					</div>
				</form>
			</div>
		</div>
	</div>