<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4><i class="fa fa-plus"></i> New Group</h4>
			</div>
			<form id="new-group-form" method="POST"> 
				<div class="modal-body">
			
					<div class="account-wall">
						<div class="row row-pad">
							<div class = "col-lg-4">
							<label>Group ID: </label>
							</div>
							<div class = "col-lg-8">
								<input name="group_id" type="text" class="form-control"
									placeholder="Group ID" required/>
							</div>
						</div>
						<div class="row row-pad">
							<div class = "col-lg-4">
							<label>Group Name: </label>
							</div>
							<div class = "col-lg-8">
								<input name="group_name" type="text" class="form-control"
									placeholder="Group Name" required/>
							</div>
						</div>
						<div class="row row-pad">
							<div class = "col-lg-4">
							<label>Group Task: </label>
							</div>
							<div class = "col-lg-8">
								<textarea name="group_task" class="form-control" placeholder = "Describe what this group is all about." required></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="saveButton" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add Group</button>
				</div>
			</form>
		</div>
	</div>
</div>	