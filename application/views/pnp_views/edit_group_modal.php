	<div class="modal fade" id="editGroupModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4>Edit Group...</h4>
				</div>
				<form id="edit-group-form" method="POST">
					<div class="modal-body">
						<div class="portlet">
							<div class="portlet-header">
								<h2><i class="fa fa-edit"></i><span class="break"></span>Edit</h2>
							</div>	
						<div class="portlet-content">
							<div class="row row-pad">
								<div class = "col-lg-4">
									<label>Group ID: </label>
								</div>
								<div class = "col-lg-8">
									<input value="<?php echo $group[0]->group_id ?>" name="group_id" type="text" class="form-control"
										placeholder="Group ID" readonly/>
								</div>
							</div>
							<div class="row row-pad">
								<div class = "col-lg-4">
								<label>Group Name: </label>
								</div>
								<div class = "col-lg-8">
									<input value="<?php echo $group[0]->group_name ?>" name="group_name" type="text" class="form-control"
										placeholder="Group Name" required/>
								</div>
							</div>
							<div class="row row-pad">
								<div class = "col-lg-4">
								<label>Group Task: </label>
								</div>
								<div class = "col-lg-8">
									<textarea rows="10" name="group_task" class="form-control" placeholder = "Describe what this group is all about." required><?php echo $group[0]->group_task ?>			
									</textarea>
								</div>
							</div>
						</div>
						</div>
					</div>	
					<div class="modal-footer">
						<button type="button" id="" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>