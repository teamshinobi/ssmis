
<h3 class="">Control Panel</h3>

<p>Control Panel let's you control some aspects of the system such as 
allowing or disallowing creation of particular forms and reports.</p>

<br><br>

<div class="row">
    <div class="col-lg-8 col-md-8">
        <h4>Allow creation of Pre-operation and Coordination</h4> 
    </div>
    <div class="col-lg-1 col-lg-offset-3">
        <input type="checkbox" id="allowPreopCoord"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-8 col-md-8">
        <h4>Allow creation of Affidavits</h4> <br>   
    </div>
    <div class="col-lg-1 col-lg-offset-3">
        <input type="checkbox" id="alowAffidavits"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-8 col-md-5">
        <h4>Automatic Archiving Schedule</h4> 
        <p class="text-muted">( Automatically archive closed cases that are 5 years old. ) </p>
    </div>
    <div class="col-lg-2 col-lg-offset-2">
        <select id="archivingSchedule" class="form-control">
            <option value="Daily">Daily</option>
            <option value="Monthly">Monthly</option>
            <option value="Yearly">Yearly</option>
        </select>
    </div>
</div>