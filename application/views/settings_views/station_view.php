
<h3 class="">Station Settings</h3>

<p>Modify general information about your station.</p>

<br><br>

<form id="station_form" method="POST">
    <div class="row">
        <div class="col-lg-2 col-md-2">
           <label>Address</label>
        </div>
        <div class="col-lg-6 col-md-6">
           <input type="text" id="address" name="address" class="form-control" placeholder="Complete address"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2">
           <label>SAID-SOTG Chief</label>
        </div>
        <div class="col-lg-6 col-md-6">
           <input type="text" name="chief" id="chief" class="form-control" placeholder="Rank/First Name/Last Name"/>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2">
           <button type="submit" class="btn btn-default"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
</form>