	<div class="modal fade" id="viewRaModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-title" id="myModalLabel">
					<h4><i class="fa fa-lg fa-book"></i> Information about the Charge</h4>
					<p id = "charge_id_view"> Barangay ID</p>
				</div>
				</div>
				<div class="modal-body">
				<form id="update-ra-form" method="POST">
								
							<div class = "row row-pad">
								
								<div class = "col-lg-2">
									<label class = "label-pad">Article:</label>
								</div>
								<div class = "col-lg-4">
									<input name="article"  id = "article" type = "text" class = "form-control" placeholder = "Article of the law" required>	
								</div>
								<div class = "col-lg-2">
									<label class = "label-pad">Section:</label>
								</div>
								<div class = "col-lg-4">
									<input name="section" id = "section" type = "text" class = "form-control" placeholder = "Section of the law" required>
								</div>

							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "label-pad">Fine:</label>
								</div>
								<div class = "col-lg-4">
									<input name="fine" id = "fine" type = "text" class = "form-control" placeholder = "Fine" required>
								</div>
								<div class = "col-lg-2">
									<label class = "label-pad">Imprisonment:</label>
								</div>
								<div class = "col-lg-4">
									<input name="imprisonment" id = "imprisonment" class="form-control" placeholder = "Time of Imprisonment" required>
								</div>
							
							</div>
					
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "label-pad">Description:</label>
								</div>
								<div class = "col-lg-10">
									<textarea name="description" id = "description" type = "text" class = "form-control" placeholder = "Description" required></textarea>
								</div>
								
							</div>
					
							
					
							
						
					
				<!-- END of MODAL BODY -->
				</div>
				
				<div class="modal-footer">
					<button type="button" id="saveButton" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
				</form>
			</div>
		</div>
	</div>	