	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="modal-title" id="myModalLabel">
						<h4><i class="fa fa-lg fa-user"></i> Record New Barangay Information</h4>
						<p id ="barangay_increment" class = "text-muted"> Barangay ID</p>
					</div>
				</div>
				<div class="modal-body">
				<form id="add-barangay-form" method="POST">
								
							<div class = "row row-pad">
								
								<div class = "col-lg-2">
									<label class = "label-pad">Barangay name:</label>
								</div>
								<div class = "col-lg-4">
									<input  name = "barangay_name" type = "text" class = "form-control" placeholder = "Name of Barangay" required>	
								</div>
								<div class = "col-lg-2">
									<label class = "label-pad">City:</label>
								</div>
								<div class = "col-lg-4">
									<input name = "city"  type = "text" class = "form-control" placeholder = "City name" >
								</div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "label-pad">Barangay Incidents:</label>
								</div>
								<div class = "col-lg-4">
									<input  name = "barangay_incidents" type = "text" class = "form-control" placeholder = "Incidents" required>	
								</div>
								<div class = "col-lg-2">
									<label class = "label-pad">Barangay Category:</label>
								</div>
								<div class = "col-lg-4">
									<input name = "barangay_category"  type = "text" class = "form-control" placeholder = "Category of Barangay" required>	
								</div>
							</div>
							<div class = "row row-pad">
								<div class = "col-lg-2">
									<label class = "label-pad">Brgy Chairman:</label>
								</div>
								<div class = "col-lg-10">
									<input name="barangay_chairman" type = "text" class = "form-control" placeholder = "Name of Chairman" required>
								</div>
							</div>
					

					
				<!-- END of MODAL BODY -->
				</div>
					
					<div class="modal-footer">
						<button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" id="saveButton" class="btn btn-primary saveButton">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>	