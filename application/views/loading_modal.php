<div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Loading...</h4>
				<p class="text-muted" id="loadingMessage">Please wait while the system is completing the request.</p>
			</div>
			<div class="modal-body">
				<img style="width:75px;height:75px;margin-left:280px;margin-top:50px;margin-bottom:50px;" src="<?php echo base_url() . 'public/resources/design_photos/loading.gif'?>" />
			</div>
		</div>
	</div>
</div>