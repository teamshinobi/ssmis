    <div id="content">
        <div id="content-header">
            <h1>Coordination Form <button style="float:right;margin-right:20px;" id="viewCoordinations" type="button" class="btn btn-default"><i class="fa fa-file"></i> View coordination forms</button> </h1> 
            <p style="float:right; margin:10px;"class="text-muted" id="coord_id">Please wait for ID.    </p>
        </div> 
        <form id="save_coordination_form" method="POST">
        <div id="content-container">              
            <div style="margin-top: 50px;" class="row form-group">
                <div class="col-lg-2 col-md-2">
                    <label>Control No: </label>
                </div> 
                <div class="col-lg-4 col-md-4">
                    <input name="control_no" type="text" class="form-control" disabled/>
                </div>  
                <div class="col-lg-2 col-md-2">
                    <label>Date: </label>
                </div> 
                <div class="col-lg-4 col-md-4">
                    <div id="datepicker" class="input-group date datepicker">
						<input name="date" id="date" class="form-control" type="text" placeholder="Date"/>
						<span class="input-group-addon"><i class="fa fa-calendar" required></i></span>
                    </div>
                </div>  
            </div>
            <div class="row form-group">
                <div class="col-lg- col-md-2">
                    <label>I. UNIT/OFFICE: </label>
                </div> 
                <div class="col-lg-10 col-md-10">
                    <select id="unit" name="unit" class="form-control">
                       
                    </select>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg- col-md-2">
                    <label>II. TYPE OF OPERATION: </label>
                </div> 
                <div class="col-lg-4 col-md-4">
                    <select name="type_of_operation" class="form-control">
                        <option>Buy-Bust</option>
                        <option>Search Warrant</option>
                        <option>Surveillance</option> 
                        <option>Casing</option>
                        <option>Test-Buy</option>
                    </select>
                </div>            
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>III. DURATION</label>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-2 col-md-2">
                    <label>From</label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <div class="input-group bootstrap-timepicker">
						<input name="duration_from_time" id="timepicker_from" type="text" class="form-control" placeholder="Duration from">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>
                </div>
                <div class="col-lg-2 col-md-2">
                    <label>To</label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <div class="input-group bootstrap-timepicker">
						<input name="duration_to_time" id="timepicker_to" type="text" class="form-control" placeholder="Duration to">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-4 col-md-4 col-lg-offset-2 col-md-offset-2">
                     <div id="duration_from_date" class="input-group date datepicker">
						<input name="duration_from_date" id="duration_from" class="form-control" type="text" placeholder="Date"/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-lg-offset-2 col-md-offset-2">
                     <div id="duration_to_date" class="input-group date datepicker">
						<input name="duration_to_date" id="duration_to" class="form-control" type="text" placeholder="Date"/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            
             <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>IV. AREA OF OPERATION</label>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-7 col-md-7">
                   <input name="area_of_operation" class="form-control"/>
                </div>
            </div>
             <div class="row form-group">
                <div class="col-lg-2 col-md-2">
                    <label>V. TEAM LEADER
                    </label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <input id="team_leader" type="text" class="form-control"/>
                </div>
                <div class="col-md-1 col-lg-1">
                    <span class="input-group-btn">
					  <button id="addTeamLeaderButton" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Choose Leader</button>
                    </span>
                </div
                </div>
            </div>
            <div class="row form-group" style="border: 1px solid black; padding-top: 20px; margin-left:40px; margin-right:40px;">
                <div class="col-lg-6 col-md-6 col-lg-offset-2 col-md-offset-2">
                    <div id="membersList" class="list-group">
				        <li class="list-group-item"> No police yet. </li>
                    </div> 
                </div>
                <div class="col-lg-1 col-md-1">
                    <button id="addMembersButton" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add Member</button>
                </div>
            </div>    
            <div class="row form-group">
                <div class="col-lg-2 col-md-2">
                    <label>VI. VEHICLES USED
                    </label>
                </div>
                <div class="col-md-1 col-lg-1">
                    <span class="input-group-btn">
					  <button title="Add Vehicle" id="addVehicleButton" type="button" class="btn btn-default"><i class="fa fa-plus"></i> Add Vehicle</button>
                    </span>
                </div>
            </div>  
            <div class="row form-group" style="border: 1px solid black; padding-top: 20px; margin-left:40px; margin-right:40px;">
                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table id="vehicles_tbl_r" class="table table-striped table-bordered table-hover table-highlight table-checkable">
                            <thead>
                                <tr>
                                    <th class="">Type</th>
                                    <th class="">Make</th>
                                    <th class="">Color</th>
                                    <th class="">Plate No.</th>
                                    <th style="width: 6%" class="">Action</th>
                                </tr>	
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
				    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>VII. COORDINATION MADE BY: </label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <input data-value="<?php echo $this->session->userdata('username');?>" id="created_by" type="text" class="form-control" value="<?php echo $created_by; ?>"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>VIII. COORDINATION RECEIVED BY: </label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <input name="received_by" type="text" class="form-control"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <button style="float:right;" type="submit" class="btn btn-md btn-default"><i class="fa fa-save"></i> Save</button>
                </div> 
            </div>
        </div>
        </form>
            
        <div class="row" id="disallowed">
            <div class = "col-md-3 col-md-offset-5">
                 <img src="<?php echo base_url() . 'public/resources/design_photos/disallow.jpg' ?>">
                 <p style="margin-left: 25px;"> Currently Disallowed.</p>
            </div>	
       </div>
    </div>       
</div>