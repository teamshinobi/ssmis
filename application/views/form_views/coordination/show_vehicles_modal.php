<div class="modal fade" id="addVehicleModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-car"></i> Add Vehicle</h4>
			</div>
            <div class="modal-body">
                <div class="portlet-content">
                  	<div class="table-responsive">
                        <table id="vehicles_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable">
                            <thead>
                                <tr>
                                    <th class="">Type</th>
                                    <th class="">Make</th>
                                    <th class="">Color</th>
                                    <th class="">Plate No.</th>
                                    <th class="">Image</th>
                                    <th class="">Action</th>
                                </tr>	
                            </thead>
                        </table>
                    </div>
                </div>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>
