<div class="modal fade" id="addMembersModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Add Member</h4>
			</div>
            <div class="modal-body">
                <div class="portlet-content">
                    <table id="members_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
                        <thead>
                            <tr>
                                <th class="">Batch ID</th>
                                <th class="">Rank</th>
                                <th class="">Full Name</th>
                                <th class="">Picture</th>
                                <th class="">Choose</th>
                            </tr>	
                        </thead>
                    </table>	
                </div>
            </div> 
            <div class="modal-footer">
                <button type="button" id="closeAddDeliveryModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>
