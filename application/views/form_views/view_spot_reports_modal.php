<div class="modal fade" id="showSpotReportsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Spot Reports</h4>
                <p>Attach a SPOT Report on Case Report Module <br/>
                
                
			</div>
            <div class="modal-body">
                 <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive">
                            <table id="spot_reports_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                                <thead>
                                    <tr>
                                        <th class="">ID</th>
                                        <th class="">Delivered By </th>
                                        <th class="">Created By </th>
                                        <th class="">Date Created </th>
                                        <th class="">Attached</th>
                                        <th class="">Action </th>
                                    </tr>	
                                </thead>				
                            </table>		
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
               	
                 <button type="button" id="closeViewPreoperationsModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>