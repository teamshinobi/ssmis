    
<div class="portlet">
		<div class="portlet-header">
			<h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Main Affidavit</h2>
            
		</div>	
		<div class="portlet-content">
            <form id="main_affidavit_form">
                <div class="row" style="padding: 0 0 0 0;">
                    <div class="col-md-6">
                         <div class="list-group narratorList" id="narratorList">
                            <li class="list-group-item"> No police yet. </li>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button id="addNarratorButton" type="button" class="btn btn-default addNarratorButton"><i class="fa fa-plus"></i> Add</button>
                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-default " style="float:right;"><i class="fa fa-save"></i> Save Main Affidavit</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div id="mainContent" class="summernote" >
                            
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>