<div class="modal fade" id="showAffidavitsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Affidavits Forms</h4>
                <p>Attach a Affidavit Form on Case Report Module <br/>   
			</div>
           
            <div class="modal-body">
                
                <div class="row">
                    <div class = "col-lg-12">
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#mainView" data-toggle="tab">Main Affidavit</a></li>
                            <li><a href="#supplementalView" data-toggle="tab">Supplemental Affidavit</a></li>

                         </ul>
                    </div>	
                    <div id="myTabContent" class="tab-content">

                        <div class="tab-pane fade in active" id="mainView" value="mainView">
                            <?php $this->load->view('form_views/affidavits/main_affidavits_view.php'); ?>
                        </div>

                        <div class="tab-pane fade in" id="supplementalView" value="supplementalView">
                            <?php $this->load->view('form_views/affidavits/supplemental_affidavit_view.php'); ?>
                        </div>

                    </div>
                </div>              
            </div>
            <div class="modal-footer">	
                 <button type="button" id="closeAffidavitsModal" class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>