<div class="portlet">
    <div class="portlet-header">
        <h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Supplemental Affidavit</h2>
    </div>	
    <div class="portlet-content">
        <div class="row" id="supplementalAffidavitsView">
            <div class="col-lg-12 col-md-12">
                <div class="table-responsive">
                    <table id="supplemental_affidavits_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                        <thead>
                            <tr>
                                <th class="">ID</th>
                                <th class="">Date</th>
                                <th class="">Attached</th>
                                <th class="">Narrators</th>
                                <th class="">Action </th>
                            </tr>	
                        </thead>				
                    </table>		
                </div>
            </div>
        </div>
    </div>
</div>