<div class="portlet">
    <div class="portlet-header">
        <h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Supplemental Affidavit</h2>
    </div>	
    <div class="portlet-content">
        <form id="supplemental_affidavit_form">
            <div class="row" style="padding: 0 0 0 0;">
                    <div class="col-md-6">
                         <div class="list-group narratorList" id="supplementalNarratorList">
                            <li class="list-group-item"> No police yet. </li>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button id="addNarratorButton" type="button" class="btn btn-default addNarratorButton"><i class="fa fa-plus"></i> Add</button>
                    </div>
                    <div class="col-md-5">
                        <button type="submit" class="btn btn-default" style="float:right;">
                            <i class="fa fa-save"></i> Save Supplemental Affidavit</button>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div id="suppContent" class="summernote">
                        
                        <div><br></div>
                        <div><div style="line-height: 22.1px;"><br></div>
                        <div style="line-height: 22.1px;"><p><br class="removedTags"></p>
                        <p><br class="removedTags"></p>
                        <p><br class="removedTags"></p>
                        <p><br class="removedTags"></p>
                        <p>SINUMPAAN AT PINATOTOHANAN&nbsp; sa harap ko ngayon ika-&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp; ng&nbsp;August 2015. Pinatututnayan ko na ang nagsasalaysay ay aking nasiyasat at ako ay may sapat na paniniwala na siya ay kusa at malayang nagsalaysay.</p>
                        <div><br></div>
                        </div>
                        </div>


                    </div>
                </div>
                </div>
            </div>
            
        </form>
    </div>
</div>