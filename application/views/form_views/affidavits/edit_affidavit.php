	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
			<h1><i class="fa fa-edit"></i> Edit Affidavit</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
			<div class="row">
                <div class="col-lg-3 col-lg-offset-9">
                    <p>
                        <span id="affidavit_id">

                            <?php 
                                echo $id;
                            ?>

                        </span>
                        &nbsp;(
                        <span id="type">

                            <?php 
                                echo $type;
                            ?>

                        </span>
                        )
                    </p>
                </div>
                   
            </div>
            <div class="row">
                <div class="col-md-6">
                     <div class="list-group narratorList" id="supplementalNarratorList">
                        <li class="list-group-item"> No police yet. </li>
                    </div>
                </div>
                <div class="col-md-1">
                    <button id="addNarratorButton" type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Add</button>
                </div>
               
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div id="affContent" class="summernote">


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <button id="saveButton" type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-save"></i> Save </button>
                </div>
            </div>
		</div>
	</div>
<!-- End of Wrapper for the sidebar -->
</div>