    
<div id="content">
        <div id="content-header">
            <h1>Affidavits<button style="float:right;margin-right:20px;" id="viewAffidavits" type="button" class="btn btn-default"><i class="fa fa-file"></i> View Affidavit Forms</button></h1> 
        </div>
        <div id="content-container">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb affidavitsTab">
						<li><a href="<?php echo base_url() . "forms/affidavits"?>">Affidavits</a></li>	
                        
                        <p style="float:right;"class="text-muted" id="aff_id">Please wait for ID.    </p>
					</ol>
				</div>
			</div>	

			<div class="row" id="affidavitMain">
				<div class = "col-lg-12">
					<ul id="myTab" class="nav nav-tabs">
						<li class="active"><a href="#main" data-toggle="tab">Main Affidavit</a></li>
						<li><a href="#supplemental" data-toggle="tab">Supplemental Affidavit</a></li>
						
					 </ul>
				</div>	
				<div id="myTabContent" class="tab-content">
					
					<div class="tab-pane fade in active" id="main" value="main">
						<?php $this->load->view('form_views/affidavits/main_affidavit.php'); ?>
					</div>
					
					<div class="tab-pane fade in" id="supplemental" value="supplemental">
						<?php $this->load->view('form_views/affidavits/supplemental_affidavit.php'); ?>
					</div>
					
				</div>
			</div>
            <div class="row" id="affidavitDisallowed">
                <div class = "col-md-3 col-md-offset-5">
                     <img src="<?php echo base_url() . 'public/resources/design_photos/disallow.jpg' ?>">
                     <p style="margin-left: 25px;"> Currently Disallowed.</p>
                </div>	
           </div>
		</div>	
    </div>       
</div>