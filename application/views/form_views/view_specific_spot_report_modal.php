<div class="modal fade" id="viewSpecificSpotReportModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4>Spot Report Details</h4>
                <p>
                    <span id="d_b"></span></br>
                    <span id="c_b"></span>
                </p>
                  
			</div>
            <div class="modal-body">
                <div style="margin:0px;" id="content">


                </div>          
            </div>
            <div class="modal-footer">
                 <button type="button" id="closeSpecificSpotReportModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>