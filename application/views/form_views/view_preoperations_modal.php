<div class="modal fade" id="viewPreoperationsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Reports</h4>	
                <p class="text-muted">
                    Associate the following pre-cooperation report in case report module. <br/>
                    NOTE: You cannot attach a pre-operation report in this module
                    since pre-operation report must be done first before case reports.
                
                </p>
			</div>
            <div class="modal-body">
                <div>
                    <div>
                       <p>Type "approve" to see approved pre-operations report and "not" to see unapproved.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive">
                            <table id="preoperations_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                                <thead>
                                    <tr>
                                        <th class="">ID</th>
                                        <th class="">Control #</th>
                                        <th class="">Date</th>
                                        <th class="">Time</th>
                                        <th class="">Created by</th>
                                        <th class="">Summary</th>
                                        <th class="">Attached To</th>
                                       
                                    </tr>	
                                </thead>				
                            </table>		
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
               	
                 <button type="button" id="closeViewPreoperationsModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>