<div class="modal fade" id="importTargetModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-arrows"></i>&nbsp;Import</h4>
			</div>
            <div class="modal-body">
                <div class="portlet-content">
                    <table id="imports_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
                        <thead>
                            <tr>
                                <th class="">ID</th>
                                <th class="">Fullname</th>
                                <th class="">Nationality</th>
                                <th class="">Alias</th>
                                <th class="">Add</th>
                            </tr>	
                        </thead>
                    </table>	
                </div>
            </div> 
            <div class="modal-footer">
                <button type="button" id="closeImportTargetModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>
