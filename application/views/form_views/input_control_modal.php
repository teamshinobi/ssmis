<div class="modal fade" id="showControlInputModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Control Number</h4>	
               
			</div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-lg-offset-2">
                        <label>Control Number </label>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <input name="cn" id="cn" class="form-control" type="text" placeholder="Enter Control Number"/>
                    </div>              
                </div>
            </div>
            <div class="modal-footer">
                 <button type="button" id="continueApprovingButton" class="btn btn-default" data-dismiss="modal">Continue</button>
                 <button type="button" id="closeShowControlInputModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>