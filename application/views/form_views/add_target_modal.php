<div class="modal fade" id="addTargetPersonModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Target Person</h4>	
                <p id="target_id" class="text-muted">Target ID did not load correctly. Refresh the page.</p>
			</div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>Full name: </label>
                    </div>
                     <div class="col-md-10 col-lg-10">
                         <input id="full_name" class="form-control"/>
                    </div> 
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>Alias (A.K.A): </label>
                    </div>
                     <div class="col-md-10 col-lg-10">
                         <input id="alias" class="form-control"/>
                    </div> 
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2">
                        <label>Nationality: </label>
                    </div>
                    <div class="col-md-3 col-lg-3">
                         <label class="checkbox-inline"><input id="is_filipino" name="is_filipino" type="checkbox" value="1" checked>Filipino</label>
                    </div>
                     <div class="col-md-3 col-lg-3">
                         <label> Others (Specify): </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                          <input id="others_nationality" class="form-control"/>
                    </div>
                </div>
            </div>
             <div class="modal-footer">
                  <button type="button" id="addTargetPersonButton" class="btn btn-default">Add Target</button>	
                 <button type="button" id="closeTargetPersonModal"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>
