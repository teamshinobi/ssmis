    <div id="content">
        <div id="content-header">
            <h1><i class="fa fa-edit"></i> Pre-operation Report</h1> 
            <p style="float:right; margin:10px;"class="text-muted" id="preoperation_id"><?php echo $id ?></p>
        </div> 
        <form id="update_precooperation_form" method="POST">
        <div id="content-container">
            <div style="margin-top: 50px;" class="row form-group">
                <div class="col-lg-2 col-md-2">
                    <label>Control No: </label>
                </div> 
                <div class="col-lg-4 col-md-4">
                    <input id="control_no" name="control_no" type="text" class="form-control"/>
                </div>  
                <div class="col-lg-1 col-md-1">
                    <label>Date: </label>
                </div> 
                <div class="col-lg-3 col-md-3">
                    <div id="datepicker" class="input-group date datepicker">
						<input name="date" class="form-control" type="text" placeholder="Date"/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>  
                  <div class="col-lg-2 col-md-2">
                    <div class="input-group bootstrap-timepicker">
						<input name="time" id="timepicker_time" type="text" class="form-control" placeholder="Time">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>	
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>I. SUMMARY OF INFORMATION </label>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-6 col-md-6">
                  <textarea name="summary" class="form-control" rows="2" id="summary"></textarea>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>II. COURSE OF ACTION</label>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-6 col-md-6">
                    <select id="course_of_action" name="course_of_action" class="form-control">
                        <option value="Buy-Bust">Buy-Bust</option>
                        <option value="Search Warrant">Search Warrant</option>
                        <option value="Casing">Casing</option>
                        <option value="Test">Test</option>
                    </select>
                </div> 
                <div class="col-lg-2 col-md-2">
                    <label>Others (Specify): </label>
                </div>
                 <div class="col-lg-4 col-md-4">
                   <input name="others" class="form-control"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>III. DURATION</label>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-2 col-md-2">
                    <label>From</label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <div class="input-group bootstrap-timepicker">
						<input name="duration_from_time" id="timepicker_from" type="text" class="form-control" placeholder="Duration from">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>
                </div>
                <div class="col-lg-2 col-md-2">
                    <label>To</label>
                </div> 
                <div class="col-md-4 col-lg-4">
                    <div class="input-group bootstrap-timepicker">
						<input name="duration_to_time" id="timepicker_to" type="text" class="form-control" placeholder="Duration to">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-4 col-md-4 col-lg-offset-2 col-md-offset-2">
                     <div id="duration_from_date" class="input-group date datepicker">
						<input name="duration_from_date" class="form-control" type="text" placeholder="Date"/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-lg-offset-2 col-md-offset-2">
                     <div id="duration_to_date" class="input-group date datepicker">
						<input name="duration_to_date" class="form-control" type="text" placeholder="Date"/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>IV. TARGET LOCATION</label>
                </div> 
            </div>
            <div class="row form-group">
                <div class="col-lg-6 col-md-6">
                   <input name="target_location" class="form-control"/>
                </div>
            </div> 
            <div class="row form-group">
                <div class="col-lg-3 col-md-3">
                    <label>V. TARGET PERSONS: </label>
                </div>
               
            </div>
            
            <div class="row form-group">
                <div class="col-lg-6 col-md-6">
                    <div id="targetsList" class="list-group">
				        <li class="list-group-item"> No target persons yet.</li>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1">
                    <button id="addTargetButton" type="button" class="btn btn-sm btn-default"><i class="fa fa-plus"></i>&nbsp;Add</button>
                </div>
                <div class="col-lg-1 col-md-1">
                    <button id="importTargetButton" type="button" class="btn btn-sm btn-default"><i class="fa fa-arrows"></i>&nbsp;Import</button>
                </div>
            </div>
            
             <div class="row form-group">
                <div class="col-lg-12 col-md-12">
                    <button style="float:right;" type="submit" class="btn btn-md btn-default"><i class="fa fa-save"></i> Save</button>
                </div> 
            </div>
        </div>
        </form>
    </div>       
</div>