    <div id="content">
        <div id="content-header">
            <h1>Spot Report <button style="float:right;margin-right:20px;" id="viewSpotReports" type="button" class="btn btn-default"><i class="fa fa-file"></i> View SPOT reports</button> </h1> 
            <p style="float:right; margin:10px;"class="text-muted" id="spot_id">Please wait for ID.    </p>
        </div>
        <div id="content-container">
            <form id="save_spot_report_form">
            <div class="row form-group">
                <div class="col-md-1 col-lg-1">
                    <label> Delivered by: </label>
                </div>
                <div class="col-md-4 col-lg-4">
                    <input id="delivery_boy_name" type="text" class="form-control"/>
                </div>
                <div class="col-md-1 col-lg-1">
                    <span class="input-group-btn">
					  <button id="showDeliveryBoysButton" type="button" class="btn btn-default"><i class="fa fa-plus"></i></button>
                    </span>
                </div>
            </div>
        
            <div class="row" style="padding-top: 20px;">
                <div class="col-md-12 col-lg-12">
                    <div id="content" class="summernote" >
                    
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-2 col-md-2 col-lg-offset-10 col-md-offset-10">
                    <button type="submit" class="btn btn-default"><i class="fa fa-save"></i> Save Spot Report</button>
                </div>
            </div>
            </form>
        </div> 
    </div>       
</div>