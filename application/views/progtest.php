	<div id="content">
		<div id="content-header">
			<h1>My Profile</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-4 col-sm-5 text-center">
							<div class="thumbnail">
								<div class="caption">
									<button id="viewProfile" type="button" class="label label-default toolbar-button btn btn-default ">View
									</button>									
									<button id="updateProfile" type="button" class="label label-default toolbar-button btn btn-default ">Update
									</button>
								</div>
								<img id ="profilePicture" src="<?php echo base_url() . $profile[0]->file_path . '.jpg' ?>" alt="Profile Picture" />
							</div>						
						</div>
						<div class="col-md-8 col-sm-7">
							<h2><?php echo $profile[0]->last_name . ', ' . $profile[0]->first_name . ' ' . $profile[0]->middle_name;?></h2>
							<h4><?php echo $profile[0]->rank; ?></h4>
							<hr />
							<ul class="icons-list">
								<li><i class="icon-li fa fa-envelope"></i><?php echo $profile[0]->contact_number; ?></li>
								<li><i class="icon-li fa fa-globe"></i><?php echo $profile[0]->email; ?></li>
								<li><i class="icon-li fa fa-map-marker"></i><?php echo $profile[0]->address; ?></li>
                                <li><i class="icon-li fa fa-gear"></i><a href="<?php echo base_url() . 'account'?>">Change Credentials</a></li>
							</ul>
							<hr />
						</div>
					</div>
				</div>
			</div> <!-- /.row -->
            <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul id="myTab" class="nav nav-tabs">
                                <li class="active"><a href="#cases" data-toggle="tab"> Cases Handled</a></li>            
                            </ul>
                             <div style="padding:10px;" id="myTabContent" class="tab-content">
                                <!-- Arrest Details Tab -->
                                <div class="tab-pane fade in active" id="cases">
                                    <h4> <i class="fa fa-search"></i>  As Investigator</h4>
                                    
                                    <hr/>
                                    
                                    <ul id="investigatorCases">
                                    
                                    </ul>
                                    
                                    
                                    <h4 style="margin-top: 50px;"> <i class="fa fa-bullhorn"></i>  As Arresting Officer</h4>
                                    <hr/>
                                    
                                    
                                    <ul id="arrestingCases">
                                    
                                    </ul>
                                </div>
                            </div>
                        </div>         
                    </div>
			
		</div> <!-- /#content-container -->
		
	</div> <!-- #content -->
	
	<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row row-pad">
						<div class="col-lg-12">
							<img id="imageProfile" src="<?php echo base_url() . $profile[0]->file_path ?>" alt="Profile Picture" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form id="update-police-photo" method="POST" enctype="multipart/form-data">
						<div class="row row-pad">
							<div class="col-lg-4 col-lg-offset-3">
								<img id="profilePreview" class="solo-upload" src="<?php echo base_url() . "public/resources/photos/default_picture.jpg"?>">
							</div>
						</div>
						<div class="row row-pad">
							<div class="col-md-12">
								<input style="margin-top:40px;" name="userfile" class="filestyle" data-size="sm" id="profile_image_file"
									type = "file">
							</div>
						</div>
						<div class="row row-pad">
							<div class="col-md-12">
								<button id="takePhoto" type="button" class="toolbar-button btn btn-default fa fa-camera">&nbsp;&nbsp;Take a Picture</button>
							</div>
						</div>
						<div class="row row-pad">
							<div class="col-lg-6">
								<button id="updateProfilePicture" type="submit" class="toolbar-button btn btn-default fa fa-picture-o">&nbsp;&nbsp;Update Picture</button>
							</div>
							<div class="col-lg-6">
								<button id="cancelButton" type="button" class="toolbar-button btn btn-primary" data-dismiss="modal" aria-hidden="true">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="takePhotoModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button id="closeButton" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4>Take Photo</h4>
				</div>
				<div class="modal-body">
					<div class="row row-pad">
						<div class="col-md-4 col-md-offset-2">
							<div id="webcam">
							</div>
						</div>						
					</div>
					<div class="row row-pad">
						<div class="col-md-4 col-md-offset-5">
							<button id="takeProfilePhoto" type="button" class="toolbar-button btn btn-default fa fa-camera">&nbsp;&nbsp;Take Photo</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>