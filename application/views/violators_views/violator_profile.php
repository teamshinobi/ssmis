	<div id="content">		
		<div id="content-header">
			<h1>Profile  </h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
			<div class="row">
				<div class="col-md-6">					
					<p class="header"><i class="fa fa-file-image-o"></i> MUG-SHOTS</p>	
					<hr/>
				</div>
				<div class="col-md-2 col-md-offset-4">
					<p id="violatorId" style="margin: 0;" class="text-muted"><?php echo $violator->violator_id ?></p>		
				</div>		
			</div>
			<div class = "row">
				<div class = "col-md-6 col-sm-12">
					<img data-value="front" id="preview" style = "width:600px;height:auto;" src = "<?php echo base_url() . $violator->file_path . '/' . $violator->violator_id .'_front.jpg' ?>" class = "img-responsive img-thumbnail">
				</div>
				<div class="col-md-6" style="padding-left:40px;padding-bottom:40px;">
					<div class="row">
						<div class="col-md-offset-1 col-md-5 col-sm-6 col-xs-6">
							<img data-id="<?php echo $violator->violator_id ?>" data-value="front" style = "width:150px;height:150px;" src= "<?php echo base_url() . $violator->file_path . '/' . '/thumbs/' . $violator->violator_id .'_front.jpg' ?>"  style = "width:95%; height:auto;" class = "mugshot img-responsive img-thumbnail">
							<p class="mugshot-label">Front</p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<img data-id="<?php echo $violator->violator_id ?>" data-value="rear" style = "width:150px;height:150px;" src= "<?php echo base_url() . $violator->file_path . '/' . '/thumbs/' . $violator->violator_id .'_rear.jpg' ?>"  style = "width:95%; height:auto;" class = "mugshot img-responsive img-thumbnail">
							<p class="mugshot-label">Rear</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-offset-1 col-md-5 col-sm-6 col-xs-6">
							<img data-id="<?php echo $violator->violator_id ?>" data-value="right" style = "width:150px;height:150px;" src= "<?php echo base_url() . $violator->file_path . '/' . '/thumbs/' . $violator->violator_id .'_right.jpg' ?>"  style = "width:95%; height:auto;" class = "mugshot img-responsive img-thumbnail">
							<p class="mugshot-label" >Right</p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<img data-id="<?php echo $violator->violator_id ?>" data-value="left" style = "width:150px;height:150px;" src= "<?php echo base_url() . $violator->file_path . '/' . '/thumbs/' . $violator->violator_id .'_left.jpg' ?>"  style = "width:95%; height:auto;" class = "mugshot img-responsive img-thumbnail">
							<p class="mugshot-label">Left</p>
						</div>
					</div>
				</div>
			</div>	
			<div class="row">
				<div style="padding-bottom:20px;" class="hidden-xs hidden-sm col-md-6 col-md-offset-2">
					<button id="showUploadForm" type="button" class="btn btn-md btn-default"><i class="fa fa-camera"></i> Change Image </button>
				</div>
			</div>
            
			<div class="row">
				<div class="col-md-8">					
					<p class="header"> <i class="fa fa-user"></i> BASIC INFORMATION</p>	
					<hr/>
				</div>
				<div class="col-md-2 col-md-offset-2">					
					<button id="updateViolatorButton" class="btn btn-default" type="button"><span style="margin:0;"><i class="fa fa-edit"></i></span> Edit</button>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">					
					<p class="p-label">Full name</p>
					<p><?php echo $violator->last_name .', ' . $violator->first_name . ' ' . $violator->middle_name ?></p>
				</div>
				<div class="col-md-3">					
					<p class="p-label">Alias</p>
					<p><?php if(!empty($violator->alias)){echo $violator->alias;}else{echo 'None';} ?></p>
				</div>
				<div class="col-md-3">		
					<p class="p-label">Birthday</p>				
					<p><?php echo $violator->birth_date ?></p>	
				</div>
				<div class="col-md-3">					
					<p><?php echo $violator->gender == 1 ? '<i class="fa fa-mars"></i> Male' : '<i class="fa fa-venus"></i> Female' ?></p>	
				</div>
			</div>
            <div class="row">
                <div class="col-md-3">					
					<p class="p-label">Place of Birth</p>
					<p><?php  echo empty($violator->place_of_birth) ? '---' : $violator->place_of_birth;  ?></p>
				</div>
				<div class="col-md-3">					
					<p class="p-label">Marital Status</p>
					<p><?php  echo empty($violator->marital_status) ? '---' : $violator->marital_status;  ?></p>
				</div>
				<div class="col-md-3">					
					<p class="p-label"><i class="fa fa-briefcase"></i> Occupation</p>
					<p><?php echo empty($violator->occupation) ? '---' : $violator->occupation; ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">Nationality</p>				
					<p><?php echo empty($violator->nationality) ? '---' : $violator->nationality; ?></p>	
				</div>
			</div>
            <div class="row">
                <div class="col-md-3">					
					<p class="p-label"><i class="fa fa-phone"></i> Tel No</p>
					<p><?php  echo empty($violator->tel_no) ? '---' : $violator->tel_no;  ?></p>
                </div>
			</div>
            
            <div class="row">
				<div class="col-md-8">					
					<p class="header"> <i class="fa fa-map-marker"></i> LIVES IN...</p>	
					<hr/>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<p><?php 
                        if(empty($violator->street_name) && empty($violator->barangay_name) && empty($violator->city_name)){
                            echo '---';

                        }else{
                             echo $violator->street_name .', ' . $violator->barangay_name . ' ' . $violator->city_name;

                        }           
                    ?>
                    </p>
				</div>	
					
			</div>
            
            
            <!-- PHYSICAL APPEARANCE -->
            <div class="row">
				<div class="col-md-8">					
					<p class="header"><i class="fa fa-male"></i> PHYSICAL APPEARANCE</p>	
					<hr/>
				</div>
			</div>
        
			<div class="row">
				<div class="col-md-3">
					<p class="p-label">Weight</p>
					<p><?php echo empty($violator->weight) || $violator->weight=='0' ? '---' : $violator->weight . ' kg.' ?></p>
				</div>	
				<div class="col-md-3">
					<p class="p-label">Height</p>
					<p><?php echo empty($violator->height) || $violator->height=='0' ? '---' : $violator->height . ' ft.' ?></p>
				</div>	
				<div class="col-md-3">
					<p class="p-label">Identifying marks</p>
					<p><?php echo empty($violator->identifying_marks) ? '---' : $violator->identifying_marks ?></p>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-3">
					<p class="p-label">Eyes color</p>
					<p><?php echo empty($violator->eyes_color)  ? 'Black' : $violator->eyes_color ?></p>
				</div>
				<div class="col-md-3">
					<p class="p-label">Hair color</p>
					<p><?php echo empty($violator->hair_color)  ? 'Black' : $violator->hair_color ?></p>
				</div>	
                <div class="col-md-3">
					<p class="p-label">Complexion</p>
					<p><?php echo empty($violator->complexion)  ? 'Black' : $violator->complexion ?></p>
				</div>	
			</div>
            
            <!-- IDS -->
            <div class="row">
				<div class="col-md-8">					
					<p class="header"><i class="fa fa-credit-card"></i> Licenses/Certificates/ID's</p>	
					<hr/>
				</div>
			</div>
            <div class="row">
                <div class="col-md-3">
					<p class="p-label">Driver's License</p>
					<p><?php echo empty($violator->dr_license)  ? '---' : $violator->dr_license ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">Issued At</p>
					<p><?php echo empty($violator->dr_issued_at)  ? '---' : $violator->dr_issued_at ?></p>
				</div>
                 <div class="col-md-3">
					<p class="p-label">Issued ON</p>
					<p><?php echo empty($violator->dr_issued_on)  ? '---' : $violator->dr_issued_on ?></p>
				</div>
            </div>
            <div class="row">
                <div class="col-md-3">
					<p class="p-label">Res Cert NR</p>
					<p><?php echo empty($violator->res_cert)  ? '---' : $violator->res_cert ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">Issued At</p>
					<p><?php echo empty($violator->res_cert_issued_at)  ? '---' : $violator->res_cert_issued_at ?></p>
				</div>
                 <div class="col-md-3">
					<p class="p-label">Issued ON</p>
					<p><?php echo empty($violator->res_cert_issued_on)  ? '---' : $violator->res_cert_issued_on ?></p>
				</div>
            </div>
            <div class="row">
                <div class="col-md-3">
					<p class="p-label">Other ID's</p>
					<p><?php echo empty($violator->other_ids)  ? '---' : $violator->other_ids ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">ID Numbers</p>
					<p><?php echo empty($violator->id_numbers)  ? '---' : $violator->id_numbers ?></p>
				</div>
               
            </div>
            <!-- EDUCATIONAL BACKGROUND  -->
            <div class="row">
				<div class="col-md-8">					
					<p class="header"><i class="fa fa-graduation-cap"></i>  EDUCATIONAL BACKGROUND</p>	
					<hr/>
				</div>
			</div>
            
            <div class="row">
				<div class="col-md-3">
					<p class="p-label">Educational attainment</p>
					<p><?php echo empty($violator->educational_attainment)  ? '---' : $violator->educational_attainment ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">Name of school</p>
					<p><?php echo empty($violator->name_of_school)  ? '---' : $violator->name_of_school ?></p>
				</div>
			</div>	
            
            <div class="row">
				<div class="col-md-3">
					<p class="p-label">Location of school</p>
					<p><?php echo empty($violator->location_of_school)  ? '---' : $violator->location_of_school ?></p>
				</div>
            
			</div>	
            <!-- FAMILY BACKGROUND -->
            <div class="row">
				<div class="col-md-8">					
					<p class="header"> <i class="fa fa-home"></i> FAMILY BACKGROUND</p>	
					<hr/>
				</div>
			</div>
            
            
			<div class="row">
				<div class="col-md-3">
					<p class="p-label">Mother</p>
					<p><?php echo empty($violator->mother_name)  ? '---' : $violator->mother_name ?></p>
				</div>
				<div class="col-md-4">
					<p class="p-label">Mother address</p>
					<p><?php echo empty($violator->mother_address)  ? '---' : $violator->mother_address ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">Age</p>
					<p><?php echo empty($violator->mother_age)  ? '---' : $violator->mother_age ?></p>
				</div>
			</div>
            <div class="row">
				<div class="col-md-3">
					<p class="p-label">Father</p>
					<p><?php echo empty($violator->father_name)  ? '---' : $violator->father_name ?></p>
				</div>
                	<div class="col-md-4">
					<p class="p-label">Mother address</p>
					<p><?php echo empty($violator->father_address)  ? '---' : $violator->father_address ?></p>
				</div>
                <div class="col-md-3">
					<p class="p-label">Age</p>
					<p><?php echo empty($violator->father_age)  ? '---' : $violator->father_age ?></p>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-3">
					<p class="p-label"><i class="fa fa-phone"></i> Who to contact?</p>
					<p><?php echo empty($violator->contact_person)  ? '---' : $violator->contact_person . ' ( ' . $violator->relationship .' )'; ?></p>
					<p class="text-muted"><?php echo '&nbsp' . $violator->contact_info ?> </p>
				</div>
               
			</div>	
          
			<div class="row">
				<div class="col-md-8">					
					<p class="header">Cases involved <span class="p-extras text-muted>"> ( Click each case to view more details. )</span></p>	
					<hr/>
                    <p id="cases_message"></p>
				</div>	
			</div>	
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div id="cards">

                    </div>
                </div>
            </div>
			<div class="row" style="margin-bottom: 0px; margin-top: 50px;">
				<div class="col-md-8">					
                    <p class="header"><i class="fa fa-hand-pointer-o"></i> Fingerprints</p>	
					<hr/>
				</div>			
			</div>
			<div class="row">
				
				<div class="col-lg-1 col-md-1">
					<label>Right Hand</label>
				</div>				
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $fingerprints->thumb_rimg){            
                            echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $fingerprints->thumb_rimg ).'"/>';
                            echo '<p style="margin-left:0px; margin-top:5px;">Thumb</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $fingerprints->index_rimg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $fingerprints->index_rimg ).'"/>';
						      echo '<p style="margin-left:0px; margin-top:5px;">Index</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $fingerprints->middle_rimg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $fingerprints->middle_rimg ).'"/>';
						       echo '<p style="margin-left:0px; margin-top:5px;">Middle</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $fingerprints->ring_rimg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $fingerprints->ring_rimg ).'"/>';
						       echo '<p style="margin-left:0px; margin-top:5px;">Ring</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $fingerprints->little_rimg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $fingerprints->little_rimg ).'"/>';
						       echo '<p style="margin-left:0px; margin-top:5px;">Little</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-1 col-md-1">
					<label>Left Hand</label>
				</div>				
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $lFingerprints->thumb_limg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $lFingerprints->thumb_limg ).'"/>';
						      echo '<p style="margin-left:0px; margin-top:5px;">Thumb</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $lFingerprints->index_limg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $lFingerprints->index_limg ).'"/>';
						      echo '<p style="margin-left:0px; margin-top:5px;">Index</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $lFingerprints->middle_limg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $lFingerprints->middle_limg ).'"/>';
						      echo '<p style="margin-left:0px; margin-top:5px;">Middle</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $lFingerprints->ring_limg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $lFingerprints->ring_limg ).'"/>';
						      echo '<p style="margin-left:0px; margin-top:5px;">Ring</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
				<div class="col-md-2 col-lg-2">
					<?php 
						if($fingerprints && $fingerprints->little_rimg){ 
							echo '<img class="img-responsive img-thumbnail" src="data:image/jpeg;base64,'.base64_encode( $fingerprints->little_rimg ).'"/>';
						       echo '<p style="margin-left:0px; margin-top:5px;">Little</p>';
                        } else { 
							echo 'Fingerprint not available.';
						} 
					?>
				</div>
			</div>
            
		</div>	
	</div>	
		
	<!-- End of -->
</div>