<div class="modal fade" id="advancedSearchModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Advanced search</h4>
				<p class="text-muted">Search violators in the database categorically.</p>
			</div>
			<div class="modal-body">
				<div class="row" style="border: 1px solid black; padding: 20px;">
					<div class="col-md-1 col-lg-1">
						<label> Start date </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<div id="start_date" class="input-group input-append date" data-date-start-view="2">
							<input name="start_date" class="form-control" type="text" placeholder="Start date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="col-md-1 col-lg-1">
						<label> &nbsp;&nbsp;&nbsp;&nbsp;To </label>
					</div>
					<div class="col-md-1 col-lg-1">
							<label> End date </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<div id="end_date" class="input-group input-append date" data-date-start-view="2">
							<input name="end_date" class="form-control" type="text" placeholder="End date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="col-md-2 col-lg-2">
						<button id="searchByDate" type="button" class="btn btn-sm btn-default"><i class="fa fa-search"></i>&nbsp; Search</button>
					</div>
					<div class="col-md-1 col-lg-1">
						<img id="searchDateLoading" class="img-responsive" style="display: none; height: 30px;" src="<?php echo base_url() . "public/resources/design_photos/loading.gif"?>"/>
					</div>
					
				</div>
				<div class="row" style="border: 1px solid black; padding: 20px; margin-top: 10px">
					<div class="col-md-1 col-lg-1">
						<label> Number of cases </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<input id="numCases" type="text" class="form-control" placeholder="Enter number of cases"/>
					</div>
					<div class="col-md-3 col-lg-3">
						<button id="searchByNumberCases" type="button" class="btn btn-sm btn-default"><i class="fa fa-search"></i>&nbsp; Search</button>
					</div>
                    <div class="col-md-3 col-lg-4">
                        <p class="text-muted" style="font-size:12px;">Please follow the format : (N-N). Example 2-5.</p>
                    </div>
				</div>
				<div class="row" style="margin-top: 40px;">
					<div class="col-md-3 col-lg-3">
						<p class="">Search results...</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-lg-12">
						<ul style="margin-left: 20px; margin-bottom: 0px; padding-bottom: 0px;" class="pagination" id="pager">
						</ul>					
					</div>
				</div>
				<div class="row">
					<ul id="search-results" class="search-results">

					</ul>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="closeSearchForm"class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>