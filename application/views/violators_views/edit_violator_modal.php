<div class="modal fade" id="editViolatorModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-black">
				<h4>Edit</h4>
				<p style="margin: 0" id="violatorId_edit" class="text-muted">Violator ID</p>
			</div>
			<form id="update-violator-form" method="POST" action="violators/update">
			<div class="modal-body" style="padding: 30px;">
                <div class="row form-group" style="padding-top: 10px;">
					<div class="col-md-6">
                        <h4><i class="fa fa-user"></i> Basic Information </h4>
                        <hr/>
					</div>					
				</div>
				<div class="row form-group" style="margin-top: 20px;">
					<div class="col-lg-1 col-md-1">
						<label>First Name: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="first_name" id="first_name" class="form-control" type="text" placeholder="First Name"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Middle Name: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="middle_name" id="middle_name" class="form-control" type="text" placeholder="Middle Name"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Last Name: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="last_name" id="last_name" class="form-control" type="text" placeholder="Last Name"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label>Gender </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<label class="radio-inline">
						  <input type="radio" name="gender" class="" value="1" checked> Male
						</label>
						<label class="radio-inline">
						  <input type="radio" name="gender" class="" value="0"> Female
						</label>
					</div>
					<div class="col-md-1 col-lg-1">
							<label> Birthdate </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<div id="birthdate" class="input-group input-append date" data-date-start-view="2">
							<input name="birthdate" class="form-control" type="text" placeholder="Birthday">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="col-md-1 col-lg-1">
						<label> Place of Birth </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="place_of_birth" id="place_of_birth" class="form-control" type="text" placeholder="Place of Birth"/>
					</div>
				</div>
				<div class="row form-group">
                      <div class="col-md-2 col-md-1">
                        <label> Marital Status </label>
                    </div>
                    <div class="col-md-3 col-sm-3" id="marital_list">
                        <select id="martial_status" name="marital_status" class="form-control">
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="Widow/er">Widow/er</option>
                            <option value="Separated">Separated</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-1">
						<label>Occupation </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<input name="occupation" id="occupation" class="form-control" type="text" placeholder="Occupation"/>		
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Alias </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<input name="alias" id="alias" class="form-control" type="text" placeholder="Alias"/>		
					</div>
				</div>
                <div class="row form-group">
                    <div class="col-lg-1 col-md-1">
						<label><i class="fa fa-phone"></i> Tel No </label>
					</div>
					<div class="col-md-3 col-lg-3">
						<input name="tel_no" id="tel_no" class="form-control num_only" type="text" placeholder="Telephone Number"/>		
					</div>
                </div>
                
				<div class="row form-group">
					<div class="col-md-6">
				        <h4><i class="fa fa-map-marker"></i> Lives In... </h4>
					</div>					
				</div>
				<div class="row form-group">
					<div class="col-lg-4 col-md-4">
						<input name="street_name" id="street_name" class="form-control" type="text" placeholder="Street"/>
					</div>	
					<div class="col-lg-4 col-md-4">
						<input name="barangay_name" id="barangay_name" class="form-control" type="text" placeholder="Barangay"/>
					</div>	
					<div class="col-lg-4 col-md-4">
						<input name="city_name" id="city_name" class="form-control" type="text" placeholder="City"/>
					</div>
				</div>	
				<div class="row form-group" style="padding-top: 10px;">
					<div class="col-md-6">
                        <h4><i class="fa fa-male"></i> Physical Appearance</h4>
                        <hr/>
					</div>					
				</div>
				<div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label>Weight: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="weight" id="weight" class="form-control decimal_only" type="text" placeholder="Weight"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Height: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="height" id="height" class="form-control decimal_only" type="text" placeholder="Height"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Identifying marks: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="identifying_marks" id="identifying_marks" class="form-control" type="text" placeholder="Identifying marks"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label>Eyes color: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="eyes_color" id="eyes_color" class="form-control" type="text" placeholder="Eyes color"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Hair Color: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="hair_color" id="hair_color" class="form-control" type="text" placeholder="Hair Color"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Nationality: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="nationality" id="nationality" class="form-control" type="text" placeholder="Nationality"/>
					</div>
				</div>
                <div class="row form-group">
                    <div class="col-lg-1 col-md-1">
						<label>Complexion: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="complexion" id="complexion" class="form-control" type="text" placeholder="Complexion"/>
					</div>
                </div>
                <div class="row form-group" style="padding-top: 10px;">
					<div class="col-md-6">
                        <h4><i class="fa fa-credit-card"></i> Licenses/Certificates/ID's </h4>
                        <hr/>
					</div>					
				</div>
                
                 <div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label> Driver's License: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="dr_license" id="dr_license" class="form-control" type="text" placeholder="Drivers License"/>
					</div>
				    <div class="col-lg-1 col-md-1">
						<label> Issued at:  </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="dr_issued_at" id="dr_issued_at" class="form-control" type="text" placeholder="Issuse at..."/>
					</div>
                    <div class="col-lg-1 col-md-1">
						<label> Issued on:  </label>
					</div>
                    <div class="col-lg-3 col-md-3">
                        <div id="dr_issued" class="input-group input-append date" data-date-start-view="2">
                            <input name="dr_issued_on" id="dr_issued_on" class="form-control" type="text" placeholder="Issued On...">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
				</div>
                
                <div class="row form-group">
					<div class="col-lg-1 col-md-1">
						 <label> Res Cert NR</label>
					</div>
					<div class="col-lg-3 col-md-3">
				        <input name="res_cert" id="res_cert" class="form-control" type="text" placeholder="Res Cert NR"/>         
					</div>
				    <div class="col-lg-1 col-md-1">
						<label> Issued At</label>
					</div>
					<div class="col-lg-3 col-md-3">
                        <input name="res_cert_issued_at" id="res_cert_issued_at" class="form-control" type="text" placeholder="Issued At..."/>
                    </div>
                    <div class="col-lg-1 col-md-1">
						<label> Issued on:  </label>
					</div>
                    <div class="col-lg-3 col-md-3">
                        <div id="res_cert_issued" class="input-group input-append date" data-date-start-view="2">
                            <input name="res_cert_issued_on" id="res_cert_issued_on" class="form-control" type="text" placeholder="Issued On...">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
				</div>
                
                <div class="row form-group">
					<div class="col-lg-1 col-md-1">
						 <label> Other ID Cards: </label>
					</div>
					<div class="col-lg-3 col-md-3">
				        <input name="other_ids" id="other_ids" class="form-control" type="text" placeholder="Other IDS"/>         
					</div>
				    <div class="col-lg-1 col-md-1">
						<label> ID numbers: </label>
					</div>
					<div class="col-lg-3 col-md-3">
                        <input name="id_numbers" id="id_numbers" class="form-control" type="text" placeholder="ID Numbers"/>
                    </div>
				</div>
                
                <div class="row form-group" style="padding-top: 10px;">
					<div class="col-md-6">
                        <h4><i class="fa fa-graduation-cap"></i> Educational Background </h4>
                        <hr/>
					</div>					
				</div>
                
                <div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label> Education Attainment: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="educational_attainment" id="educational_attainment" class="form-control" type="text" placeholder="Education attainment"/>
                    </div>
				    <div class="col-lg-1 col-md-1">
						<label> Name of School </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="name_of_school" id="name_of_school" class="form-control" type="text" placeholder="Name of school"/>
					</div>
				</div>
                <div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label> Location of School: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="location_of_school" id="location_of_school" class="form-control" type="text" placeholder="Location of school"/>
					</div>
				   
				</div>
				<div class="row form-group" style="padding-top: 10px;">
					<div class="col-md-6">
						<h4><i class="fa fa-home"></i> Family Background </h4>
                        <hr/>
					</div>	
				</div>
				<div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label>Mother Name: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="mother_name" id="mother_name" class="form-control" type="text" placeholder="Mother name"/>
					</div>
				    <div class="col-lg-1 col-md-1">
						<label>Mother Address: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="mother_address" id="mother_address" class="form-control" type="text" placeholder="Mother address"/>
					</div>
                     <div class="col-lg-1 col-md-1">
						<label>Mother Age: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="mother_age" id="mother_age" class="form-control" type="text" placeholder="Mother age"/>
					</div>
				</div>
                <div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label>Father name: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="father_name" id="father_name" class="form-control" type="text" placeholder="Father name"/>
					</div>
                    <div class="col-lg-1 col-md-1">
						<label>Father Address: </label>
					</div>
                    <div class="col-lg-3 col-md-3">
						<input name="father_address" id="father_address" class="form-control" type="text" placeholder="Father address"/>
					</div>
                    <div class="col-lg-1 col-md-1">
						<label>Father Age: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="father_age" id="father_age" class="form-control" type="text" placeholder="Father age"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-1 col-md-1">
						<label>Contact person: </label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="contact_person" id="contact_person" class="form-control" type="text" placeholder="Contact person"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Relationship</label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="relationship" id="relationship" class="form-control" type="text" placeholder="Relationship with the contact person"/>
					</div>
					<div class="col-lg-1 col-md-1">
						<label>Contact #</label>
					</div>
					<div class="col-lg-3 col-md-3">
						<input name="contact_info" id="contact_info" class="form-control" type="text" placeholder="Contact number"/>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<a style="float: left;" href=""> Help? </a>
				<button type="submit" id="saveViolatorButton" class="btn btn-default">Save Changes</button>
				<button type="button" id="closeViolatorButton"class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
			</form>
		</div>
	</div>
</div>