<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Print</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-lg-12">
						<button id="printAllButton" style="width:100%;" type="button" class="btn btn-default">Print All Violators</button>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-md-12 col-lg-12">
						<button id="printSpecificButton" style="width:100%;" type="button" class="btn btn-default">Print Specific Violator</button>
					</div>
				</div>
				
				<div style="margin-top: 30px;" class="row formg-group" id="inputSection">
					
				</div>
			</div>
		
			<div class="modal-footer">
				<button type="button" id="" class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>
		