	<div id="content">
		<div id="content-header">
				<h1>Settings</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
              <div class="row">

                    <div class="col-md-3 col-sm-4">

                        <ul id="myTab" class="nav nav-pills nav-stacked">
                             <li class="active">
                                <a href="#station-tab" data-toggle="tab">
                                    <i class="fa fa-home"></i> 
                                    &nbsp;&nbsp;Station Information
                                </a>
                            </li>
                            <li>
                                <a href="#template-tab" data-toggle="tab">
                                    <i class="fa fa-file"></i> 
                                    &nbsp;&nbsp;Template Settings
                                </a>
                            </li>
                             <li>
                                <a href="#user-tab" data-toggle="tab">
                                    <i class="fa fa-user"></i> 
                                    &nbsp;&nbsp;User account settings
                                </a>
                            </li>
                          
                            <li>
                                <a href="#control-tab" data-toggle="tab">
                                    <i class="fa fa-dashboard"></i> 
                                    &nbsp;&nbsp;System Control
                                </a>
                            </li>
                            
                        </ul>
                    </div> <!-- /.col -->

                    <div class="col-md-9 col-sm-8">

                        <div class="tab-content stacked-content">
                            <div class="tab-pane fade" id="template-tab">

                                <h3 class="">Edit Forms Templates  <button id="tempButton" class="btn btn-default pull-right cleafix" type="button"><i class="fa fa-save"></i> Save </button></h3>
                                <p>Change the following reports and forms templates.</p> 
                                <br />
                                
                                <h3 class="">Spot Report</h3> 
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="spot" class="summernote" >

                                        </div>

                                        <hr/>
                                    </div>       
                                </div>
                                
                                <h3 class="">Main Affidavit</h3> 
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="main" class="summernote" >

                                        </div>

                                        <hr/>
                                    </div>       
                                </div>
                                <h3 class="">Supplemental Affidavit</h3> 
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="supp" class="summernote" >

                                        </div>

                                        <hr/>
                                    </div>       
                                </div>
                            </div>
                            
                            <div class="tab-pane fade" id="user-tab">

                                  <h3 class="">Users account settings</h3>

                                  <p>Set user to active or inactive. You can also reset their passwords here.</p>

                                  <br />
                                

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="table-responsive">
                                                <table id="users_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                                                    <thead>
                                                        <tr>
                                                            <th class="">ID</th>
                                                            <th class="">Username</th>
                                                            <th class="">Owner</th>
                                                            <th class="">Created On</th>
                                                            <th class="">Active</th>
                                                        </tr>	
                                                    </thead>				
                                                </table>		
                                            </div>
                                        </div>
                                    </div>

                            </div>
                            
                            <div class="tab-pane fade" id="control-tab">

                                <?php $this->load->view('settings_views/control_view.php'); ?>

                            </div>
                            
                              
                            <div class="tab-pane fade in active" id="station-tab">

                                <?php $this->load->view('settings_views/station_view.php'); ?>

                            </div>
                        </div>
                    </div>
           </div>
        </div>
    </div>

<!-- End of Wrapper for the sidebar -->
</div>

