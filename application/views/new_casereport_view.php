	

    <form id="case-report-form" method="POST" enctype="multipart/form-data">
	
	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
            <h1><i class="fa fa-save"></i> New Case Report</h1>
		</div> <!-- #content-header -->	
		
		<div id="content-container">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() . "index.php/casereport"?>">Case Report</a>
						</li>	
						<li><a href="<?php echo base_url() . "index.php/casereport"?>">New Case Report</a>
						</li>	
					</ol>
				</div>
			</div>	

			<div class="row">
				<div class = "col-lg-12">
					<ul id="myTab" class="nav nav-tabs">
						<li class="active"><a href="#arrest" data-toggle="tab"> Arrest Details</a></li>
						<li><a href="#violators" data-toggle="tab">Violator's Details</a></li>
						<li><a href="#inventory" data-toggle="tab">Inventory Details</a></li>
						<li><a href="#witness" data-toggle="tab">Witnesses' Details</a></li>
						<li><a href="#attachments" data-toggle="tab">Attachments</a></li>
						<button id="saveButton" style="margin-left: 10px;" type="submit" class="btn btn-md btn-default pull-right">
						  <i class="fa fa-save"></i> Save
						</button>
					 </ul>
				</div>	
				<div style="margin-left:10px; margin-right: 10px;" id="myTabContent" class="tab-content">
					<!-- Arrest Details Tab -->
					<div class="tab-pane fade in active" id="arrest">
						<?php $this->load->view('casereport_views/arrestdetails_form.php'); ?>
					</div>
					<!-- Arrest Details Tab -->
					<div class="tab-pane fade in" id="violators">
						<?php $this->load->view('casereport_views/violatorsdetails_form.php'); ?>
					</div>
					<!-- Inventory Details Tab -->
					<div class="tab-pane fade in" id="inventory">
						<?php $this->load->view('casereport_views/inventorydetails_form.php'); ?>
					</div>
                    <div class="tab-pane fade in" id="witness">
						<?php $this->load->view('casereport_views/witness_details.php'); ?>
					</div>
                    <div class="tab-pane fade in" id="attachments">
						<?php $this->load->view('casereport_views/attachments.php'); ?>
					</div>
				</div>
			</div>
		</div>	
	</div>
     <!-- /#page-content-wrapper -->
	 </form> 
	   
<!-- End of Wrapper for the sidebar -->
</div>