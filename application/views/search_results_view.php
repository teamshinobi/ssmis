	<div id="content">		
		
		<div id="content-header" style="margin-bottom: 0px; border-bottom: 1px solid black">
			<h1>Search &nbsp;<i class="fa fa-search"></i></h1>
		</div> <!-- #content-header -->	
		<div id="content-container" style="padding: 0px !important;">	
			  <div class="row" id="search-area" >
                <div class="row">
                     <div class="col-md-4 col-lg-4 col-lg-offset-2 col-md-offset-2">
                         <h4 style="font-weight:bold;">Search Term:</h4>
                    </div>
                </div>
                <div class="row">
                    <form action="/./ssmis/search/" type="GET">
                        <div class="col-md-6 col-lg-6 col-lg-offset-2 col-md-offset-2">
                            <input type="text" value="<?php echo $k; ?>" class="form-control" name="k" placeholder="Search..."/>
                        </div>
                        <div class="col-md-2 col-lg-2">
                           <button class="btn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-lg-offset-4 col-md-offset-4">
                        
                    </div>
                </div>
            </div>
            
            <div id="searchContent" class="row">
				<div class = "col-lg-12">
                    <p class="pull-right"><a><i class="fa fa-question-circle"></i> How to find what I am looking for? </a></p>
					<ul id="myTab" class="nav nav-tabs">
						<li class="active"><a href="#cases" data-toggle="tab"> <i class="fa fa-lg fa-file"></i>  Cases</a> </li>
						<li><a href="#violators" data-toggle="tab"><i class="fa fa-lg fa-group"></i>  Violators</a></li>
						<li><a href="#pnp" data-toggle="tab"><i class="fa fa-lg fa-fire"></i>  PNP</a></li>
                        <li><a href="#images" data-toggle="tab"><i class="fa fa-lg fa-image"></i>  Images</a></li>
                    
					 </ul>
				</div>	
				<div class="tab-content">
					
					<div class="tab-pane fade in active" id="cases">
                       
                        <h4 style="margin-left: 20px;" id="caseResultsMessage"></h4>
                        <div class="row" style="margin-left: 10px;margin-bottom:5px;">
                            <div style="padding-top: 10px !important;" id="case_per_page" class="col-md-12 col-lg-12"> 
                                <label>Case per page: </label> 
                                <select id="cases_num_pages" class="form-c"> 
                                    <option value="6">6</option>
                                    <option value="12">12</option>
                                    <option value="18">18</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12" style="margin-left: 15px; !important;">
                                <ul id="casePagination" class="pagination">
                                </ul>
				            </div>
                        </div>
      
                        <p id="caseCount" style="margin-left: 20px;margin-top: -20px; !important;margin-bottom: 20px;"></p>
                          
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-12 col-lg-12">
                                <div id="caseCards">

                                </div>
                            </div>
                        </div>
					</div>
					
                    <!-- VIOLATOR RESULTS -->
					<div style="padding-top: 10px !important;" class="tab-pane fade in" id="violators">
				        <h4 style="margin-left: 20px;" id="violatorResultsMessage"></h4>
                        
                        <div class="row">
                            <div class="col-md-4 col-lg-4" style="margin-left: 15px; !important;">
                                <ul id="violatorPagination" class="pagination">
                                </ul>
				            </div>
                            <div style="padding-top: 10px !important;" id="violator_per_page" class="col-md-2 col-lg-2 col-md-offset-5 col-lg-offset-5"> 
                                <label>Violators per page: </label> 
                                <select id="violator_num_page" class="form-c"/> 
                                    <option value="6">6</option>
                                    <option value="12">12</option>
                                    <option value="18">18</option>
                                </select>
                            </div>
                        </div>
                        
                        <p id="violatorCount" style="margin-left: 20px;margin-top: -20px; !important;margin-bottom: 20px;"></p>
                        
                        <div style="margin-left: 5px;margin-right: 10px;" class="row">
                            <div id="violatorCards">


                            </div>
                        </div>
					</div>
					
					<div class="tab-pane fade in" id="pnp">
                        <h4 style="margin-left: 20px;" id="pnpResultsMessage"></h4>
                        <div class="row">
                            <div class="col-md-4 col-lg-4" style="margin-left: 15px; !important;">
                                <ul id="pnpPagination" class="pagination">
                                </ul>
				            </div>
                            <div style="padding-top: 10px !important;" id="pnp_per_page" class="col-md-2 col-lg-2 col-md-offset-5 col-lg-offset-5"> 
                                <label>PNP per page: </label> 
                                <select id="pnp_num_page" class="form-c"/> 
                                    <option value="6">6</option>
                                    <option value="12">12</option>
                                    <option value="18">18</option>
                                </select>
                            </div>
                        </div>
                        <p id="pnpCount" style="margin-left: 20px;margin-top: -20px; !important;margin-bottom: 20px;"></p>
                        <div style="margin-left: 5px;margin-right: 10px;" class="row">
                            <div id="pnpCards">
                
                            </div>
                        </div>
					</div>
                    <div class="tab-pane fade in" id="images">
                        <h4 style="margin-left: 20px;" id="imagesResultsMessage"></h4>
                         <div class="row">
                            <div class="col-md-5 col-lg-5" style="margin-left: 15px; !important;">
                                <ul id="imagesPagination" class="pagination">
                                </ul>
				            </div>
                             <p id="imagesCount" style="margin-left: 20px;margin-top: -20px; !important;margin-bottom: 20px;"></p>
                            <div style="padding-top: 10px !important;" id="images_per_page" class="col-md-2 col-lg-2 col-md-offset-4 col-lg-offset-4"> 
                                <label>Images per page: </label> 
                                <select id="images_num_page" class="form-c"/> 
                                    <option value="6">6</option>
                                    <option value="12">12</option>
                                    <option value="18">18</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 5px;margin-right: 10px;">
                            <div id="imagesCards">
                                <!--<div class="panel panel-default bootcards-media">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Media Card Heading</h3>
                                    </div>
                                    <div class="panel-body">
                                        Media card description lorem ipsum dolor est compendium
                                    </div>
                                    <img src="http://www.teamstudio.com/Portals/218295/images/istock_000001242290small.jpg" class="img-responsive"/>
                                </div> --> 
                            </div>
                        </div>
					</div>
                </div>
			</div>
		</div>
	</div>
	
	   
<!-- End of -->
</div>