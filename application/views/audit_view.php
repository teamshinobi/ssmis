	<div id="content">		
		
		<div id="content-header" style="margin-bottom: 0px; border-bottom: 1px solid black">
			<h1>Audit Trail &nbsp;<i class="fa fa-unlock"></i></h1>
		</div> <!-- #content-header -->	
		<div id="content-container" style="padding: 0px !important;">	
            <div class="row" style="margin: 10px;">
                <div class="table-responsive col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <table id="audit_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
                        <thead>
                            <tr>
                                <th class="">ID</th>
                                <th class="">Date</th>
                                <th class="">Time</th>
                                <th class="">Operation</th>
                                <th class="">Description</th>					
                            </tr>	
                        </thead>				
                    </table>		
			 </div>
        </div>
    </div>
	   
<!-- End of -->
</div>