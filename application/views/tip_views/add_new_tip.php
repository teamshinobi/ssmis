<div class="modal fade" id="addModal"  tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><i class="fa fa-lg fa-book"></i> Add Tip</h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class = "col-lg-2" style="padding-top: 10px;">
                        <label>Date of Tipped:</label> 
                    </div>
                    <div class = "col-lg-6">
                        <div id="datepicker" class="input-group date datepicker">
                            <input id="date" name="date" class="form-control" type="text" placeholder="Date"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div> 
                </div>	
                
                <div class="row form-group">
                    <div class = "col-lg-2" style="padding-top: 10px;">
                        <label>Story:</label> 
                    </div>
                    <div class = "col-lg-6">
                        <textarea id="story" class="form-control" placeholder = "Story"></textarea>
                    </div>      
                </div>
                <div class="row form-group">
                    <div class = "col-lg-2" style="padding-top: 10px;">
                        <label><i class="fa fa-crosshairs"></i> Targets:</label> 
                    </div>   
                </div> 
                <div class="row form-group">
                  	<div class = "col-lg-2 col-lg-offset-2" style="padding-top: 10px;">
                        <label>Full name</label> 
                    </div>
                    <div class = "col-lg-4">
                        <input id="fullName" type="text" class="form-control" placeholder="Full name"/>
                    </div>   
                </div>
                 <div class="row form-group">
                  	<div class = "col-lg-2 col-lg-offset-2" style="padding-top: 10px;">
                        <label>Alias </label> 
                    </div>
                    <div class = "col-lg-4">
                        <input id="alias" type="text" class="form-control" placeholder="Alias"/>
                    </div>   
                </div>
                 <div class="row form-group">
                    <div class="col-md-2 col-lg-2 col-lg-offset-2">
                        <label>Nationality: </label>
                    </div>
                    <div class="col-md-3 col-lg-3">
                         <label class="checkbox-inline"><input id="is_filipino" name="is_filipino" type="checkbox" value="1" checked>Filipino</label>
                    </div>  
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2 col-lg-offset-2">
                         <label> Others (Specify): </label>
                    </div>
                    <div class="col-md-4 col-lg-4">
                          <input id="others_nationality" class="form-control"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2 col-lg-2 col-lg-offset-4">
                        <button id="addTargetButton" class="btn btn-default btn-sm" type="button">
                            <i class="fa fa-plus"></i> Add
                        </button>
                    </div> 
                    
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div id="targetsList" class="list-group">

                        </div>
                    </div>			
                </div>
            </div>
               		
            <div class="modal-footer">
                <button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="saveButton" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            </div>	
          
        </div>
    </div>
</div>	