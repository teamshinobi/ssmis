<div class="modal fade" id="importModal"  tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><i class="fa fa-lg fa-database"></i>&nbsp; Choose From Database</h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class = "col-lg-2" style="padding-top: 10px;">
                        <label>Date of Tipped:</label> 
                    </div>
                    <div class = "col-lg-4">
                        <div id="datepicker2" class="input-group date datepicker">
                            <input id="date2" name="date2" class="form-control" type="text" placeholder="Date"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div> 
                </div>
                <div class="row form-group">
                    <div class = "col-lg-2" style="padding-top: 10px;">
                        <label>Story:</label> 
                    </div>
                    <div class = "col-lg-4">
                        <textarea id="story2" class="form-control" placeholder = "Story"></textarea>
                    </div>      
                </div>
                <hr />
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive">
                            <table id="ex_violators_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                                <thead>
                                    <tr>
                                        <th class="">Violator ID</th>
                                        <th class="">Full Name</th>
                                        <th class="">Alias</th>
                                        <th class="">Picture</th>
                                        <th class="">Select</th>
                                    </tr>	
                                </thead>				
                            </table>		
                        </div>   
                    </div>
                </div> 
            </div>              		
            <div class="modal-footer">
                <button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="saveButton2" class="btn btn-default"><i class="fa fa-save"></i>&nbsp; Save </button>
            </div>	    
        </div>
    </div>
</div>	