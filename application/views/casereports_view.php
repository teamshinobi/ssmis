	<div id="content">
		<div id="content-header" style="margin-bottom: 0px !important;border-bottom: solid 1px black;">
			<h1>Case Reports</h1>
		</div> <!-- #content-header -->	
		<div id="content-container" style="padding: 0px !important;">
            <div class="row" style="margin-left: 10px; margin-top: 20px;">
                <div class="col-md-7 col-lg-7">
                     <h4 id="retResults">Results returned: </h4>       
                </div>
               
            </div>
            <div class="row" style="margin-left: 10px;">
                <div class="col-md-1 col-lg-1">
                     <h4 id="">Legends: </h4> 
                </div>
                <div class="col-md-8 col-lg-8">
                     <button style="background-color: f5f5f5;" class="btn btn-default"></button>  Pending in PNP
                     <button id="inquestButton" style="background-color: #1B5E20;" class="btn btn-default"></button>  Pending in Inquest
                     <button id="courtButton" style="background-color: steelblue;" class="btn btn-default"></button>  Pending on Court
                     <button id="closedButton" style="background-color: #B71C1C;" class="btn btn-default"></button>  Closed
                     <button id="dismissedButton" style="background-color: #212121;" class="btn btn-default"></button>  Dismissed
                </div> 
                
            </div>
            
            <hr/>
            <div class="row" style="margin-left: 10px;margin-bottom:5px;">
                <div class="col-md-6 col-lg-6">
                    <label>Case per page: </label> 
                    <select id="num_pages" class="form-c" onchange="VCRM.Events.onSelectChange(this.selectedIndex)"> 
                        <option value="6">6</option>
                        <option value="12">12</option>
                        <option value="18">18</option>
                    </select>
                  
				</div>  
            </div>
            <div class="row" style="margin-left: 10px;">
				<div class="col-md-6 col-lg-6">
					<ul id="myPage" class="pagination">
					</ul>
				</div>
              
			</div>
            <div class="row">
				<div class="col-md-12 col-lg-12">
					<div id="cards">
			
					</div>
				</div>
			</div>
		</div>
	</div>
</div>