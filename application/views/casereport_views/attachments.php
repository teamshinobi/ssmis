	<div class="portlet">
		<div class="portlet-header">
			<h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Attachments</h2>
			
		</div>	
		<div class="portlet-content">
            <div class="row">
                <div class="col-md-5">
                    <p>
                        Click <i class="fa fa-paperclip"></i> button to attach.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <ul class="attachments-list">
                        <li class="attachment">
                            <span class="attachment-label"> Spot Report </span><button id="spotButton" data-action="attach" class="btn btn-sm btn-default" type="button"><i class="fa fa-paperclip"></i></button><br>
                            <span id="sp_attached_id" class="attachment-id"> <a>Attachment ID</a> </span>
                        </li>
                        <li class="attachment">
                            <span class="attachment-label"> Pre-operation Report </span><button id="preopButton" data-action="attach" class="btn btn-sm btn-default" type="button"><i class="fa fa-paperclip"></i></button><br>
                            <span id="preop_attached_id" class="attachment-id"> <a>Attachment ID</a> </span>
                        </li>
                        <li class="attachment">
                            <span class="attachment-label"> Coordination Form </span><button id="coordButton" data-action="attach" class="btn btn-sm btn-default" type="button"><i class="fa fa-paperclip"></i></button><br>
                            <span id="coord_attached_id" class="attachment-id"> <a>Attachment ID</a> </span>
                        </li>
                        <li class="attachment">
                            <span class="attachment-label"> Main Affidavit </span><button id="mainButton" data-action="attach" class="btn btn-sm btn-default" type="button"><i class="fa fa-paperclip"></i></button><br>
                            <span id="main-id" class="attachment-id"> <a>Attachment ID</a> </span>
                        </li>
                        <li class="attachment">
                            <span class="attachment-label"> Supplemental Affidavit </span><button id="suppButton" data-action="attach" class="btn btn-sm btn-default" type="button"><i class="fa fa-paperclip"></i></button><br>
                            <span id="supp-id" class="attachment-id"> <a>Attachment ID</a> </span>
                        </li>
                    </ul> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <p>
                        Upload scanned documents
                    </p>
                </div>
            </div>
            <div class="row" id="labDocuments">
               
            </div>
             <div class="row">
                 
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    <span class="attachment-label"> Laboratory Examination </span>
                    <input type="file" id="lab" name="lab[]" size="10" class="filestyle" multiple/>
                    <a id="removeLab" href="">Remove All</a>
                </div>
            </div>
            <div class="row" id="medDocuments">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    <span class="attachment-label"> Medical Examination </span>
                   <input type="file" id="med" name="med[]" size="10" class="filestyle" multiple/>
                     <a id="removeMed" href="">Remove All</a>
                </div>
            </div>
            <div class="row" id="swDocuments">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    <span class="attachment-label"> Search Warrant </span>
                   <input type="file" id="sw" name="sw[]" size="10" class="filestyle" multiple/>
                    <a id="removeSw" href="">Remove All</a>
                </div>
            </div>
            <div class="row" id="waDocuments">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    <span class="attachment-label"> Warrant of Arrest </span>
                   <input type="file" id="wa" name="wa[]" size="10" class="filestyle" multiple/>
                     <a id="removeWa" href="">Remove All</a>
                </div>
            </div>
            <div class="row" id="othersDocuments">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
                    <span class="attachment-label"> Other Documents</span>
                   <input type="file" id="others" name="d[]" size="10" class="filestyle" multiple/>
                     <a id="removeOthers" href="">Remove All</a>
                </div>
            </div>
        </div>
	</div>
	