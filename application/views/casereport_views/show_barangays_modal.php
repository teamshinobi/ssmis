<div class="modal fade" id="barangaysModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Choose Barangay</h4>
                
            </div>
            <div class="modal-body">
                <table id="barangay_table" class="table table-striped table-bordered table-hover table-highlight table-checkable">
                    <thead>
                        <tr>
                            <th class="">Barangay ID</th>
                            <th class="">Barangay</th>
                            <th class="">City</th>
                            <th class="">Incidents</th>
                            <th class="">Select</th>
                        </tr>	
                    </thead>
                </table> 
            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>