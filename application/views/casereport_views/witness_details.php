<div class="portlet">
    <div class="portlet-header">
        <h2><i class="fa fa-eye"></i><span class="break"></span>Witness Details</h2>
        <ul class="portlet-tools pull-right">
            <li>
                <button title="Add new witness"  id="showAddWitnessModal" type="button" class="btn btn-md btn-default">
                    <span><i class="fa fa-plus"></i></span>
                </button>
            </li>
        </ul>
    </div>	
    <div class="portlet-content">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="table-responsive">
                    <table id="witness_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable">
                        <thead>
                            <tr>
                                <th class="">#</th>
                                <th class="">Name</th>
                                <th class="">Address</th>
                                <th class="">Contact Info</th>
                            </tr>	
                        </thead>
                    </table>
                </div>
            </div>    
        </div>
    </div>
</div>