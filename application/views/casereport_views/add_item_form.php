<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Add Item</h4>
				<p id="itemId" class="text-muted">Item ID</p>
			</div>
		
			<div class="modal-body" id="inventory_form_div">
				<div class="portlet">
					<div class="portlet-header">
						<h2><i class="fa fa-plus"></i><span class="break"></span>Item</h2>
						<ul class="portlet-tools pull-right">
							<li>
								<label style="visibility: hidden;" class="checkbox-inline"><input name="inc_cc" id="inc_cc" type="checkbox" value="1">Include in CC</label>
							</li>
						</ul>
					</div>
					<div  class="portlet-content">
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label>Description</label>
							</div>
							<div class="col-md-10 col-sm-6">
								<input maxlength="60" id="description" class="form-control" type="text" placeholder="Description"/>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label>Quantity</label>
							</div>
							<div class="col-md-10 col-sm-6">
								<input maxlength="20" id="qty" class="form-control" type="text" placeholder="Quantity"/>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label>Markings</label>
							</div>
							<div class="col-md-10 col-sm-6">
								<input maxlength="30" id="markings" class="form-control" type="text" placeholder="Markings"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="thumbnail">
									<img id="itempreview" src="<?php echo base_url() . 'public/resources/photos/default_img.jpg'?>"/>
								</div> <!-- /.thumbnail -->
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<p class="text-muted">Choose image</p>
									</div>		
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12" style="padding-top:80px;">
										<input name="itemfile" class="filestyle" data-size="sm" id="itemfile" type = "file">
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 20px;">
							<div class="col-md-12 col-sm-12">
								<div class="well">
									<p>Currently Selected</p>
								</div>
							</div>			
						</div>
                        <div class="row">
							<div class="col-md-12 col-sm-12">
								<div id="selectedViolatorsList" class="list-group">
								
								</div>
							</div>			
						</div>
                        
                        <div class="row" style="margin-top: 20px;">
							<div class="col-md-12 col-sm-12">
								<div class="well">
									<p>Choose Violators</p>
								</div>
							</div>			
						</div>
                        
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div id="regViolatorsList" class="list-group">
								
								</div>
							</div>			
						</div>
					</div>
				</div>
					
			</div>
			
			<div class="modal-footer">
				<button type="button" id="addItemButton" class="btn btn-default">Add Item</button>
				<button type="button" id="closeAddItemModal"class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>