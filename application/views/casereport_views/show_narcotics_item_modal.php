<div class="modal fade" id="showNarcoticsItemModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Item Details</h4>
				<p id="narcotidsItemId_v" class="text-muted">Item ID</p>
			</div>
			<div class="modal-body" id="add_narcotics_form">
				<div class="portlet">
					<div class="portlet-header">
						<h2><i class="fa fa-plus"></i><span class="break"></span>Narcotics</h2>
						<ul class="portlet-tools pull-right">
							<li>
								<label style="visibility: hidden;" class="checkbox-inline"><input name="inc_cc_n" id="inc_cc_n" type="checkbox" value="1">Include in CC</label>
							</li>
						</ul>
					</div>
					<div class="portlet-content">
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label>Kind</label>
							</div>
							<div class="col-md-10 col-sm-6 select-div" >
								<select id="narcotics_list_v" class="form-control">
								
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label>Quantity</label>
							</div>
							<div class="col-md-10 col-sm-6">
								<input id="qty_n_v" class="form-control" type="text" placeholder="Quantity"/>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label>Markings</label>
							</div>
							<div class="col-md-10 col-sm-6">
								<input id="markings_n_v" class="form-control" type="text" placeholder="Markings"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="thumbnail">
									<img id="itempreview_n_v" src="<?php echo base_url() . 'public/resources/photos/default_img.jpg'?>"/>
								</div> <!-- /.thumbnail -->
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<p class="text-muted">Choose image</p>
									</div>		
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12" style="padding-top:80px;">
										<input name="itemfile_n_v" class="filestyle" data-size="sm" id="itemfile_n_v" type = "file">
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 20px;">
							<div class="col-md-12 col-sm-12">
								<div class="well">
									<p>This item was collected to the following violators:</p>
								</div>
							</div>			
						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div id="collectedFromList_n" class="list-group">
								
								</div>
							</div>			
						</div>
						<div class="row" style="margin-top: 20px;">
							<div class="col-md-12 col-sm-12">
								<div class="well">
									<p>Add more violators</p>
								</div>
							</div>			
						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div id="addMoreList_n" class="list-group">
								
								</div>
							</div>			
						</div>
					</div>
				</div>
			</div>		
			<div class="modal-footer">
				<button type="button" id="updateNItemButton" class="btn btn-default">Update Item</button>
				<button type="button" id="closeShowNarcoticsItemButton"class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>