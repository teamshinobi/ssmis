<div class="modal fade" id="editCcJudgeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Edit CC# / Judge</h4>
				
			</div>
            <div class="modal-body">
                 <div class="row form-group">
                    <div class="col-md-2 col-md-offset-2">
                        <label> CC#: </label>
                    </div>
                    <div class="col-md-5">
                        <input id="cc_i" class="form-control" type="text"/>
                    </div>
                </div>
                 <div class="row form-group">
                    <div class="col-md-2 col-md-offset-2">
                        <label> Judge: </label>
                    </div>
                    <div class="col-md-5">
                        <input id="cc_judge_i" class="form-control" type="text"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
				<button type="button" id="editCcJudgeButton" class="btn btn-default">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
            
		</div>
	</div>
</div>