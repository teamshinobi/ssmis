<div class="modal fade" id="editDistributionModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <button id="deleteDistribution" type="button" class="close"><i class="fa fa-trash"></i></button>
				<h4><i class="fa fa-folder"></i>&nbsp; View/Edit Distribution</h4>
                <p id="d_id"></p>
			</div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-3">
                        <label> Distributed to: </label>
                    </div>
                    <div class="col-md-6">
                        <input id="distributed_to_v" class="form-control" type="text" type="text"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label> Copies: </label>
                    </div>
                    <div class="col-md-5">
                        <input id="copies_v" class="form-control" type="text" type="text"/>
                    </div>
                </div>
                 <div class="row form-group">
                    <div class="col-md-3">
                        <label> When: </label>
                    </div>
                    <div class="col-md-5">
                        <div id="datepicker" class="input-group date datepicker">
                            <input id="when_v" class="form-control" type="text" placeholder="Date Of Arrest"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                     </div>       
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" id="editDistributionButton"class="btn btn-default">Save Changes</button>
                <button type="button" id="closeDistribution"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>