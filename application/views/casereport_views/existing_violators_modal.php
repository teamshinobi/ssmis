<div class="modal fade" id="existingViolatorModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-group"></i> Violators</h4>
			</div>
			<div class="modal-body">
				<div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="portlet">
                            <div class="portlet-header">
                                <h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Violators</h2>
                            </div>	
                            <div class="portlet-content">
                                <p class="text-muted">Click the "SELECT" button to select a violator.</p>
                                <div class="well">
                                        <p id="selection-details">There is no violator currently selected.</p>
                                </div>
                                <div class="table-responsive">
                                    <table id="ex_violators_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                                        <thead>
                                            <tr>
                                                <th class="">Violator ID</th>
                                                <th class="">Full Name</th>
                                                <th class="">Alias</th>
                                                <th class="">Picture</th>
                                                <th class="">Select</th>
                                            </tr>	
                                        </thead>				
                                    </table>		
                                </div>
                                <p>Click a row in a table to view violator.</p>
                            </div>
                        </div>
                    </div>
				</div>
				
				<div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="portlet" style="margin-top:20px;">
                            <div class="portlet-header">
                                <h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Charges</h2>
                            </div>	
                            <div class="portlet-content">
                                <p class="text-muted">Select multiple charges that the violator committed.</p>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <table id="ex_ra_table" class="table table-striped table-bordered table-hover table-highlight table-checkable">
                                            <thead>
                                                <tr>
                                                    <th class="">Select</th>
                                                    <th class="">Charges ID</th>
                                                    <th class="">Article</th>
                                                    <th class="">Section</th>
                                                    <th class="">Desc</th>
                                                </tr>	
                                            </thead>
                                        </table>
                                    </div>
                                </div>		
                            </div>	
                        </div>
                    </div>
				</div>
			<div class="modal-footer">
				<button type="button" id="getViolatorsInfoButton" class="btn btn-default">Add Selected</button>
				<button type="button" id="closeExistingViolatorsModalButton" class="btn btn-default" data-dismiss="modal">Close</button>	
			</div>
			</div>
		</div>
	</div>
</div>