<div class="modal fade" id="viewViolatorModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-eye"></i> Violator's Information</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-lg-12" style="margin-bottom:20px;">
						<h4 class="heading">Mugshots</h4>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<img id="frontimagepreview" src="" style="width: 100%" alt="Gallery Image" />						
						 </div>				
					</div> <!-- /.col -->
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<div class="thumbnail-view">	
								<img id="rearimagepreview" src="" style="width: 100%" alt="Gallery Image" />
							</div>
						 </div>				
					</div> <!-- /.col -->
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<div class="thumbnail-view">								
								<img id="rightimagepreview" src="" style="width: 100%" alt="Gallery Image" />
							</div>
						 </div>				
					</div> <!-- /.col -->
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<div class="thumbnail-view">						
								<img id="leftimagepreview" src="" style="width: 100%" alt="Gallery Image" />
							</div>
						 </div>				
					</div> <!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6">
						Front View
					</div>
					<div class="col-md-3 col-sm-6">
						Rear View
					</div>
					<div class="col-md-3 col-sm-6">
						Right View
					</div>
					<div class="col-md-3 col-sm-6">
						Left View
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<input name="frontfile" class="filestyle" data-size="sm" id="frontfile_v" type = "file">
					</div>
					<div class="col-md-3 col-sm-6">
						<input name="rearfile" class="filestyle" data-size="sm" id="rearfile_v" id="pnp_image_file" type = "file">
					</div>
					<div class="col-md-3 col-sm-6">
						<input name="rightfile" class="filestyle" data-size="sm" id="rightfile_v" id="pnp_image_file" type = "file">
					</div>
					<div class="col-md-3 col-sm-6">
						<input name="leftfile" class="filestyle" data-size="sm" id="leftfile_v" id="pnp_image_file" type = "file">
					</div>
				</div>		
				
				
				<div class="portlet" style="margin-top:20px;">
					<div class="portlet-header">
						<h2><i class="fa fa-user"></i><span class="break"></span>Basic Information</h2>
                        <ul class="portlet-tools pull-right">
                            <li>
                                <label id="violator_id_v">Violator ID here</label>
                            </li>
                         </ul>
					</div>
					<div class="portlet-content">
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> Alias </label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="alias_v" class="form-control" type="text" placeholder="Alias (Optional) "/>
							</div>
                            <div class="col-md-2 col-sm-6">
								<label> Gender</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<label class="radio-inline">
								  <input type="radio" name="gender_v" class="" value="1" checked> Male
								</label>
								<label class="radio-inline">
								  <input type="radio" name="gender_v" class="" value="0"> Female
								</label>
							</div>	
						</div>	
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> First Name</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="first_name_v" class="form-control" type="text" placeholder="First name"/>
							</div>
							<div class="col-md-2 col-sm-6">
								<label> Middle Name</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="middle_name_v" class="form-control" type="text" placeholder="Middle name"/>
							</div>
						</div>	
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> Last Name</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="last_name_v" class="form-control" type="text" placeholder="Last name"/>
							</div>
							<div class="col-md-2 col-sm-6">
								<label> Birthdate </label>
							</div>
							<div class="col-md-4 col-sm-6">
								<div id="birthdate_v" class="input-group input-append date" data-date-start-view="2">
									<input name="birthdate_v" class="form-control" type="text" placeholder="Birthday">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
                        <div class="row form-group">
                            <div class="col-md-2 col-sm-6">
                                <label> Place of Birth</label>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <input id="place_of_birth_v" name="place_of_birth" class="form-control" type="text" placeholder="Place of birth"/>
                            </div> 
                            <div class="col-md-2 col-sm-6">
                                <label> Nationality</label>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <input id="nationality_v" name="nationality" class="form-control" type="text" placeholder="Nationality"/>
                            </div>               
				        </div>
                        <div class="row form-group">
                            <div class="col-md-2 col-sm-6">
                                <label> Marital Status </label>
                            </div>
                            <div class="col-md-4 col-sm-6" id="marital_list_v">
                                <select id="marital_status_v" name="marital_status" class="form-control">
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>
                                    <option value="Widow/er">Widow/er</option>
                                    <option value="Separated">Separated</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <label> Occupation</label>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <input id="occupation_v" name="occupation" class="form-control" type="text" placeholder="Occupation"/>	
                            </div>
                        </div>
                        <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
									<label> Tel No</label>
								</div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="tel_no_v" name="tel_no_v" class="form-control num_only" type="text" placeholder="Telephone Number"/>	
                                </div>
                            </div>
					</div>
				</div>
                
				<div class="portlet" style="margin-top:20px;">
					<div class="portlet-header">
						<h2><i class="fa fa-automobile"></i><span class="break"></span>Location</h2>
					</div>
					<div class="portlet-content">
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> Street</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="street_name_v" class="form-control" type="text" placeholder="Street"/>
							</div>	
							<div class="col-md-2 col-sm-6">
								<label> Barangay</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="barangay_name_v" class="form-control" type="text" placeholder="Barangay"/>
							</div>	
						</div>	
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> City</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="city_name_v" class="form-control" type="text" placeholder="City"/>
							</div>		
						</div>			
					</div>
				</div>
                 <div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-university"></i><span class="break"></span>Educational Background</h2>
						</div>
						<div class="portlet-content">
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                        <label> Educational Attainment </label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input name="educational_attainment" id="educational_attainment_v" class="form-control" type="text" placeholder="Education attainment">							
                                </div>	
                                <div class="col-md-2 col-sm-6">
                                        <label> Name of School </label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input name="name_of_school" id="name_of_school_v" class="form-control" type="text" placeholder="Name of school">							
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                        <label> Location of School </label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input name="location_of_school" id="location_of_school_v" class="form-control" type="text" placeholder="Location of school">							
                                </div>
                            </div>                
                        </div>
                    </div>
				<div class="portlet" style="margin-top:20px;">
					<div class="portlet-header">
						<h2><i class="fa fa-male"></i><span class="break"></span>Physical Appearance</h2>
					</div>
					<div class="portlet-content">
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> Weight (kg) </label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="weight_v" class="form-control decimal_only" type="text" placeholder="Weight"/>
							</div>	
						<div class="col-md-2 col-sm-6">
								<label> Height (ft)</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="height_v" class="form-control decimal_only" type="text" placeholder="Height"/>
							</div>									
						</div>			
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> Eyes Color</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="eyes_color_v" class="form-control" type="text" placeholder="Eyes color"/>
							</div>	
							<div class="col-md-2 col-sm-6">
								<label> Hair Color</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="hair_color_v" class="form-control" type="text" placeholder="Hair Color"/>
							</div>									
						</div>	
						<div class="row form-group">
							<div class="col-md-2 col-sm-6">
								<label> Identifying Marks</label>
							</div>
							<div class="col-md-4 col-sm-6">
								<input id="ident_marks_v" class="form-control" type="text" placeholder="Identifying marks"/>
							</div>	
                            <div class="col-md-2 col-sm-6">
                                <label> Complexion</label>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <input id="complexion_v" class="form-control" type="text" placeholder="Complexion"/>
                            </div>  
						</div>	
					</div>					
				</div>
				
				<div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-home"></i><span class="break"></span>Family Background</h2>
						</div>
						<div class="portlet-content">
							<div class="row form-group">
								<div class="col-md-1 col-sm-6">
									<label> Mother Name</label>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<input id="mother_name_v" class="form-control" type="text" placeholder="Mother name"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Address</label>
								</div>
                                <div class="col-md-3 col-lg-3 col-sm-6">
									<input id="mother_address_v" class="form-control num_only" type="text" placeholder="Address"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Age</label>
								</div>
                                 <div class="col-md-3 col-lg-3 col-sm-6">
									<input id="mother_age_v" class="form-control num_only" type="text" maxlength="2" placeholder="Age"/>
								</div>
																	
							</div>	
                            <div class="row form-group">
                               <div class="col-md-1 col-sm-6">
									<label> Father Name</label>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<input id="father_name_v" class="form-control" type="text" placeholder="Father name"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Address</label>
								</div>
                                <div class="col-md-3 col-lg-3 col-sm-6">
									<input id="father_address_v" class="form-control" type="text" placeholder="Address"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Age</label>
								</div>
                                 <div class="col-md-3 col-lg-3 col-sm-6">
									<input id="father_age_v" class="form-control" type="text" maxlength="2" placeholder="Age"/>
								</div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6 col-lg-6">
                                    <h4 style="padding: 10px; border-bottom: 1px solid black;">Contact person in case of emergency</h4>
                                   
                                </div>
                            </div>
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Contact Person</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="contact_person_v" class="form-control" type="text" placeholder="Who to contact?"/>
								</div>	
								<div class="col-md-2 col-sm-6">
									<label> Relationship with the contact person</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="relationship_v" class="form-control" type="text" placeholder="Violator's relationship to contact person"/>
								</div>									
							</div>
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Contact Info</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="contact_info_v" class="form-control num_only" type="text" placeholder="Contact person's contact info"/>
								</div>		
							</div>								
						</div>
					</div>
                
				<div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-credit-card"></i><span class="break"></span>Credentials and ID's</h2>
						</div>
						<div class="portlet-content">
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                    <label> Driver's License</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="dr_license_v" class="form-control" type="text" placeholder="Driver's License"/>
                                </div>	 
                                <div class="col-md-2 col-sm-6">
                                    <label> Issued At</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="dr_issued_at_v" class="form-control" type="text" placeholder="Issued At..."/>
                                </div>	
                            </div>
                            <div class="row form-group">
                                <div class="col-md-offset-6 col-md-2 col-sm-6">
                                    <label> Issued On</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
									<div id="dr_issued_v" class="input-group input-append date" data-date-start-view="2">
										<input id="dr_issued_on_v" class="form-control" type="text" placeholder="Issued On...">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                    <label> Res Cert NR</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="res_cert_v" class="form-control" type="text" placeholder="Res Cert NR"/>
                                </div>	 
                                <div class="col-md-2 col-sm-6">
                                    <label> Issued At</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="res_cert_issued_at_v" class="form-control" type="text" placeholder="Issued At..."/>
                                </div>	
                            </div>
                            <div class="row form-group">
                                <div class="col-md-offset-6 col-md-2 col-sm-6">
                                    <label> Issued On</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
									<div id="res_cert_issued_v" class="input-group input-append date" data-date-start-view="2">
										<input id="res_cert_issued_on_v" class="form-control" type="text" placeholder="Issued On...">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                    <label> Other ID Cards</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
								    <input id="other_ids_v" class="form-control" type="text" placeholder="Other ID Cards..."/>
								</div>
                                 <div class="col-md-2 col-sm-6">
                                    <label> ID numbers...</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
								    <input id="id_numbers_v" class="form-control" type="text" placeholder="ID Numbers..."/>
								</div>
                            </div>
                        </div>
                    </div>
				<div class="portlet" style="margin-top:20px;">
					<div class="portlet-header">
						<h2><i class="fa fa-bank"></i><span class="break"></span>Charges</h2>
					</div>
					<div class="portlet-content">
						<div class="well">
							<p id="charges-details"> </p>
						</div>
						<p class="text-muted">Choosing other sets of charges will override the previous ones.</p>		
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<table id="view_charges_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable">
									<thead>
										<tr>
											<th class="">Select</th>
											<th class="">Charges ID</th>
											<th class="">Article</th>
											<th class="">Section</th>
											<th class="">Desc</th>
										</tr>	
									</thead>
								</table>
							</div>
						</div>									
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" id="updateViolatorButton" class="btn btn-default">Save Changes</button>
				<button type="button" id="closeViewViolatorModalButton" class="btn btn-default" data-dismiss="modal">Close</button>	
			</div>
		</div>		
	</div>
</div>