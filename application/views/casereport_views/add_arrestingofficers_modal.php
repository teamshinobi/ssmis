<div class="modal fade" id="arresttingModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Choose arresting officers.</h4>
			</div>
			<div class="modal-body">
				<!-- Group Members -->		
				<div class="portlet">
					<div class="portlet-header">
							<h2><i class="fa fa-group"></i><span class="break"></span>Members</h2>
					</div>
					<div class="portlet-content">
						<table id="arresting_officers_table" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
							<thead>
								<tr>
									<th class="">Add</th>
									<th class="">Batch ID</th>
									<th class="">Rank</th>
									<th class="">Full Name</th>
									<th class="">Picture</th>
								
								</tr>	
							</thead>
						</table>	
					</div>
				</div>				
			</div>
			
			<div class="modal-footer">
				<button type="button" id="addSelectedArrestingButton" class="btn btn-default">Add Selected</button>
				<button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>	
			</div>
			
		</div>
	</div>
</div>	