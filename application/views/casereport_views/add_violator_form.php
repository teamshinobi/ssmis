<div class="modal fade" id="addViolatorModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
                <p style="font-size: 15px; font-weight:bold;"> Fill-up fields with <span class="req-ast" style="font-size: 20px;"> * </span> </p>
			</div>
			<div id="add_violator_form_div" class="modal-body">
					<div class="portlet">
						<div class="portlet-header">
							<h2><i class="fa fa-image"></i><span class="break"></span>Mugshots</h2>
						</div>
						<div class="portlet-content">
							<div class="row">
								<div class="col-md-3 col-sm-6">
									<div class="thumbnail">
										<img id="frontpreview" src="<?php echo base_url() . 'public/resources/photos/default_picture.jpg'?>"/>
									</div> <!-- /.thumbnail -->
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="thumbnail">
										<img id="rearpreview" src="<?php echo base_url() . 'public/resources/photos/default_picture.jpg'?>"/>
									</div> <!-- /.thumbnail -->
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="thumbnail">
										<img  id="rightpreview" src="<?php echo base_url() . 'public/resources/photos/default_picture.jpg'?>"/>
									</div> <!-- /.thumbnail -->
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="thumbnail">
										<img  id="leftpreview" src="<?php echo base_url() . 'public/resources/photos/default_picture.jpg'?>"/>
									</div> <!-- /.thumbnail -->
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-sm-6">
									Front View
								</div>
								<div class="col-md-3 col-sm-6">
									Rear View
								</div>
								<div class="col-md-3 col-sm-6">
									Right View
								</div>
								<div class="col-md-3 col-sm-6">
									Left View
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-sm-6">
									<input name="frontfile" class="filestyle" data-input="false" data-size="sm" id="frontfile" type = "file">
								</div>
								<div class="col-md-3 col-sm-6">
									<input name="rearfile" class="filestyle" data-input="false" data-size="sm" id="rearfile" id="pnp_image_file" type = "file">
								</div>
								<div class="col-md-3 col-sm-6">
									<input name="rightfile" class="filestyle" data-input="false" data-size="sm" id="rightfile" id="pnp_image_file" type = "file">
								</div>
								<div class="col-md-3 col-sm-6">
									<input name="leftfile" class="filestyle" data-input="false" data-size="sm" id="leftfile" id="pnp_image_file" type = "file">
								</div>
							</div>		
						</div>
					</div>
					
					<div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-user"></i><span class="break"></span>Basic Information</h2>
                            <ul class="portlet-tools pull-right">
                                <li>
                                    <label id="violator-id">Violator ID here</label>
                                </li>
                            </ul>
						</div>
						<div id="basicInfoDiv" class="portlet-content">
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Alias </label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="alias" class="form-control" type="text" placeholder="Alias (Optional) " maxlength="60"/>
								</div>
                                <div class="col-md-2 col-sm-6">
									<label>Gender</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<label class="radio-inline">
									  <input type="radio" name="gender" class="" value="1" checked> Male
									</label>
									<label class="radio-inline">
									  <input type="radio" name="gender" class="" value="0"> Female
									</label>
								</div>	
							</div>	
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
                                    <label> <span class="req-ast"> * </span> First Name</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="first_name" class="form-control" type="text" placeholder="First name" maxlength="120"/>
								</div>
								<div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Middle Name</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="middle_name" class="form-control" type="text" placeholder="Middle name" maxlength="120"/>
								</div>
							</div>	
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Last Name</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="last_name" class="form-control" type="text" placeholder="Last name" maxlength="120"/>
								</div>
								<div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Birthdate </label>
								</div>
								<div class="col-md-4 col-sm-6">
									<div id="birthdate" class="input-group input-append date" data-date-start-view="2">
										<input name="birthdate" class="form-control" type="text" placeholder="Birthday">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
							<div class="row form-group">
                                <div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Place of Birth</label>
								</div>
                                <div class="col-md-4 col-sm-6">
									<input maxlength="60" id="place_of_birth" name="place_of_birth" class="form-control" type="text" placeholder="Place of birth"/>
								</div> 
                                <div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Nationality</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="nationality" name="nationality" class="form-control" type="text" placeholder="Nationality"/>
								</div>               
							</div>
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
									<label> Marital Status </label>
								</div>
                                <div class="col-md-4 col-sm-6" id="marital_list">
                                    <select id="martial_status" name="marital_status" class="form-control">
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Widow/er">Widow/er</option>
                                        <option value="Separated">Separated</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Occupation</label>
								</div>
                                <div class="col-md-4 col-sm-6">
                                    <input maxlength="60" id="occupation" name="occupation" class="form-control" type="text" placeholder="Occupation"/>	
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
									<label> Tel No</label>
								</div>
                                <div class="col-md-4 col-sm-6">
                                    <input maxlength="12" id="tel_no" name="tel_no" class="form-control num_only" type="text" placeholder="Telephone Number"/>	
                                </div>
                            </div>
						</div>
					</div>
                    <div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-automobile"></i><span class="break"></span>Location</h2>
						</div>
						<div id="locationDiv" class="portlet-content">
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Street</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input id="street_name" class="form-control" type="text" placeholder="Street"/>
								</div>	
								<div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> Barangay</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="barangay_name" class="form-control" type="text" placeholder="Barangay"/>
								</div>	
							</div>	
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> <span class="req-ast"> * </span> City</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="city_name" class="form-control" type="text" placeholder="City"/>
								</div>		
							</div>			
						</div>
					</div>
                    <div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-university"></i><span class="break"></span>Educational Background</h2>
						</div>
						<div class="portlet-content">
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                        <label> Educational Attainment </label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input maxlength="100" name="educational_attainment" id="educational_attainment" class="form-control" type="text" placeholder="Education attainment">							
                                </div>	
                                <div class="col-md-2 col-sm-6">
                                        <label> Name of School </label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input maxlength="100" name="name_of_school" id="name_of_school" class="form-control" type="text" placeholder="Name of school">							
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                        <label> Location of School </label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input name="location_of_school" id="location_of_school" class="form-control" type="text" placeholder="Location of school">							
                                </div>
                            </div>                
                        </div>
                    </div>
					<div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-male"></i><span class="break"></span>Physical Appearance</h2>
						</div>
						<div class="portlet-content">
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Weight (kg) </label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="6" id="weight" class="form-control decimal_only" type="text" placeholder="Weight"/>
								</div>	
							<div class="col-md-2 col-sm-6">
									<label> Height (ft)</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="6" id="height" class="form-control decimal_only" type="text" placeholder="Height"/>
								</div>									
							</div>			
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Eyes Color</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="eyes_color" class="form-control" type="text" placeholder="Eyes color"/>
								</div>	
								<div class="col-md-2 col-sm-6">
									<label> Hair Color</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="hair_color" class="form-control" type="text" placeholder="Hair Color"/>
								</div>									
							</div>	
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Identifying Marks</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="ident_marks" class="form-control" type="text" placeholder="Identifying marks"/>
								</div>
                                <div class="col-md-2 col-sm-6">
									<label> Complexion</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="complexion" class="form-control" type="text" placeholder="Complexion"/>
								</div>     
							</div>	
                            
						</div>						
					</div>
					
					<div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-home"></i><span class="break"></span>Family Background</h2>
						</div>
						<div class="portlet-content">
							<div class="row form-group">
								<div class="col-md-1 col-sm-6">
									<label> Mother Name</label>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<input maxlength="60" id="mother_name" class="form-control" type="text" placeholder="Mother name"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Address</label>
								</div>
                                <div class="col-md-3 col-lg-3 col-sm-6">
									<input maxlength="100" id="mother_address" class="form-control" type="text" placeholder="Address"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Age</label>
								</div>
                                 <div class="col-md-3 col-lg-3 col-sm-6">
									<input maxlength="2" id="mother_age" class="form-control num_only"  maxlength="2" type="text" placeholder="Age"/>
								</div>
																	
							</div>	
                            <div class="row form-group">
                               <div class="col-md-1 col-sm-6">
									<label> Father Name</label>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<input maxlength="60" id="father_name" class="form-control" type="text" placeholder="Father name"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Address</label>
								</div>
                                <div class="col-md-3 col-lg-3 col-sm-6">
									<input maxlength="100" id="father_address" class="form-control" type="text" placeholder="Address"/>
								</div>	
                                <div class="col-md-1 col-sm-6">
									<label> Age</label>
								</div>
                                 <div class="col-md-3 col-lg-3 col-sm-6">
									<input maxlength="2" id="father_age" class="form-control num_only" maxlength="2" type="text" placeholder="Age"/>
								</div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6 col-lg-6">
                                    <h4 style="padding: 10px; border-bottom: 1px solid black;">Contact person in case of emergency</h4>
                                   
                                </div>
                            </div>
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Contact Person</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="contact_person" class="form-control" type="text" placeholder="Who to contact?"/>
								</div>	
								<div class="col-md-2 col-sm-6">
									<label> Relationship with the contact person</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="60" id="relationship" class="form-control" type="text" placeholder="Violator's relationship to contact person"/>
								</div>									
							</div>
							<div class="row form-group">
								<div class="col-md-2 col-sm-6">
									<label> Contact Info</label>
								</div>
								<div class="col-md-4 col-sm-6">
									<input maxlength="12" id="contact_info" class="form-control num_only" type="text" placeholder="Contact person's contact info"/>
								</div>		
							</div>								
						</div>
					</div>
                
                    <div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-credit-card"></i><span class="break"></span>Credentials and ID's</h2>
						</div>
						<div class="portlet-content">
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                    <label> Driver's License</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="dr_license" class="form-control" type="text" placeholder="Driver's License"/>
                                </div>	 
                                <div class="col-md-2 col-sm-6">
                                    <label> Issued At</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="dr_issued_at" class="form-control" type="text" placeholder="Issued At..."/>
                                </div>	
                            </div>
                            <div class="row form-group">
                                <div class="col-md-offset-6 col-md-2 col-sm-6">
                                    <label> Issued On</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
									<div id="dr_issued" class="input-group input-append date" data-date-start-view="2">
										<input id="dr_issued_on" class="form-control" type="text" placeholder="Issued On...">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                    <label> Res Cert NR</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="res_cert" class="form-control" type="text" placeholder="Res Cert NR"/>
                                </div>	 
                                <div class="col-md-2 col-sm-6">
                                    <label> Issued At</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <input id="res_cert_issued_at" class="form-control" type="text" placeholder="Issued At..."/>
                                </div>	
                            </div>
                            <div class="row form-group">
                                <div class="col-md-offset-6 col-md-2 col-sm-6">
                                    <label> Issued On</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
									<div id="res_cert_issued" class="input-group input-append date" data-date-start-view="2">
										<input id="res_cert_issued_on" class="form-control" type="text" placeholder="Issued On...">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-2 col-sm-6">
                                    <label> Other ID Cards</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
								    <input id="other_ids" class="form-control" type="text" placeholder="Other ID Cards..."/>
								</div>
                                 <div class="col-md-2 col-sm-6">
                                    <label> ID numbers...</label>
                                </div>
                                <div class="col-md-4 col-sm-6">
								    <input id="id_numbers" class="form-control" type="text" placeholder="ID Numbers..."/>
								</div>
                            </div>
                        </div>
                    </div>
                
					<div class="portlet" style="margin-top:20px;">
						<div class="portlet-header">
							<h2><i class="fa fa-bank"></i><span class="break"></span>Charges</h2>
						</div>
						<div class="portlet-content">
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<table id="ra_table" class="table table-striped table-bordered table-hover table-highlight table-checkable">
										<thead>
											<tr>
												<th class="">Select</th>
												<th class="">Charges ID</th>
												<th class="">Article</th>
												<th class="">Section</th>
												<th class="">Desc</th>
											</tr>	
										</thead>
									</table>
								</div>
							</div>		
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="addViolatorButton" class="btn btn-default">Add Violator</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>	
			
			
			