<div class="modal fade" id="addDistributionModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-folder"></i>&nbsp; Distribution</h4>
			</div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-3">
                        <label> Distributed to: </label>
                    </div>
                    <div class="col-md-6">
                        <input id="distributed_to" class="form-control" type="text" type="text"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label> Copies: </label>
                    </div>
                    <div class="col-md-5">
                        <input id="copies" class="form-control" type="text" type="text"/>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" id="addDistributionButton"class="btn btn-default">Add</button>
                <button type="button" id="closeDistribution"class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>