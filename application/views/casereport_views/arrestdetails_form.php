	
	<div class="portlet">
		<div class="portlet-header">
			<h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Arrest Details</h2>
			<ul class="portlet-tools pull-right">
				<li>
					<label id="case-id">Case ID here</label>
				</li>
			</ul>
		</div>	
		<div class="portlet-content">
		
			<div class = "row row-pad">
			
				<div class = "col-lg-2">
					<label>IS Number (if possible):</label>
				</div>
				<div class = "col-lg-4">
						<input id="is_number" type="text" class="form-control" name="is_number">
				</div>
				<div class = "col-lg-2">
					<label>Case Progress:</label> 
				</div>
				<div class = "col-lg-4">
					<select id="progress" name="progress" class="form-control">
						<option value="Pending">Pending</option>
						<option value="Pending on Inquest Office">Pending on Inquest Office</option>
						<option value="Pending on Court">Pending on Court</option>
                        <option value="Closed">Closed</option>
						<option value="Dismissed">Dismissed</option>
					</select>
						
				</div>
			</div>
		
			<div class = "row row-pad">
				<div class = "col-lg-2">
					<label>Blottery Entry No.:</label> 
				</div>
				<div class = "col-lg-4">
					<input name="blotter_entry_nr" type="text" class="form-control" id="blotter_entry_nr" 
					placeholder="Enter Blotter No." maxlength="20" focusable>
				</div>
				<div class = "col-lg-2">	
						<label>Nature of Arrest:</label>  
				</div>	
				<div class = "col-lg-4">
					<select id="nature" name="nature" class="form-control">
						<option value="Buy Bust">Buy Bust</option>
						<option value="With Warrant of Arrest">With Warrant of Arrest</option>
						<option value="Search Warrant">Search Warrant</option>
						<option>On Chanced</option>
						<option>Incidental Arrest</option>
					</select>
				</div>
			</div>
			<div class = "row row-pad">
				<div class = "col-lg-2">	
					<label>Time of Arrest:</label>  
				</div>	
				<div class = "col-lg-4">
				  <div class="input-group bootstrap-timepicker">
						<input name="time_of_arrest" id="timepicker" type="text" class="form-control" placeholder="Time of Arrest">
						<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
					</div>	
				</div>
				<div class = "col-lg-2">
					<label>Date of Arrest:</label> 
				</div>
				<div class = "col-lg-4">
					<div id="datepicker" class="input-group date datepicker">
						<input id="date_of_arrest" name="date_of_arrest" class="form-control" type="text" placeholder="Date Of Arrest"/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
				</div>	
			</div>
			<div class = "row row-pad">
				<div class = "col-lg-2">
					<label>Street:</label> 
				</div>
				<div class = "col-lg-4">
					<input name="street_name" type="text" class="form-control" 
						id="street" placeholder="Street" focusable/>
				</div>
				<div class = "col-lg-2">
					<label>City:</label> 
				</div>
				<div class = "col-lg-4">
					<input name="city_name" id="city" type="text" class="form-control" 
						placeholder="City" maxlength="120" focusable/>
				</div>
			</div>
			<div class = "row row-pad">
			     <div class = "col-lg-2">
					<label>Barangay:</label> 
                   
				</div>
             	<div class = "col-lg-4 padding">
					<div class="input-group">
						<input id="barangay" type="text" class="form-control" placeholder = "Barangay">
						<span class="input-group-btn">
							<button id="selectBarangayButton" class="btn btn-md btn-default" type="button">+</button>
						</span>
					</div>
				</div>
				<div class = "col-lg-2">
					<label>Lawyer:</label> 
				</div>
				<div class = "col-lg-4">
						<input name="lawyer" id="lawyer" type="text" class="form-control" 
						placeholder="Lawyer" maxlength="60" > 
				</div>
				
			</div>
         
            <div class = "row row-pad">
                <div class = "col-lg-2">
					<label>Medical Examination Conducted at:</label> 
				</div>
                <div class = "col-lg-4">
						<input name="med_at" id="med_at" type="text" class="form-control" 
						placeholder="Conducted at..."> 
				</div>
                <div class = "col-lg-2">
					<label>By DR.</label> 
				</div>
                <div class = "col-lg-4">
						<input name="dr" id="dr" type="text" class="form-control" 
						placeholder="Doctor's Name"  maxlength="60"> 
				</div>     
            </div>
             <div class = "row">
               <div class = "col-lg-4 col-lg-offset-2">
					<div id="medDatePicker" class="input-group date datepicker">
						<input id="med_on" name="med_on" class="form-control" type="text" placeholder="Conducted on..."/>
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
				</div>	
                    
            </div>
			<div class = "row row-pad">
				<div class = "col-lg-2">
					<label>Arresting Officer:</label> 
				</div>
				<div class = "col-lg-4">
				<div class="input-group">
						<textarea id="addArrestingOfficersTextField" class="form-control" placeholder = "Select Arresting Officer"></textarea>
						<span class="input-group-btn">
							<button  id="addArrestingOfficersButton" class="btn btn-md btn-default" type="button">+</button>
						</span>
				</div>
				</div>
				
				<div class = "col-lg-2 padding">
					<label>Investigator:</label> 
				</div>
				<div class = "col-lg-4 padding">
					<div class="input-group">
						<input id="investigator" type="text" class="form-control" placeholder = "Investigator">
						<span class="input-group-btn">
							<button id="addInvestigatorButton" class="btn btn-md btn-default" type="button">+</button>
						</span>
					</div>
				</div>					
			</div>
                        
            <div class = "row row-pad">
				<div class = "col-lg-2">
					<label>Fingerprint taken by:</label> 
				</div>
                <div class = "col-lg-4 padding">
					<div class="input-group">
						<input id="fing_taken_by" type="text" class="form-control" placeholder = "Who took fingerprints?">
						<span class="input-group-btn">
							<button id="addFingerManButton" class="btn btn-md btn-default" type="button">+</button>
						</span>
					</div>
				</div>	
                <div class = "col-lg-2">
					<label>Photo taken by:</label> 
				</div>
                <div class = "col-lg-4 padding">
					<div class="input-group">
						<input id="photo_taken_by" type="text" class="form-control" placeholder = "Who took photos?">
						<span class="input-group-btn">
							<button id="addPhotographerButton" class="btn btn-md btn-default" type="button">+</button>
						</span>
					</div>
				</div>	
            </div>
            
            
			<div class = "row form-group">
				<div class = "col-lg-12 padding">
					<label class="control-label form-label" for="summary">More details</label>
					<hr/>
				</div>
			</div>
			<div class = "row form-group">
				<div class = "col-lg-2">
					<label>Operation Title:</label> 
				</div>
				<div class = "col-lg-4">
						<input id="operation_title" name="operation" type="text" class="form-control" 
						maxlength="60" placeholder="Title">
				</div>
			</div>
			<div class = "row form-group">
				<div class="col-lg-12">
					 <div id="content" class="summernote" >
                       
                    </div>
                    
                    <hr/>
				</div>
			</div>
            <div class = "row form-group">
				<div class = "col-lg-2">
					<label>Upload case scene</label> 
				</div>
				<div class = "col-lg-4">
				    	<input name="casefile" class="filestyle" data-input="false" data-size="sm" id="casefile" type = "file">				
				</div>
			</div>
            <div class = "row form-group">
                <div class = "col-lg-12">
                    <div style="border:1px solid black;" class="thumbnail img-thumbnail">
				        <img  id="casepreview" src="<?php echo base_url() . 'public/resources/default_img.jpg'?>"/>
                    </div>
                </div>
			</div>
		</div>
	</div>
	