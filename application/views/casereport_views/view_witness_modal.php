<div class="modal fade" id="viewWitnessModal"  tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><i class="fa fa-lg fa-eye"></i> View/Edit Witness</h4>
                <p id="witness_id" class="text-muted"></p>
            </div>

            <div class="modal-body" id="add_witness">
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            Name    
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input id="v_witnessName" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            Address
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input id="v_witnessAddress" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            Contact Info
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input id="v_witnessContact" type="text" class="form-control" />
				    </div>
                </div>
            </div>			
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="saveWitnessButton" class="btn btn-primary">Save Changes</button>
            </div>	
            
        </div>
    </div>
</div>	