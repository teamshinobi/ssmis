	<div class="portlet">
		<div class="portlet-header">
			<h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Violators</h2>
			<ul class="portlet-tools pull-right">
				<li>
					<button title="Add new violator"  id="showViolatorFormButton" type="button" class="btn btn-md btn-default">
						<span><i class="fa fa-plus"></i></span>
					</button>
					<button title="Add violator(s) that are already recorded" id="showExistingViolatorsButton" type="button" class="btn btn-md btn-default">
						<span><i class="fa fa-group"></i></span>
					</button>
				</li>
			</ul>
		</div>	
		<div class="portlet-content">
			<div class="table-responsive">
				<table id="violators_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
					<thead>
						<tr>
							<th class="">Violator ID</th>
							<th class="">Full Name</th>
							<th class="">Charges</th>
							<th class="">Picture</th>
						</tr>	
					</thead>				
				</table>		
			</div>
			<p class="text-primary">Click a row in a table to view violator.</p>
		</div>
	</div>