<div class="modal fade" id="addWitnessModal"  tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><i class="fa fa-lg fa-eye"></i> Add Witness</h4>
                <p id="witness_id" class="text-muted"></p>
            </div>

            <div class="modal-body" id="add_witness">
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            Name    
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input maxlength="60" id="witnessName" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            Address
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input maxlength="100" id="witnessAddress" type="text" class="form-control" />
				    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-2 col-md=2 col-lg-offset-2 col-md-offset-2">
                        <label>
                            Contact Info
                        </label>
				    </div>
                    <div class="col-lg-6 col-md=6">
                        <input maxlength="12" id="witnessContact" type="text" class="form-control num_only" />
				    </div>
                </div>
            </div>			
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="addWitnessButton" class="btn btn-primary">Add Witness</button>
            </div>	
            
        </div>
    </div>
</div>	