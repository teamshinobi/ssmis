<div class="modal fade" id="showSuppAffidavitsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-paperclip"></i> Supplemental Affidavits</h4>	
			</div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive">
                            <table id="supp_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
                                <thead>
                                    <tr>
                                        <th class="">ID</th>
                                        <th class="">Date</th>
                                        <th class="">Narrators</th>
                                        <th class="">Action</th>
                                    </tr>	
                                </thead>				
                            </table>		
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 <button type="button" id="closeViewSuppAffidavitsModal" class="btn btn-default" data-dismiss="modal">Close</button>		
            </div>
        </div>      
    </div>
</div>