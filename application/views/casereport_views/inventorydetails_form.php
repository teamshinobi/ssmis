	
	<div class="portlet">
		<div class="portlet-header">
			<h2><i class="glyphicon glyphicon-book"></i><span class="break"></span>Inventory</h2>
		</div>	
		<div class="portlet-content">
			<div class="row form-group">			
				<div class="col-md-2 col-sm-6">
					<label>Select seizing officer: </label>
				</div>
				<div class="col-md-4 col-sm-5">
					<input id="seizing_officer" class="form-control" type="text" placeholder="Seizing officer"/>
				</div>
				<div class="col-md-2 col-sm-1">
					<button type="button" id="showSeizingOfficerFormButton" class="btn btn-default"><span><i class="fa fa-plus"></i></span></button>
				</div>
			</div>
            <div class="row">
				<div class="col-md-12 col-sm-12">
                    <h4><i class="fa fa-archive"></i>  Item Seized</h4>
                    <hr style="margin-bottom: 0px;"/>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<button id="showItemFormButton" type="button" class="btn btn-default">Add Item</button>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="table-responsive">
						<table id="item_seized_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
							<thead>
								<tr>
									<th class="">Item ID</th>
									<th class="">Description</th>
									<th class="">Qty</th>	
									<th class="">Markings</th>
									
								</tr>	
							</thead>				
						</table>		
					</div>
				</div>
			</div>
			 <div class="row">
				<div class="col-md-12 col-sm-12">
                    <h4><i class="fa fa-leaf"></i>  Narcotics Seized</h4>
                    <hr style="margin-bottom: 0px;"/>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<button id="showNarcoticsItemFormButton" type="button" class="btn btn-default">Add Narcotics</button>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="table-responsive">
						<table id="narcotics_seized_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer" role="grid">
							<thead>
								<tr>
									<th class="">Narcotics Item ID</th>
									<th class="">Description</th>
									<th class="">Qty</th>	
									<th class="">Markings</th>
									
								</tr>	
							</thead>				
						</table>		
					</div>
				</div>
			</div>
	   </div>
</div>

