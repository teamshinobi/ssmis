<div class="modal fade" id="editStatusModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Edit Status</h4>
				
			</div>
            <div class="modal-body">
                 <div class="row form-group">
                    <div class="col-md-2 col-md-offset-2">
                        <label> Status: </label>
                    </div>
                    <div class="col-md-6">
                        <input id="status_ev" class="form-control" type="text"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
				<button type="button" id="editStatusButton" class="btn btn-default">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>		
			</div>
            
		</div>
	</div>
</div>