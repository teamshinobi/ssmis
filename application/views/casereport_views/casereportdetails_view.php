
    <div id="content">

		<div id="content-header">
			<h1>Case Report Details<button id="caseReport" style="float:right;margin-right:20px;" id="clearViolatorButton" type="button" class="btn btn-default">View FULL Details</button></h1>
			
		</div> <!-- #content-header -->	
		<div id="content-container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                    <div id="photosCarousel" class="carousel slide" data-ride="carousel" >
              
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-briefcase"></i>  Case Details </h4>
                    <hr style="margin-bottom: 0px;">
                </div>
            </div>
            
            <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label>Case ID </label>
                    <p id="case_id"></p>
                </div>
                 <div class="co-lg-4 col-md-4">
                    <label>IS Number </label>
                    <p id="is_number"></p>
                </div>
                <div class="co-lg-4 col-md-4">
                    <label>Nature </label>
                    <p id="nature"></p>
                </div>
            </div>
             <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label>Blotter Entry </label>
                    <p id="blotter"></p>
                </div>
                 <div class="co-lg-4 col-md-4">
                    <label>Progress </label>
                    <p id="progress"></p>
                </div>
               
            </div>
            <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label>Operation Title</label>
                    <p id="title"></p>
                </div>    
            </div>
            <div class="row row-indent">
                <div class="co-lg-11 col-md-11">
                    <label>Summary</label>
                    <p id="summary"></p>
                </div>    
            </div>
            
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-tag"></i>  Arrest Details </h4>
                <hr style="margin-bottom: 0px;">
                </div>
            </div>
            <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label><i class="fa fa-calendar"></i> Date of arrest</label>
                    <p id="date_of_arrest"></p>
                </div>   
                <div class="co-lg-4 col-md-4">
                    <label><i class="fa fa-clock-o"></i> Time of arrest</label>
                    <p id="time_of_arrest"></p>
                </div>   
            </div>
            
            <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label><i class="fa fa-map-marker"></i> Location</label>
                    <p id="location"></p>
                </div>   
              
            </div>
            
            
            
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-group"></i>  Violators Invovled </h4>
               <hr style="margin-bottom: 0px;">
                </div>
            </div>
            <div class="row row-indent">
                
                <div class="co-lg-12 col-md-12">
                    <p>The following is the list of violators in this case 
                        along with their charges and items collected from them.
                    </p>
                    <ul id="violatorsList">
                        
                    </ul>
                </div>
            </div>
            
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-fire"></i>  Arresting officers </h4>
                    <hr>
                </div>
            </div>
            
            <div class="row row-indent">
                <div class="co-lg-12 col-md-12">
                    <ul id = "arrestingList">
                    
                    </ul>
                </div>
            </div>
            
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-paperclip"></i>  Attachments </h4>
                    <hr style="margin-bottom: 0px;">
                </div>
            </div>
             <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Spot Report </label>
                    <p id="spot"></p>
                </div>
                <div class="co-lg-4 col-md-4">
                    <label>Pre-operation Report</label>
                    <p id="preop"></p>
                </div>
                <div class="co-lg-4 col-md-4">
                    <label>Coordination Form</label>
                    <p id="coord"></p>
                </div>
            </div>
            
            <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Main Affidavit </label>
                    <p id="main"></p>
                </div>
                <div class="co-lg-4 col-md-4">
                    <label>Supplemental Affidavit</label>
                    <p id="supp"></p>
                </div>
            </div>
            <div class="row row-indent" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <label> Scanned Documents </label>
                    <hr style="margin-bottom: 0px;">
                </div>
            </div>
            <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Laboratory Examination</label>
                  
                </div>
            </div>
            <div class="row row-indent" id="labDocuments">
                
            </div>
            <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Medical Examination</label>
                    
                </div>
            </div>
            <div class="row row-indent" id="medDocuments">
                
            </div>
            <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Search Warrant</label>
                    
                </div>
            </div>
           
            <div class="row row-indent" id="searchDocuments">
                
            </div>
            
            <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Warrant of Arrest</label>
                    
                </div>
            </div>
           
            <div class="row row-indent" id="warrantDocuments">
                
            </div>
            <div class="row row-indent" >
                <div class="co-lg-4 col-md-4">
                    <label>Others </label>
                    
                </div>
            </div>
            <div class="row row-indent" id="othersDocuments">
                
            </div>
            
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-check"></i>  Other information </h4>
                    <hr style="margin-bottom: 0px;">
                </div>
            </div>
             <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label> Booked by </label>
                    <p id="booked_by"></p>
                </div>
                 <div class="co-lg-4 col-md-4">
                    <label> Case Investigator </label>
                    <p id="investigator"></p>
                </div>
            </div>
            <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label> Seizing Officers </label>
                    <ul id="seizing">
                    
                    </ul>
                </div>
            </div>
             <div class="row row-indent">
                <div class="co-lg-4 col-md-4">
                    <label> Fingerprints Taken By:  </label>
                    <p id="fingerman"></p>
                </div>
                <div class="co-lg-4 col-md-4">
                    <label> Photos Taken By:  </label>
                    <p id="photographer"></p>
                </div> 
            </div>
          
            
            <div class="row">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-leaf"></i>  Status of Evidences </h4>
                    <span><p style="margin-left: 20px;" class="text-muted pull-right clear-fix"><a id="edit_status"> Edit  </a></p></span>
                    <hr style="margin-bottom: 0px;">
                </div>
            </div>
            <div class="row row-indent">
                <div class="co-lg-12 col-md-12">
                    <p  style="margin-top: 0px;margin-bottom: 10px;" id="status_ev_p"></p>
                </div> 
            </div>  
            <div class="row" style="margin-top: 40px;">
                <div class="co-lg-12 col-md-12">
                    <h4><i class="fa fa-legal"></i>  CC# and Judge</h4>
                    <span><p style="margin-left: 20px;" class="text-muted pull-right clear-fix"><a id="edit_cc"> Edit  </a></p></span>
                    <hr style="margin-bottom: 0px;">
                </div>
            </div>  
            <div class="row row-indent">
                <div class="co-lg-12 col-md-12">
                    <p  style="margin-top: 0px;margin-bottom: 10px;" >
                        <strong>CC#: </strong><span id="cc"></span>
                    </p>
                    
                    <p  style="margin-top: 0px;margin-bottom: 10px;" >
                        <strong>Judge: </strong><span id="cc_judge"></span>
                    </p>
                    
                    
                </div> 
            </div> 
            
            <div class="row row-indent" style="margin-top:40px !important;">
                <div class="co-lg-12 col-md-12">
                    <p> **Copies of this case have been distributed to the following: &nbsp;
                        <span id="distributions" style="font-size: 16px"> 
                        </span>         
                    </p>
                    
                    <p class="text-muted"><a id="distribute"> Add distribution </a></p>
                </div>
            </div>
		</div>
	</div>
</div>