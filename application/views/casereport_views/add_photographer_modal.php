<div class="modal fade" id="photographerModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Photos Taken By</h4>	
			</div>
			<div class="modal-body">
				<div class="portlet span9">
					<div class="portlet-header">
						<h2><i class="fa fa-group"></i><span class="break"></span>Members</h2>
					</div>
					<div class="portlet-content">
						<table id="photographer_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
							<thead>
								<tr>
									<th class="">Batch ID</th>
									<th class="">Rank</th>
									<th class="">Full Name</th>
									<th class="">Picture</th>
									<th class="">Choose</th>
								</tr>	
							</thead>
						</table>	
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" id="saveButton" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>	