<div class="modal fade" id="monthYearModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Input month and year</h4>
				
			</div>
			<div class="modal-body">
				<p>Manually input month and year. <br/>
                    Example: Year = 2015 / Month = September
                </p>
                
                <div class="row">
                    <div class="col-md-2 col-lg-2">
                        <label> Year </label>
                    </div>
                     <div class="col-md-4 col-lg-4">
                        <input maxlength="4" type="text" class="form-control num_only" id="pickerYear"/>
                    </div>
                     <div class="col-md-2 col-lg-2">
                        <label> Month </label>
                    </div>
                     <div class="col-md-4 col-lg-4">
                        <select id="pickerMonth" class="form-control">
                            <option value="January">January</option>
                            <option value="Feburary">Feburary</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                </div>
			</div>
            <div class="modal-header">
			     <button id="pickerButton" type="button" class="btn btn-default">Finish</button> 
			</div>
		</div>
	</div>
</div>