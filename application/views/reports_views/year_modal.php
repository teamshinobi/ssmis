<div class="modal fade" id="yearModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Input year</h4>
				
			</div>
			<div class="modal-body">
				<p>Manually input year. <br/>
                    Example: Year = 2015 
                </p>
                
                <div class="row">
                    <div class="col-md-2 col-lg-2">
                        <label> Year </label>
                    </div>
                     <div class="col-md-4 col-lg-4">
                        <input maxlength="4" type="text" class="form-control num_only" id="pickerYear2"/>
                    </div>
                </div>
			</div>
            <div class="modal-header">
			     <button id="pickerButton2" type="button" class="btn btn-default">Finish</button> 
			</div>
		</div>
	</div>
</div>