<div class="modal fade" id="caseIdModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Input Case ID</h4>			
			</div>
			<div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-lg-2">
                        <label> Case ID </label>
                    </div>
                     <div class="col-md-4 col-lg-4">
                        <input type="text" class="form-control" id="caseID"/>
                    </div>
                </div>
			</div>
            <div class="modal-header">
			     <button id="enterCaseButton" type="button" class="btn btn-default">OK</button> 
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
			</div>
		</div>
	</div>
</div>