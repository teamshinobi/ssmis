<div class="modal fade" id="beginEndModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Please follow the instructions</h4>			
			</div>
			<div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12" style="margin-bottom: 10px;">
                        <p> Type the whole name for the month: <br> 
                            <strong>Example:</strong> May
                        </p>                    
                    </div> 
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-3 col-lg-3">
                        <label> Begin Month: </label>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <select id="bMonthInput" class="form-control">
                            <option value="January">January</option>
                            <option value="Feburary">Feburary</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-3 col-lg-3">
                        <label> End Month: </label>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <select id="eMonthInput" class="form-control">
                            <option value="January">January</option>
                            <option value="Feburary">Feburary</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-3 col-lg-3">
                        <label> Year: </label>
                    </div>
                     <div class="col-md-6 col-lg-6">
                        <input maxlength="4" type="text" class="form-control num_only" id="cYearInput"/>
                    </div>
                </div>
			</div>
            <div class="modal-header">
			     <button id="enterBeginEndMonth" type="button" class="btn btn-default">OK</button> 
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
			</div>
		</div>
	</div>
</div>