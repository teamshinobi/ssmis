<div class="modal fade" id="monthlyAccomplishmentModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Monthly Accomplishment Report</h4>	
			</div>
			<div class="modal-body">
				<div class="row" >
                    <div class="col-md-12">
                        <table id="monthAccReport" class="acc-table display" cellspacing="0">
                           <thead>
                                <tr>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">Place of Arrest</th>
                                    <th rowspan="2">Offenses Charged</th>
                                    <th rowspan="2">Operation Conducted</th>
                                    <th colspan="3" id="evidences">Drugs Evidences</th>
                                    <th rowspan="2">Status of Evidences</th>
                                    <th rowspan="2">IS #</th>
                                    <th rowspan="2">CC # JUDGE</th>
                                    <th rowspan="2">Status of Case</th>
                                </tr>
                                <tr>
                                    <th>Shabu</th>
                                    <th>Marijuana</th>
                                    <th>Others</th> 
                                </tr>
                            </thead>
                        </table>
				    </div>	
                </div>  
			</div>
            <div class="modal-header">
			   <button type="button" id="" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>