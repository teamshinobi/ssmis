	<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<div id = "" class = "myModalLabel">
						<h4>Narcotics Information</h4>
						<p id = "narcotics_view"> Narcotics ID</p>
					</div>
				</div>
				
				<div class="modal-body">
					
					<form id="view-narcotics-form" method="POST">

							<div class = "row row-pad">

							
							
							<div class = "col-lg-4">
								<label class = "label-pad">Narcotics Name:</label>
							</div>
							<div class = "col-lg-8">
								<input id="name" name="name" type = "text" class = "form-control" placeholder = "Narcotics Name" required>
							</div>	
						</div>
					
						<div class = "row row-pad">
							<div class = "col-lg-4">
								<label class = "label-pad">Narcotics Description:</label>
							</div>
							<div class = "col-lg-8">
								<textarea id="description" name="description" class="form-control" placeholder = "Narcotics Description" required></textarea>
							</div>
						</div>
								<!-- END of MODAL BODY -->
					</div>
					
					<div class="modal-footer">
						<button type="button" id="closeButton_v" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" id="updateButton" class="btn btn-primary">Save changes</button>
					</div>
					</form>
			</div>
		</div>
	</div>	