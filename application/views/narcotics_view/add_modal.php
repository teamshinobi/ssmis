	<div class="modal fade" id="addModal"  tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<h4><i class="fa fa-lg fa-car"></i> Add Narcotics</h4>
                	<p id="narcotics_increment" class="text-muted narcotics_increment"> Narcotics ID</p>
				</div>
				<div class="modal-body">
					<form id="add-narcotics-form" method="POST">
						
						<div class = "row row-pad">
							<div class = "col-lg-4">
								<label class = "label-pad">Narcotics Name:</label>
							</div>
							<div class = "col-lg-8">
								<input name="name" type = "text" class = "form-control" placeholder = "Narcotics Name" required>
							</div>	
						</div>
						
						<div class = "row row-pad">
							<div class = "col-lg-4">
								<label class = "label-pad">Narcotics Description:</label>
							</div>
							<div class = "col-lg-8">
								<textarea name="description" class="form-control" placeholder = "Narcotics Description" required></textarea>
							</div>
						</div>
					<!-- END of MODAL BODY -->
					</div>
					
					<div class="modal-footer">
						<button type="button" id="closeButton" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
					</form>
			</div>
		</div>
	</div>	