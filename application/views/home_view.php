
	<!-- Page Content -->
	<div id="content">
		<div id="content-header">
			<h1>Welcome to the new SAID-SOTG Management Information System!</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="active">Home</a>
						</li>	
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 id="date" class="heading-inline">Statistics as of 
					</h4>
				</div>	
			</div>
			
			<div class="row">
				<div class="col-md-4 col-sm-6">

					<a href="<?php echo base_url() . 'violators';?>" class="dashboard-stat primary">
						<div class="visual">
							<i class="fa fa-leaf"></i>
						</div> <!-- /.visual -->

						<div class="details">
                            <span class="content">Violators</span>
							<span class="value"><?php echo $violators; ?></span>
						</div> <!-- /.details -->

						<i class="fa fa-play-circle more"></i>

					</a> <!-- /.dashboard-stat -->

				</div> <!-- /.col-md-3 -->

				<div class="col-md-4 col-sm-6">

					<a href="<?php echo base_url() . 'viewcasereports'; ?>" class="dashboard-stat secondary">
						<div class="visual">
							<i class="fa fa-file"></i>
						</div> <!-- /.visual -->

						<div class="details">
							<span class="content">Cases</span>
						  <span class="value"><?php echo $cases; ?></span>
						</div> <!-- /.details -->

						<i class="fa fa-play-circle more"></i>

					</a> <!-- /.dashboard-stat -->

				</div> <!-- /.col-md-3 -->

				<div class="col-md-4 col-sm-6">

					<a href="<?php echo base_url() . 'pnp'; ?>" class="dashboard-stat tertiary">
						<div class="visual">
							<i class="fa fa-clock-o"></i>
						</div> <!-- /.visual -->

						<div class="details">
							<span class="content">Policemen</span>
							<span class="value"><?php echo $pnps; ?></span>
						</div> <!-- /.details -->

						<i class="fa fa-play-circle more"></i>

					</a> <!-- /.dashboard-stat -->

				</div> <!-- /.col-md-3 -->
            
                <div class="row">
                    <div class="col-md-12" >
					   <h4 class="heading-inline" style="margin-left:20px;margin-top:40px;">
                            Line Chart of Cases for the Current Month
					   </h4>
				    </div>	
                </div>
                
              
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                         <div id="chartContainer2" style="width: 100%;height: 250px;">
                            
                        </div>               
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-12" >
					   <h4 class="heading-inline" style="margin-left:20px;margin-top:40px;">
                            Line Chart of Cases for the Current Year
					   </h4>
				    </div>	
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                         <div id="chartContainer" style="width: 100%;height: 250px;">
                            
                        </div>               
                    </div>
                </div>
                
                
                
                 <div class="row">
                    <div class="col-md-12" >
					   <h4 class="heading-inline" style="margin-left:20px;margin-top:40px;">
                            Narcotics Occurances for the Current Month
					   </h4>
				    </div>	
                </div>
                <div class="row">
                    
                    <div class="col-md-12 col-lg-12">
                         <div id="donutChart" style="width: 100%;">
                            
                        </div>               
                    </div>
                    
                </div>
                
                
                <div class="row">
                    <div class="col-md-12">
					   <h4 id="month" style="margin-left:20px;margin-top:40px;" class="heading-inline">
					   </h4>
				    </div>	
                </div>
                <div style="margin-left:15px;margin-right:40px;" class="row" >
                    <div class="col-md-12">
                        <table id="monthAccReport" class="acc-table" cellspacing="0">
                           <thead>
                                <tr>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">Place of Arrest</th>
                                    <th rowspan="2">Offenses Charged</th>
                                    <th rowspan="2">Operation Conducted</th>
                                    <th colspan="3" id="evidences">Drugs Evidences</th>
                                    <th rowspan="2">Status of Evidences</th>
                                    <th rowspan="2">IS #</th>
                                    <th rowspan="2">CC # JUDGE</th>
                                    <th rowspan="2">Status of Case</th>
                                </tr>
                                <tr>
                                    <th>Shabu</th>
                                    <th>Marijuana</th>
                                    <th>Others</th> 
                                </tr>
                            </thead>
                        </table>
				    </div>	
                </div>  
                
                
                <div class="row">
                    <div class="col-md-12">
					   <h4 id="yearly" style="margin-left:20px;marginstop:40px;" class="heading-inline">
                           Yearly Accomplishment Report
					   </h4>
				    </div>	
                </div>
                
                <div style="margin-left:15px;margin-right:40px;" class="row" >
                    <div class="col-md-12">
                        <table id="yearAccReport" class="acc-table display" cellspacing="0">
                           <thead>
                                <tr>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">Place of Arrest</th>
                                    <th rowspan="2">Offenses Charged</th>
                                    <th rowspan="2">Operation Conducted</th>
                                    <th colspan="3" id="evidences">Drugs Evidences</th>
                                    <th rowspan="2">Status of Evidences</th>
                                    <th rowspan="2">IS #</th>
                                    <th rowspan="2">CC # JUDGE</th>
                                    <th rowspan="2">Status of Case</th>
                                </tr>
                                <tr>
                                    <th>Shabu</th>
                                    <th>Marijuana</th>
                                    <th>Others</th> 
                                </tr>
                            </thead>
                        </table>
				    </div>	
                </div> 
                
			</div>
		</div>
	</div>
     <!-- /#page-content-wrapper -->
	   
	   
<!-- End of Wrapper for the sidebar -->
</div>