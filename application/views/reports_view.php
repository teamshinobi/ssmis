	<div id="content">		
		
		<div id="content-header">
			<h1>Reports</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">	
            <div class="row"> 
                <div class="col-md-6 col-lg-6"> 
                    <p> Choose from the selections</p>
                </div>
            </div>
            <!-- START PANEL WITH STATIC CONTROLS -->
            <div class="row"> 
                <div class="col-md-6 col-lg-6">
                     <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Monthly Accomplishment Report</h3>
                            <ul class="panel-controls">
                                <li><a id="pickDateForMonthly" class="panel-remove"><span class="fa fa-calendar"></span></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            Generate monthly accomplishment report based on the given month and year. <br>
                            <p> 
                                <br/>Generate report using: <br/>
                                <span id="month">Month:</span>  <br/>
                                <span id="year"> Year:</span>
                            
                            </p>
                           
                        </div>      
                        <div class="panel-footer">
                            <button type="button" id="generateMonthlyAccButton" class="btn btn-primary pull-right clearfix">Generate</button>
                        </div>
                       
                    </div>
                </div>
                 <div class="col-md-6 col-lg-6">
                     <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Yearly Accomplishment Report</h3>
                            <ul class="panel-controls">
                                <li><a id="pickDateForYearly" class="panel-remove"><span class="fa fa-calendar"></span></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            Generate yearly accomplishment report based on the given year.
                             <p> 
                                <br/>Generate report using: <br/>
                                <span id="year2">Year:  <br/>
                                <br/>
                            
                            </p>
                        </div>      
                        <div class="panel-footer">
                            <button id="generateYearlyAccButton" class="btn btn-primary pull-right">Generate</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row"> 
                <div class="col-md-6 col-lg-6">
                     <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Case Report</h3>
                            <ul class="panel-controls">
                                <li><a id="enterCaseIdButton" href="#" class="panel-remove"><span class="fa fa-calendar"></span></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            Generate a particular case report in printable format based on supplied CASE ID.<br><br>
                            
                            <p id="case_id_p">Case ID: ---</p>
                           
                        </div>      
                        <div class="panel-footer">
                            <button id="generateCaseReport" class="btn btn-primary pull-right clearfix">Generate</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                     <div class="panel panel-colorful">
                        <div class="panel-heading">
                            <h3 class="panel-title">Statistics on Dangerous Drugs</h3>
                            <ul class="panel-controls">
                                <li><a id="beMonthButton" href="#" class="panel-remove"><span class="fa fa-calendar"></span></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            Statistics on dangerous and prohibited drugs.   <br>
                            <p>
                                <strong> Begin Month: </strong><span id="bMonth">---</span> <br> 
                                <strong> End Month: </strong><span id="eMonth">---</span> <br>
                                <strong> Year: </strong><span id="cYear">---</span> <br>
                            </p>
                        </div>      
                        <div class="panel-footer">
                            <button id="generateDrugStatsButton" class="btn btn-primary pull-right clearfix">Generate</button>
                        </div>
                    </div>
                </div>
            </div>
            
             <div class="row"> 
                
            </div>
           
    </div>   
<!-- End of -->
</div>