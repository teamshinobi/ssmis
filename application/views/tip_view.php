	<div id="content">
		<div id="content-header">
			<h1>Tips</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button style="width:100px;" id="addTipButton" class="btn btn-sm btn-default" type="button">
                        <i class="fa fa-plus"></i>
                        &nbsp; New Tip
                    </button>
                    <button style="width:100px;" id="importButton" class="btn btn-sm btn-default" type="button">
                        <i class="fa fa-database"></i>
                        &nbsp; Import
                    </button>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-12 col-md-12">
                <p class="text-muted">
                    Right click for options.
                </p>
                <table id="tips_tbl" class="table table-striped table-bordered table-hover table-highlight table-checkable no-footer">
                    <thead>
                        <tr>
                            <th class="">ID</th>
                            <th class="">Date</th>
                            <th class="">Story</th>
                            <th class="">Targets</th>
                            <th class="">Shown</th>
                        </tr>	
                    </thead>				
                </table>
                    
                </div>
               
            </div>
        </div>	
	</div>	
	<!-- End of -->
</div>