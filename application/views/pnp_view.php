	<div id="content">		
		
		<div id="content-header">
			<h1>Maintenance</h1>
		</div> <!-- #content-header -->	
		<div id="content-container">	
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() . "index.php/pnp"?>">PNP</a>
						</li>
						<li><a href="<?php echo base_url() . "index.php/pnp"?>" class="active">Maintenance</a>
						</li>							
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="portlet">
						<div class="portlet-header">
							<h2><i class="fa fa-group"></i><span class="break"></span>Our Policemen</h2>
                            <ul class="portlet-tools pull-right">
                                <li>
                                    <button title="Add POLICE"  id="addButton" type="button" class="btn btn-md btn-default">
                                        <span><i class="fa fa-plus"></i></span>
                                    </button>
                                </li>
                            </ul>
						</div>
						<div class="portlet-content">
							<div class="table-responsive">
								<table id="police_table" class="table table-striped table-bordered table-hover table-highlight table-checkable">
									<thead>
										<tr>
											<th class="">Batch ID</th>
											<th class="">Rank</th>
											<th class="">Full Name</th>
                                            <th class="">Status</th>
											<th class="">Picture</th>
										</tr>	
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	   
<!-- End of -->
</div>