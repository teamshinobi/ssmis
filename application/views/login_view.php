<img class="bg-image"
	<?php
		echo ' src=' . base_url() . 'public/resources/design_photos/policemen_salute.jpg';
	?>
>
<div class="container main-container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Sign-in to continue to SSMIS</h1>
            <div class="account-wall">
                <img class="profile-img" src="<?php echo base_url() . 'public/resources/design_photos/login-icon.png'?>"
                    alt="">
                <form class="form_signin" id="form_signin" method="POST">
				
					<div class="form-group">
						<input autocomplete="off" type="text" name="batch_id" class="form-control" placeholder="Batch ID" required autofocus>
						<span class="help-block" id="msg_batch">Batch ID is empty</span>
					</div>
					
					<div class="form-group">
						<input autocomplete="off" type="password" name="password" class="form-control" placeholder="Password" required>
						<span class="help-block" "msg_pass">Password is empty</span>
					</div>
					
					<button id="submitButton" class="btn btn-lg btn-primary btn-block" type="submit" >
                        Sign in</button>			
                </form>
            </div>  
        </div>
    </div>
</div>

<section class="links text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <ul>
                    <li> <a> About </a> </li>   
                    <li> <a> Mission </a> </li>
                    <li> <a> Vision </a> </li>
                </ul>
            </div>   
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h5>Copyright PNP 2015</h5>
            </div>   
        </div>
    </div>
</section>


