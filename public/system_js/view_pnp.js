$(function() {

	//initialize datepickers
	var dps = ['#member_since'];
	Main.Init.InitDatePicker(dps);
	
	var idsMap = {
		pnp_image_file : { preview : '#preview' },
		
	}
	var selectors = '#pnp_image_file';
	
	Main.Mod.handle_image_change(selectors, idsMap);
	//disable browser caching
	$.ajaxSetup({cache: false});
	
    PNP.ViewPnpModule.onLoad();
	//handle add button click
	$('#affiliationsButton').click(function(){
		PNP.ViewPnpModule.show_modal('a');
	});
	
	//handle closing of affiliation modal
	$('#closeAffiliationModal').click(function(){
		PNP.ViewPnpModule.close_modal('a');
	});
	
    //handle upload button click
	$('#uploadButton').click(function(){
		PNP.ViewPnpModule.show_modal('b');
	});
    $('#showUpdateFormButton').click(function(){
		PNP.ViewPnpModule.show_modal('c');
	});
    showUpdateFormButton
    
	//handle form submission
	$("#update_pnp_form").on('submit',(function(e){
		e.preventDefault();
		
		var formdata = new FormData(this);
        formdata.append('prev_id' , $('#pnp_id').text().trim());

		PNP.ViewPnpModule.update(formdata);
	}));    
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
});