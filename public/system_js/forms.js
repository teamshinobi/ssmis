$(function(){
    try{
        PM.Main.onLoad();

        $('#addTargetButton').on('click',function(){
            PM.Main.on_modal_showing(PM.Main.modal_const.ADD_TARGET);
        });
    }catch(e){
        console.log('NOT USING PM MODULE.');
    }   
    
    try{
        VPM.Main.onLoad();
        $('#addTargetButton').on('click',function(){
            VPM.Main.on_modal_showing(VPM.Main.modal_const.ADD_TARGET);
        });

    }catch(e){
        console.log(e);
    }   
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");

});