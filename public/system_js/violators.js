$(document).ready(function(){
	/*====================================================================
	*	Main Module. If violators_module.js is called the code below will be 
	*	executed.
	*===================================================================*/
	try{
		VM.Main.onLoad();
		$('#showAdvanceSearch').on('click',function(){
			VM.Main.on_modal_showing(VM.Main.modal_const.SEARCH,1200,-300);
		})
		$('#showPrintForm').on('click',function(){
			VM.Main.on_modal_showing(VM.Main.modal_const.PRINT,500,100);
		});
	}catch(e){
		console.log('VM is not defined. The program will continue by using VVM Module');
	}
		
	
	
	/*====================================================================
	*	View Module. If view_violators_module.js is called the code 
	*	below will be executed.
	*===================================================================*/
	try{
		VVM.Main.onLoad();
		$('#updateViolatorButton').on('click',function(){
			VVM.Main.on_modal_showing(VVM.Main.modal_const.UPDATE_VIOLATOR,1200, -300);
		});
		$('#showUploadForm').on('click',function(){
			VVM.Main.on_modal_showing(VVM.Main.modal_const.UPLOAD,500,100);
		});
		$('#showPrintForm').on('click',function(){
			VVM.Main.on_modal_showing(VVM.Main.modal_const.PRINT,500,100);
		});	
		
	}catch(e){
		console.log('VVM is not defined. Th program will continue by using VM Module');
	}
        $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
});