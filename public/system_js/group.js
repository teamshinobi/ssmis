$(function() {
	
	var group_table = $('#groups_table');
	var members_table = $('#members_table');
	//Initialize DataTable for group
	PNP.PnpGroupModule.retrieve(group_table,'g');
	//Initialize DataTable for group
	PNP.PnpGroupModule.retrieve(members_table,'m');	
	
	$('#addButton').click(function(){
        
		PNP.PnpGroupModule.add();
	});
	
	//handle form submission for new group
	$("#new-group-form").on('submit',(function(e){
		e.preventDefault();
		
		var formdata = new FormData(this);	
		
		PNP.PnpGroupModule.save(formdata);
		
	}));
	
	
	/*===============================================================
	*
	*					VIEWING OF GROUP SCRIPTS
	*
	*=================================================================*/
	var police_table = $('#police_table');
	//Initialize DataTable for available police
	PNP.PnpGroupModule.retrieve(police_table,'p');	

	
	//handle edit group button click
	$('#editGroupButton').click(function(){
		PNP.PnpGroupModule.show_modal('edit');
	});
	
	//handle form submission for edit group
	$("#edit-group-form").on('submit',(function(e){
		e.preventDefault();
		
		var formdata = new FormData(this);	
			
		PNP.PnpGroupModule.update(formdata);
		
	}));
	
	//handle edit group button click
	$('#newMemberButton').click(function(){
		PNP.PnpGroupModule.show_modal('add');
	});
	
	    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
	
});