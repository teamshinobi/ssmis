/*======================================================
*
*   Reusable javascript codes for the system
*
*====================================================*/
var Services = Services || {};

//MODAL SERVICE
Services.ModalService  = (function(){
    
    var showModal = function(id,width,margin){ 
        
        var modal_options = {
            "backdrop" : "static",
            "keyboard" : true,
        }

        var selector = '#' + id;
        
		$(selector).modal(modal_options);
		$(selector + ' .modal-content').css('width', width+'px');
		$(selector + ' .modal').css('position' , 'absolute');
		$(selector + ' .modal-content').css('margin-left', margin+'px');	
    }
    
    return {
        showModal : showModal,
    }

})();

Services.Ajax =  (function(){
    
    
})(); 