$(function(){



	
	NM.NarcoticsModule.retrieve($('#narcotics_table'));
	
	$('#addButton').click(function(){
		NM.NarcoticsModule.add();
	});

	$('#deletebutton').click(function(){
		NM.NarcoticsModule.delete_narcotics();
	});
	
	$('#reportButton').click(function(){
		NM.NarcoticsModule.report();
	});


	
	$('#editButton').click(function(){
		NM.NarcoticsModule.edit();
	});
	$('#closeButton').click(function(){
		NM.NarcoticsModule.close();
	});

	

	$("#view-narcotics-form").on('submit',(function(e){
		e.preventDefault();
		var formdata = new FormData(this);
		formdata.append('narcotics_id', $('#narcotics_view').text().trim());
		NM.NarcoticsModule.update(formdata);


	}));
	
	$("#add-narcotics-form").on('submit',(function(e){
		
		e.preventDefault();
	
		
		var formdata = new FormData(this);	
		
		NM.NarcoticsModule.save(formdata);


	}));
      $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
	
});