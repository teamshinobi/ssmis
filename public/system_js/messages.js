$(function(){
	PM.ProfileModule.retrieve('inbox');

	$('#composeButton').click(function(){
		$('#composeModal').modal('toggle');
	});
	
	$("#message-form").on('submit',(function(e){
		
		e.preventDefault();
		
		var formdata = new FormData(this);	
		
		PM.ProfileModule.save(formdata);
	}));
	
	$('#inbox').click(function(){
		$('#viewContent').hide();
		$('#continueMsg').css('display', 'none');
		$('#deleteMsg').css('display', 'inline');
		PM.ProfileModule.retrieve('inbox');
	});
	
	$('#sentItems').click(function(){
		$('#viewContent').hide();
		$('#continueMsg').css('display', 'none');
		$('#deleteMsg').css('display', 'inline');
		PM.ProfileModule.retrieve('sentItems');
	});
	
	$('#drafts').click(function(){
		$('#viewContent').hide();
		$('#deleteMsg').css('display', 'inline');
		$('#continueMsg').css('display', 'inline');
		PM.ProfileModule.retrieve('drafts');
	});
	
	$('#deleteMsg').click(function(){
		PM.ProfileModule.remove();
	});
	
	$('#closeButton').click(function(){
		PM.ProfileModule.close();
	});

	
	$("#continue-message-form").on('submit',(function(e){
		
		e.preventDefault();
		
		var formdata = new FormData(this);	
		
		PM.ProfileModule.updateDraft(formdata);
	}));
        $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");

});