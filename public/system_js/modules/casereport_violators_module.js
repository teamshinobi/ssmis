//Itong module na ito ang pangsapal sa panget na code sa adding casereport_module
var CVM = CVM || {};

CVM.Vars = (function(){ 
    //violators table
    var violators_table = undefined;
    //violators array.
    var violators = new Array();
    //current selected violator
    var selected_violator = {};
    //array of currently selected charges. Ilalagay tong array na to kada violator objects.
    var charges = new Array();
    //array of charges objects. Mostly gagamitin lang natin to sa front_end pero ilalagay din to sa violator objects.
    var chargesObj = new Array();
    //current selected existing violators. Ginagamit ko lang sa pag-aadd ng existing violators.
    var selected_ex_violator = {};
    
    return {
        violators_table : violators_table,
        violators : violators,
        selected_violator : selected_violator,
        charges : charges,
        chargesObj : chargesObj,
        selected_ex_violator : selected_ex_violator,
    }
})();



CVM.Main = (function(){ 
    
    var onLoad = function onLoad(){
        initViolatorsTable();
        initViolatorFirstId();
        
        //attach events
        CVM.Events.attach(CVM.Events.events_const.ON_SHOW_NEW_VIOLATOR);
        CVM.Events.attach(CVM.Events.events_const.ON_SHOW_EXISTING_VIOLATORS);
        CVM.Events.attach(CVM.Events.events_const.ON_VIOLATOR_UPDATE);
        
        //context menu
        $.contextMenu({
            selector: '.violators-context', 
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                CVM.Events.attach(CVM.Events.events_const.CONTEXT_ACTION,key,options);
            },
            items: {
                "view": {name: "View", icon: 'edit'},
                "remove": {name : "Remove", icon: 'delete'},
            }
        });
    }
    
    
    /*================================================================
    *                       INIT FUNCTIONS
    *===============================================================*/
    //load first violator id in add modal
    function initViolatorFirstId(){
        $.ajax({
            type: 'GET',
            url: Main.Vars.host + 'casereport/firstviolatorid',
            contentType: false,
            cache: false,   
            processData:false, 
            dataType: 'json',
			success: function(data){
				$('#violator-id').text(data.id);
			},
		});
    }
    function initViolatorsTable(){
        CVM.Vars.violators_table = $('#violators_tbl');
        CVM.Vars.violators_table.DataTable({
			"autoWidth": false,
			"aoColumnDefs": [
				{ "bSortable": false, "aTargets": [ 2,3 ] },
				{ "sWidth": "20%", "aTargets": [ 0 ] },
				{ "sWidth": "35%", "aTargets": [ 1 ] },
				{ "sWidth": "30%", "aTargets": [ 2 ] },
				{ "sWidth": "15%", "aTargets": [ 3 ] },
			],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                $(nRow).addClass('violators-context');	
            }
		});  
      
    }
    
    
    return {
        onLoad : onLoad,
    }


})();

            


CVM.Events = (function(){ 
    //prototypes
    var violator_prototype = {
        get_full_name: function(){
			return this.last_name + ', ' + this.first_name;
		} 
    }
    var charge_prototype = {
		to_string : function(){
			return this.article  + " "  + this.section;
		}	
	}
    
    var events_const = {
        'ON_SHOW_NEW_VIOLATOR' : 1,
        'ON_SHOW_EXISTING_VIOLATORS' : 2,
        'ON_VIOLATOR_UPDATE' : 3,
    
    }
    function on_show_new_violator(){
        $('#showViolatorFormButton').off('click').on('click',function(){
            Main.Mod.show_modal('addViolatorModal',1200,-300);
            initChargesTable('ra_table');
            onAddViolator();
        });
        
    }
    
    function on_show_existing_violators(){
        $('#showExistingViolatorsButton').off('click').on('click',function(){
            Main.Mod.show_modal('existingViolatorModal',1200,-300);
            initChargesTable('ex_ra_table');
            initExViolatorsTable();
            onAddExistingViolator();
        });
    }
    
    /*================================================================
    *                     UPDATING FUNCTIONS
    *===============================================================*/
    
    function on_violator_update(){
        $('#updateViolatorButton').click(function(){	
           update_violator();
	   });
    }
    
    function update_violator(){
        var update_message = "";
		
		var callback_func = function(){
            //selected violator is assigned by referrence. Any changes made to this object automatically reflected in violators array.
            CVM.Vars.selected_violator.alias = $('#alias_v').val();
            CVM.Vars.selected_violator.first_name = $('#first_name_v').val();
            CVM.Vars.selected_violator.last_name = $('#last_name_v').val();
            CVM.Vars.selected_violator.middle_name = $('#middle_name_v').val();
            CVM.Vars.selected_violator.birth_date = $("input[name='birthdate_v']").val();
            CVM.Vars.selected_violator.gender = $("input[name='gender_v']:checked").val();
            CVM.Vars.selected_violator.educational_attainment = $('#educational_attainment_v').val();
            CVM.Vars.selected_violator.weight = $('#weight_v').val();
            CVM.Vars.selected_violator.height = $('#height_v').val();
            CVM.Vars.selected_violator.eyes_color = $('#eyes_color_v').val();
            CVM.Vars.selected_violator.hair_color = $('#hair_color_v').val();
            CVM.Vars.selected_violator.identifying_marks = $('#ident_marks_v').val();
            CVM.Vars.selected_violator.complexion = $('#complexion_v').val();
            CVM.Vars.selected_violator.nationality = $('#nationality_v').val();
            CVM.Vars.selected_violator.mother_name = $('#mother_name_v').val();
            CVM.Vars.selected_violator.father_name = $('#father_name_v').val();
            CVM.Vars.selected_violator.mother_address = $('#mother_address_v').val();
            CVM.Vars.selected_violator.mother_age = $('#mother_age_v').val();
            CVM.Vars.selected_violator.father_address = $('#father_address_v').val();
            CVM.Vars.selected_violator.father_age = $('#father_age_v').val();
            CVM.Vars.selected_violator.contact_person = $('#contact_person_v').val();
            CVM.Vars.selected_violator.contact_info = $('#contact_info_v').val();
            CVM.Vars.selected_violator.relationship = $('#relationship_v').val();
            CVM.Vars.selected_violator.street_name = $('#street_name_v').val();
            CVM.Vars.selected_violator.barangay_name = $('#barangay_name_v').val();
            CVM.Vars.selected_violator.city_name = $('#city_name_v').val();
            CVM.Vars.selected_violator.occupation = $('#occupation_v').val();
            CVM.Vars.selected_violator.tel_no = $('#tel_no_v').val();
            CVM.Vars.selected_violator.place_of_birth = $('#place_of_birth_v').val();
            CVM.Vars.selected_violator.nationality = $('#nationality_v').val();
            CVM.Vars.selected_violator.marital_status = $('#marital_list_v option:selected').val();
            CVM.Vars.selected_violator.name_of_school = $('#name_of_school_v').val();
            CVM.Vars.selected_violator.location_of_school = $('#location_of_school_v').val();
            CVM.Vars.selected_violator.dr_license = $('#dr_license_v').val();
            CVM.Vars.selected_violator.dr_issued_at = $('#dr_issued_at_v').val();
            CVM.Vars.selected_violator.dr_issued_on = $('#dr_issued_on_v').val();
            CVM.Vars.selected_violator.res_cert = $('#res_cert_v').val();
            CVM.Vars.selected_violator.res_cert_issued_at = $('#res_cert_issued_at_v').val();
            CVM.Vars.selected_violator.res_cert_issued_on = $('#res_cert_issued_on_v').val();
            CVM.Vars.selected_violator.other_ids = $('#other_ids_v').val();
            CVM.Vars.selected_violator.id_numbers = $('#id_numbers_v').val();
            
            
            if(CVM.Vars.charges.length > 0 && CVM.Vars.chargesObj.length > 0){
                CVM.Vars.selected_violator.charges =  CVM.Vars.charges.slice();
                CVM.Vars.selected_violator.charges_obj =  CVM.Vars.chargesObj.slice();
            }
            //remove items in charges(reset)
            CVM.Vars.charges.length = 0;
            CVM.Vars.chargesObj.length = 0;

            if(CVM.Vars.selected_violator.is_existing == true){
                update_message="Violator successfully updated!\n This violator has a previous record in the database, in order to reflect the changes\n" +
                " you've made you need to save the case report accordingly."
            }else{
                update_message = "Violator successfully updated!"   
            }

            bootbox.alert({ 
                message: update_message,
                title: "Message",
                callback: function(){
                    $('#viewViolatorModal').modal('hide');
                }			
            });	

            update_violator_photos();				
		}
		Main.Mod.show_bootbox_confirm('Are you sure you want to update this violator?', 'Message', callback_func);	
    }
    
    function update_violator_photos(){
		var formdata = new FormData();

		formdata.append('violator_id', CVM.Vars.selected_violator.violator_id);
		formdata.append('front', $('#frontfile_v')[0].files[0]);
		formdata.append('rear', $('#rearfile_v')[0].files[0]);
		formdata.append('right', $('#rightfile_v')[0].files[0]);
		formdata.append('left', $('#leftfile_v')[0].files[0]);
		
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'casereport/update_existing_violator_image',
			data: formdata,
            contentType: false,
            cache: false,   
            processData:false, 
			success: function(){
				update_table_after_updating(CVM.Vars.selected_violator);	
			},
		});
	}
    
    function update_table_after_updating(selected_violator){
        CVM.Vars.violators_table.find('tr').each(function (i, el) {
			var tr = ($(this));
			var td_text = $(this).find('td:first').text();		
			if(td_text == CVM.Vars.selected_violator.violator_id){
				tr.empty();
				return false;
			}
		});
		update_table(selected_violator);
    }
    
    /*================================================================
    *                     EXISTING VIOLATOR FUNCTIONS
    *===============================================================*/
    function onAddExistingViolator(){  
        $('#getViolatorsInfoButton').off('click').on('click',function(){
            get_existing_violator();
        });
    }
    
    //gets info of a particular in the database
    function get_existing_violator(){
        if(CVM.Vars.selected_ex_violator == undefined){
			Main.Mod.show_bootbox_alert('Select a violator before adding.', 'Message', null);		
		}else{
            //if violators is not yet on the list
            if(Main.Objects.index_of(CVM.Vars.violators,CVM.Vars.selected_ex_violator,'violator_id') == -1){
                $.ajax({
                    type: 'GET',
                    url: Main.Vars.host + 'casereport/existingviolator/' + CVM.Vars.selected_ex_violator,
                    contentType: false,
                    cache: false,   
                    processData:false, 
                    dataType: 'json',
                    success: function(data){
                        create_existing_violator(data);
                    },
			     });		
            } else {
                Main.Mod.show_bootbox_alert('The violator is already on the list','Message', null);
            }
        }
    }
    
    //create existing violator object
    function create_existing_violator(data){
        //reset
		selected_ex_violator = undefined;  
		$('#selection-details').text('There is no violator currently selected.');
		
		var violator = Object.create(violator_prototype);
		//since we return only one object in the array.
		var json_violator = data.data[0];
		violator.violator_id = json_violator.violator_id;
		violator.alias = json_violator.alias;
		violator.first_name = json_violator.first_name;
		violator.last_name = json_violator.last_name;
		violator.middle_name = json_violator.middle_name;
		violator.birth_date = json_violator.birth_date;
        violator.place_of_birth = json_violator.place_of_birth;
        violator.occupation = json_violator.occupation;
        violator.marital_status = json_violator.marital_status;
		violator.gender = json_violator.gender;
		violator.height = json_violator.height;
		violator.weight = json_violator.weight;
		violator.eyes_color = json_violator.eyes_color;
		violator.hair_color = json_violator.hair_color;
		violator.identifying_marks = json_violator.identifying_marks;
        violator.complexion = json_violator.complexion;
		violator.nationality = json_violator.nationality;
		violator.mother_name = json_violator.mother_name;
        violator.mother_address = json_violator.mother_address;
        violator.mother_age = json_violator.mother_age;
		violator.father_name = json_violator.father_name;	
        violator.father_address = json_violator.father_address;
        violator.father_age = json_violator.father_age;
		violator.contact_person = json_violator.contact_person;
		violator.relationship = json_violator.relationship;
		violator.contact_info = json_violator.contact_info;
		violator.remarks = 'On-Jail';
		violator.educational_attainment  = json_violator.educational_attainment;
        violator.name_of_school = json_violator.name_of_school;
        violator.location_of_school = json_violator.location_of_school;
		violator.street_name  = json_violator.street_name;
		violator.barangay_name  = json_violator.barangay_name;
		violator.city_name  = json_violator.city_name;
		violator.is_existing = true;
        
        violator.dr_license = json_violator.dr_license;
        violator.dr_issued_at = json_violator.dr_issued_at;
        violator.dr_issued_on = json_violator.dr_issued_on;
        violator.res_cert = json_violator.res_cert
        violator.res_cert_issued_at = json_violator.res_cert_issued_at;
        violator.res_cert_issued_on = json_violator.res_cert_issued_on;
		violator.other_ids = json_violator.other_ids;
        violator.id_numbers = json_violator.id_numbers;
		violator.front_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_front' + '.jpg';
		violator.rear_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_rear' + '.jpg';
		violator.right_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_right' + '.jpg';
		violator.left_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_left' + '.jpg';
		violator.file_path = "public/resources/photos/violator/" + violator.violator_id;
		
		violator.charges = CVM.Vars.charges.slice();
		violator.charges_obj = CVM.Vars.chargesObj.slice();
		
		update_table(violator);
		
		CVM.Vars.violators.push(violator);
		
		//remove items in charges
		CVM.Vars.charges.length = 0;
		CVM.Vars.chargesObj.length = 0;
		
		$('#existingViolatorModal').modal('hide');
	}
    /*===================END EXISTING FUNCTIONS======================*/
    
    /*================================================================
    *                     ADDING VIOLATOR FUNCTIONS
    *===============================================================*/
    //adds violator to table after uploading temporary photos in the server
    function onAddViolator(){
        $('#addViolatorButton').off('click').on('click',function(){
            //Dito ka magvalidate JC
            if(has_complete_basic()){
                upload_temp();
            } else {
                Main.Mod.show_bootbox_alert("Please complete basic info and location. And make sure to add charges.",
                                           "Message", null);
            }
        });
    }
    //validates if the user completed the basic info of the violator.
    function has_complete_basic(){
        var isComplete = true;
        
        $('#basicInfoDiv input').each(function () {
            var id = $(this).attr('id');
            if(id != 'alias' && id != 'tel_no'){
                 if($(this).val() == ''){
                    isComplete = false;
                    return false;
                 }
            }
           
        });
         
        $('#locationDiv input').each(function () {
            if($(this).val() == ''){
                isComplete = false;
                return false;
            }
        });
        
        if(CVM.Vars.charges.length < 1){
            return false;
        }
        
        return isComplete;
    }
    //uploads temporary photos
    function upload_temp(){
        var formdata = new FormData();

		formdata.append('violator_id', $('#violator-id').text());
		formdata.append('front', $('#frontfile')[0].files[0]);
		formdata.append('rear', $('#rearfile')[0].files[0]);
		formdata.append('right', $('#rightfile')[0].files[0]);
		formdata.append('left', $('#leftfile')[0].files[0]);
		
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'casereport/uploadtemp',
			data: formdata,
            contentType: false,
            cache: false,   
            processData:false, 
			success: function(){
				create_violator();
				//gets previous id before clearing. Because we have to use the value of violator_id field
				var prev_id = $('#violator-id').text();
				//clears only text inputs and files
				Main.Mod.clear_form_inputs('add_violator_form_div', new Array('text','file'));
                //gets next id
				get_next_id(prev_id);
			},
		});
    }
    
   
    
    //creates violator objects
    function create_violator(){
        var violator = Object.create(violator_prototype);
	    //Siguro pwede ako magloop sa loob ng div para makuha lahat to eh, para di mano-mano potang ina!
		//set properties for violator here.
		violator.violator_id = $('#violator-id').text();
		violator.alias = $('#alias').val();
		violator.first_name = $('#first_name').val();
		violator.middle_name = $('#middle_name').val();
		violator.last_name = $('#last_name').val();
		violator.birth_date = $("input[name='birthdate']").val();
        violator.place_of_birth = $('#place_of_birth').val();
        violator.marital_status = $('#marital_list option:selected').val();
        violator.occupation = $('#occupation').val();
        violator.tel_no = $('#tel_no').val();
		violator.gender = $("input[name='gender']:checked").val();
		violator.height = $('#height').val();
		violator.weight = $('#weight').val();
		violator.eyes_color = $('#eyes_color').val();
		violator.hair_color = $('#hair_color').val();
		violator.identifying_marks = $('#ident_marks').val();
        violator.complexion =  $('#complexion').val();
		violator.nationality = $('#nationality').val();
		violator.mother_name = $('#mother_name').val();
        violator.mother_address = $('#mother_address').val();
        violator.mother_age = $('#mother_age').val();
        violator.father_name = $('#father_name').val();	
        violator.father_address = $('#father_address').val();
        violator.father_age = $('#father_age').val();
		violator.contact_person = $('#contact_person').val();	
		violator.relationship = $('#relationship').val();
		violator.contact_info = $('#contact_info').val();	
		violator.remarks = 'On-Jail';
		violator.educational_attainment  = $('#educational_attainment').val();
        violator.name_of_school = $('#name_of_schoold').val();
        violator.location_of_school = $('#name_of_school').val();
		violator.street_name  = $('#street_name').val();
		violator.barangay_name  = $('#barangay_name').val();
		violator.city_name  = $('#city_name').val();
        violator.dr_license = $('#dr_license').val();
        violator.dr_issued_at = $('#dr_issued_at').val();
        violator.dr_issued_on = $('#dr_issued_on').val();
        violator.res_cert = $('#res_cert').val();
        violator.res_cert_issued_at = $('#res_cert_issued_at').val();
        violator.res_cert_issued_on = $('#res_cert_issued_on').val();
		violator.other_ids = $('#other_ids').val();
        violator.id_numbers = $('#id_numbers').val();
        
        violator.is_existing = false;
	
		violator.front_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_front' + '.jpg';
		violator.rear_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_rear' + '.jpg';
		violator.right_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_right' + '.jpg';
		violator.left_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_left' + '.jpg';
		violator.file_path = "public/resources/photos/violator/" + $('#violator-id').text();
        
       
        violator.charges = CVM.Vars.charges.slice();
        violator.charges_obj = CVM.Vars.chargesObj.slice();
        
        //add the newly created object in the array.
        CVM.Vars.violators.push(violator);
        
        CVM.Vars.charges.length = 0;
		CVM.Vars.chargesObj.length = 0;
		
        update_table(violator);       
        
        console.log(violator);
        
        //close add violator modal
        $('#addViolatorModal').modal('hide');
    }
    //gets next id after uploading temporary photos
    function get_next_id(prev_id){
        $.ajax({
            type: 'GET',
            url: Main.Vars.host + 'casereport/nextviolatorid/' + prev_id,
            contentType: false,
            cache: false,   
            processData:false, 
            dataType: 'json',
			success: function(data){
				$('#violator-id').text(data.id);
			},
		}); 
    }

    /*===================END VIOLATOR FUNCTIONS======================*/
    
    //updates table
    var update_table = function update_table(violator){
        
        var img_str1 = '<img class=\"obj-pic\" src=\"' + Main.Vars.base_path + violator.front_temp_file_path + '\">';
		var img_str2 = '<img style=\"margin-left: 10px;\" class=\"obj-pic\" src=\"' + Main.Vars.base_path + violator.rear_temp_file_path + '\">';
		var img_str3 = '<img style=\"margin-left: 10px;\" class=\"obj-pic\" src=\"' + Main.Vars.base_path + violator.right_temp_file_path + '\">';
		var img_str4 = '<img style=\"margin-left: 10px;\" class=\"obj-pic\" src=\"' + Main.Vars.base_path + violator.left_temp_file_path + '\">';
		
		//consolidate charges
        var str_charges = 'R.A 9165 ';
        $.each(violator.charges_obj, function(idx,obj){
            str_charges += (obj.article + ' ' +obj.section + ', ');
        });
        
        CVM.Vars.violators_table.dataTable().fnAddData([
            violator.violator_id,
			violator.get_full_name(),
            str_charges,
			img_str1 + img_str2 + img_str3 + img_str4,
        ]);
        
	   on_violator_selected();
    }
    
    //handle table row click on table
    function on_violator_selected(){
    	$('#violators_tbl tbody tr').off('click').on('click', function(){
			var td = $(this).find('td:first').text();
			
			CVM.Vars.selected_violator = Main.Objects.get_object(CVM.Vars.violators,td,'violator_id')[0];	
            
            view_violator();	
		});
    }
    
    //view violator
    function view_violator(){
		Main.Mod.show_modal('viewViolatorModal',1200,-300);
        
        var selected_violator = CVM.Vars.selected_violator;
        
        console.log(selected_violator);
        
		//set violators info here
		$('#frontimagepreview').attr('src',Main.Vars.base_path + selected_violator.front_temp_file_path);
		$('#rearimagepreview').attr('src',Main.Vars.base_path + selected_violator.rear_temp_file_path);
		$('#rightimagepreview').attr('src',Main.Vars.base_path + selected_violator.right_temp_file_path);
		$('#leftimagepreview').attr('src',Main.Vars.base_path + selected_violator.left_temp_file_path);
		
		$('#violator_id_v').text(selected_violator.violator_id);
		$('#alias_v').val(selected_violator.alias);
		$('#first_name_v').val(selected_violator.first_name);
		$('#last_name_v').val(selected_violator.last_name);
		$('#middle_name_v').val(selected_violator.middle_name);
		$("input[name='birthdate_v']").val(selected_violator.birth_date);
        $('#place_of_birth_v').val(selected_violator.place_of_birth);
        $("#occupation_v").val(selected_violator.occupation);
        $("#tel_no_v").val(selected_violator.tel_no);
        //set fucking marital status
        $("#marital_status_v option[value=" + selected_violator.marital_status +"]").attr("selected", "selected");
		$("#educational_attainment_v").val(selected_violator.educational_attainment);
        $("#name_of_school_v").val(selected_violator.name_of_school);
        $("#location_of_school_v").val(selected_violator.location_of_school);
       
        
		//setting fucking gender
		var $radios = $('input:radio[name=gender_v]');
		if(selected_violator.gender == "1"){
			$radios.filter('[value=1]').prop('checked', true);
		}else{
			$radios.filter('[value=0]').prop('checked', true);
		}
			
		$('#weight_v').val(selected_violator.weight);
		$('#height_v').val(selected_violator.height);
		$('#eyes_color_v').val(selected_violator.eyes_color);
		$('#hair_color_v').val(selected_violator.hair_color);
		$('#ident_marks_v').val(selected_violator.identifying_marks);
        $('#complexion_v').val(selected_violator.complexion);
		$('#nationality_v').val(selected_violator.nationality);	
		
		$('#mother_name_v').val(selected_violator.mother_name);	
        $('#mother_address_v').val(selected_violator.mother_address);
        $('#mother_age_v').val(selected_violator.mother_age);	
		$('#father_name_v').val(selected_violator.father_name);
        $('#father_address_v').val(selected_violator.father_address);
        $('#father_age_v').val(selected_violator.father_age);
        
		$('#contact_person_v').val(selected_violator.contact_person);
		$('#relationship_v').val(selected_violator.relationship);
		$('#contact_info_v').val(selected_violator.contact_info);
		
		$('#street_name_v').val(selected_violator.street_name);
		$('#barangay_name_v').val(selected_violator.barangay_name);
		$('#city_name_v').val(selected_violator.city_name);
        $('#dr_license_v').val(selected_violator.dr_license);
        $('#dr_issued_at_v').val(selected_violator.dr_issued_at);
        $('#dr_issued_on_v').val(selected_violator.dr_issued_on);
        $('#res_cert_v').val(selected_violator.res_cert);
        $('#res_cert_issued_at_v').val(selected_violator.res_cert_issued_at);
        $('#res_cert_issued_on_v').val(selected_violator.res_cert_issued_on);
        $('#other_ids_v').val(selected_violator.other_ids);
        $('#id_numbers_v').val(selected_violator.id_numbers);
        
        //parse charges	
		var com_charges = $('#charges-details');
        com_charges.empty();
		$(com_charges).append('Already committed the following: ');
       
		$.each(CVM.Vars.selected_violator.charges_obj, function( index, value ){
			var tag = '<a data-toggle="tooltip" title="' + value.description + '" target="_blank" href="' + Main.Vars.host + 'ra/get/' + value.charge_id +'">' + value.to_string() + "</a>";
			$(com_charges).append(tag);
			if(index != (selected_violator.charges_obj.length - 1)){
				$(com_charges).append(' | ');
			}
				
		});
        initChargesTable('view_charges_tbl');
    }

    /*================================================================
    *                      INIT FUNCTIONS
    *===============================================================*/
    //initializes charges tables. Dynamic na ito supply lang ang id ng table.
    function initChargesTable(table_id){
        $('#' + table_id).DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			//for reinitialisation purpose
			destroy: true,
			"ajax" : {
				url :  Main.Vars.host + "ra/retrieve",
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 0 ] },
					{ "sWidth": "10%", "aTargets": [ 0 ] },
					{ "sWidth": "15%", "aTargets": [ 1 ] },
					{ "sWidth": "15%", "aTargets": [ 2 ] },
					{ "sWidth": "15%", "aTargets": [ 3 ] },	
					{ "sWidth": "35%", "aTargets": [ 4 ] },								
			],
			
			"columns": [
				{	
					"render": function(data,type,row,meta) {
						return '<input style="margin-left:40px;" data-description="' + row.description+ '" data-article=\"' + row.article + '\" data-section=\"' + row.section + '\" data-value=\"' + row.charge_id +'\" style=\"padding-left: 0px; margin-left: 10px\" type=\"checkbox\"/>';
					}
				},
				{ 
					"data": "charge_id",
					
				},	
				{
					"data": "article",	
				
				},
				{ 	
					"data" : "section",
				
				},
				{ 	
					"data" : "description",
				}	
			],
			sPaginationType: "full_numbers",	
		});
		
		onChargesSelection(table_id);
    }
    
   
    function onChargesSelection(table_id){
        $('#' + table_id).off("change").on("change", ":checkbox", function() { 
			if($(this).is(':checked')){	
				
				CVM.Vars.charges.push($(this).attr('data-value'));
                
				var charge_obj = Object.create(charge_prototype);
				charge_obj.charge_id = $(this).attr('data-value');
				charge_obj.article = $(this).attr('data-article');
				charge_obj.section = $(this).attr('data-section');
				charge_obj.description = $(this).attr('data-description');
				
                CVM.Vars.chargesObj.push(charge_obj);
				
			}else{
				//remove item
				var idx = CVM.Vars.charges.indexOf($(this).attr('data-value'));
				CVM.Vars.charges.splice(idx, 1);
				CVM.Vars.chargesObj.splice(idx, 1);
			}
		});
    }
    
    function initExViolatorsTable(){
        $('#ex_violators_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "casereport/violators",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "15%", "aTargets": [ 0 ] },
                        { "sWidth": "35%", "aTargets": [ 1 ] },
                        { "sWidth": "20%", "aTargets": [ 2 ] },
                        { "sWidth": "22%", "aTargets": [ 3 ] },
                        { "sWidth": "8%", "aTargets": [ 3 ] },
                ],
                "order": [[ 0, "DESC" ]],
                "columns": [
                    { 
                        "data": "violator_id",
                    },	
                    { 	
                        "render": function(data,type,row,meta) {

                            return row.first_name + " " + row.last_name;
                        }

                    },
                    { 
                        "data": "alias",
                    },	
                    { 

                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            var str_append = 'thumbs/' + row.violator_id + '_front.jpg';
                            var img1 = '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
                            str_append = 'thumbs/' + row.violator_id + '_rear.jpg';
                            var img2 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
                            str_append = 'thumbs/' + row.violator_id + '_right.jpg';
                            var img3 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
                            str_append = 'thumbs/' + row.violator_id + '_left.jpg';
                            var img4 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';


                            return img1 + img2 + img3 + img4;

                        }
                    },
                    {
                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return '<button data-value=\"'+ row.violator_id + '\" data-name=\"' + row.first_name + ' '  + row.last_name + '\" class="btn btn-default">Select</button>';

                        }

                    },

                ],

                sPaginationType: "full_numbers",

		});
        
        $('#ex_violators_tbl tbody').off().on( 'click', 'button', function () {
			$('#selection-details').text('A violator with ID of ' + $(this).attr('data-value') + ' and name of ' + $(this).attr('data-name') + ' has been selected.');
			//get the parent tr
            var tr = $(this).parent().parent();
            //remove classes
            $('#ex_violators_tbl tbody').find('tr').removeClass('selected-existing');
            
            var options = {
				content: 'Selected!',
				role: 'info',
				duration: 800,
				width: 250,
			}
			var toast = new JToast(options);
			toast.makeToast();
			
            //set class of tr
            tr.addClass('selected-existing');
        
			CVM.Vars.selected_ex_violator = $(this).attr('data-value');
		});
    }
    
    /*================================================================
    *                       CONTEXT ACTIONS
    *===============================================================*/
    
    var context_action = function context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		var item_id = $(tr).find('td:first').text();
        
		switch (key) {
            case 'remove' :
                remove_violator(item_id);
            break;
                
            case 'view' :  
                view(item_id);
            break;
                
            default :
            break;
	   }           
	}
    
    function view(id){
        CVM.Vars.selected_violator = Main.Objects.get_object(CVM.Vars.violators,id,'violator_id')[0];	
        view_violator();	
    }
    function remove_violator(id){
 
        var callback = function(){
               var i = Main.Objects.index_of(CVM.Vars.violators, id, 'violator_id');
           
            if( i > -1){
                var removed = CVM.Vars.violators.splice(i,1)[0]; 
                CVM.Vars.violators_table.fnDeleteRow(i);

                //use removed to remove the violator to its association with items and nitems      
                var aoRemoved = new Array();
                var aoRemovedN = new Array();

                //ITEMS
                $.each(IM.Vars.items, function(i, obj){
                    $.each(obj.violators, function(j, v){
                        if(v.violator_id == removed.violator_id){
                            aoRemoved.push({
                                item_idx : i ,
                                violator_idx : j,
                            })
                        }
                    });
                }); 


                //loop backwards due to array live nature  
                for(var i = aoRemoved.length-1; i >= 0; i--){
                    var obj = aoRemoved[i];   
                    IM.Vars.items[obj.item_idx].violators.splice(obj.violator_idx,1);
                    if(IM.Vars.items[obj.item_idx].violators.length < 1){
                        var rem = IM.Vars.items.splice(obj.item_idx,1);
                        IM.Vars.items_rem.push(rem[0].item_id);
                        IM.Events._update_table(obj.item_idx, 'item_seized_tbl');
                    }
                }

                //NITEMS
                $.each(IM.Vars.nitems, function(i, obj){
                    $.each(obj.violators, function(j, v){
                        if(v.violator_id == removed.violator_id){
                            aoRemovedN.push({
                                item_idx : i ,
                                violator_idx : j,
                            })
                        }
                    });
                });

                //loop backwards due to array live nature  
                for(var i = aoRemovedN.length-1; i >= 0; i--){
                    var obj = aoRemovedN[i];   
                    IM.Vars.nitems[obj.item_idx].violators.splice(obj.violator_idx,1);
                    if(IM.Vars.nitems[obj.item_idx].violators.length < 1){
                        var rem = IM.Vars.nitems.splice(obj.item_idx,1);
                        IM.Vars.nitems_rem.push(rem[0].item_id);
                        IM.Events._update_table(obj.item_idx, 'narcotics_seized_tbl');
                    }
                }

                //ajax to remove images of the removed violator.
                var formdata = new FormData();
                formdata.append('id', removed.violator_id);
                Main.Mod.do_ajax('casereport/removetemp','POST',function(){},formdata);

            }
        
        }

        var message = 'Are you sure you want to remove this violator? \n' +
            'If this violator is associated with items the association or items might be removed too.';
        Main.Mod.show_bootbox_confirm(message,'Confirmation',callback,null);
        
    }
    
    var attach = function attach(type,key,options){
        switch(type){
            case events_const.ON_SHOW_NEW_VIOLATOR:
                on_show_new_violator();
                break;
            case events_const.ON_SHOW_EXISTING_VIOLATORS:
                on_show_existing_violators();
                break;
            case events_const.ON_VIOLATOR_UPDATE:    
                on_violator_update();
                break;
            case events_const.CONTEXT_ACTION:
                context_action(key,options);
                break;
            default:
                alert('Cannot attach events. Contact your IT support!');
        }
    }
    
    return {
        attach : attach, 
        events_const : events_const,
        update_table : update_table,
    }

})();