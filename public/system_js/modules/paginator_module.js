//Paginate the motherfucker pagination. Dependency simplePagination.js



var Paginator = function(){

    var pagination;
    var cards;
    var aoitems;
    var nItems;
    var perPage;
    var sysMod;
    
    var cardCreate;
    //create_pagination
    function paginate(items,page_id, card_id, num_items, num_per, sys_mod){
        pagination = $('#' + page_id);
        cards = $('#' + card_id);
        aoitems = items;
        nItems = num_items;
        perPage = num_per;
        sysMod = sys_mod;
        
        switch(sysMod){
            case 'Violator':
                cardCreate = 'createViolatorCards';
                break;
            case 'Case' :
                cardCreate = 'createCards';
                break;
            case 'PNP' :
                cardCreate = 'createPnpCards';
                break;
            case 'Images' :
                cardCreate = 'createImageCards';
                break;
        }  
        
        pagination.pagination({
            items:  num_items,
            itemsOnPage:  num_per,
            onPageClick : function(pageNumber, event){	
                paging(pageNumber);
            }
        });
        
        onSelectChange();
       
    }
    
    var paging = function paging(pageNumber){
        cards.sscardify('destroy');
        if(pageNumber == 1){
            pageNumber = 0;
            last_page = pageNumber + perPage;
            console.log(pageNumber + ' ' + last_page);
            cards.sscardify(cardCreate, aoitems.slice(pageNumber,last_page));
        } else {
            var last = parseInt(pageNumber) * perPage;
            var first = last - perPage;
           
            cards.sscardify(cardCreate, aoitems.slice(first,last));
        }
        $('.scroll').mCustomScrollbar();
    }
    
    var reinitialise = function(){
        console.log(pagination);
        pagination.pagination('destroy');
        pagination.pagination({
			items:  nItems,
			itemsOnPage:  perPage,
			onPageClick : function(pageNumber, event){		
				paging(pageNumber);	
			}
		});
    }

  function onSelectChange(){
        var select = '';
        if(sysMod == 'Violator'){
            select = 'violator_per_page';
        } else if (sysMod == 'Case'){
            select = 'case_per_page';
        } else if(sysMod == 'Case'){
              select = 'case_per_page';
        } else {
            select = 'images_per_page';
        }
        
        console.log(select);
        $('#' + select).change(function() {
            var page = parseInt($('#' + select + ' option:selected').text());
            switch(page){
                case 6:  
                    perPage = 6;
                    paging(1);
                    reinitialise();
                    break;
                case 12:
                    perPage = 12;
                    paging(1);
                    reinitialise();
                    break;         
                case 18:
                    SM.Vars.casePerPage = 18;
                    break;
                default:
                    console.log('Error in handling select change');
            }
        });
     
        
    }
    
  
    
    return {
        paginate : paginate,
       
    }
    
};