var VM = VM || {};

VM.Vars = (function(){
    
})();


VM.Main = (function(){

    var vehicle_id = "";
    
    var onLoad = function onLoad(){
        VM.Events.attach(VM.Events.events_const.ON_ADD_VEHICLE);
        VM.Events.attach(VM.Events.events_const.ON_VEHICLE_SAVED);
        VM.Events.attach(VM.Events.events_const.ON_VEHICLE_UPDATE);
        //VM.Main.vehicles_context_action();
        //Image uploading
        var idsMap = {
            file : { preview : '#filepreview'},
            fileView : { preview : "#filepreviewView"}, 
        }
        var selectors = '#file,#fileView';
        

        Main.Mod.handle_image_change(selectors, idsMap);
	   
        init_id();
        init_table();
        
        $.contextMenu({
            selector: '.item-context', 
            callback: function(key, options) {
            // CM.InventoryDetails.context_action(key, options);
            VM.Main.vehicles_context_action(key, options);
            },
            items: {
                "view": {name: "View", icon: 'view'},   
            }

        });
    }
       
   
    function init_table(){
    
		$('#vehicles_tbl').DataTable({
		//so that all rows are not rendered on init time.
		"deferRender": true,
		"autoWidth": false,
		//for reinitialisation purpose
		destroy: true,
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		//NOTE: We are directly providing group id since we want to get said-sotg members only
		"ajax" : {
				url :  Main.Vars.host + "vehicles/retrieve",
				type : "GET",
			},
			"aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4,5 ] },
					{ "sWidth": "10%", "aTargets": [ 0 ] },
					{ "sWidth": "30%", "aTargets": [ 1 ] },
					{ "sWidth": "20%", "aTargets": [ 2 ] },
					{ "sWidth": "20%", "aTargets": [ 3 ] },
				    { "sWidth": "20%", "aTargets": [ 4 ] },
			],
			"columns": [
                {
                    "data" : "vehicle_id",
                },

				{ 
					"data": "type",
                    
				},
                { 
					"data": "make",
				},	
				{ 
					"data": "color",
				},	   
				{ 
					"data": "plate_no",
				},	
				{
                    "render": function(data,type,row,meta) {
                        //console.log(base_path + row.file_path_thumb);
                        return '<img style="margin-left:0px;" class="obj-pic" src=' + (Main.Vars.base_path + row.image_path) + '/' + row.vehicle_id + '_thumb.jpg' + ' />';

                    }
                
                },

				
			],
               "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                $(nRow).addClass('item-context');   
            },

			
			sPaginationType: "full_numbers",

		});
   
    }
    
   
    function init_id(){  
        Main.Mod.do_ajax('vehicles/id','GET', function(data){
            $('#vehicles_id').text(data.id);
        },null);
    
    }
    /*================================================================
    *                      CONTEXT ACTIONS
    *===============================================================*/

     var vehicles_context_action = function vehicles_context_action(key,options){
        var cellIndex = parseInt(options.$trigger[0].cellIndex),
        //use rowIndex to identify row.
        tbody = options.$trigger[0].parentNode;
        //rowIndex = row.index();
        var rowIndex = options.$trigger[0].rowIndex;
        var tr = $(tbody).find(':nth-child(' + rowIndex +')');
        vehicle_id = $(tr).find('td:first').text();
        var className = (options.$trigger[0].className.split(" ")[0]);
        switch (key) {
           case 'remove' :
                    ci_remove_item(vehicle_id);
                break;
            case 'view' :
                    ci_view_item(vehicle_id);        
                break;
           default :
               break;
       }           
    }
    //eto na viewing sa narcotics
     function ci_view_item(id){

       $('#viewModal').modal(Main.Vars.modal_option);
           
              $.ajax({
                    type: 'GET',
                    url: Main.Vars.host + 'vehicles/view/' + id,
                    contentType: false,
                    cache: false,   
                    processData:false, 
                    dataType: 'json',
                    success: function(data){
                        if(data.error == true){
                            alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
                        } else {
                            
                            $('#vehicle_view').text(vehicle_id);
                            $('#type').val(data.data.type);
                            $('#make').val(data.data.make);
                            $('#color').val(data.data.color);
                            $('#plate_no').val(data.data.plate_no);
                            $('#filepreviewView').attr('src', (Main.Vars.base_path + data.data.image_path) + "/" + id  + '.jpg');
                            
                        }
                    }
                });
    }
    function ci_remove_item(id){
        alert("tangina");
    }
    

    return {
        onLoad : onLoad,
        vehicles_context_action : vehicles_context_action,
    }


    
    
})();


VM.Events= (function(){
    
    var events_const = {
        'ON_ADD_VEHICLE' : 1, 
        'ON_VEHICLE_SAVED' : 2,
        'ON_VEHICLE_UPDATE' : 3,
        
    }
    
    function on_vehicle_saved(){
         $("#add_vehicle_form").on('submit',(function(e){
            e.preventDefault();
            var formdata = new FormData(this);
            formdata.append('vehicle_id', $('#vehicles_id').text().trim());
            formdata.append('file', $('#file')[0].files[0]);
            Main.Mod.do_ajax('vehicles/add', 'POST', function(data){            
                if(data.success){
                    Main.Mod.show_bootbox_alert(data.message,'Success', function(){
                        $('#vehicles_tbl').DataTable().ajax.reload();
                        $('#addModal').modal('hide');
                        //Main.Mod.clear_form_inputs('add_vehicles', new Array('text','file'));
                        //$('#filepreview').attr('src', Main.Vars.host  + 'public/resources/default_img.jpg');
                        $('#vehicles_id').text(data.id);            
                    });  
                } else {
                    Main.Mod.show_bootbox_alert('Error in saving vehicle. Contact IT support or try again.','Success');     
                }
            }, formdata);
        }));
    
    }
    function on_add_vehicle(){
        $('#addButton').on('click', function(){
            Main.Mod.show_modal('addModal',800,-100);
        });
    }
    function on_vehicle_update(){

        $("#view-vehicle-form").on('submit',(function(e){

            e.preventDefault();
            var formdata = new FormData(this);
            formdata.append('vehicle_id', $('#vehicle_view').text().trim());
            formdata.append('file', $('#fileView')[0].files[0]);
            Main.Mod.do_ajax('vehicles/update_vehicle', 'POST', function(data){            
                if(data.success){
                    Main.Mod.show_bootbox_alert(data.message,'Success', function(){
                        $('#vehicles_tbl').DataTable().ajax.reload();
                        $('#viewModal').modal('hide');
                        //Main.Mod.clear_form_inputs('view_vehicles', new Array('text','file'));
                         //$('#filepreviewView').attr('src', Main.Vars.host  + 'public/resources/default_img.jpg');
                          
                    });  

                } else {
                    Main.Mod.show_bootbox_alert('Error in saving vehicle. Contact IT support or try again.','Success');     
                }

            }, formdata);
        }));
    }
    var attach = function attach(type){
         
        switch(type){
            case events_const.ON_ADD_VEHICLE:
                on_add_vehicle();
                break;
            case events_const.ON_VEHICLE_SAVED:
                on_vehicle_saved();
                break;
            case events_const.ON_VEHICLE_UPDATE:
                on_vehicle_update();
                break;
            default:
                alert('Cannot attach events.');
        
        }
    }
     

        
    return {
        attach : attach,
        events_const : events_const,

    }

})();
