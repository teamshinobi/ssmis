var SM = SM || {};

SM.Vars = (function(){
           
    var q = '';
    
    var caseCards;
    var caseResults = new Array();
    var casePagination;
    var casePerPage;
    
    var violatorCards;
    var violatorResults = new Array();
    var violatorPagination;
    var violatorPerPage;
    
    var pnpCards;
    var pnpResults = new Array();
    
    var imagesCards;
    var imagesResults = new Array();
           
    return {
        q : q,
        caseCards : caseCards,
        caseResults : caseResults,
        casePagination : casePagination,
        casePerPage : casePerPage,
        
        violatorCards : violatorCards,
        violatorResults : violatorResults,
        violatorPagination : violatorPagination,
        violatorPerPage : violatorPerPage,
        
        pnpCards : pnpCards,
        pnpResults : pnpResults,
        
        imagesCards : imagesCards,
        imagesResults : imagesResults,
    }
       
})();

SM.Main = (function(){
    
    var onLoad = function onLoad(){
        
        var curr_url = window.location.href;
        var params = curr_url.extract();
        
        SM.Vars.q = params.k;

        //show loading
       Main.Mod.show_modal('loadingModal', 700, '');
        //this would be the default ajax on first load;
        Main.Mod.do_ajax('search/q_case/' + SM.Vars.q ,'GET',case_callback,null);
    
        //handle tabs click events
        SM.Events.attach(SM.Events.events_const.ON_TAB_CLICK);
    }
    
    
    var case_callback = function case_callback(data){
        setTimeout(function(){
          if(data.has_data == false){
                $('#retResults').text('Results Returned: ' + data.message);
                //hide
                $('#case_per_page').hide();
                $('#caseResultsMessage').text("No results found!");
                $('#caseCount').hide();
          } else {
                SM.Vars.caseCards  = $('#caseCards');
                SM.Vars.casePerPage = 6;
                $('#caseResultsMessage').hide();
                $('#caseCount').text('Query returned ' + data.data.length + ' results...');

                $.each(data.data, function(idx, obj){
                    var kaso = {};

                    kaso.caseId = obj.case_id;
                    kaso.caseDate = obj.date_of_arrest;
                    kaso.caseBlotter = obj.blotter_entry_nr;

                    if(obj.summary.length > 100){
                        kaso.caseSummary = obj.summary.substring(0,95) + '...';
                    } else {
                      kaso.caseSummary =  obj.summary;
                    }

                    kaso.caseImg = Main.Vars.host + obj.doc_path + obj.case_id + '_thumb.jpg';
                    kaso.suspectsCount = obj.count_violators;
                    kaso.caseSuspects = obj.violators;
                    kaso.caseOperation = obj.nature;
                    kaso.progress = obj.progress;
                    
                    var investigator = obj.investigator.rank + " " +
                    obj.investigator.first_name + " " + obj.investigator.last_name + " " + obj.investigator.middle_name.substring(0,1) + ".";
                
                    kaso.caseInvestigator = investigator;
                    
                    SM.Vars.caseResults.push(kaso);

                });

                 SM.Vars.caseCards.sscardify({
                    'module': 'casereport',	
                    'contents' : SM.Vars.caseResults.slice(0,6),
                    'onViewClick' : function(itemId, ev){
                    Main.Mod.show_modal('loadingModal',750,'');
                    
                    var callback = function(){
                        Main.Mod.do_ajax('viewcasereports/get_access/' + itemId,'POST',function(data){
                            if(data.proceed){
                                location.assign(Main.Vars.host + 'viewcasereports/view/' + itemId);
                            }  else {
                                $('#loadingModal').modal('hide');
                                Main.Mod.show_bootbox_alert('You don\'t have access to this case.',
                                                           'Warning', null);
                            }           
                        },null);
                    }
                    
                    
                    Main.Mod.do_ajax('editcasereport/full/' + itemId,'GET',function(data){
                        var progress = data.case.case_details.progress;
                        if(progress != 'Dismissed' && progress != 'Closed'){
                            callback();
                        } else {
                            location.assign(Main.Vars.host + 'viewcasereports/view/' + itemId);
                        }
                    },null)
                            
                    
                },
                'onEditClick' : function(itemId,ev){
                    
                    Main.Mod.show_modal('loadingModal',750,'');
                    Main.Mod.do_ajax('viewcasereports/get_access/' + itemId,'POST',function(data){
                        if(data.proceed){
                           location.assign(Main.Vars.host + 'editcasereport/edit/' + itemId);
                        }  else {
                            $('#loadingModal').modal('hide');
                            Main.Mod.show_bootbox_alert('You don\'t have access to this case.',
                                                       'Warning', null);
                        }           
                    },null);
                }
                });


                var casePaginator = new Paginator();

                casePaginator.paginate(SM.Vars.caseResults,'casePagination','caseCards', SM.Vars.caseResults.length, 6, 'Case');

                $(".scroll").mCustomScrollbar();
          }
             $('#loadingModal').modal('hide');
        },1500);//end setTimeout  
    }
    
    
    
    return {
        onLoad : onLoad,
        case_callback : case_callback,
    }
})();

SM.Events = (function(){
    
    
    var events_const = {
        'ON_TAB_CLICK' : 1, 
    }
    
    function on_tab_click(){
        $('#myTab li a').off('click').on('click',function(){
            var text = $(this).text().trim();
            switch(text){
                case 'Violators':
                    violator_tab(); 
                    break;
                case 'Cases':
                    case_tab();
                    break;
                case 'PNP' :
                    pnp_tab();
                    break;
                case 'Images' :
                    images_tab();
                    break;
                default:
                    alert('OMG OMG OMG! This is impossible LOLS.');
            }
        });
    }
    
    function pnp_tab(){
        Main.Mod.show_modal('loadingModal', 700, '');
        SM.Vars.pnpResults.length = 0;
        Main.Mod.do_ajax('search/q_pnp/' + SM.Vars.q ,'GET',pnp_callback,null);
    }
    
    function case_tab(){
        Main.Mod.show_modal('loadingModal', 700, '');
        SM.Vars.caseResults.length = 0;
        $('#caseCards').empty();
        Main.Mod.do_ajax('search/q_case/' + SM.Vars.q ,'GET',SM.Main.case_callback,null);
    }
    
    function violator_tab(){
        Main.Mod.show_modal('loadingModal', 700, '');
        Main.Mod.do_ajax('search/q_violators/' + SM.Vars.q ,'GET',violator_callback,null);
    }
    
    function images_tab(){
        Main.Mod.show_modal('loadingModal', 700, '');
        SM.Vars.imagesResults.length = 0;
        Main.Mod.do_ajax('search/q_images/' + SM.Vars.q ,'GET',images_callback,null);
    }
    
    
    function images_callback(data){
        SM.Vars.imagesCards  = $('#imagesCards');
        
        setTimeout(function(){ 
            if(data.has_data == false){
                $('#imagesResultsMessage').text(data.message);
                $('#imagesCount').hide(); 
                $('#loadingModal').modal('hide');
            } else {
                $('#imagesResultsMessage').hide();
                $('#imagesCount').text('Query returned ' + data.data.length + ' results...');
                $.each(data.data, function(idx,obj){
                    var imageFile = {};
                    imageFile.filePath = obj.file_path + "/" + obj.violator_id + "_front.jpg";
                    imageFile.fileName = obj.violator_id + "_front.jpg";
                    imageFile.desription = "Front mugshot of " + obj.first_name + " " + obj.last_name;

                    SM.Vars.imagesResults.push(imageFile);

                    imageFile = {};
                    imageFile.filePath = obj.file_path + "/" + obj.violator_id + "_rear.jpg";
                    imageFile.fileName = obj.violator_id + "_rear.jpg";
                    imageFile.desription = "Rear mugshot of " + obj.first_name + " " + obj.last_name;

                    SM.Vars.imagesResults.push(imageFile);

                    imageFile = {};
                    imageFile.filePath = obj.file_path + "/" + obj.violator_id + "_right.jpg";
                    imageFile.fileName = obj.violator_id + "_right.jpg";
                    imageFile.desription = "Right mugshot of " + obj.first_name + " " + obj.last_name;

                    SM.Vars.imagesResults.push(imageFile);

                    imageFile = {};
                    imageFile.filePath = obj.file_path + "/" + obj.violator_id + "_left.jpg";
                    imageFile.fileName = obj.violator_id + "_left.jpg";
                    imageFile.desription = "Left mugshot of " + obj.first_name + " " + obj.last_name;

                    SM.Vars.imagesResults.push(imageFile);
                });
                SM.Vars.imagesCards.sscardify({
                    'module': 'images',	
                    'contents' : SM.Vars.imagesResults.slice(0,6),
                    'onViewClick' : function(itemId, ev){
                        //location.assign(Main.Vars.host + 'violators/view/' + itemId);
                    }
                });

                var pnpPaginator = new Paginator();

                pnpPaginator.paginate(SM.Vars.imagesResults,'imagesPagination', 'imagesCards', SM.Vars.imagesResults.length, 6, 'Images');

            } 
             $('#loadingModal').modal('hide');
        },1000);
 
       
    }
    
    function pnp_callback(data){
        SM.Vars.pnpCards  = $('#pnpCards');
        $('#pnpCards').empty();
        
        setTimeout(function(){ 
            if(data.has_data == false){
                $('#pnpResultsMessage').text(data.message);
                $('#pnpCount').hide(); 
                $('#loadingModal').modal('hide');
            } else {

                $('#pnpResultsMessage').hide();
                $('#pnpCount').text('Query returned ' + data.data.length + ' results...');

                $.each(data.data, function(idx,obj){
                var p = {};
                    p.policeName = obj.first_name + ' ' + obj.last_name;
                    p.filePath = obj.file_path;
                    p.policeRank = obj.rank;
                    p.status = obj.status;
                    p.pnpId = obj.pnp_id;
                    p.address = obj.address;

                    SM.Vars.pnpResults.push(p);
                });



                SM.Vars.pnpCards.sscardify({
                    'module': 'pnp',	
                    'contents' : SM.Vars.pnpResults.slice(0,6),
                    'onViewClick' : function(itemId, ev){
                        //location.assign(Main.Vars.host + 'violators/view/' + itemId);
                    }
                });

                var pnpPaginator = new Paginator();

                pnpPaginator.paginate(SM.Vars.pnpResults,'pnpPagination', 'pnpCards', SM.Vars.pnpResults.length, 6, 'PNP');

            } 
            $('#loadingModal').modal('hide');
         },1000);
    
    }
    
    function violator_callback(data){
        
        setTimeout(function(){
        
             //reset all variables
            SM.Vars.violatorResults.length = 0;
            $('#violatorCards').empty();
            $('#violator_per_page option[value="6"]').prop('selected',true);

            SM.Vars.violatorCards  = $('#violatorCards');
            SM.Vars.violatorPerPage = 6;
           
            if(data.has_data == false){
                $('#violatorResultsMessage').text(data.message);
                $('#violatorCount').hide(); 
                 $('#loadingModal').modal('hide');
            } else {
                $('#violatorResultsMessage').hide();
                $('#violatorCount').text('Query returned ' + data.data.length + ' results...');
               
                $.each(data.data, function(idx,obj){
                    var v = {};

                    v.violatorName = obj.first_name + ' ' + obj.last_name;
                    v.violatorAlias = obj.alias;
                    v.violatorNationality = obj.nationality;
                    v.violatorOccupation = obj.occupation;
                    v.violatorFilePath = obj.file_path;
                    v.violatorId = obj.violator_id;
                    SM.Vars.violatorResults.push(v);

                });

                SM.Vars.violatorCards.sscardify({
                    'module': 'violator',	
                    'contents' : SM.Vars.violatorResults.slice(0,6),
                    'onViewClick' : function(itemId, ev){
                        location.assign(Main.Vars.host + 'violators/view/' + itemId);
                    }
                });
                
                
                var violatorPaginator = new Paginator();
                
                violatorPaginator.paginate(SM.Vars.violatorResults,'violatorPagination', 'violatorCards', SM.Vars.violatorResults.length, 6, 'Violator');

                $(".scroll").mCustomScrollbar();
            }
        
            $('#loadingModal').modal('hide');
        },1500); 
    }
    
    
    var attach = function attach(type){
        switch(type){
            
            case events_const.ON_TAB_CLICK:
                on_tab_click();
                break;
                
            default : 
                alert('Cannot attach events.');
        }
    }

    
    return {
    
        attach : attach,
        events_const : events_const,
    
    }
    
})();