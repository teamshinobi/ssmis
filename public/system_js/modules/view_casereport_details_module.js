var VCRDM = VCRDM || {};

VCRDM.Vars = (function(){
    
    var case_id = '';
    var dist_color = new Array('default','primary','secondary',
                               'tertiary','success','info','warning','danger');
   
    return {
        case_id : case_id,
        dist_color: dist_color,
     
    }
})();

VCRDM.ViewCaseReportDetailsModule = (function(){

	var onLoad = function onLoad(){
        Main.Mod.do_ajax('casereport/ss_clear_temps/', 'POST', function(){}, null);
        
        var url = window.location.href;
		var res = url.split("/");	
		VCRDM.Vars.case_id = res.slice(-1)[0]; 

        var datepickers = ['#datepicker'];
        Main.Init.InitDatePicker(datepickers);
        
        init_casedetails();
        
        init_distributions();
        distribute_copies(); 
       
        editEvidencesStatusListener();
        editCcJudgeListener();
        
        //enable magnific
        $('.pop-up').magnificPopup(
            {
                type :'image',
                gallery: {
                  enabled: true,
                },
                removalDelay: 300,
            }
        );
        
        //Events
        VCRDM.Listeners.viewDetailsListener();
      
	};
	
	function init_casedetails(){
		Main.Mod.do_ajax('editcasereport/full/' + VCRDM.Vars.case_id, 'GET', casereport_details_callback, null );
	}
    /*================================ CC / JUDGE =====================================*/
    
    function editCcJudgeListener(){
        $('#edit_cc').on('click',function(){
            Main.Mod.show_modal('editCcJudgeModal',620,'');
        }); 
        
        $('#editCcJudgeButton').on('click',function(){
            var callback = function(){
                
                Main.Mod.show_modal('loadingModal',800,'');
                
                var formdata = new FormData();
                formdata.append('case_id',$('#case_id').text().trim());
                formdata.append('cc', $('#cc_i').val());
                formdata.append('judge', $('#cc_judge_i').val());
                
                 Main.Mod.do_ajax('viewcasereports/update_cc_judge', 'POST',function(data){
                     $('#loadingModal').modal('hide');
                     if(data.success) {
                        $('#editCcJudgeModal').modal('hide');
                        $('#cc').text($('#cc_i').val() == "" ? 'No information yet.' : $('#cc_i').val());   
                        $('#cc_judge').text($('#cc_judge_i').val() == "" ? 'No information yet.' : $('#cc_judge_i').val());  
                        Main.Mod.show_bootbox_alert('Succesfully updated!', 'Success!', null);
                    } else {
                        Main.Mod.show_bootbox_alert('Cannot update due to some error!', 'Error!', null);
                    }
                }, formdata);
                
            }
            Main.Mod.show_bootbox_confirm('Are you sure you want to edit the cc/judge?', 'Confirmation',
                                        callback,null);
        }); 
    }
    
    /*=============================== END CC / JUDGE ====================================*/
    
    
    /*=============================== STATUS OF EVIDENCES  =============================*/

    
    function editEvidencesStatusListener(){
        $('#edit_status').on('click',function(){
            Main.Mod.show_modal('editStatusModal',620,'');
        });
        
        $('#editStatusButton').on('click', function(){
            var callback = function(){
                var formdata = new FormData();
                formdata.append('case_id',$('#case_id').text().trim());
                formdata.append('status', $('#status_ev').val());
                
                Main.Mod.do_ajax('viewcasereports/update_status', 'POST',function(data){
                    if(data.success) {
                        $('#editStatusModal').modal('hide');
                        $('#status_ev_p').text($('#status_ev').val());
                        Main.Mod.show_bootbox_alert('Succesfully updated!', 'Success!', null);
                    } else {
                        Main.Mod.show_bootbox_alert('Cannot update status due to some error!', 'Error!', null);
                    }
                }, formdata);
            }
            
            Main.Mod.show_bootbox_confirm('Are you sure you want to edit the status?', 'Confirmation',
                                        callback,null);
        });
    }
    
 
    
    /*============================ END OF STATUS OF EVIDENCES  ==========================*/
    
    
    
    /*================================ DISTRIBUTION  ==============================*/
    
    function init_distributions(){     
        Main.Mod.do_ajax('casereport/distributions/' + VCRDM.Vars.case_id, 'GET', function(data){
            if(data.has_data){
                var str = '';
                $.each(data.data,function(idx,obj){
                    str += '<span data-id="' + obj.id + '"data-copies="' + obj.copies +'" data-date="' + obj.when + '"class="dist label label-' +VCRDM.Vars.dist_color[idx] + '">';
                    str += obj.distributed_to + '</span>'
                });
                
                $('#distributions').append(str);
            }
        }, null );
        
        //handle event clicking
        $( "#distributions" ).delegate( ".dist", "click", function() {
            var copies = $(this).attr('data-copies');
            var when = $(this).attr('data-date'); 
            var id = $(this).attr('data-id'); 
            Main.Mod.show_modal('editDistributionModal', 600,'');
            
            $('#distributed_to_v').val($(this).text().trim());
            $('#copies_v').val(copies);
            $('#when_v').val(when);
            $('#d_id').text(id);
            $('#editDistributionButton').on('click',function(){
                 Main.Mod.show_bootbox_confirm("Save Changes?","Confirm",update_distribution,null);
            });
        });
        
        $('#deleteDistribution').on('click',function(){
            delete_distribution();
        });
    }
    
    function delete_distribution(){
        
        var callback = function(){
            var formdata = new FormData();
            formdata.append('id', $('#d_id').text().trim());
            
            Main.Mod.do_ajax('casereport/delete_distribution','POST',function(data){
                if(data.success){
                
                    Main.Mod.show_bootbox_alert('Successfully removed!','Success',function(){
                        window.location.reload();
                    });
                    
                } else {
                    
                    Main.Mod.show_bootbox_alert('Unable to remove distribution','Success',function(){
                        window.location.reload();
                    });
                }
                
            },formdata);
        }
        
        Main.Mod.show_bootbox_confirm('Are you sure you want to delete this distribution?',
                                     'Confirmation',callback, null);
    }
    
	function update_distribution(){
        $('loadingModal',750,'');
        var formdata = new FormData();
        
        formdata.append('case_id', $('#case_id').text().trim());
        formdata.append('distributed_to',$('#distributed_to_v').val());
        formdata.append('copies', $('#copies_v').val());
        formdata.append('when', $('#when_v').val());
        
        Main.Mod.do_ajax('casereport/update_distribution', 'POST', function(data){
            $('#loadingModal').modal('hide');
            if(data.success){
                Main.Mod.show_bootbox_alert("Success!", "Success", function(){
                    window.location.reload();
                });
            } else {
                Main.Mod.show_bootbox_alert("Failed!", "Failed", null);
            }
        },formdata);
    }
    
    function distribute_copies(){
       
        $('#distribute').on('click',function(){
            Main.Mod.show_modal('addDistributionModal', 600,'');
            $('#addDistributionButton').off('click').on('click',function(){
                
                perform_distribute();
            });
        });
    }
    
    function perform_distribute(){
        
        var call_back = function(){
            
            $('loadingModal',750,'');
            
            var formdata = new FormData();
            formdata.append('case_id', $('#case_id').text().trim());
            formdata.append('distributed_to',$('#distributed_to').val());
            formdata.append('copies', $('#copies').val());
            
            Main.Mod.do_ajax('casereport/distribute/', 'POST', function(data){
                
                $('#loadingModal').modal('hide');
                $('#addDistributionModal').modal('hide');
                if(data.success){
                    var audit_description = 'has given copies of case ' +  $('#case_id').text() + ' to ' + $('#distributed_to').val();
                    var audit_operation = 'added distribution'
                    Main.Audit.save_audit(audit_description, audit_operation);
                    Main.Mod.show_bootbox_alert("Success!", "Success", function(){
                        window.location.reload();
                    });
                
                } else {
                    Main.Mod.show_bootbox_alert("Failed!", "Failed", null);
                }
                
            }, formdata);
        }
        
        
        Main.Mod.show_bootbox_confirm("Are you sure you want to add distribution?","Confirmation",
                                        call_back,null);
    }
    
    /*================================ END DISTRIBUTION  ==============================*/
	
    var casereport_details_callback = function casereport_details_callback(data){
        create_carousel(data);
        load_case_details(data.case.case_details);
        load_arrest_details(data.case.arrest_details);
        load_violator_details(data);
        load_arresting_officers(data.case.arrest_details.arresting_officers);
        load_other_information(data);
        load_attachments(data.case.case_details);
    }
    
    
    function load_attachments(caserep){
        
        $('#spot').append(caserep.spot_report_id == 'NONE' ? 'No attachment yet' :
                        'You can view the attachment in this link.  <a>'  + caserep.spot_report_id + '</a>');
        $('#preop').append(caserep.pre_op_id == 'NONE' ? 'No attachment yet' :
                        'You can view the attachment in this link.  <a>'  + caserep.pre_op_id + '</a>');
        $('#coord').append(caserep.coordination_id == 'NONE' ? 'No attachment yet' :
                        'You can view the attachment in this link.  <a>'  + caserep.coordination_id + '</a>');
        $('#main').append(caserep.main_affidavit_id == 'NONE' ? 'No attachment yet' :
                        'You can view the attachment in this link.  <a>'  + caserep.main_affidavit_id + '</a>');
        $('#supp').append(caserep.supp_affidavit_id == 'NONE' ? 'No attachment yet' :
                        'You can view the attachment in this link.  <a>'  + caserep.supp_affidavit_id + '</a>');
        load_scanned_docs(caserep.case_id);
    }
    
    function load_scanned_docs(case_id){
        Main.Mod.do_ajax('casereport/scan_permanent_dir/' + case_id,'GET', function(data){
            data.files.laboratory.splice(0,2);
            render_documents(data.files.laboratory,case_id,'labDocuments','laboratory');
            data.files.medical.splice(0,2);
            render_documents(data.files.medical,case_id,'medDocuments','medical');
            data.files.search.splice(0,2);
            render_documents(data.files.search,case_id,'searchDocuments','search');
            data.files.warrant.splice(0,2);
            render_documents(data.files.warrant,case_id,'warrantDocuments','warrant');
            data.files.others.splice(0,2);
            render_documents(data.files.others,case_id,'othersDocuments','others');
        }, null);
    }
    function render_documents(files,case_id,selector,folder){
        
        var documents = $('#' + selector);
     
        var str = '';
        if(files.length < 1){
            str = '<div class="col-md-6 col-lg-6">' + 
                    '<p> No attachment yet.</p>' +
                  '</div>';
        } else {
            str =  '<div class="col-md-6 col-lg-6">';
    
            $.each(files, function(idx,obj){
                //dont use thumb images
                if(obj.indexOf('thumb') < 0){
                    str += '<a class="pop-up" href="' + Main.Vars.host + 'public/resources/photos/case/' +case_id + '/' + folder + '/' + obj+ '"><img class="doc-pic" src="' + Main.Vars.host + 'public/resources/photos/case/' +case_id + '/' + folder + '/' + obj+ '" /></a>';
                    
                }        
            });
            str +='</div>'; 
        }
        documents.append(str);  
    }
    
    function load_arresting_officers(ao){
        var list = $('#arrestingList');
       
        $.each(ao,function(idx,obj){
            
            var str = '<li>' + obj.rank  + ' ' + obj.last_name + ' ' + obj.first_name  + '</li>';
        
            list.append(str);
        });
        
        
    }
    
    function load_other_information(data){
        console.log(data);
        var list = $('#seizing');
        var b_name = data.case.arrest_details.booker.rank + ' ' + data.case.arrest_details.booker.first_name +
            ' ' + data.case.arrest_details.booker.last_name;
        var i_name = data.case.arrest_details.investigator.rank + ' ' + data.case.arrest_details.investigator.first_name +
            ' ' + data.case.arrest_details.investigator.last_name;
        $('#booked_by').text(b_name);
        $('#investigator').text(i_name);
        
        $.each(data.case.seizing_officers,function(idx,obj){
            var li = '<li>' + obj.rank + ' ' + obj.first_name + ' ' + obj.last_name + '</li>';
            
            list.append(li);
        });
        
        if(data.case.arrest_details.fingerman != null){
            var fingerman = data.case.arrest_details.fingerman;
            var f_name = fingerman.rank + ' ' + fingerman.first_name + ' ' + 
                fingerman.last_name;
            $('#fingerman').text(f_name);
        } else {
            $('#fingerman').text('---');
        }
        
        if(data.case.arrest_details.photographer != null){
            var photographer = data.case.arrest_details.photographer;
            var p_name = photographer.rank + ' ' + photographer.first_name + ' ' +
                photographer.last_name;
            $('#photographer').text(p_name);
        } else {
            $('#photographer').text('---');
        }
        
    }
    function load_violator_details(data){
        
        var aoViolators = new Array();
        var violators  = data.case.violators;
        var items = data.case.items;
        var nitems = data.case.nitems;
        
        $.each(violators, function(idx, obj){
            var violator = {};
            violator.violator_id = obj.violator_id;
            violator.alias = obj.alias;
            violator.first_name = obj.first_name;
            violator.middle_name = obj.middle_name;
            violator.last_name = obj.last_name;
            violator.nationality = obj.nationality;
            violator.file_path = obj.file_path;
            violator.charges = obj.charges;
            violator.items = new Array();
            violator.nitems = new Array();
            //loop items
            $.each(items, function(idx1, obj1){
                $.each(obj1.violators, function(idx2,obj2){
                    //console.log('Comparing ' + violator.violator_id + ' ' + obj2.violator_id);
                    if(obj2.violator_id == violator.violator_id){
                        violator.items.push(obj1);
                    } 
                });
                
            });
            
            $.each(nitems, function(idx1, obj1){
                $.each(obj1.violators, function(idx2,obj2){
                    //console.log('Comparing ' + violator.violator_id + ' ' + obj2.violator_id);
                    if(obj2.violator_id == violator.violator_id){
                        violator.nitems.push(obj1);
                    } 
                });
                
            });   
            
            aoViolators.push(violator);
            
        });
        
        //rendering on screen
        var violatorsList = $('#violatorsList');
        $.each(aoViolators, function(idx,obj){
            var li = '<li class="violator">';
            //IMAGE and Info
            li += '<div class="violator-details">' +
                        '<img class="violator-image" src="' + Main.Vars.host + obj.file_path + '/' +  obj.violator_id + '_front.jpg" />' +  
                        '<p>' +      
                            '<span> NAME :  </span>'+ obj.last_name + ' ' + obj.first_name + '<br/>' + 
                            '<hr style="margin-bottom: 5px; height:1px;color:#333;background-color:#333;"/>' +
                            '<span> ALIAS: </span> ' + obj.alias + ' <br/>' +

                            '<span> Nationality: </span>' + obj.nationality + '<br/>' +
                        '</p>' +
                    '</div>';
            
            //Charges
            li +=   '<div class="charges-details">' +
                        '<p class="info-header"> Commited the following charges: </p>' +
                            '<ul class="chargesList">';
            
                    //loop charges
                    $.each(obj.charges, function(idx,obj){
                        
                        li += '<li>' + obj.article + ' ' + obj.section + ', </li>';
                    
                    
                    });
            
            li +=           '</ul>' +
                    '</div>';
            
            //Items
            li +=    '<div class="items-details">' + 
                        '<p class="info-header"> Items collected: </p>' +
                            '<ul class="itemsList">';
              
                        //loop items
                        if(obj.items.length > 0){
                             $.each(obj.items, function(idx,obj){
                               
                                li +=   '<li class="item"> ' +
                                            '<img class="item-image" src="' + Main.Vars.host + obj.file_path + '/thumbs/' + obj.item_seized_id + '_item.jpg' + '" />' +
                                             '<p class="details">' + 
                                                '<span> Quantity :  </span>' + obj.quantity + '<br/>' + 

                                                '<span> Markings: </span> ' + obj.markings + '  <br/>' + 

                                                '<span> Description: </span>' + obj.description + '<br/>' +
                                            '</p>' +
                                        '</li>';
                            });
                        }
                        
                        if(obj.nitems.length > 0){
                             $.each(obj.nitems, function(idx,obj){
                               
                                li +=   '<li class="item"> ' +
                                            '<img class="item-image" src="' + Main.Vars.host + obj.file_path + '/thumbs/' + obj.narcotics_seized_id + '_item.jpg' + '" />' +
                                             '<p class="details">' + 
                                                '<span> Quantity :  </span>' + obj.quantity + '<br/>' + 

                                                '<span> Markings: </span> ' + obj.markings + '  <br/>' + 

                                                '<span> Description: </span>' + obj.narcotics.narcotics_name + '<br/>' +
                                            '</p>' +
                                        '</li>';
                            });
                        }  
                
            li +=           '</ul>' +
                    '</div>';
            
            li += '</li>';
            violatorsList.append(li);
        });
        
       
      
    }
    
    function load_case_details(c_obj){
        $('#case_id').text(c_obj.case_id);
        $('#is_number').text(c_obj.is_number == "" ? '---'  : c_obj.is_number);  
        $('#nature').text(c_obj.nature);
        $('#title').text(c_obj.operation_name);
        $('#summary').append(c_obj.summary);
        $('#blotter').text(c_obj.blotter_entry_nr);
        var progress = '';
        if(c_obj.progress == 'Closed'){
            progress = c_obj.progress + ' ( ' + c_obj.date_closed + ' )';
        } else {
            progress = c_obj.progress;
        }
        
        $('#progress').text(progress);
        $('#status_ev_p').text(c_obj.status_of_evidences == "" ? '---'  : c_obj.status_of_evidences);     
        $('#cc').text(c_obj.cc == "" ? 'No information yet.'  : c_obj.cc);   
        $('#cc_judge').text(c_obj.cc_judge == "" ? 'No information yet.'  : c_obj.cc_judge);     
    }
    
    function load_arrest_details(a_obj){
        $('#date_of_arrest').text(a_obj.date_of_arrest); 
        $('#time_of_arrest').text(a_obj.time_of_arrest); 
        var location = a_obj.street_name + ' ' + a_obj.barangay + ' ' + a_obj.city_name;
        $('#location').text(location);
    }
    
    function create_carousel(data){
        var carousel = $('#photosCarousel');
        var violators = data.case.violators;
        var casereport = data.case.case_details;
        var slides = ' <div class="carousel-inner" role="listbox">';
        
        //case scene
        slides +=   '<div class="item active" >' +
                        '<img src="' + Main.Vars.host + casereport.doc_path + '/' +  casereport.case_id + '.jpg" alt="Photo">' +
                        '<div class="carousel-caption">' + 
                            '<h3>Case Scene</h3>' + 
                           
                          '</div>' +
                    '</div>';
    
        //violators
        $.each(violators, function(idx,obj){
    
              slides += '<div class="item">' +
                        '<img src="' + Main.Vars.host + obj.file_path + '/' +  obj.violator_id + '_front.jpg" alt="Photo">' +
                        '<div class="carousel-caption">' + 
                            '<h3>Mugshot</h3>' + 
                            '<h4>Front mugshot of ' + obj.first_name + ' ' + obj.last_name + '  </h4>' +
                          '</div>' +
                        '</div>';
             
        });
        
        slides += '<div>';
        
        carousel.append(slides);
        
        var chevrons =  
                  '<a class="left carousel-control" href="#photosCarousel" role="button" data-slide="prev">' +
                    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' + 
                    '<span class="sr-only">Previous</span>' +
                  '</a>' + 
                  '<a class="right carousel-control" href="#photosCarousel" role="button" data-slide="next">' +
                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
                    '<span class="sr-only">Next</span>' +
                  '</a>';
        carousel.append(chevrons);
    
    }
	
	//Depracated
    var report = function report(){
		init_report(VCRDM.Vars.case_id);
	}	
    //Depracated
	function init_report(id){
		window.location.assign(Main.Vars.host + 'viewcasereports/report/' + id);
	}
	
	return{
		onLoad : onLoad,
		report : report,
	}
})();

VCRDM.Listeners = (function(){

    
    var viewDetailsListener = function(){
        $('#caseReport').on('click', function(){
            Main.Mod.show_modal('loadingModal',650,'');
            window.location.assign(Main.Vars.host + 'viewcasereports/report/' + VCRDM.Vars.case_id); 
            /*===================================================================
            *
            *   The commented codes below lets the system evaluates if 
            *   the case is complete. Uncomment if you want to add the functionality.
            *
            =====================================================================*/
            
            /*var callback = function(data){
               
                $('#loadingModal').modal('hide');
                if(data.printable){
                    
                    Main.Mod.show_bootbox_alert("Please wait while the system is generating the report.",
                                               "Message", null);
                    
                    window.location.assign(Main.Vars.host + 'viewcasereports/report/' + VCRDM.Vars.case_id); 
                
                } else {
                    
                    var str = '<ul>';
                    
                    $.each(data.emps, function(idx,msg){
                        
                        str += '<li>' + msg + '</li>';
                    
                    });
                    
                    str += '</ul><br><br>';
                    
                    str += 'Please complete the case before printing.';
                        
                 
                    Main.Mod.show_bootbox_alert(str,"Message", null);
                
                }
            }
            
            Main.Mod.do_ajax('viewcasereports/is_case_complete/' + VCRDM.Vars.case_id,
                            'GET', callback, null);*/
        });
    }
    
    return {
        viewDetailsListener : viewDetailsListener,
    }
    
    
})();
