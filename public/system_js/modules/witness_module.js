var Witness = Witness || {};

Witness.Vars = (function(){
    
    var table = '';
    var witnesses = new Array();
    //holds the current selected witness
    var currentWitness = {};
    var fake_id = 1;

    return {
        table : table,
        witnesses : witnesses,
        currentWitness : currentWitness,
        fake_id : fake_id,
    }

})();
Witness.Main = (function(){
    
    var onLoad = function onLoad(){
        Witness.Events.attach(Witness.Events.events_const.ON_SHOW_WITNESS);
        
        init();
        
        
        //context menu
        $.contextMenu({
            selector: '.witness-context', 
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                Witness.Events.attach(Witness.Events.events_const.CONTEXT_ACTION,key,options);
            },
            items: {
                "view": {name: "View", icon: 'edit'},
                "remove": {name : "Remove", icon: 'delete'},
            }
        });
     
    }

    function init(){
        Witness.Vars.table = $('#witness_tbl');
        Witness.Vars.table.DataTable({
			"autoWidth": false,
            
			"aoColumnDefs": [ 
                
				{ "sWidth": "5%", "aTargets": [ 0 ] },
				{ "sWidth": "30%", "aTargets": [ 1 ] },
				{ "sWidth": "20%", "aTargets": [ 2 ] },
                { "sWidth": "20%", "aTargets": [ 3 ] },
			],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                $(nRow).addClass('witness-context');	
            }
		});  
    }
    
    return {
        onLoad : onLoad,
    };
})();


Witness.Events = (function(){
    
    var events_const = {
        'ON_SHOW_WITNESS' : 1,
        'CONTEXT_ACTION' : 2,
    };
    
    function on_show_witness(){
        $('#showAddWitnessModal').on('click',function(){
            Main.Mod.show_modal('addWitnessModal',800,-100);
            on_add_witness();
        });
    }
    
    function on_add_witness(){
        $('#addWitnessButton').off('click').on('click',function(){
            var name = $('#witnessName').val();
            var address = $('#witnessAddress').val();
            var contact = $('#witnessContact').val();
            
            var witness = {};
            witness.name = name;
            witness.address = address;
            witness.contact = contact;
            witness.fake_id = Witness.Vars.fake_id;
            
            //clear texts
            $('#witnessName').val('');
            $('#witnessAddress').val('');
            $('#witnessContact').val('');
            
            add_witness(witness);
        });
    }
    
    var add_witness = function add_witness(witness){
        Witness.Vars.table.dataTable().fnAddData([
            witness.fake_id,
            witness.name,
            witness.address,
            witness.contact,
        ]);
        Witness.Vars.fake_id++;
        Witness.Vars.witnesses.push(witness);
        $('#addWitnessModal').modal('hide');
    }
    
   
    /*========================================================
    *                   CONTEXT ACTION
    *
    *=======================================================*/
    var context_action = function context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
        var tr = $(tbody).find(':nth-child(' + rowIndex +')');
        var item_id = $(tr).find('td:first').text();
        
        switch (key) {
            case 'remove' :
               remove(item_id, rowIndex - 1);
            break;
            case 'view' : 
               view(item_id);
            break;
                
            default :
            break;
	   }           
	}
    
    function remove(item_id, row){   
      
        var idx =  Main.Objects.index_of(Witness.Vars.witnesses, item_id.trim(), 'fake_id');
        /*console.log(item_id.trim())
        console.log(Witness.Vars.witnesses);
        console.log(idx);*/
        
        var splice =  Witness.Vars.witnesses.splice(idx,1);  
     
        Witness.Vars.table.fnDeleteRow(row);
    }
    
    function view(idx){
        
        console.log(Witness.Vars.witnesses);
        
        Main.Mod.show_modal('viewWitnessModal',800,-100);
        Witness.Vars.currentWitness = Main.Objects.get_object(Witness.Vars.witnesses,idx,'fake_id')[0];	
        
        $('#v_witnessName').val(Witness.Vars.currentWitness.name);
        $('#v_witnessAddress').val(Witness.Vars.currentWitness.address);
        $('#v_witnessContact').val(Witness.Vars.currentWitness.contact);
        
        //listen to updating a witness
        on_witness_updated();
    }
    
    function on_witness_updated(){
        
        $('#saveWitnessButton').off('click').on('click',function(){
            Witness.Vars.currentWitness.name =  $('#v_witnessName').val();
            Witness.Vars.currentWitness.address =  $('#v_witnessAddress').val();
            Witness.Vars.currentWitness.contact =  $('#v_witnessContact').val();
            
            //update table
            Witness.Vars.table.find('tr').each(function (i, el) {
                var tr = ($(this));
                var td_text = $(this).find('td:first').text();		
                if(td_text == Witness.Vars.currentWitness.fake_id){
                    tr.empty();
                    update_table();
                    return false;
                }
            });
            
            $('#viewWitnessModal').modal('hide');    
        });        
    }
    
    function update_table(){
       Witness.Vars.table.dataTable().fnAddData([
            Witness.Vars.currentWitness.fake_id,
            Witness.Vars.currentWitness.name,
            Witness.Vars.currentWitness.address,
            Witness.Vars.currentWitness.contact,
        ]);
    }
    
    var attach = function attach(type,key,options){
        switch(type){
            case events_const.ON_SHOW_WITNESS:
                  on_show_witness();  
                break;
            case events_const.CONTEXT_ACTION:
                context_action(key,options);
                break;
            default:
                alert('Cannot attach event!');
        }
    };
    
    
    return {
        attach : attach,
        events_const : events_const,
        add_witness : add_witness,
    };
    
})();