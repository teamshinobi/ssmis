var VCRM = VCRM || {};


VCRM.Vars = (function(){
   
    var aoCases = new Array();
    var perPage = 6;
    
    var pagination = {};
    var cards = {};
    
    return {
        aoCases : aoCases,
        perPage : perPage,
        pagination : pagination,
        cards : cards,
    }
   
})();  

VCRM.ViewCaseReportsModule = (function(){
	/*===================================================================
	*						CALLBACKS and UTILS
	*===================================================================*/
	var id;
	
	var onLoad = function onLoad(){
        Main.Mod.do_ajax('casereport/ss_clear_temps/', 'POST', function(){}, null);
		init_casereports();
        
        VCRM.Listeners.addProgressSearchListener();
	}
	
	var report = function report(){
		window.location.assign(Main.Vars.host + "viewcasereports/report");
	};
	
	
	function init_casereports(){
		do_ajax(
			'viewcasereports/retrieve',
			'GET',
			casereport_callback,
			null
		);
	}
	
	/*===================================================================
	*					AJAX ON SUCCESS CALLBACKS
	*===================================================================*/
	
	var casereport_callback  = function casereport_callback(data){
        
        
        if(data.has_data == false){
            $('#retResults').text('Results Returned: ' + data.message);
            //hide
            $('#case_per_page').hide();
        } else {
            //Initialzie Cards
            VCRM.Vars.cards  = $('#cards');
            VCRM.Vars.perPage = 6;

            //set results returned
            $('#retResults').text('Results Returned: ' + data.data.length);


            $.each(data.data, function(idx, obj){
                var kaso = {};

                kaso.caseId = obj.case_id;
                kaso.caseDate = obj.arrestDate;
                kaso.caseBlotter = obj.blotter_entry_nr;

                if(obj.summary.length > 100){
                    kaso.caseSummary = obj.summary.substring(0,95) + '...';
                } else {
                  kaso.caseSummary =  obj.summary;
                }

                kaso.caseImg = obj.doc_path + obj.case_id + '_thumb.jpg';
                kaso.suspectsCount = obj.count_violators;
                kaso.caseSuspects = obj.violators;
                kaso.caseOperation = obj.nature;
                kaso.progress = obj.progress;
                
                var investigator = obj.investigator.rank + " " +
                    obj.investigator.first_name + " " + obj.investigator.last_name + " " + obj.investigator.middle_name.substring(0,1) + ".";
                
                kaso.caseInvestigator = investigator;
                VCRM.Vars.aoCases.push(kaso);

            });


            VCRM.Vars.cards.sscardify({
                'module': 'casereport',	
                'contents' : VCRM.Vars.aoCases.slice(0,6),
                'onViewClick' : function(itemId, ev){
                    Main.Mod.show_modal('loadingModal',750,'');
                    
                    var callback = function(){
                        Main.Mod.do_ajax('viewcasereports/get_access/' + itemId,'POST',function(data){
                            if(data.proceed){
                                location.assign(Main.Vars.host + 'viewcasereports/view/' + itemId);
                            }  else {
                                $('#loadingModal').modal('hide');
                                Main.Mod.show_bootbox_alert('You don\'t have access to this case.',
                                                           'Warning', null);
                            }           
                        },null);
                    }
                    
                    
                    Main.Mod.do_ajax('editcasereport/full/' + itemId,'GET',function(data){
                        var progress = data.case.case_details.progress;
                        if(progress != 'Dismissed' && progress != 'Closed'){
                            callback();
                        } else {
                            location.assign(Main.Vars.host + 'viewcasereports/view/' + itemId);
                        }
                    },null)
                            
                    
                },
                'onEditClick' : function(itemId,ev){
                    
                    Main.Mod.show_modal('loadingModal',750,'');
                    Main.Mod.do_ajax('viewcasereports/get_access/' + itemId,'POST',function(data){
                        if(data.proceed){
                           location.assign(Main.Vars.host + 'editcasereport/edit/' + itemId);
                        }  else {
                            $('#loadingModal').modal('hide');
                            Main.Mod.show_bootbox_alert('You don\'t have access to this case.',
                                                       'Warning', null);
                        }           
                    },null);
                }
            });


            //Initialize pagination
            VCRM.Vars.pagination =  $('#myPage');
            VCRM.Vars.pagination .pagination({
                items:  VCRM.Vars.aoCases.length,
                itemsOnPage:  VCRM.Vars.perPage,
                onPageClick : function(pageNumber, event){		
                    VCRM.Events.reInitCards(pageNumber);	
                }
            });
        
        }
        
        $(".scroll").mCustomScrollbar();
     
	}

   
	function do_ajax(url,type,callback_function,formdata){
		var params = {
			type: type,
			url: Main.Vars.host + url,
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
				callback_function(data);
			}
		}
		if(formdata != null){
			params.data = formdata;
		}
		$.ajax(params);
	}
	
	return{
		onLoad : onLoad,
		report : report,
		
	}
})();


VCRM.Events = (function(){

    var onSelectChange = function onSelectChange(perPage){
        switch(perPage){
            case 0:
                VCRM.Vars.perPage = 6;
                reInitCards(1);
                reInitPagination();
                break;
            case 1:
                VCRM.Vars.perPage = 12;
                reInitCards(1);
                reInitPagination();
                break;
                
            case 2:
                VCRM.Vars.perPage = 18;
                break;
            default:
                console.log('Error in handling select change');
        }
       
    } 
    
    function reInitCards(pageNumber){
        VCRM.Vars.cards.sscardify('destroy');
       
        if(pageNumber == 1){
            pageNumber = 0;
            last_page = pageNumber + VCRM.Vars.perPage;
            VCRM.Vars.cards.sscardify('createCards', VCRM.Vars.aoCases.slice(pageNumber,last_page));
        } else {
            var last = parseInt(pageNumber) * VCRM.Vars.perPage;
            var first = last - VCRM.Vars.perPage;
           
            VCRM.Vars.cards.sscardify('createCards', VCRM.Vars.aoCases.slice(first,last));
        }
        $('.scroll').mCustomScrollbar(); 
    }
    
    function reInitPagination(){
        //destroys pagination then reinitialize base on current options
        VCRM.Vars.pagination.pagination('destroy');
        VCRM.Vars.pagination .pagination({
			items:  VCRM.Vars.aoCases.length,
			itemsOnPage:  VCRM.Vars.perPage,
			onPageClick : function(pageNumber, event){		
				reInitCards(pageNumber);	
			}
		});
    }
    return {
        onSelectChange : onSelectChange,
        reInitCards : reInitCards,
    }

})();

VCRM.Listeners = (function(){
    
    
    var addProgressSearchListener = function (){
        
        $('#inquestButton').on('click',function(){ 
            location.assign(Main.Vars.host + 'search/?k=Inquest');    
        });
        
        $('#courtButton').on('click',function(){ 
            location.assign(Main.Vars.host + 'search/?k=Court');    
        });
        
        $('#closedButton').on('click',function(){ 
            location.assign(Main.Vars.host + 'search/?k=Closed');    
        });
        
        $('#dismissedButton').on('click',function(){ 
            location.assign(Main.Vars.host + 'search/?k=Dismissed');    
        });
    } 
    
    return {
        addProgressSearchListener : addProgressSearchListener,
    }
})();