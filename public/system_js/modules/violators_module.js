var VM = VM || {};

VM.Main = (function(){
	var violators = [];
	var selected_violator = null;
	var violators_table;
	
	modal_const = {
		'UPDATE_VIOLATOR' : 1,
		'SEARCH' : 2,
		'PRINT' : 3,
	}
	
	/*===================================================================
	*						CALLBACKS and UTILS
	*===================================================================*/
	var onLoad = function onLoad(){
		//initialization here
		init_violators();
		//init datepickers
		var datepickers = ['#end_date','#start_date'];
		Main.Init.InitDatePicker(datepickers);
	}
	var on_modal_showing = function on_modal_showing(type,size,margin){
		var src = "";	
		
		switch(type){
			case modal_const.SEARCH:
				Main.Mod.show_modal('advancedSearchModal',size,margin);
				VM.Events.attach(VM.Events.events_const.ON_DATE_SEARCHED);
				VM.Events.attach(VM.Events.events_const.ON_NUMCASES_SEARCHED);
				break;
			case modal_const.PRINT:
				Main.Mod.show_modal('printModal',size,margin);
				VM.Events.attach(VM.Events.events_const.ON_PRINT_ALL);
				VM.Events.attach(VM.Events.events_const.ON_PRINT_SPECIFIC);
				break;
			default:
				console.log('Cannot show modal!');
		}
	}
	
	function on_modal_closing(source){
	
	}
	function reset(){
		
	}
	/*===================================================================
	*					Initialization Methods Here
	*===================================================================*/
	function init_violators(){
        on_successful_violators_retrieval();
	}
	/*===================================================================
	*					AJAX ON SUCCESS CALLBACKS
	*===================================================================*/
	var on_successful_violators_retrieval  = function on_successful_violators_retrieval(data){
	
		var violators_table = $('#violators_tbl');
		$(violators_table).DataTable({
		//so that all rows are not rendered on init time.
		"deferRender": true,
		"autoWidth": false,
		//for reinitialisation purpose
		destroy: true,
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		//NOTE: We are directly providing group id since we want to get said-sotg members only
		"ajax" : {
				url :  Main.Vars.host + "violators/getall",
				type : "GET",
                failure: function (result) {
                   alert( 'There are no violators recorded yet.' );
                }
			},
		"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 4 ] },
					{ "sWidth": "20%", "aTargets": [ 0 ] },
					{ "sWidth": "35%", "aTargets": [ 1 ] },
					{ "sWidth": "15%", "aTargets": [ 2 ] },
					{ "sWidth": "22%", "aTargets": [ 3 ] },
					
			],
			"columns": [
				{ 
					"data": "violator_id",
				},	
				{ 	
					"render": function(data,type,row,meta) {

						return row.first_name + " " + row.last_name + " " + row.middle_name.substring(0,1) + ".";
					}
				
				},
				{ 
					"data": "alias",
					

				},
				{ 
					"render": function(data,type,row,meta) {
						var oCase = row.last_case;
                        var lCase = '<a href="' + Main.Vars.host +   
                                    'viewcasereports/view/' + oCase.case_id + 
                                    '">' + oCase.case_id + '</a>';
                        return lCase;
						
					}
				},
				
				{ 
					
					"render": function(data,type,row,meta) {
						var str_append = 'thumbs/' + row.violator_id + '_front.jpg';
						var img1 = '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
						str_append = 'thumbs/' + row.violator_id + '_rear.jpg';
						var img2 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
						str_append = 'thumbs/' + row.violator_id + '_right.jpg';
						var img3 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
						str_append = 'thumbs/' + row.violator_id + '_left.jpg';
						var img4 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';		
						
						return img1 + img2 + img3 + img4;
					}
				},		
			],	
			sPaginationType: "full_numbers",
           
		});	
		VM.Events.attach(VM.Events.events_const.ON_VIOLATOR_SELECTED);
	}
	
	var on_success_cases_involved = function on_success_cases_involved(data){
		
	}
	/*===================================================================
	*						Miscellaneous Here
	*===================================================================*/
  
	
	return {
      
		onLoad : onLoad,
		on_modal_showing : on_modal_showing,
		modal_const : modal_const,
	}
})();

VM.Events = (function(){
	var events_const = {
		'ON_VIOLATOR_SELECTED' : 1,
		'ON_VIOLATOR_UPDATED' : 2,
		'ON_VIOLATOR_DELETED' : 3,
		'ON_MUGSHOT_CLICKED' : 4,
		'ON_DATE_SEARCHED' : 5,
		'ON_NUMCASES_SEARCHED' : 6,
		'ON_PRINT_ALL' : 7,
		'ON_PRINT_SPECIFIC' : 8,
	}
	
	function on_print_all(){
		$('#printAllButton').on('click',function(){
			var sect = $('#inputSection');
		});
	}
	function on_print_specific(){
		$('#printSpecificButton').on('click',function(){
			var sect = $('#inputSection');
			sect.empty();
			var str = '<div class="col-md-3 col-lg-3"> ' +
						'<label> Violator ID </label>' +
					'</div>' +
					'<div class="col-md-7 col-lg-7">' +
						'<input class="form-control" type="text" id="violator_id_s"/> ' +
					'</div>' +
					'<div class="col-md-2 col-lg-2">' +
						'<button id="goPrintSpecificButton" class="btn btn-default" type="button"> GO </button>' +
					'</div>' 
			sect.hide().append(str).fadeIn(1000);
		});
	}
	function on_date_searched(){
		$('#searchByDate').on('click',function(){
			if($("input[name='start_date']").val()==="" || $("input[name='end_date']").val() ===""){
				alert('Please specify both start and end dates.');
			}else{	
				var sdate = new Date($("input[name='start_date']").val());
				var edate = new Date( $("input[name='end_date']").val());
				//compare if sdate is earlier that edate.
				if(sdate > edate){
					alert('Start date must be earlier than end date');
				}else{
					$('#searchDateLoading').show();
					Main.Mod.do_ajax('violators/datesearch/' + $("input[name='start_date']").val()  + '/' +  $("input[name='end_date']").val() + '/', 'GET', on_successful_date_searched, null);		
				}			
			}
			
		});
	}
    
	
	function on_numcases_searched(){
		$('#searchByNumberCases').on('click',function(){
			if(patternMatched($('#numCases').val())){			
				Main.Mod.do_ajax('violators/casessearch/' + $('#numCases').val(), 'GET', on_successful_numcases_search, null);					
			} else { 
				 alert('Pattern not matched');
			}
		});
	}
	function on_violator_selected(){
		//handle the redirection for viewing of police
		$('#violators_tbl tbody').on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
			window.location.assign(Main.Vars.host + "violators/view/" + id);
		});	
	}
	function on_violator_updated(){
		
	}
	
	
	/*=============================================================================
	*								AJAX CALLBACKS
	*=============================================================================*/
	var on_successful_numcases_search = function on_successful_numcases_search(data){
		var results = $('#search-results');
		var num_res = "";
		results.empty();
		$('#results-found').remove();
		
		if(!data.success){
			num_res += "No results found.";
		} else { 
			paginate_1(data.data,data.size);
			append1(data.data.slice(0,5));
			num_res = data.size + ' results found.';
		}
	
		$(results).after('<div id="results-found" class="row"><div class="col-md-12 col-lg-12">' +
				'<p class="text-muted">' + num_res + '</p>' +
				'</div></div>');
		
	}
	var on_successful_date_searched = function on_successful_date_searched(data){
		$('#searchDateLoading').hide();
		var results = $('#search-results');
		var num_res = "";
		results.empty();
		$('#results-found').remove();
		if(!data.success){
			num_res += "No results found.";
		} else {
			
			$.each(data.data, function(idx,obj){
				var str = '<li class="result"><p><a class="result-header">'+ 
							obj.case_report.case_id + 
							'</a><span class="case-date">' + obj.date_of_arrest + '</span>' + 
							'<p>Violators involved: </p>' +
							'<ul>' ;
			
				$.each(obj.violators,function(idx,v){
					
					str += '<li><img class="violator-img img-responsive img-thumbnail" src="' + Main.Vars.host + v.details[0].file_path + '/thumbs/' + v.violator_id + '_front.jpg ">' +
					'   ' + '<a class="violator" target="_blank" href="violators/view/' + v.violator_id +
						'">' + v.details[0].first_name + ' ' + v.details[0].last_name +
					'</a></li>';
				});			
								
				str += '</ul></a></p><li><hr/>';
				
				results.append(str);
			});
		
			num_res += data.data.length + " results found.";
		}
		$(results).after('<div id="results-found" class="row"><div class="col-md-12 col-lg-12">' +
					'<p class="text-muted">' + num_res + '</p>' +
					'</div></div>');
	}
	
	/*=============================================================================
	*								Miscellaneous
	*=============================================================================*/
	//append results for number of cases search
	function append1(data){
		var results = $('#search-results');
		results.empty();
		$.each(data,function(idx,obj){
				var str = '<li class="result">' +
					'<p>' +
						'<img style="float: left;" class="violator-img img-responsive img-thumbnail" src="'  + Main.Vars.host + obj.info.file_path + '/thumbs/' + obj.violator_id + '_front.jpg' + '"/>' +
						'<label class="result-header">' + obj.info.first_name + ' ' + obj.info.last_name + '</label><br/>' +
						'<a target="_blank" href="violators/view/' + obj.violator_id + '" >' + obj.violator_id +'</a>' +
					'</p>' +
					'<div>' + 
						'<p>Cases Involved: </p>' + 
						'<ul>';
						
						$.each(obj.blotters,function(idx,obj){
							str += '<li>' +
								'<p><a target="_blank" href="casereports/view/' + obj.case_id + '" >' + obj.case_id + '</a> happened on ' + obj.date_of_arrest + ' with blotter entry number of ' + obj.blotter_entry_nr +
								' </p></li>';
						});
		
				str += '</ul></div><hr/></li>';
		
				results.append(str);
		});
	}
	//paginate search results for number of cases search;
	function paginate_1(data,size){
		var pagination = $('#pager');
		var pages = Math.ceil(size/5);
			
		if(pages > 1){
			var str = "";
		
			str += '<li><a aria-label="Previous">' +
						'<span aria-hidden="true"> &laquo; </span>' +
				'</a></li>';
				
			for(var i = 0; i < pages; i++){
				if(i == 0){
					str += '<li data-value="' + 0 + '"><a>'+ (i+1) + '</a></li>';
				}else{
					str += '<li data-value="' + (i+4) + '"><a>'+ (i+1) + '</a></li>';
				}
				
			}
			
			str += '<li><a aria-label="Previous">' +
						'<span aria-hidden="true"> &raquo; </span>' +
				'</a></li>';
		
			pagination.append(str);
			
			//add event handling in pagination
			$('.pagination li').on('click',function(){
				var pageIndex = $(this).attr('data-value');
				append1(data.slice(pageIndex, pageIndex + 5));
			});
		}else{
			pagination.remove();
		}
	}
	//tells whether the range input is correct
	function patternMatched(input){
		var splitted = input.trim().split('-');
		
		if(splitted.length === 2){
			if(splitted[0].trim() > splitted[1].trim()){
				return false;
			}
			return true;
		}
		
		return false;
	}
	var attach = function(source){
		switch(source){
			case events_const.ON_VIOLATOR_SELECTED:
				on_violator_selected();
				break;
			case events_const.ON_MUGSHOT_CLICKED:
				on_mugshot_clicked();
				break;
			case events_const.ON_DATE_SEARCHED:
				on_date_searched();
				break;
			case events_const.ON_NUMCASES_SEARCHED: 
				on_numcases_searched();
				break;
			case events_const.ON_PRINT_ALL:
				on_print_all();
				break;
			case events_const.ON_PRINT_SPECIFIC: 
				on_print_specific();
				break;
			default:
				alert('Cannot attach events');
		}
	}
	return {
		attach : attach,
		events_const : events_const,
	}
})();