var Acct = Acct || {};
Acct.Vars = (function(){
    
    var pnp_id = '';
    
    return {
        pnp_id : pnp_id,
    }

})();  
    
Acct.Main = (function(){
    var isAdmin;
    
    var onLoad = function onLoad(){
        
        Acct.Events.attach(Acct.Events.events_const.ON_PASSWORD_CHANGED);
        Acct.Events.attach(Acct.Events.events_const.ON_INFO_UPDATE);
        
        
        checkAccount();
    }   
    
    function checkAccount(){

        Main.Mod.do_ajax("home/get_user_pnp_id", "GET", function(data){
        
            isAdmin = data.is_admin;
          
            if(isAdmin){
                
                $('#message').text("You can change your username and other police username by changing batch id.");
            
            }
            Acct.Vars.pnp_id = data.username; 
            loadProfileInfo(Acct.Vars.pnp_id);
            
        },null);
        
    }
    
    function loadProfileInfo(id){
         Main.Mod.do_ajax("pnp/get/" + id, "GET", function(data){
            $('#first_name').val(data.data.first_name);
            $('#middle_name').val(data.data.middle_name);
            $('#last_name').val(data.data.last_name);
            $('#address').val(data.data.address);
            $('#contact_number').val(data.data.contact_number);
             
             //setting fucking gender
            var $radios = $('input:radio[name=gender]');
            if(data.data == "1"){
                $radios.filter('[value=1]').prop('checked', true);
            }else{
                $radios.filter('[value=0]').prop('checked', true);
            }
             
         },null);
    }
    
    return {
        onLoad : onLoad,
    }
   
})();


Acct.Events = (function(){
    
    var events_const = {
        'ON_PASSWORD_CHANGED' : 1,
        'ON_INFO_UPDATE' : 2,
    }
    
    function on_info_update(){
        profile_info_form
        $("#profile_info_form").on('submit',(function(e){
            e.preventDefault();
            Main.Mod.show_modal('loadingModal',650,'');
            var formdata = new FormData(this);
            formdata.append('pnp_id', Acct.Vars.pnp_id);
            formdata.append('prev_id', Acct.Vars.pnp_id);
            
            Main.Mod.do_ajax('account/update', 'POST',function(data){
                $('#loadingModal').modal('hide');
                
                if(data.success){
                    Main.Mod.show_bootbox_alert(data.message, "Success!",null);
                } else {
                    Main.Mod.show_bootbox_alert(data.message, "Error!",null);
                    console.log(data.errors);
                }
            },formdata);
            
        }));
    }
    function on_password_changed(){
        $('#changePassButton').on('click',function(){
            if($('#changePass').val().trim().length < 8) {
                Main.Mod.show_bootbox_alert('Password must be atleast 8 characters.', 'Message',null);
            } else {
                var formdata = new FormData();
                formdata.append('password', $('#changePass').val());

                Main.Mod.do_ajax('account/change_pass', 'POST',function(data){
                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message, 'Success!', function(){
                            window.location.reload();
                        });
                    } else {
                        Main.Mod.show_bootbox_alert(data.message, 'Failed!', function(){
                            window.location.reload();
                        });
                    }

                },formdata);
            }
            
        });  
    }
    
    var attach = function attach(type){
        switch(type){
            case events_const.ON_PASSWORD_CHANGED:
                on_password_changed();
                break;
            case events_const.ON_INFO_UPDATE:
                on_info_update();
                break;
            default:
                alert('Cannot attach event.');
        
        }
        
    }
    
    return {
        attach: attach,
        events_const: events_const,
    }
    
    
})();