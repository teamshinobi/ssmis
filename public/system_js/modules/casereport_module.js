
var CSR = CSR || {};

CSR.Vars = (function() {
    //attachments
    var spot_report_id = 'NONE';
    var pre_op_id = 'NONE';
    var coordination_id = 'NONE';
    var main_id = 'NONE';
    var supp_id = 'NONE';
    
    //used for editing. Para malaman ko kung nagbago ba ung attacment or watsoever
    var spot_report_id_e = 'NONE';
    var coordination_id_e = 'NONE';
    var pre_op_id_e = 'NONE';
    var main_id_e = 'NONE';
    var supp_id_e = 'NONE';
    
    var prev_blotter_entry = '';
    
    var submission_url = 'casereport/submit';
    
    var barangay_id = '';
    //for editing purposes
    var prev_brgy_id = '';
    //
    
    var user_mod = 'casereport';
    
    var is_editing = false;
    
   
    return {
        spot_report_id : spot_report_id,
        pre_op_id : pre_op_id,
        coordination_id : coordination_id,
        spot_report_id_e : spot_report_id_e,
        pre_op_id_e : pre_op_id_e,
        coordination_id_e : coordination_id_e,
        main_id : main_id,
        main_id_e : main_id_e,
        supp_id : supp_id,
        supp_id_e : supp_id_e,
        
        submission_url : submission_url,
        
        barangay_id : barangay_id,
        prev_brgy_id : prev_brgy_id,
        prev_blotter_entry : prev_blotter_entry,
        user_mod : user_mod,
        
        is_editing : is_editing,
        
    }
})();
CSR.SubmitForm = (function(){
	var results = new Array();
    var doneChecking = false;
	var submit = function submit(formdata){
        var callback_func = function callback_func(){
            
            $('#loadingModal').modal(Main.Vars.modal_options)
			$('#loadingModal .modal-content').css('width', '700');
			$('#loadingModal .modal-content').css('margin-left', '-50');
            
			formdata.append('case_id', $('#case-id').text());
            formdata.append('barangay_id', CSR.Vars.barangay_id);
           
			formdata.append('arresting_officers_id', CSR.ArrestDetails.arresting_officers_ids);
			formdata.append('investigator_id', CSR.ArrestDetails.investigator_id);
            formdata.append('finger_taken_by', CSR.ArrestDetails.finger_taken_by);
            formdata.append('photo_taken_by', CSR.ArrestDetails.photo_taken_by);
            formdata.append('summary', $('.summernote').code());
			formdata.append('violators', JSON.stringify(CVM.Vars.violators));
            formdata.append('witnesses', JSON.stringify(Witness.Vars.witnesses));
			formdata.append('items', JSON.stringify(IM.Vars.items));
			formdata.append('nitems', JSON.stringify(IM.Vars.nitems));	
            formdata.append('seizing_officers', JSON.stringify(IM.Vars.aoSelected_officers));
            formdata.append('spot_report_id', CSR.Vars.spot_report_id);
            formdata.append('pre_op_id', CSR.Vars.pre_op_id);
            formdata.append('coordination_id', CSR.Vars.coordination_id);
            formdata.append('main_id', CSR.Vars.main_id);
            formdata.append('supp_id', CSR.Vars.supp_id);
            formdata.append('prev_blotter', CSR.Vars.supp_id);
            //checks prev_id if in case we are using edit casereport module
            if(CSR.Vars.prev_brgy_id != ''){
                formdata.append('prev_brgy_id',CSR.Vars.prev_brgy_id);
            }
            
            formdata.append('prev_blotter',CSR.Vars.prev_blotter_entry);
        
            formdata.append('items_rem', JSON.stringify(IM.Vars.items_rem));
            formdata.append('nitems_rem', JSON.stringify(IM.Vars.nitems_rem));
            
            formdata.append('spot_report_id_e', CSR.Vars.spot_report_id_e);
            formdata.append('pre_op_id_e', CSR.Vars.pre_op_id_e);
            formdata.append('coordination_id_e', CSR.Vars.coordination_id_e);
            formdata.append('main_id_e', CSR.Vars.main_id_e);
            formdata.append('supp_id_e', CSR.Vars.supp_id_e);
            
			$.ajax({
				type: 'POST',
				url: Main.Vars.host + CSR.Vars.submission_url,
				contentType: false,
				cache: false,   
				processData:false, 
				data: formdata,
				dataType: 'json',
				success: function(data){
					if(data.error == true){
                        $('#loadingModal').modal('hide');
						Main.Mod.show_bootbox_alert('There were errors in saving the record. \nError Message: ' + data.db_err.error_message, 'Error', null);					  
                    } else {
						//refresh page here
                        $('#loadingModal').modal('hide');
                        var audit_description
                        var audit_operation 
                        
                        if(!CSR.Vars.is_editing){
                            audit_description = 'booked a case report with id of ' + $('#case-id').text();
                            audit_operation = 'create case report';
                        } else {
                            audit_description = 'edited a case report with id of ' + $('#case-id').text();
                            audit_operation = 'edit case report';
                        }
                       
                        
                        Main.Audit.save_audit(audit_description, audit_operation);
						Main.Mod.show_bootbox_alert('Successfully saved!', 'Message', null);	
					}
				}
			});
			
		};
		
		Main.Mod.show_bootbox_confirm('Are you sure you want to submit the form?', 'Message', callback_func);				
		
	};
	
	//isang malupit na validation logic. Wahaha!
	var validate_casereport= function validate_casereport(){
        validate_others();
	};
    
    function validate_others(){
        if($("input[name='blotter_entry_nr']").val() == ''){
			results.push("Blotter Entry cannot be empty.");
		}
		if($("input[name='time_of_arrest']").val() == ''){
			results.push("Time of arrest cannot be empty.");
		}
		if($("input[name='date_of_arrest']").val() == ''){
			results.push("Date of arrest cannot be empty.");
		}
		if($("input[name='street_name']").val() == ''){
			results.push("Street cannot be empty.");
		}
		if($("input[name='street_name']").val() == ''){
			results.push("City cannot be empty.");
		}
		if(CSR.ArrestDetails.investigator_id.length == 0){
			results.push("The case needs an investigator.");
		}
		if(CSR.ArrestDetails.arresting_officers_ids.length == 0){
			results.push("The case needs atleast 1 arresting officer.");
		}
		if(Witness.Vars.witnesses.length < 1){
			results.push("The case needs at least 1 witness.");
		}
		if(CVM.Vars.violators.length == 0){
			results.push("The case needs atleast 1 violator");
		}
        
		if(IM.Vars.aoSelected_officers.length < 1){
			results.push("The case needs at least 1 seizing officer.");
		}
        
        if(IM.Vars.items.length < 1 && IM.Vars.nitems.length < 1){
			results.push("There must be evidence seized.");
		}
    
    }
	
	//validations parameters comes from the results of validate_casereport. Actually it is the results array in this module. Haha!
	var show_validation_errors = function show_validation_errors(validations){
		
		var message = "<div class=\"row\"><div class=\"col-lg-12\"><ul>";
		
		$.each(validations, function(index, item) {
			message += "<li>" + item + "</li>";
		});
						
		message+= "</ul></div></div>";		
		
		Main.Mod.show_bootbox_alert('Please complete the form before submitting.\n' + message,'Validation Results', null);
	}
	
	
	return {		
		submit : submit,	
		validate_casereport : validate_casereport,
		results : results,
		show_validation_errors : show_validation_errors,
	};
	
})();

//Functionalities needed by Arrest Details Section
CSR.ArrestDetails = (function(){
	
	//Investigator's Batch ID for saving. Can be retrieved by calling the module concatenated with '.investigator_id'
	var investigator_id = new Array();
	//Arresting officers' Batch ID for saving. Can be retrieved by called the module concatenated with '.arresting_officers_ids'
	var arresting_officers_ids = new Array();    
    
	var temp_arresting_officers_ids = new Array();
	
	var arresting_officers_names = new Array();
	var aoArrestingOfficers = new Array();
    
    var photo_taken_by = new Array();
    var finger_taken_by = new Array();
    
    var barangay_id = "";
    
	/*========================================================
	*
	*					RETRIEVING
	*
	*=========================================================*/
	var investigators_table = undefined;
	var arresting_officers_table = undefined;
	
	//what = what data will be ajaxed
	var retrieve = function retrieve(what){
		if(what == 'barangay'){
			retrieve_barangay();
		}else if(what=='investigator'){
			retrieve_possible_investigators();
		} else if(what == 'fingerman') {
            retrieve_fingerman();
        } else if(what == 'photographer'){
            retrieve_photographer();
        }else{
			retrieve_possible_arresting_officers();
		}	
	};
	//case scene
    var onLoad = function onLoad(){
        Main.Mod.do_ajax('casereport/ss_clear_temps/', 'POST', function(){}, null);
        on_scene_changed();
        //barangay
        selectBarangayButtonClicked();
        delegateBarangaySelection();
        
        
        $(window).bind("beforeunload",function(event) {
            return "You might have inputted information that needs to be saved.";
        });
    }
    
    function delegateBarangaySelection(){
        $('#barangay_table').delegate('button','click',function(){
            var brgy_id = $(this).attr('data-id');
            var barangay_name = $(this).attr('data-name');
            var city = $(this).attr('data-city');
            
            CSR.Vars.barangay_id = brgy_id,
            
            $('#city').val(city);
            $('#barangay').val(barangay_name);
            
            $('#barangaysModal').modal('hide');
        });
    } 
    
    function selectBarangayButtonClicked(){
        $('#selectBarangayButton').on('click',function(){
            showBarangays();
        });
    }
    
    function showBarangays(){
        Main.Mod.show_modal('barangaysModal', 1200,-300);
        $('#barangay_table').DataTable({
			"autoWidth": false,
            destroy: true,
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"ajax" : {
				url :  Main.Vars.host + "barangay/retrieve",
				type : "GET",
			},
            "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4 ] },
					{ "bOrderable": false, "aTargets": [ 4] },
					{ "sWidth": "20%", "aTargets": [ 0 ] },
					{ "sWidth": "40%", "aTargets": [ 1 ] },
					{ "sWidth": "40%", "aTargets": [ 2 ] },
					{ "sWidth": "5%", "aTargets": [ 3 ] },
					
			],
			"columns": [
                { 
					"data": "barangay_id",
				},
                { 
					"data": "barangay_name",
				},
                { 
					"data": "city",
				},
                { 
					"data": "barangay_incidents",
				},
                {
                    "render": function(data,type,row,meta) {
                        
                        var button = '<button data-id="' + row.barangay_id  + '" class="btn btn-sm btn-default"' +
                            
                            ' data-name="' + row.barangay_name + '"' +  
                            ' data-city="' + row.city + '"' +  
                            ' style="margin-left:15px;"> <i class="fa fa-plus"></i>'
                        
                           button += '</button>';
                        
				        return button;
                    }
                
                }
						
			],
				
		});
    }
    
    function create_document_folders(){
        //move default image in case temp folder
        var formdata = new FormData();
        formdata.append('case_id',$('#case-id').text());
        Main.Mod.do_ajax('casereport/documentfolders/', 'POST', function(){}, formdata);
    }
    
    function on_scene_changed(){
        $("#casefile").change(function() {        
            if(Main.Vars.imageRead != false){
                var formdata = new FormData();

                formdata.append('case_id', $('#case-id').text());
                formdata.append('case', $('#casefile')[0].files[0]);

                Main.Mod.do_ajax('casereport/uploadscene', 'POST', function(){}, formdata);

                //return to original value, Important.
                Main.Vars.imageRead = false;
            }
        });
    }
    //set the value of arresting officers text field
	var get_selected_arresting_officers = function get_selected_arresting_officers(){

		if(aoArrestingOfficers.length > 0){
			var strings = '';
			$.each(aoArrestingOfficers, function(index, item) {
				strings += item.name + ', ';
			});
			//empty the textfield
			$('#addArrestingOfficersTextField').text('');
			$('#addArrestingOfficersTextField').text(strings);
					
			$('#arresttingModal').modal('hide');
		}else{
			Main.Mod.show_bootbox_alert('Please select 1 or more police.', 'Message', null);			
		}	
        
      
        
	};
    
    
	function retrieve_possible_arresting_officers(){
		var arresting_officers_table = $('#arresting_officers_table');
		arresting_officers_table.DataTable({
		"deferRender": true,
		"autoWidth": false,
		//for reinitialisation purpose
		destroy: true,
		"aLengthMenu": [[5, 25, 50, 100, -1], [5, 25, 50, 100, "All"]],
		"ajax" : {
				url :  Main.Vars.host + "casereport/activepnps",
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 0 , 4] },
					{ "bOrderable": false, "aTargets": [ 0 , 4] },
					{ "sWidth": "10%", "aTargets": [ 0 ] },
					{ "sWidth": "20%", "aTargets": [ 1 ] },
					{ "sWidth": "30%", "aTargets": [ 2 ] },
					{ "sWidth": "30%", "aTargets": [ 3 ] },
					{ "sWidth": "10%", "aTargets": [ 4 ] },
			],
			"columns": [
				{
					"render": function(data,type,row,meta) {

                        var i = Main.Objects.index_of(aoArrestingOfficers,row.pnp_id,'pnp_id');
                        var str = '';
                        
                        if(i > -1) {
                            str = '<input data-name=\"' + row.first_name + ' ' + row.last_name + '\" data-value=\"' + row.pnp_id +'\"style=\"padding-left: 0px; margin-left: 10px\" type=\"checkbox\" checked/>'
                        } else {
                            str = '<input data-name=\"' + row.first_name + ' ' + row.last_name + '\" data-value=\"' + row.pnp_id +'\"style=\"padding-left: 0px; margin-left: 10px\" type=\"checkbox\"/>';
                        }
						return str;
					}
				},
				{ 
					"data": "pnp_id",
					
				},	
				{
					"data": "rank",	
				
				},
				{ 	
					"render": function(data,type,row,meta) {

						return row.first_name + " " + row.last_name;
					}
				
				},
				{ 
					
					"render": function(data,type,row,meta) {
				        return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';
                    }
				},
			],
			
			sPaginationType: "full_numbers",
		});
		
		//handle event for checkbox checking.
		arresting_officers_table.off("change").on("change", ":checkbox", function() { 
			if($(this).is(':checked')){
				//alert('checked! ' + $(this).attr('data-value'));
                var id = $(this).attr('data-value');
                var name = $(this).attr('data-name');
                var ao = {
                    'pnp_id' : id,  
                    'name' : name,
                }
              
                arresting_officers_ids.push(ao.pnp_id);
                aoArrestingOfficers.push(ao);
                console.log(arresting_officers_ids);
			}else{
				//remove item
				var idx = arresting_officers_ids.indexOf($(this).attr('data-value').trim());
				arresting_officers_ids.splice(idx, 1);
				aoArrestingOfficers.splice(idx, 1);
                console.log(arresting_officers_ids);
			}
		});
    
	}
	
	function retrieve_possible_investigators(){
		investigators_table = $('#members_table');
		investigators_table.DataTable({
		//so that all rows are not rendered on init time.
		"deferRender": true,
		"autoWidth": false,
		//for reinitialisation purpose
		destroy: true,
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		//NOTE: We are directly providing group id since we want to get said-sotg members only
		"ajax" : {
				url :  Main.Vars.host + "casereport/activeinvestigators/",
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 3,4 ] },
					{ "sWidth": "25%", "aTargets": [ 0 ] },
					{ "sWidth": "25%", "aTargets": [ 1 ] },
					{ "sWidth": "35%", "aTargets": [ 2 ] },
					{ "sWidth": "10%", "aTargets": [ 3 ] },
					{ "sWidth": "5%", "aTargets": [ 3 ] },
			],
			"columns": [
				{ 
					"data": "pnp_id",
					

				},	
				{
					"data": "rank",	
				
				},
				{ 	
					"render": function(data,type,row,meta) {

						return row.first_name + " " + row.last_name;
					}
				
				},
				{ 
					
					"render": function(data,type,row,meta) {
					   return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';
					}
				},
				{
					data: null,
					className: "center",
					"render": function(data,type,row,meta) {
						//console.log(base_path + row.file_path_thumb);
						return  '<button data-value="' +  row.pnp_id +'" style="img style="padding-left: 0px; padding-right:0px;" class="btn btn-primary btn-md action"><span class="glyphicon glyphicon-plus"></span></button>';

					}
					
				
				}	
			],
			
			sPaginationType: "full_numbers",

		});
		//add click event to table when choosing an investigator
		$('#members_table tbody').off().on( 'click', '.action', function () {
			var tr = $(this).closest('tr');
			var selected_name = undefined;
			var selected_id = $(this).attr('data-value');
			var idx = 0;
			//get the name
			tr.find('td').each (function() {
				if(idx == 2){
					selected_name = $(this).text();
				}
				idx++;
			});
			idx = 0;
			
			var callback_func = function callback_func(){	
				$('#investigatorsModal').modal('hide');			
				bootbox.alert({ 
					message: selected_name + " has been successfully set as the investigator!",
					title: "Message",
					callback: function(){}			
				});
				investigator_id.length = 0;
				investigator_id.push(selected_id);
        
				//selected name would not be sent on post since we would not give the input text a name.
				$('#investigator').val(selected_name);	
			};
			Main.Mod.show_bootbox_confirm('Are you sure you want to set this police as case investigator?', 'Confirmation', callback_func);
		});	
	}
	
    function retrieve_fingerman(){
        $('#fingerman_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "casereport/activepnps",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "25%", "aTargets": [ 0 ] },
                        { "sWidth": "25%", "aTargets": [ 1 ] },
                        { "sWidth": "35%", "aTargets": [ 2 ] },
                        { "sWidth": "10%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 3 ] },
                ],
                "columns": [
                    { 
                        "data": "pnp_id",


                    },	
                    {
                        "data": "rank",	

                    },
                    { 	
                        "render": function(data,type,row,meta) {

                            return row.first_name + " " + row.last_name;
                        }

                    },
                    { 

                        "render": function(data,type,row,meta) {
                           return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';
                        }
                    },
                    {
                        data: null,
                        className: "center",
                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return  '<button data-value="' +  row.pnp_id +'" style="img style="padding-left: 0px; padding-right:0px;" class="btn btn-primary btn-md action"><span class="glyphicon glyphicon-plus"></span></button>';

                        }


                    }	
                ],

                sPaginationType: "full_numbers",

		});
        //add click event to table when choosing an fingerer
		$('#fingerman_tbl tbody').off().on( 'click', '.action', function () {
			var tr = $(this).closest('tr');
			var selected_name = undefined;
			var selected_id = $(this).attr('data-value');
			var idx = 0;
			//get the name
			tr.find('td').each (function() {
				if(idx == 2){
					selected_name = $(this).text();
				}
				idx++;
			});
			idx = 0;
			
          
			var callback_func = function callback_func(){	
				$('#fingermanModal').modal('hide');			
				bootbox.alert({ 
					message: selected_name + " has been successfully set as the fingerprint taker!",
					title: "Message",
					callback: function(){}			
				});
				finger_taken_by.length = 0;
				finger_taken_by.push(selected_id);
        
				//selected name would not be sent on post since we would not give the input text a name.
				$('#fing_taken_by').val(selected_name);	
                
                console.log(finger_taken_by);
			};
			Main.Mod.show_bootbox_confirm('Continue?', 'Confirmation', callback_func);
        });	
    }
    
    function retrieve_photographer(){
        $('#photographer_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "casereport/activepnps",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "25%", "aTargets": [ 0 ] },
                        { "sWidth": "25%", "aTargets": [ 1 ] },
                        { "sWidth": "35%", "aTargets": [ 2 ] },
                        { "sWidth": "10%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 3 ] },
                ],
                "columns": [
                    { 
                        "data": "pnp_id",


                    },	
                    {
                        "data": "rank",	

                    },
                    { 	
                        "render": function(data,type,row,meta) {

                            return row.first_name + " " + row.last_name;
                        }

                    },
                    { 

                        "render": function(data,type,row,meta) {
                           return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';
                        }
                    },
                    {
                        data: null,
                        className: "center",
                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return  '<button data-value="' +  row.pnp_id +'" style="img style="padding-left: 0px; padding-right:0px;" class="btn btn-primary btn-md action"><span class="glyphicon glyphicon-plus"></span></button>';

                        }


                    }	
                ],

                sPaginationType: "full_numbers",
		});
        
        //add click event to table when choosing an photographer
		$('#photographer_tbl tbody').off().on( 'click', '.action', function () {
			var tr = $(this).closest('tr');
			var selected_name = undefined;
			var selected_id = $(this).attr('data-value');
			var idx = 0;
			//get the name
			tr.find('td').each (function() {
				if(idx == 2){
					selected_name = $(this).text();
				}
				idx++;
			});
			idx = 0;
			
          
			var callback_func = function callback_func(){	
				$('#photographerModal').modal('hide');			
				bootbox.alert({ 
					message: selected_name + " has been successfully set as the photographer!",
					title: "Message",
					callback: function(){}			
				});
				photo_taken_by.length = 0;
				photo_taken_by.push(selected_id);
        
				//selected name would not be sent on post since we would not give the input text a name.
				$('#photo_taken_by').val(selected_name);	
                
                console.log(photo_taken_by);
			};
			Main.Mod.show_bootbox_confirm('Continue?', 'Confirmation', callback_func);
        });	
    }
	function retrieve_barangay(){
		$.ajax({
            type: 'GET',
            url: Main.Vars.host + 'barangay/retrieve/in',
            contentType: false,
            cache: false,   
            processData:false, 
            dataType: 'json',
			success: function(data){
				var select = $('#barangay-select');
				$.each(data.data, function(key, val){
				  select.append('<option value="' + val.barangay_id + '">' + val.barangay_name + '</option>');
				});
			},
		});
	};
	
	function load_case_id(){
		$.ajax({
            type: 'GET',
            url: Main.Vars.host + 'casereport/caseid',
            contentType: false,
            cache: false,   
            processData:false, 
            dataType: 'json',
			success: function(data){
				$('#case-id').text(data.id);
                //create default scene kapag case report module ang gamit
                if(CSR.Vars.user_mod == 'casereport'){
                    create_document_folders();
                }
			},
		});
	}
	/*=======================END RETRIEVING OPERATIONS============================*/
	
	/*========================================================
	*
	*						MODALS SHOWING
	*
	*=========================================================*/
	
	var show_modal = function show_modal(what){
		if(what == 'investigator_modal'){ 
			$('#investigatorsModal').modal(Main.Vars.modal_options)
			$('#investigatorsModal .modal-content').css('width', '1000');
			$('#investigatorsModal .modal-content').css('margin-left', '-150');
		} else if(what == 'fingerman_modal'){
            Main.Mod.show_modal('fingermanModal',1200, -300);
        } else if(what == 'photographer_modal'){
            Main.Mod.show_modal('photographerModal',1200, -300);
        }else{
			$('#arresttingModal').modal(Main.Vars.modal_options)
			$('#arresttingModal .modal-content').css('width', '1000');
			$('#arresttingModal .modal-content').css('margin-left', '-150');		
		}
		
		
	}
	
	/*=======================END MODALS SHOWING============================*/
	return {
		onLoad : onLoad,
		retrieve : retrieve, 
		show_modal : show_modal,
		investigator_id : investigator_id,
        finger_taken_by : finger_taken_by,
        photo_taken_by : photo_taken_by,
        barangay_id : barangay_id,
		load_case_id : load_case_id,
		get_selected_arresting_officers : get_selected_arresting_officers,
		arresting_officers_ids : arresting_officers_ids,
		temp_arresting_officers_ids : temp_arresting_officers_ids,
        arresting_officers_names : arresting_officers_names,
        aoArrestingOfficers : aoArrestingOfficers,
	}
})();


