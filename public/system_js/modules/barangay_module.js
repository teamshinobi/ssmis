var BM = BM || {};

BM.BarangayModule = (function(){
//getting all barangay and put it on the datatable
	var barangay_id = "";
	var barangay_table = null;

	$.contextMenu({
            selector: '.item-context, .nitem-context', 
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                BM.BarangayModule.barangay_context_action(key, options);
            },
            items: {
                "view": {name: "View", icon: 'view'},
                //"remove": {name : "Remove", icon: 'delete'},
            }
        });


	var retrieve = function retrieve(table){
		barangay_table = table;
		barangay_table.dataTable({
			"autoWidth": false,
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"ajax" : {
				url :  Main.Vars.host + "barangay/retrieve",
				type : "GET",
			},
			"columns": [
				{ 
					"data": "barangay_id",
					"width": "150px",
				

				},	
				{
					"data": "barangay_name",	
					"width": "150px",
				},
				{
					"data": "city",	
					"width": "150px",
				},
				{
					"data": "barangay_incidents",	
					"width": "100px",
				},
				{
					"data": "barangay_chairman",	
					"width": "100px",
				},
						
			],
			"fnCreatedRow"  : function( nRow, aData, iDataIndex ){
				$(nRow).addClass('item-context');	
			},
			
		});

		/*$('#barangay_table tbody').on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
		
			$('#viewModal').modal(Main.Vars.modal_option);
				$.ajax({
				type: 'GET',
	            url:  Main.Vars.host + 'barangay/get/' + id,
	            contentType: false,
	            cache: false,   
	            processData:false, 
	           // data: formdata,
	            dataType: 'json',
				success: function(data){
					if(data.error == true){
						alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);	
					} else {
						$('#barangay_id').val(data.data[0].barangay_id);
						$('#barangay_name').val(data.data[0].barangay_name);
						$('#barangay_incidents').val(data.data[0].barangay_incidents);
						$('#barangay_category').val(data.data[0].barangay_category);
						$('#barangay_chairman').val(data.data[0].barangay_chairman);
						$('#imprisonment').val(data.data[0].imprisonment);
					}
				}


			});
		});	*/
	}

	var update = function update(formdata){
		
			$.ajax({
	            type: 'POST',
	            url:  Main.Vars.host + 'barangay/update',
	            contentType: false,
	            cache: false,   
	            processData:false, 
	            data: formdata,
	            dataType: 'json',
				success: function(data){
					//$('#myModal').modal('hide');
					//check if there are errors
					if(data.error == true){
						bootbox.alert(
							{ 
								message: 'There were errors on updating the record. \nError Message: ' + data.db_err.db_err_message,
								title: "Message"
							}
						);
					} else {
						bootbox.alert(
							{ 
								message: data.message,
								title: "Message"
							});//end of bootbox
						
						}			
						//refresh table
						$('#viewModal').modal('hide');
						barangay_table.DataTable().ajax.reload();	
					
				}
 			});
		
	}
	var report = function report(){
		window.location.assign(Main.Vars.host + "barangay/report");
	};
	function init_id(){  
	        Main.Mod.do_ajax('barangay/id','GET', function(data){
	            $('#barangay_increment').text(data.id);
	        },null);
	 }


	var add = function add(){
		$('#addModal').modal(Main.Vars.modal_option);
		init_id();		
		//on_vehicle_saved();
		 $("#add-barangay-form").on('submit',(function(e){
            e.preventDefault();
            var formdata = new FormData(this);
            formdata.append('barangay_id', $('#barangay_increment').text().trim());
            //formdata.append('file', $('#file')[0].files[0]);
            Main.Mod.do_ajax('barangay/add', 'POST', function(data){            
                if(!data.error){
                    Main.Mod.show_bootbox_alert(data.message,'Success', function(){
                      
                        barangay_table.DataTable().ajax.reload();
                        $('#addModal').modal('hide');
                       // Main.Mod.clear_form_inputs('add_narcotics', new Array('text'));
                        //$('#filepreview').attr('src', Main.Vars.host  + 'public/resources/default_img.jpg');
                        $('#barangay_increment').text(data.id);            
                    });  
                } else {
                    Main.Mod.show_bootbox_alert('Error in saving vehicle. Contact IT support or try again.','Success');     
                }
            }, formdata);
        }));
	}
	
	  /*================================================================
    *                      CONTEXT ACTIONS
    *===============================================================*/

	 var barangay_context_action = function barangay_context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		barangay_id = $(tr).find('td:first').text();
        var className = (options.$trigger[0].className.split(" ")[0]);
		switch (key) {
		   case 'remove' :
             /*   if(className == 'item-context'){
                    ci_remove_item(item_id);
                }else {
                    cn_remove_item(item_id);
                }*/
                	ci_remove_item(barangay_id);
              
                break;
            case 'view' :
                    ci_view_item(barangay_id);       
                break;
		   default :
			   break;
	   }           
	}
	//eto na viewing sa barangay
	 function ci_view_item(id){
	 //	alert(barangay_id);
       $('#viewModal').modal(Main.Vars.modal_option);
	          $.ajax({
					type: 'GET',
					url: Main.Vars.host + 'barangay/view/' + id,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							
							$('#barangay_view').text(barangay_id);
							$('#barangay_name').val(data.data[0].barangay_name);
							$('#city').val(data.data[0].city);
							$('#barangay_incidents').val(data.data[0].barangay_incidents);
							$('#barangay_category').val(data.data[0].barangay_category);
							$('#barangay_chairman').val(data.data[0].barangay_chairman);

							
						
						}
					}
				});
    }
    function ci_remove_item(id){
    
    	 $.ajax({
					type: 'GET',
					url: Main.Vars.host + 'barangay/remove_barangay/' + id,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
						if(data.error == true){
						bootbox.alert(
							{ 
								message: 'There were errors on deleting the record. \nError Message: ' + data.db_err.db_err_message,
								title: "Message"
							}
						);
					} else {
						bootbox.alert(
							{ 
								message: data.message,
								title: "Message"
							});//end of bootbox
						
						}			
						//refresh table
						barangay_table.DataTable().ajax.reload();	
					
				}
 			});
    }
     


	return{
		retrieve : retrieve,
		update : update,
		report : report,
		add : add,
		barangay_context_action : barangay_context_action,

	}


})();