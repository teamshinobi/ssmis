var RA = RA || {};

RA.RaModule = (function(){
	
	var charges_id= "";
	var ra_table = undefined;
	//retrieve all ra law and initialize

	$.contextMenu({
            selector: '.item-context, .nitem-context', 
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                RA.RaModule.ra_context_action(key, options);
            },
            items: {
                "view": {name: "View", icon: 'view'},
                "remove": {name : "Remove", icon: 'delete'},
            }
     });

	var retrieve = function retrieve(table){

		ra_table = table;
		ra_table.dataTable({
			"autoWidth": false,
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"ajax" : {
				url :  Main.Vars.host + "ra/retrieve",
				type : "GET",
			},
				
			"columns": [
				{ 
					"data": "charge_id",
					"width": "200px",

				},	
				{
					"data": "article",	
					"width": "200px",
				},
				{ 
					"data": "section",
					"width": "200px",
					
				},
				{
					"data": "description",
					
				}	
			
			],

			"fnCreatedRow"  : function( nRow, aData, iDataIndex ){
				$(nRow).addClass('item-context');	
			},
			"aoColumnDefs ": [
				null,
				null,
				null,
				{ "bSortable": false },
			],
			sPaginationType: "full_numbers",
		});

	/*	$('#ra_table tbody').on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
			//view();
			$.ajax({
				type: 'GET',
	            url:  Main.Vars.host + 'ra/get/' + id,
	            contentType: false,
	            cache: false,   
	            processData:false, 
	           // data: formdata,
	            dataType: 'json',
				success: function(data){
					if(data.error == true){
						alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);	
					} else {
						$('#charge_id').val(data.data[0].charge_id);
						$('#article').val(data.data[0].article);
						$('#section').val(data.data[0].section);
						$('#description').val(data.data[0].description);
						$('#fine').val(data.data[0].fine);
						$('#imprisonment').val(data.data[0].imprisonment);
					}
				}


			});
		});*/	
	};

	var add = function add(){
		//$('#addModal').modal(Main.Vars.modal_option);
//		$('#addModal .modal-content').css('width', '800');
//		$('#addModal .modal-content').css('margin-left', '-100');
		$('#addModal').modal(Main.Vars.modal_option);
		init_id();		
		//on_vehicle_saved();
        $("#add-ra-form").off('submit').on('submit',(function(e){
            e.preventDefault();
            var formdata = new FormData(this);
            formdata.append('charge_id', $('#charge_increment').text().trim());
            //formdata.append('file', $('#file')[0].files[0]);
            Main.Mod.do_ajax('ra/add', 'POST', function(data){            
                if (!data.error){
                    Main.Mod.show_bootbox_alert('Charge successfully saved!','Success', function(){
                        
                        ra_table.DataTable().ajax.reload();
                        $('#addModal').modal('hide');
                       // Main.Mod.clear_form_inputs('add_narcotics', new Array('text'));
                        //$('#filepreview').attr('src', Main.Vars.host  + 'public/resources/default_img.jpg');
                        $('#charge_increment').text(data.id);            
                    });  
                } else {
                    Main.Mod.show_bootbox_alert('Error in saving vehicle. Contact IT support or try again.','Success');     
                }
            }, formdata);
        }));


	};

	function view(){
	 	$('#viewRaModal').modal(Main.Vars.modal_option);
		$('#viewRaModal .modal-content').css('width', '800');
		$('#viewRaModal .modal-content').css('margin-left', '-100');
	};
	function init_id(){  
	        Main.Mod.do_ajax('ra/id','GET', function(data){
	            $('#charge_increment').text(data.id);
	        },null);
	 }

	var update = function update(formdata){
		$.ajax({
            type: 'POST',
            url:  Main.Vars.host + 'ra/update',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				//$('#myModal').modal('hide');
				//check if there are errors
				if(data.error == true){
					bootbox.alert(
						{ 
							message: 'There were errors on updating the record. \nError Message: ' + data.db_err.db_err_message,
							title: "Message"
						}
					);
				} else {
					$('#viewRaModal').modal('hide');
					bootbox.alert(
						{ 
							message: data.message,
							title: "Message"
						}
					);
					//refresh table
					ra_table.DataTable().ajax.reload();
				}
			}
		});
	}



	 /*================================================================
    *                      CONTEXT ACTIONS
    *===============================================================*/

	 var ra_context_action = function ra_context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		charges_id = $(tr).find('td:first').text();
        var className = (options.$trigger[0].className.split(" ")[0]);
		switch (key) {
		   case 'remove' :
                	ci_remove_item(charges_id);
                break;
            case 'view' :
                    ci_view_item(charges_id);        
                break;
		   default :
			   break;
	   }           
	}
	function ci_view_item(id){
		view();
		$.ajax({
				type: 'GET',
	            url:  Main.Vars.host + 'ra/get/' + id,
	            contentType: false,
	            cache: false,   
	            processData:false, 
	           // data: formdata,
	            dataType: 'json',
				success: function(data){
					if(data.error == true){
						alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);	
					} else {
						$('#charge_id_view').text(id);
						$('#article').val(data.data[0].article);
						$('#section').val(data.data[0].section);
						$('#description').val(data.data[0].description);
						$('#fine').val(data.data[0].fine);
						$('#imprisonment').val(data.data[0].imprisonment);
					}
				}


			});
	}
	function ci_remove_item(id){
        Main.Mod.show_bootbox_confirm("Are you sure you want to delete?", "Confirmation", function(){
            $.ajax({
					type: 'GET',
					url: Main.Vars.host + 'ra/remove_charges/' + id,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
						if(data.error == true){
						bootbox.alert(
							{ 
								message: 'Cannot delete charge. It is associated with one or may cases.',
								title: "Message"
							}
						);
					} else {
						bootbox.alert(
							{ 
								message: "Deleted!",
								title: "Message"
							});//end of bootbox
						
						}			
						//refresh table
						ra_table.DataTable().ajax.reload();	
					
				}
 			});
        },null);
    	 
    }
	
	return{
		retrieve : retrieve,
		add : add,
		//save : save,
		update : update,
		ra_context_action : ra_context_action,
	}
})();