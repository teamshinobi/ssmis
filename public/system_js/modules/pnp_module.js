var PNP = PNP || {};

//Module for adding new PNP including adding his/her privileges
PNP.PnpModule = (function(){
	
	var pnp_table = null;
    //because username is just the pnp_id
    var pnpId = '';
    var onLoad = function onLoad(){
        Main.Mod.do_ajax('home/get_user_pnp_id', 'GET', function(data){
           
            userId = data.username;
            
        }, null);
    }
    
	//gets all pnp and putting it on DataTables
	var retrieve = function retrieve(table){
		pnp_table = table;
		pnp_table.DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			
			"ajax" : {
				url :  Main.Vars.host + "pnp/retrieve/p",
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 3 ] },
					{ "sWidth": "20%", "aTargets": [ 0 ] },
					{ "sWidth": "20%", "aTargets": [ 1 ] },
					{ "sWidth": "40%", "aTargets": [ 2 ] },
                    { "sWidth": "40%", "aTargets": [ 3 ] },
					{ "sWidth": "20%", "aTargets": [ 4 ] },
					{ "sClass": "hidden-xs" , "aTargets": [ 4 ] },
					
			],
			"columns": [
				{ 
					"data": "pnp_id",
					
				},	
				{
					"data": "rank",	
				
				},
				{ 	
					"render": function(data,type,row,meta) {
                        var str = '';
                        if(row.pnp_id == userId){
                            str = 'You';
                        } else {
                            str = row.first_name + " " + row.last_name;
                        }
                      
						return str;
					}
				
				},
                {
                    "data" : "status",
                },
				{ 	
					class: "hidden-sm",
					"render": function(data,type,row,meta) {
						return "<img class=\"obj-pic\" src=" + (Main.Vars.base_path + row.file_path) + '_thumb.jpg' + " />";
					}
				}	
			
			],
			
			sPaginationType: "full_numbers",
			
		});
		
		//handle the redirection for viewing of police
		$('#police_table tbody').on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
			window.location.assign(Main.Vars.host + "pnp/view/" + id);
		});	
	};
	
	
	var add = function add(){
        Services.ModalService.showModal('addModal',1000,-150);
	};
	
	var save = function save(formdata){
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'pnp/add/p',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				//$('#myModal').modal('hide');
				//check if there are errors
				if(data.error == true){
					alert('There were errors in saving the record. There might be duplicate in batch id.');
				} else {
					$('#addModal').modal('hide');
					alert('Successfully saved!');
					//refresh table
					pnp_table.DataTable().ajax.reload();
				}
			}
		});
	}
	
	/*=========================================================================
	*
	*							SHOWING NG MODALS
	*
	*======================================================================*/
	return {
		retrieve : retrieve,
		add : add,
		save : save,
        onLoad : onLoad,
	};

})();

//Module for viewing with updating and removing of police
PNP.ViewPnpModule = (function(){
	var batch_id = window.location.href;
	var res = batch_id.split("/");	
	batch_id = res.slice(-1)[0]; 	
    
    
    var onLoad = function onLoad(){
        retrieve_affiliations();
        check_privileges();
        check_cases();
    }
	var update = function update(formdata){
		
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'pnp/update/p',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				if(data.error == true){
					alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
				} else {
					bootbox.alert(
					{ 	message: data.message,
						title: "Message",
						callback : function(){
							//redirect to the same page for reloading...
							//window.location.assign(Main.Vars.host + "pnp/view/" +  $("input[name='pnp_id']").val());
				          window.location.assign(Main.Vars.host + 'pnp/view/' + $('#pnp_id_u').val().trim());
                        }
						
					});				
			
				
				}
			}
		});	
	};
	
	/*=========================================================================
	*
	*								RETRIEVING
	*
	*======================================================================*/
    
    function check_cases(){
        var iCases = $('#investigatorCases');
        var aCases = $('#arrestingCases');
        
        Main.Mod.do_ajax('pnp/as_investigator/' +batch_id, 'GET', function(data){
            
            if(data.has_data){
                
                $.each(data.result, function(idx,obj){
                    var str = '<li>';
                    
                    str += 'Case BLOTTER : ' + obj.blotter_entry_nr + ' with CASE ID of ' + '<a href="" >' + obj.case_id + '</a>';
                    
                    str+='</li>';
                    
                    iCases.append(str);
                });
            } else {
                var str = '<li>' + data.message + '</li>';
                iCases.append(str);
            }

        
        }, null );
        
        
        Main.Mod.do_ajax('pnp/as_arresting/' +batch_id, 'GET', function(data){
            
            if(data.has_data){
                
                $.each(data.result, function(idx,obj){
                    var str = '<li>';
                    
                    str += 'Case BLOTTER : ' + obj.blotter_entry_nr + ' with CASE ID of ' + '<a href="" >' + obj.case_id + '</a>';
                    
                    str+='</li>';
                    
                    aCases.append(str);
                });
            } else {
                var str = '<li>' + data.message + '</li>';
                aCases.append(str);
            } 

        
        }, null );
    }
	function retrieve_affiliations(){
		$.ajax({
            type: 'GET',
            url: Main.Vars.host + 'pnp/retrieve_affiliations/' + $('#pnp_id').text().trim(),
            contentType: false,
            cache: false,   
            processData:false,           
            dataType: 'json',
			success: function(data){
				var list = $('#affiliationsList');
				if(data.error == true){
					list.append('<div class="list-group-item">No groups yet.</div>');
				}else{
					$.each (data['data'], function (i,obj) {
						list.append('<li class="list-group-item">' + obj.group_name  + '<span class="pull-right">' + 
							'<button data-value="' + obj.group_id + '"class="btn btn-info"><span class="glyphicon glyphicon-trash"></span></button></li>');					
					});
					
					//add clicking of buttons functionality for removing
					$('#affiliationsList button').click(function(){
						var button = $(this);
						var tr = button.parent().parent();
						//function to be executed when the user click 'yes' on bootbox
						var callback_func = function callback_func(){
							//alert(button.attr('data-value') + ' ' + pnp_id );
							$.ajax({
								type: 'GET',
								url: Main.Vars.host + 'pnp/remove_affiliation/' + button.attr('data-value') + '/' + batch_id,
								contentType: false,
								cache: false,   
								processData:false, 
								dataType: 'json',
								success: function(data){
									if(data.error == true){
										alert('There were errors in completing the operation. \nError Message: ' + data.message);
									} else {
										bootbox.alert({ message: data.message,
										title: "Message",});	
										tr.remove();	
									}
								}
							});
						}
						
						Main.Mod.show_bootbox_confirm('Are you sure you want to remove this police from the group?', 'Confirmation', callback_func);			
					});
				}	
			}
		});	
	}
    
    //check account of a particular police.
    function check_privileges(){
        var callback = function(data){
            if(data.has_account){
                $('#p-message').remove();
                var content = $('#privileges');
                var group  = '';
         
                if(data.account_details.group.name == 'members'){
                    group = data.account_details.group.name.substring(0,6) + ' (General User)';
                } else {
                   group = data.account_details.group.name;
                }
                var str = '<p class="p-highlight"> Account ID: <span style="font-weight:normal;">' +
                            data.account_details.id +
                        '<span> <p>' +
                        '<p class="p-highlight"> Date Created: <span style="font-weight:normal;">' +
                            data.account_details.date_created +
                        '<span> <p>' +
                          '<span> <p>' +
                        '<p class="p-highlight"> User-type: <span style="font-weight:normal;">' +
                            group
                        '<span> <p>' +
                        '<p>' +
                            '<button class="btn btn-default"><i class="fa fa-wrench"></i> Manage </button>' +
                        '</p>';
                content.append(str);
            } else {
                $('#p-message').text(data.message + '  ');
                $('#p-message').append('<a id="create_account_a">Create Account.</a>');
            }
            
            on_create_account();
        }
        
        var formdata = new FormData();
        formdata.append('pnp_id', $('#pnp_id').text().trim());
        
        Main.Mod.do_ajax('pnp/privileges','POST', callback, formdata);
    }
    //handle create account event
    function on_create_account(){
        $('#create_account_a').off('click').on('click',function(){
            var ok_callback = function(){
                var formdata = new FormData();
                formdata.append('pnp_id', $('#pnp_id').text().trim());
                Main.Mod.do_ajax('pnp/account', 'POST', function(data){
                    if(data.success){
                        Main.Mod.show_bootbox_alert("Use this password to login: " + data.password, 'Message', null);
                    } else {
                        Main.Mod.show_bootbox_alert("Cannot create account.", 'Error', null);
                    }
                }, formdata);
            }
            Main.Mod.show_bootbox_confirm(
               'The sytem will create account for this officer, are you sure you want to continue?',
               'Confirmation',
               ok_callback,
               null
            );
        });
    }
    function on_image_upload(){
        $('#uploadPhotoButton').off('click').on('click',function(){
            var callback = function(data){
                if(data.success){
                    location.reload();
                }else{
                    alert('Cannot update image');
                }
            }
            
            var formdata = new FormData();
			formdata.append('pnp_id', $('#pnp_id').text().trim());
			formdata.append('file', $('#userfile')[0].files[0]);
			formdata.append('tag', $('#preview').attr('data-value'));
			
			Main.Mod.do_ajax('pnp/upload','POST', callback, formdata);
            
            //Main.Mod.do_ajax();
        });
    }
    
    function view_police(){
        var pnp_id = $('#pnp_id').text().trim();
        
        var callback = function(data){
            $('#pnp_id_u_text').text(data.data.pnp_id);  
            $('#pnp_id_u').val(data.data.pnp_id);  
            $('#email_u').val(data.data.email);  
            $('#first_name_u').val(data.data.first_name);
            $('#last_name_u').val(data.data.last_name);
            $('#middle_name_u').val(data.data.middle_name);
            var $radios = $('input:radio[name=gender_u]');
            if(data.data.gender == "1"){
                $radios.filter('[value=1]').prop('checked', true);
            }else{
                $radios.filter('[value=0]').prop('checked', true);
            }
            
            //select
            $('#rank_div option[value="' +data.data.rank +'"]').prop('selected',true);
            $('input[name=member_since]').val(data.data.member_since);
            $('#status_u option[value="' +data.data.status +'"]').prop('selected',true);
            $('#contact_number_u').val(data.data.contact_number);
            $('#address_u').val(data.data.address);
        }
        
        Main.Mod.do_ajax('pnp/get/' + pnp_id, 'GET', callback, null);
        
    }
	/*=========================================================================
	*
	*							SHOWING NG MODALS
	*
	*======================================================================*/
	var show_modal = function show_modal(type){
		//show affiliations modal
		if(type == 'a'){
			retrieve_affiliations();
			$('#affiliationsModal').modal(Main.Vars.modal_options);
        }else if(type == 'b'){
            Main.Mod.show_modal('uploadFormModal',500,100);
            on_image_upload();
        }else if(type == 'c'){
            Main.Mod.show_modal('updatePNPModal',1000,-200);
            view_police();
        }
		
	};
	
	var close_modal = function close_modal(type){
		//close affiliation modal
		if(type == 'a'){
			//remove all children in the list.
			$('#affiliationsList').empty();
		}
	}
	
	return {
        onLoad : onLoad,
		update : update,
		show_modal : show_modal,
		close_modal : close_modal,
	};
})();

//Module for pnp groups
PNP.PnpGroupModule = (function(){
	var group_table = undefined;
	var pnp_table = undefined;
	var members_table = undefined;
	
	var add = function add(){
		$('#addModal').modal(Main.Vars.modal_options);
	};
	
	/*=================================================================
	*
	*							RETRIEVING
	*
	*==================================================================*/
	var retrieve = function retrieve(table, type){
		if( type == 'g'){
			init_group_table(table);
		}else if (type == 'p'){
			init_pnp_table(table);
		}else{
			init_members_table(table);
		}
	}
	
	//private method for creating group table
	function init_group_table(table){
		group_table = table;
		
		group_table.DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			
			"ajax" : {
				url :  Main.Vars.host + "pnp/retrieve/g",
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 3 ] },
					{ "sWidth": "20%", "aTargets": [ 0 ] },
					{ "sWidth": "25%", "aTargets": [ 1 ] },
					{ "sWidth": "45%", "aTargets": [ 2 ] },
					{ "sWidth": "10%", "aTargets": [ 3 ] },
			],
			"columns": [
				{ 
					"data": "group_id",
				},	
				{
					"data": "group_name",	
				},
				{ 	
					"render": function(data,type,row,meta) {
						var n = row.group_task.length;
						var str = row.group_task;		
						
						str = row.group_task.substring(0, 200);	
						
						if(n > 200){
							str = str + '...';
						
						}
						
						return str;
					}
				},
				{ 
					"data": "members",
				}	
			
			],
			
			sPaginationType: "full_numbers",
			
		});
		//handle the redirection for viewing of group
		group_table.on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
			if(id != ''){
				window.location.assign(Main.Vars.host + "pnp/viewgroup/" + id);
			}
		});	
	
	}
	//private method for creating police table
	function init_pnp_table(table){
	
		pnp_table = table;
		
		var group_id = window.location.href;
		var res = group_id.split("/");	
		group_id = res.slice(-1)[0]; 	
		
		//retrieve available police for adding in the group
		pnp_table.DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			
			"ajax" : {
				url :  Main.Vars.host + "pnp/retrieve_not_group_members/" + group_id,
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 3 ] },
					{ "sWidth": "25%", "aTargets": [ 0 ] },
					{ "sWidth": "30%", "aTargets": [ 1 ] },
					{ "sWidth": "30%", "aTargets": [ 2 ] },
					{ "sWidth": "15%", "aTargets": [ 3 ] },
				
			],
			"columns": [
				{ 
					"data": "pnp_id",
					

				},	
				{
					"data": "rank",	
				
				},
				{ 	
					"render": function(data,type,row,meta) {
						return row.first_name + " " + row.last_name;
					}
				
				},
				{ 
					
					"render": function(data,type,row,meta) {
						//console.log(base_path + row.file_path_thumb);
						return "<img style='margin-left: 40px' class='obj-pic' src=" + (Main.Vars.base_path + row.file_path) + '_thumb.jpg' + " />";

					}
				},
			],
			
			sPaginationType: "full_numbers",
			
		});
		
		//ajax baby boy! Add the member to a particular group.
		$('#police_table tbody').on( 'click', 'tr', function () {
			var tr = $(this);
			var id = $(this).find("td:first").text();
			
			var callback_func = function callback(){
				tr.remove();
				var group_id = window.location.href;
				var res = group_id.split("/");	
				group_id = res[6];	
				ajax_join_member(group_id, id);	
			};
			
			Main.Mod.show_bootbox_confirm('Are you sure you want to add this police to the group?', 'Confirmation', callback_func);
			
		});		
	}
	
	//private method for creating members table
	function init_members_table(table){
		members_table = table;
		
		var group_id = window.location.href;
		var res = group_id.split("/");	
		group_id = res.slice(-1)[0];	
	
		members_table.DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			
			"ajax" : {
				url :  Main.Vars.host + "pnp/retrieve_group_members/" + group_id,
				type : "GET",
			},
			"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 3 ] },
					{ "sWidth": "25%", "aTargets": [ 0 ] },
					{ "sWidth": "30%", "aTargets": [ 1 ] },
					{ "sWidth": "30%", "aTargets": [ 2 ] },
					{ "sWidth": "15%", "aTargets": [ 3 ] },
				
			],
			"columns": [
				{ 
					"data": "pnp_id",
					

				},	
				{
					"data": "rank",	
				
				},
				{ 	
					"render": function(data,type,row,meta) {
	
						return row.first_name + " " + row.last_name;
					}
				
				},
				{ 
					
					"render": function(data,type,row,meta) {
						//console.log(base_path + row.file_path_thumb);
						return "<img style='margin-left: 40px' class='obj-pic' src=" + (Main.Vars.base_path + row.file_path)+ '_thumb.jpg' + " />";

					}
				},
			],
			
			sPaginationType: "full_numbers",
			
		});
		//handle the redirection for viewing of police
		$('#members_table tbody').on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
			window.location.assign(Main.Vars.host + "pnp/view/" + id);
		});	
	}
	
	function ajax_join_member(group_id, batch_id){
	   Main.Mod.show_modal('loadingModal', 750, -50);    
       
		$.ajax({
			type: 'GET',
			url: Main.Vars.host + 'pnp/join_group/' + group_id + '/' + batch_id,
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
                $('#loadingModal').modal('hide');
				if(data.error == true){
					alert('There were errors in completing the request. \nError Message: ' + data.db_err.db_err_message);
				   
                } else {
					bootbox.alert({ 
						message: data.message,
						title: "Message",
						callback: function(){
							//reload the current document from the server
                           
							location.reload(true);
                            
						}			
					});
					
				}
			}
		});
	
	}
	
	/*==============================END RETRIEVING====================================*/
	var save = function save(formdata){
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'pnp/add/g',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				if(data.error == true){
					alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
				} else {
					$('#addModal').modal('hide');
					alert('Successfully saved!');	
					group_table.DataTable().ajax.reload();					
				}
			}
		});
	}	
	
	
	var show_modal = function show_modal(type){
		if(type == 'edit'){
			$('#editGroupModal').modal(Main.Vars.modal_options);
		}else{
		
			$('#addMemberModal').modal(Main.Vars.modal_options);
		}

	};
	
	var update = function update(formdata){
		//show prompt here
		Main.Mod.show_prompt('Please wait...', 'Processing request...');
		
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'pnp/update/g',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				if(data.error == true){
					alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
				} else {
					alert('Successfully updated!');
					
					Main.Mod.hide_prompt();
					
					//redirect to the same page for reloading...
					window.location.assign(Main.Vars.host + "pnp/viewgroup/" + $("input[name='group_id']").val() );
				}
			}
		});	
	};
	return {
		add : add,
		save : save,
		retrieve : retrieve,
		show_modal: show_modal,
		update : update,
	};
})();

