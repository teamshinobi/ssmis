var NM = NM || {};

NM.NarcoticsModule = (function(){


var narcotics_id = "";
/*NM.Vars = (function(){ 
  
    var item_seized_tbl = undefined;
    var nitem_seized_tbl = undefined;
    var item_violators = new Array();
    var items = new Array();
    var nitems = new Array();
    //removed item
    var items_rem = new Array();
    var nitems_rem = new Array();
    var selected_item = {};
    var selected_sOfficers = new Array();
    var aoSelected_officers = new Array();
    return {
        item_seized_tbl : item_seized_tbl,
        nitem_seized_tbl : nitem_seized_tbl,
        item_violators : item_violators,   
        items : items,
        items_rem : items_rem,
        nitems_rem : nitems_rem,
        nitems : nitems,
        selected_item : selected_item,
        selected_officers : selected_sOfficers,
        aoSelected_officers : aoSelected_officers,
    }
    
})();*/


	//gets all pnp and putting it on DataTables

	var onLoad = function onLoad(){



	}

	$.contextMenu({
            selector: '.item-context', 
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                NM.NarcoticsModule.narcotics_context_action(key, options);
            },
            items: {
                "view": {name: "View", icon: 'view'},
                "remove": {name : "Remove", icon: 'delete'},
            }
        });

	
	var narcotics_table = null;
	var retrieve = function retrieve(table){
		narcotics_table = table;
		narcotics_table.dataTable({
			"autoWidth": false,
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"order" : [[ 0, "desc" ]],
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"ajax" : {
				url :  Main.Vars.host + "narcotics/retrieve",
				type : "GET",
			},
			"columns": [
				{ 
					"data": "narcotics_id",
					"width": "150px",

				},	
				{
					"data": "name",	
					"width": "150px",
				},
				{
					"data": "description",	
					"width": "600px",
				},
		
						
			],
			"fnCreatedRow"  : function( nRow, aData, iDataIndex ){
				$(nRow).addClass('item-context');	
			},
			
		});
		$('#narcotics_table tbody').on( 'click', 'tr', function () {
			var id = $(this).find("td:first").text();
			//$('#viewModal').modal(Main.Vars.modal_option);
		
				$.ajax({
					type: 'GET',
					url: Main.Vars.host + 'narcotics/view/' + id,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){

						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							
							$('#narcotics_id').val(data.data[0].narcotics_id);
							$('#name_v').val(data.data[0].name);
							$('#description_v').val(data.data[0].description);
							
							//$('#narcotics_id_v').prop('disabled', true);
							$('#name_v').prop('disabled', false);
							$('#description_v').prop('disabled', false);
							
					

						}
					}
				});
		});	
	}
	function on_vehicle_saved(){
         $("#add_narcotics_form").on('submit',(function(e){
            e.preventDefault();
            var formdata = new FormData(this);
            formdata.append('narcotics_id', $('#narcotics_id').text().trim());
            //formdata.append('file', $('#file')[0].files[0]);
            Main.Mod.do_ajax('narcotics/add', 'POST', function(data){            
                if(data.success){
                    Main.Mod.show_bootbox_alert(data.message,'Success', function(){
                        $('#narcotics_tbl').DataTable().ajax.reload();
                        $('#addModal').modal('hide');
                       // Main.Mod.clear_form_inputs('add_narcotics', new Array('text'));
                        //$('#filepreview').attr('src', Main.Vars.host  + 'public/resources/default_img.jpg');
                        $('#narcotics_id').text(data.id);            
                    });  
                } else {
                    Main.Mod.show_bootbox_alert('Error in saving vehicle. Contact IT support or try again.','Success');     
                }
            }, formdata);
        }));
 

	}
	function view(){
	 	$('#viewModal').modal(Main.Vars.modal_option);
		$('#viewModal .modal-content').css('width', '800');
		$('#viewModal .modal-content').css('margin-left', '-100');
	};

		

	

	var add = function add(){
		$('#addModal').modal(Main.Vars.modal_option);
		init_id();		
		//on_vehicle_saved();
		 $("#add_narcotics_form").on('submit',(function(e){
            e.preventDefault();
            var formdata = new FormData(this);
            formdata.append('narcotics_id', $('#narcotics_id').text().trim());
            //formdata.append('file', $('#file')[0].files[0]);
            Main.Mod.do_ajax('narcotics/add', 'POST', function(data){            
                if(data.success){
                    Main.Mod.show_bootbox_alert(data.message,'Success', function(){
                        $('#narcotics_tbl').DataTable().ajax.reload();
                        $('#addModal').modal('hide');
                       // Main.Mod.clear_form_inputs('add_narcotics', new Array('text'));
                        //$('#filepreview').attr('src', Main.Vars.host  + 'public/resources/default_img.jpg');
                        $('#narcotics_id').text(data.id);            
                    });  
                } else {
                    Main.Mod.show_bootbox_alert('Error in saving vehicle. Contact IT support or try again.','Success');     
                }
            }, formdata);
        }));
 

		

	};

	function init_id(){  
	        Main.Mod.do_ajax('narcotics/id','GET', function(data){
	            $('#narcotics_increment').text(data.id);
	        },null);
	    }
	function get_id(){  
	            $('#narcotics_view').text(data.id);
	           
	    }
	
	var report = function report(){
		window.location.assign(Main.Vars.host + "narcotics/report");
	};
	var close = function close(){
		
		if($('#narcotics_id').val() != ''){
			$('#addModal').unbind();
			$('#addModal').css('opacity', .9);
			bootbox.dialog({
			closeButton: false,
			autoOpen: false,
			modal: true,
			message: "Are you sure to close this window? This will clear the fields",
			title: "Confirm",
			size: 'small',
			backdrop : true,
			
			buttons: {
				yes: {
				  label: "YES",
				  className: "btn-success",
				  callback: function() {
					$('#addModal').bind();
					$('#addModal').css('opacity', 1);
					$('#addModal').removeData("modal").modal({});
					$('#addModal').modal('toggle');
					$('#narcotics_id').val('');
					$('#name').val('');
					$('#description').val('');
				  }
				},
				no: {
				  label: "NO",
				  className: "btn-danger",
				  callback: function() {
					$('#addModal').bind();
					$('#addModal').css('opacity', 1);
				  }
				}
			},
			className : "bootbox-small"
			});
			
		}else{
			$('#narcotics_id').val('');
			$('#name').val('');
			$('#description').val('');
			$('.disabled-style').css('color', '#9d9da3');
		}
		
	};
	var clear = function clear(){
			}

	var remove_narcotics = function remove_narcotics(){


	}

	var update = function update(formdata){

		$.ajax({
            type: 'POST',
            url:  Main.Vars.host + 'narcotics/update',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				if(data.error == true){
					bootbox.alert(
						{ 
							message: 'There were errors on updating the record. \nError Message: ' + data.db_err.db_err_message,
							title: "Message"
						}
					);
				} else{
					$('#viewModal').modal('hide');
					bootbox.alert(
						{ 
							message: data.message,
							title: "Message"
						}
					);
					//refresh table
					narcotics_table.DataTable().ajax.reload();
				}
			}
		});
	}

	var save = function save(formdata){
		
		$.ajax({
            type: 'POST',
            url:  Main.Vars.host + 'narcotics/add',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				//$('#myModal').modal('hide');
				//check if there are errors
				if(data.error == true){
					bootbox.alert(
						{ 
							message: 'There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message,
							title: "Message"
						}
					);
				} else {
					$('#addModal').modal('hide');
					bootbox.alert(
						{ 
							message: data.message,
							title: "Message"
						}
					);
					//refresh table
					narcotics_table.DataTable().ajax.reload();
				}
			}
		});
	}

	/*================================================================
    *                      CONTEXT ACTIONS
    *===============================================================*/

	 var narcotics_context_action = function narcotics_context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
	    narcotics_id = $(tr).find('td:first').text();
        var className = (options.$trigger[0].className.split(" ")[0]);
		switch (key) {
		   case 'remove' :
                	ci_remove_item(narcotics_id);
                break;
            case 'view' :
                    ci_view_item(narcotics_id);        
                break;
		   default :
			   break;
	   }           
	}
	//eto na viewing sa narcotics
	 function ci_view_item(id){
	 	

       $('#viewModal').modal(Main.Vars.modal_option);
	          $.ajax({
					type: 'GET',
					url: Main.Vars.host + 'narcotics/view/' + id,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
						    
							$('#narcotics_view').text(narcotics_id);
							$('#name').val(data.data[0].name);
							$('#description').val(data.data[0].description);
						//	$('#unit').val(data.data[0].unit);
							

							
						}
					}
				});
    }
     function ci_remove_item(id){
        
         Main.Mod.show_bootbox_confirm("Are you sure you want to continue?","Warning", function(){
            $.ajax({
                type: 'GET',
                url: Main.Vars.host + 'narcotics/remove_narcotics/' + id,
                contentType: false,
                cache: false,   
                processData:false, 
                dataType: 'json',
                success: function(data){
                    if(data.error == true){
                    bootbox.alert(
                        { 
                            message: 'Cannot delete narcotics. Possible association with one or many cases.',
                            title: "Message"
                        }
                    );
                } else {
                    bootbox.alert(
                        { 
                            message: data.message,
                            title: "Message"
                        });//end of bootbox

                    }			
                    //refresh table
                    narcotics_table.DataTable().ajax.reload();	

                }
            });
       
        },null);

    }
     


	
	return{
		retrieve : retrieve,
		add : add,
		save : save,
		report : report,
		update : update,
		close : close,
		clear : clear,
		narcotics_context_action,
	}
})();
