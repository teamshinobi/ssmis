var EA = EA || {};

EA.Vars = (function(){
   
    var aff_id = '';
    var type = '';
    var narrators = new Array();
    
    
    return {     
        aff_id : aff_id,
        type : type,
        narrators : narrators,
    }
    
})();


EA.Main = (function(){

    var onLoad = function(){
        EA.Vars.aff_id = $('#affidavit_id').text().trim();
        EA.Vars.type = $('#type').text().trim();
        
        EA.EventsListener.addFormSavingListener();
        EA.EventsListener.addRemoveNarratorListener();
        EA.EventsListener.addNarratorListener();
        //init summernote
        $('#affContent').summernote();
        
        init();

    }
    
  
    function init(){
        Main.Mod.do_ajax('forms/get/affidavit/' + EA.Vars.aff_id, 'GET', function(data){
            
            if(data.success){          
                $('#affContent').code(data.data.content);
              
                initNarrators(data.data.narrators);
            }
        },null);        
    }
    
    
    function initNarrators(narrators){     
        $('.narratorList').empty();
        $.each(narrators,function(idx,obj){
        EA.Vars.narrators.push(obj.pnp_id);

        $('.narratorList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + obj.narratorName  +
             '<span class="pull-right">' + '<button type="button" data-value="' + obj.pnp_id +
             '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');	
        }); 
    }  
    
    return {     
        onLoad : onLoad,
    }
    
})();

EA.EventsListener = (function(){
    
    var addNarratorListener = function(){
        $('#addNarratorButton').on('click',function(){
            Main.Mod.show_modal('addNarratorModal', 1200, -300);
            show_pnps();
        });
    }
    
    
    function show_pnps(){
        $('#police_tbl').DataTable({

            "deferRender": true,
            "autoWidth": false,

            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],

            "ajax" : {
                    url :  Main.Vars.host + "casereport/activepnps",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "25%", "aTargets": [ 0 ] },
                        { "sWidth": "25%", "aTargets": [ 1 ] },
                        { "sWidth": "35%", "aTargets": [ 2 ] },
                        { "sWidth": "10%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 4 ] },

                ],
                "columns": [
                    { 
                        "data": "pnp_id",


                    },	
                    {
                        "data": "rank",	

                    },
                    { 	
                        "render": function(data,type,row,meta) {

                            return row.first_name + " " + row.last_name;
                        }

                    },
                    { 

                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return '<img style="margin-left:20px;" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path) + '_thumb.jpg' + ' />';

                        }
                    },
                    {
                        data: null,
                        className: "center",
                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return  '<button style="margin-left: 10px;" data-name="' + row.first_name + ' ' + row.last_name +
                                    ' "data-value="' +  row.pnp_id + '" style="img style="padding-left: 0px; padding-right:0px;' +
                                    ' class="btn btn-primary btn-md action">' +
                                    '<span class="glyphicon glyphicon-plus"></span></button>';

                        }


                    }	
                ],

                sPaginationType: "full_numbers",

        });
        
         //add event handling
        $('#police_tbl tbody').off().on( 'click', '.action', function () {

            var selected_id = $(this).attr('data-value');
            var name = $(this).attr('data-name');

            if(EA.Vars.narrators.indexOf(selected_id) > -1){
                Main.Mod.show_bootbox_alert('Already in the list.', 'Message', null);
            } else {
               EA.Vars.narrators.push(selected_id);
                if(EA.Vars.narrators.length == 1){
                    $('.narratorList').empty();
                    $('.narratorList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name +
                                             '<span class="pull-right">' + '<button type="button" data-value="' + selected_id +
                                             '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                } else {
                    $('.narratorList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name  +
                                             '<span class="pull-right">' + '<button type="button" data-value="' + selected_id +
                                             '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                }

                $('#addNarratorModal').modal('hide');
            }

        });
            
    }
    
    var addRemoveNarratorListener = function(){
        $('.narratorList').off('click').on('click','button',function(){
            
            var id = $(this).attr('data-value');
            var parent_li = $(this).parent().parent();
            parent_li.remove();	
            
            //remove the item in array
            var i = EA.Vars.narrators.indexOf(id);
           
            //actual removing          
            EA.Vars.narrators.splice(i,1);
        
            
            if(EA.Vars.narrators.length == 0){
                $('.narratorList').append('<li class="list-group-item"> No police yet.</li>');
            }
            
        });  
    
    }
    
    var addFormSavingListener = function(){
      
        $('#saveButton').on('click',function(){  
            if(EA.Vars.narrators.length < 1){
                Main.Mod.show_bootbox_alert("Please choose atleast 1 narratoir.","Error", null);
            } else {
                var conf_callback = function(){
                    Main.Mod.show_modal('loadingModal',650,'');
                    var formdata = new FormData();
                    formdata.append('affidavit_id', EA.Vars.aff_id);
                    formdata.append('content', $('#affContent').code());
                    formdata.append('type', EA.Vars.type);
                    formdata.append('narrators', JSON.stringify(EA.Vars.narrators));

                    Main.Mod.do_ajax('forms/update/affidavit/', 'POST', function(data){
                        $('#loadingModal').modal('hide');
                        if(data.success){
                            Main.Mod.show_bootbox_alert("Successfully updated!", "Message",function(){
                                location.reload();
                            });
                        } else {
                            Main.Mod.show_bootbox_alert("Unable to update affidavit. Try again.", "Error",nul);
                        }

                    },formdata);
                }

                Main.Mod.show_bootbox_confirm("Are you sure you want to update this affidavit?", "Confirmation",
                                             conf_callback, null);
            }
        });
     
    }
    return {
        addFormSavingListener : addFormSavingListener,
        addRemoveNarratorListener : addRemoveNarratorListener,
        addNarratorListener : addNarratorListener
    }
   
})();