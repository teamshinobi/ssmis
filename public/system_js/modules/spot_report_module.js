var SPR = SPR || {};

SPR.Vars = (function(){
    
    //string that holds pnp_id
    var delivery_boy = '';
    var is_editing = false;

    return {
        delivery_boy : delivery_boy,
        is_editing : is_editing,
       
    }
})();

SPR.Main = (function(){

    var onLoad = function onLoad(get){
        if(!SPR.Vars.is_editing){
           init_summernote();
        }
     
        SPR.Events.attach(SPR.Events.events_const.ON_FORM_SAVED);
        SPR.Events.attach(SPR.Events.events_const.ON_SHOW_DELIVERY_BOYS);
        SPR.Events.attach(SPR.Events.events_const.ON_VIEW_SPOT_REPORTS);
        SPR.Events.attach(SPR.Events.events_const.ON_TABLE_BUTTONS_CLICKED);
      
        //this will become false if using edit spot report module along with this module
        if(get){
            get_id();
        }
       
    }
    
    function init_summernote(){
        $('.summernote').summernote();
        Main.Mod.do_ajax('forms/get_template/spot','GET',function(data){
            if(data.has_data){
                var dom = $(data.data.spot);
                $.each(dom, function(idx,obj){
                    $('.summernote').summernote('insertNode', obj);
                });
               
               
            }
        },null)
    }
    
    function get_id(){
        var callback = function(data){
            $('#spot_id').text(data.id);  
        }
        Main.Mod.do_ajax('forms/id/spotreport', 'GET', callback ,null);
    }
    
    return {
        onLoad : onLoad,
    }
})();



SPR.Events = (function(){

    var events_const = {    
        'ON_FORM_SAVED' : 1,
        'ON_SHOW_DELIVERY_BOYS' : 2,
        'ON_VIEW_SPOT_REPORTS' : 3,
        'ON_TABLE_BUTTONS_CLICKED' : 4,
    
    }
    function on_table_buttons_clicked(){
        $('#spot_reports_tbl').off('click').on('click','.action',function(e){
            var id = $(this).attr('data-value').trim();
    
            var action = $(this).attr('data-action')
            
            //console.log(action + ' ' + id);
            
            switch(action){
                case 'view':   
                    view_spotreport(id);
                    break;
                case 'edit':
                    edit_spot_report(id);
                    break;
                case 'delete':
                    var attachment = $(this).parent().parent().attr('data-attach');
                    delete_spot_report(id,attachment);
                    break;
                case 'unattach':
                    var tr = $(this).closest('tr');
                    var case_id = tr.find("td").eq(4).text();  
                    
                    unattach_spotreport(id,case_id);
                    break;
                default: 
                    alert('Cannot handle event. Contact IT Admini');
            }
        
        });    
      
    }
    
    function view_spotreport(id){
          window.open(Main.Vars.host + "forms/spot_report_pdf/" + id , '_blank');
    }
    
    function edit_spot_report(id){
        
        Main.Mod.show_modal('loadingModal',750,-50);
        Main.Mod.do_ajax('forms/get_created_by/spot/' + id, 'GET', function(data){  
           
            Main.User.get_current_user();
    
            var creator = data.creator;
            
            var interval = setInterval(function(){ 
               if(Main.User.currentUser != false){
                    clearInterval(interval);
            
                    if(Main.User.currentUser == creator.created_by || Main.User.isAdmin){
                       window.location.assign(Main.Vars.host + "forms/edit/spotreport/" + id);
                    } else {
                        $('#loadingModal').modal('hide');
                        Main.Mod.show_bootbox_alert('You cannot edit this spot report. You did not create it.','Message',null);
                    }
               }
            },1000);
            
        },null);      
    }
    
    function remove_interval(){
    
    }
    
    function delete_spot_report(id,attachment){
        Main.Mod.show_modal('loadingModal',750,-50);
          
        Main.Mod.do_ajax('forms/get_created_by/spot/' + id, 'GET', function(data){  
           
            Main.User.get_current_user();
    
            var creator = data.creator;
            
            var interval = setInterval(function(){ 
               if(Main.User.currentUser != false){
                    clearInterval(interval);
                    $('#loadingModal').modal('hide');
                    //if the user is the one who creates it and or admin, allow delete
                    if(Main.User.currentUser == creator.created_by || Main.User.isAdmin){
                            if(attachment != 'NONE'){
                                Main.Mod.show_bootbox_alert('Cannot delete spot report. This was attached to another case report. Remove attachment first.',
                                                           'Message',null);
                            } else {
                                Main.Mod.show_bootbox_confirm('Are you sure you want to delete this report?', 'Confirmation', function(){

                                    var callback = function(data){
                                        if(data.success){
                                            Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                                                location.reload();
                                            });

                                        }else{
                                            Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                                        }
                                    }
                                   Main.Mod.do_ajax('forms/delete/spotreport/' + id, 'GET', callback, null);
                                },null);
                            }
                    } else {
                       
                        Main.Mod.show_bootbox_alert('You cannot delete this spot report. You did not create it.','Message',null);
                    }
               }
            },1000); //end interval
            
        },null);          
    }
    
    function unattach_spotreport(id, case_id){
        Main.Mod.show_modal('loadingModal',750,-50);
        
        Main.Mod.do_ajax('forms/get_created_by/spot/' + id, 'GET', function(data){  
           
            Main.User.get_current_user();
    
            var creator = data.creator;
            
            var interval = setInterval(function(){ 
               if(Main.User.currentUser != false){
                    clearInterval(interval);
            
                    if(Main.User.currentUser == creator.created_by || Main.User.isAdmin){
                        $('#loadingModal').modal('hide');
                        
                        Main.Mod.show_bootbox_confirm("Are you sure you want to unattach this spot report?","Confirmation",function(){
                            Main.Mod.show_modal('loadingModal',750,-50);           
                           
                                var formdata = new FormData();
                                formdata.append('id', id);
                                formdata.append('case_id', case_id);
                                Main.Mod.do_ajax('forms/unattach/spot', 'POST', function(data){ 
                                    if(data.success){
                                        Main.Mod.show_bootbox_alert('Spot report has been unattached!',
                                                                   'Message', function(){
                                            location.reload();
                                        });

                                    } else {
                                        Main.Mod.show_bootbox_alert('Unable to unattach spot report.',
                                                                   'Message', null);
                                    }      
                                },formdata);
                            
                        },null);
                   
                        
                    } else {
                        $('#loadingModal').modal('hide');
                        Main.Mod.show_bootbox_alert('You cannot unattach this spot report. You did not create it.','Message',null);
                    }
               }
            },1000);
            
        },null);      
    }
    
    
    function on_show_spot_reports(){
        $('#viewSpotReports').on('click',function() {
         Main.Mod.show_modal('showSpotReportsModal',1100,-250);
            
            
         $('#spot_reports_tbl').DataTable({
                //so that all rows are not rendered on init time.
                "deferRender": true,
                "autoWidth": false,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                //for reinitialisation purpose
                destroy: true,
                "ajax" : {
                    url :  Main.Vars.host + "forms/getall/spotreport",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 4,5 ] },
                        { "sWidth": "15%", "aTargets": [ 0 ] },
                        { "sWidth": "20%", "aTargets": [ 1 ] },
                        { "sWidth": "18%", "aTargets": [ 2 ] },
                        { "sWidth": "14%", "aTargets": [ 3 ] },	
                        { "sWidth": "12%", "aTargets": [ 4 ] },	
                        { "sWidth": "21%", "aTargets": [ 5 ] },								
                ],
                "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                    $(nRow).attr('data-attach', aData.attached_to);
                },
                "columns": [
                    { 'data' : 'spot_report_id'},
                    { 
                        "render": function(data,type,row,meta) {                  
                            var str = row.delivered_by_info.rank + ' ' + row.delivered_by_info.first_name + ' ' + row.delivered_by_info.last_name;
                           
               
                            return str;
                        } 
                    },
                    { 
                        "render": function(data,type,row,meta) {                  
                            var str = row.created_by_info.rank + ' ' + row.created_by_info.first_name + ' ' + row.created_by_info.last_name;
               
                                
                            return str;
                        } 
                    },
                    { 'data' : 'date'},
                    { 'data' : 'attached_to'},
                    { 
                        "render": function(data,type,row,meta) {                  
                            str = '<button type="button" data-action="edit" style="margin-right: 5px;margin-left:10px;" title="Edit" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-edit"></i></button>' +
                                '<button data-action="view" style="margin-right: 5px;" title="View" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-eye"></i></button>' +
                                '<button data-action="delete" style="margin-right: 5px;" title="Delete" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-trash"></i></button>'; 
                            if(row.attached_to != "NONE"){
                                 str += '<button data-action="unattach" title="Unattach" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-chain-broken"></i></button>';              
                            }
                            return str;
                        }
                    },        
                ],
                "order": [[ 0, "desc" ]],
                sPaginationType: "full_numbers",
            });
    
            
        });
    }
    
    function on_form_saved(){
        $("#save_spot_report_form").on('submit',(function(e){
            e.preventDefault();
            
            if(SPR.Vars.delivery_boy === ""){
                Main.Mod.show_bootbox_alert('Provide delivered by field.', 'Error', null);     
                 
            }else{
                var formdata = new FormData(this);
                formdata.append('spot_report_id', $('#spot_id').text());
                formdata.append('content', $('.summernote').code());
                formdata.append('delivered_by', SPR.Vars.delivery_boy);

                var callback = function(data){

                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                            location.reload();
                        });

                    }else{
                        Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                    }
                }
                Main.Mod.do_ajax('forms/save/spotreport', 'POST', callback , formdata);
            }    
         }));
    }
    
    function on_show_delivery_boys(){
        $('#showDeliveryBoysButton').on('click',function(){
           Main.Mod.show_modal('addDeliveryBoyModal',1000,-200);
        
            $('#pnp_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "casereport/activepnps",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "25%", "aTargets": [ 0 ] },
                        { "sWidth": "25%", "aTargets": [ 1 ] },
                        { "sWidth": "35%", "aTargets": [ 2 ] },
                        { "sWidth": "10%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 3 ] },
                ],
                "columns": [
                    { 
                        "data": "pnp_id",


                    },	
                    {
                        "data": "rank",	

                    },
                    { 	
                        "render": function(data,type,row,meta) {

                            return row.first_name + " " + row.last_name;
                        }

                    },
                    { 

                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';

                        }
                    },
                    {
                        data: null,
                        className: "center",
                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            return  '<button data-name="' + row.first_name + ' ' + row.last_name +' "data-value="' +  row.pnp_id +'" style="img style="padding-left: 0px; padding-right:0px;" class="btn btn-primary btn-md action"><span class="glyphicon glyphicon-plus"></span></button>';

                        }


                    }	
                ],

                sPaginationType: "full_numbers",

            });    
            
            $('#pnp_tbl tbody').off().on( 'click', '.action', function () {
			    var selected_id = $(this).attr('data-value');
                var name = $(this).attr('data-name');
                SPR.Vars.delivery_boy = selected_id;
                $('#delivery_boy_name').val(name);
                $('#addDeliveryBoyModal').modal('hide');          
            });
        
        });
    }
    
    var attach = function attach(type){
        switch(type){
            case events_const.ON_FORM_SAVED:
                on_form_saved();
                break;
            case events_const.ON_SHOW_DELIVERY_BOYS:
                on_show_delivery_boys();
                break;
            case events_const.ON_VIEW_SPOT_REPORTS:
                on_show_spot_reports();
                break;
            case events_const.ON_TABLE_BUTTONS_CLICKED:
                on_table_buttons_clicked();
                break;
            default:
                alert('Cannot attach events. This is an error, contact your IT.');
        }
    }

    return {
        attach : attach,
        events_const : events_const,
    }
})();