
var MAM = MAM || {};

MAM.Vars = (function(){

    var polices = new Array();
    var has_unattach_main = false;
    var has_unattach_supp = false;
    var type;
    
    
    return {     
        polices : polices, 
        has_unattach_main : has_unattach_main,
        has_unattach_supp : has_unattach_supp,
    }
    
})();

MAM.Main = (function (){
   var onLoad = function onLoad(){
        initSettings();
        init_summernote();
        initAffidavits();
        MAM.Events.attach(MAM.Events.events_const.ON_FORM_MAIN_SUBMITTED);
        MAM.Events.attach(MAM.Events.events_const.ON_FORM_SUPPLEMENTAL_SUBMITTED);
        MAM.Events.attach(MAM.Events.events_const.ON_ADD_NARRATOR);
        MAM.Events.attach(MAM.Events.events_const.ON_REMOVE_NARRATOR);
        MAM.Events.attach(MAM.Events.events_const.ON_VIEW_NARRATOR);
        MAM.Events.attach(MAM.Events.events_const.ON_TABLE_BUTTONS_CLICKED); 
       
        init_id();
       
        $.contextMenu({
            selector: '.item-context2',
            callback: function(key, options) {
               // CM.InventoryDetails.context_action(key, options);
                MAM.Events.context_action2(key,options);
            },
            items: {
                "edit": {name: "Edit", icon: 'edit'},
                "unattach": {name: "Unattach", icon: 'unattach'}, 
            }
        });
       
        //context menu
        $.contextMenu({
            selector: '.item-context',
            callback: function(key, options) {
               // CM.InventoryDetails.context_action(key, options);
                MAM.Events.context_action(key,options);
            },
            items: {
                "edit": {name: "Edit", icon: 'edit'},
                "unattach": {name: "Unattach", icon: 'unattach'}, 
            }
        });
        
   }
   
   function initAffidavits(){
        $('#main_affidavits_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for reinitialisation purpose
            destroy: true,
            "ajax" : {
                url :  Main.Vars.host + "forms/getall_affidavits/main",
                type : "GET",
            },
            "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4 ] },
                    { "sWidth": "20%", "aTargets": [ 0 ] },
                    { "sWidth": "20%", "aTargets": [ 1 ] },
                    { "sWidth": "15%", "aTargets": [ 2 ] },
                    { "sWidth": "25%", "aTargets": [ 3 ] },
                    { "sWidth": "10%", "aTargets": [ 4 ] },

            ],
             "fnCreatedRow"  : function( nRow, aData, iDataIndex ){

                    //add attributes
                 $(nRow).attr('data-attach', aData.attached_to);
                 if($(nRow).attr('data-attach') == 'NONE'){
                    MAM.Vars.has_unattach_main = true;
                 };
                 $(nRow).addClass('item-context');	

            },
            "columns": [
                { 'data' : 'affidavit_id' },
                { 'data' : 'date_created' },
                { 'data' : 'attached_to' },
                { "render": function(data,type,row,meta) {
                        var str = '<ul style="list-style:none; margin-left:-20px;">';
                        $.each(row.narrators, function(i, obj){                            
                            str += '<li>' + obj.narratorName + '</li>';                        
                        });
                        str += '</ul>';
                        return str;

                    }
                },
                { 
                    "render": function(data,type,row,meta) {                  
                        str = '<button data-action="view" style="margin-right: 5px;" title="View" class="btn btn-info action"' +
                                'data-value="' + row.affidavit_id + '"><i class="fa fa-eye"></i></button>' +
                            '<button data-action="delete" style="margin-right: 5px;" title="Delete" class="btn btn-info action"'+
                            'data-value="' + row.affidavit_id + '"><i class="fa fa-trash"></i></button>'; 
                        return str;
                    }

                },

            ],
          
            "order": [[ 0, "desc" ]],
            sPaginationType: "full_numbers",

        });

        $('#supplemental_affidavits_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for reinitialisation purpose
            destroy: true,
            "ajax" : {
                url :  Main.Vars.host + "forms/getall_affidavits/supplemental",
                type : "GET",
            },
            "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 4 ] },
                    { "sWidth": "20%", "aTargets": [ 0 ] },
                    { "sWidth": "20%", "aTargets": [ 1 ] },
                    { "sWidth": "15%", "aTargets": [ 2 ] },
                    { "sWidth": "25%", "aTargets": [ 3 ] },
                    { "sWidth": "10%", "aTargets": [ 4 ] },

            ],

            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){

                //add attributes
                $(nRow).attr('data-attach', aData.attached_to);
                $(nRow).addClass('item-context2');
                
                if($(nRow).attr('data-attach') == 'NONE'){
                    MAM.Vars.has_unattach_supp = true;
                };

            },

            "columns": [
                { 'data' : 'affidavit_id' },
                { 'data' : 'date_created' },
                { 'data' : 'attached_to' },
                { "render": function(data,type,row,meta) {
                        var str = '<ul style="list-style:none; margin-left:-20px;">';
                        $.each(row.narrators, function(i, obj){                            
                            str += '<li>' + obj.narratorName + '</li>';                        
                        });
                        str += '</ul>';
                        return str;

                    }
                },
                { 
                    "render": function(data,type,row,meta) {                  
                        str = '<button data-action="view" style="margin-right: 5px;" title="View" class="btn btn-info action"' +
                                'data-value="' + row.affidavit_id + '"><i class="fa fa-eye"></i></button>' +
                            '<button data-action="delete" style="margin-right: 5px;" title="Delete" class="btn btn-info action"'+
                            'data-value="' + row.affidavit_id + '"><i class="fa fa-trash"></i></button>'; 
                        return str;
                    }

                },

            ],  
            "initComplete" : function(){
                console.log(MAM.Vars.has_unattach_supp);
            },
            "order": [[ 0, "desc" ]],
            sPaginationType: "full_numbers",

        });
      
   }
   
   
   function initSettings(){
       
       var callback = function(data){
            if( data.data.allow_affidavits == 0 ){
                $('#affidavitMain').remove();
            } else {
                $('#affidavitDisallowed').remove();
            }
       }
       
       Main.Settings.load(callback);
   }
    
   function init_summernote(){
       /*var callback = function(){
            $('#suppContent').summernote();
            Main.Mod.do_ajax('forms/get_template/supp','GET',function(data){
                if(data.has_data){
                    console.log(data.data.supp);
                    var dom1 = $(data.data.supp);
                    $.each(dom1, function(idx,obj){
                        $('#suppContent').summernote('insertNode', obj);
                    });
                }
            },null);
       }*/
        $('#suppContent').summernote();

        $('#mainContent').summernote(); 
            Main.Mod.do_ajax('forms/get_template/main','GET',function(data){
            if(data.has_data){       
                var dom = $(data.data.main);
                $.each(dom, function(idx,obj){
                    $('#mainContent').summernote('insertNode', obj);
                });

            }
        },null);
   
   }
   
   function init_id(){
       Main.Mod.do_ajax('forms/id/affidavit', 'GET', function(data){
            $('#aff_id').text(data.id);
        }, null);
    
    }
   
   return {
        onLoad : onLoad,
   }  
})();

MAM.Events = (function (){
    
    var events_const = {    
        'ON_FORM_MAIN_SUBMITTED' : 1,
        'ON_FORM_SUPPLEMENTAL_SUBMITTED' : 2,
        'ON_ADD_NARRATOR' : 3,
        'ON_REMOVE_NARRATOR' : 4,
        'ON_VIEW_NARRATOR' : 5,
        'ON_TABLE_BUTTONS_CLICKED' : 6,
        
    }
    
    
    function on_submit_main_form(){
        
        $('#main_affidavit_form').on('submit', (function(e){
            e.preventDefault();
            if(MAM.Vars.has_unattach_main){
                Main.Mod.show_bootbox_alert('Cannot create another affidavit. There is still unattach affidavit.', 'Message', null);     
            } else {
                
               if(MAM.Vars.polices.length == 0){
                    Main.Mod.show_bootbox_alert("Please Select a Narrator", 'Error', null);
                }else{
                    Main.Mod.show_modal('loadingModal', 650, '');
                    var formData = new FormData(this);
                    formData.append('affidavit_id', $('#aff_id').text().trim());
                    formData.append('content', $('#mainContent').code());
                    formData.append('type', "main");
                    formData.append('narrators', JSON.stringify(MAM.Vars.polices));

                    var callback = function(data){
                        $('#loadingModal').modal('hide');
                        if(data.success){
                            Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                                location.reload();
                            });

                        }else{
                            Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                        }
                    }
                }

                Main.Mod.do_ajax('forms/save/affidavit', 'POST', callback , formData);
                
            
            }
            
        }));

    }
    
    function on_submit_supplemental_form(){
        
        $('#supplemental_affidavit_form').on('submit', (function(e){
            e.preventDefault();
            
            if(MAM.Vars.has_unattach_supp){
                Main.Mod.show_bootbox_alert('Cannot create another affidavit. There is still unattach affidavit.', 'Message', null);     
            } else {
                
                if(MAM.Vars.polices.length == 0){
                    Main.Mod.show_bootbox_alert("Please Select a Narrator", 'Error', null);
                }else{
                    Main.Mod.show_modal('loadingModal', 650, '');
                    var formData = new FormData(this);

                    formData.append('affidavit_id', $('#aff_id').text().trim());
                    formData.append('content', $('#suppContent').code());
                    formData.append('type', "supplemental");
                    formData.append('narrators', JSON.stringify(MAM.Vars.polices));

                    var callback = function(data){
                        $('#loadingModal').modal('hide');
                        if(data.success){
                            Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                                location.reload();
                            });

                        }else{
                            Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                        }
                    }
                }

                Main.Mod.do_ajax('forms/save/affidavit', 'POST', callback , formData);
            
            }
            
            
        }));
        
        
    }
    
    function on_add_narrator(){
        $('.addNarratorButton').on('click',function(){
           
            Main.Mod.show_modal('addNarratorModal', 1200, -300);
            
            $('#police_tbl').DataTable({
                
                "deferRender": true,
                "autoWidth": false,
                
                destroy: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                
                "ajax" : {
                        url :  Main.Vars.host + "casereport/activepnps",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 3,4 ] },
                            { "sWidth": "25%", "aTargets": [ 0 ] },
                            { "sWidth": "25%", "aTargets": [ 1 ] },
                            { "sWidth": "35%", "aTargets": [ 2 ] },
                            { "sWidth": "10%", "aTargets": [ 3 ] },
                            { "sWidth": "5%", "aTargets": [ 4 ] },

                    ],
                    "columns": [
                        { 
                            "data": "pnp_id",


                        },	
                        {
                            "data": "rank",	

                        },
                        { 	
                            "render": function(data,type,row,meta) {

                                return row.first_name + " " + row.last_name;
                            }

                        },
                        { 

                            "render": function(data,type,row,meta) {
                                //console.log(base_path + row.file_path_thumb);
                                return '<img style="margin-left:20px;" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path) + '_thumb.jpg' + ' />';

                            }
                        },
                        {
                            data: null,
                            className: "center",
                            "render": function(data,type,row,meta) {
                                //console.log(base_path + row.file_path_thumb);
                                return  '<button style="margin-left: 10px;" data-name="' + row.first_name + ' ' + row.last_name +
                                        ' "data-value="' +  row.pnp_id + '" style="img style="padding-left: 0px; padding-right:0px;' +
                                        ' class="btn btn-primary btn-md action">' +
                                        '<span class="glyphicon glyphicon-plus"></span></button>';

                            }


                        }	
                    ],

                    sPaginationType: "full_numbers",

            });
            //add event handling
            $('#police_tbl tbody').off().on( 'click', '.action', function () {
                
			    var selected_id = $(this).attr('data-value');
                var name = $(this).attr('data-name');
               
                if(MAM.Vars.polices.indexOf(selected_id) > -1){
                    Main.Mod.show_bootbox_alert('Already in the list.', 'Message', null);
                } else {
                    MAM.Vars.polices.push(selected_id);
                    if(MAM.Vars.polices.length == 1){
                        $('.narratorList').empty();
                        $('.narratorList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name +
                                                 '<span class="pull-right">' + '<button type="button" data-value="' + selected_id +
                                                 '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                    } else {
                        $('.narratorList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name  +
                                                 '<span class="pull-right">' + '<button type="button" data-value="' + selected_id +
                                                 '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                    }

                    $('#addNarratorModal').modal('hide');
                }
               
            });
            
            on_remove_narrator();
            
      });
    }
    

    function on_remove_narrator(){
        $('.narratorList').off('click').on('click','button',function(){
            
            var id = $(this).attr('data-value');
            var parent_li = $(this).parent().parent();
            parent_li.remove();	
            
            //remove the item in array
            var i = MAM.Vars.polices.indexOf(id);
            //actual removing          
            MAM.Vars.polices.splice(i,1);
            
            if(MAM.Vars.polices.length == 0){
                $('.narratorList').append('<li class="list-group-item"> No police yet.</li>');
            }
            
        });  
    }
    
    function on_affidavit_view(){
        $('#viewAffidavits').on('click', function(){
            Main.Mod.show_modal('showAffidavitsModal', 1200, -300);
              
           
        });
    }
    
    function on_table_buttons_clicked(){
        $('#main_affidavits_tbl').off('click').on('click','.action',function(e){
            var id = $(this).attr('data-value').trim();
    
            var action = $(this).attr('data-action');
            
            switch(action){
                case 'view':   
                    view_affidavit(id);
                    break;
                
                case 'delete':
                    var attached = $(this).closest('tr').attr('data-attach');
                    delete_affidavit(id, attached);
                    break;
                
                default: 
                    alert('Cannot handle event. Contact IT Admini');
            }
        
        });    
        
        $('#supplemental_affidavits_tbl').off('click').on('click','.action',function(e){
            var id = $(this).attr('data-value').trim();
    
            var action = $(this).attr('data-action');
            
            switch(action){
                case 'view':   
                    view_affidavit(id);
                    break;
                
                case 'delete':
                    var attached = $(this).closest('tr').attr('data-attach');
                    delete_affidavit(id,attached);
                    break;
                
                default: 
                    alert('Cannot handle event. Contact IT Admini');
            }
        
        }); 
      
    }
    
    function view_affidavit(id){
            
        $('#showAffidavitsModal').modal('hide');
           
        window.open(Main.Vars.host + "forms/affidavit_pdf/" + id , '_blank');
        

    }
    
    function unattach_supp(id, case_id){
        if( case_id == 'NONE') {
            Main.Mod.show_bootbox_alert('Nothing to unattach.','Message', null);    
        } else {
            var callback = function(){
                var formdata = new FormData();
                formdata.append('id', id);
                formdata.append('case_id', case_id);
                Main.Mod.do_ajax('forms/unattach/supp', 'POST', function(data){             
                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message,'Message',null);
                        window.location.reload();
                    } else {
                        Main.Mod.show_bootbox_alert(data.message,'Message',null);
                    }
                    
                },formdata);
                
            }
             Main.Mod.show_bootbox_confirm('Are you sure you want to unattach this item?',
                                         'Confirmation', callback); 
        }
    }
    
    function unattach_main(id, case_id){
        if( case_id == 'NONE'){
            Main.Mod.show_bootbox_alert('Nothing to unattach.','Message', null);    
        } else {
            var callback = function(){
                var formdata = new FormData();
                formdata.append('id', id);
                formdata.append('case_id', case_id);
                Main.Mod.do_ajax('forms/unattach/main', 'POST', function(data){             
                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message,'Message',null);
                        window.location.reload();
                    } else {
                        Main.Mod.show_bootbox_alert(data.message,'Message',null);
                    }
                    
                },formdata);
                
            }
            
             Main.Mod.show_bootbox_confirm('Are you sure you want to unattach this item?',
                                         'Confirmation', callback);
        }
    }
    
    function edit_affidavit(id,type){
        
        var conf_callback = function(){
            window.location.assign(Main.Vars.host + "forms/edit/main/" + id);
        }
        
        Main.Mod.show_bootbox_confirm('Are you sure you want to edit this affidavit?','Confirmaton',
                                      conf_callback,null);
     
    }
    
    function delete_affidavit(id, case_id){
        if(case_id != 'NONE'){
            Main.Mod.show_bootbox_alert('Cannot delete affidavit attached to a case.','Error', null);
        } else {
            
            Main.Mod.show_bootbox_confirm('Are you sure you want to delete this?','Confirmation', function(){

                var callback = function(data){
                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                            location.reload();
                       });
                    } else {
                        $('#loadingModal').modal('hide');
                        Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                    }
                }
                Main.Mod.do_ajax('forms/delete/affidavit/' + id, 'GET', callback, null);      
            
            },null);
        
        }

    }
    
    var attach = function attach(type){
        switch(type){
            case events_const.ON_FORM_MAIN_SUBMITTED:
                on_submit_main_form();
                break;
            case events_const.ON_FORM_SUPPLEMENTAL_SUBMITTED:
                on_submit_supplemental_form();
                break;
            case events_const.ON_ADD_NARRATOR:
                on_add_narrator();
                break;
            case events_const.ON_REMOVE_NARRATOR:
                on_add_narrator();
                break;
            case events_const.ON_VIEW_NARRATOR:
                on_affidavit_view();
                break;
            case events_const.ON_TABLE_BUTTONS_CLICKED:
                on_table_buttons_clicked();
                break;
            default:
                alert('Cannot attach events. This is an error, contact your IT.');
        }
    }
    
    //right-click menu events handling in main affidavit
	var context_action = function context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
        
        
        switch (key) {
            case 'unattach': 
                var case_id = options.$trigger.attr('data-attach');
                unattach_main(item_id, case_id);
                break;
            case 'edit':
                edit_affidavit(item_id,'main');          
                break;
        }
    }
    
    var context_action2 = function context_action2(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
        
        
        switch (key) {
            case 'unattach':
                var case_id = options.$trigger.attr('data-attach');
                unattach_supp(item_id, case_id);
                break;
                
            case 'edit':
                edit_affidavit(item_id,'supp');          
                break;
        }
    }
    
    return {
        attach : attach,
        events_const : events_const,
        context_action : context_action,
        context_action2 : context_action2,
    }
})();