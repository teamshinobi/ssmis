var ESPR = ESPR || {};

ESPR.Vars = (function(){
    
    var id  = '';
     
    return {
        id : id,
       
    }
})();
ESPR.Main = (function(){
    
    var onLoad = function onLoad(){
        ESPR.Vars.id = $('#spot_id').text().trim();
        ESPR.Events.attach(ESPR.Events.events_const.ON_FORM_SAVED);
        init();
    }
    
    function init(){     
        var callback = function(data){
            $('.summernote').summernote();
            $(".summernote").code(data.data.content);
            $('#delivery_boy_name').val(data.data.delivered_by_info.first_name + ' ' + data.data.delivered_by_info.last_name);
            SPR.Vars.delivery_boy = data.data.delivered_by;
       }
        Main.Mod.do_ajax('forms/get/spotreport/' + ESPR.Vars.id, 'GET', callback, null); 
    }
    
    
    return {
        onLoad : onLoad,
    }
})();
    
    
ESPR.Events = (function(){
    
    var events_const = {
        'ON_FORM_SAVED' : 1
    }
    
    function on_form_saved(){
        $("#update_spotreport_form").on('submit',(function(e){
            e.preventDefault();
            if(SPR.Vars.delivery_boy === ""){
                Main.Mod.show_bootbox_alert('Provide delivered by field.', 'Error', null);     
                 
            }else{
                var formdata = new FormData(this);
                formdata.append('spot_report_id', $('#spot_id').text());
                formdata.append('content', $('.summernote').code());
                formdata.append('delivered_by', SPR.Vars.delivery_boy);
                var callback = function(data){

                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                            location.reload();
                        });

                    }else{
                        Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                    }
                }
                Main.Mod.do_ajax('forms/update/spotreport', 'POST', callback , formdata);
            
            }
        }));
    }
    var attach = function attach(type){
          switch(type){
            case events_const.ON_FORM_SAVED:
                on_form_saved();
                break;
            default:
                alert('Cannot attach events. This is an error, contact your IT.');
        }
    }
    
    return {
        attach : attach,
        events_const : events_const,
    }
    
})();