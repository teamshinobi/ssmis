var PM = PM || {};

PM.Objects = (function(){
    var targets = new Array();
    var selected_pc = {};
    var has_unattach = false;
    return {    
        targets : targets,
        selected_pc : selected_pc,
        has_unattach : has_unattach,
    
    }
})();
PM.Main = (function(){
    
    var modal_const = {
        VIEW_ALL : 1,
        ADD_TARGET : 2,
       
    }
    
    var onLoad = function onLoad(){
    
        init_id();
        initSettings();
        init_pre_op_tbl();
        
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        $('#datepicker,#duration_from_date, #duration_to_date').datepicker({ 
            format: 'yyyy-mm-dd',
            startDate: today,   
            endDate: '+1d',  
            autoClose: true,
        });
  
        var timepickers = ['#timepicker_time','#timepicker_to', '#timepicker_from'];
        Main.Init.InitTimePicker(timepickers);
        
        PM.Events.attach(PM.Events.events_const.ON_FORM_SAVED);
        PM.Events.attach(PM.Events.events_const.ON_VIEW_PREOPERATIONS);
        PM.Events.attach(PM.Events.events_const.IMPORT_TARGET);
        PM.Events.attach(PM.Events.events_const.ON_REMOVE_TARGET);
        
        //initiliaze context menu base on user credentials
        Main.User2.loadUserCredentials(function(data){
            
           if(data.is_admin){
                //context menu
                $.contextMenu({
                    selector: '.item-context',
                    callback: function(key, options) {
                       // CM.InventoryDetails.context_action(key, options);
                        PM.Events.attach(PM.Events.events_const.CONTEXT_ACTION,key,options);
                    },
                    items: {
                        "approve": {name: "Mark as approved", icon: 'approve'},
                        "unapprove": {name: "Mark as not approved", icon: 'unapprove'},
                        "view": {name: "View", icon: 'view'},
                        "edit": {name: "Edit", icon: 'edit'},
                        "delete": {name: "Delete", icon: 'delete'},   
                        "unattach": {name: "Unattach", icon: 'unattach'},   
                        "deny": {name: "Mark as denied", icon: 'deny'}, 
                        "undeny": {name: "Remove denied mark", icon: 'undeny'},   
                    }
                });
           } else {
               $.contextMenu({
                    selector: '.item-context',
                    callback: function(key, options) {
                       // CM.InventoryDetails.context_action(key, options);
                        PM.Events.attach(PM.Events.events_const.CONTEXT_ACTION,key,options);
                    },
                    items: {
                        "view": {name: "View", icon: 'view'},
                        "edit": {name: "Edit", icon: 'edit'},
                    }
                });
           }
            
        });
        
       
    }
    
    
 
    function initSettings(){

        var callback = function(data){
            if( data.data.allow_coord_preop == 0 ){
                $('#content-container').remove();
            } else {
                $('#disallowed').remove();
            }
        }

        Main.Settings.load(callback);
    }
    
    var on_modal_showing = function on_modal_showing(type){
        switch(type){
            case modal_const.ADD_TARGET:
                Main.Mod.show_modal('addTargetPersonModal',600,'');
                PM.Events.attach(PM.Events.events_const.ADD_TARGET);
              
                break;
            case modal_const.VIEW_ALL:
                Main.Mod.show_modal('viewPreoperationsModal',1200,-300);
                break;
            default: 
                alert('Unable to show modal! Contact your IT Programmer.');
        
        }
    }
    
    
    function init_id(){
        
        var callback = function(data){
            $('#pre_coord_id').text(data.id);    
        }
        //callback for first load of target id
        var callback2 = function(data){
            $('#target_id').text(data.id);  
        }
        Main.Mod.do_ajax('forms/id/preoperation', 'GET', callback ,null);
        Main.Mod.do_ajax('forms/targetid', 'GET', callback2 ,null);
    }
    
    function init_pre_op_tbl(){
        
		$('#preoperations_tbl').DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			//for reinitialisation purpose
			destroy: true,
			"ajax" : {
				url :  Main.Vars.host + "forms/getall/preoperation",
				type : "GET",
			},
			"aoColumnDefs": [
                { "bVisible": false, "aTargets": [ 7 ] },
                { "bSortable": false, "aTargets": [ 5 ] },
                { "sWidth": "10%", "aTargets": [ 0 ] },
                { "sWidth": "10%", "aTargets": [ 1 ] },
                { "sWidth": "8%", "aTargets": [ 2 ] },
                { "sWidth": "7%", "aTargets": [ 3 ] },
                { "sWidth": "10%", "aTargets": [ 4 ] },
                { "sWidth": "20%", "aTargets": [ 5 ] },
                { "sWidth": "10%", "aTargets": [ 6 ] },
                { "sWidth": "5%", "aTargets": [ 7 ] },							
			],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                $(nRow).addClass('item-context');	
                if(aData.has_approved == 1){
                    $(nRow).addClass('approved-row');	
                }         
                
                if(aData.denied == 1){
                    $(nRow).addClass('approved-denied');	
                }
                
                $(nRow).attr('data-attach', aData.attached_to);
                
                if( $(nRow).attr('data-attach') == 'NONE'){
                    PM.Objects.has_unattach = true;
                }
            },
			"columns": [
                { 'data' : 'pre_op_id'},
                {'data' : 'control_no' },
                { 'data' : 'date' },
                { 'data' : 'time'},
                { 
                    "render": function(data,type,row,meta) { 
                            return row.created_by_info.rank + ' ' + row.created_by_info.first_name + ' ' + row.created_by_info.last_name;
                    }
                },
                { 'data' : 'summary'},
                { 
                    "render": function(data,type,row,meta) {  
                        return row.attached_to;
                    }
                }, 
                { 
                    "render": function(data,type,row,meta) {  
                       if(row.has_approved == 1){
                            return 'Approved';
                       } else {                      
                            return 'Not';
                       }
                    } 
                },
			],
            "initComplete" : function(){
                console.log(PM.Objects.has_unattach);
            },
            "order": [[ 0, "desc" ]],
			sPaginationType: "full_numbers",
            
		});
    }

    return {
		onLoad : onLoad,
		on_modal_showing : on_modal_showing,
		modal_const : modal_const,
	}

})();


PM.Events = (function(){

    var events_const = {
        'ON_FORM_SAVED' : 1,
        'ADD_TARGET' : 2,
        'ON_REMOVE_TARGET' : 3,
        'ON_VIEW_PREOPERATIONS' : 4,
        'ON_APPROVE_PREOPERATION' : 5,
        'ON_TABLE_BUTTON_CLICKED' : 6,
        'CONTEXT_ACTION' : 7,
        'IMPORT_TARGET' : 8,
    }
    function on_table_row_clicked(){
        //add row event handling
        $('#preoperations_tbl tbody').off('click').on('click','tr',function(){
          
        });
    
    }
    function on_view_preoperations() {
        $('#viewPreoperations').on('click',function(){
            PM.Main.on_modal_showing(PM.Main.modal_const.VIEW_ALL);
        });
    }
    function on_remove_target(){
        $('#targetsList').off('click').on('click','button',function(){
            
            var id = $(this).attr('data-value');
            var parent_li = $(this).parent().parent();
            parent_li.remove();	
            
            //remove the item in array
            var i = Main.Objects.index_of(PM.Objects.targets,id,'target_id');
          
            var removed = PM.Objects.targets.splice(i,1)[0];
            
            //recycle id
            $('#target_id').text(removed.target_id);
            
            if(PM.Objects.targets.length == 0){
                $('#targetsList').append('<li class="list-group-item"> No target persons yet.</li>');
            }
        });  
    }
    function on_add_target(){
        $('#addTargetPersonButton').off('click').on('click',function(){
            
            var target = {};
            target.full_name = $('#full_name').val();
            target.alias = $('#alias').val();
            target.target_id = $('#target_id').text();
            
            if($('#is_filipino').is(":checked")){
                target.nationality = 'Filipino';
            }else{
                target.nationality = $('#others_nationality').val();
            }
          
            PM.Objects.targets.push(target);
            
            //remove the default message in the list
            if(PM.Objects.targets.length == 1){
                $('#targetsList').empty();
                $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
		     }else{
                $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');					      
            }
            //ajax next id
            var callback = function(data){
                $('#target_id').text(data.id);
            }
            
            Main.Mod.do_ajax('forms/targetnextid/' + $('#target_id').text(),'GET', callback, null);
            $('#addTargetPersonModal').modal('hide');
        });
    }
    
    
    function on_form_saved(){
        //handle form submission
        $("#save_precooperation_form").on('submit',(function(e){
            e.preventDefault();
            if(PM.Objects.has_unattach){
                 Main.Mod.show_bootbox_alert('Cannot create another pre-operation report. There is still unattach pre-operation.', 'Message', null);    
            } else {
                var result = validate_form();

                if(result.isOk){
                    Main.Mod.show_modal('loadingModal', 650, '');
                    var formdata = new FormData(this);
                    formdata.append('pre_op_id', $('#pre_coord_id').text());
                    formdata.append('targets', JSON.stringify(PM.Objects.targets));
                    //nakadisable kasi eh.
                    formdata.append('control_no', '');


                    var callback = function(data){
                        $('#loadingModal').modal('hide');
                        if(data.success){
                            var callback = function () {
                                 Main.Mod.show_bootbox_alert("You will be redirected to Coordination Form.", 'Message', function(){
                                    window.location.assign(Main.Vars.host +  'forms/coordination/');
                                 });     
                            }
                            Main.Mod.show_bootbox_alert(data.message, 'Message', callback);     
                        }else{
                            Main.Mod.show_bootbox_alert(data.message, 'Message', null);  
                        }

                    }

                    Main.Mod.do_ajax('forms/save/preoperation', 'POST', callback, formdata);
                } else {
                    var message = '<ul>';

                    $.each(result.messages,function(idx,obj){
                        message += '<li>' + obj + '</li>';
                    });

                    message += '</ul>';

                    Main.Mod.show_bootbox_alert(message, 'Please complete the following: ', null);
                }
            } //END ELSE
 
        }));
    }
    
    //returns result OBJECT
    function validate_form(){
            
        var toReturn = true;
        var vMessages = new Array();
        if($('#summary').val() == ''){
            vMessages.push("Summary cannot be empty.");
            toReturn = false;
        }
        
        if($('#date').val() == '' || $('#duration_from').val() == '' || $('#duration_to').val() == '' ){
            vMessages.push("Dates cannot be empty.");
            toReturn = false;
        }
        
        if($('#target_location').val() == ''){
            vMessages.push("Target location cannot be empty.");
            toReturn = false;
        }
        
        if(PM.Objects.targets.length < 1){
            vMessages.push("Provide atleast 1 target.");
            toReturn = false;
        }
        
        
        var result = {};
        result.isOk = toReturn;
        result.messages = vMessages;
        
        return result;
    }

    function import_target(){
        $('#importTargetButton').on('click',function(){
            Main.Mod.show_modal('importTargetModal', 1200,-280);
            init_imports();
            addImportListener();
        }); 
    }
    
    function init_imports(){
        $('#imports_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "tip/get_shown_targets",
                    type : "GET",
                },
                "aoColumnDefs": [
                       { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "5%", "aTargets": [ 0 ] },
                        { "sWidth": "40%", "aTargets": [ 1 ] },
                        { "sWidth": "20%", "aTargets": [ 2 ] },
                        { "sWidth": "20%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 4 ] },
                ],
                "order": [[ 0, "DESC" ]],
               
                "columns": [
                    { 
                        "data": "id",
                    },
                    
                    { 
                        "data": "full_name",
                    },	
                    { 
                        "data": "nationality",
                    },	   	
                    {
                        "data": "alias",
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = '<button style="margin-left:10px;" class="btn btn-default btn-sm" ' +
                                        '" data-fname="' + row.full_name +
                                        '" data-nationality="' + row.nationality + 
                                        '" data-alias="' + row.alias + '">';
                                str += '<i class="fa fa-plus"></i>';
                                str += '</button>';
                           
                            return str;

                        }

                    }
                ],

                sPaginationType: "full_numbers",

		});
    }
    
    function addImportListener(){
        $('#imports_tbl').off('click').delegate('button','click',function(){
            var button = $(this);
             
            var callback = function(data){
                var target = {};
            
                target.target_id = $('#target_id').text();
                target.full_name = button.attr('data-fname');
                target.nationality = button.attr('data-nationality');
                target.alias = button.attr('data-alias');
                
                $('#target_id').text(data.id);
                
                PM.Objects.targets.push(target);
                
                //render on screen
                if(PM.Objects.targets.length == 1){
                    $('#targetsList').empty();
                    $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
                        '<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                 }else{
                    $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
                        '<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');					      
                }
                
                $('#importTargetModal').modal('hide');
            } //end ajax callback
            
            Main.Mod.do_ajax('forms/targetnextid/' + $('#target_id').text(),'GET', callback, null);   
           
        });
    }
    /*====================================================================================
    *                               CONTEXT ACTION
    *  
    *===================================================================================*/
    function approve(id){
        Main.Mod.show_modal('showControlInputModal', 650, '');
        $('#continueApprovingButton').off('click').on('click',function(){
             
            var callback = function(){

                var callback = function(data){
                    Main.Mod.show_bootbox_alert(data.message, 'Message',function(){
                      $('#viewPreoperationsModal').modal('hide'); 
                    });      
                }

                Main.Mod.do_ajax('forms/approve/' + id + '/preoperation/' + $('#cn').val().trim() , 'GET' , callback, null);
            };

            Main.Mod.show_bootbox_confirm("Are you sure you want to mark this as approved?", "Confirmation", callback,null);
        
        });
       
    }
    
    
    function unapprove(id){
        var callback = function(){

        var callback = function(data){
            Main.Mod.show_bootbox_alert(data.message, 'Message',function(){
                  $('#viewPreoperationsModal').modal('hide'); 
              });      
            }

            Main.Mod.do_ajax('forms/unapprove/' + id + '/preoperation' , 'GET' , callback, null);
        };

        Main.Mod.show_bootbox_confirm("Are you sure you want to mark this as unapproved?", "Confirmation", callback,null);
    
    }
    function del(id, case_id){
        if(case_id != 'NONE'){
            Main.Mod.show_bootbox_alert('Cannot delete preoperation report. This was attached to another case report. Remove attachment first.',
                                   'Message',null);
       } else {
            Main.Mod.do_ajax('forms/get_created_by/preoperation/' + id, 'GET', function(data){  
                var creator = data.creator.created_by;
                Main.Mod.show_modal('loadingModal',750,-50);
                Main.User.get_current_user();
                var interval = setInterval(function(){ 
                    if(Main.User.currentUser != false){
                        $('#loadingModal').modal('hide');
                        clearInterval(interval);
                        if(Main.User.currentUser == creator || Main.User.isAdmin){

                            Main.Mod.show_bootbox_confirm('Are you sure you want to delete this preoperation report?', 'Confirmation', function(){
                                Main.Mod.show_modal('loadingModal',750,-50);
                                var callback = function(data){
                                    if(data.success){
                                        $('#loadingModal').modal('hide');
                                        Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                                            location.reload();
                                        });

                                    }else{
                                        $('#loadingModal').modal('hide');
                                        Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                                    }
                                }

                               Main.Mod.do_ajax('forms/delete/preoperation/' + id, 'GET', callback, null);

                            },null);


                        } else {
                            $('#loadingModal').modal('hide');
                            Main.Mod.show_bootbox_alert('You cannot delete this preoperation report. You did not create it.','Message',null);
                        }
                    }
                },1000);
            },null);
       }
    }
    
    function edit(id){
        
        Main.Mod.show_modal('loadingModal',750,-50);
        
        Main.Mod.do_ajax('forms/get_created_by/preoperation/' + id, 'GET', function(data){  
            
            Main.User.get_current_user();
    
            var creator = data.creator.created_by;
            
            var interval = setInterval(function(){ 
               if(Main.User.currentUser != false){
                    clearInterval(interval);
                    if(Main.User.currentUser == creator || Main.User.isAdmin){
                        window.location.assign(Main.Vars.host + "forms/edit/preoperation/" + id);
                    } else {
                        $('#loadingModal').modal('hide');
                        Main.Mod.show_bootbox_alert('You cannot edit this preoperation report. You did not create it.','Message',null);
                    }
               }
            },1000);
            
        },null);
        
    }
    
    function view(id){
        window.open(Main.Vars.host + "forms/preoperation_pdf/" + id , '_blank');
    }
    
    function unattach(id, case_id){
      if(case_id == 'NONE'){
            Main.Mod.show_bootbox_alert('There is nothing to unattach.', 'Message', null);
       } else {
            var callback = function(){
                var formdata = new FormData();
                formdata.append('id', id);
                formdata.append('case_id', case_id);
                Main.Mod.do_ajax('forms/unattach/preoperation', 'POST', function(data){ 
                    if(data.success){
                        Main.Mod.show_bootbox_alert('Preoperation has been unattached!',
                                                   'Message', function(){
                            location.reload();
                        });
                       
                    } else {
                        Main.Mod.show_bootbox_alert('Unable to unattach preoperation report.',
                                                   'Message', null);
                    }
                },formdata);
                
            }
            
            Main.Mod.show_bootbox_confirm('Are you sure you want to unattach this item?',
                                         'Confirmation', callback);
       }
    }
    
    
    function deny(id){
       Main.Mod.do_ajax('forms/mark_as_denied/' + id + '/preoperation', 'GET',function(data){
            if(data.success){
                Main.Mod.show_bootbox_alert('Preoperation has been denied!',
                                           'Message', function(){
                    location.reload();
                });

            } else {
                Main.Mod.show_bootbox_alert('Unable to mark as deny.',
                                           'Message', null);
            }
       
       },null);
    }
    
    function undeny(id){
         Main.Mod.do_ajax('forms/mark_as_undenied/' + id + '/preoperation', 'GET',function(data){
                if(data.success){
                Main.Mod.show_bootbox_alert('Removed denied mark!',
                                           'Message', function(){
                    location.reload();
                });

            } else {
                Main.Mod.show_bootbox_alert('Unable to remove denied mark.',
                                           'Message', null);
            }
       
       },null);
    }
    
    //right-click menu events handling
	var context_action = function context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
		switch (key) {
            case 'approve' : 
                if(tr.hasClass('approved-row')){
                    Main.Mod.show_bootbox_alert("This coordination form has already been approved!", "Message", null); 
                } else {
                    approve(item_id);
                }
               break;

            case 'unapprove' :
                if(tr.hasClass('approved-row')){
                    unapprove(item_id);
                } else {
                   Main.Mod.show_bootbox_alert("This coordination form is not yet approved.", "Message", null); 

                }
               break;

            case 'view':
                 view(item_id);
                 break;

            case 'edit':
                edit(item_id);
                break;
            
            case 'delete':
                var case_id = options.$trigger.attr('data-attach');
                del(item_id,case_id);
                break;
            case 'unattach':
                var case_id = options.$trigger.attr('data-attach');
                //var act_tr = tr[1];
                unattach(item_id, case_id);
                break;
            case 'deny':
                var case_id = options.$trigger.attr('data-attach');
                
                if(case_id == 'NONE') {
                    deny(item_id);
                } else {
                    Main.Mod.show_bootbox_alert("This report is attached to a case report. Cannot mark as denied.","Message",null);
                }
             
                break; 
            case 'undeny':
                undeny(item_id);
                break;
            default :
			   break;
	   }           
	}
    
    var attach = function attach(type,key,options){
        switch(type){
            case events_const.ON_FORM_SAVED:
                on_form_saved();
                break;   
            case events_const.ADD_TARGET:
                on_add_target();
                break;  
            case events_const.ON_REMOVE_TARGET:
                on_remove_target();
                break;
            case events_const.ON_VIEW_PREOPERATIONS:
                on_view_preoperations();
                break;
            case events_const.ON_TABLE_ROW_CLICKED:
                on_table_row_clicked();
                break;
            case events_const.CONTEXT_ACTION:
                context_action(key,options);
                break;
            case events_const.IMPORT_TARGET:
                import_target();
                break;
            default:
                alert('Cannot attach event. Contact your IT support.');
        }
    }
    
    
    return {
        attach : attach,
        events_const : events_const,
    
    }

})();
  
    