var IM = IM || {};


IM.Vars = (function(){ 
  
    var item_seized_tbl = undefined;
    var nitem_seized_tbl = undefined;
    var item_violators = new Array();
    var items = new Array();
    var nitems = new Array();
    //removed item
    var items_rem = new Array();
    var nitems_rem = new Array();
    var selected_item = {};
    var selected_sOfficers = new Array();
    var aoSelected_officers = new Array();
    return {
        item_seized_tbl : item_seized_tbl,
        nitem_seized_tbl : nitem_seized_tbl,
        item_violators : item_violators,   
        items : items,
        items_rem : items_rem,
        nitems_rem : nitems_rem,
        nitems : nitems,
        selected_item : selected_item,
        selected_officers : selected_sOfficers,
        aoSelected_officers : aoSelected_officers,
    }
    
})();


IM.Main = (function(){ 
    
    var onLoad = function onLoad(){    
        init_item_seized_tbl();
        init_first_item_id();
        init_narcotics_seized_tbl();
        init_first_narcotics_item_id();
      
        //attach events
        IM.Events.attach(IM.Events.events_const.ON_ADD_ITEM);
        IM.Events.attach(IM.Events.events_const.ON_ADD_NITEM);
        IM.Events.attach(IM.Events.events_const.ON_SHOW_PNP);
        
     
        //item context menu
        $.contextMenu({
            selector: '.item-context, .nitem-context', 
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                IM.Events.attach(IM.Events.events_const.ITEM_CONTEXT_ACTION,key,options);
            },
            items: {
                "view": {name: "View", icon: 'view'},
                "remove": {name : "Remove", icon: 'delete'},
            }
        });
        
        //nitem context menu
        /*$.contextMenu({
            selector: '.nitem-context', 
            callback: function(key, options) {
                console.log(key);
                // CM.InventoryDetails.context_action(key, options);
                IM.Events.attach(IM.Events.events_const.NITEM_CONTEXT_ACTION,key,options);
            },
            items: {
                "nview": {name: "View", icon: 'view'},
                "nremove": {name : "Remove", icon: 'delete'},
            }
        });*/
        
    }
    
    /*================================================================
    *                       INIT FUNCTIONS
    *===============================================================*/
    function init_item_seized_tbl(){
		IM.Vars.item_seized_tbl = $('#item_seized_tbl');
        IM.Vars.item_seized_tbl.DataTable({
			"autoWidth": false,
			"aoColumnDefs": [
			
				{ "sWidth": "20%", "aTargets": [ 0 ] },
				{ "sWidth": "40%", "aTargets": [ 1 ] },
				{ "sWidth": "10%", "aTargets": [ 2 ] },
				{ "sWidth": "20%", "aTargets": [ 3 ] },
				
			],
			"fnCreatedRow"  : function( nRow, aData, iDataIndex ){
				$(nRow).addClass('item-context');	
			},
		});
		
		$('#item_seized_tbl tbody').on('click','tr', function(e){
            //avoid showing of modals kahit wala ng item
            if(IM.Vars.items.length > 0){
                var td = $(this).find('td:first').text();
                IM.Vars.selected_item = Main.Objects.get_object(IM.Vars.items,td,"item_id")[0];
                IM.Events.attach(IM.Events.events_const.ON_VIEW_ITEM);
            }	
		});
	}
    function init_first_item_id(){
        $.ajax({
			type: 'GET',
			url: Main.Vars.host + 'casereport/itemfirstid/',
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
				$('#itemId').text(data.id);
			},
		});
    }
    function init_narcotics_seized_tbl(){
        IM.Vars.nitem_seized_tbl = $('#narcotics_seized_tbl');
        IM.Vars.nitem_seized_tbl.DataTable({
			"autoWidth": false,
			"aoColumnDefs": [
				
				{ "sWidth": "20%", "aTargets": [ 0 ] },
				{ "sWidth": "40%", "aTargets": [ 1 ] },
				{ "sWidth": "10%", "aTargets": [ 2 ] },
				{ "sWidth": "20%", "aTargets": [ 3 ] },
				
			],
			"fnCreatedRow"  : function( nRow, aData, iDataIndex ){
				$(nRow).addClass('nitem-context');	
			},
		});
		
		$('#narcotics_seized_tbl tbody').on('click','tr', function(e){
             if(IM.Vars.nitems.length > 0){
                var td = $(this).find('td:first').text();
                IM.Vars.selected_item = Main.Objects.get_object(IM.Vars.nitems,td,"item_id")[0];
                IM.Events.attach(IM.Events.events_const.ON_VIEW_NITEM);   
             }
		});
    
    }
    function init_first_narcotics_item_id(){
		$.ajax({
			type: 'GET',
			url: Main.Vars.host + 'casereport/narcoticsitemfirstid/',
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
				if(data.success == true){
					$('#narcotidsItemId').text(data.id);
				}else{
					alert('Narcotics Item Id cannot be fetched. Contact your IT Admin.');
				}			
			},
		});
	}
    
    return {
        onLoad : onLoad,
    }
    
})();


IM.Events =(function(){ 
    
    var events_const = {
        'ON_ADD_ITEM' : 1,
        'ON_VIEW_ITEM' : 2,
        'ON_ADD_NITEM' : 3,
        'ON_VIEW_NITEM' : 4,
        'ON_SHOW_PNP' : 5,
        'ITEM_CONTEXT_ACTION' : 6,
        'NITEM_CONTEXT_ACTION' : 6,
    
    }
    /*================================================================
    *                     SEIZING OFFICER FUNCTIONS
    *===============================================================*/
    
    function on_show_pnp(){
        var seizing_officer_tbl = $('#seizing_officers_table');
        $('#showSeizingOfficerFormButton').click(function(){
            Main.Mod.show_modal('seizingOfficerModal',1200,-300);
            seizing_officer_tbl.DataTable({
                "deferRender": true,
                "autoWidth": false,
                //for reinitialisation purpose
                destroy: true,
                "aLengthMenu": [[5, 25, 50, 100, -1], [5, 25, 50, 100, "All"]],
                "ajax" : {
                        url :  Main.Vars.host + "casereport/activepnps",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 0 , 4] },
                            { "bOrderable": false, "aTargets": [ 0 , 4] },
                            { "sWidth": "10%", "aTargets": [ 0 ] },
                            { "sWidth": "20%", "aTargets": [ 1 ] },
                            { "sWidth": "30%", "aTargets": [ 2 ] },
                            { "sWidth": "30%", "aTargets": [ 3 ] },
                            { "sWidth": "10%", "aTargets": [ 4 ] },
                    ],
                    "columns": [
                        {
                            "render": function(data,type,row,meta) {
                                var i = Main.Objects.index_of(IM.Vars.aoSelected_officers,row.pnp_id,'pnp_id');
                                var str = '';
                                
                                if(i > -1) {
                                    str = '<input data-name=\"' + row.first_name + ' ' + row.last_name + '\" data-value=\"' + row.pnp_id +'\"style=\"padding-left: 0px; margin-left: 10px\" type=\"checkbox\" checked/>'
                                } else {
                                    str = '<input data-name=\"' + row.first_name + ' ' + row.last_name + '\" data-value=\"' + row.pnp_id +'\"style=\"padding-left: 0px; margin-left: 10px\" type=\"checkbox\"/>';
                                }
                                return str;
                            }
                        },
                        { 
                            "data": "pnp_id",

                        },	
                        {
                            "data": "rank",	

                        },
                        { 	
                            "render": function(data,type,row,meta) {

                                return row.first_name + " " + row.last_name;
                            }

                        },
                        { 

                            "render": function(data,type,row,meta) {
                                return '<img style="margin-left:20px;" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';
                            }
                        },
                    ],

                    sPaginationType: "full_numbers",
            });
	   });
        
        //handle event for checkbox checking.
		seizing_officer_tbl.off("change").on("change", ":checkbox", function() { 
         
            
			if($(this).is(':checked')){
                
                var id =  $(this).attr('data-value');
                var name= $(this).attr('data-name');
                var s_o = {
                    'pnp_id' : id,
                    'name' : name,
                }
                IM.Vars.selected_officers.push(id);
                IM.Vars.aoSelected_officers.push(s_o);
			}else{
                //remove item
                var id = $(this).attr('data-value');
                var i = IM.Vars.selected_officers.indexOf(id);
                IM.Vars.aoSelected_officers.splice(i,1);
                IM.Vars.selected_officers.splice(i,1); 
			}
		});
        
        //add event handling
        $('#addSelectedSeizingOfficerButton').on('click',function(){
            var tf = $('#seizing_officer');
            tf.val('');
            if(IM.Vars.selected_officers.length < 1){
                Main.Mod.show_bootbox_alert('You need to choose atleast 1 seizing officer.','Message',null);
            }else{
              
                var str = "";
                var callback = function(){
                    $('#seizingOfficerModal').modal('hide');
                    console.log(IM.Vars.aoSelected_officers);
                    $.each(IM.Vars.aoSelected_officers,function(idx,obj){
                        str += obj.name +  ', ';
                    });
                    
                    tf.val(str);
                }
                Main.Mod.show_bootbox_alert('Seizing officers added.','Message',callback);
            }
        });
    }
    
    //called by edit case report module
    var refresh_so_list = function refrest_so_list(){
        
        var tf = $('#seizing_officer');
        tf.val('');
        
        var str = "";
        $.each(IM.Vars.aoSelected_officers,function(idx,obj){
            str += obj.name +  ', ';
        });
        
        tf.val(str);

    }
      
    /*================================================================
    *                       ITEM FUNCTIONS
    *===============================================================*/
    function on_add_item(){
        $('#showItemFormButton').click(function(){
		   Main.Mod.show_modal('addItemModal',700,-50);
           load_registered_violators(1); 
        }); 
        
        $('#addItemButton').click(function(){
            add_item();
	   });
    }
    
    function on_view_item(){  
        Main.Mod.show_modal('showItemModal',700,-50);
        view_item();
    }
  
    function add_item(){
        
        if(IM.Vars.item_violators.length < 1){
			var message = "You need to associate this item with atleast 1 violator.";
			if(CVM.Vars.violators.length < 1){
				message += "\n But it seems that there are no violators yet.";
			}
			Main.Mod.show_bootbox_alert(message, 'Message', null);
		}else{
			var callback_func = function(){
			
			var item = Object.create(null);
			item.case_id = $('#case-id').text();
			item.item_id =  $('#itemId').text();
			item.quantity = $('#qty').val();
			item.description = $('#description').val();
			item.markings = $('#markings').val();
            item.is_chain_custody = 0;
			if($("#inc_cc").is(':checked')){
				item.is_chain_custody = 1;
			}else{
				item.is_chain_custody = 0; 
			}
            item.is_existing = false;
			item.file_path = 'public/resources/photos/inventory/' + item.case_id;
			item.temp_file_path =  'public/resources/temps/inventory/' + item.case_id;
			//set the violators associated with the item
			item.violators = IM.Vars.item_violators.slice();
			
			//clear selected_violators array
			IM.Vars.item_violators.length = 0;
			
			var formdata = new FormData();
			//one item at a time.
			formdata.append('item_id', $('#itemId').text());
			formdata.append('case_id', $('#case-id').text());
			formdata.append('item', $('#itemfile')[0].files[0]);
			formdata.append('what', 'item');
			$.ajax({
				type: 'POST',
				//get the next id here
				url: Main.Vars.host + 'casereport/uploadtempitem',
				data: formdata,
				contentType: false,
				cache: false,   
				processData:false, 
				dataType: 'json',
				success: function(data){
						if(data.success == true){
							$('#itemId').text(data.id);
							update_table(item,'item_seized_tbl');
							IM.Vars.items.push(item);
							//clears form
							$('#addItemModal').modal('hide');
							Main.Mod.clear_form_inputs('inventory_form_div',new Array('text','file'));
							
						}else{
							alert('System error: cannot fetch next id. Contact your IT Admin.');
						}
						
					},
				});
			};
			Main.Mod.show_bootbox_confirm('Are you sure you want to add this item?', 'Confirmation', callback_func);
		}
    
    }
    function view_item(){
        var selected_item = IM.Vars.selected_item;
        
        $('#itemId_v').text(selected_item.item_id);
		$('#description_v').val(selected_item.description);
		$('#qty_v').val(selected_item.quantity);
		$('#markings_v').val(selected_item.markings);
		$('#itempreview_v').attr('src', Main.Vars.host + selected_item.temp_file_path + '/' + selected_item.item_id + '_item.jpg');
	
		var list = $('#collectedFromList');
		var list2 = $('#addMoreList');
		
        //assure lists reset
        list.empty(); list2.empty();
        
        
		var collectedFrom = new Array();
		var full_violators = new Array();
		var difference = new Array();
		var ids = {};
		
		//create new objects based on selected_items.violators
		$.each(selected_item.violators, function(idx, obj){
			var new_obj = {};
			new_obj.full_name = obj.full_name;
			new_obj.violator_id = obj.violator_id;	
			collectedFrom.push(new_obj);
		});
		
		//set difference array
		$.each(CVM.Vars.violators, function(idx, obj){
			var new_obj = {};
			new_obj.full_name = obj.first_name + " " + obj.last_name;
			new_obj.violator_id = obj.violator_id;	
			full_violators.push(new_obj);
		});

		collectedFrom.forEach(function(obj){
			//this will contain objects with violator_id as their index.
			ids[obj.violator_id] = obj;
		});	
		
		difference = full_violators.filter(function(obj){
			return !(obj.violator_id in ids);
		});

		//append to lists
		$.each(collectedFrom, function(idx, obj){
			list.append('<li class="list-group-item">' + obj.full_name  + '<span class="pull-right">' + 
						'<button data-name="' + obj.full_name + '"data-value="' + obj.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');
		});
		
		if(difference.length > 0){
			$.each(difference, function(idx, obj){
				list2.append('<li class="list-group-item">' + obj.full_name  + '<span class="pull-right">' + 
					'<button data-value="' + obj.violator_id + '"class="btn btn-info"><span class="fa fa-plus"></span></button></li>');					
		
			});
		} else {
			list2.append('<li class="list-group-item" data-value=\"empty\"" > All violators were already associated with this item. </li>');
		}
		
		//add event handling to both list
		$('#collectedFromList').off('click').on('click','button',function(){
			var button = $(this);
			var parent_li = button.parent().parent();
			var i = Main.Objects.index_of(collectedFrom,button.attr('data-value'),'violator_id');
		
			var removed = collectedFrom.splice(i,1);
			console.log(removed);
			if(list2.find('li:first').attr('data-value') == 'empty'){
				list2.empty();
				list2.append('<li class="list-group-item">' + removed[0].full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed[0].violator_id + '"class="btn btn-info"><span class="fa fa-plus"></span></button></li>');					
			}else{
				list2.append('<li class="list-group-item">' + removed[0].full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed[0].violator_id + '"class="btn btn-info"><span class="fa fa-plus"></span></button></li>');				
			}
			parent_li.remove();		
			
			//add removed object to difference array;
			difference.push(removed[0]);
			//check if collectedFrom is empty, then append empty list
			if(collectedFrom.length === 0){
				list.append('<li class="list-group-item" data-value=\"empty\"" > You cannot leave this empty. </li>');
			}
		});
		
		$('#addMoreList').off('click').on('click','button',function(){
			var button = $(this);
			var parent_li = button.parent().parent();
			var i = Main.Objects.index_of(difference,button.attr('data-value'),'violator_id');
			var removed = difference.splice(i,1)[0];
			
			if(list.find('li:first').attr('data-value') == 'empty'){
				list.empty();
				list.append('<li class="list-group-item">' + removed.full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');					
			}else{
				list.append('<li class="list-group-item">' + removed.full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');				
			}
			parent_li.remove();		
			
			collectedFrom.push(removed);
			
			if(difference.length === 0){
				list2.append('<li class="list-group-item" data-value=\"empty\"" > All violators were already associated with this item. </li>');
			}
		});
		
		//handle updating
		$('#updateItemButton').off('click').on('click',function(){
			if(collectedFrom.length === 0){
				Main.Mod.show_bootbox_alert("An item needs atleast 1 violator associated with it!","Message",null);
			}else{
				
				var formdata = new FormData();
				//one item at a time.
				formdata.append('item_id', $('#itemId_v').text());
				formdata.append('case_id', $('#case-id').text());
				formdata.append('item', $('#itemfile_v')[0].files[0]);
				formdata.append('what', 'item');
				//update photo
				$.ajax({
					type: 'POST',
					//get the next id here
					url: Main.Vars.host + 'casereport/uploadtempitem',
					data: formdata,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
							if(data.success == true){
								
                                update_item_table(selected_item);
                                
							}else{
								alert('System error: cannot update photo but other details might have been updated.');
							}
							
						},
				});
						
				//update selected item
				selected_item.description = $('#description_v').val();
				selected_item.quantity = $('#qty_v').val();
				selected_item.markings = $('#markings_v').val();
                
                if($("#inc_cc_v").is(':checked')){
                    selected_item.is_chain_custody = 1;
                }else{
                    selected_item.is_chain_custody = 0; 
                }
					
				selected_item.violators.length = 0;
				selected_item.violators = collectedFrom.slice();
				collectedFrom.length = 0;
				Main.Mod.show_bootbox_alert("Item updated!","Message",null);
				$('#showItemModal').modal('hide');
			}	
		});
    }
    
    function update_item_table(item){
         IM.Vars.item_seized_tbl.find('tr').each(function (i, el) {
			var tr = ($(this));
			var td_text = $(this).find('td:first').text();		
			if(td_text == item.item_id){
				$('#item_seized_tbl').dataTable().fnDeleteRow(i-1);
                update_table(item,'item_seized_tbl');
				return false;
			}
		});
    }   
    
    /*================================================================
    *                       NITEMS FUNCTIONS
    *===============================================================*/
    function on_add_nitem(){
        //lagay na lang sa main to
        load_narcotics('narcotics_list');
        load_narcotics('narcotics_list_v');
        
        $('#showNarcoticsItemFormButton').click(function(){
            Main.Mod.show_modal('addNarcoticsItemModal',700,-50);
            //assure that there is no violators selected
            IM.Vars.item_violators.length = 0;
          
            load_registered_violators(2); 
            
        }); 
        
        $('#addNarcoticsItemButton').click(function(){
              add_nitem();
	   });
    
    }
    function on_view_nitem(){
        Main.Mod.show_modal('showNarcoticsItemModal',700,-50);
        view_nitem();
    }
    
    //accepts two possible params narcotics_list or narcotics_list_v
    function load_narcotics(list_id){
		$.ajax({
			type: 'GET',
			url: Main.Vars.host + 'narcotics/retrieve/',
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
				var list = $('#' + list_id);
				$.each(data.data, function(idx,obj){
					$(list).append('<option value="'+ obj.narcotics_id + '"> ' + obj.name + '</option>')
				})
				
			},
		});
	} 
    function add_nitem(){
        //check kung may inassociate na violators
        if(IM.Vars.item_violators.length < 1){
            Main.Mod.show_bootbox_alert('Please associate atleast 1 violator.', 'Message', null);
        } else {
            //after thesis burahin na tong proto and props na to. Kagaguhan ko lang to. HAHAHA!
            var proto = Main.Objects.proto_type.N_ITEM;
            var props = {};
            props.case_id = $('#case-id').text();
            props.item_id  = $('#narcotidsItemId').text();
            props.quantity = $('#qty_n').val();
            props.markings = $('#markings_n').val();
            props.narcotics = { 
                'narcotics_name' : $( "#narcotics_list option:selected" ).text(),		
                'narcotics_id' : $( "#narcotics_list option:selected" ).val(), 	
            };
            //props.is_chain_custody = $("#inc_cc_n").is(':checked') ? 1:0;
            props.is_chain_custody = 0;
            props.file_path = 'public/resources/photos/inventory/' + $('#case-id').text();
            props.temp_file_path =  'public/resources/temps/inventory/' + $('#case-id').text();
            props.is_existing = false;

            var nItem = Main.Objects.create_object(proto,props);

            nItem.violators = IM.Vars.item_violators.slice();

            //clear selected_violators array
            IM.Vars.item_violators.length = 0;

            var formdata = new FormData();
            //one item at a time.
            formdata.append('item_id', $('#narcotidsItemId').text());
            formdata.append('case_id', $('#case-id').text());
            formdata.append('item', $('#itemfile_n')[0].files[0]);
            formdata.append('what', 'nitem');

            $.ajax({
                type: 'POST',
                //get the next id here
                url: Main.Vars.host + 'casereport/uploadtempitem',
                data: formdata,
                contentType: false,
                cache: false,   
                processData:false, 
                dataType: 'json',
                success: function(data){
                    if(data.success == true){
                        IM.Vars.nitems.push(nItem);
                        update_table(nItem,'narcotics_seized_tbl');
                        Main.Mod.show_bootbox_alert("Successfully added!","Message",null);

                        $('#addNarcoticsItemModal').modal('hide');
                        $('#narcotidsItemId').text(data.id);
                        Main.Mod.clear_form_inputs('add_narcotics_form',new Array('select','text','file'));					
                    }else{
                        alert('System error: cannot fetch next id. Contact your IT Admin.');
                    }
                },
            });

        } 
        
    }
    
    function view_nitem(){
        var selected_item = IM.Vars.selected_item;
        $('#narcotidsItemId_v').text(selected_item.item_id);
		$('#qty_n_v').val(selected_item.quantity);
		$('#markings_n_v').val(selected_item.markings);
		$('#itempreview_n_v').attr('src', Main.Vars.host + selected_item.temp_file_path + '/' + selected_item.item_id + '_item.jpg');
		//load select narcotics on select.
		$('#narcotics_list_v option[value="' + selected_item.narcotics.narcotics_id.trim() +'"]').prop('selected',true);
		console.log(selected_item.narcotics.narcotics_id);
        
		var list = $('#collectedFromList_n');
		var list2 = $('#addMoreList_n');
        
        //assure to be empty
        list.empty(); list2.empty();
		
		var collectedFrom = new Array();
		var full_violators = new Array();
		var difference = new Array();
		var ids = {};
		
		//create new objects based on selected_items.violators
		$.each(IM.Vars.selected_item.violators, function(idx, obj){
			var new_obj = {};
			new_obj.full_name = obj.full_name;
			new_obj.violator_id = obj.violator_id;	
			collectedFrom.push(new_obj);
		});
		//set difference array
		$.each(CVM.Vars.violators, function(idx, obj){
			var new_obj = {};
			new_obj.full_name = obj.first_name + " " + obj.last_name;
			new_obj.violator_id = obj.violator_id;	
			full_violators.push(new_obj);
		});

		collectedFrom.forEach(function(obj){
			//this will contain objects with violator_id as their index.
			ids[obj.violator_id] = obj;
		});	
		
		difference = full_violators.filter(function(obj){
			return !(obj.violator_id in ids);
		});

		//append to lists
		$.each(collectedFrom, function(idx, obj){
			list.append('<li class="list-group-item">' + obj.full_name  + '<span class="pull-right">' + 
						'<button data-name="' + obj.full_name + '"data-value="' + obj.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');
		});
		if(difference.length > 0){
			$.each(difference, function(idx, obj){
				list2.append('<li class="list-group-item">' + obj.full_name  + '<span class="pull-right">' + 
					'<button data-value="' + obj.violator_id + '"class="btn btn-info"><span class="fa fa-plus"></span></button></li>');					
		
			});
		} else {
			list2.append('<li class="list-group-item" data-value=\"empty\"" > All violators were already associated with this item. </li>');
		}
		
		//add event handling to both list
		$('#collectedFromList_n').off('click').on('click','button',function(){
			var button = $(this);
			var parent_li = button.parent().parent();
			var i = Main.Objects.index_of(collectedFrom,button.attr('data-value'),'violator_id');
		
			var removed = collectedFrom.splice(i,1);
			console.log(removed);
			if(list2.find('li:first').attr('data-value') == 'empty'){
				list2.empty();
				list2.append('<li class="list-group-item">' + removed[0].full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed[0].violator_id + '"class="btn btn-info"><span class="fa fa-plus"></span></button></li>');					
			}else{
				list2.append('<li class="list-group-item">' + removed[0].full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed[0].violator_id + '"class="btn btn-info"><span class="fa fa-plus"></span></button></li>');				
			}
			parent_li.remove();		
			
			//add removed object to difference array;
			difference.push(removed[0]);
			//check if collectedFrom is empty, then append empty list
			if(collectedFrom.length === 0){
				list.append('<li class="list-group-item" data-value=\"empty\"" > You cannot leave this empty. </li>');
			}
		});
		
		$('#addMoreList_n').off('click').on('click','button',function(){
			var button = $(this);
			var parent_li = button.parent().parent();
			var i = Main.Objects.index_of(difference,button.attr('data-value'),'violator_id');
			var removed = difference.splice(i,1)[0];
			
			if(list.find('li:first').attr('data-value') == 'empty'){
				list.empty();
				list.append('<li class="list-group-item">' + removed.full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');					
			}else{
				list.append('<li class="list-group-item">' + removed.full_name  + '<span class="pull-right">' + 
					'<button data-value="' + removed.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');				
			}
			parent_li.remove();		
			
			collectedFrom.push(removed);
			
			if(difference.length === 0){
				list2.append('<li class="list-group-item" data-value=\"empty\"" > All violators were already associated with this item. </li>');
			}
		});
		//handle updating
		$('#updateNItemButton').off('click').on('click',function(){
			if(collectedFrom.length === 0){
				Main.Mod.show_bootbox_alert("An item needs atleast 1 violator associated with it!","Message",null);
			}else{
				
				var formdata = new FormData();
				//one item at a time.
				formdata.append('item_id', $('#narcotidsItemId_v').text());
				formdata.append('inventory_id', $('#inventory-id').text());
				formdata.append('item', $('#itemfile_n_v')[0].files[0]);
				formdata.append('case_id', $('#case-id').text());
                formdata.append('what', 'nitem');
				//update photo
				$.ajax({
					type: 'POST',
					//get the next id here
					url: Main.Vars.host + 'casereport/uploadtempitem',
					data: formdata,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
                        if(data.success == true){
                            //update selected item
                            selected_item.narcotics = { 
                                'narcotics_name' : $( "#narcotics_list_v option:selected" ).text(),		
                                'narcotics_id' : $( "#narcotics_list_v option:selected" ).val(), 	
                            };

                            selected_item.quantity = $('#qty_n_v').val();
                            selected_item.markings = $('#markings_n_v').val();

                            selected_item.violators.length = 0;
                            selected_item.violators = collectedFrom.slice();
                            collectedFrom.length = 0;
                            update_nitem_table(selected_item);

                            Main.Mod.show_bootbox_alert("Item updated!","Message",null);	
                            $('#showNarcoticsItemModal').modal('hide');
                        }else{
                            alert('System error: cannot update photo but other details might have been updated.');
                        }
							
				    },
                });
						
			}	
		});
    }
    
    
    function update_nitem_table(nitem){
        IM.Vars.nitem_seized_tbl.find('tr').each(function (i, el) {
			var tr = ($(this));
			var td_text = $(this).find('td:first').text();
           
			if(td_text == nitem.item_id){
                $('#narcotics_seized_tbl').dataTable().fnDeleteRow(i-1);
                update_table(nitem,'narcotics_seized_tbl');
				return false;
			}
		});
    }
    /*================================================================
    *                       MISC FUNCTIONS
    *===============================================================*/
   function update_table(item,what){
		var in_cc = (item.is_chain_custody==1) ? "Yes" : "No";
		var param_row;
		if(what=='item_seized_tbl'){
			param_row = [
				item.item_id,
				item.description,
				item.quantity,
				item.markings,
				
			]
		}else{
			param_row = [
				item.item_id,
				item.narcotics.narcotics_name,
				item.quantity,
				item.markings,
               
			]
		}
		$('#' + what).dataTable().fnAddData(param_row);			
	}
    
    //this is for updating table after a violator removal. Called by casereport_violators_modules.js
    var _update_table = function _update_table(idx, what){
        var param_row;
		if(what=='item_seized_tbl'){
             $('#item_seized_tbl').dataTable().fnDeleteRow(idx);
		} else {
            $('#narcotics_seized_tbl').dataTable().fnDeleteRow(idx);    
        }

    }
    
    
    /*================================================================
    *                       INIT FUNCTIONS
    *===============================================================*/
    function load_registered_violators(what){
        //reset selection
        IM.Vars.item_violators.length = 0;
        var list_name;
		var list;
        var selectedList;
        //for items else for narcotics items
		if(what == 1){
            list = $('#regViolatorsList');
			list_name = 'regViolatorsList';
            selectedList = $('#selectedViolatorsList');
            selectedList.empty();
            selectedList.append('<div class="list-group-item">No selection yet.</div>');
		}else{
			list = $('#regViolatorsList2');
			list_name = 'regViolatorsList2';
            selectedList = $('#selectedViolatorsList2');
            selectedList.empty();
            selectedList.append('<div class="list-group-item">No selection yet.</div>');
		}

        //empty list
        $(list).empty();
      
        
		var regViols = CVM.Vars.violators;
		if(regViols.length < 1){
			list.append('<div class="list-group-item">No violators yet.</div>');
		}else{
			$.each(regViols, function(idx, obj){
				var full_name = obj.first_name + ' ' + obj.last_name;
				list.append('<li class="list-group-item">' + full_name  + '<span class="pull-right">' + 
							'<button data-name="' + full_name + '"data-value="' + obj.violator_id + '"class="btn btn-info"><span class="glyphicon glyphicon-plus"></span></button></li>');
			});
		}
		
		//add to selection list and pushing to item violators array
		$('#' + list_name).off('click').delegate('button', 'click', function() {
			var button = $(this);
			var callback_func = function(){
				var tr = button.parent().parent();
				var violator = {};
                violator.violator_id = button.attr('data-value');
                violator.full_name = button.attr('data-name');
				IM.Vars.item_violators.push(violator);
                //remove to list
				tr.remove();	
                
                if(IM.Vars.item_violators.length == 1){
                    selectedList.empty();
                }
                
                selectedList.append('<li class="list-group-item">' + violator.full_name  + '<span class="pull-right">' + 
				    '<button data-name="' + violator.full_name + '"data-value="' + violator.violator_id + '"class="btn btn-info"><span class="fa fa-times"></span></button></li>');
	  
			}
   
            Main.Mod.show_bootbox_confirm('Are you sure you want to associate this item with this violator?', 'Confirmation', callback_func);		
        });
        
        selectedList.off('click').delegate('button', 'click', function() {
            
            var button = $(this);
            
            var violator = {};
            violator.violator_id = $(this).attr('data-value');
            violator.full_name = $(this).attr('data-name');
            
            var i = Main.Objects.index_of(IM.Vars.item_violators,violator.violator_id,'violator_id');
            
            var removed = IM.Vars.item_violators.splice(i,1)[0];
            
            list.append('<li class="list-group-item">' + removed.full_name  + '<span class="pull-right">' + 
							'<button data-name="' + removed.full_name + '"data-value="' + removed.violator_id + '"class="btn btn-info"><span class="glyphicon glyphicon-plus"></span></button></li>');
            
            //remove fucking shet
            var tr = button.parent().parent();
            tr.remove();
            
            if(IM.Vars.item_violators.length == 0){
                selectedList.append('<div class="list-group-item">No selection yet.</div>');
            }
        });
       
    }
    
    /*================================================================
    *                      CONTEXT ACTIONS
    *===============================================================*/
    var item_context_action = function item_context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		var item_id = $(tr).find('td:first').text();
        var className = (options.$trigger[0].className.split(" ")[0]);
		switch (key) {
		   case 'remove' :
                if(className == 'item-context'){
                    ci_remove_item(item_id);
                }else {
                    cn_remove_item(item_id);
                }
              
                break;
            case 'view' :
                if(className == 'item-context'){
                    ci_view_item(item_id);
                }else {
                    cn_view_item(item_id);
                }
              
                break;
		   default :
			   break;
	   }           
	}
    
    function ci_remove_item(id){
        var i = Main.Objects.index_of(IM.Vars.items, id, 'item_id');
        console.log(id);
      
        if( i > -1){
            var removed = IM.Vars.items.splice(i,1)[0]; 
           
            //magic code, papakyu!
            $('#item_seized_tbl').dataTable().fnDeleteRow(i);
        
            IM.Vars.items_rem.push(removed.item_id);
        }
        
    }
    //manually set the selected_item
    function ci_view_item(id){
        var i = Main.Objects.index_of(IM.Vars.items,id,'item_id');
        IM.Vars.selected_item = IM.Vars.items.slice(i,1)[0];
        
        on_view_item();
    }
    
    function cn_remove_item(id){
        var i = Main.Objects.index_of(IM.Vars.nitems, id, 'item_id');
      
        if( i > -1){
            var removed = IM.Vars.nitems.splice(i,1)[0]; 
            //magic code, papakyu!
            $('#narcotics_seized_tbl').dataTable().fnDeleteRow(i);
              
            IM.Vars.nitems_rem.push(removed.item_id);
        }
    }
    
    
    function cn_view_item(id){
        var i = Main.Objects.index_of(IM.Vars.nitems,id,'item_id');
        IM.Vars.selected_item = IM.Vars.nitems.slice(i,1)[0];
        
        on_view_nitem();
    }

                                             
    var attach = function(type,key,options){
        switch(type){
            case events_const.ON_ADD_ITEM:
                on_add_item();
                break;
            case events_const.ON_VIEW_ITEM:
                on_view_item();
                break; 
            case events_const.ON_ADD_NITEM:
                on_add_nitem();
                break;
            case events_const.ON_VIEW_NITEM:
                on_view_nitem();
                break;  
            case events_const.ON_SHOW_PNP:
                on_show_pnp();
                break;  
            case events_const.ITEM_CONTEXT_ACTION:
                item_context_action(key,options);
                break;
            default:
                alert('Cannot attach event.');
        }
    }
    
    return {
        events_const : events_const,
        attach : attach,
        update_table : update_table,
        _update_table : _update_table,
        refresh_so_list : refresh_so_list,
    } 
    
})();