var ECM = ECM || {};

ECM.Vars = (function(){ 
    
    //this hold our case id to be edited.
    var active_case = '';

    return {
        active_case : active_case,   
    }
    
    
})();


ECM.Main = (function(){ 
    
    
    var onLoad = function onLoad(){
        
        var href = window.location.pathname;
        ECM.Vars.active_case = href.split('/').pop();
        
        //change url submission
        CSR.Vars.submission_url = 'editcasereport/submit';
        //change usermod
        CSR.Vars.user_mod = 'editcasereport';
        //yeah!
        CSR.Vars.is_editing = true;
        init();
    }
    
    
    function init(){
        
        Main.Mod.do_ajax('editcasereport/full/' + ECM.Vars.active_case ,'GET',render_page,null); 
              
    }
    
   
    function render_page(data){
        load_witnesses(data.case.witnesses);
        load_case_scene_image(data.case.case_details.case_id);
        load_violator_photos(data.case.violators);
        load_items_photos(data.case.case_details.case_id, data.case.items, data.case.nitems, data.case.seizing_officers);
        render_case_details(data.case.case_details);
        render_arrest_details(data.case.arrest_details);
          
    }
    
    function load_witnesses(witnesses){
        $.each(witnesses, function(idx,obj){
            var witness = {};
            witness.fake_id = Witness.Vars.fake_id;
            witness.name = obj.name;
            witness.case_id = obj.case_id;
            witness.address = obj.address;
            witness.contact = obj.contact;
            
            Witness.Events.add_witness(witness);
        });
    }
    
    function load_case_scene_image(case_id){
        var formdata = new FormData();
        formdata.append('case_id', case_id);
         
        Main.Mod.do_ajax('editcasereport/scene_photos','POST',function(data){
            if(data.success){
                AM.Main.scan_documents(case_id);
            } else {
                var message = "Cannot complete loading the page, try to refresh. \n" + 
                    "If problem still persists, contact your IT admin."
                Main.Mod.show_bootbox_alert(message, 'Error', null);
            }
        },formdata);    
    }
  
    function load_items_photos(case_id, items, nitems, seizing){
        var callback = function(){
            render_items_details(items);
            render_nitems_details(nitems);
            load_seizing_officer(seizing);
        }
        
        
        var formdata = new FormData();
        formdata.append('case_id', case_id);
        
        Main.Mod.do_ajax_error_secured('editcasereport/item_photos','POST',callback,callback,formdata);  
    }
    
    function render_items_details(items){
       
        $.each(items, function(idx,obj){
            var item = Object.create(null);
            
            for (var property in obj) {
                
                if(property == 'violators'){
                    
                    var o_violators = obj['violators'];
                    var n_violators = new Array();
                    $.each(o_violators, function(idx, obj){
                        var full_name = obj.first_name + ' ' + obj.last_name;
                        var violator = {};
                        violator.violator_id = obj.violator_id;
                        violator.full_name = full_name;
                        
                        n_violators.push(violator);
                    });
                    
                    item.violators = n_violators;
                } else if (property == 'item_seized_id') {
                    //because we are using item_id as index in front end.
                    item['item_id'] = obj[property];
                    
                }else {
            
                    item[property] = obj[property];
                
                }
                
            }    
            
            //set temp file path
            item.file_path = 'public/resources/photos/inventory/' + item.case_id;
			item.temp_file_path =  'public/resources/temps/inventory/' + item.case_id;
            //set is_existing meaning comes from the database
            item.is_existing = true;
            IM.Vars.items.push(item);
          
            //push to table
            IM.Events.update_table(item,'item_seized_tbl');
        });
        
    }
    
    
    function render_nitems_details(nitems){
        
        $.each(nitems, function(idx,obj){
            var item = Object.create(null);
            
            for (var property in obj) {
                
                if(property == 'violators'){
                    
                    var o_violators = obj['violators'];
                    var n_violators = new Array();
                    $.each(o_violators, function(idx, obj){
                        var full_name = obj.first_name + ' ' + obj.last_name;
                        var violator = {};
                        violator.violator_id = obj.violator_id;
                        violator.full_name = full_name;
                        
                        n_violators.push(violator);
                    });
                    
                    item.violators = n_violators;
                } else if (property == 'narcotics_seized_id') {
                    //because we are using item_id as index in front end.
                    item['item_id'] = obj[property];
                    
                } else if(property == 'is_in_chain_custody'){
                    //sobrang bobo ko gamit ko sa front end is_chain_custody tapos sa back eto. PAKYU!
                    item['is_chain_custody'] = obj[property]
                }else {
                    item[property] = obj[property];
                }
                
            }    
           
            //set temp file path
            item.file_path = 'public/resources/photos/inventory/' + item.case_id;
			item.temp_file_path =  'public/resources/temps/inventory/' + item.case_id;
            
            //set is_existing meaning comes from the database
            item.is_existing = true;
            
            IM.Vars.nitems.push(item);
           
            //push to table
            IM.Events.update_table(item,'narcotics_seized_tbl');
        });

    }
    
    function load_seizing_officer(officers){
     
        $.each(officers, function(idx,obj){
            var officer = {};
            
            var name = obj.rank + ' ' + obj.first_name + ' ' + obj.last_name;
            
            officer.pnp_id = obj.pnp_id;
            officer.name = obj.rank + ' ' + obj.first_name + ' ' + obj.last_name;
        
            
            IM.Vars.aoSelected_officers.push(officer);
            IM.Vars.selected_officers.push(officer.pnp_id);
        
        });
        
        IM.Events.refresh_so_list();
        
    }
    
    
    function load_violator_photos(violators){
       
        var callback = function(){
             render_violators_details(violators);
        }
        
        var ids = new Array();
        $.each(violators, function(idx,obj){
            ids.push(obj.violator_id);  
        });
        console.log(ids);
        var formdata = new FormData();
       
        formdata.append('ids',JSON.stringify(ids));
        
        Main.Mod.do_ajax_error_secured('editcasereport/violator_photos','POST',callback,callback,formdata); 
       
    }
    
    function render_violators_details(violators){
       
        var violator_prototype = {
            get_full_name: function(){
                return this.last_name + ', ' + this.first_name;
            } 
        } 
        
        var charge_prototype = {
            to_string : function(){
                return this.article  + " "  + this.section;
            }	
	   }
        
        $.each(violators, function(idx,obj){
           
            var violator = Object.create(violator_prototype);
            
            for (var property in obj) {
                
                if(property == 'charges'){
                    
                    var t_charges = obj[property]
                    //para magka-tostring function tayo
                    var o_charges = new Array(); //array of objects
                    var i_charges = new Array(); //array of strings
                    
                    $.each(t_charges, function(idx,obj){
                        var ch_obj = Object.create(charge_prototype);
                        var ch_id = obj.charge_id;
                        for (var ch_property in obj) {
                            ch_obj[ch_property] = obj[ch_property];
                        }
                        o_charges.push(ch_obj);
                        i_charges.push(ch_id);
                    });
                    
                    violator['charges_obj'] = o_charges;
                    violator['charges'] = i_charges;
                   
                } else {
                    violator[property] = obj[property];
                }
               
            }
            
            //set is-existing
            violator.is_existing = true;
            
            violator.front_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_front' + '.jpg';
            violator.rear_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_rear' + '.jpg';
            violator.right_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_right' + '.jpg';
            violator.left_temp_file_path = 'public/resources/temps/violator/' + violator.violator_id + '/' + violator.violator_id + '_left' + '.jpg';
            
            CVM.Vars.violators.push(violator);
           
            CVM.Events.update_table(violator);
        });
    }
    
   
    function render_case_details(case_details){
        $('#case-id').text(case_details.case_id);
        $('#is_number').val(case_details.is_number);
        $('#progress option[value="' + case_details.progress +'"]').prop('selected',true);
        $('#nature option[value="' + case_details.nature.trim() +'"]').prop('selected',true);
        $('.summernote').code(case_details.summary);
        $('#operation_title').val(case_details.operation_name);
        $('#casepreview').attr('src', Main.Vars.host + case_details.doc_path + case_details.case_id + '.jpg');
        CSR.Vars.spot_report_id = case_details.spot_report_id;
        CSR.Vars.coordination_id = case_details.coordination_id;
        CSR.Vars.pre_op_id = case_details.pre_op_id;
        CSR.Vars.supp_id = case_details.supp_affidavit_id;
        CSR.Vars.main_id = case_details.main_affidavit_id;
        console.log(CSR.Vars.spot_report_id + ' ' + CSR.Vars.coordination_id + ' ' + CSR.Vars.pre_op_id);
        
        //check attachments presence
        if(CSR.Vars.pre_op_id != 'NONE'){
            AM.Events.update_attachment_ui('preoperation', CSR.Vars.pre_op_id);
            
        }
        
        if(CSR.Vars.coordination_id != 'NONE'){
            AM.Events.update_attachment_ui('coordination', CSR.Vars.coordination_id);
        }
        
        if(CSR.Vars.spot_report_id != 'NONE'){
            AM.Events.update_attachment_ui('spotreport', CSR.Vars.spot_report_id);
        }
        
        if(CSR.Vars.main_id != 'NONE'){
            AM.Events.update_attachment_ui('main', CSR.Vars.main_id);
        }
        
        if(CSR.Vars.supp_id != 'NONE'){
            AM.Events.update_attachment_ui('supp', CSR.Vars.supp_id);
        }
         
    }
    
    function render_arrest_details(arrest_details){
        
        //set previous barangay_id
        CSR.Vars.prev_brgy_id = arrest_details.barangay_id;
        CSR.Vars.prev_blotter_entry = arrest_details.blotter_entry_nr;
        
        $('#blotter_entry_nr').val(arrest_details.blotter_entry_nr);
        $('#date_of_arrest').val(arrest_details.date_of_arrest);
        $('#timepicker').val(arrest_details.time_of_arrest);
        CSR.Vars.barangay_id = arrest_details.barangay_id;
        $('#barangay').val(arrest_details.barangay);
        $('#street').val(arrest_details.street_name);
        $('#city').val(arrest_details.city_name);
        $('#lawyer').val(arrest_details.lawyer);
        $('#dr').val(arrest_details.dr_name);
        $('#med_on').val(arrest_details.med_on);
        $('#med_at').val(arrest_details.med_at);
        
        //set case investigator
        CSR.ArrestDetails.investigator_id.push(arrest_details.investigator.pnp_id);
        var investigator = arrest_details.investigator.rank + ' ' + arrest_details.investigator.first_name + ' ' + arrest_details.investigator.last_name; 
        $('#investigator').val(investigator);
        
        
        //set fingerman
        if(arrest_details.fingerman != null){
            CSR.ArrestDetails.finger_taken_by.push(arrest_details.fingerman.pnp_id);
            var fingerman = arrest_details.fingerman.rank + ' ' + arrest_details.fingerman.first_name + ' ' + arrest_details.fingerman.last_name; 
            $('#fing_taken_by').val(fingerman);
        }
        
          //set fingerman
        if(arrest_details.photographer != null){
            CSR.ArrestDetails.photo_taken_by.push(arrest_details.photographer.pnp_id);
            var photographer = arrest_details.photographer.rank + ' ' + arrest_details.photographer.first_name + ' ' + arrest_details.photographer.last_name; 
            $('#photo_taken_by').val(photographer);
        }
     
        
        
        
        //set arresting officers
        $.each(arrest_details.arresting_officers, function(idx,obj){
            var ao = {
                name : obj.first_name + ' ' + obj.last_name,
                pnp_id : obj.pnp_id,
            };
            CSR.ArrestDetails.arresting_officers_ids.push(obj.pnp_id);
            CSR.ArrestDetails.aoArrestingOfficers.push(ao);
        });    
        console.log(CSR.ArrestDetails.aoArrestingOfficers);
        CSR.ArrestDetails.get_selected_arresting_officers();
    }
    
    
    return {
        onLoad : onLoad,
    }
    
})();


ECM.Events = (function(){ 
    
    
    
    
})();