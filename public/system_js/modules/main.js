var Main = Main || {};

Main.Settings = (function(){

    var load = function (callback) {
        Main.Mod.do_ajax('settings/get_controls', 'GET',function(data){
          
            callback(data);
  
        },null);
    }

    
    return {
        load : load,
    }
    
})();

Main.Validator = (function(){
    
    var activate = function activate(){
        input_num_only();
        input_decimal_only();
    }
    
    function input_num_only(){
        $('.num_only').keypress(function(e) {
            var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);
            
            if (!(a.indexOf(k)>=0))
                e.preventDefault();
        });
    }
    
    function input_decimal_only(){
        $('.decimal_only').keypress(function(e) {
            if(!isDecimal(e,this))
                e.preventDefault();
        });
    }
    
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isDecimal(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
    return {
        activate : activate,
    }

})();

Main.Mod = (function(){
	
	//id = id of the element. This is often <div>
	//array = name of inputs to be cleared
	var clear_form_inputs = function clear_form_inputs(id, array){

		$("#"+id).find(':input').each(function() {
		
			switch(this.type) {
				case 'password':
					if($.inArray('password', array) > -1){
						//do code here
					}
					break;
				case 'text':
					if($.inArray('text', array) > -1){
						$(this).val('');
					}
					break;
				case 'textarea':
					if($.inArray('textarea', array) > -1){
						
					}
					break;
				case 'file':
					if($.inArray('file', array) > -1){
						$(this).replaceWith($(this).clone(true));
						switch($(this).attr('id')){
							case 'frontfile': 
								$('#frontpreview').attr('src', Main.Vars.base_path + 'public/resources/photos/default_picture.jpg');
							break;
							case 'rearfile': 
								$('#rearpreview').attr('src', Main.Vars.base_path + 'public/resources/photos/default_picture.jpg');
							break;
							case 'rightfile':
								$('#rightpreview').attr('src', Main.Vars.base_path + 'public/resources/photos/default_picture.jpg');
							break;
							case 'leftfile': 
								$('#leftpreview').attr('src', Main.Vars.base_path + 'public/resources/photos/default_picture.jpg');
							break;
							case 'itemfile': 
								$('#itempreview').attr('src', Main.Vars.base_path + 'public/resources/photos/default_img.jpg');
							break;
							case 'itemfile_n': 
								$('#itempreview_n').attr('src', Main.Vars.base_path + 'public/resources/photos/default_img.jpg');
							break;
						}	
					}
					break;
				case 'select-one':
					break;
				case 'select-multiple':
					break;
				case 'checkbox':
					break;
				case 'radio':
					break;

			}
		  });	
	}
	
	var handle_image_change = function handle_image_change(selector_strings,idsmap){
		$(selector_strings).change(function(){
            var ids = idsmap[$(this).attr('id')];
            reader = handleFileImageChange(this);
            
            if(reader != false){
                reader.onload = function(e) {   
                  imageloaded_callback(e, ids.preview);
                };
            }        
		});
	}
	function handleFileImageChange(obj){
		//holds the image preview object
	
		var file = obj.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/jpg"];
        
        
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
		      Main.Mod.show_bootbox_alert('Invalid Image Format. Use JPG instead.' + "<br/> The system will use default image.",'Error',null);
            Main.Vars.imageRead = false;
		  return false;
		}else{
            
            if(file.size > 5000000){
                alert("Exceeds Maximum Size Limit. You still be able to upload this form but the system " + 
                  "will be using the default image.");
                return false;
            } else {
                var reader = new FileReader();
                //reader.onload = imageIsLoaded;
                reader.readAsDataURL(obj.files[0]);	
                Main.Vars.imageRead = true;
                return reader;
            }		
		}
	};
	
	function imageloaded_callback(e, preview){
		$(preview).attr('src', e.target.result);
	}
	
	//handle the showing of modal.
	//NOTE: include first the prompt_modal.php in includes
	//deprecated na to! Wahaha
	var show_prompt = function show_prompt(title, message){
		$('#prompt-modal').modal(Main.Vars.modal_options);
		$('#modal-title').html(title);
		$('#modal-body p').html(message);
	};
	
	//deprecated na to! Wahaha
	var hide_prompt = function hide_prompt(){
		$('#prompt-modal').hide();
	};
	var show_bootbox_alert = function show_bootbox_alert(message, title, callback_func){
		bootbox.alert({ 	
			message: message,
			title: title,
			callback : callback_func,
		});				
			
		
	};
	var show_bootbox_confirm = function show_bootbox_confirm(message, title, callback_func, callback_func_2){
		bootbox.dialog({
			message: message,
			title: title,
            backdrop: false,
			buttons: {
				success: {
					  label: "Ok",
					  className: "btn-success",
					  callback: callback_func,
					},
				danger: {
					  label: "Cancel",
					  className: "btn-danger",
					  callback: callback_func_2,
				},
			}
		})	
	};
	
    var show_loading_modal = function(message){
        $('#loadingMessage').text(message);
        show_modal('loadingModal',700,'');
    }
    
	var show_modal = function show_modal(id,width,margin){
		var selector = '#' + id;
		$(selector).modal(Main.Vars.modal_options);
		$(selector + ' .modal-content').css('width', width+'px');
		$(selector + ' .modal').css('position' , 'absolute');
		$(selector + ' .modal-content').css('margin-left', margin+'px');	
		
	};
	
	function do_ajax(url,type,callback_function,formdata){
		var params = {
			type: type,
			url: Main.Vars.host + url,
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
				callback_function(data);
			}
		}
		if(formdata != null){
			params.data = formdata;
		}
		$.ajax(params);
	}
    
    var do_ajax_error_secured = function do_ajax_error_secured(url,type,s_callback, e_callback,formdata){
        var params = {
			type: type,
			url: Main.Vars.host + url,
			contentType: false,
			cache: false,   
			processData:false, 
			dataType: 'json',
			success: function(data){
				s_callback(data);
			},
            error : function(xhr, error){
                e_callback();
            }
		}
		if(formdata != null){
			params.data = formdata;
		}
		$.ajax(params);
    
    }
	return{
		handle_image_change : handle_image_change,		
		show_prompt : show_prompt,
		hide_prompt : hide_prompt,
		show_bootbox_confirm : show_bootbox_confirm,
		show_bootbox_alert : show_bootbox_alert,
		clear_form_inputs : clear_form_inputs,
		show_modal : show_modal,
		do_ajax : do_ajax,
        do_ajax_error_secured : do_ajax_error_secured,
         show_loading_modal : show_loading_modal,       
        
	};
	
})();

//some initialization methods that might be repeated.
Main.Init = (function(){
	//array -> array of ids, maybe pickerOpts might as well be dynamic
	var date_pick_init = function date_pick_init(array){
		var pickerOpts = {
			format: 'yyyy-mm-dd',
			autoClose: true,
            endDate: '1d',
            autoclose: true
		};  
		
		array.forEach(function(item){
			$(item).datepicker(pickerOpts);
		});
	};
    
    var date_pick_init_2 = function date_pick_init_2(array){
        var pickerOpts2 = {
			format: 'yyyy-mm-dd',
			autoClose: true,
            endDate: '+2d',
            autoclose: true
		};  
		
		array.forEach(function(item){
			$(item).datepicker(pickerOpts2);
		});
    }
	
	var time_pick_init = function time_pick_init(array){
		var pickerOpts = {
			template : 'modal',
            disable: true,
		};  
		array.forEach(function(item){
			$(item).timepicker(pickerOpts);
		})
	}
	
	var disable_cache = function disable_cache(){
		$.ajaxSetup({cache: false});
	};
	
	return {
		InitDatePicker : date_pick_init,
        InitDatePicker2 : date_pick_init_2,
		InitTimePicker : time_pick_init,
		disable_cache : disable_cache,
		
	};
})();

//Functions related to modifying/retrieving objects etc.
Main.Objects = (function(){

	var proto_type = {
		'N_ITEM' : create_proto('N_ITEM'),
	}
	
	function create_proto(what){
		
		var obj;
		
		var base_proto = {
			//initialize properties based on object supplied on 3rd parameter 
			init_props : function (props){
				 for(var k in props)
					this[k] = props[k];      
			}
		}
		
		var base_obj = Object.create(Object.prototype);
		
		var obj = Object.create(base_proto);
		
		switch(what){
			case 'N_ITEM':
				obj['to_string'] = function(){
					return this.narcotics.narcotics_name;
				};
			break;
			
			default:
				//throw exception here/
				console.log('Unable to create right prototype.')
				
		}
	
		return obj;
	}
	
	
	var create_object = function create_object(proto,props){
		//get an Array of all the arguments except the first one
        //var args = Array.prototype.slice.call(arguments, 2);
		
        var object = Object.create(proto);
			
		object.init_props(props);
		
        //func.apply(object,args);
 
        return object;
	}
	/**
	*	Gets an object inside an array with a value specified.
	*	
	*	This actually returns an array but if you are certain that this should return only 1
	*   object then use res[0]
	**/
	var get_object = function get_object(array,propertyValue,propertyName){
		var res = null;
		/*$.each(array, function(idx, value){
			console.log(value.propertyName);	
			if(value.propertyName == propertyValue){
				object = value;
				console.log('Nandito');
			}
		});*/

		res = array.filter(function(obj){
			return obj[propertyName] == propertyValue;
		});	
		
		return res;
	}
	//returns -1 if not found.
	var index_of = function index_of(array,propertyValue,propertyName){
		for(var i = 0; i < array.length; i += 1) {
            //if it happens you find bug including this method try chaging == to === and vice versa
			if(array[i][propertyName] == propertyValue) {
				return i;
			}
		}
		return -1;
	}
	/**
	*	Find object inside an array with the whole object specified
	*
	*
	**/
	var is_in_array = function is_in_array(target_array, object){
		return target_array.indexOf(object);
	}
	
	return {
		create_object : create_object,
		get_object : get_object,	
		index_of : index_of,
		proto_type : proto_type,
	}

})();

//Load current User credentials. This is much flexible since you will supply a callback function here
Main.User2 = (function(){

    var loadUserCredentials = function(callback){
        Main.Mod.do_ajax('home/get_user_pnp_id','GET',callback,null);
    }
    
    return {
        loadUserCredentials : loadUserCredentials,
    }

})();
//This should never be use again. Use Main.User2 instead;
Main.User =  (function(){
    
    
    var currentUser = false;
    var isAdmin = false;
    
    var get_current_user = function get_current_user(){
        
        Main.Mod.do_ajax('home/get_user_pnp_id','GET',callback_function,null);
        
    }
    
    var callback_function = function callback_function(data){
        Main.User.currentUser = data.username; 
        Main.User.isAdmin = data.is_admin;
      
    }

    return {
        get_current_user : get_current_user,
        currentUser : currentUser,
        isAdmin : isAdmin,
    }
})();

Main.Audit = (function(){

    var save_audit = function save_audit(description,operation){
        var formdata = new FormData();
        formdata.append('description', description);
        formdata.append('operation', operation);
        Main.Mod.do_ajax('home/save_audit/','POST', function(){}, formdata);
    }
    
    return {
        save_audit : save_audit,
    }

})();

Main.Vars = (function(){
	
    var modal_options = {
		"backdrop" : "static",
		"keyboard" : true,
	}
    
    //after checking the value and performing operation please, return this to false.
    var imageRead = false;
    
	var host = "http://localhost/ssmis/";
	var base_path = "http://localhost/ssmis/";
	
	return{
		modal_options : modal_options,
		host : host,
		base_path : base_path,
        imageRead : imageRead,
	}
    
})();