var AU = AU || {};

AU.Vars = (function(){ 
    
    var audit_tbl;
    
    return {
       
        audit_tbl : audit_tbl,
        
    }
       
})();

AU.Main = (function(){ 
    
    var onLoad = function onLoad(){    
        init_table();
    }
    
    function init_table(){
        AU.Vars.audit_tbl = $('#audit_tbl');
        AU.Vars.audit_tbl.DataTable({
                "deferRender": true,
                "autoWidth": false,
                //for reinitialisation purpose
                destroy: true,
                "aLengthMenu": [[20, 40, 50, 100, -1], [20, 40, 50, 100, "All"]],
                "order": [[ 0, "DESC" ]],
                "ajax" : {
                        url :  Main.Vars.host + "audit_trail/retrieve",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                           
                            { "sWidth": "5%", "aTargets": [ 0 ] },
                            { "sWidth": "25%", "aTargets": [ 1 ] },
                            { "sWidth": "20%", "aTargets": [ 2 ] },
                            { "sWidth": "15%", "aTargets": [ 3 ] },
                            { "sWidth": "35%", "aTargets": [ 4 ] },
                        
                    ],
                    "columns": [
                        { 
                            "data": "at_id",

                        },
                        { 
                            "data": "date",

                        },	
                        {
                            "data": "time",	

                        },
                         {
                            "data": "operation",	

                        },
                        {
                            "data": "description",	

                        },
   
                    ],

                    sPaginationType: "full_numbers",
            });
    }
    
    
    return {
        onLoad : onLoad,
    }

})();