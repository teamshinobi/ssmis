var Settings = Settings || {};


Settings.Settings =  (function(){

    
    var onLoad = function onLoad(){
        Settings.Events.attach();
        init_stationSettings();
        init_templates();
        init_controls();
        init_archsettings();
        $.contextMenu({
            selector: '.item-context',
            callback: function(key, options) {
                 // CM.InventoryDetails.context_action(key, options);
                //PM.Events.attach(PM.Events.events_const.CONTEXT_ACTION,key,options);
                Settings.Events.context(key,options);
            },
            items: {
                "inactive": {name: "Set inactive", icon: 'unapprove'},   
                "active": {name: "Set active", icon: 'approve'},
                "reset": {name: "Reset password", icon: 'reset'},
            }
        });
       
        Main.User.get_current_user();
        //checks if id has finished loading
        var interval = setInterval(function(){ 
           if(Main.User.currentUser != false){
                clearInterval(interval);
                init_users();
           }
        },1000);
        
       
      
    }
    function init_stationSettings(){
        Main.Mod.do_ajax('settings/station_information/','GET',function(data){
            if(data.has_data){
                $('#address').val(data.settings.address);
                $('#chief').val(data.settings.chief);
            }
        }, null);
    }
    
    function init_archsettings(){
        Main.Mod.do_ajax('archives/get_archive_settings/','GET',function(data){
            if(data.has_data){
              $('#archivingSchedule').val(data.settings.archive_frequency);       
            }
        }, null);
    }
     //Controls
    function init_controls(){
        Main.Mod.do_ajax('settings/get_controls', 'GET',function(data){
          
            
            if(data.data.allow_affidavits == 1){
                $('#alowAffidavits').prop('checked', true);
            }
            
            if(data.data.allow_coord_preop == 1){
                $('#allowPreopCoord').prop('checked', true);
            }
            

        },null);
    }
                          
    function init_templates(){
        Main.Mod.do_ajax('settings/get_templates','GET',function(data){
            $('#spot').code(data.data.spot);
            $('#main').code(data.data.main);
            $('#supp').code(data.data.supp);
        },null);
    }
    
    function init_users(){
        var users_tbl =$('#users_tbl');
        users_tbl.DataTable({
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[20, 40, 50, 100, -1], [20, 40, 50, 100, "All"]],
            "order": [[ 0, "DESC" ]],
            "ajax" : {
                    url :  Main.Vars.host + "settings/get_users",
                    type : "GET",
                    dataSrc: function(json) {
                        var rows = [];
                        for (var i=0;i<json.data.length;i++) {
                            var obj = json.data[i]; 
                            if(obj.username == Main.User.currentUser){
                                continue;
                            }
                            rows.push(json.data[i]);
                        }
                        return rows;
                    }
                },
                "aoColumnDefs": [

                        { "sWidth": "5%", "aTargets": [ 0 ] },
                        { "sWidth": "20%", "aTargets": [ 1 ] },
                        { "sWidth": "25%", "aTargets": [ 2 ] },
                        { "sWidth": "15%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 4 ] },

                ],
               "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                    $(nRow).addClass('item-context');	
                   
                   return false;
                },

                "columns": [
                    { 
                        "data": "id",

                    },
                    { 
                        "data": "username",

                    },	
                    {
                       "render": function(data,type,row,meta) {
                            var owner = row.pnp_info.first_name + " " + row.pnp_info.last_name;
                            return owner;
                       }  
                    },
                    {
                        "data": "created_on",	

                    },
                    {
                       "render": function(data,type,row,meta) {
                            return row.active == 1 ? "YES" : "NO";
                       }  
                    },

                ],

                sPaginationType: "full_numbers",
        });
    }
    
    return {
        
        onLoad : onLoad,
    
    }
    
})();

Settings.Events  =  (function(){
    
   var attach = function attach(){
        $('#tempButton').on('click',function(){
            Main.Mod.show_modal('loadingModal',700,-50);
            save_template_settings();
        });
       
         $('#startBackUp').on('click',function(){
            
        });
        checkBoxListener();
        archiveScheduleListener();
        stationSettingsFormListener();
   } 
   
   function stationSettingsFormListener(){
        $('#station_form').on('submit',function(e){
            e.preventDefault();
            var formData = new FormData(this);
            
            Main.Mod.show_modal('loadingModal',700,-50);
            
            Main.Mod.do_ajax('settings/save_station_settings', 'POST', function(data){
               $('#loadingModal').modal('hide');
            }, formData);   
        });
   }
   
   function archiveScheduleListener(){
       $('#archivingSchedule').on('change', function (e) {
            Main.Mod.show_modal('loadingModal',700,-50);
            var valueSelected = this.value;
            Main.Mod.do_ajax('archives/update_archive_freq/' + valueSelected, 'GET',function(data){
                $('#loadingModal').modal('hide');
            },null);
       });
   }
   
   function checkBoxListener(){
        $('#alowAffidavits').change(function() {
            if($(this).is(":checked")) {
                update_control('allow_affidavits', 1);
            } else {
                update_control('allow_affidavits', 0);
            }
        });

        $('#allowPreopCoord').change(function() {
            if($(this).is(":checked")) {
                update_control('allow_coord_preop', 1);
            } else {
                update_control('allow_coord_preop', 0);
            }
        });

    }
    
    function update_control(control, value){
        Main.Mod.do_ajax('settings/update_control/' + control + '/' + value, 'POST', function(data){
            
        });
    }
   
   function save_template_settings(){
        var formdata = new FormData();
        
        formdata.append('spot' , $('#spot').code());
        formdata.append('main' , $('#main').code());
        formdata.append('supp' , $('#supp').code());
        Main.Mod.do_ajax('settings/save_templates/', 'POST', function(data){
            if(data.success){
                $('#loadingModal').modal('hide');
                Main.Mod.show_bootbox_alert(data.message, 'Message', function(){
                    window.location.reload();
                });
            }
             
       },formdata);
   }
     
    
    var context = function context(key,options){
        var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
        switch (key) {
            case 'inactive':
                set_as_inactive(item_id);
                break;
            case 'active':
                set_as_active(item_id);
                break; 
            case 'reset':
                reset(item_id);
                break;           
        } 
    }
    
    function reset(item_id){
        
        var callback = function(){
            Main.Mod.do_ajax('settings/reset_password/' + item_id, 'GET', function(data){
                if (data.success) {
                    Main.Mod.show_bootbox_alert('Use this as new password: <strong>' + data.mix.new_pass + '</strong>','Success!',null);
                } else {
                    Main.Mod.show_bootbox_alert('Unable to reset password! Contact your IT!','Failed!',null);
                }
            
            },null );
        }
        
        Main.Mod.show_bootbox_confirm('Are you sure you want to reset this user\'s password?','Confirmation',callback,null);
       
    }
    
    function set_as_active(item_id){
        var callback = function(){
            Main.Mod.show_modal('loadingModal',700,'');
            Main.Mod.do_ajax('settings/set_active/' + item_id, 'GET', function(data){
            
                if(data.success){
                    $('#users_tbl').DataTable().ajax.reload();
                } else {
                    Main.Mod.show_bootbox_alert("Cannot set user to active.","Error",null);
                }
                $('#loadingModal').modal('hide');
            
            
            },null )
        }
        
        Main.Mod.show_bootbox_confirm("Are you sure you want to set this user as active?","Confirmation",
                                     callback, null);
    }
    
    function set_as_inactive(item_id){
      
        var callback = function(){
            Main.Mod.show_modal('loadingModal',700,'');
            Main.Mod.do_ajax('settings/set_inactive/' + item_id, 'GET', function(data){
            
                if(data.success){
                    $('#users_tbl').DataTable().ajax.reload();
                } else {
                    Main.Mod.show_bootbox_alert("Cannot set user to inactive.","Error",null);
                }
                $('#loadingModal').modal('hide');
            
            },null )
        }
        
        Main.Mod.show_bootbox_confirm("Are you sure you want to set this user as inactive?","Confirmation",
                                     callback, null);
    }
    
    return {
        attach : attach,
        context : context,
    }
})();
