var CM = CM || {};

CM.Vars = (function(){
    
    var team_leader = '';
    var members = new Array();
    var vehicles = new Array();
    var coordinations = new Array();
    var has_unattach = false;
    return {
        team_leader : team_leader,
        members : members, 
        vehicles : vehicles,
        coordinations : coordinations,
        has_unattach : has_unattach,
    }
    
})();


CM.Main = (function(){
    
    var onLoad = function onLoad(get){
        
        initSettings();
        initCoordinations();
        
        CM.Events.attach(CM.Events.events_const.ON_FORM_SAVED);
        CM.Events.attach(CM.Events.events_const.ON_ADD_TEAMLEADER);
        CM.Events.attach(CM.Events.events_const.ON_ADD_MEMBER);
        CM.Events.attach(CM.Events.events_const.ON_ADD_VEHICLE);
        CM.Events.attach(CM.Events.events_const.ON_COORDINATION_VIEWED);
        
        var nowDate = new Date();
        
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        $('#datepicker,#duration_from_date, #duration_to_date').datepicker({ 
            format: 'yyyy-mm-dd',
            startDate: today,   
            endDate: '+1d',  
            autoClose: true,
        });
        
        var timepickers = ['#timepicker_time','#timepicker_to', '#timepicker_from'];
        Main.Init.InitTimePicker(timepickers);  
        
        
         //initiliaze context menu base on user credentials
        Main.User2.loadUserCredentials(function(data){
            
           if(data.is_admin){
                //context menu
                $.contextMenu({
                    selector: '.item-context',
                    callback: function(key, options) {
                       // CM.InventoryDetails.context_action(key, options);
                        CM.Events.attach(CM.Events.events_const.CONTEXT_ACTION,key,options);
                    },
                    items: {
                        "approve": {name: "Mark as approved", icon: 'approve'},
                        "unapprove": {name: "Mark as not approved", icon: 'unapprove'},
                        "view": {name: "View", icon: 'view'},
                        "edit": {name: "Edit", icon: 'edit'},
                        "unattach": {name: "Unattach", icon: 'unattach'}, 
                        "delete": {name: "Delete", icon: 'delete'},  
                        "deny": {name: "Mark as denied", icon: 'deny'}, 
                        "undeny": {name: "Remove denied mark", icon: 'undeny'},   
                    }
                });
        
           } else {
               $.contextMenu({
                    selector: '.item-context',
                    callback: function(key, options) {
                       // CM.InventoryDetails.context_action(key, options);
                        PM.Events.attach(PM.Events.events_const.CONTEXT_ACTION,key,options);
                    },
                    items: {
                        "view": {name: "View", icon: 'view'},
                        "edit": {name: "Edit", icon: 'edit'},
                    }
                });
           }
            
        });
      
        
        if(get){
           init_id();
        }
    
        init_units();
    }
    function initCoordinations(){
        
        $('#coordinations_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for reinitialisation purpose
            destroy: true,
            "ajax" : {
                url :  Main.Vars.host + "forms/getall/coordination",
                type : "GET",
            },
            "aoColumnDefs": [
                    { "bVisible": false, "aTargets": [7] },

                    { "sWidth": "20%", "aTargets": [ 0 ] },
                    { "sWidth": "12%", "aTargets": [ 1 ] },
                    { "sWidth": "10%", "aTargets": [ 2 ] },
                    { "sWidth": "15%", "aTargets": [ 3 ] },
                    { "sWidth": "15%", "aTargets": [ 4 ] },
                    { "sWidth": "20%", "aTargets": [ 5 ] },	
                    { "sWidth": "10%", "aTargets": [ 5 ] },	

            ],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                $(nRow).addClass('item-context');	
                if(aData.has_approved == 1){
                    $(nRow).addClass('approved-row');	
                }

                if(aData.denied == 1){
                    $(nRow).addClass('approved-denied');	
                }

                //add attributes
                $(nRow).attr('data-attach', aData.attached_to);
                
                if( $(nRow).attr('data-attach') == 'NONE'){
                    CM.Vars.has_unattach = true;
                }

            },
            "columns": [
                { 'data' : 'coordination_id'},
                { 'data' : 'control_no' },
                { 'data' : 'date' },
                { 
                    "render": function(data,type,row,meta) { 
                        return row.team_leader_info.rank + ' ' + row.team_leader_info.first_name + ' ' + row.team_leader_info.last_name;
                    }
                },
                { 
                    "render": function(data,type,row,meta) { 
                        return row.created_by_info.rank + ' ' + row.created_by_info.first_name + ' ' + row.created_by_info.last_name;
                    }
                },
                { 

                    "render": function(data,type,row,meta) {  

                        var str = row.duration_from_date + ' ' + row.duration_from_time + ' TO\n ' +
                             row.duration_to_date + ' ' + row.duration_to_time;

                        return str;                                   
                    }

                },    
                { 
                    "render": function(data,type,row,meta) {  
                        return row.attached_to;
                    }
                }, 
                { 
                    "render": function(data,type,row,meta) {  
                       if(row.has_approved == 1){
                            return 'Approved';
                       } else {                      
                            return 'Not';
                       }
                    } 
                },
            ],
            
            "initComplete" : function(){
                console.log(CM.Vars.has_unattach);
            },
            
            "order": [[ 0, "desc" ]],
            sPaginationType: "full_numbers",

        });
             
    }
    
    function initSettings(){

        var callback = function(data){
            if( data.data.allow_coord_preop == 0 ){
                $('#content-container').remove();
            } else {
                $('#disallowed').remove();
            }
        }

        Main.Settings.load(callback);
    }
    function init_id(){
       Main.Mod.do_ajax('forms/id/coordination', 'GET', function(data){
            $('#coord_id').text(data.id);
        }, null);
    
    }
    function init_units(){
        Main.Mod.do_ajax('pnp/retrieveg', 'GET', function(data){
            var select = $('#unit');
            $.each(data.data,function(idx,obj){
                var str = '<option value="' + obj.group_id + '">' + obj.group_name + '</option>';
                select.append(str);
            });
        
        }, null);
    }
    
    return {
        onLoad : onLoad,
    
    }
    
})();


CM.Events = (function(){

    var events_const = {
        'ON_FORM_SAVED' : 1,
        'ON_ADD_TEAMLEADER' : 2,
        'ON_ADD_MEMBER' : 3,
        'ON_COORDINATION_VIEWED' : 4,
        'ON_ADD_VEHICLE' : 5,
        'CONTEXT_ACTION' : 6,
        'ON_REMOVE_MEMBERS' : 7,
        'ON_REMOVE_VEHICLE' : 8,
    }

    function on_add_vehicle(){
        $('#addVehicleButton').off('click').on('click',function(){
          Main.Mod.show_modal('addVehicleModal', 1200, -300);
            
            $('#vehicles_tbl').DataTable({
                //so that all rows are not rendered on init time.
                "deferRender": true,
                "autoWidth": false,
                //for reinitialisation purpose
                destroy: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                //NOTE: We are directly providing group id since we want to get said-sotg members only
                "ajax" : {
                        url :  Main.Vars.host + "vehicles/retrieve",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 4,5 ] },
                            { "sWidth": "20%", "aTargets": [ 0 ] },
                            { "sWidth": "20%", "aTargets": [ 1 ] },
                            { "sWidth": "20%", "aTargets": [ 2 ] },
                            { "sWidth": "25%", "aTargets": [ 3 ] },
                            { "sWidth": "10%", "aTargets": [ 4 ] },
                            { "sWidth": "5%", "aTargets": [ 4 ] },
                    ],
                    "columns": [
                        { 
                            "data": "type",
                        },
                        { 
                            "data": "make",
                        },	
                        { 
                            "data": "color",
                        },	   
                        { 
                            "data": "plate_no",
                        },	
                        {
                            "render": function(data,type,row,meta) {
                                //console.log(base_path + row.file_path_thumb);
                                return '<img style="margin-left:0px; width: 100px;" class="obj-pic" src=' + (Main.Vars.base_path + row.image_path) + '/' + row.vehicle_id + '_thumb.jpg' + ' />';

                            }

                        },
                         {
                             "render": function(data,type,row,meta) {
                                //console.log(base_path + row.file_path_thumb);
                                 
                                 
                                 return '<button style="margin-left:10px;" data-id="' + row.vehicle_id + '" data-type="' + row.type + '" data-make="' + row.make +'" data-plate = "' + row.plate_no + '" data-color="' + row.color + '" title="add" type="button" class="btn btn-default"><i class="fa fa-plus"></i></button>';
                              }

                        },

                    ],

                    sPaginationType: "full_numbers",
            });
            
            //Add button event handling
            $('#vehicles_tbl tbody').off('click').on('click','button',function(){
                var vehicle = {};
                vehicle.vehicle_id = $(this).attr('data-id');
                vehicle.type = $(this).attr('data-type');
                vehicle.make = $(this).attr('data-make');
                vehicle.plate_no = $(this).attr('data-plate');
                vehicle.color = $(this).attr('data-color');

                
                if(CM.Vars.vehicles.indexOf(vehicle.vehicle_id) > -1){
                    Main.Mod.show_bootbox_alert('Already in the list.', 'Message', null);
                } else {
                    CM.Vars.vehicles.push(vehicle.vehicle_id);
                    var tbody = $('#vehicles_tbl_r tbody');
                    
                    var str = '<tr role="row" class="odd">'  +
                        '<td>' + vehicle.type + '</td>' + 
                        '<td>' + vehicle.make + '</td>' + 
                        '<td>' + vehicle.color + '</td>' + 
                        '<td>' + vehicle.plate_no + '</td>' + 
                        '<td >' + '<button data-id="' + vehicle.vehicle_id + '" type="button" class="btn btn-default"><i class="fa fa-trash"></i></button>' + '</td>' + 
                    '</tr>';
                
                    //append to table
                    tbody.append(str);
                    vehicles_has_changed();
                    $('#addVehicleModal').modal('hide');
                }
            });
            
            //add remove button handling
            on_remove_vehicle();
        });
    
    }
    
    function on_remove_vehicle(){
        $('#vehicles_tbl_r tbody').off('click').on('click','button',function(){
            var vehicle_id = $(this).attr('data-id');
            $(this).parent().parent().empty();
            //actual removing          
            CM.Vars.vehicles.splice(CM.Vars.vehicles.indexOf(vehicle_id),1);
            vehicles_has_changed();
        });
    
    }
   
    function on_coordination_viewed(){
        $('#viewCoordinations').on('click', function(){
            Main.Mod.show_modal('showCoordinationsModal', 1200, -300);
        });
    }
    function on_add_member(){
      $('#addMembersButton').on('click',function(){
         Main.Mod.show_modal('addMembersModal', 1200, -300);
            
            $('#members_tbl').DataTable({
                //so that all rows are not rendered on init time.
                "deferRender": true,
                "autoWidth": false,
                //for reinitialisation purpose
                destroy: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                //NOTE: We are directly providing group id since we want to get said-sotg members only
                "ajax" : {
                        url :  Main.Vars.host + "casereport/activepnps",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 3,4 ] },
                            { "sWidth": "25%", "aTargets": [ 0 ] },
                            { "sWidth": "25%", "aTargets": [ 1 ] },
                            { "sWidth": "35%", "aTargets": [ 2 ] },
                            { "sWidth": "10%", "aTargets": [ 3 ] },
                            { "sWidth": "5%", "aTargets": [ 3 ] },
                    ],
                    "columns": [
                        { 
                            "data": "pnp_id",


                        },	
                        {
                            "data": "rank",	

                        },
                        { 	
                            "render": function(data,type,row,meta) {

                                return row.first_name + " " + row.last_name;
                            }

                        },
                        { 

                            "render": function(data,type,row,meta) {
                              return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';

                            }
                        },
                        {
                            data: null,
                            className: "center",
                            "render": function(data,type,row,meta) {
                                //console.log(base_path + row.file_path_thumb);
                                return  '<button style="margin-left: 10px;" data-name="' + row.first_name + ' ' + row.last_name +' "data-value="' +  row.pnp_id +'" style="img style="padding-left: 0px; padding-right:0px;" class="btn btn-primary btn-md action"><span class="glyphicon glyphicon-plus"></span></button>';

                            }


                        }	
                    ],

                    sPaginationType: "full_numbers",

            });
          
            //add event handling
            $('#members_tbl tbody').off().on( 'click', '.action', function () {
			    var selected_id = $(this).attr('data-value');
                var name = $(this).attr('data-name');
               
                if(CM.Vars.members.indexOf(selected_id) > -1){
                    Main.Mod.show_bootbox_alert('Already in the list.', 'Message', null);
                } else {
                    CM.Vars.members.push(selected_id);
                    if(CM.Vars.members.length == 1){
                        $('#membersList').empty();
                        $('#membersList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name +'<span class="pull-right">' + 
                         '<button type="button" data-value="' + selected_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                    } else {
                        $('#membersList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name  +'<span class="pull-right">' + 
                         '<button type="button" data-value="' + selected_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                    }
                    members_has_changed();
                    $('#addMembersModal').modal('hide');
                }
               
            });
         
            on_remove_members();
            
      });
    }
    
    //add remove button handling
    function on_remove_members(){
        $('#membersList').off('click').on('click','button',function(){
            
            var id = $(this).attr('data-value');
            var parent_li = $(this).parent().parent();
            parent_li.remove();	
            
            //remove the item in array
            var i = CM.Vars.members.indexOf(id);
            //actual removing          
            CM.Vars.members.splice(i,1);
            
            if(CM.Vars.members.length == 0){
                $('#membersList').append('<li class="list-group-item"> No police yet.</li>');
            }
            
            members_has_changed();
            
        });  
    }
    
    function on_add_teamleader(){
        $('#addTeamLeaderButton').on('click',function(){
             Main.Mod.show_modal('addTeamLeaderModal', 1200, -300);
            
            
            $('#pnp_tbl').DataTable({
                //so that all rows are not rendered on init time.
                "deferRender": true,
                "autoWidth": false,
                //for reinitialisation purpose
                destroy: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "ajax" : {
                        url :  Main.Vars.host + "casereport/activepnps",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 3,4 ] },
                            { "sWidth": "25%", "aTargets": [ 0 ] },
                            { "sWidth": "25%", "aTargets": [ 1 ] },
                            { "sWidth": "35%", "aTargets": [ 2 ] },
                            { "sWidth": "10%", "aTargets": [ 3 ] },
                            { "sWidth": "5%", "aTargets": [ 3 ] },
                    ],
                    "columns": [
                        { 
                            "data": "pnp_id",


                        },	
                        {
                            "data": "rank",	

                        },
                        { 	
                            "render": function(data,type,row,meta) {

                                return row.first_name + " " + row.last_name;
                            }

                        },
                        { 

                            "render": function(data,type,row,meta) {
                                return '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src="' + (Main.Vars.base_path + row.file_path) + '.jpg" />';
                            }
                        },
                        {
                            data: null,
                            className: "center",
                            "render": function(data,type,row,meta) {
                                //console.log(base_path + row.file_path_thumb);
                                return  '<button style="margin-left: 10px;" data-name="' + row.first_name + ' ' + row.last_name +' "data-value="' +  row.pnp_id +'" style="img style="padding-left: 0px; padding-right:0px;" class="btn btn-primary btn-md action"><span class="glyphicon glyphicon-plus"></span></button>';

                            }


                        }	
                    ],

                    sPaginationType: "full_numbers",

            });
            
            //add event handling
            $('#pnp_tbl tbody').off().on( 'click', '.action', function () {
			    var selected_id = $(this).attr('data-value');
                var name = $(this).attr('data-name');
                $('#team_leader').val(name);
                CM.Vars.team_leader = selected_id;
                $('#addTeamLeaderModal').modal('hide');
            });
            
        });
    }
    
    function on_form_saved(){
      
        //handle form submission
        $("#save_coordination_form").on('submit',(function(e){
            e.preventDefault();
            var result = validate_form();
            
            if(CM.Vars.has_unattach){
                 Main.Mod.show_bootbox_alert('Cannot create another coordination form. There is still unattach coordination.', 'Message', null);    
            } else {
                if(result.isOk){
                    Main.Mod.show_modal('loadingModal', 650, '');
                    var formdata = new FormData(this);
                    formdata.append('coordination_id',$('#coord_id').text().trim());
                    formdata.append('team_leader', CM.Vars.team_leader.trim());
                    formdata.append('members', JSON.stringify(CM.Vars.members));
                    formdata.append('vehicles', JSON.stringify(CM.Vars.vehicles));
                    formdata.append('control_no', '');

                    Main.Mod.do_ajax('forms/save/coordination', 'POST', function(data){
                        $('#loadingModal').modal('hide');
                        if(data.success){
                            Main.Mod.show_bootbox_alert(data.message, 'Success', function(){
                                location.reload();
                            });             
                        } else {
                             Main.Mod.show_bootbox_alert(data.message, 'Error', null);
                        }

                    },formdata);
                } else {
                      var message = '<ul>';

                    $.each(result.messages,function(idx,obj){
                        message += '<li>' + obj + '</li>';
                    });

                    message += '</ul>';

                    Main.Mod.show_bootbox_alert(message, 'Please complete the following: ', null);

                }
            }
      }));

    }
    
    function validate_form(){
        
        var toReturn = true;
        var vMessages = new Array();
        
        if($('#date').val() == '' || $('#duration_from').val() == '' || $('#duration_to').val() == '' ){
            vMessages.push("Dates cannot be empty.");
            toReturn = false;
        }
        
        if(CM.Vars.team_leader == ''){
            vMessages.push('Team Leader must be set.');
            toReturn = false;
        }
        
        if(CM.Vars.members.length < 1){
            vMessages.push('Provide atleast 1 member.');
            toReturn = false;
        }
        
        if(CM.Vars.vehicles.length < 1){
            vMessages.push('Provide atleast 1 vehicle.');
            toReturn = false;
        }
        
        var result = {};
        result.isOk = toReturn;
        result.messages = vMessages;
        
        return result;
        
    }
    /*====================================================================================
    *                               CONTEXT ACTION
    *  
    *===================================================================================*/
    function approve(id){
        Main.Mod.show_modal('showControlInputModal', 650, '');
        $('#continueApprovingButton').off('click').on('click',function(){
            var con_callback = function(){

                var callback = function(data){
                    Main.Mod.show_bootbox_alert(data.message, 'Message',function(){
                          $('#showCoordinationsModal').modal('hide'); 
                      });      
                }

                Main.Mod.do_ajax('forms/approve/' + id + '/coordination/' + $('#cn').val().trim() , 'GET' , callback, null);
            };

            Main.Mod.show_bootbox_confirm("Are you sure you want to mark this as approved?", "Confirmation", con_callback,null);
   
        });
     }
    
    function unapprove(id){
        var callback = function(){

        var callback = function(data){
            Main.Mod.show_bootbox_alert(data.message, 'Message',function(){
                  $('#showCoordinationsModal').modal('hide'); 
              });      
            }

            Main.Mod.do_ajax('forms/unapprove/' + id + '/coordination' , 'GET' , callback, null);
        };

        Main.Mod.show_bootbox_confirm("Are you sure you want to mark this as unapproved?", "Confirmation", callback,null);
    }
    
    function view(id){
         window.open(Main.Vars.host + "forms/coordination_pdf/" + id , '_blank');
        
    }
    
    function edit(id){
        Main.Mod.show_modal('loadingModal',750,-50);
        Main.Mod.do_ajax('forms/get_created_by/coordination/' + id, 'GET', function(data){  
            
            Main.User.get_current_user();
    
            var creator = data.creator.created_by;
            
            var interval = setInterval(function(){ 
               if(Main.User.currentUser != false){
                    clearInterval(interval);
                    if(Main.User.currentUser == creator){
                        window.location.assign(Main.Vars.host + "forms/edit/coordination/" + id);
                    } else {
                        $('#loadingModal').modal('hide');
                        Main.Mod.show_bootbox_alert('You cannot edit this coordination form. You did not create it.','Message',null);
                    }
               }
            },1000);
            
        },null);
    }
    
    function unattach(id, case_id){
       if(case_id == 'NONE'){
            Main.Mod.show_bootbox_alert('There is nothing to unattach.', 'Message', null);
       } else {
            var callback = function(){
                var formdata = new FormData();
                formdata.append('id', id);
                formdata.append('case_id', case_id);
                Main.Mod.do_ajax('forms/unattach/coordination', 'POST', function(data){ 
                
                
                
                
                },formdata);
                
            }
            
            Main.Mod.show_bootbox_confirm('Are you sure you want to unattach this item?',
                                         'Confirmation', callback);
       }
    }
    
    function del(id, case_id) {
        if(case_id != 'NONE'){
            Main.Mod.show_bootbox_alert('Cannot coordination. This was attached to another case report. Remove attachment first.',
                                   'Message',null);
        } else {
            Main.Mod.show_modal('loadingModal',750,-50);
            
            Main.Mod.do_ajax('forms/get_created_by/coordination/' + id, 'GET', function(data){  
           
                Main.User.get_current_user();

                var creator = data.creator;
                
                var interval = setInterval(function(){ 
                   if(Main.User.currentUser != false){
                        clearInterval(interval);
                        $('#loadingModal').modal('hide');
                        //if the user is the one who creates it and or admin, allow delete
                        if(Main.User.currentUser == creator.created_by || Main.User.isAdmin ){
                                $('#loadingModal').modal('hide');
                                if(case_id != 'NONE'){
                                    Main.Mod.show_bootbox_alert('Cannot delete coordination form. This was attached to another case report. Remove attachment first.',
                                                               'Message',null);
                                } else {
                                    Main.Mod.show_bootbox_confirm('Are you sure you want to delete this coordination form?', 'Confirmation', function(){
                                        //show loading when ajaxing delete
                                        Main.Mod.show_modal('loadingModal',750,-50);
                                        var callback = function(data){
                                            if(data.success){
                                                Main.Mod.show_bootbox_alert(data.message, 'Success',function(){
                                                    location.reload();
                                                });

                                            }else{
                                                $('#loadingModal').modal('hide');
                                                Main.Mod.show_bootbox_alert(data.message, 'Error', null);     
                                            }
                                        }
                                       Main.Mod.do_ajax('forms/delete/coordination/' + id, 'GET', callback, null);
                                        
                                    },null);
                                }
                        } else {
                            $('#loadingModal').modal('hide');
                            Main.Mod.show_bootbox_alert('You cannot delete this Coordination Form. You did not create it.','Message',null);
                        }
                   }
                },1000); //end interval   
            },null);         
        }
    }
    
    function deny(id){
       Main.Mod.do_ajax('forms/mark_as_denied/' + id + '/coordination', 'GET',function(data){
            if(data.success){
                Main.Mod.show_bootbox_alert('Coordination has been denied!',
                                           'Message', function(){
                    location.reload();
                });

            } else {
                Main.Mod.show_bootbox_alert('Unable to mark as deny.',
                                           'Message', null);
            }
       
       },null);
    }
    
    function undeny(id){
         Main.Mod.do_ajax('forms/mark_as_undenied/' + id + '/coordination', 'GET',function(data){
                if(data.success){
                Main.Mod.show_bootbox_alert('Removed denied mark!',
                                           'Message', function(){
                    location.reload();
                });

            } else {
                Main.Mod.show_bootbox_alert('Unable to remove denied mark.',
                                           'Message', null);
            }
       
       },null);
    }
    
    //right-click menu events handling
	var context_action = function context_action(key,options){
		var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
		switch (key) {
            case 'approve' :
                if(tr.hasClass('approved-row')){
                    Main.Mod.show_bootbox_alert("This coordination form has already been approved!", "Message", null); 
                } else {
                    approve(item_id);
                }
               break;

            case 'unapprove' :
                if(tr.hasClass('approved-row')){
                    unapprove(item_id);
                } else {
                   Main.Mod.show_bootbox_alert("This coordination form is not yet approved.", "Message", null); 

                }
               break;

            case 'view':
                 view(item_id);
                 break;

            case 'edit':
                edit(item_id);
                break;
            case 'unattach':
                //attach(item_id);
                var case_id = options.$trigger.attr('data-attach');
                //var act_tr = tr[1];
                unattach(item_id, case_id);
                break;
            case 'delete':
                var case_id = options.$trigger.attr('data-attach');
                del(item_id,case_id);
                break;
                
            case 'deny':
                var case_id = options.$trigger.attr('data-attach');
                
                if(case_id == 'NONE') {
                    deny(item_id);
                } else {
                    Main.Mod.show_bootbox_alert("This report is attached to a case report. Cannot mark as denied.","Message",null);
                }
             
                break; 
            case 'undeny':
                undeny(item_id);
                break;
		   default :
			   break;
	   }           
	}
    
    function members_has_changed(){
        try{
            VCM.Vars.has_members_changed = true;
        
        } catch(e) { 
            //console.log('Cannot trigger change!');
        }  
    }
    function vehicles_has_changed(){
        try{
            VCM.Vars.has_vehicles_changed = true;
        
        } catch(e) { 
            //console.log('Cannot trigger change!');
        }      
    }
    var attach = function attach(type,key,options){
        switch(type){
            case events_const.ON_FORM_SAVED:
                on_form_saved();
                break;  
            case events_const.ON_ADD_TEAMLEADER:
                on_add_teamleader();
                break;  
            case events_const.ON_ADD_MEMBER:
                on_add_member();
                break;  
            case events_const.ON_COORDINATION_VIEWED:
                on_coordination_viewed();
                break;
            case events_const.ON_ADD_VEHICLE:
                on_add_vehicle();
                break;
            case events_const.CONTEXT_ACTION:
                context_action(key,options);
                break;
            case events_const.ON_REMOVE_MEMBERS:
                on_remove_members();
                break;
            case events_const.ON_REMOVE_VEHICLE:
                on_remove_vehicle();
                break;
            default:
                alert('Cannot attach event. Contact your IT support.');
        }
    }
    
    return {
        attach : attach,
        events_const : events_const,
    
    }
})();