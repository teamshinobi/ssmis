var PM = PM || {};

PM.ProfileModule = (function(){
	var id;
	var titleHead;
	var formData = new FormData();
	
    
    var onLoad = function onLoad(){
       //get pnp_id
        Main.Mod.do_ajax('home/get_user_pnp_id/', 'GET', function(data){
            load_cases(data.username);
        }, null);
    }
    
    function load_cases(id){
        var iCases = $('#investigatorCases');
        var aCases = $('#arrestingCases');
        
        Main.Mod.do_ajax('pnp/as_arresting/' + id, 'GET', function(data){
          if(data.has_data){
                
                $.each(data.result, function(idx,obj){
                    var str = '<li>';
                    str += 'Case BLOTTER : ' + obj.blotter_entry_nr + ' with CASE ID of ' + '<a href="' + Main.Vars.host +   
                                    'viewcasereports/view/' + obj.case_id + 
                                    '">' + obj.case_id + '</a>';
                    
                    str+='</li>';
                    
                    aCases.append(str);
                });
            } else {
                var str = '<li>' + data.message + '</li>';
                aCases.append(str);
            } 
           
        
        },null);
        
        
        Main.Mod.do_ajax('pnp/as_investigator/' + id, 'GET', function(data){
          
          
            
              if(data.has_data){
                
                $.each(data.result, function(idx,obj){
                    var str = '<li>';
                    
                    str += 'Case BLOTTER : ' + obj.blotter_entry_nr + ' with CASE ID of ' + '<a href="' + Main.Vars.host +   
                                    'viewcasereports/view/' + obj.case_id + 
                                    '">' + obj.case_id + '</a>';
                    
                    str+='</li>';
                    
                    iCases.append(str);
                });
            } else {
                var str = '<li>' + data.message + '</li>';
                iCases.append(str);
            }
        
        },null);
    }
    
	var retrieve = function retrieve(title){
		$('.messagesList').empty();
		if(title == 'inbox'){
			titleHead = title;
			$('.msgTitle a').removeClass('titleActive');
			$('#inbox').addClass('titleActive');
			$.ajax({
				type: 'GET',
				url: Main.Vars.host + 'messages/inbox',
				contentType: false,
				cache: false,   
				processData:false,
				dataType: 'json',
				success: function(data){
					if(data.error == true){
						$('.messagesList').append('<p class="text-muted">Your Inbox is Empty</p');
					} else {
						var messages = [];
						
						$.each(data.data, function(i, message){
							if(message.msg_status == 'unread'){
								messages.push(
								'<li data-id="'+ message.msg_id +'"><span class="from"><span></span><i id="msgStatus" class="fa fa-envelope"></i>&nbsp;&nbsp;'+ message.senderName + '</span><span class="subject">' +  message.subject + '</span><span class="title">' +  message.content + '</span><span class="date">' + message.msg_date + '/' + message.msg_time +'</span></li>'
								);
							}else if(message.msg_status == 'read'){
								messages.push(
								'<li data-id="'+ message.msg_id +'"><span class="from"><span></span><i id="msgStatus" class="fa fa-envelope-o"></i>&nbsp;&nbsp;'+ message.senderName + '</span><span class="subject">' +  message.subject + '</span><span class="title">' +  message.content + '</span><span class="date">' + message.msg_date + '/' + message.msg_time +'</span></li>'
								);
							}
							
						});
						$('.messagesList').append( messages.join('') );
					}
				}
			});
			
		}else if(title == 'drafts'){
			titleHead = title;
			$('.msgTitle a').removeClass('titleActive');
			$('#drafts').addClass('titleActive');
			$.ajax({
				type: 'GET',
				url: Main.Vars.host + 'messages/drafts',
				contentType: false,
				cache: false,   
				processData:false, 
				dataType: 'json',
				success: function(data){
					if(data.error == true){
						$('.messagesList').append('<p class="text-muted">Your drafts is Empty</p');
					} else {
						var messages = [];
						var sender = data.senderName;
						
						$.each(data.data, function(i, message){
							messages.push(
							'<li data-id="'+ message.msg_id +'"><span class="from"><span></span>&nbsp;'+ message.senderName + '</span><span class="subject">' +  message.subject + '</span><span class="title">' +  message.content + '</span><span class="date">' + message.msg_date + '/' + message.msg_time +'</span></li>'
							);
						});
						$('.messagesList').append( messages.join('') );
					}
				}
			});
			
		}else if(title == 'sentItems'){	
			titleHead = title;
			$('.msgTitle a').removeClass('titleActive');
			$('#sentItems').addClass('titleActive');
			$.ajax({
				type: 'GET',
				url: Main.Vars.host + 'messages/sents',
				contentType: false,
				cache: false,   
				processData:false, 
				dataType: 'json',
				success: function(data){
					if(data.error == true){
						$('.messagesList').append('<p class="text-muted">Your Sent items is Empty</p');
					} else {
						var messages = [];
						var sender = data.senderName;
						var recipient = data.recipientName;
						
						$.each(data.data, function(i, message){
							messages.push(
							'<li data-id="'+ message.msg_id +'"><span class="from"><span></span>From: &nbsp;'+ message.senderName + '</span><span class="from"><span></span>To:&nbsp;'+ message.recipientName + '</span><span class="subject">' +  message.subject + '</span><span class="smallTitle">' +  message.content + '</span><span class="date">' + message.msg_date + '/' + message.msg_time +'</span></li>'
							);
						});
						$('.messagesList').append( messages.join('') );
					}
				}
			});
		}
		$('.messagesList').off('click').on('click','li', function(e){
			id = $(this).attr('data-id');
			$('#viewContent').fadeIn(700);
			$('.messagesList li').removeClass('msgActive');
			$(this).addClass('msgActive');
			
			$(this).find('i').removeClass('fa-envelope').addClass('fa-envelope-o');
			
			formData.append("id", id);
			formData.append("column", "msg_status");
			formData.append("status", "read");

			if(title == 'inbox'){
				$.ajax({
					type: 'POST',
					url: Main.Vars.host + 'messages/update',
					contentType: false,
					cache: false,   
					processData:false,
					data: formData,
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							$.ajax({
								type: 'GET',
								url: Main.Vars.host + 'messages/view/' + id,
								contentType: false,
								cache: false,   
								processData:false, 
								dataType: 'json',
								success: function(data){
									if(data.error == true){
										alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
									} else {
										var date = new Date();
										var newDate = new Date(data.data[0].msg_date);
										
										//-----------------------------------Date Style--------------------------------------->
										if(newDate.getMonth() == date.getMonth() && newDate.getDay() == date.getDay()){
											$('#message-date').text('Today');
										}else if(newDate.getMonth() == date.getMonth() && date.getDay() - newDate.getDay() == 1){
											$('#message-date').text('Yesterday');
										}else{
											$('#message-date').text(data.data[0].msg_date);
										}
										$('#message-subject').text(data.data[0].subject);
										$('#message-content').text(data.data[0].content);
										$('#message-recipient').text('To: ' + data.recipientName);
										$('#message-sender').text('From: ' + data.senderName);
										$('#message-time').text(data.data[0].msg_time);
									}
								}
							});
						}
					}
				});
			}else{
				$.ajax({
					type: 'GET',
					url: Main.Vars.host + 'messages/view/' + id,
					contentType: false,
					cache: false,   
					processData:false, 
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							var date = new Date();
							var newDate = new Date(data.data[0].msg_date);
							
							//-----------------------------Date style--------------------------------------->
							if(newDate.getMonth() == date.getMonth() && newDate.getDay() == date.getDay()){
								$('#message-date').text('Today');
							}else if(newDate.getMonth() == date.getMonth() && date.getDay() - newDate.getDay() == 1){
								$('#message-date').text('Yesterday');
							}else{
								$('#message-date').text(data.data[0].msg_date);
							}
							
							if(data.recipientName == ''){
								$('#receiver').css('display', 'none');
							}else{
								$('#message-subject').text(data.data[0].subject);
								$('#message-content').text(data.data[0].content);
								$('#message-recipient').text('From: ' + data.senderName);
								$('#message-sender').text('To: ' + data.recipientName);
								$('#message-time').text(data.data[0].msg_time);
							}
						}
						$("#continueMsg").off('click').on('click', function(){
							$('#continueModal').modal('toggle');
							$('#recipient_c').val(data.data[0].recipient);
							$('#subject_c').val(data.data[0].subject);
							$('#msgcontent_c').val(data.data[0].content);
						});
					}
				});
			}
			
		});
		$('#message-subject').text('');
		$('#message-content').text('');
		$('#message-recipient').text('');
		$('#message-sender').text('');
		$('#message-time').text('');
	}
	
	var save = function save(formdata){
		
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'messages/add',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				if(data.error == true){
					alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
				} else {
					$('#composeModal').modal('hide');
					alert('Successfully saved!');
					$('#recipient').val('');
					$('#subject').val('');
					$('#msgcontent').val('');
				}
				if(data.name == 'Sent'){
					$('#sentItems').trigger('click');
				}else{
					$('#drafts').trigger('click');
				}
				
			}
		});
	};
	
	var remove = function(){
		var callback_func = function callback_func(){
			$('#viewModal').modal('hide');
			bootbox.alert(
			{ 	
				message: "Message Successfully Deleted!",
				title: "Message",
			});
			if(titleHead == 'inbox'){
				formData.append("id", id);
				formData.append("column", "inbox_status");
				formData.append("status", "deleted");
				$.ajax({
					type: 'POST',
					url: Main.Vars.host + 'messages/deleteMessage',
					contentType: false,
					cache: false,   
					processData:false, 
					data: formData,
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							$('.messagesList').find('[data-id="'+id+'"]').remove();
							$('#viewContent').css('display','none');
							$('#inbox').trigger('click');
						}
					}
				});
			}else if (titleHead == 'sentItems'){
				formData.append("id", id);
				formData.append("column", "sent_item_status");
				formData.append("status", "deleted");
				$.ajax({
					type: 'POST',
					url: Main.Vars.host + 'messages/deleteMessage',
					contentType: false,
					cache: false,   
					processData:false, 
					data: formData,
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							$('.messagesList').find('[data-id="'+id+'"]').remove();
							$('#viewContent').css('display','none');
							$('#sentItems').trigger('click');
						}
					}
				});
			}else if(titleHead == 'drafts'){
				formData.append("id", id);
				formData.append("column", "draft_status");
				formData.append("status", "deleted");
				$.ajax({
					type: 'POST',
					url: Main.Vars.host + 'messages/deleteMessage',
					contentType: false,
					cache: false,   
					processData:false, 
					data: formData,
					dataType: 'json',
					success: function(data){
						if(data.error == true){
							alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
						} else {
							$('.messagesList').find('[data-id="'+id+'"]').remove();
							$('#viewContent').css('display','none');
							$('#drafts').trigger('click');
						}
					}
				});
			}
		};
		Main.Mod.show_bootbox_confirm('Are you sure you want to delete this message?', 'Confirmation', 'YES', 'NO', callback_func);
	}
	
	var updateDraft = function(formdata){
		formdata.append("id", id);
		$.ajax({
            type: 'POST',
            url: Main.Vars.host + 'messages/updateDraft',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
			success: function(data){
				if(data.error == true){
					alert('There were errors in saving the record. \nError Message: ' + data.db_err.db_err_message);
				} else {
					$('#continueModal').modal('hide');
					alert('Successfully saved!');
					$('#recipient').val('');
					$('#subject').val('');
					$('#msgcontent').val('');
				}
				if(data.name == 'Sent'){
					$('#sentItems').trigger('click');
				}else{
					$('#drafts').trigger('click');
				}
			}
		});
	}
	
	return{
		retrieve : retrieve,
		save : save,
		remove : remove,
		updateDraft : updateDraft,
        onLoad : onLoad,
	}
	
})();