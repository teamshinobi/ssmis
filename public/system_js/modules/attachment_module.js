var AM = AM || {};

AM.Main = (function(){
    
    var onLoad = function onLoad(){
        AM.Events.attach(AM.Events.events_const.ON_ATTACHMENT_BUTTON);
        AM.Events.attach(AM.Events.events_const.ON_DOCUMENTS_CHANGED);
        AM.Events.removeAttachmentsListener();
    }
    
    //scan files in temps folder
    var scan_documents = function scan_documents(case_id){
        Main.Mod.do_ajax('casereport/scan_temp_dir/' + case_id,'GET', function(data){
            data.files.laboratory.splice(0,2);
            render_documents(data.files.laboratory,case_id,'labDocuments','laboratory');
            
            data.files.medical.splice(0,2);
            render_documents(data.files.medical,case_id,'medDocuments','medical');
            
            data.files.search.splice(0,2);
            render_documents(data.files.search,case_id,'swDocuments','search');
            
            data.files.warrant.splice(0,2);
            render_documents(data.files.warrant,case_id,'waDocuments','warrant');
            
            data.files.others.splice(0,2);
            render_documents(data.files.others,case_id,'othersDocuments','others');
        }, null);  
    }
    function render_documents(files,case_id,selector,folder){
       
        var documents = $('#' + selector);
        documents.empty();
        var str =  '<div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">';

        $.each(files, function(idx,obj){
            //dont use thumb images
            if(obj.indexOf('thumb') < 0){
                str += '<img class="doc-pic" src="' + Main.Vars.host + 'public/resources/temps/case/' +case_id + '/' + folder + '/' + obj+ '" />';
            }        
        });
        str +='</div>'; 
        
        documents.append(str);  
    }
    
    return {
        
        onLoad : onLoad,
        scan_documents : scan_documents,
    }
    
    
})();

AM.Events = (function(){
    
    var events_const = {
        'ON_ATTACHMENT_BUTTON' : 1,
        'ON_DOCUMENTS_CHANGED' : 2,
    }
    
    function on_attachment_button(){
        spotreport();
        preoperation();
        coordination();
        main_affidavit();
        supplemental_affidavit();
    }
    /*================================================================================
    *                        SUPPLEMENTAL ATTACHMENT
    *   
    *===============================================================================*/
    function supplemental_affidavit(){
        $('#suppButton').on('click',function(){
            var action = $(this).attr('data-action');
            if(action == 'attach'){
                Main.Mod.show_modal('showSuppAffidavitsModal',1200,-300);
                
                $('#supp_tbl').DataTable({
                    //so that all rows are not rendered on init time.
                    "deferRender": true,
                    "autoWidth": false,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    //for reinitialisation purpose
                    destroy: true,
                    "ajax" : {
                        url :  Main.Vars.host + "forms/getall_un/supplemental",
                        type : "GET",
                    },
                     
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 3 ] },
                            { "sWidth": "25%", "aTargets": [ 0 ] },
                            { "sWidth": "25%", "aTargets": [ 1 ] },
                            { "sWidth": "25%", "aTargets": [ 2 ] },
                            { "sWidth": "10%", "aTargets": [ 3 ] },	
                         
                    ],
                    "columns": [
                        { 'data' : 'affidavit_id'},
                        { 'data' : 'date_created'},
                        { 
                            "render": function(data,type,row,meta) {                  
                                 var str = '<ul style="list-style:none; margin-left:-20px;">';
                                $.each(row.narrators, function(i, obj){                            
                                    str += '<li>' + obj.narratorName + '</li>';                        
                                });
                                str += '</ul>';
                                return str;
                            } 
                        },
                        { 
                            "render": function(data,type,row,meta) {                  
                                var str= '';
                                str+= '<button data-action="attach" data-attachedto = "' + row.attached_to+ '"style="margin-right: 5px;margin-left:10px;" title="Attach" class="btn btn-info action" data-value="' + row.affidavit_id + '"><i class="fa fa-paperclip"></i></button>';
                                str += '<button data-action="view" style="margin-right: 5px;" title="View" class="btn btn-info action" data-value="' + row.affidavit_id + '"><i class="fa fa-eye"></i></button>';
                                
                                return str;
                            } 
                        },
                          
                    ],
                    "order": [[ 0, "desc" ]],
                    sPaginationType: "full_numbers",
                });
                
                 $('#supp_tbl').off('click').on('click','.action',function(e){
                    var id = $(this).attr('data-value').trim();
                    var attached_to = $(this).attr('data-attachedto').trim();
                    var action = $(this).attr('data-action');


                    switch(action){
                        case 'attach':
                            attach_supp(id);
                            break;
                        case 'unattach':
                            alert('Unattach!');
                            break;

                        default: 
                            alert('Cannot handle event. Contact IT Admini');
                    }
                 });
                //end if
            } else {
                removed_attached_supp();
            }
        });
    }
    
    function attach_supp(id){
        var callback = function(){
            CSR.Vars.supp_id = id;
            $('#showSuppAffidavitsModal').modal('hide');  
            Main.Mod.show_bootbox_alert('Supplemental Affidavit has been marked for attachment. Please save the case report to complete attachment','Message', null);
            update_attachment_ui('supp',id);
            console.log(CSR.Vars.supp_id);
        }
        Main.Mod.show_bootbox_confirm('Are you sure you want to attach this supplemental affidavit? <br/> Please save the case report to complete attachment.',   
                                      'Confirmation',callback,null);
    }
     function removed_attached_supp(){
        CSR.Vars.supp_id_e = CSR.Vars.supp_id;
        CSR.Vars.supp_id = 'NONE';
    
        $('#supp-id').hide();
        $('#supp-id').parent().removeClass('attachment-active');
        $('#suppButton i').removeClass().addClass('fa fa-paperclip'); 
        $('#suppButton').attr('data-action', 'attach');
    }
    /*================================================================================
    *                         MAIN AFFIDAVIT ATTACHMENT
    *   
    *===============================================================================*/
    function main_affidavit(){
         $('#mainButton').on('click',function(){
            var action = $(this).attr('data-action');
             
            if(action == 'attach'){
                Main.Mod.show_modal('showAffidavitsModal',1200,-300);
                
                
                 $('#main_tbl').DataTable({
                    //so that all rows are not rendered on init time.
                    "deferRender": true,
                    "autoWidth": false,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    //for reinitialisation purpose
                    destroy: true,
                    "ajax" : {
                        url :  Main.Vars.host + "forms/getall_un/main",
                        type : "GET",
                    },
                     
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 3 ] },
                            { "sWidth": "25%", "aTargets": [ 0 ] },
                            { "sWidth": "25%", "aTargets": [ 1 ] },
                            { "sWidth": "25%", "aTargets": [ 2 ] },
                            { "sWidth": "10%", "aTargets": [ 3 ] },	
                         
                    ],
                    "columns": [
                        { 'data' : 'affidavit_id'},
                        { 'data' : 'date_created'},
                        { 
                            "render": function(data,type,row,meta) {                  
                                 var str = '<ul style="list-style:none; margin-left:-20px;">';
                                $.each(row.narrators, function(i, obj){                            
                                    str += '<li>' + obj.narratorName + '</li>';                        
                                });
                                str += '</ul>';
                                return str;
                            } 
                        },
                        { 
                            "render": function(data,type,row,meta) {                  
                                var str= '';
                                str+= '<button data-action="attach" data-attachedto = "' + row.attached_to+ '"style="margin-right: 5px;margin-left:10px;" title="Attach" class="btn btn-info action" data-value="' + row.affidavit_id + '"><i class="fa fa-paperclip"></i></button>';
                                str += '<button data-action="view" style="margin-right: 5px;" title="View" class="btn btn-info action" data-value="' + row.affidavit_id + '"><i class="fa fa-eye"></i></button>';
                                
                                return str;
                            } 
                        },
                          
                    ],
                    "order": [[ 0, "desc" ]],
                    sPaginationType: "full_numbers",
                });
                     
               $('#main_tbl').off('click').on('click','.action',function(e){
                    var id = $(this).attr('data-value').trim();
                    var attached_to = $(this).attr('data-attachedto').trim();
                    var action = $(this).attr('data-action');


                    switch(action){
                        case 'attach':
                            attach_main(id);
                            break;
                        case 'unattach':
                            alert('Unattach!');
                            break;

                        default: 
                            alert('Cannot handle event. Contact IT Admini');
                    }
                 });
                //end if
            } else {
                removed_attached_main();
            }
             
         });
    }
    
    function attach_main(id){
        var callback = function(){
            CSR.Vars.main_id = id;
            $('#showAffidavitsModal').modal('hide');  
            Main.Mod.show_bootbox_alert('Main Affidavit has been marked for attachment. Please save the case report to complete attachment','Message', null);
            update_attachment_ui('main',id);
            console.log(CSR.Vars.main_id);
        }
        Main.Mod.show_bootbox_confirm('Are you sure you want to attach this main affidavit? <br/> Please save the case report to complete attachment.',   
                                      'Confirmation',callback,null);
    }
    
    function removed_attached_main(){
        CSR.Vars.main_id_e = CSR.Vars.main_id;
        CSR.Vars.main_id = 'NONE';
    
        $('#main-id').hide();
        $('#main-id').parent().removeClass('attachment-active');
        $('#mainButton i').removeClass().addClass('fa fa-paperclip'); 
        $('#mainButton').attr('data-action', 'attach');
    }
    
    /*================================================================================
    *                           SPOT REPORT ATTACHMENT
    *   
    *===============================================================================*/
    function spotreport(){
        $('#spotButton').on('click',function(){
            var action = $(this).attr('data-action');
       
            if(action == 'attach'){
                Main.Mod.show_modal('showSpotReportsModal',1000,-200);
                $('#spot_reports_tbl').DataTable({
                    //so that all rows are not rendered on init time.
                    "deferRender": true,
                    "autoWidth": false,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    //for reinitialisation purpose
                    destroy: true,
                    "ajax" : {
                        url :  Main.Vars.host + "forms/getall_un/spot",
                        type : "GET",
                    },
                    "aoColumnDefs": [
                            { "bSortable": false, "aTargets": [ 4,5 ] },
                            { "sWidth": "15%", "aTargets": [ 0 ] },
                            { "sWidth": "20%", "aTargets": [ 1 ] },
                            { "sWidth": "20%", "aTargets": [ 2 ] },
                            { "sWidth": "14%", "aTargets": [ 3 ] },	
                            { "sWidth": "17%", "aTargets": [ 4 ] },	
                            { "sWidth": "14%", "aTargets": [ 5 ] },								
                    ],

                    "columns": [
                        { 'data' : 'spot_report_id'},
                        { 
                            "render": function(data,type,row,meta) {                  
                                var str = row.delivered_by_info.rank + ' ' + row.delivered_by_info.first_name + ' ' + row.delivered_by_info.last_name;


                                return str;
                            } 
                        },
                        { 
                            "render": function(data,type,row,meta) {                  
                                var str = row.created_by_info.rank + ' ' + row.created_by_info.first_name + ' ' + row.created_by_info.last_name;


                                return str;
                            } 
                        },
                        { 'data' : 'date'},
                        { 'data' : 'attached_to'},
                        { 
                            "render": function(data,type,row,meta) {                  
                                str = '';

                                if(row.attached_to == 'NONE'){
                                    str+= '<button data-action="attach" data-attachedto = "' + row.attached_to+ '"style="margin-right: 5px;margin-left:10px;" title="Attach" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-paperclip"></i></button>';
                                }else{
                                    str+= '<button data-action="unattach" data-attachedto = "' + row.attached_to+ '"style="margin-right: 5px;margin-left:10px;" title="Unattach" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-chain-broken"></i></button>';
                                }

                                str += '<button data-action="view" style="margin-right: 5px;" title="View" class="btn btn-info action" data-value="' + row.spot_report_id + '"><i class="fa fa-eye"></i></button>';
                                return str;
                            }
                        },        
                    ],
                    "order": [[ 0, "desc" ]],
                    sPaginationType: "full_numbers",
                });

                 //handle table button click for spot report
                 $('#spot_reports_tbl').off('click').on('click','.action',function(e){
                    var id = $(this).attr('data-value').trim();
                    var attached_to = $(this).attr('data-attachedto').trim();
                    var action = $(this).attr('data-action');


                    switch(action){
                        case 'attach': 
                            attach_spotreport(id);
                            break;
                        case 'unattach':
                            alert('Unattach!');
                            break;

                        default: 
                            alert('Cannot handle event. Contact IT Admini');
                    }
                 });
            
            } else {
                removed_attached_spotreport();       
            }
        });
  
     
    }
    
    //function from table button
    function attach_spotreport(id){
        var callback = function(){
            CSR.Vars.spot_report_id = id;
            $('#showSpotReportsModal').modal('hide');  
            Main.Mod.show_bootbox_alert('Spot report has been marked for attachment. Please save the case report to complete attachment','Message', null);
            update_attachment_ui('spotreport',id);
        }
        Main.Mod.show_bootbox_confirm('Are you sure you want to attach this spot report? <br/> Please save the case report to complete attachment.',   
                                      'Confirmation',callback,null);
    }
    
    function removed_attached_spotreport(){
        CSR.Vars.spot_report_id_e = CSR.Vars.spot_report_id;
        CSR.Vars.spot_report_id = 'NONE';
    
        $('#sp_attached_id').hide();
        $('#sp_attached_id').parent().removeClass('attachment-active');
        $('#spotButton i').removeClass().addClass('fa fa-paperclip'); 
        $('#spotButton').attr('data-action', 'attach');
    }
    
    /*================================================================================
    *                           Pre-operation ATTACHMENT
    *   
    *===============================================================================*/
    function preoperation(){
          $('#preopButton').on('click',function(){
            var action = $(this).attr('data-action');
            
            if(action == 'attach'){
                show_preoperations();
            
            }else{
                remove_attached_preoperation();
            }
          });
    }
    
    function show_preoperations(){
        Main.Mod.show_modal('showPreoperationsModal',1200,-300);
        $('#preoperations_tbl').DataTable({
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"autoWidth": false,
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			//for reinitialisation purpose
			destroy: true,
			"ajax" : {
				url :  Main.Vars.host + "forms/getall_un/preoperation",
				type : "GET",
			},
			"aoColumnDefs": [
                    { "bVisible": false, "aTargets": [6] },
					{ "bSortable": false, "aTargets": [ 4,5 ] },
					{ "sWidth": "15%", "aTargets": [ 0 ] },
					{ "sWidth": "10%", "aTargets": [ 1 ] },
					{ "sWidth": "10%", "aTargets": [ 2 ] },
					{ "sWidth": "30%", "aTargets": [ 3 ] },
                    { "sWidth": "20%", "aTargets": [ 4 ] },	
					{ "sWidth": "20%", "aTargets": [ 5 ] },								
			],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
              
                if(aData.has_approved == 1){
                    $(nRow).addClass('approved-row');	
                }
            },
			"columns": [
                { 'data' : 'pre_op_id'},
                { 'data' : 'date' },
                { 'data' : 'time'},
                { 'data' : 'summary'},
                { 
                    "render": function(data,type,row,meta) {  
                        return row.attached_to;
                    }
                },
                { 
                    "render": function(data,type,row,meta) {
                        var str = '';
                        if(row.attached_to == 'NONE'){
                            str += '<button data-action="attach" type="button" class="btn btn-info action" title="Attach" style="margin-right: 5px; margin-left:25px;" data-value="' + row.pre_op_id + '"><i class="fa fa-paperclip"></i></button>';  
                        }else{
                            str += '<button data-action="unattach" type="button" class="btn btn-info action" title="Unattach" style="margin-right: 5px; margin-left:25px;" data-value="' + row.pre_op_id + '"><i class="fa fa-chain-broken"></i></button>';
                        }
                        str += '<button data-action="view" type="button" class="btn btn-info action" title="View" data-value="' + row.pre_op_id + '"><i class="fa fa-eye"></i></button>';
						
                        return str;
                    }
                },
                
                { 
                    "render": function(data,type,row,meta) {  
                       if(row.has_approved == 1){
                            return 'Approved';
                       } else {                      
                            return 'Not';
                       }
                    } 
                },
			],
            "order": [[ 0, "desc" ]],
			sPaginationType: "full_numbers",
            
		});
        
        //add table button click events
         $('#preoperations_tbl').off('click').on('click','.action',function(e){
            var id = $(this).attr('data-value').trim();
            //var attached_to = $(this).attr('data-attachedto').trim();
            var action = $(this).attr('data-action')
            var parent = $(this).parent().parent();

            switch(action){
                case 'attach': 
                    if(parent.hasClass('approved-row')){
                         attach_preoperation(id);
                    } else {
                        Main.Mod.show_bootbox_alert("This report is not yet marked as approved. You can approve this on Preoperation Module.","Error",null);
                    }
                   
                    break;
                case 'unattach':
                    alert('Unattach!');
                    break;
            default: 
                alert('Cannot handle event. Contact IT Admini');
            }
         });       
    }
    function remove_attached_preoperation(){
        CSR.Vars.pre_op_id_e = CSR.Vars.pre_op_id;
        CSR.Vars.pre_op_id = 'NONE';
        $('#preop_attached_id').hide();
        $('#preop_attached_id').parent().removeClass('attachment-active');
        $('#preopButton i').removeClass().addClass('fa fa-paperclip'); 
        $('#preopButton').attr('data-action', 'attach');
    }
    function attach_preoperation(id){ 
        var callback = function(){
            CSR.Vars.pre_op_id = id;  
            update_attachment_ui('preoperation', id);
            $('#showPreoperationsModal').modal('hide');  
            Main.Mod.show_bootbox_alert('Spot report has been marked for attachment. Please save the case report to complete attachment','Message', null);
        }
        Main.Mod.show_bootbox_confirm('Are you sure you want to attach this pre-operation report? <br/> Please save the case report to complete attachment.',   
                                      'Confirmation',callback,null);
    }
    

    /*================================================================================
    *                           COORDINATION ATTACHMENT
    *   
    *===============================================================================*/
    function coordination(){
          $('#coordButton').on('click',function(){
            var action = $(this).attr('data-action');
            
            if(action == 'attach'){
                show_coordinations();
            
            }else {
                removed_attached_coordination();
            }   
          
          });
    }
    
    function show_coordinations(){
        Main.Mod.show_modal('showCoordinationFormsModal',1200,-300);
        
        
        $('#coordinations_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //for reinitialisation purpose
            destroy: true,
            "ajax" : {
                url :  Main.Vars.host + "forms/getall_un/coordination",
                type : "GET",
            },
            "aoColumnDefs": [
                    { "bVisible": false, "aTargets": [7] },
                    { "bSortable": false, "aTargets": [ 5,6 ] },
                    { "sWidth": "15%", "aTargets": [ 0 ] },
                    { "sWidth": "12%", "aTargets": [ 1 ] },
                    { "sWidth": "10%", "aTargets": [ 2 ] },
                    { "sWidth": "20%", "aTargets": [ 3 ] },
                    { "sWidth": "20%", "aTargets": [ 4 ] },	

            ],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){

                if(aData.has_approved == 1){
                    $(nRow).addClass('approved-row');	
                }
            },
            "columns": [
                { 'data' : 'coordination_id'},
                { 'data' : 'control_no' },
                { 'data' : 'date' },
                { 
                    "render": function(data,type,row,meta) { 
                        return row.team_leader_info.rank + ' ' + row.team_leader_info.first_name + ' ' + row.team_leader_info.last_name;
                    }
                },
                { 

                    "render": function(data,type,row,meta) {  

                        var str = row.duration_from_date + ' ' + row.duration_from_time + ' TO\n ' +
                             row.duration_to_date + ' ' + row.duration_to_time;

                        return str;                                   
                    }

                },    
                { 
                    "render": function(data,type,row,meta) {  
                        return row.attached_to;
                    }
                },
                { 
                    "render": function(data,type,row,meta) {
                        var str = '';
                        if(row.attached_to == 'NONE'){
                            str += '<button data-action="attach" type="button" class="btn btn-info action" title="Attach" style="margin-right: 5px; margin-left:10px;" data-value="' + row.coordination_id + '"><i class="fa fa-paperclip"></i></button>';  
                        }else{
                            str += '<button data-action="unattach" type="button" class="btn btn-info action" title="Unattach" style="margin-right: 5px; margin-left:10px;" data-value="' + row.coordination_id + '"><i class="fa fa-chain-broken"></i></button>';
                        }
                        str += '<button data-action="view" type="button" class="btn btn-info action" title="View" data-value="' + row.coordination_id + '"><i class="fa fa-eye"></i></button>';

                        return str;
                    }
                },
                { 
                    "render": function(data,type,row,meta) {  
                       if(row.has_approved == 1){
                            return 'Approved';
                       } else {                      
                            return 'Not';
                       }
                    } 
                },
            ],
            "order": [[ 0, "desc" ]],
            sPaginationType: "full_numbers",

        });
        
        //add button event handling
        $('#coordinations_tbl').off('click').on('click','.action',function(e){
            var id = $(this).attr('data-value').trim();
            //var attached_to = $(this).attr('data-attachedto').trim();
            var action = $(this).attr('data-action')
            var parent = $(this).parent().parent();

            switch(action){
                case 'attach': 
                    if(parent.hasClass('approved-row')){
                        attach_coordination(id);
                    } else {
                        Main.Mod.show_bootbox_alert("This report is not yet marked as approved. You can approve this on Coordination Module.","Error",null);
                    }
                   
                    break;
                case 'unattach':
                  
                    break;
            default: 
                alert('Cannot handle event. Contact IT Admini');
            }
         });       
    }
    
    function attach_coordination(id){
        var callback = function(){
            CSR.Vars.coordination_id = id;
            $('#showCoordinationFormsModal').modal('hide');  
            Main.Mod.show_bootbox_alert('Coordination form has been marked for attachment. Please save the case report to complete attachment','Message', null);
            update_attachment_ui('coordination',id);
        }
        Main.Mod.show_bootbox_confirm('Are you sure you want to attach this coordination form? <br/> Please save the case report to complete attachment.',   
                                      'Confirmation',callback,null);
    }
    
    function removed_attached_coordination(){
        CSR.Vars.coordination_id_e = CSR.Vars.coordination_id;
        CSR.Vars.coordination_id = 'NONE';
        $('#coord_attached_id').hide();
        $('#coord_attached_id').parent().removeClass('attachment-active');
        $('#coordButton i').removeClass().addClass('fa fa-paperclip'); 
        $('#coordButton').attr('data-action', 'attach');
    }
   
    
    var update_attachment_ui = function update_attachment_ui(type, id){
        
        if(type == 'preoperation') { 
           
            $('#preop_attached_id a').text(id);
            $('#preop_attached_id').show();
            $('#preop_attached_id').parent().addClass('attachment-active');
            $('#preopButton i').removeClass().addClass('fa fa-times'); 
            $('#preopButton').attr('data-action', 'remove');
        }else if(type == 'coordination') { 

            $('#coord_attached_id a').text(id);
            $('#coord_attached_id').show();
            $('#coord_attached_id').parent().addClass('attachment-active');
            $('#coordButton i').removeClass().addClass('fa fa-times'); 
            $('#coordButton').attr('data-action', 'remove');
        } else if(type == 'spotreport'){
        
            $('#sp_attached_id a').text(id);
            $('#sp_attached_id').show();
            $('#sp_attached_id').parent().addClass('attachment-active');
            $('#spotButton i').removeClass().addClass('fa fa-times'); 
            $('#spotButton').attr('data-action', 'remove');
        
        } else if(type == 'main') {
            $('#main-id a').text(id);
            $('#main-id').show();
            $('#main-id').parent().addClass('attachment-active');
            $('#mainButton i').removeClass().addClass('fa fa-times'); 
            $('#mainButton').attr('data-action', 'remove');

        } else if(type == 'supp') {
            $('#supp-id a').text(id);
            $('#supp-id').show();
            $('#supp-id').parent().addClass('attachment-active');
            $('#suppButton i').removeClass().addClass('fa fa-times'); 
            $('#suppButton').attr('data-action', 'remove');

        }
        
    }
     
     //Uploading of documents
    function on_documents_changed(){
        $("#lab").change(function() {
               
            var formdata = new FormData();

            formdata.append('case_id', $('#case-id').text());
            var inp = document.getElementById('lab').files;

            for (var i = 1; i <=  inp.length; i++) {
                formdata.append('laboratory_' + i, inp.item(i-1));
            }

            Main.Mod.show_modal('loadingModal', 750, -50);
            Main.Mod.do_ajax('casereport/upload_documents/laboratory', 'POST', function(){
                 console.log('Hi here!');
                $('#labDocuments').fadeOut( "slow", function() {
                    AM.Main.scan_documents($('#case-id').text());
                    $('#labDocuments').fadeIn();
                    $('#loadingModal').modal('hide');
                 });
            }, formdata);

        
        });
         
        $("#med").change(function() {
            var formdata = new FormData();

            formdata.append('case_id', $('#case-id').text());
            var inp = document.getElementById('med').files;
           
            for (var i = 1; i <=  inp.length; i++) {
                formdata.append('medical_' + i, inp.item(i-1));
            }

            Main.Mod.show_modal('loadingModal', 750, -50);
            Main.Mod.do_ajax('casereport/upload_documents/medical', 'POST', function(){
                 $('#medDocuments').fadeOut( "slow", function() {
                    AM.Main.scan_documents($('#case-id').text());
                    $('#medDocuments').fadeIn();
                    $('#loadingModal').modal('hide');
                 });
                
            }, formdata);
        });
        
        $("#sw").change(function() {
            var formdata = new FormData();

            formdata.append('case_id', $('#case-id').text());
            var inp = document.getElementById('sw').files;
           
            for (var i = 1; i <=  inp.length; i++) {
                formdata.append('search_' + i, inp.item(i-1));
            }

            Main.Mod.show_modal('loadingModal', 750, -50);
            Main.Mod.do_ajax('casereport/upload_documents/search', 'POST', function(){
                 $('#swDocuments').fadeOut( "slow", function() {
                    AM.Main.scan_documents($('#case-id').text());
                    $('#swDocuments').fadeIn();
                    $('#loadingModal').modal('hide');
                 });
                
            }, formdata);
        });
        
        $("#wa").change(function() {
            var formdata = new FormData();

            formdata.append('case_id', $('#case-id').text());
            var inp = document.getElementById('wa').files;
           
            for (var i = 1; i <=  inp.length; i++) {
                formdata.append('warrant_' + i, inp.item(i-1));
            }

            Main.Mod.show_modal('loadingModal', 750, -50);
            Main.Mod.do_ajax('casereport/upload_documents/warrant', 'POST', function(){
                 $('#waDocuments').fadeOut( "slow", function() {
                    AM.Main.scan_documents($('#case-id').text());
                    $('#waDocuments').fadeIn();
                    $('#loadingModal').modal('hide');
                 });
                
            }, formdata);
        });
        $("#others").change(function() {
            
            Main.Mod.show_modal('loadingModal', 750, -50);
            
            var formdata = new FormData();

            formdata.append('case_id', $('#case-id').text());
            var inp = document.getElementById('others').files;
           
            for (var i = 1; i <=  inp.length; i++) {
                formdata.append('others_' + i, inp.item(i-1));
            }
            Main.Mod.do_ajax('casereport/upload_documents/others', 'POST', function(){
                 $('#othersDocuments').fadeOut( "slow", function() {
                    AM.Main.scan_documents($('#case-id').text());
                    $('#othersDocuments').fadeIn();
                    $('#loadingModal').modal('hide');
                 });
                
            }, formdata);

        });
    }
    
    var attach = function attach(type){
          switch(type){
            case events_const.ON_ATTACHMENT_BUTTON:
                on_attachment_button();
                break;   
            case events_const.ON_DOCUMENTS_CHANGED:
                on_documents_changed();
                break; 
            default:
                alert('Cannot attach event. Contact your IT support.');
        }
    }
    
    //MISC
    var removeAttachmentsListener = function(){
        $('#removeLab').on('click',function(e){
            e.preventDefault();
            var url = 'casereport/remove_temp_doc_dir/' + $('#case-id').text().trim() + '/laboratory';
            Main.Mod.do_ajax(url,'GET',function(data){
                
                if(data.success){
                    $('#labDocuments').fadeIn();
                    AM.Main.scan_documents($('#case-id').text());
                    $("#lab").val('');
                }
               
            },null);      
       });
        
        $('#removeMed').on('click',function(e){
            e.preventDefault();
            var url = 'casereport/remove_temp_doc_dir/' + $('#case-id').text().trim() + '/medical';
            Main.Mod.do_ajax(url,'GET',function(data){
                
                if(data.success){
                    $('#labDocuments').fadeIn();
                    AM.Main.scan_documents($('#case-id').text());
                    $("#med").val('');
                }
               
            },null);   
       });
       
     $('#removeSw').on('click',function(e){
            e.preventDefault();
            var url = 'casereport/remove_temp_doc_dir/' + $('#case-id').text().trim() + '/search';
            Main.Mod.do_ajax(url,'GET',function(data){
                
                if(data.success){
                    $('#swDocuments').fadeIn();
                    AM.Main.scan_documents($('#case-id').text());
                    $("#sw").val('');
                }
               
            },null);      
       });
        
        $('#removeWa').on('click',function(e){
            e.preventDefault();
            var url = 'casereport/remove_temp_doc_dir/' + $('#case-id').text().trim() + '/warrant';
            Main.Mod.do_ajax(url,'GET',function(data){
                
                if(data.success){
                    $('#waDocuments').fadeIn();
                    AM.Main.scan_documents($('#case-id').text());
                    $("#wa").val('');
                }
               
            },null);      
       });
        
        $('#removeOthers').on('click',function(e){
            e.preventDefault();
            var url = 'casereport/remove_temp_doc_dir/' + $('#case-id').text().trim() + '/others';
            Main.Mod.do_ajax(url,'GET',function(data){
                
                if(data.success){
                    $('#othersDocuments').fadeIn();
                    AM.Main.scan_documents($('#case-id').text());
                    $("#others").val('');
                }
               
            },null);      
       });
    }
    return {
        events_const : events_const,
        attach : attach,
        update_attachment_ui : update_attachment_ui,
        removeAttachmentsListener : removeAttachmentsListener,
    }
})();