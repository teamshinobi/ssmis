var Archives = Archives || {};

Archives.Vars = (function(){
    
    
    var aoArchives = new Array();
    
    
    
    return {
        aoArchives : aoArchives,
    }
    
})();
//Feeling pro haha!
Archives.Bootstrap = (function(){
    
    var load = function(){
        Archives.Listeners.addArchiveListener();
        Archives.Listeners.newArchivesListener();
        Archives.Listeners.fileManagerListener();
        loadArchives();
        loadCaseReports();
        
         $.contextMenu({
            selector: '.item-context',
            callback: function(key, options) {
                // CM.InventoryDetails.context_action(key, options);
                context_action(key,options);
            },
            items: {
                "download": {name: "Download", icon: 'download'},
                "restore": {name: "Restore", icon: 'restore'}, 
            }
        });
       
        
    }
    
    function context_action(key,options){
        var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
        
        
        switch (key) {
            case 'download': 
                downloadArchive(item_id);
                break;
            case 'restore':
                restoreFromWeb(item_id);
                break;
        }
    }
    function restoreFromWeb(id){
        Main.Mod.do_ajax('archives/restore_from_web/' + id, 'POST', function(data){
        
            if(data.success){
                Main.Mod.show_bootbox_alert("Successfully Restored!","Message", function(){
                    window.location.reload();     
                });
            } else {
                Main.Mod.show_bootbox_alert("Cannot restore!", "Error", null);
            }
        
        },null);
    }
    
    function downloadArchive(id){
        window.location.assign(Main.Vars.host + 'archives/download/' + id);
    }
    
    function loadArchives(){
         $('#archives_tbl').DataTable({
		//so that all rows are not rendered on init time.
		"deferRender": true,
		"autoWidth": false,
		//for reinitialisation purpose
		destroy: true,
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		//NOTE: We are directly providing group id since we want to get said-sotg members only
		"ajax" : {
				url :  Main.Vars.host + "archives/retrieve",
				type : "GET",
			},
			"aoColumnDefs": [
                 
					{ "sWidth": "5%", "aTargets": [ 0 ] },
					{ "sWidth": "20%", "aTargets": [ 1 ] },
					{ "sWidth": "20%", "aTargets": [ 2 ] },
					{ "sWidth": "20%", "aTargets": [ 3 ] },
				   
			],
			"columns": [
                {
                    "data" : "id",
                },

				{ 
					"data": "case_id",
                    
				},
                { 
					"data": "blotter",
				},	
				{ 
					"data": "date",
				},	   	
			

				
			],
            "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                $(nRow).addClass('item-context');   
            },
            
            
			sPaginationType: "full_numbers",

		});
    }
    
    function loadCaseReports(){
        $('#casereports_tbl').DataTable({
		//so that all rows are not rendered on init time.
		"deferRender": true,
		"autoWidth": false,
		//for reinitialisation purpose
		destroy: true,
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		//NOTE: We are directly providing group id since we want to get said-sotg members only
		"ajax" : {
				url :  Main.Vars.host + "archives/casereports",
				type : "GET",
			},
			"aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 5 ] },
					{ "sWidth": "20%", "aTargets": [ 0 ] },
					{ "sWidth": "20%", "aTargets": [ 1 ] },
					{ "sWidth": "20%", "aTargets": [ 2 ] },
					{ "sWidth": "20%", "aTargets": [ 3 ] },
                    { "sWidth": "10%", "aTargets": [ 4 ] },
				    { "sWidth": "5%", "aTargets": [ 5 ] },
			],
			"columns": [
                {
                    "data" : "case_id",
                },

				{ 
					"data": "blotter_entry_nr",
                    
				},
                { 
					"data": "doa",
				},	
				{ 
					"data": "is_number",
				},	
                { 
					"data": "progress",
				},
				{
                    "render": function(data,type,row,meta) {
                        var str = '<input style="margin-left:11px;" data-progress="' + row.progress + '" data-value=\"' + row.case_id +'\" data-blotter=\"' + row.blotter_entry_nr +'\" style=\"padding-left: 0px; margin-left: 10px\" type=\"checkbox\"/>'
                        return str;
                    }
                
                },

				
			],
            
            
            "fnDrawCallback": function( oSettings ) {
                Archives.Listeners.archiveSelectionListener();
            },
            
			sPaginationType: "full_numbers",

		});
 
    }
    
    
    return {
        load : load,
    }
    
})();


Archives.Listeners = (function(){
    
    var archiveSelectionListener = function(){
        
		$('#casereports_tbl').off("change").on("change", ":checkbox", function(e) { 
            if($(this).attr('data-progress') != 'Closed' && $(this).is(':checked')){
                Main.Mod.show_bootbox_alert("This case is not yet closed. Cannot be archived",'Message',null);
                $(this).attr('checked', false);
            } else {
                if($(this).is(':checked')){
                    var arch = {};
                    arch.case_id = $(this).attr('data-value');
                    arch.blotter = $(this).attr('data-blotter');

                    Archives.Vars.aoArchives.push(arch);

                    console.log(Archives.Vars.aoArchives);
                }else{
                    var idx = Archives.Vars.aoArchives.indexOf($(this).attr('data-value').trim());
                    Archives.Vars.aoArchives.splice(idx, 1);

                    console.log(Archives.Vars.aoArchives);
                }
            }
			
		});
 
    }
    
    var addArchiveListener = function(){
        $('#addButton').on('click',function(){
           Main.Mod.show_modal('addModal',1200,-300);
        });
    }
    
    var newArchivesListener = function(){
        $('#startArchiveButton').on('click',function(){
            if(Archives.Vars.aoArchives.length < 1){
                Main.Mod.show_bootbox_alert('No files selected.', 'Message', null);     
            } else {
                var formdata = new FormData();
                formdata.append('cases', JSON.stringify(Archives.Vars.aoArchives));
                Main.Mod.do_ajax('archives/archive','POST',function(data){
                    if(data.success){
                        Main.Mod.show_bootbox_alert('Successfully archived!','Message', null);
                        window.location.reload();
                    } else {
                        Main.Mod.show_bootbox_alert('Cannot archived!','Message', null);
                    }
                }, formdata);
            }
        });
    }
    
    var fileManagerListener = function(){
        $('#fileManager').on('click',function(){
            window.location.assign(Main.Vars.host + 'archives/file_manager');
        });
    }
    
    return {
        addArchiveListener : addArchiveListener,
        archiveSelectionListener : archiveSelectionListener,
        newArchivesListener : newArchivesListener,
        fileManagerListener : fileManagerListener,
    }
        
    
})();
    
    