var LogIn = LogIn || {};


LogIn.LogInModule = (function(){
	
	var login = function login(formdata){
		$.ajax({
			type: 'POST',
            url: Main.Vars.host + 'login/loguser',
			contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
			dataType: 'json',
			
			success: function(data){
		
				if(data.success == true){
                    var formdata = new FormData();
        
                    for ( var key in data.session ) {
                        formdata.append(key, data.session[key]);
                    }
                    formdata.append('operation','log-in');
                    formdata.append('description', data.session['first_name'] + ' ' + data.session['last_name'] +
                                   ' has logged-in into system.');
                    formdata.append('name', data.session['first_name'] + ' ' + data.session['last_name']);
                    
                    var callback = function(){
                       window.location.assign(Main.Vars.host + "home/");
                    }

                    Main.Mod.do_ajax('audit_trail/add', 'POST', callback, formdata);
                    
				}else{
                    if(typeof data.attempts != 'undefined'){
                        location.reload();
                    } else {
                        alert("Cannot logged in!");  
                    }
					
				}
	
			}
		
		});
	};


	return {
		
		LogUserIn : login,
		
	};
})();