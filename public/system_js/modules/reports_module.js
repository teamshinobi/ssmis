var Reports = Reports || {};

Reports.Vars = (function(){
    
    //yearly report year
    var y_selected_year = '';
    //monthly report year
    var m_selected_year = '';
    var selected_month = '';
    
    var caseId = ''
    
    var acc_url = '';
    
    var bMonth = '';
    var eMonth = '';
    
    return {
        
        m_selected_year : m_selected_year,
        y_selected_year : y_selected_year,
        selected_month : selected_month,
        acc_url : acc_url,
        caseId : caseId,
        bMonth : bMonth,
        eMonth : eMonth,
    }
    
})();


Reports.Main = (function(){

    var onLoad = function onLoad(){
        get_current_date();
        
        //attach events
        Reports.Events.attach(Reports.Events.events_const.ON_MONTHLY_CALENDAR_PICK);
        Reports.Events.attach(Reports.Events.events_const.ON_GENERATE_MONTHLY_ACC);
        Reports.Events.attach(Reports.Events.events_const.YEAR_MONTH_PICKER_TRIGGERED);   
        
        Reports.Events.attach(Reports.Events.events_const.ON_YEARLY_CALENDAR_PICK);   
        Reports.Events.attach(Reports.Events.events_const.YEAR_PICKER_TRIGGERED);  
        Reports.Events.attach(Reports.Events.events_const.ON_GENERATE_YEARLY_ACC);  
        
        Reports.Events.attach(Reports.Events.events_const.ON_ENTER_CASE_ID); 
        Reports.Events.attach(Reports.Events.events_const.ON_GENERATE_CASE);  
        
        Reports.Events.attach(Reports.Events.events_const.ON_PICK_BEGIN_END_MONTH); 
        Reports.Events.attach(Reports.Events.events_const.ON_GENERATE_DRUG_STATISTICS); 

    }
    
    function get_current_date(){
        
        Main.Mod.do_ajax('home/ss_year','GET', function(data){
            Reports.Vars.m_selected_year = data.date;
            Reports.Vars.y_selected_year = data.date;
            $('#year,#year2').text('Year: ' + data.date);
        },null);
        
        Main.Mod.do_ajax('home/ss_month','GET', function(data){
            Reports.Vars.selected_month = data.date;
            $('#month').text('Month: ' + data.date);
        },null);
    }

    return {
        onLoad : onLoad,
    }
    
})();


Reports.Events =  (function(){
    var events_const = {
        
        ON_MONTHLY_CALENDAR_PICK : 1,
        ON_YEARLY_CALENDAR_PICK : 2,
        ON_GENERATE_MONTHLY_ACC : 3,
        YEAR_MONTH_PICKER_TRIGGERED : 4,
        YEAR_PICKER_TRIGGERED : 5,
        ON_GENERATE_YEARLY_ACC : 6,
        ON_ENTER_CASE_ID : 7,
        ON_GENERATE_CASE : 8,
        ON_PICK_BEGIN_END_MONTH : 9,
        ON_GENERATE_DRUG_STATISTICS : 10,
    }
    
    function render_datatable(){
        Main.Mod.show_modal('monthlyAccomplishmentModal',1200,-300);
        $('#monthAccReport').DataTable({
                "deferRender": true,
                "autoWidth": false,
                //for reinitialisation purpose
                destroy: true,
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "ajax" : {
                    url :  Reports.Vars.acc_url ,
                    type : "GET",
                },
                "order": [[ 1, "DESC" ]],
                "columns": [
                    {                 
                        "render": function(data,type,row,meta) {
                            var str = '<ul>';

                            $.each(row.violators, function(idx,obj){
                                str += '<li>' + obj.first_name  + ' ' + obj.middle_name.substring(0,1) +
                                        ' ' + obj.last_name + '</li>';
                            });


                            str += '</ul>'

                            return str;

                        }
                    },
                    { 
                        "data" : "date_of_arrest",
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = '';

                            str += row.street_name + ' ' + row.barangay + ' ' + row.city_name; 

                            return str;
                        }     
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = ' Violations of RA 9165 ';

                            $.each(row.violations, function(idx,obj){
                                str += obj.article  + ' ' + obj.section + ', ';

                            });

                            return str;

                        }
                    },
                    {
                        "data" : "nature",
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = '';


                            $.each(row.nitems,function(idx,obj){

                                if(obj.narcotics.narcotics_name == 'Shabu'){
                                    str += obj.quantity;
                                }

                            });

                            return str;         
                        }
                    },
                    {

                        "render": function(data,type,row,meta) {
                            var str = '';


                            $.each(row.nitems,function(idx,obj){

                                if(obj.narcotics.narcotics_name == 'Marijuana'){
                                    str += obj.quantity;
                                }

                            });

                            return str;         
                        }
                    },
                    {
                      "render": function(data,type,row,meta) {
                            var str = '';


                            $.each(row.nitems,function(idx,obj){

                                if(obj.narcotics.narcotics_name != 'Marijuana' && 
                                    obj.narcotics.narcotics_name != 'Shabu'){
                                    str += obj.quantity;
                                }

                            });

                            return str;         
                        }
                    },
                    {
                         "render": function(data,type,row,meta) {
                            var str = 'Currently on development';

                            return str;         
                        }
                    },
                    {
                        "data" : "is_number",
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = '';

                            return str;         
                        }   
                    },
                    {
                        "data" : "progress",
                    },
                ]
            });  
    }
    
    
    function on_generate_monthly_acc(){
        $('#generateMonthlyAccButton').on('click',function(){
          
            Reports.Vars.acc_url = Main.Vars.host + "reports/monthly_acc_pdf/" + Reports.Vars.selected_month + '/' + Reports.Vars.m_selected_year;
            
            window.open(Reports.Vars.acc_url,'_blank');
           
        });
    }
    
    function on_monthly_calendar_pick(){
        $('#pickDateForMonthly').on('click',function(){
            Main.Mod.show_modal('monthYearModal', 700,-80);
        });
    }
    
    function year_month_picker_triggered(){
        $('#pickerButton').on('click',function(){
            
            Reports.Vars.selected_month = $('#pickerMonth').val();
            Reports.Vars.m_selected_year = $('#pickerYear').val();
             
            $('#month').text('Month: ' + Reports.Vars.selected_month);
            $('#year').text('Year: ' +  Reports.Vars.m_selected_year);
            
            $('#monthYearModal').modal('hide');
        });
    }
    
    /*====================================================================
    *                   YEARLY ACCOMPLISHMENT REPORT
    *
    **====================================================================*/

    function on_yearly_calendar_pick(){
        $('#pickDateForYearly').on('click',function(){
            Main.Mod.show_modal('yearModal', 700,-80);
        });
    }
    
    function year_picker_triggered(){
        $('#pickerButton2').on('click',function(){
            
            Reports.Vars.y_selected_year = $('#pickerYear2').val();
            
            $('#year2').text('Year: ' +  Reports.Vars.y_selected_year);
            
            $('#yearModal').modal('hide');
        });
    }
    
     function on_generate_yearly_acc(){
        $('#generateYearlyAccButton').on('click',function(){
            Reports.Vars.acc_url = Main.Vars.host + "reports/yearly_acc_pdf/" + Reports.Vars.y_selected_year;
            window.open(Reports.Vars.acc_url,'_blank');
        });
    }
    
    
    /*====================================================================
    *                           CASE REPORT
    *
    **====================================================================*/
    function on_enter_case_id(){
        $('#enterCaseIdButton').on('click',function(){
           Main.Mod.show_modal('caseIdModal',650,'');
        });
        $('#enterCaseButton').on('click',function(){
            Reports.Vars.caseId = $('#caseID').val().trim();
            $('#case_id_p').text('Case ID: ' + Reports.Vars.caseId);
            $('#caseIdModal').modal('hide');
        });
    }
    
    function on_generate_case(){
        $('#generateCaseReport').on('click',function(){
            var url = Main.Vars.host + "viewcasereports/report/" + Reports.Vars.caseId;
            window.open(url ,'_blank');
        });
    }
    
    /*====================================================================
    *                     STATISTICS ON DANGEROUS DRUGS
    *
    **====================================================================*/
    
    function on_pick_begin_end_month(){
        
        $('#beMonthButton').on('click',function(){
            Main.Mod.show_modal('beginEndModal',650,'');
        });
        
        $('#enterBeginEndMonth').on('click',function(){
            Reports.Vars.bMonth = $('#bMonthInput').val();
            Reports.Vars.eMonth = $('#eMonthInput').val();
            Reports.Vars.m_selected_year = $('#cYearInput').val();
            
            $('#bMonth').text(Reports.Vars.bMonth);
            $('#eMonth').text(Reports.Vars.eMonth);
            $('#cYear').text(Reports.Vars.m_selected_year);
            
            $('#beginEndModal').modal('hide');
        });
    }
    
    function on_generate_drug_statistics(){
        $('#generateDrugStatsButton').on('click',function(){
            var url = Main.Vars.host + "reports/statistics_pdf/" + Reports.Vars.bMonth + "/" + Reports.Vars.eMonth + "/" + Reports.Vars.m_selected_year;
            window.open(url ,'_blank');
        });
    }
    
    
    var attach = function attach(type){
        switch(type){
            case events_const.ON_MONTHLY_CALENDAR_PICK:
                on_monthly_calendar_pick();
                break;   
            case events_const.ON_GENERATE_MONTHLY_ACC:
                on_generate_monthly_acc();
                break;
            case events_const.YEAR_MONTH_PICKER_TRIGGERED:
                year_month_picker_triggered();
                break;  
            case events_const.ON_YEARLY_CALENDAR_PICK:
                on_yearly_calendar_pick();
                break;  
            case events_const.YEAR_PICKER_TRIGGERED:
                year_picker_triggered();
                break; 
            case events_const.ON_GENERATE_YEARLY_ACC:
                on_generate_yearly_acc();
                break; 
            case events_const.ON_ENTER_CASE_ID:
                on_enter_case_id();
                break; 
            case events_const.ON_GENERATE_CASE:
                on_generate_case();
                break; 
            case events_const.ON_PICK_BEGIN_END_MONTH:
                on_pick_begin_end_month();
                break;      
            case events_const.ON_GENERATE_DRUG_STATISTICS:
                on_generate_drug_statistics();
                break;
            default:
                alert('Cannot attach events.');
        }
    }
    
    return {
        attach : attach,
        events_const : events_const,
    }
})();

