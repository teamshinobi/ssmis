var HOME = HOME || {};


HOME.Vars = (function(){

    var systemDate = false;
    
    return {
        systemDate : systemDate,
    }
    
})();

HOME.Main = (function(){
    
    var onLoad = function onLoad(){
        initYearlyCases();
        initMonthlyCases();
        Main.Mod.do_ajax('home/ss_clear_temps/', 'POST', function(){}, null);
        
        //init date
        initDate();
        initMontlyAccomplishment();
        initYearlyAccomplishment();
        initNarcoticsStats();
        doArchiving();
        //$(".scroll").mCustomScrollbar({ axis:"x"});
    }
    
    
    /*====================================================================================
    *
    *                                 ARCHIVING PROCESS   
    *
    *===================================================================================*/
    function doArchiving(){
        //make sure that HOME.Vars.systemDate is already set before making another ajax request.
        var interval = setInterval(function(){ 
            if(HOME.Vars.systemDate != false){
                clearInterval(interval);
                getArchiveSettings();
                    
                
            }
        },500);
    }
    
    //AJAX Archive Settings
    function getArchiveSettings(){
        Main.Mod.do_ajax('archives/get_archive_settings/','GET',function(data){
            if(data.has_data){
               
                var ok = isOk(data.settings.last_archiving,data.settings.archive_frequency);
                if(ok){
                    
                    Main.Mod.show_loading_modal('The system is scheduled for automatic archiving. Please wait...');
                    doArchivingProcess(); 
                }
            }
        }, null);
    }
    
    //do the actual process
    function doArchivingProcess(){
        Main.Mod.do_ajax('archives/do_archiving','POST',function(data){
            $('#loadingModal').modal('hide');
            if(data.success){
                Main.Mod.show_bootbox_alert('Scheduled archiving has been performed.', 'Message',null);
            } else {
                Main.Mod.show_bootbox_alert('Archiving failed.', 'Message',null);
            }
        
        },null);
    }
    
    //tells whether or not to do archiving.
    function isOk(archiveLastDate, frequency){
        var archivingLastDate = new Date(archiveLastDate);
 
        var timeDiff = Math.abs(HOME.Vars.systemDate.getTime() - archivingLastDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        
        var ok = false;
        
       
        
        console.log(diffDays);
        
        switch(frequency){
            case 'Daily':
                if(diffDays >= 1){
                   ok = true;
                } else {
                   ok = false;
                }
                break;
                
            case 'Monthly':
                if(diffDays >= 30){
                    console.log('DO!');
                   ok = true;
                } else {
                   ok = false;
                }
                break;
            case 'Yearly':
                if(diffDays >= 256){
                   ok = true;
                } else {
                   ok = false;
                }
                break;
            default: 
                ok = false;
        }
        
        return ok;
        
    }
    
    /*====================================================================================
    *                                   END ARCHIVING
    *===================================================================================*/
    
    function initDate(){
        Main.Mod.do_ajax('home/ss_date','GET',function(data){           
            $('#date').text('Statistics as of ' + data.date);
            HOME.Vars.systemDate = new Date(data.date);
        },null);
        Main.Mod.do_ajax('home/ss_month','GET',function(data){
            $('#month').text('Accomplishment Report for the month of : ' + data.date);
        },null);
    }
    
    function initNarcoticsStats(){
        Main.Mod.do_ajax('home/statistics_dangerous_drugs/','GET',function(data){
        
            
        if(jQuery.isEmptyObject(data.data)){
            $('#donutChart').append('<p style="margin-left:40px;font-weight:bold;">No data yet.</p>');
        } else {
            Morris.Donut({
              element: 'donutChart',
              data: [
                {label: "Seeds \n (occurences)", value: data.data.seeds },
                {label: "Seedlings \n (occurences)", value: data.data.seedlings.toString()},
                {label: "Stalks \n (occurences)", value: data.data.stalks},
                {label: "Plants \n (occurences)", value: data.data.plants},
                {label: "Cocaine \n (occurences)", value: data.data.cocaine},
                {label: "Syrup \n (occurences)", value: data.data.syrup},
                {label: "Marijuana \n (occurences)", value: data.data.dried},
                {label: "Shabu \n (occurences)" , value: data.data.shabu},
                {label: "Others \n (occurences)", value: data.data.others},
              ]
            });
        }
       

        },null);
    }
    
    function initYearlyCases(){ 
        Main.Mod.do_ajax('home/annual_case_report','GET',render_annual_chart,null);
    }
    function initMonthlyCases(){ 
        Main.Mod.do_ajax('home/monthly_case_report','GET',render_monthly_chart,null);
    }
    function initYearlyAccomplishment(){
        $('#yearAccReport').DataTable({
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "order": [[ 1, "DESC" ]],
            "ajax" : {
                url :  Main.Vars.host + "home/yearly_acc_report",
                type : "GET",
            },
            "columns": [
                {                 
                    "render": function(data,type,row,meta) {
                        var str = '<ul>';
                      
                        $.each(row.violators, function(idx,obj){
                            str += '<li>' + obj.first_name  + ' ' + obj.middle_name.substring(0,1) +
                                    ' ' + obj.last_name + '</li>';
                        });
                        
                        
                        str += '</ul>'
                        
                        return str;
                        
                    }
                },
                { 
                    "data" : "date_of_arrest",
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = '';
                        
                        str += row.street_name + ' ' + row.barangay + ' ' + row.city_name; 
                        
                        return str;
                    }     
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = ' Violations of RA 9165 ';
                      
                        $.each(row.violations, function(idx,obj){
                            str += obj.article  + ' ' + obj.section + ', ';
                            
                        });
               
                        return str;
                        
                    }
                },
                {
                    "data" : "nature",
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = '<ul>';
                        
                        
                        $.each(row.nitems,function(idx,obj){
                        
                            if(obj.narcotics.narcotics_name == 'Shabu'){
                                str += '<li>' + obj.quantity + '</li>';
                            }
                             str += '</ul>';
                        });
                          
                        return str;         
                    }
                },
                {
                    
                    "render": function(data,type,row,meta) {
                        var str = '<ul>';
                        
                        
                        $.each(row.nitems,function(idx,obj){
                        
                            if(obj.narcotics.narcotics_name == 'Dried Leaves'){
                                 str += '<li>' + obj.quantity + '</li>';
                            }
                        
                        }); 
                        
                        str += '</ul>';
                          
                        return str;         
                    }
                },
                {
                  "render": function(data,type,row,meta) {
                        var str = '';
                        
                        
                        $.each(row.nitems,function(idx,obj){
                        
                            if(obj.narcotics.narcotics_name != 'Dried Leaves' && 
                                obj.narcotics.narcotics_name != 'Shabu'){
                                str += obj.quantity;
                            }
                        
                        });
                          
                        return str;         
                    }
                },
                {
                     "render": function(data,type,row,meta) {
                        var str = 'Currently on development';
                    
                        return str;         
                    }
                },
                {
                    "data" : "is_number",
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = '';
                    
                        return str;         
                    }   
                },
                {
                    "data" : "progress",
                },
            ]
        });  
    
    
    }
    function initMontlyAccomplishment(){
         
        $('#monthAccReport').DataTable({
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "ajax" : {
                url :  Main.Vars.host + "home/montly_acc_report",
                type : "GET",
            },
            "order": [[ 1, "DESC" ]],
            "columns": [
                {                 
                    "render": function(data,type,row,meta) {
                        var str = '<ul>';
                      
                        $.each(row.violators, function(idx,obj){
                            str += '<li>' + obj.first_name  + ' ' + obj.middle_name.substring(0,1) +
                                    ' ' + obj.last_name + '</li>';
                        });
                        
                        
                        str += '</ul>'
                        
                        return str;
                        
                    }
                },
                { 
                    "data" : "date_of_arrest",
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = '';
                        
                        str += row.street_name + ' ' + row.barangay + ' ' + row.city_name; 
                        
                        return str;
                    }     
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = ' Violations of RA 9165 ';
                      
                        $.each(row.violations, function(idx,obj){
                            str += obj.article  + ' ' + obj.section + ', ';
                            
                        });
               
                        return str;
                        
                    }
                },
                {
                    "data" : "nature",
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = '<ul>';
                        
                        
                        $.each(row.nitems,function(idx,obj){
                        
                            if(obj.narcotics.narcotics_name == 'Shabu'){
                                str += '<li>' + obj.quantity + '</li>';
                            }
                             str += '</ul>';
                        });
                          
                        return str;         
                    }
                },
                {
                    
                    "render": function(data,type,row,meta) {
                        var str = '<ul>';
                        
                        
                        $.each(row.nitems,function(idx,obj){
                        
                            if(obj.narcotics.narcotics_name == 'Dried Leaves'){
                                 str += '<li>' + obj.quantity + '</li>';
                            }
                        
                        }); 
                        
                        str += '</ul>';
                          
                        return str;         
                    }
                },
                {
                  "render": function(data,type,row,meta) {
                        var str = '';
                        
                        
                        $.each(row.nitems,function(idx,obj){
                        
                            if(obj.narcotics.narcotics_name != 'Dried Leaves' && 
                                obj.narcotics.narcotics_name != 'Shabu'){
                                str += obj.quantity;
                            }
                        
                        });
                          
                        return str;         
                    }
                },
                {
                     "render": function(data,type,row,meta) {
                        var str = 'Currently on development';
                    
                        return str;         
                    }
                },
                {
                    "data" : "is_number",
                },
                {
                    "render": function(data,type,row,meta) {
                        var str = '';
                    
                        return str;         
                    }   
                },
                {
                    "data" : "progress",
                },
            ]
        });  
    
    }
    
    /*================================================================
    *                      CALLBACK FUNCTIONS
    *===============================================================*/
    var render_monthly_chart =  function render_monthly_chart(data){
        var dpts = new Array();

        for (var property in data.monthly_cases) {

            if (data.monthly_cases.hasOwnProperty(property)) {
                var obj = {
                    day : property,
                    value : parseInt(data.monthly_cases[property]),
                }

                dpts.push(obj);         

            }
        }
        
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'chartContainer2',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: dpts,
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Cases'],
            parseTime:false ,
        });
    }
    var render_annual_chart = function render_annual_chart(data){
    
        var dpts = new Array();

        for (var property in data.annual_cases) {

            if (data.annual_cases.hasOwnProperty(property)) {
                var obj = {
                    month : property,
                    value : parseInt(data.annual_cases[property]),
                }

                dpts.push(obj);         

            }
        }

         new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'chartContainer',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: dpts,
            // The name of the data record attribute that contains x-values.
            xkey: 'month',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Cases'],
            parseTime:false ,
        });

    }
    
    return {
        onLoad : onLoad,
    }  
})();