var VVM = VVM || {};

VVM.Main = (function(){
	var violators = [];
	var selected_violator = null;
	var violators_table;
	
	modal_const = {
		'UPDATE_VIOLATOR' : 1,
		'UPLOAD' : 2,
	}
	/*===================================================================
	*						CALLBACKS and UTILS
	*===================================================================*/
	var onLoad = function onLoad(){
		//initialization here
		init_cases();
		//attach event handlers in each mugshot clicked in viewing violators;
		VVM.Events.attach(VVM.Events.events_const.ON_MUGSHOT_CLICKED);
		//initialize some controls
		var datepickers = ['#birthdate','#dr_issued','#res_cert_issued'];
		Main.Init.InitDatePicker(datepickers);
        
        Main.Validator.activate();
	}
	
	var on_modal_showing = function on_modal_showing(type,size,margin){
		var src = "";	
		switch(type){
			case modal_const.UPDATE_VIOLATOR:
				init_view_violator_modal(size,margin);
				break;
			case modal_const.UPLOAD:
				Main.Mod.show_modal('uploadFormModal',size,margin);
				VVM.Events.attach(VVM.Events.events_const.ON_IMAGE_UPLOADED);
				break;
			default:
				console.log('Cannot show modal!');
		}
	}
	
	/*===================================================================
	*					Initialization Methods Here
	*===================================================================*/
	function init_cases(){
		Main.Mod.do_ajax('violators/cases/' + $('#violatorId').text(),'GET',on_success_cases_involved,null);
	}
	/*===================================================================
	*					AJAX ON SUCCESS CALLBACKS
	*===================================================================*/
	var on_success_cases_involved = function on_success_cases_involved(data){
		
        if(data.has_data){
           
            $('#cases_message').hide();
            var cards = $('#cards');
            var array = new Array();
            $.each(data.data, function(idx,obj){
                var kaso = {};

                kaso.caseId = obj.case_id;
                kaso.caseDate = obj.date_of_arrest;
                kaso.caseBlotter = obj.blotter_entry_nr;

                if(obj.summary.length > 95){
                    kaso.caseSummary = obj.summary.substring(0,90) + '...';
                } else {
                  kaso.caseSummary =  obj.summary;
                }

                kaso.caseImg = Main.Vars.base_path + obj.doc_path + obj.case_id + '_thumb.jpg';
                kaso.suspectsCount = obj.count_violators;
                kaso.caseSuspects = obj.violators;
                kaso.caseOperation = obj.nature;
                kaso.progress = obj.progress;
                 
                var investigator = obj.investigator.rank + " " +
                obj.investigator.first_name + " " + obj.investigator.last_name + " " + obj.investigator.middle_name.substring(0,1) + ".";
                kaso.caseInvestigator = investigator;
                
                array.push(kaso);
              
                
            });
            
           cards.sscardify({
                'module': 'casereport',	
                'contents' : array,
                'onViewClick' : function(itemId, ev){
                    Main.Mod.show_modal('loadingModal',750,'');
                    
                    var callback = function(){
                        Main.Mod.do_ajax('viewcasereports/get_access/' + itemId,'POST',function(data){
                            if(data.proceed){
                                location.assign(Main.Vars.host + 'viewcasereports/view/' + itemId);
                            }  else {
                                $('#loadingModal').modal('hide');
                                Main.Mod.show_bootbox_alert('You don\'t have access to this case.',
                                                           'Warning', null);
                            }           
                        },null);
                    }
                    
                    
                    Main.Mod.do_ajax('editcasereport/full/' + itemId,'GET',function(data){
                        var progress = data.case.case_details.progress;
                        if(progress != 'Dismissed' && progress != 'Closed'){
                            callback();
                        } else {
                            location.assign(Main.Vars.host + 'viewcasereports/view/' + itemId);
                        }
                    },null)
                            
                    
                },
                'onEditClick' : function(itemId,ev){
                    
                    Main.Mod.show_modal('loadingModal',750,'');
                    Main.Mod.do_ajax('viewcasereports/get_access/' + itemId,'POST',function(data){
                        if(data.proceed){
                           location.assign(Main.Vars.host + 'editcasereport/edit/' + itemId);
                        }  else {
                            $('#loadingModal').modal('hide');
                            Main.Mod.show_bootbox_alert('You don\'t have access to this case.',
                                                       'Warning', null);
                        }           
                    },null);
                }
            });
            $(".scroll").mCustomScrollbar();
     
        } else {
            var message = data.message + '<br/>' +
                'You can clear this violator from the database to prevent from showing.';
            $('#cases_message').append(message); 
        }
   
	}
	
	/*===================================================================
	*						Miscellaneous Here
	*===================================================================*/
	function init_view_violator_modal(size,margin){
		var violatorId = $('#violatorId').text();
		$('#violatorId_edit').text(violatorId);	
		Main.Mod.show_modal('loadingModal',750,'');
		
		var create_view = function(data){
			$('#first_name').val(data.violator.first_name);
			$('#last_name').val(data.violator.last_name);
			$('#middle_name').val(data.violator.middle_name);
			$('#nationality').val(data.violator.nationality);
			$("input[name='birthdate']").val(data.violator.birth_date);
			var $radios = $('input:radio[name=gender]');
			if(data.violator.gender == "1"){
				$radios.filter('[value=1]').prop('checked', true);
			}else{
				$radios.filter('[value=0]').prop('checked', true);
			}
            $('#place_of_birth').val(data.violator.place_of_birth);
            $('#occupation').val(data.violator.occupation);
            $('#tel_no').val(data.violator.tel_no);
			$('#alias').val(data.violator.alias);
			$('#educational_attainment').val(data.violator.educational_attainment);
            $('#name_of_school').val(data.violator.name_of_school);
            $('#location_of_school').val(data.violator.location_of_school);
			$('#street_name').val(data.violator.street_name);
			$('#barangay_name').val(data.violator.barangay_name);
			$('#city_name').val(data.violator.city_name);
			$('#weight').val(data.violator.weight);
			$('#height').val(data.violator.height);
			$('#identifying_marks').val(data.violator.identifying_marks);
            $('#complexion').val(data.violator.complexion);
			$('#hair_color').val(data.violator.hair_color);
			$('#eyes_color').val(data.violator.eyes_color);
			$('#mother_name').val(data.violator.mother_name);
            $('#mother_address').val(data.violator.mother_address);
            $('#mother_age').val(data.violator.mother_age);
			$('#father_name').val(data.violator.father_name);
            $('#father_address').val(data.violator.father_address);
            $('#father_age').val(data.violator.father_age);
			$('#contact_person').val(data.violator.contact_person);
			$('#relationship').val(data.violator.relationship);
			$('#contact_info').val(data.violator.contact_info);
            
            $('#dr_license').val(data.violator.dr_license);
            $('#dr_issued_at').val(data.violator.dr_issued_at);
            $('#dr_issued_on').val(data.violator.dr_issued_on);
            $('#res_cert').val(data.violator.res_cert);
            $('#res_cert_issued_at').val(data.violator.res_cert_issued_at);
            $('#res_cert_issued_on').val(data.violator.res_cert_issued_on);
            $('#other_ids').val(data.violator.other_ids);
            $('#id_numbers').val(data.violator.id_numbers);
            
			$('#loadingModal').modal('hide');		
			Main.Mod.show_modal('editViolatorModal',size,margin);
			//attach events
			VVM.Events.attach(VVM.Events.events_const.ON_VIOLATOR_UPDATED);
		}
		Main.Mod.do_ajax('violators/get/' + violatorId, 'GET',create_view, null);
	}
	
	return {
		onLoad : onLoad,
		on_modal_showing,
		modal_const,
	}
})();

VVM.Events =  (function(){
	var events_const = {
		'ON_MUGSHOT_CLICKED' : 1,
		'ON_IMAGE_UPLOADED' : 2,
		'ON_VIOLATOR_UPDATED' : 3,
	}
	function on_violator_update(){
		//handle form submission
		$("#update-violator-form").on('submit',(function(e){
			e.preventDefault();
			var formdata = new FormData(this);	
			formdata.append('violator_id', $('#violatorId').text());
			$.ajax({
				type: 'POST',
				url: Main.Vars.host + 'violators/update',
				contentType: false,
				cache: false,   
				processData:false, 
				data: formdata,
				dataType: 'json',
				success: function(data){
					var callback;
					if(data.success){
						callback = function(){
                            var audit_description = 'updated violator with ID of ' + $('#violatorId').text();
                            var audit_operation = 'update violator'
                            Main.Audit.save_audit(audit_description, audit_operation);
							location.reload();
						}
					}else{
						callback = function(){
							
						}
					}
					Main.Mod.show_bootbox_alert(data.message,'Message', callback);
				}
			});
		}));
	}
	function on_mugshot_clicked(){
	      
		$('.mugshot').click(function(){
			var value = $(this).attr('data-value');
			var id = $(this).attr('data-id');
			var preview = $('#preview');
			var src = Main.Vars.host;
			
			switch(value){
				case 'front':
					src += 'public/resources/photos/violator/' + id + '/' + id + '_front.jpg';
					$(preview).attr('data-value', 'front');
					break;
				case 'rear':
					src += 'public/resources/photos/violator/' + id + '/' + id + '_rear.jpg';
					$(preview).attr('data-value', 'rear');
					break;
				case 'right':
					src += 'public/resources/photos/violator/' + id + '/' + id + '_right.jpg';
					$(preview).attr('data-value', 'right');
					break;
				case 'left':
					src += 'public/resources/photos/violator/' + id + '/' + id + '_left.jpg';
					$(preview).attr('data-value', 'left');
					break;
				default:
					console.log('Cannot change image.');
			}	
			$('#preview').attr('src',src);		
		});
	}
	function on_image_uploaded(){
		$('#uploadPhotoButton').click(function(){
			var formdata = new FormData();
			formdata.append('violator_id', $('#violatorId').text());
			formdata.append('file', $('#violatorfile')[0].files[0]);
			formdata.append('tag', $('#preview').attr('data-value'));
			
			Main.Mod.do_ajax('violators/upload','POST', on_success_upload, formdata);
		});
	}
	/*===================================================================
	*					AJAX ON SUCCESS CALLBACKS
	*===================================================================*/
	var on_success_upload = function on_success_upload(data){
		var callback;
		if(data.success){
			callback = function(){
				location.reload();
			}
		}else{
			
		}	
		Main.Mod.show_bootbox_alert(data.message,'Message', callback);
	}
	var attach = function attach(source){	
		switch(source){
			case events_const.ON_MUGSHOT_CLICKED:
				on_mugshot_clicked();
				break;
			case events_const.ON_IMAGE_UPLOADED:
				on_image_uploaded();
				break;
			case events_const.ON_VIOLATOR_UPDATED:
				on_violator_update();
				break;
			default:
				alert('Cannot attach events');
		}
	}
	
	return {
		attach : attach,
		events_const,
	}
})();

