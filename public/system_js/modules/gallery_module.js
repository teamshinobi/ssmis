var GM = GM || {};

GM.Gallery = (function(){
    
    
    var render_masonry = function render_masonry(){
        init();
    }
    
    function init(){
        Main.Mod.do_ajax('gallery/violators','GET', create_masonry,null);
    }
    
    var create_masonry = function create_masonry(data){
        var $container = $('#grid');
        var gutter = 6;
        var min_width = 250;
        
        $.each(data.data, function(idx,obj){
            var str ='<div id="image-1" class="grid-item">';
           
            str += '<a class="img-link" href="' + Main.Vars.host  + 
                 obj.file_path + obj.violator_id + '_front.jpg' + '" >' +
                '</a>';
            
            str += '<img src="' + Main.Vars.host + 
                obj.file_path + '/thumbs/' +
                obj.violator_id + '_front.jpg' +
                '" alt="Image">';
            str+= '</div>';
            
            $container.append(str);
        });
        
       
       $container.imagesLoaded().progress( function() {
            $container.masonry({
                itemSelector: '.grid-item',
                gutterWidth: 50,    
                isFitWidth: true,      
            });
            $('.img-link').magnificPopup({ 
                 delegate: 'a',
                type: 'image'
            });
        });
    
    }
    
    return {
        render_masonry : render_masonry,
    }
    
})();