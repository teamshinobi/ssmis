var Tip = Tip || {};

Tip.Vars = (function(){
    
    var fakeId = 1;
    
    var targets = new Array();
    //holds the selected violators when importing
    var asImports = new Array();
    
    var events_const = {
        'ON_NEW_TIP' : 1,
        'ON_IMPORT' : 2,
    }
    
    return {
        targets : targets,
        events_const : events_const,
        
        fakeId : fakeId,
        
        asImports : asImports,
    }
    
})();

Tip.Main = (function(){
    
    var onLoad = function onLoad(){
        Tip.Events.attach(Tip.Vars.events_const.ON_NEW_TIP);
        Tip.Events.attach(Tip.Vars.events_const.ON_IMPORT);
        var datepickers = ['#datepicker', '#datepicker2'];
        Main.Init.InitDatePicker(datepickers);
        
        initTable();
        
        $.contextMenu({
            selector: '.item-context',
            callback: function(key, options) {
                 // CM.InventoryDetails.context_action(key, options);
                //PM.Events.attach(PM.Events.events_const.CONTEXT_ACTION,key,options);
                Tip.Events.context(key,options);
            },
            items: {
                "show": {name: "Show on preoperation", icon: 'approve'},   
                "unshow": {name: "Unshow on preoperation", icon: 'unapprove'},   
            }
        });
        
    }
       
    function initTable(){
        $('#tips_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "tip/retrieve",
                    type : "GET",
                },
                "aoColumnDefs": [
                       { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "5%", "aTargets": [ 0 ] },
                        { "sWidth": "10%", "aTargets": [ 1 ] },
                        { "sWidth": "40%", "aTargets": [ 2 ] },
                        { "sWidth": "20%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 4 ] },
                ],
                "order": [[ 0, "DESC" ]],
                "fnCreatedRow"  : function( nRow, aData, iDataIndex ){
                    $(nRow).addClass('item-context');	
                   
                   return false;
                },
                "columns": [
                    { 
                        "data": "id",
                    },
                    { 
                        "data": "date",
                    },	
                    { 
                        "data": "story",
                    },	   	
                    {
                        "render": function(data,type,row,meta) {
                            
                            var str = '<ul>';
                            
                            $.each(row.targets,function(idx,obj){
                                str += '<li>';
                                str +=  obj.full_name + ' A.K.A ' + obj.alias;
                                str += '</li>';
                            });
                            str += '</ul>';
                            return str;

                        }

                    },
                    {
                        "render": function(data,type,row,meta) {
                            return row.do_show == 1 ? "YES" : "NO";

                        }

                    },

                ],

                sPaginationType: "full_numbers",

		});
    
    }
     
    return {
        onLoad : onLoad,
    }
    
})();



Tip.Events = (function(){
    
    function on_new_tip(){
        Tip.Vars.fakeId = 1;
        Tip.Vars.targets.length = 0;
           
        $('#addTipButton').on('click',function(){
            Main.Mod.show_modal('addModal',800,'');
            on_add_tip();
            
            $('#targetsList').empty();
            $('#targetsList').append('<li class="list-group-item"> No target persons yet.</li>');
        });
    }
    
    function on_add_tip(){
        startAddTargetListener();
        startOnRemoveTargetListener();
        startOnAddTipListener(); 
    }
    
    function sendServerRequest(formdata){
        
        Main.Mod.do_ajax('tip/add','POST',function(data){
            
            if( data.success ){
                Main.Mod.show_bootbox_alert('Successfully saved!', 'Success', null);
                $('#tips_tbl').DataTable().ajax.reload();
                $('#importModal').modal('hide');
            } else {
                Main.Mod.show_bootbox_alert('Failed!', 'Failed', null);
            }
            $('#loadingModal').modal('hide');
            $('#addModal').modal('hide');
        },formdata);
        
    }
    
    function startOnAddTipListener(){
        
        $('#saveButton').off('click').on('click',function(){
            Main.Mod.show_modal('loadingModal', 800, '');
            if(Tip.Vars.targets.length >= 1){
                var formdata = new FormData();
                formdata.append('date',$('#date').val());
                formdata.append('story',$('#story').val());
                formdata.append('targets', JSON.stringify(Tip.Vars.targets));

                sendServerRequest(formdata);
            } else {
                Main.Mod.show_bootbox_alert("Please add atleast 1 target.", "Error",null);
            } 
        });
    }
    
    function startOnRemoveTargetListener(){
        $('#targetsList').off('click').on('click','button',function(){
            
            var id = $(this).attr('data-value');
            var parent_li = $(this).parent().parent();
            parent_li.remove();	
            
            //remove the item in array
            var i = Main.Objects.index_of(Tip.Vars.targets,id,'target_id');
          
            Tip.Vars.targets.splice(i,1);
            
            if(Tip.Vars.targets.length == 0){
                $('#targetsList').append('<li class="list-group-item"> No target persons yet.</li>');
            }
        });  
    }
    
    function startAddTargetListener(){
        $('#addTargetButton').on('click',function(){
            var target = {};
            target.full_name = $('#fullName').val();
            target.alias = $('#alias').val();
            target.target_id = Tip.Vars.fakeId;
            Tip.Vars.fakeId++;
            
            if($('#is_filipino').is(":checked")){
                target.nationality = 'Filipino';
            }else{
                target.nationality = $('#others_nationality').val();
            }
            
            Tip.Vars.targets.push(target);
            
            if(Tip.Vars.targets.length == 1){
                $('#targetsList').empty();
                $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
		     }else{
                $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');					      
            }
            
            //reset fields
            $('#fullName').val('');
            $('#alias').val('');
            $('#others_nationality').val('');
            $('#is_filipino').prop('checked', true);
        });
    }
    
    function on_import(){
        $('#importButton').click('click',function(){
            Main.Mod.show_modal('importModal',1200,-270);
            Tip.Vars.targets.length = 0;
            Tip.Vars.fakeId = 1;
            init_violators_tbl();
            startViolatorSelectionListener();
            startOnSaveImportsListener();
        });
    }
    
    function startViolatorSelectionListener(){
        //handle event for checkbox checking.
		 $('#ex_violators_tbl').off("change").on("change", ":checkbox", function() { 
            
            var cb =  $(this);
             
			if($(this).is(':checked')){

                var target = {};
                target.alias = cb.attr('data-alias');
                target.full_name = cb.attr('data-name');
                target.nationality = cb.attr('data-nationality') == '' ? 'Filipino' : cb.attr('data-nationality');
                target.target_id = Tip.Vars.fakeId;
                target.violator_id = cb.attr('data-value');
                
                Tip.Vars.targets.push(target);
                
                Tip.Vars.fakeId++; 
                
               
			} else {
                var i = Main.Objects.index_of(Tip.Vars.targets, cb.attr('data-value') ,'violator_id');
                Tip.Vars.targets.splice(i,1);	
                
                
			}
		});
    }
    
    function startOnSaveImportsListener(){
        
        $('#saveButton2').off('click').on('click',function(){
            Main.Mod.show_modal('loadingModal', 800, '');
            if(Tip.Vars.targets.length >= 1){
                var formdata = new FormData();
                formdata.append('date',$('#date2').val());
                formdata.append('story',$('#story2').val());
                formdata.append('targets', JSON.stringify(Tip.Vars.targets));

                sendServerRequest(formdata);
            } else {
                Main.Mod.show_bootbox_alert("Please add atleast 1 target.", "Error",null);
            } 
        
        });
    
    }
    
    function init_violators_tbl(){
        $('#ex_violators_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "casereport/violators",
                    type : "GET",
                },
                "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "15%", "aTargets": [ 0 ] },
                        { "sWidth": "35%", "aTargets": [ 1 ] },
                        { "sWidth": "20%", "aTargets": [ 2 ] },
                        { "sWidth": "22%", "aTargets": [ 3 ] },
                        { "sWidth": "8%", "aTargets": [ 3 ] },
                ],
                "order": [[ 0, "DESC" ]],
                "columns": [
                    { 
                        "data": "violator_id",
                    },	
                    { 	
                        "render": function(data,type,row,meta) {

                            return row.first_name + " " + row.last_name;
                        }

                    },
                    { 
                        "data": "alias",
                    },	
                    { 

                        "render": function(data,type,row,meta) {
                            //console.log(base_path + row.file_path_thumb);
                            var str_append = 'thumbs/' + row.violator_id + '_front.jpg';
                            var img1 = '<img style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
                            str_append = 'thumbs/' + row.violator_id + '_rear.jpg';
                            var img2 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
                            str_append = 'thumbs/' + row.violator_id + '_right.jpg';
                            var img3 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';
                            str_append = 'thumbs/' + row.violator_id + '_left.jpg';
                            var img4 = '<img style="margin-left:10px;" style="img style="padding-left: 0px;" margin-left: 40px" class="obj-pic" src=' + (Main.Vars.base_path + row.file_path + '/' + str_append)+ ' />';


                            return img1 + img2 + img3 + img4;

                        }
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = '';
                            
                            var fullName = row.first_name + ' ' + row.last_name + ' ' + row.middle_name.substring(0,1);
                            var alias = row.alias;
                            var nationality = row.nationality;
                            str = '<input data-nationality="' + nationality + '" data-alias="' + alias + '" data-name=\"' + fullName + '\" data-value=\"' + row.violator_id +'\" style="margin-left: 28px; margin-top:15px;" type=\"checkbox\"/>';
                            
                            return str;
                        }

                    },

                ],

                sPaginationType: "full_numbers",

		});
    
    
    }
    
    var attach = function attach(type){
        
        switch(type){
            case Tip.Vars.events_const.ON_NEW_TIP:
                on_new_tip();
                break;
            case Tip.Vars.events_const.ON_IMPORT:
                on_import();
                break; 
            default:
                alert('Cannot attach event!');
                break;
        
        }
    }
    
    /**=======================================================
    *                   CONTEXT ACTIONS
    *=======================================================*/
    var context = function context(key,options){
        var cellIndex = parseInt(options.$trigger[0].cellIndex),
		//use rowIndex to identify row.
		tbody = options.$trigger[0].parentNode;
		//rowIndex = row.index();
		var rowIndex = options.$trigger[0].rowIndex;
		var tr = $(tbody).find(':nth-child(' + rowIndex +')');
		
		var item_id = $(tr).find('td:first').text();
        switch (key) {
            case 'show':
                    show_tip(item_id);
                break;
            case 'unshow':
                    unshow_tip(item_id );
                break;     
        } 
    } 
    
    function unshow_tip(id){
        var callback = function(){
            Main.Mod.show_modal('loadingModal',800,'');
            Main.Mod.do_ajax("tip/unshow/" + id, 'GET',function(data){
                if(data.success){
                    Main.Mod.show_bootbox_alert('Successfully unshown!', 'Success', null);
                    $('#tips_tbl').DataTable().ajax.reload();
                } else {
                    Main.Mod.show_bootbox_alert('Unable to unshow!', 'Failed', null);
                }
                $('#loadingModal').modal('hide');
            },null);
        }
        
        
        Main.Mod.show_bootbox_confirm("Are you want to unshow this tip?",
                                     "Confirmation",callback,null)
      
    }
    
    function show_tip(id){
        
        var callback = function(){
            Main.Mod.show_modal('loadingModal',800,'');
            Main.Mod.do_ajax("tip/show/" + id, 'GET',function(data){
                if(data.success){
                    Main.Mod.show_bootbox_alert('Successfully shown!', 'Success', null);
                    $('#tips_tbl').DataTable().ajax.reload();
                } else {
                    Main.Mod.show_bootbox_alert('Unable to show!', 'Failed', null);
                }
                $('#loadingModal').modal('hide');
            },null);
        }
        
        
        Main.Mod.show_bootbox_confirm("Are you want to show this tip?",
                                     "Confirmation",callback,null)
      
    }
    

       
    return {
        attach : attach,
        context : context,
    }
    
})();