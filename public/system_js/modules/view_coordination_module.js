var VCM = VCM || {};


VCM.Vars = {
    'has_members_changed' : false,
    'has_vehicles_changed' : false,
}


VCM.Main = (function(){
    
    var onLoad = function onLoad(){
        VCM.Events.attach(VCM.Events.events_const.ON_FORM_SAVED);
        init();
    }
    
    function init(){
        Main.Mod.do_ajax('forms/get/coordination/' + $('#coord_id').text().trim(),'GET',function(data){
            init_form(data);
        },null);
    
    }
    
    function init_form(data){
        $('input[name="control_no"]').val(data.data.control_no);
        $('input[name="date"]').val(data.data.date);
        $('#unit').val(data.data.unit_id);
        $('#type_of_operation').val(data.data.type_of_operation);
        $('input[name="duration_to_time"]').val(data.data.duration_to_time);
        $('input[name="duration_from_time"]').val(data.data.duration_from_time);
        $('input[name="duration_to_date"]').val(data.data.duration_to_date);
        $('input[name="duration_from_date"]').val(data.data.duration_from_date);
        console.log(data);
        $('input[name="received_by"]').val(data.data.received_by);
        
        var tl = data.data.team_leader_info.rank + ' ' + data.data.team_leader_info.first_name + ' ' + data.data.team_leader_info.last_name;
        $('#team_leader').val(tl);
       
        CM.Vars.team_leader = data.data.team_leader;
        
        //load members
        $('#membersList').empty();
        $.each(data.data.members, function(idx,obj){
            CM.Vars.members.push(obj.pnp_id);
            var name = obj.rank + ' ' + obj.first_name + ' ' + obj.last_name;
            $('#membersList').append('<li style="padding-bottom: 15px;" class="list-group-item">' + name +'<span class="pull-right">' + 
                                     '<button type="button" data-value="' + obj.pnp_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
        });
        //load vehicles
        var tbody = $('#vehicles_tbl_r tbody');
        $.each(data.data.vehicles, function(idx,vehicle){
            CM.Vars.vehicles.push(vehicle.vehicle_id); 
        
            var str = '<tr role="row" class="odd">'  +
                '<td>' + vehicle.type + '</td>' + 
                '<td>' + vehicle.make + '</td>' + 
                '<td>' + vehicle.color + '</td>' + 
                '<td>' + vehicle.plate_no + '</td>' + 
                '<td >' + '<button data-id="' + vehicle.vehicle_id + '" type="button" class="btn btn-default"><i class="fa fa-trash"></i></button>' + '</td>' + 
            '</tr>';

            //append to table
            tbody.append(str);
        });
        
        
        CM.Events.attach(CM.Events.events_const.ON_REMOVE_MEMBERS);
        CM.Events.attach(CM.Events.events_const.ON_REMOVE_VEHICLE);
    }
    return {
        onLoad : onLoad,  
    }
    
})();


VCM.Events = (function(){
    
    var events_const = {
        'ON_FORM_SAVED' : 1.
    
    }
    
    function on_form_saved(){
        $("#update_coordination_form").on('submit',(function(e){
            e.preventDefault();
           
            var success = true;
            if(CM.Vars.members.length < 1){
                Main.Mod.show_bootbox_alert("Must have atleast 1 pnp officer as member.",'Error',null);
                success = false;
            }
            if(CM.Vars.vehicles.length < 1){
                Main.Mod.show_bootbox_alert("Must have atleast 1 vehicle.",'Error',null);
                success = false;
            }
            
            if (success) {
                Main.Mod.show_modal('loadingModal', 650, '');
                var formdata = new FormData(this);
                formdata.append('coordination_id',$('#coord_id').text().trim());
                formdata.append('team_leader', CM.Vars.team_leader.trim());
                formdata.append('members', JSON.stringify(CM.Vars.members));
                formdata.append('vehicles', JSON.stringify(CM.Vars.vehicles));
                formdata.append('has_members_changed', VCM.Vars.has_members_changed);
                formdata.append('has_vehicles_changed', VCM.Vars.has_vehicles_changed);
               
                
                Main.Mod.do_ajax('forms/update/coordination/', 'POST', function(data){
                    $('#loadingModal').modal('hide');
                    if(data.success){
                        Main.Mod.show_bootbox_alert(data.message, 'Success', function(){
                            location.reload();
                        });             
                    } else {
                         Main.Mod.show_bootbox_alert(data.message, 'Error', null);
                    }
                }, formdata);
            }
            
        }));
    }
    
    function validate_form(){
        
        var toReturn = true;
        var vMessages = new Array();
        
        if($('#date').val() == '' || $('#duration_from').val() == '' || $('#duration_to').val() == '' ){
            vMessages.push("Dates cannot be empty.");
            toReturn = false;
        }
        
        var result = {};
        result.isOk = toReturn;
        result.messages = vMessages;
        
        return result;
        
    }
    
    
    
    var attach = function attach(type){
        switch(type){
            case events_const.ON_FORM_SAVED:
                on_form_saved();
                break;        
            default : 
                alert('Cannot attach event. Contact your IT Admin.');
        }
    }
    
    
    return {
        events_const : events_const,
        attach : attach,
    }
    


})();