var VPM = VPM || {};

VPM.Objects = (function(){

    var targets = new Array();
    var has_targets_changed = false;
    return {
        targets : targets,
        has_targets_changed : has_targets_changed,
    }
    
})();

VPM.Main = (function(){
    var modal_const = {
        'ADD_TARGET' : 1,
    }
    
    var onLoad = function onLoad(){
        var datepickers = ['#datepicker','#duration_from_date','#duration_to_date'];
        Main.Init.InitDatePicker(datepickers);
        var timepickers = ['#timepicker_time','#timepicker_to', '#timepicker_from'];
        Main.Init.InitTimePicker(timepickers);
        
        VPM.Events.attach(VPM.Events.events_const.ON_FORM_SAVED);
        VPM.Events.attach(VPM.Events.events_const.IMPORT_TARGET);
        init();
        init_id();
    }
    
    var on_modal_showing = function on_modal_showing(type){
        switch(type){
            case modal_const.ADD_TARGET:
                Main.Mod.show_modal('addTargetPersonModal',600,'');
                VPM.Events.attach(VPM.Events.events_const.ON_ADD_TARGET);
                break;
            
            default: 
                alert('Unable to show modal! Contact your IT Programmer.');
        
        }
    } 
    
    function init(){
        var callback = function(data){
            $('#control_no').val(data.data.control_no);
            $('input[name="date"]').val(data.data.date);
            $('input[name="time"]').val(data.data.time);
            $('#summary').val(data.data.summary);
            $('#course_of_action').val(data.data.course_of_action);
            $('input[name="duration_from_time"]').val(data.data.duration_from_time);
            $('input[name="duration_to_time"]').val(data.data.duration_to_time);
            $('input[name="duration_from_date"]').val(data.data.duration_from_date);
            $('input[name="duration_to_date"]').val(data.data.duration_to_date);
            $('input[name="target_location"]').val(data.data.target_location);
            
            $('#targetsList').empty();
            $.each(data.data.targets,function(idx,target){
                VPM.Objects.targets.push(target);
                //create list
                $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');			    
            });
            
            //attach event handling of button rows
            VPM.Events.attach(VPM.Events.events_const.ON_TARGET_REMOVED);       
        }
        
        Main.Mod.do_ajax('forms/get/preoperation/' + $('#preoperation_id').text(), 'GET', callback, null);
    }
    
    function init_id(){
        var callback = function(data){
            $('#target_id').text(data.id);  
        }
        Main.Mod.do_ajax('forms/targetid', 'GET', callback ,null);
    }
    return {
        
        onLoad : onLoad,
        on_modal_showing : on_modal_showing,
        modal_const : modal_const,
    
    }

})();

VPM.Events = (function(){
    var events_const = {
        'ON_TARGET_REMOVED' : 1,
        'ON_FORM_SAVED' : 2,
        'ON_ADD_TARGET' : 3,
        'IMPORT_TARGET' : 4,
    } 
    
    function on_add_target(){
      
        $('#addTargetPersonButton').off('click').on('click',function(){  
            var target = {};
            target.full_name = $('#full_name').val();
            target.alias = $('#alias').val();
            target.target_id = $('#target_id').text();
            
            if($('#is_filipino').is(":checked")){
                target.nationality = 'Filipino';
            }else{
                target.nationality = $('#others_nationality').val();
            }
          
            VPM.Objects.targets.push(target);
            VPM.Objects.has_targets_changed = true;
            //remove the default message in the list
            if(VPM.Objects.targets.length == 1){
                $('#targetsList').empty();
                 $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
		     }else{
                $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
					'<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');					      
            }
            //ajax next id
            var callback = function(data){
                $('#target_id').text(data.id);
            }
            
            Main.Mod.do_ajax('forms/targetnextid/' + $('#target_id').text(),'GET', callback, null);
            $('#addTargetPersonModal').modal('hide');
        })
    }
    function on_form_saved(){
        
        $("#update_precooperation_form").on('submit',(function(e){
            e.preventDefault();
            Main.Mod.show_modal('loadingModal', 650, 100);
            if (VPM.Objects.targets.length < 1) {
                Main.Mod.show_bootbox_alert("Provide atleast 1 target", "Message", null);
            } else {
                var formdata = new FormData(this);
                formdata.append('pre_op_id', $('#preoperation_id').text());
                formdata.append('targets', JSON.stringify(VPM.Objects.targets));    
                formdata.append('has_targets_changed', VPM.Objects.has_targets_changed);
                
                var callback = function(data){
                    $('#loadingModal').modal('hide');
                    if(data.success) {
                        Main.Mod.show_bootbox_alert(data.message, 'Success', function(){
                            location.reload();                           
                        });
                    } else {
                        Main.Mod.show_bootbox_alert(data.message, 'Error', null);
                    }
                }
                Main.Mod.do_ajax('forms/update/preoperation', 'POST', callback, formdata);
            }

        }));
    }
    
    
    
    
    function on_target_removed(){    
        $('#targetsList').off('click').on('click','button',function(){    
            var id = $(this).attr('data-value');
            var parent_li = $(this).parent().parent();
            parent_li.remove();	
            
            //remove the item in array
            var i = Main.Objects.index_of(VPM.Objects.targets,id,'target_id');
            
            var removed = VPM.Objects.targets.splice(i,1)[0];
            
            //recycle id
            $('#target_id').text(removed.target_id);
            
            //set flag
            VPM.Objects.has_targets_changed = true;
            
            if(VPM.Objects.targets.length == 0){
                $('#targetsList').append('<li class="list-group-item"> No target persons yet.</li>');
            }
        });  
    }
    function import_target(){
        $('#importTargetButton').on('click',function(){
            Main.Mod.show_modal('importTargetModal', 1200,-280);
            init_imports();
            addImportListener();
        }); 
    }
    
    function init_imports(){
        $('#imports_tbl').DataTable({
            //so that all rows are not rendered on init time.
            "deferRender": true,
            "autoWidth": false,
            //for reinitialisation purpose
            destroy: true,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            //NOTE: We are directly providing group id since we want to get said-sotg members only
            "ajax" : {
                    url :  Main.Vars.host + "tip/get_shown_targets",
                    type : "GET",
                },
                "aoColumnDefs": [
                       { "bSortable": false, "aTargets": [ 3,4 ] },
                        { "sWidth": "5%", "aTargets": [ 0 ] },
                        { "sWidth": "40%", "aTargets": [ 1 ] },
                        { "sWidth": "20%", "aTargets": [ 2 ] },
                        { "sWidth": "20%", "aTargets": [ 3 ] },
                        { "sWidth": "5%", "aTargets": [ 4 ] },
                ],
                "order": [[ 0, "DESC" ]],
               
                "columns": [
                    { 
                        "data": "id",
                    },
                    { 
                        "data": "full_name",
                    },	
                    { 
                        "data": "nationality",
                    },	   	
                    {
                        "data": "alias",
                    },
                    {
                        "render": function(data,type,row,meta) {
                            var str = '<button style="margin-left:10px;" class="btn btn-default btn-sm" ' +
                                        '" data-fname="' + row.full_name +
                                        '" data-nationality="' + row.nationality + 
                                        '" data-alias="' + row.alias + '">';
                                str += '<i class="fa fa-plus"></i>';
                                str += '</button>';
                           
                            return str;

                        }

                    }
                ],

                sPaginationType: "full_numbers",

		});
    }
    
    function addImportListener(){
        $('#imports_tbl').off('click').delegate('button','click',function(){
            var button = $(this);
             
            var callback = function(data){
                var target = {};
            
                target.target_id = $('#target_id').text();
                target.full_name = button.attr('data-fname');
                target.nationality = button.attr('data-nationality');
                target.alias = button.attr('data-alias');
                
                $('#target_id').text(data.id);
                
                VPM.Objects.targets.push(target);
                
                //render on screen
                if(VPM.Objects.targets.length == 1){
                    $('#targetsList').empty();
                    $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
                        '<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');				
                 }else{
                    $('#targetsList').append('<li class="list-group-item">' + target.full_name  + ' a.k.a ' + target.alias +  '  ( ' + target.nationality +' )' +'<span class="pull-right">' + 
                        '<button type="button" data-value="' + target.target_id + '"class="btn btn-info"><span class="fa fa-trash"></span></button></li>');					      
                }
                
                $('#importTargetModal').modal('hide');
            } //end ajax callback
            
            Main.Mod.do_ajax('forms/targetnextid/' + $('#target_id').text(),'GET', callback, null);   
           
        });
    }
    var attach = function attach(type){
        switch(type){
            case events_const.ON_TARGET_REMOVED:
                on_target_removed();
                break;
            case events_const.ON_FORM_SAVED:
                on_form_saved();
                break;
            case events_const.ON_ADD_TARGET:
                on_add_target();
                break;
            case events_const.IMPORT_TARGET:
                import_target();
                break;
            default:
                alert('Cannot attach events.');
        }
    }
    
    return {
        attach : attach,
        events_const : events_const,
    }
})();
