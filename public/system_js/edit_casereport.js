$(function() {

	//initialize timepickers
	var timepickers = ['#timepicker'];
	Main.Init.InitTimePicker(timepickers);
	//initialize datepickerse
	var datepickers = ['#res_cert_issued_v, #dr_issued_v,#res_cert_issued','#datepicker', '#birthdate', '#birthdate_v', '#medDatePicker','#dr_issued'];
	Main.Init.InitDatePicker(datepickers);
    //activate validation
    Main.Validator.activate();
	//Initialize summernote
	$('.summernote').summernote({
        height: 300,       
    });
	//Initialize toolbar
	$('#toolbarButton').toolbar({
		content: '#user-toolbar-options', 
		position: 'right',
		hideOnClick: true
	});
	
	//Image uploading
	var idsMap = {
		itemfile : { preview : '#itempreview' },
		itemfile_v : {preview : '#itempreview_v'},
		itemfile_n : { preview : '#itempreview_n' },
		itemfile_n_v : { preview : '#itempreview_n_v' },
		frontfile : { preview : '#frontpreview'},
		rearfile : { preview : '#rearpreview'},
		leftfile : { preview : '#leftpreview'},
		rightfile : { preview : '#rightpreview'},
		frontfile_v : { preview : '#frontimagepreview' },
		rearfile_v : { preview : '#rearimagepreview' },
		leftfile_v : { preview : '#leftimagepreview' },
		rightfile_v : { preview : '#rightimagepreview' },
        casefile : { preview : '#casepreview' },
	}
	var selectors = '#casefile, #itemfile, #itemfile_n, #itemfile_v, #itemfile_n_v, #frontfile, #rearfile, #leftfile, #rightfile, #frontfile_v, #rearfile_v, #leftfile_v, #rightfile_v';
	Main.Mod.handle_image_change(selectors, idsMap);
	
	//handle toolbar event
	$('#toolbarButton').on('toolbarItemClick',
		function (event, buttonClicked) {
			var targetBlock = $(event.target).parents('.article') // get article
			var buttonClickedID = buttonClicked.id // get the id of the button click

			switch (buttonClickedID) {
				case 'saveButton':
					$( "#case-report-form" ).submit();
					break;
				case 'menu-remove':
					removeArticle(targetBlock)
					break;
			}
    });
	//retrieve barangay info
	CSR.ArrestDetails.retrieve('barangay');
	//retrieve the first id for violator on page load
	//CSR.ViolatorDetails.load_first_violator_id();
	
	//handle add investigator click
	$('#addInvestigatorButton').click(function(){
		CSR.ArrestDetails.retrieve('investigator');
		CSR.ArrestDetails.show_modal('investigator_modal');
	});
     //handle add investigator click
	$('#addFingerManButton').click(function(){
        CSR.ArrestDetails.retrieve('fingerman');
		CSR.ArrestDetails.show_modal('fingerman_modal');     
	});
    
    $('#addPhotographerButton').click(function(){
        CSR.ArrestDetails.retrieve('photographer');
		CSR.ArrestDetails.show_modal('photographer_modal');
	});
    
    
    
	//handle add investigator click
	$('#addArrestingOfficersButton').click(function(){
		CSR.ArrestDetails.retrieve('arresting');
		CSR.ArrestDetails.show_modal('arresting');
	});
	//handle putting of selected arresting officer into textarea
	$('#addSelectedArrestingButton').click(function(){	
		CSR.ArrestDetails.get_selected_arresting_officers();	
	});
	$('#closeButton').click(function(){	
		//console.log(CSR.ArrestDetails.temp_arresting_officers_ids );
		CSR.ArrestDetails.arresting_officers_ids = CSR.ArrestDetails.temp_arresting_officers_ids.slice(0);
		CSR.ArrestDetails.temp_arresting_officers_ids.length = 0;
		//console.log(CSR.ArrestDetails.arresting_officers_ids);
	});
	
	//handle form submission
	$("#case-report-form").on('submit',(function(e){
		e.preventDefault();
		
		CSR.SubmitForm.validate_casereport();
		
		results = CSR.SubmitForm.results;
		
		if(results.length == 0){
			var formdata = new FormData(this);	
			CSR.SubmitForm.submit(formdata);	
		}else{
			CSR.SubmitForm.show_validation_errors(results);
		}
		
		CSR.SubmitForm.results.length = 0;
		
	}));
    CSR.ArrestDetails.onLoad();
    CVM.Main.onLoad();
    IM.Main.onLoad();
    Witness.Main.onLoad();
    AM.Main.onLoad();
    ECM.Main.onLoad();
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
});