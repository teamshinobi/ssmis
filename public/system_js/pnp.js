$(function() {
	
	var police_table = $('#police_table');
	
	//Image uploading
	var idsMap = {
		pnp_image_file : { preview : '#preview' },	
	}
	var selectors = '#pnp_image_file';
	Main.Mod.handle_image_change(selectors, idsMap);
	
	//initialize datepickers
	var dps = ['#member_since'];
	Main.Init.InitDatePicker(dps);
	
	//disable browser caching
	Main.Init.disable_cache;
    
    PNP.PnpModule.onLoad();
	//render datatables for pnp
	PNP.PnpModule.retrieve(police_table);

	
	//handle add button click
	$('#addButton').click(function(){
		PNP.PnpModule.add();
	});
    
		
	//handle form submission
	$("#add-police-form").on('submit',(function(e){
		e.preventDefault();	
		var formdata = new FormData(this);		
		PNP.PnpModule.save(formdata);
	}));
    
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
});