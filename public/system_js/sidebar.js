
 $(document).ready(function() {
 
	initMenu();
 
	$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
	$("#menu-toggle-2").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled-2");
		$('#menu ul').hide();
	});
	
    function initMenu() {
		  $('#menu ul').hide();
		  $('#menu ul').children('.current').parent().show();
		  //$('#menu ul:first').show();
		  $('#menu li a').click(
			function() {
			  var checkElement = $(this).next();
			  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
				return false;
				}
			  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				$('#menu ul:visible').slideUp('normal');
				checkElement.slideDown('normal');
				return false;
				}
			  }
			);
     }
	 
	//holds the selected <a> tag
	var selected_a_option = null;
	//handles highlighting
	$("li").hover(function(){
		//gets the nearest a with prev_selected class
		if($(this).find('a').hasClass('prev_selected')){
			selected_a_option = $(this).find('a');
			$(this).find('a').removeClass('prev_selected');	
		}
	},function(){
		if(selected_a_option != null){
			//console.log("Adding class!");
			selected_a_option.addClass('prev_selected');
			selected_a_option = null;
		}		
	});
	
	$(".list_header").click(function(){
		var side_bar = $('.sidebar-nav li');
		
		$('#menu a').each(function(index){
			//console.log("Loooping");
			if($(this).hasClass('prev_selected')){
				//console.log("Found one!");
				$(this).removeClass('prev_selected');
			}
		
		});
		
		selected_a_option = null;
	});
 
 });