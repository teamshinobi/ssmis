$(function() {
	//render datatables
	RA.RaModule.retrieve($('#ra_table'));

	$('#addButton').click(function(){
		RA.RaModule.add();
	});

	// handles adding submission
	$("#add-ra-form").on('submit',(function(e){
		
		e.preventDefault();
		
		var formdata = new FormData(this);	
		
		RA.RaModule.add(formdata);
	}));

	//updating the form
	$('#update-ra-form').on('submit', (function(e){

		e.preventDefault();

		var formdata = new FormData(this);
        formdata.append('charge_id',$('#charge_id_view').text().trim());

		RA.RaModule.update(formdata);
	}));
    
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");

});

