$(function(){
	BM.BarangayModule.retrieve($('#barangay_table'));

	$('#view-barangay-form').on('submit', (function(e){

		e.preventDefault();

		var formdata = new FormData(this);
		formdata.append('barangay_id', $('#barangay_view').text().trim());
		BM.BarangayModule.update(formdata);
	}));

	$('#reportButton').click(function(){
		BM.BarangayModule.report();
	});

	$('#addButton').click(function(){

		BM.BarangayModule.add();
	});
    
    
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
});


 