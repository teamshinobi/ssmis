$(function() {
	
	/*==========================hover============================*/
    
    PM.ProfileModule.onLoad();
    
    
	$("[rel='tooltip']").tooltip();    
	 
	$('.thumbnail').hover(
		
		function(){
			$(this).find('.caption').slideDown(300); //.fadeIn(250)
		},
		function(){
			$(this).find('.caption').slideUp(300,function(){$ (this).stop( true, true )}); //.fadeOut(205) and stop hover
		}
	); 
	
	$('#viewProfile').click(function(){
		$('#viewModal').modal('toggle');
	});
	
	$('#updateProfile').click(function(){
		$('#updateModal').modal('toggle');
		$('#webcam').scriptcam({
			path: '/' +  "ssmis/public/swf/",
			width: 350,
			height: 350,
			showMicrophoneErrors:false,
			onError:onError,
			cornerRadius:20,
			disableHardwareAcceleration:1,
			cornerColor:'e3e5e2',
			onWebcamReady:onWebcamReady,
						
		});
	});
	
	$('#profile_image_file').change(function(){
		reader = Main.Mod.image_change(this);
		reader.onload = imageIsLoaded;
	});
	function imageIsLoaded(e) {
		$("#profilePreview").attr('src', e.target.result);
		$("#preview-msg").css('color', 'green');
    };
	
	$('#update-police-photo').on('submit',(function(e){
		alert('update LOL');
	}));

	$('#takePhoto').click(function(e){
		$('#takePhotoModal').modal('toggle');
		
	});
	
	function base64_toimage() {
		$('#profilePreview').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
	};
	
	$('#takeProfilePhoto').click(function(){
		base64_toimage();
		
		$('#takePhotoModal').toggle();
	});

	$('#cancelButton').click(function(){
		$('#updateModal').bind().toggle();
	});
	function onError(errorId,errorMsg) {
		$( "#btn1" ).attr( "disabled", true );
		$( "#btn2" ).attr( "disabled", true );
		alert(errorMsg);
	};
	function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
		$.each(cameraNames, function(index, text) {
			$('#cameraNames').append( $('<option></option>').val(index).html(text) )
		}); 
		$('#cameraNames').val(camera);
	};
    $("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");
});