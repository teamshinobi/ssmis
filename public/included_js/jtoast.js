
(function() {

 var defaults = {
 	 content : 'This is sample content',
	 duration: 1000,
	 role : 'default',
	 width: 300,
  }	
 
  this.JToast = function() {
  	extendDefaults(this,arguments[0]);
  }

  JToast.prototype.makeToast = function(){
  		buiildToast(this);
  }

  function extendDefaults(that,args){	
  	var prop;
	$.each(defaults, function(){
		for(prop in defaults){
			if(args.hasOwnProperty(prop)){
				that[prop] = args[prop];
			}else{
				that[prop] = defaults[prop];
			}
		}	
	});	
  }

  //private methods
  function buiildToast(that){

  	var toast_div = '<div class="jtoast">' + 
						'<div class="' + that.role + '">' + 
							that.content + 
						'</div>' + 
					'</div>';
	$(toast_div).appendTo('body');	
	$('.jtoast div').css('width', that.width);	
	$('.jtoast').fadeIn(that.duration, function(){
	 	$('.jtoast').fadeOut(that.duration,function(){
	 		$('.jtoast').remove();
	 	})
	});			
  }	
}());
