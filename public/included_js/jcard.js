(function($) {

	var settings;
	$.fn.jcard = function(contents,options){
		
		settings = $.extend({
			type : 'default',
			headerhover : '#FFCDD2',
			paginate : false,
			shown : 'all',
			width : '100%',
		},options);
			

		createCards(this,contents);
	}	

	function createCards(that, contents){
		//$('#' + this.selector).append("<li>Hello!</li>");

		$.each(contents, function(idx, obj){
			var str = '<li class="card-holder">' + 
							'<div class="card">' +
								'<div class="card-header header-' + settings.type + '">'+
									'<p>' +
										'<a>' + obj.header + '</a>' +
										'<span class="card-pull pull-right">' + obj.pull + '</span>' +
									'</p>' +
									'<span class="card-sub">' + obj.subheader	 + '</span>' +
								'</div>'+
								'<div class="card-content"><p>' + obj.content + '</div>' +
							'</div>' + 
						'</li>';
			$(that).append(str);
			str ="";
		});

		//apply styles
		$(".card-header p a").mouseenter(function() {
		    $(this).css("color", settings.headerhover)
		}).mouseleave(function() {
		     $(this).css("color", 'white').css("border-radius", "0px");
		});

		$('.card').css('width', settings.width);


	}

}( jQuery ));	