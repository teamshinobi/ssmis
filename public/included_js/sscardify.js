/**
* ssCardify.js v1.0
* A simple jQuery cardifying plugin that uses simplePagination.js for pagination behavior and 
*	bootCards framework for css styling.
*
* Made by JC Frane, No Copyright Infringement.
*
* Striclty for SAID-SOTG, Philippines.
*/


(function($){
	
	var methods = {
		init: function(options) {
			var ss_options = $.extend({
				module : 'casereport',
				contents : new Array(),
				onViewClick: function(itemId, event) {
				
				},
                onEditClick: function(itemId, event) {
                
                }
			},options || {});
			
			methods.selector = this;
			methods.opts = ss_options;
			
            if(ss_options.module == 'casereport'){
                methods.createCards(ss_options.contents);
            } else if (ss_options.module == 'pnp'){
                methods.createPnpCards(ss_options.contents);
            } else if (ss_options.module == 'images'){
                methods.createImageCards(ss_options.contents);
            } else {
                methods.createViolatorCards(ss_options.contents);
            }
			
			
			return this;
		}, //end init
	    createImageCards : function(contents){
            console.log(contents)
            $.each(contents, function(idx, obj){
                var str = '<div class="col-lg-4">';
                    
                    str += '<div class="panel panel-default bootcards-media">' +
                                '<div class="panel-heading">' + 
                                    '<h3 class="panel-title">' + obj.fileName + '</h3>' + 
                                '</div>';
                    str += '<div class="panel-body" style="padding-bottom: 20px;">' +
                                obj.desription  + 
                            '</div>'+
                            '<img class="img-thumbnail case-image img-responsive" src="' +  Main.Vars.host + obj.filePath  + '"/>';
                    
                    str += '</div>';
                    str += '</div>';
            
                methods.selector.append(str);
            });
            
        },    
        createPnpCards : function(contents){
            console.log(contents);
            $.each(contents, function(idx, obj){
                var str = '<div class="col-lg-4">' +
                            '<div class="panel panel-default">';
                    //HEADER
                    str += '<div class="panel-heading clearfix">' +
                            '<h3 class="panel-title pull-left">' + obj.policeName + '</h3><br>' + 
                                '<img style="width: 75px; height: 75px" class="pull-right img-thumbnail" src="' + Main.Vars.host + obj.filePath + '_thumb.jpg"/>' +
                            '</div>';
                    //INFO
                    str += '<div class="list-group">' +
                              '<div class="list-group-item clear clearfix">' + 
                                '<p class="list-group-item-text">Rank</p>' + 
                                '<h4 class="list-group-item-heading">' + obj.policeRank + '</h4>' + 
                              '</div>' +
                              '<div class="list-group-item">' +
                               '<p class="list-group-item-text">Status</p>' +
                                '<h4 class="list-group-item-heading">' + obj.status + '</h4>' +
                              '</div>' + 
                              '<div class="list-group-item">' +
                                '<h4 class="list-group-item-heading">Address</h4>' +
                                '<p class="list-group-item-text">' + obj.address + '</p>' +
                              '</div>' +
                            '</div>';
                    //FOOTER
                    str += '<div class="panel-footer">' + 
                                '<small><a href="' + '/./ssmis/pnp/view/' + obj.pnpId + '">' + obj.pnpId + '</a></small>' +
                            '</div>';
                    str +='</div></div>'; //end col and panel-default
            
                methods.selector.append(str);
            });
        },
        
        createViolatorCards: function (contents) {
            $.each(contents,function(idx, obj){
                var str = '<div class="col-md-4 col-lg-4 violator_info">';
                    str += '<div class="panel panel-default">';
                    str += '<div class="panel-heading clearfix">';
                    str += '<h3 class="panel-title pull-left">' + obj.violatorName + '</h3>' +
                            '<a data-id="'+ obj.violatorId + '" class="btn btn-default pull-right viewButton">' +
                                '<i class="fa fa-sm fa-eye"></i>' +
                                'View' +
                            '</a>';
                    str += '</div>'; //end heading
                    str += ' <div class="list-group">';
                    str +=  '<div class="list-group-item">' + 
                                '<p class="list-group-item-text">Alias</p>' +
                                '<h4 class="list-group-item-heading">' +obj.violatorAlias + '</h4>' +
                            '</div>';
                    str +=  '<div class="list-group-item">' +
                                '<p class="list-group-item-text">Nationality</p>' + 
                                '<h4 class="list-group-item-heading">' + obj.violatorNationality + '</h4>' +
                            '</div>';
                    str +=  '<div class="list-group-item">' +
                                '<p class="list-group-item-text">Occupation</p>' + 
                                '<h4 class="list-group-item-heading">' + obj.violatorOccupation + '</h4>' +
                            '</div>';
                    str += '<div class="list-group-item">' +
                                '<p class="list-group-item-text">Mugshots</p>' + 
                                '<div class="row">' + 
                                    '<div class="col-lg-3 col-md-3"> ' +
                                       '<img style="width: 60px; height:60px;" src="'+ Main.Vars.host + obj.violatorFilePath + '/thumbs/' + obj.violatorId + '_front.jpg' + '" />' +
                                    '</div>' +
                                     '<div class="col-lg-3 col-md-3"> ' +
                                       '<img style="width: 60px; height:60px;" src="'+ Main.Vars.host + obj.violatorFilePath + '/thumbs/' + obj.violatorId + '_right.jpg' + '" />' +
                                    '</div>' +
                                    '<div class="col-lg-3 col-md-3"> ' +
                                       '<img style="width: 60px; height:60px;" src="'+ Main.Vars.host + obj.violatorFilePath + '/thumbs/' + obj.violatorId + '_left.jpg' + '" />' +
                                    '</div>' +
                                    '<div class="col-lg-3 col-md-3"> ' +
                                       '<img style="width: 60px; height:60px;" src="'+ Main.Vars.host + obj.violatorFilePath + '/thumbs/' + obj.violatorId + '_rear.jpg' + '" />' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
            
                    str += '</div>'; //end list group
                    str += '<div class="panel-footer">' +
                                '<small>' + obj.violatorId + '</small>' +
                            '</div>';
                    str += '</div>'; //end panel default
                str += '</div>' //end col
                
                methods.selector.append(str);
            });
            
            $('.viewButton').on('click',function(e){
				var id = $(this).attr('data-id');
            
				methods.opts.onViewClick(id,e);
				
			});
        },
        
		createCards : function(contents){
			$.each(contents,function(idx, obj){
				var str = '<div class="col-md-4 col-lg-4">';
					str += '<div class="panel panel-default bootcards-media">';
                    var header = '<div class="panel-heading clearfix">';
                   
                    if(obj.progress == 'Pending on Inquest Office'){
                        header = '<div class="panel-heading panel-heading-inquest clearfix">';
                    } else if (obj.progress == 'Closed'){
                        header = '<div class="panel-heading panel-heading-closed clearfix">';
                    } else if (obj.progress == 'Pending on Court'){
                        header = '<div class="panel-heading panel-heading-court clearfix">';
                    } else if (obj.progress == 'Dismissed'){
                        header = '<div class="panel-heading panel-heading-dismissed clearfix">';
                    }
                    
					str += header;
					str += '<h3 class="panel-title pull-left">' + obj.caseId + '</h3>';
					str += '<p class="pull-right">' + obj.caseDate +'</p>';
					str += '</div>';
					str +=  '<div class="panel-body"  id="panel-' + obj.caseId +'" >' + 
							'</div>';
					str +=  '<img class="case-image" src="' + obj.caseImg + '" class="img-responsive"/>';
					str += '<div class="list-group scroll" style="max-height: 155px;">' +
                                '<div class="list-group-item">' + 
									'<h4 class="list-group-item-heading">Blotter Entry NR:</h4>' + 
									'<p class="list-group-item-text">' + obj.caseBlotter + '</p>'	+
								'</div>' +
                                '<div class="list-group-item">' + 
									'<h4 class="list-group-item-heading">Type of Operation:</h4>' + 
									'<p class="list-group-item-text">' + obj.caseOperation + '</p>'	+
								'</div>' +
								'<div class="list-group-item" >' + 
									'<h4 class="list-group-item-heading">Suspects Involved: ( ' +  obj.suspectsCount + ' )</h4>' + 
                                    '<ul>';
                                    //loop through violators
                                    $.each(obj.caseSuspects,function(idx,obj){
                                        str += '<li>' + obj.first_name + ' ' + obj.last_name; 
                                    });

                        str+=   '</ul></div>';
                            
                        str+= '<div class="list-group-item">' + 
                                    '<h4 class="list-group-item-heading">Case Investigator:</h4>' +
                                    '<p class="list-group-item-text">' + obj.caseInvestigator + '</p>';
                        str+= '</div>';
						str+= '</div>';
                    
                    var footer = '<div class="panel-footer">';
                
					str += footer +
								'<div class="btn-group btn-group-justified">'+
								  '<div class="btn-group">'+
									'<button data-id="' + obj.caseId + '" class="btn btn-default viewButton">'+
									 ' <i class="fa fa-eye"></i>'+
									  'View'+
									'</button>'+
								'</div>'+
                                '<div class="btn-group">'+
									'<button data-id="' + obj.caseId + '" class="btn btn-default editButton">'+
									  '<i class="fa fa-edit"></i>'+
										'Edit'+
									'</button>'+
									'</div>'+
								'</div>'+
                                '</div>' +
						      '</div>';
					str +=  '</div></div>';
					
				methods.selector.append(str);
                $('#panel-' + obj.caseId).append(obj.caseSummary);
			});
			
			$('.viewButton').on('click',function(e){
				var id = $(this).attr('data-id');
				methods.opts.onViewClick(id,e);
				
			});
            $('.editButton').on('click',function(e){
				var id = $(this).attr('data-id');
				methods.opts.onEditClick(id,e);
				
			});
			
		},
		
		destroy : function() {
			methods.selector.empty();
		},
		
		getCard : function(){
		
		},
		
		_itemClick : function(){
			
			
		}
	}

	
	$.fn.sscardify = function(method) {
		if(typeof method === 'object' || !method){
			return methods.init.apply(this, arguments);
		} else if(methods[method] && method.charAt(0) != '_'){
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}else {
			$.error('Cannot create cards. Invalid initialization');
		}	
	}
	
})(jQuery);