$(function() {
    
	var violators_list = $('#violators_list tbody');
	
    var selectedInvestigator = "";
	//holds the violator id of existing violator that has been selected
	var selectedExisting = "";
    //holds violators objects
    var violators = [];
    //initializes displays
    $('.investigators-form').hide();
    $('.arresting-officers-form').hide();  
    $('.message').hide();
	$('.additional-messages').hide();
    $('#violator-details').fadeToggle();
	$('#violator_id').val(get_violator_next_db_id());
	$('#existing-violators-div').fadeOut();
	$('#view-violator-div').fadeOut();
	
	//initialize datepickers
	var dps = ['#date', '#bday', '#bday_v'];
	Main.Init.InitDatePicker(dps);
	
	
	/*================================================
	*
	*	Handling events in violators list table
	*
	*=================================================*/
	//violators' id
	var selected_violator = "";
	//handles selection of violator in table
	$('#violators_list tbody').delegate('tr', 'click', function () {
		if ( $(this).hasClass('row-selected') ) {
            $(this).removeClass('row-selected');
			selected_violator = "";
		} else {
			$('#violators_list tbody tr.row-selected').removeClass('row-selected');
			$(this).addClass('row-selected');
			var td = $(this).find('td').first();
			selected_violator = td.text();
        }
    });
	
	$('#view-violator').click(function(){
		if(selected_violator == ""){
			Main.Mod.ShowModal('Error!', 'No violator selected!', 'case-modal'); 
		}else{
			$('#view-violator-div').fadeIn();
		
			$('#view_img').attr('src', base_path + '/public/resources/photos/temps/violator/' + selected_violator + ".jpg")
		
			var selected_violator_obj = CSR.ViolatorDetails.GetViolator(selected_violator);
			$('#fname_v').val(selected_violator_obj.first_name);
			$('#mname_v').val(selected_violator_obj.middle_name);
			$('#lname_v').val(selected_violator_obj.last_name);
			$('#alias_v').val(selected_violator_obj.alias);
			$('#bday_v').val(selected_violator_obj.birth_date);
			$('#barangay_name_v').val(selected_violator_obj.barangay_name);
			$('#street_name_v').val(selected_violator_obj.street_name);	
			$('#city_name_v').val(selected_violator_obj.city_name);
			$('#nationality_v').val(selected_violator_obj.nationality);
			$('#weight_v').val(selected_violator_obj.weight);
			$('#height_v').val(selected_violator_obj.height);
			$('#eyes_v').val(selected_violator_obj.eyes_color);
			$('#hair_v').val(selected_violator_obj.hair_color);
			$('#ident_marks_v').val(selected_violator_obj.identifying_marks);
			$('#mother_v').val(selected_violator_obj.mother_name);
			$('#father_v').val(selected_violator_obj.father_name);
			$('#contact_v').val(selected_violator_obj.contact_person);
			$('#relationship_v').val(selected_violator_obj.relationship);
			$('#contact_info_v').val(selected_violator_obj.contact_info);
				
			if(selected_violator_obj.gender != 1){
				$('#female_v').prop('checked',true);
			}else{
				$('#male_v').prop('checked',true);
			}
			
			//shows charges commited
			var charges = CSR.ViolatorDetails.GetViolatorCharges(selected_violator);
			
			
		}
	});
	
	$('#cancel_view').click(function(){
		$('#view-violator-div').fadeOut();
	});
	$('#update_viol').click(function(){
		CSR.ViolatorDetails.UpdateViolator(selected_violator);
		$('#view-violator-div').fadeOut();
	});
	
	/*======================END========================*/
	
	/*=================================================
	*
	*	Handling showing of possible investigators
	*
	*==================================================*/
    $('#show-investigators').click(function(){
        $('.investigators-form').show();
        //scrolls the pages to bottom
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        $.ajax({
            type: 'GET',
            url: host + 'pnp/possibleinvestigators',
            dataType: 'json',
            success: function(data){
                var template = $("#investigators_template").html();
                var musch = Mustache.render(template,data);
                $('#investigators_list').append(musch);          
            }
        });
    });
    //cancels/hide investigator lists on click
    $('#cancel-investigators-form').click(function(){
        $('.investigators-form').hide();
        $('#investigators_list tbody').remove();
    });
	/*======================END=========================*/
	
	/*=================================================
	*
	*	Handling showing of possible arresting officers
	*	and other stuffs
	*
	*==================================================*/
	
	//handle select click
    $("#investigators_list").delegate(".select-investigator",'click', function(){
        selectedInvestigator = $(this).attr("data-id");
       
        $("#investigatorTF").val($(this).attr("data-content"));
        //transfers the id to investigatorTF
        $("#investigatorTF").attr('value',$(this).attr("data-id"));
        //scrolls the pages to then hide after
        $("html, body").animate({ scrollTop: 100 },1000,function(){
          $('.investigators-form').hide();       
          $('#investigators_list tbody').remove();
        });
      
    });
    //show arersting officers lists on click.
    $('#show-arresting-officers-form').click(function(){
        $('.arresting-officers-form').show();
        //scrolls the pages to bottom
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        
        $.ajax({
            type: 'GET',
            url: host + 'pnp/possiblearrestingofficers',
            dataType: 'json',
            success: function(data){
                var template = $("#arresting_template").html();
                var musch = Mustache.render(template,data);
                $('#arresting_list').append(musch);          
            }
        });

    });
    //cancels/hide arresting officers lists on click
    $('#cancel-arresting-form').click(function(){
        $('.arresting-officers-form').hide();
        $('#arresting_list tbody').remove();
    });
    //handling checkbox checking
    var checkBox;
    $("#arresting_list").delegate(".select-arresting",'click', function(){
        checkBox = $(this).children('td').children('input[name=chk]');  
        if(checkBox.prop('checked') == true){
              checkBox.prop('checked', false);  
        }else{ 
             checkBox.prop('checked', true); 
        }
    });
    //adds selected rows to table
    $('#add-arresting').click(function(){
        $("#arresting_list input[type=checkbox]:checked").each(function () {    
            //appends to table
            $('#selected-list tbody').append('<tr name="selected-arresting"><td>' +  $(this).attr("data-value") + '</td>' +  
                '<td><button type="button" class="btn btn-danger .remove">-</button>'
                + '</td></tr>');
            $('#selected-list tbody tr:last').attr('value', $(this).attr("data-id"));
        });
        $("html, body").animate({ scrollTop: 600 },500,function(){
            $('.arresting-officers-form').hide();
            $('#arresting_list tbody').remove();
        });   
    });
    //remove selected rows from selected arresting officers
    $('#selected-list').delegate('button', 'click', function(){     
        $(this).closest('tr').remove();
    });
	
	//gets selected arresting officers by looping through the table.
    function getSelectedArrestingOfficers(){
        var rows = [];
        $("#selected-list tbody").each(function() {
            $(this).find("tr").each(function(){
               rows.push($(this).attr('value'));
            });
        });   
        return rows;
    }
	
	/*======================END=========================*/
	
	
	/*==================================================
	*
	*		Image Uploading Functionalies
	*
	*===================================================*/
	
	//handle file input change.
	$('#violator_pic_file').change(function(){
		reader = handleFileChange(this);
		reader.onload = imageIsLoaded;
	});
	$('#violator_view_pic_file').change(function(){
		reader = handleFileChange(this);
		reader.onload = imageIsLoaded2;
	});
	 
	 //when a valid photo was chosen
    function imageIsLoaded(e) {
        $('#image_preview').css("display", "block");
        $('#preview').attr('src', e.target.result);
        $('#preview').attr('width', '300px');
        $('#preview').attr('height', '300px');
    } 
	//for updating violator
	function imageIsLoaded2(e) {    
        $('#view_img').attr('src', e.target.result);
        $('#view_img').attr('width', '300px');
        $('#view_img').attr('height', '300px');
    } 
	
	
	/*======================END=========================*/
	
	
	/*==================================================
	*
	*	Adding Violator Functionalities including loading 
	*	all charges using ajax.
	*
	*===================================================*/
	var ex_table = $('#existing_violators');
	var ex_charges_table = $('#charges_table');
	
	
	//to avoid showing other forms from other tab
	$('#arrest_tab').click(function(){
		$('#violator-details').hide();
	});
	//show existing violators
	$('#show-existing').click(function(){
		$("html, body").animate({ scrollTop: 400 },500,function(){
			$('#existing-violators-div').fadeIn();
			ex_table.api().ajax.reload(); // user paging is not reset on reload
        });
	});
	//hide existing violators
	$('#cancel-existing').click(function(){
		hide_existing_div();
	});
	
	 //handles showing of new violator form.
    $('#show-violator-details').click(function(){
        $("html, body").animate({ scrollTop: 400 },500,function(){
			$('#violator-details').show(); 
        });
        //ajax charges
        $.ajax({
            type: 'GET',
            url:  host + 'law/ra9165',
            dataType: 'json',
            success: function(data){
				$('#charges_list tbody').remove(); 
                var template = $("#charges_template").html();
                var musch = Mustache.render(template,data);
                $('#charges_list').append(musch);          
            }
        });

    });
    //adds clickable capability in charges_list
    var checkBox1;
    $("#charges_list").delegate(".select-charges",'click', function(){
        checkBox = $(this).children('td').children('input[name=chk]');  
        if(checkBox.prop('checked') == true){
              checkBox.prop('checked', false);  
        }else{ 
             checkBox.prop('checked', true); 
        }
    });

    //handles hiding of new violator form.
    $('#hide-violator-details').click(function(){  
        $('#violator-details').fadeOut(function(){
            $("html, body").animate({ scrollTop: 400 },500);
            $('#charges_list tbody').remove();  
        });    
    });
	
	//handles adding violator in table
    $('#add-violator').click(function(){
        /*  hide the input file and create new input file
            If the current violator_id in textfield is equals to the first input attribute name.
        */
        validateForm();
        //errors is in the usables.js
        if(errors){
            $('.modal .modal-header').html('<h2>Error...</h2>');
            $('.modal-body p').html('Please correct errors highlighted with red.');
            //dont allow the user to close the prompt until the response has been received.
            $('#case-modal').attr('data-backdrop','static'); 
            $('#case-modal').modal('show');  
            //reset errors flag  
            errors = false;
        }else{
            $('.modal .modal-header').html('<h2>Success...</h2>');
            $('.modal-body p').html('Successfully added!');
            //dont allow the user to close the prompt until the response has been received.
            $('#case-modal').attr('data-backdrop','static'); 
            $('#case-modal').modal('show');  
           
			
            //hides error messages again and again
            $('.message').hide();  
            //append to table
            push_violator(); 
        }
      
    });
	
	//adds violator in the table
	function push_violator(){
	
		var formdata = new FormData();
		formdata.append('violator_id', $('#violator_id').val());
		formdata.append('userfile', $('#violator_pic_file')[0].files[0]);
		 
		 //post request to save image to temporary folder in the server.
		 $.ajax({
            type: 'POST',
            url: host + 'casereport/temphoto',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
			dataType: 'json',
            success: function(data){
                console.log('Saved to temp folder.');
                
				//actual appending
                violators_list.append('<tr>'+ 
                    '<td>' + $('#violator_id').val() + '</td>' +
                    '<td>' + $('#fname').val() + ' ' + $('#lname').val() + '</td>' +
                    '<td>' + $('#alias').val() + '</td>' +
                    '<td>' + 'Still working...' + '</td>' +
                    '<td>' + '<img class="off-pic" src="' + base_path + '/public/resources/photos/temps/violator/thumbs/' + $('#violator_id').val() + '_thumb.jpg"</td>' +
                    '</tr>');
					
				$('#violator_id').val(data.next_id);
            }
        });
		
		//creating violator object.
		var object = new violator();

        object.violator_id = $('#violator_id').val();
        object.first_name = $('#fname').val();
        object.last_name = $('#lname').val();
        object.middle_name = $('#mname').val();
        object.alias = $('#alias').val();
        object.birth_date = $('#bday').val();
        object.gender = $('input[name=gender]:radio:checked').val();
        object.street_name = $('#street_v').val();
        object.barangay_name = $('#brgy_v').val();
        object.city_name = $('#city_v').val();
        object.nationality = $('#nationality').val();
        object.height = $('#height').val();
        object.weight = $('#weight').val();
        object.educational_attainment = $('#educ_bg').val();
        object.eyes_color = $('#eyes_color').val();
        object.hair_color = $('#hair_color').val();
        object.identifying_marks = $('#ident_marks').val();
        object.mother_name = $('#mother').val();
        object.father_name = $('#father').val();
        object.contact_person = $('#contact').val();
        object.relationship = $('#relationship').val();
        object.contact_info = $('#contact_info').val();
        object.remarks = "On-Jail";
		
		
		//loop through charge_list table and add the charges
        $("#charges_list input[type=checkbox]:checked").each(function () {    
            object.charges.push($(this).attr("data-id"));
            console.log('charge: ' + $(this).attr("data-id"));
        });
		
        //violators.push(object);
		
		CSR.ViolatorDetails.AddViolator(object);
		
		//hides no content message
		$('.no-content-message').hide();
		
        //$('#preview').attr('src', '../resources/photos/pnp/default_picture.jpg');
	}
	
	
	//DataTable for existing violators
	ex_table.dataTable({
			"autoWidth": false,
			//so that all rows are not rendered on init time.
			"deferRender": true,
			"ajax" : {
				url :  host + "violator/violatorslist",
				type : "GET",
			},
			"columns": [
				{ 
					"width": "5px",
					"data": "violator_id"					
				},	
				{ 
					"data": null,	
					"render": function(data,type,row,meta) {
						return row.first_name + " " + row.last_name;
					},
					"width": "400px",
				},
				{ "data": "alias" },
				{ "data" : "remarks" },
				{ 
					"width": "5px",
					"data": null,
					"sortable": false,
					"searchable": false,
					"render": function(data,type,row,meta) {
						//console.log(base_path + row.file_path_thumb);
						return "<img src=" + (base_path + row.file_path_thumb)+ " />";

					}
				}
			]
	});
	
	
	//DataTable for charges when selecting existing violators
	ex_charges_table.dataTable({
			"autoWidth": false,
			"ajax" : {
				url :  host + "law/ra9165",
				type : "GET",
			},
			"columns": [
				{ 
					"width": "10px",
					"data": "charge_id"					
				},	
				{ "data": "article" },
				{ "data" : "section" },
				{ 
					"width": "100px",
					"data" : "description" 
				}
			]
	});	
	
	
	//single selection for violators
	$('#existing_violators tbody').on( 'click', 'tr', function () {
		if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
			selectedExisting = "";
		}
        else {
			$('#existing_violators tr.selected').removeClass('selected');
			$(this).addClass('selected');
			
			var td = $(this).find('td').first();
			selectedExisting = td.text();
        }
    });
	
	//multiple selection of charges for existing violator
	$('#charges_table tbody').on( 'click', 'tr', function () {
        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }else{
			$(this).addClass('selected');
		}
    });
	
	//add selected violator to table and ajax its properties
	$("#add_selected_viol").click(function(){ 
		console.log("<===================>");
		var x_charges = [];
		$("#charges_table tbody").each(function() {
			 $(this).find("tr").each(function(){
				if($(this).hasClass('selected')){
					var td= $(this).find('td').first();
					x_charges.push(td.text());
					//console.log(td.text());
				}
			});
		});
		
		if(selectedExisting == "" || x_charges.length < 1){	
			Main.Mod.ShowModal('Error', 'Please select a violator and charges against him/her before adding.','case-modal'); 
		} else { 
			//add selected existing violator to table after ajax. This includes copying permanent photos to temp folder
			  $.ajax({
				type: 'GET',
				url: host + 'violator/getviolator/' + selectedExisting,
				dataType: 'json',
				success: function(data){
	
					//console.log('From ajax: ' + data.data[0].violator_id);
					violators_list.append('<tr>'+ 
                    '<td>' +  data.data[0].violator_id + '</td>' +
                    '<td>' +  data.data[0].first_name + ' ' +  data.data[0].last_name + '</td>' +
                    '<td>' +  data.data[0].alias + '</td>' +
                    '<td>' + 'Still working...' + '</td>' +
                    '<td>' + '<img class="off-pic" src="' + base_path + '/public/resources/photos/violator/thumbs/' +  data.data[0].violator_id + '_thumb.jpg"</td>' +
                    '</tr>');
					
					var object = new violator();

					object.violator_id = data.data[0].violator_id;
					object.first_name = data.data[0].first_name;
					object.last_name =  data.data[0].last_name;
					object.middle_name = data.data[0].middle_name;
					object.alias = data.data[0].alias;
					object.birth_date = data.data[0].birth_date;
					object.gender = data.data[0].gender;
					object.street_name = data.data[0].street_name;
					object.barangay_name = data.data[0].barangay_name;
					object.city_name = data.data[0].city_name;
					object.nationality = data.data[0].nationality;
					object.height = data.data[0].height;
					object.weight =  data.data[0].weight;
					object.educational_attainment =  data.data[0].educational_attainment;
					object.eyes_color = data.data[0].eyes_color;
					object.hair_color =  data.data[0].hair_color;
					object.identifying_marks =  data.data[0].identifying_marks;
					object.mother_name =  data.data[0].mother_name;
					object.father_name =  data.data[0].father_name;
					object.contact_person =  data.data[0].contact_person;
					object.relationship = data.data[0].relationship;
					object.contact_info =  data.data[0].contact_info;
					object.remarks = "On-Jail";
					//to tell that this violator came from the database and has previous records
					object.existing = true;
					object.charges = x_charges;
					
					//violators.push(object);
					
					CSR.ViolatorDetails.AddViolator(object);
					
					hide_existing_div();
							
					//hides no content message
					$('.no-content-message').hide();
							
					Main.Mod.ShowModal('Success!', 'Violator successfully added!','case-modal'); 
				}
			});
		}
	});
	
	//gets the of the next violator id based on the last row in the db.
	function get_violator_next_db_id(){
		$.ajax({
            type: 'GET',
            url: host + 'violator/nextid',
            dataType: 'json',
            success: function(data){
                $('#violator_id').val(data.next_id);          
            }
        });
	}
	function hide_existing_div(){
		console.log('Putaena maghide ka!');
		$("html, body").animate({ scrollTop: 400 },500,function(){
			$('#existing-violators-div').fadeOut();
			selectedExisting = "";
			
			//removes all selected classes to ensure that there are no selected. In short, reset
			$('#existing_violators tr.selected').removeClass('selected');
			$('#charges_table tr.selected').removeClass('selected');
        });
	}
	//validates violator.
	function hasViolators(){
		var violators = CSR.ViolatorDetails.GetViolators();
		if(violators.length < 1){
			return false;
		}
		return true;;
	}
	/*=================END ADDING VIOLATOR FUNCTIONS================*/
	
	/*==================================================
    *   
	*	 Submitting the whole form! Woah!
	*
    *====================================================*/
	
    $("#submit-form").on('submit',(function(e) {
		e.preventDefault();
		if(hasViolators()){
			Main.Mod.ShowModal('Saving', 'Please wait while saving...', 'case-modal'); 
	
			//get selected arresting officers
			arresting = get_arresting_officers();

			var formdata = new FormData(this);
		 
			//tang inang to!
			formdata.append("investigator", selectedInvestigator);
			//arresting is only string.
			formdata.append("arresting",arresting); 
			//append violators here in json.
			formdata.append("violators", JSON.stringify(CSR.ViolatorDetails.GetViolators()));

			submit(formdata);
		} else {
			Main.Mod.ShowModal('Information not complete!', 'Please complete all modules.', 'case-modal'); 
		}
       
    }));
    //handle submitting...
    function submit(formdata){
        $.ajax({
            type: 'POST',
            url: host + 'casereport/submitcase',
            contentType: false,
            cache: false,   
            processData:false, 
            data: formdata,
            dataType: 'json',
            success: function(data){
                $('.dismiss-button').show();
                //means there were errors in validation
                if(data.hasOwnProperty("errors")){
                    $('.modal-body p').html('There were errors in validating, check your inputs again.');
                  
                    $('.modal .modal-header').html('<h2>Error in validating.</h2>');
                    if("blotter_entry_err" in data.errors){
                        $('#blotter-message').show();
                        $('#blotter-message').html(data.errors.blotter_entry_err);
                    }
                    if("investigator_err" in data.errors){
                        $('#investigator-message').show();
                        $('#investigator-message').html(data.errors.investigator_err);
                    }
                    if("date_err" in data.errors){
                        $('#date-message').show();
                        $('#date-message').html(data.errors.date_err);
                    }
                    if("street_err" in data.errors){
                        $('#street-message').show();
                        $('#street-message').html(data.errors.street_err);
                    }
                    if("city_err" in data.errors){
                        $('#city-message').show();
                        $('#city-message').html(data.errors.city_err);
                    }
                    if("arresting_err" in data.errors){
                        $('#arresting-message').show();
                        $('#arresting-message').html(data.errors.arresting_err);
                    }

                }else{
                    if(data.hasOwnProperty("db_errors")){       
						Main.Mod.ShowModal('Fatal!', data.db_errors.db_err_message, 'case-modal'); 						
                    }else{
						Main.Mod.ShowModal('Success!', 'Saving was successful!', 'case-modal'); 						
                   
                        //clears violators arrays
                        violators.length = 0;
                        //hides violators details form. Actually not a form, but part of the form. 
                        $('#violator-details').fadeOut();
                        //reset the form
                        $('#submit-form')[0].reset();
                        //reset the violator id textfield
                        $('#violator_id').val(data.next_ids.violator_id);
                        //empty violator table by removing child on tbody
                        $('#violators_list tbody').empty();
                        //empty selected investigator
                        investigator = "";
                        $('#investigatorTF').val("");
                        //empty arresting table by removing child on tbody
                        $('#selected-list tbody').empty();

                        console.log('Success in saving...');
                    }               
                }
				
            }
        });
    }
	
	 //gets selected arresting officers
    function get_arresting_officers(){
        var rows = [];
        $("#selected-list tbody").each(function() {
            $(this).find("tr").each(function(){
               rows.push($(this).attr('value'));
            });
        });   
        return rows;
    }
	
	/*======================END=========================*/
	
	/**
    * Ajax init calls
    */
    
    //initialize barangay drop-down (located at san juan only)
    $.ajax({
        type: 'GET',
        url: host + 'barangay/barangaylist',
        dataType: 'json',
        success: function(data){
            var template = $("#barangay_template").html();
            var musch = Mustache.render(template,data);
            $('#barangay_select').append(musch);
        }
    
    }); //end getting barangays for dropdown

});